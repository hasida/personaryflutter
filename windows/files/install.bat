@echo off

set d=%~dp0
set d=%d:\=^\\%

echo Windows Registry Editor Version 5.00 > plrctl.reg
echo. >> plrctl.reg

echo [HKEY_CLASSES_ROOT\plrctl] >> plrctl.reg
echo "URL Protocol"="" >> plrctl.reg
echo @="personary" >> plrctl.reg
echo. >> plrctl.reg

echo [HKEY_CLASSES_ROOT\plrctl\shell\open\command] >> plrctl.reg
echo @="\"%d%personary.exe\" \"%%1\"" >> plrctl.reg

reg import plrctl.reg
del plrctl.reg
