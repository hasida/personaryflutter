#!/bin/sh

#  retrieve_dart_defines.sh
#  Runner
#
#  Created by hayato miura on 2021/10/12.
#  
# Flavorの指定によってxcconfigをincludeする
OUTPUT_FILE="${SRCROOT}/Flutter/DartDefines.xcconfig"

function decode_url() { echo "${*}" | base64 --decode; }

: > $OUTPUT_FILE

IFS=',' read -r -a define_items <<<"$DART_DEFINES"
is_flavor_specified=false

for index in "${!define_items[@]}"
do
    item=$(decode_url "${define_items[$index]}")
    if [ $(echo $item | grep 'FLAVOR') ] ; then
        is_flavor_specified=true
        value=${item#*=}
        # FLAVORに対応したXCConfigファイルをincludeする
        echo "#include \"$value.xcconfig\"" >> $OUTPUT_FILE
    fi
done

# flavorが指定されていなかった場合、personary.xcconfigをincludeさせる
$is_flavor_specified || echo "#include \"personary.xcconfig\"" >> $OUTPUT_FILE
