import 'package:flutter/material.dart';

const double _paddingBaseScale = 4;
const int _paddingStyleCount = 11;

final List<EdgeInsets> allPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.all(_paddingBaseScale * index),
);

final List<EdgeInsets> verticalPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.symmetric(vertical: _paddingBaseScale * index),
);

final List<EdgeInsets> horizontalPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.symmetric(horizontal: _paddingBaseScale * index),
);

final List<EdgeInsets> topPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.only(top: _paddingBaseScale * index),
);

final List<EdgeInsets> rightPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.only(right: _paddingBaseScale * index),
);

final List<EdgeInsets> bottomPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.only(bottom: _paddingBaseScale * index),
);

final List<EdgeInsets> leftPadding = List<EdgeInsets>.generate(
  _paddingStyleCount,
  (int index) => EdgeInsets.only(left: _paddingBaseScale * index),
);
