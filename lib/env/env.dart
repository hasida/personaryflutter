import 'package:envied/envied.dart';

part 'env.g.dart';

@Envied(path: '.env')
abstract class Env {
  @EnviedField(varName: 'SYSTEM_PROMPT_API_KEY', obfuscate: true)
  static String SystemPromptApiKey = _Env.SystemPromptApiKey;
}