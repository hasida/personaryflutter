import "package:i18n_extension/i18n_extension.dart";

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  retrievingTranslation +
  unknownTranslation +
  lifeRecordTranslation +
  {
    "en_us": "Health data coordination",
    "ja_jp": "ヘルスデータ連携",
  } +
  {
    "en_us": "No coordination settings.",
    "ja_jp": "連携設定がありません",
  } +
  {
    "en_us": "Coordinate",
    "ja_jp": "連携",
  } +
  {
    "en_us": "Channel to coordinate",
    "ja_jp": "連携先チャネル",
  } +
  {
    "en_us": "Data types",
    "ja_jp": "データタイプ",
  } +
  {
    "en_us": "Remove",
    "ja_jp": "削除",
  } +
  {
    "en_us": "Remove the setting",
    "ja_jp": "設定の削除",
  } +
  {
    "en_us": "Are you sure to remove the Health data coordination setting?",
    "ja_jp": "このヘルスデータ連携設定を削除してよろしいですか?",
  } +
  {
    "en_us": "No activity recognition access is granted.",
    "ja_jp": "身体活動データにアクセスできません",
  };

  String get i18n => localize(this, t);
}
