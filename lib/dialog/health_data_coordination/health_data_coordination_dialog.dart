import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import '../../util.dart';
import '../../widget.dart';
import 'health_data_coordination_dialog.i18n.dart';

class HealthDataCoordinationDialog extends StatefulConsumerPlrWidget {
  const HealthDataCoordinationDialog({ super.key });

  @override
  PlrConsumerState<HealthDataCoordinationDialog> createState() =>
    _HealthDataCoordinationDialogState();
}

class _HealthDataCoordinationDialogState
  extends PlrConsumerState<HealthDataCoordinationDialog>
  with WidgetsBindingObserver {

  PermissionObserver? observer;

  bool _isGranted = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    if (Platform.isAndroid) {
      observer = PermissionObserver(
        this, Permission.activityRecognition,
        "No activity recognition access is granted.".i18n,
        (_, isGranted) => setState(() => _isGranted = isGranted),
      );
      _isGranted = observer!.isGranted;

      Future.microtask(() => observer!.observe(context));
    }
    Future.microtask(() => _refresh(context));
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) =>
    observer?.onChangeAppLifecycleState(context, state);

  @override
  void dispose() {
    observer?.dispose();

    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  late Iterable<HealthQuery> _queries;
  late List<MultiSelectItem<HealthQuery>> _queryItems;
  List<HealthSetting>? _settings;

  Future<void> _refresh(BuildContext context) async {
    var queriesSettings; try {
      queriesSettings = await Future.wait([
          lastHealthQueries, storage.healthSettings,
      ]);
    } catch (e, s) {
      showError(context, e, s);
      queriesSettings = [[], []];
    }
    setState(() {
        _queries = queriesSettings[0];
        _queryItems = _queries.where((q) => q.isAvailAnyDataTypes).map(
          (q) => MultiSelectItem<HealthQuery>(
            q, q.label?.defaultValue?.toString() ?? "Unknown".i18n,
          ),
        ).toList().cast<MultiSelectItem<HealthQuery>>();
        _settings = queriesSettings[1];
    });
  }

  final List<Widget> bgPair = dismissibleBackgroundPairWith(
    Colors.red, const Icon(Icons.delete, color: Colors.white),
    Text("Remove".i18n, style: const TextStyle(color: Colors.white)),
  );

  bool get _isAvailable => _isGranted && (_settings != null);

  @override
  Widget build(BuildContext context) => SafeArea(
    child: ProgressHUD(
      child: Builder(
        builder: (context) => Scaffold(
          appBar: AppBar(
            title: Text("Health data coordination".i18n),
            actions: [
              Container(
                width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
                child: FloatingActionButton(
                  heroTag: "hero_create_health_setting",
                  tooltip: "Coordinate".i18n,
                  backgroundColor: _isAvailable ?
                    Theme.of(context)
                      .floatingActionButtonTheme.backgroundColor :
                    Theme.of(context).disabledColor,
                  child: const Icon(Icons.link),
                  onPressed: !_isAvailable ? null : () => _add(context),
                ),
              ),
            ],
          ),
          body: RefreshIndicator(
            onRefresh: () => _refresh(context),
            child: Builder(
              builder: (context) {
                var progress = ProgressHUD.of(context)!;

                if (_settings == null) {
                  Future.microtask(progress.show);
                  return emptyPage;
                }
                Future.microtask(progress.dismiss);

                if (_settings!.isEmpty) return PageScrollView(
                  child: Center(
                    child: Text("No coordination settings.".i18n),
                  ),
                );
                return _buildList(context);
              }
            ),
          ),
        ),
      ),
    ),
  );

  Widget _buildList(BuildContext context) => ListView.builder(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    itemCount: _settings!.length,
    itemBuilder: (context, index) {
      var setting = _settings![index];
      var timeline = setting.timeline;

      var queriesString = setting.queries.map(
        (q) => q.label?.defaultValue?.toString() ?? "Unknown".i18n,
      ).join(", ");

      return Dismissible(
        key: ValueKey(setting.id),
        background: bgPair[0],
        secondaryBackground: bgPair[1],
        confirmDismiss: (_) => _remove(context, setting),
        onDismissed: (_) => _refresh(context),
        child: Card(
          child: ListTile(
            enabled: _isGranted,
            title: Consumer(
              key: UniqueKey(),
              builder: (context, ref, _) {
                final provider = entitySyncProvider(timeline, notifyAll: true);
                ref.listen(provider, (_, next) {
                    switch (next) {
                      case AsyncError(:final error, :final stackTrace): {
                        if (error is EntryNotFoundException) {
                          _remove(context, setting, true);
                        }
                        showError(context, error, stackTrace);
                      }
                    }
                });
                ref.watch(entitySyncProvider(timeline, notifyAll: true));

                var name;
                if (!timeline.isLoaded) name = "Retrieving...".i18n;
                else {
                  if (
                    (timeline is! Channel) &&
                    timeline.hasProperty(channelInfoProperty)
                  ) timeline = timeline.as<Channel>();

                  if (timeline is Channel) {
                    var channel = timeline as Channel;
                    if (channel.isRemoved) {
                      Future.microtask(() => _remove(context, setting, true));
                      return Container();
                    }
                    name = (timeline as Channel).name
                      ?.defaultValue?.toString() ?? "Unknown".i18n;
                  } else name = "Life record".i18n;
                }
                return Text(name, overflow: TextOverflow.ellipsis);
              },
            ),
            subtitle: Text(queriesString),
            onTap: () => _edit(context, setting),
          ),
        ),
      );
    },
  );

  Future<List<HealthQuery>?> showQuerySelectDialog(
    BuildContext context, [
      List<HealthQuery> initialValue = const [],
  ]) async {
    var localizations = MaterialLocalizations.of(context);

    List<HealthQuery>? queries;
    await showDialog(
      context: context,
      builder: (context) => MultiSelectDialog<HealthQuery>(
        title: Text("Data types".i18n),
        listType: MultiSelectListType.CHIP,
        items: _queryItems,
        initialValue: initialValue,
        searchable: false,
        selectedColor: Theme.of(context).colorScheme.secondary,
        selectedItemsTextStyle: const TextStyle(color: Colors.white),
        cancelText: Text(localizations.cancelButtonLabel),
        confirmText: Text(localizations.okButtonLabel),
        onConfirm: (values) => queries = values,
      ),
    );
    if (queries?.isNotEmpty != true) return null;

    return _queries.where(queries!.contains).toList();
  }

  Future<void> _add(BuildContext context) async {
    var currentChannelIds = (await runWithProgress(
        context, () => Future.wait(
          _settings!.map((s) async {
              if (!await s.isChannelTimeline) return null;
              return (await s.timelineAsChannel)!.id;
            },
        )),
        onError: (e, s) => showError(context, e, s),
    ))!.nonNulls;

    var channel = (
      await showChannelSelectDialog(
        context, root,
        title: "Channel to coordinate".i18n,
        excludeIds: currentChannelIds,
        onError: (e, s) => showError(e, s),
      )
    )?.first;
    if (channel == null) return;

    var queries = await showQuerySelectDialog(context);
    if (queries == null) return;

    var accountName = (storage.type.name == "googleDrive") ?
      storage.plrId.email : null;

    await runWithProgress(
      context, () => newHealthSetting(
        storage, accountName: accountName,
        queries: queries, timeline: channel,
      ),
      onError: (e, s) => showError(context, e, s),
      onFinish: (r) async {
        if (r != null) await _refresh(context);
      },
    );
  }

  Future<bool> _edit(BuildContext context, HealthSetting setting) async {
    var queries = await showQuerySelectDialog(
      context, setting.queries.toList(),
    );
    if (queries == null) return false;

    setting.queries = queries;

    await runWithProgress(
      context, setting.save,
      onError: (e, s) => showError(context, e, s),
      onFinish: (_) => _refresh(context),
    );
    return true;
  }

  Future<bool> _remove(
    BuildContext context,
    HealthSetting setting, [
      bool force = false,
  ]) async {
    if (!force && !await showConfirmDialog(
        context, "Remove the setting".i18n,
        "Are you sure to remove the Health data coordination setting?".i18n,
    )) return false;

    await runWithProgress(
      context, setting.remove, onError: (e, s) => showError(context, e, s),
    );
    return true;
  }
}
