import "package:i18n_extension/i18n_extension.dart";
import "package:plr_ui/plr_ui.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  confirmationTranslation +
  {
    "en_us": "App definitions",
    "ja_jp": "アプリ定義"
  } +
  {
    "en_us": "Add app defnition.",
    "ja_jp": "アプリ定義を追加"
  } +
  {
    "en_us": "Create new",
    "ja_jp": "新規作成"
  } +
  {
    "en_us": "Import from friend",
    "ja_jp": "友達からインポート",
  } +
  {
    "en_us": "Import app definitions",
    "ja_jp": "アプリ定義をインポート",
  } +
  {
    "en_us": "Import",
    "ja_jp": "インポート",
  } +
  {
    "en_us": "Set this app definition to public.",
    "ja_jp": "このアプリ定義を公開します",
  } +
  {
    "en_us": "Set this app definition to private.",
    "ja_jp": "このアプリ定義を非公開にします",
  };

  String get i18n => localize(this, t);
}
