import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../app.dart';
import '../../util.dart';
import '../../widget.dart';
import '../../page/main/active_app_definition.dart';
import 'app_definition_dialog.i18n.dart';

enum _AddType {
  Create_new, Import_from_friend;
  String get label => name.replaceAll("_", " ").i18n;
}

class AppDefinitionDialog extends StatelessConsumerPlrWidget {
  AppDefinitionDialog();

  late final AppDefinitionRegistrySyncProvider
    _appDefinitionRegistrySyncProvider;
  late final AppDefinitionRegistrySync _appDefinitionRegistrySyncController;

  late final PublicRootSyncProvider _publicRootSyncProvider;
  late final PublicRootSync _publicRootSyncController;

  late final ActiveAppDefinitionProvider _activeAppDefinitionProvider;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _appDefinitionRegistrySyncProvider =
      appDefinitionRegistrySyncProvider(appDefinitionRegistry);
    _appDefinitionRegistrySyncController =
      ref.read(_appDefinitionRegistrySyncProvider.notifier);

    _publicRootSyncProvider = publicRootSyncProvider(publicRoot);
    _publicRootSyncController = ref.read(_publicRootSyncProvider.notifier);

    _activeAppDefinitionProvider = activeAppDefinitionProvider(storage);
  }

  Future<void> _refresh(
    WidgetRef ref, [
      bool force = false,
  ]) => Future.wait([
      _appDefinitionRegistrySyncController.refresh(force: force),
      _publicRootSyncController.refresh(force: force),
  ]);

  @override
  Widget doBuild(BuildContext rootContext, WidgetRef ref) => HookConsumer(
    builder: (context, ref, _) {
      final Schemata? schemata; {
        final snapshot = useStream(
          useMemoized(() => appDefinitionRegistry.timelineSchemata),
        );
        schemata = snapshot.data;

        if (schemata == null) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const Center(child: const CircularProgressIndicator());
          }
          Future.microtask(() {
              showMessage(
                rootContext, "Failed to get the app definition schemata.",
              );
              Navigator.pop(context);
          });
          return emptyPage;
        }
      }
      return _buildPage(context, ref, schemata);
    },
  );

  Widget _buildPage(BuildContext context, WidgetRef ref, Schemata schemata) {
    ref.listen(
      _appDefinitionRegistrySyncProvider,
      (_, next) => processError(context, next),
    );

    final appDefinitionRegistryValue = ref.watch(
      _appDefinitionRegistrySyncProvider,
    ).value;

    return SafeArea(
      child: ProgressHUD(
        child: Builder(
          builder: (context) => Scaffold(
            appBar: _buildAppBar(
              context, ref, schemata, appDefinitionRegistryValue,
            ),
            body: RefreshIndicator(
              onRefresh: () => _refresh(ref),
              child: _buildList(context, ref, schemata),
            ),
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(
    BuildContext context,
    WidgetRef ref,
    Schemata schemata,
    AppDefinitionRegistryState? appDefinitionRegistryValue,
  ) {
    final int count = appDefinitionRegistryValue?.appDefinitions.length ?? 0;

    return AppBar(
      title: Text(
        "App definitions".i18n + ((count > 0) ? " (${count})" : ""),
      ),
      actions: [
        PopupMenuButton(
          position: PopupMenuPosition.under,
          itemBuilder: (_) => _AddType.values.map(
            (t) => PopupMenuItem(child: Text(t.label), value: t),
          ).toList(),
          onSelected: (value) {
            switch (value) {
              case _AddType.Create_new: _createNew(context, ref, schemata);
              case _AddType.Import_from_friend: _importFromFriend(
                context, ref, appDefinitionRegistryValue,
              );
            }
          },
          child: Container(
            width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
            child: FloatingActionButton(
              heroTag: "hero_add_app_definition",
              tooltip: "Add app defnition.".i18n,
              child: const Icon(Icons.add),
              onPressed: null,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildList(
    BuildContext context,
    WidgetRef ref,
    Schemata schemata,
  ) => AppDefinitionList(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    provider: _appDefinitionRegistrySyncProvider,
    container: appDefinitionRegistry,
    onError: (e, s) => showError(context, e, s),
    cellConfig: appDefinitionCellConfigWithDefault(
      onTap: (appDefinition, controller) => _edit(
        context, ref, schemata, appDefinition, controller,
      ),
      buttonBuilders: [
        (
          context, ref, _2, appDefinition, _4, _5, isEnabled,
          wrapCallback,
        ) {
          if (!appDefinition.isInner) return emptyWidget;

          return Consumer(
            builder: (context, ref, _) {
              final publicRootValue = ref.watch(_publicRootSyncProvider).value;
              if (publicRootValue == null) {
                return const CircularProgressIndicator();
              }
              final isPublic = publicRootValue.appDefinitions.any(
                (ad) => ad.id == appDefinition.id,
              );

              return CompactIconButton(
                icon: isPublic
                  ? const Icon(Icons.public, color: Colors.black)
                  : const Icon(Icons.public_off, color: Colors.grey),
                tooltip: (
                  isPublic
                    ? "Set this app definition to private."
                    : "Set this app definition to public."
                ).i18n,
                onPressed: isEnabled
                  ? wrapCallback(
                    context, ref, () {
                      if (isPublic) {
                        publicRoot.removeAppDefinition(appDefinition);
                      } else publicRoot.addAppDefinition(appDefinition);

                      _publicRootSyncController.refresh();
                    },
                  )
                  : null,
              );
            },
          );
        },
        (
          context, ref, _2, appDefinition, _4, _5, isEnabled,
          wrapCallback,
        ) => Consumer(
          builder: (context, ref, _) => switch (
            ref.watch(_activeAppDefinitionProvider)
          ) {
            AsyncLoading() => const CircularProgressIndicator(),
            AsyncData(:final value) => CompactIconButton(
              icon: (appDefinition.id == value)
                ? const Icon(Icons.check_circle, color: Colors.green)
                : const Icon(Icons.check_circle, color: Colors.grey),
              onPressed: isEnabled
                ? wrapCallback(
                  context, ref, () => _applyAppDefinition(
                    context, ref, appDefinition, value,
                  ),
                )
                : null,
            ),
            _ => emptyWidget,
          },
        ),
      ],
      useDefaultDismissibleConfig: false,
      dismissibleConfig: appDefinitionDismissibleConfigWithDefault(
        objectRemover: (
          context, ref, controller, appDefinition, referrer, state,
        ) async => await runWithProgress(context, () async {
            final isActive =
              ref.read(_activeAppDefinitionProvider).value == appDefinition.id;
            if (isActive && !await showConfirmDialog(
                context, "Confirmation".i18n,
                "Unset the app definition and restart the standard app.\n"
                " Are you sure?.".i18n,
              )
            ) return false;

            if (
              !await defaultAppDefinitionRemover(
                context, ref, controller, appDefinition, referrer, state,
              )
            ) return false;

            if (appDefinition.isInner) {
              publicRoot.removeAppDefinition(appDefinition);
            }

            if (isActive) {
              await ref.read(_activeAppDefinitionProvider.notifier).clear();

              appDefinitionRegistry.syncSilently();
              if (publicRoot.isAnyChanged) publicRoot.syncSilently();

              App.restart(context);
            }
            return true;
        }) ?? false,
        onDismissed: (_) {
          _appDefinitionRegistrySyncController.refresh();
          if (publicRoot.isAnyChanged) _publicRootSyncController.refresh();
        },
      ),
    ),
  );

  Future<AppDefinition?> _createNew(
    BuildContext context,
    WidgetRef ref,
    Schemata schemata,
  ) async {
    final p = schemata.propertyOf(appDefinitionProperty);
    if (p?.firstRange == null) return null;

    final recordEntity = RecordEntity.create(
      schemata, p!.firstRange!.id, referer: p, commitNewVersion: false,
    );
    if (recordEntity == null) return null;

    return await _showEntityDialog(context, ref, schemata, recordEntity, false);
  }

  Future<void> _edit(
    BuildContext context,
    WidgetRef ref,
    Schemata schemata,
    AppDefinition appDefinition,
    AppDefinitionSync controller,
  ) async {
    final recordEntity = RecordEntity.from(schemata, appDefinition);
    if (recordEntity == null) return;

    final isReadOnly = !appDefinition.isInner;
    await _showEntityDialog(context, ref, schemata, recordEntity, isReadOnly);

    if (!isReadOnly) controller.refresh();
  }

  Future<AppDefinition?> _showEntityDialog(
    BuildContext context,
    WidgetRef ref,
    Schemata schemata,
    RecordEntity recordEntity,
    bool isReadOnly,
  ) async {
    final e = await showDialog(
      context: context,
      builder: (context) => EntityDialog(
        account, EntityDialogSettings(),
        appDefinitionRegistry, recordEntity, isReadOnly, !isReadOnly,
      ),
    );
    if (e == null) return null;

    _appDefinitionRegistrySyncController.refresh();

    return e.entity.as<AppDefinition>();
  }

  Future<void> _importFromFriend(
    BuildContext context,
    WidgetRef ref,
    AppDefinitionRegistryState? appDefinitionRegistryValue,
  ) async {
    final friend = (
      await showFriendSelectDialog(context)
    )?[0];
    if (friend == null) return;
    if (!friend.isLoaded) await friend.syncSilently();

    final publicRoot = friend.publicRoot;
    if (publicRoot == null) return;

    final (imports, applied) = await showAppDefinitionSelectDialog(
      context, publicRoot,
      title: "Import app definitions".i18n,
      okButton: "Import".i18n,
      excludeIds: appDefinitionRegistryValue?.appDefinitions.where(
        (ad) => !ad.isInner,
      ).map(
        (ad) => ad.id,
      ),
      selectionMode: AsyncListSelectionMode.multiple,
      showApplyButton: true,
      onError: (e, s) => showError(context, e, s),
    );
    if (imports == null) return;

    for (final i in imports) {
      appDefinitionRegistry.addAppDefinition(i);
    }
    _appDefinitionRegistrySyncController.refresh();

    if (applied != null) _applyAppDefinition(context, ref, applied);
  }

  Future<bool> _applyAppDefinition(
    BuildContext context,
    WidgetRef ref,
    AppDefinition appDefinition, [
      String? activeAppDefinitionId,
  ]) async {
    if (appDefinition.id == activeAppDefinitionId) {
      return await clearAppDefinition(
        context, ref, root, provider: _activeAppDefinitionProvider,
      );
    }
    return await applyAppDefinition(
      context, ref, root, appDefinition, provider: _activeAppDefinitionProvider,
    );
  }
}
