import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText('en_us') +
      {
        'en_us': 'OK',
        'ja_jp': 'OK',
      } +
      {
        'en_us': 'Cancel',
        'ja_jp': 'キャンセル',
      } +
      {
        'en_us': 'Unregistered',
        'ja_jp': '未登録',
      } +
      {
        'en_us': 'change',
        'ja_jp': '変更',
      } +
      {
        'en_us': 'Please enter your API KEY',
        'ja_jp': 'API KEYを入力してください',
      } +
      {
        'en_us': 'execution',
        'ja_jp': '実行',
      };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
