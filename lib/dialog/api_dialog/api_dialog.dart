import 'package:flutter/material.dart';
import 'package:personaryFlutter/dialog/api_dialog/api_dialog.i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> apiDialog(BuildContext context, prefsApiKey) async {
  bool ret = false;
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final textEditingController = TextEditingController();

  await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text("API KEY: ${prefs.getString(prefsApiKey) == null || prefs.getString(prefsApiKey) == "" ? 'Unregistered'.i18n : prefs.getString(prefsApiKey)}"),
          actions: [
            TextButton(
              child: Text('change'.i18n),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Please enter your API KEY'.i18n),
                        content: TextField(obscureText: false, controller: textEditingController,),
                        actions: [
                          TextButton(
                            child: Text('Cancel'.i18n),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          TextButton(
                            child: Text('OK'.i18n),
                            onPressed: () {
                              ret = true;
                              prefs.setString(prefsApiKey, textEditingController.text.trim());
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    }
                );
                if (ret) Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('execution'.i18n),
              onPressed: prefs.getString(prefsApiKey) == null || prefs.getString(prefsApiKey) == "" ? null : () {
                ret = true;
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      }
  );
  return ret;
}