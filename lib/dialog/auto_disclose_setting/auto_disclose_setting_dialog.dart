import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../style.dart';
import '../../util.dart';
import '../../widget.dart';
import '../../page/timeline/timeline_item.dart';
import 'auto_disclose_setting_dialog.i18n.dart';

enum _ConditionType {
  Channel, Node;
  String get label => toString().split(".")[1].i18n;
}

class AutoDiscloseSettingDialog extends StatelessConsumerPlrWidget {
  AutoDiscloseSettingDialog({ super.key });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    final DisclosureCondition? disclosureCondition; {
      final snapshot = useFuture(
        useMemoized(() => storage.disclosureCondition),
      );
      disclosureCondition = snapshot.data;

      if (disclosureCondition == null) {
        if (snapshot.error == null) {
          return const Center(child: const CircularProgressIndicator());
        }
        Future.microtask(() {
            showError(context, snapshot.error!, snapshot.stackTrace);
            Navigator.pop(context);
        });
        return emptyPage;
      }
    }
    return SafeArea(
      child: _buildSchemata(context, ref, disclosureCondition),
    );
  }

  Widget _buildSchemata(
    BuildContext rootContext,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
  ) => HookConsumer(
    builder: (context, ref, _) {
      final Schemata? schemata; {
        final snapshot = useStream(
          useMemoized(() => disclosureCondition.timelineSchemata),
        );
        schemata = snapshot.data;

        if (schemata == null) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const Center(child: const CircularProgressIndicator());
          }
          Future.microtask(() {
              showMessage(
                rootContext, "Failed to get the disclosure condition schemata.",
              );
              Navigator.pop(context);
          });
          return emptyPage;
        }
      }
      return _buildPage(context, ref, disclosureCondition, schemata);
    },
  );

  Widget _buildPage(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
  ) => Scaffold(
    appBar: _buildAppBar(context, ref, disclosureCondition, schemata),
    body: RefreshIndicator(
      onRefresh: () => ref.read(
        disclosureConditionSyncProvider(disclosureCondition).notifier,
      ).refresh(),
      child: Consumer(
        builder: (context, ref, _) {
          ref.listen(
            disclosureConditionSyncProvider(disclosureCondition),
            (_, next) => processError(context, next),
          );

          final state = ref.watch(
            disclosureConditionSyncProvider(disclosureCondition),
          );
          final value = state.value;

          var channelDisclosureConditions =
            disclosureCondition.channelDisclosureConditions;
          var itemDisclosureConditions =
            disclosureCondition.itemDisclosureConditions;

          if (
            (value == null) || (
              value.channelDisclosureConditions.isEmpty &&
              value.itemDisclosureConditions.isEmpty
            )
          ) {
            if (state.isLoading) {
              return const Center(child: const CircularProgressIndicator());
            }
            return PageScrollView(
              child: Center(
                child: Text("No disclosure conditions.".i18n),
              ),
            );
          }

          return PageScrollView(
            child: Container(
              padding: allPadding[3],
              child: _buildList(
                context, ref, disclosureCondition, schemata,
                channelDisclosureConditions, itemDisclosureConditions,
              ),
            ),
          );
        },
      ),
    ),
  );

  AppBar _buildAppBar(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
  ) => AppBar(
    title: Text("Auto disclosure conditions".i18n),
    actions: [
      PopupMenuButton(
        position: PopupMenuPosition.under,
        itemBuilder: (context) => _ConditionType.values.map(
          (t) => PopupMenuItem(child: Text(t.label), value: t),
        ).toList(),
        onSelected: (dynamic value) {
          switch (value) {
            case _ConditionType.Channel: _addChannelCondition(
              context, ref, disclosureCondition, schemata,
            ); break;
            case _ConditionType.Node: _addNodeCondition(
              context, ref, disclosureCondition, schemata,
            ); break;
          }
        },
        child: Container(
          width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
          child: FloatingActionButton(
            heroTag: "hero_add_disclosure_condition",
            tooltip: "Add disclosure condition.".i18n,
            child: const Icon(Icons.add),
            onPressed: null,
          ),
        ),
      ),
    ],
  );

  final List<Widget> _bgPair = dismissibleBackgroundPairWith(
    Colors.red, const Icon(Icons.delete, color: Colors.white),
    Text("Remove".i18n, style: const TextStyle(color: Colors.white)),
  );

  Widget _buildList(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
    Iterable<Item> channelDisclosureConditions,
    Iterable<Item> itemDisclosureConditions,
  ) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      if (channelDisclosureConditions.isNotEmpty)
        _buildDisclosureConditionList(
          context, ref, disclosureCondition, schemata,
          _ConditionType.Channel, channelDisclosureConditions,
        ),
      if (itemDisclosureConditions.isNotEmpty)
        _buildDisclosureConditionList(
          context, ref, disclosureCondition, schemata,
          _ConditionType.Node, itemDisclosureConditions,
        ),
    ],
  );

  Widget _buildDisclosureConditionList(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
    _ConditionType conditionType,
    Iterable<Item> disclosureConditions,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SectionHeader(conditionType.label),
        ...disclosureConditions.map((i) {
            var recordEntity = RecordEntity.from(schemata, i);
            if (recordEntity == null) return Container();
            return Dismissible(
              key: ValueKey(i.id),
              background: _bgPair[0],
              secondaryBackground: _bgPair[1],
              confirmDismiss: (_) => _remove(
                context, disclosureCondition, recordEntity,
              ),
              onDismissed: (_) => ref.read(
                disclosureConditionSyncProvider(disclosureCondition).notifier,
              ).refresh(),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: StatefulBuilder(
                    builder: (_, setState) {
                      var callback = (_) => setState(() {});
                      return TimelineItem(
                        account, recordEntity, false,
                        disclosureCondition, root.plrId.toString(), false,
                        schemata, callback, (_) {}, callback, contentOnly: true,
                      );
                    },
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }

  Future<Item?> _addChannelCondition(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
  ) => _addCondition(
    context, ref, disclosureCondition, schemata,
    channelDisclosureConditionProperty,
  );

  Future<Item?> _addNodeCondition(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
  ) => _addCondition(
    context, ref, disclosureCondition, schemata,
    itemDisclosureConditionProperty,
  );

  Future<Item?> _addCondition(
    BuildContext context,
    WidgetRef ref,
    DisclosureCondition disclosureCondition,
    Schemata schemata,
    String propertyId,
  ) async {
    var p = schemata.propertyOf(propertyId);
    if (p?.firstRange == null) return null;

    var recordEntity = RecordEntity.create(
      schemata, p!.firstRange!.id, referer: p,
    );
    if (recordEntity == null) return null;

    var e = await showDialog(
      context: context,
      builder: (context) => EntityDialog(
        account, EntityDialogSettings(), disclosureCondition, recordEntity, false, true,
      ),
    );
    if (e == null) return null;

    ref.read(
      disclosureConditionSyncProvider(disclosureCondition).notifier,
    ).refresh();

    return e.entity.as<Item>();
  }

  Future<bool> _remove(
    BuildContext context,
    DisclosureCondition disclosureCondition,
    RecordEntity recordEntity,
  ) async {
    if (!await showConfirmDialog(
        context, "Remove the disclosure condition".i18n,
        "Are you sure to remove the disclosure condition?".i18n,
    )) return false;

    await runWithProgress(
      context, () async {
        recordEntity.delete();
        await recordEntity.save(disclosureCondition, false);
      },
    );
    return true;
  }
}
