import "package:i18n_extension/i18n_extension.dart";

import 'package:plr_util/src/util/base_translations.dart' show meTranslation;

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  meTranslation +
  {
    "en_us": "Auto disclosure conditions",
    "ja_jp": "自動開示条件",
  } +
  {
    "en_us": "No disclosure conditions.",
    "ja_jp": "開示条件がありません",
  } +
  {
    "en_us": "Add disclosure condition.",
    "ja_jp": "開示条件を追加",
  } +
  {
    "en_us": "Channel",
    "ja_jp": "チャネル",
  } +
  {
    "en_us": "Node",
    "ja_jp": "ノード",
  } +
  {
    "en_us": "Remove",
    "ja_jp": "削除",
  } +
  {
    "en_us": "Remove the disclosure condition",
    "ja_jp": "開示条件の削除",
  } +
  {
    "en_us": "Are you sure to remove the disclosure condition?",
    "ja_jp": "この開示条件を削除してよろしいですか?",
  };

  String get i18n => localize(this, t);
}
