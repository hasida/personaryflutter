import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import 'info_channel_setting_dialog.i18n.dart';

class InfoChannelSettingDialog extends StatelessConsumerPlrWidget {
  InfoChannelSettingDialog({ super.key });

  late final PublicRootSyncProvider _provider;
  late final PublicRootSync _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _provider = publicRootSyncProvider(publicRoot);
    _controller = ref.read(_provider.notifier);
  }

  Future<void> _refresh([ bool force = false ]) =>
    _controller.refresh(force: force);

  final List<Widget> _bgPair = dismissibleBackgroundPairWith(
    Colors.red, const Icon(Icons.public_off, color: Colors.white),
    Text("Release".i18n, style: const TextStyle(color: Colors.white)),
  );

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: Scaffold(
      appBar: _buildAppBar(context, ref),
      body: RefreshIndicator(
        onRefresh: () => _refresh(),
        child: _buildList(context, ref),
      ),
    ),
  );

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Text("Info channel".i18n),
    actions: [
      Container(
        width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
        child: FloatingActionButton(
          heroTag: "hero_add_info_channel",
          tooltip: "Add info channel.".i18n,
          child: const Icon(Icons.add),
          onPressed: () => _add(context, ref),
        ),
      ),
    ],
  );

  Widget _buildList(BuildContext context, WidgetRef ref) => ChannelList<
    InfoChannel, PublicRootSyncProvider, PublicRootSync, PublicRootState
  >(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    provider: _provider,
    container: publicRoot,
    itemsGetter: (value) => value.infoChannels,
    cellConfig: channelCellConfigWith<InfoChannel>(
      context, ref, _controller,
      useDefaultButtons: false, useDefaultDismissibleConfig: false,
    ).copyWith(
      buttonBuilders: [
        EntityButtonDef<InfoChannel, ChannelUpdate>(
          const Icon(Icons.timer), tooltip: "Set validity term.".i18n,
          onPressed: (channel, _) => _edit(context, channel),
        ).toBuilder(),
      ],
      dismissibleConfig: channelDismissibleConfigWithDefault(
        canDismiss: (_, __, ___, ____, _____) => true,
        keyBuilder: (channel, _, __) => ValueKey(channel.id),
        background: _bgPair[0], secondaryBackground: _bgPair[1],
        confirm: _confirm,
        objectRemover: _remove,
        onDismissed: (_) => _refresh(),
        onError: (e, s) => showError(context, e, s),
      ),
    ),
    empty: Text("No info channels.".i18n),
    onError: (e, s) => showError(context, e, s),
  );

  Future<Duration?> _showMaxValidityTermDialog(
    BuildContext context, [
      Duration initialDuration = Duration.zero,
  ]) => showDurationPickerDialog(
    context, title: "Validity term".i18n,
    initialDuration: initialDuration,
  );

  Future<InfoChannel?> _add(BuildContext context, WidgetRef ref) async {
    var channel = (
      await showChannelSelectDialog(
        context, root,
        excludes: ref.read(_provider).value?.infoChannels,
        onError: (e, s) => showError(context, e, s),
      )
    )?.first;
    if (channel == null) return null;

    var maxValidityTerm = await _showMaxValidityTermDialog(context);
    if (maxValidityTerm == null) return null;

    var infoChannel = publicRoot.addInfoChannel(channel, maxValidityTerm);
    await _refresh();

    return infoChannel;
  }

  Future<bool> _edit(BuildContext context, InfoChannel channel) async {
    var maxValidityTerm = await _showMaxValidityTermDialog(
      context, channel.maxValidityTerm,
    );
    if (
      (maxValidityTerm == null) ||
      !channel.setMaxValidatedTerm(maxValidityTerm)
    ) return false;

    await publicRoot.syncSilently();
    return true;
  }

  Future<bool> _confirm(
    BuildContext context,
    WidgetRef ref,
    InfoChannel channel,
    Entity? referrer,
    AsyncValue<ChannelUpdateState?> state,
    DismissDirection direction,
  ) => showConfirmDialog(
    context, "Release the info channel".i18n,
    "Are you sure to release the info channel setting?".i18n,
  );

  Future<bool> _remove(
    BuildContext context,
    WidgetRef ref,
    PlrAsyncNotifier<ChannelUpdateState?> controller,
    InfoChannel channel,
    Entity? referrer,
    AsyncValue<ChannelUpdateState?> state,
  ) async => publicRoot.removeInfoChannel(channel);
}
