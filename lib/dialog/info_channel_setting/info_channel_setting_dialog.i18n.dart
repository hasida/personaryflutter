import "package:i18n_extension/i18n_extension.dart";

import 'package:plr_util/src/util/base_translations.dart' show meTranslation;

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  meTranslation +
  {
    "en_us": "Info channel",
    "ja_jp": "広報チャネル",
  } +
  {
    "en_us": "No info channels.",
    "ja_jp": "広報チャネルがありません",
  } +
  {
    "en_us": "Add info channel.",
    "ja_jp": "広報チャネルを追加",
  } +
  {
    "en_us": "Channel to add",
    "ja_jp": "追加するチャネル",
  } +
  {
    "en_us": "Set validity term.",
    "ja_jp": "有効期限を設定",
  } +
  {
    "en_us": "Validity term",
    "ja_jp": "有効期限",
  } +
  {
    "en_us": "Release",
    "ja_jp": "解除",
  } +
  {
    "en_us": "Release the info channel",
    "ja_jp": "広報チャネルの解除",
  } +
  {
    "en_us": "Are you sure to release the info channel setting?",
    "ja_jp": "この広報チャネルを解除してよろしいですか?",
  };

  String get i18n => localize(this, t);
}
