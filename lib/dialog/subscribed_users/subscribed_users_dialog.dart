import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import '../../../page/friend/friend_page.dart';
import 'subscribed_users_dialog.i18n.dart';

final List<Widget> _bgPair = dismissibleBackgroundPairWith(
  Colors.red, const Icon(Icons.person_off, color: Colors.white),
  Text("Release".i18n, style: const TextStyle(color: Colors.white)),
);

class SubscribedUsersDialog extends StatelessConsumerPlrWidget {
  SubscribedUsersDialog();

  late final InfoRegistrySyncProvider _provider;
  late final InfoRegistrySync _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _provider = infoRegistrySyncProvider(
      infoRegistry, processMatchedEntities: false,
    );
    _controller = ref.read(_provider.notifier);
  }

  Future<void> _refresh([
      bool force = false,
  ]) => _controller.refresh(force: force);

  final Map<PlrId, SubscribedUser> _addedUsers = {};

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    ref.listen(_provider, (_, next) => processError(context, next));

    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(context, ref),
        body: PopScope(
          canPop: false,
          onPopInvokedWithResult: (didPop, _) {
            if (!didPop) Navigator.pop(context, _addedUsers.values);
          },
          child: RefreshIndicator(
            onRefresh: () => _refresh(),
            child: _buildSubscribedUserList(context, ref),
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Text("Subscribed users".i18n),
    actions: [
      Container(
        width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
        child: FloatingActionButton(
          heroTag: "hero_add_subscribed_user",
          tooltip: "Add subscribed user.".i18n,
          child: const Icon(Icons.person_add),
          onPressed: () => _add(context, ref),
        ),
      ),
    ],
  );

  Widget _buildSubscribedUserList(
    BuildContext context,
    WidgetRef ref,
  ) => UserList<
    SubscribedUser,
    InfoRegistrySyncProvider, InfoRegistrySync, InfoRegistryState
  >(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    provider: _provider,
    cellConfig: userCellConfigWithDefault(
      onTap: (f, _) => openFriendOrUserPage(context, account, f),
      dismissibleConfig: DismissibleConfig(
        keyBuilder: (user, _, __) => ValueKey(user.id),
        background: _bgPair[0],
        secondaryBackground: _bgPair[1],
        confirm: _confirm,
        objectRemover: _remove,
        onDismissed: (_) => _refresh(),
        onError: (e, s) => showError(context, e, s),
      ),
      onError: (e, s) => showError(context, e, s),
    ),
    empty: Text("No subscribed users.".i18n),
    onError: (e, s) => showError(context, e, s),
  );

  Future<SubscribedUser?> _add(BuildContext context, WidgetRef ref) async {
    var friend = (
      await showFriendSelectDialog(
        context, title: "Friends to add".i18n,
        empty: "No friends to subscribe.".i18n,
        excludeIds: ref.read(_provider).value
          ?.subscribedUsers.map((u) => u.id).nonNulls,
        onError: (e, s) => showError(context, e, s),
      )
    )?.first;
    if (friend == null) return null;

    var user = await runWithProgress(
      context, () async {
        if (!friend.isLoaded) await friend.syncSilently();
        var publicRoot = friend.publicRoot;
        if (publicRoot == null) return null;

        return infoRegistry.addSubscribedUser(publicRoot);
      },
      onError: (e, s) => showError(context, e, s),
    );
    if (user?.plrId == null) return null;
    _addedUsers[user!.plrId!] = user;

    await _refresh();

    return user;
  }

  Future<bool> _confirm(
    BuildContext context,
    WidgetRef ref,
    SubscribedUser user,
    Entity? referrer,
    AsyncValue<UserInfo?> state,
    DismissDirection direction,
  ) => showConfirmDialog(
    context, "Release the subscribed user".i18n,
    "Are you sure to release the subscribed user?".i18n,
  );

  Future<bool> _remove(
    BuildContext context,
    WidgetRef ref,
    UserInfoUpdate controller,
    SubscribedUser user,
    Entity? referrer,
    AsyncValue<UserInfo?> state,
  ) async {
    if (!infoRegistry.removeSubscribedUser(user)) return false;

    _addedUsers.remove(user.plrId);
    return true;
  }
}
