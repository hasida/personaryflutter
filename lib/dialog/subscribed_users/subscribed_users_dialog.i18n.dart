import "package:i18n_extension/i18n_extension.dart";

import 'package:plr_util/src/util/base_translations.dart' show meTranslation;

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  meTranslation +
  {
    "en_us": "Subscribed users",
    "ja_jp": "購読対象ユーザ",
  } +
  {
    "en_us": "No subscribed users.",
    "ja_jp": "購読対象ユーザはいません",
  } +
  {
    "en_us": "Add subscribed user.",
    "ja_jp": "購読対象ユーザを追加",
  } +
  {
    "en_us": "Friends to add",
    "ja_jp": "追加する友達",
  } +
  {
    "en_us": "No friends to subscribe.",
    "ja_jp": "購読対象となる友達はいません",
  } +
  {
    "en_us": "Release",
    "ja_jp": "解除",
  } +
  {
    "en_us": "Release the subscribed user",
    "ja_jp": "購読対象ユーザの解除",
  } +
  {
    "en_us": "Are you sure to release the subscribed user?",
    "ja_jp": "この購読対象ユーザを解除してよろしいですか?",
  };

  String get i18n => localize(this, t);
}
