import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "PLR passphrase deposits",
    "ja_jp": "PLRパスフレーズ預け先"
  } +
  {
    "en_us":
      "The passphrase deposits are being prepared.\nPlease wait for a while.",
    "ja_jp": "パスフレーズ預け先を初期化中です。\nしばらくお待ちください。"
  } +
  {
    "en_us": "Add fiduciaries",
    "ja_jp": "預け先を追加"
  } +
  {
    "en_us": "No fiduciaries.",
    "ja_jp": "預け先がありません"
  } +
  {
    "en_us": "Select fiduciaries.",
    "ja_jp": "預け先の選択"
  } +
  {
    "en_us": "Release",
    "ja_jp": "解除",
  } +
  {
    "en_us": "Release the fiduciary",
    "ja_jp": "預け先の解除",
  } +
  {
    "en_us": "Are you sure to release this passphrase deposit?",
    "ja_jp": "このパスフレーズ預け先を解除してよろしいですか?",
  };

  String get i18n => localize(this, t);
}
