import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import '../../page/friend/friend_page.dart';
import 'passphrase_depositing_dialog.i18n.dart';

final List<Widget> _bgPair = dismissibleBackgroundPairWith(
  Colors.red, const Icon(Icons.person_off, color: Colors.white),
  Text("Release".i18n, style: const TextStyle(color: Colors.white)),
);

class PassphraseDepositingDialog extends StatelessConsumerPlrWidget {
  const PassphraseDepositingDialog();

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    final depositariesState = useState<List<Friend>?>(null);
    final depositaries = depositariesState.value;

    final refreshCountState = useState(0);
    final refreshCount = refreshCountState.value;

    Future<void> refresh([ bool force = false ]) async {
      if (force) refreshCountState.value++;
      depositariesState.value = await passphraseDepositaryFriendsOf(storage);
    }

    final isPassphraseDepositaryInitializedFuture = useFuture(
      useMemoized(
        () => storage.isPassphraseDepositaryInitialized, [ refreshCount ],
      ), initialData: false,
    );
    final isPassphraseDepositaryInitialized =
      isPassphraseDepositaryInitializedFuture.data!;

    return SafeArea(
      child: ProgressHUD(
        child: Builder(
          builder: (context) => Scaffold(
            appBar: _buildAppBar(context, depositaries, refresh),
            body: isPassphraseDepositaryInitialized
              ? _buildBody(depositaries, refresh, refreshCount)
              : _buildBeingPreparedBody(refreshCountState),
            ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(
    BuildContext context,
    List<Friend>? depositaries,
    Future<void> refresh([ bool force ]),
  ) => AppBar(
    title: Text("PLR passphrase deposits".i18n),
    actions: [
      Container(
        width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
        child: FloatingActionButton(
          heroTag: "hero_add_depositories",
          tooltip: "Add fiduciaries".i18n,
          backgroundColor: Theme.of(context).colorScheme.secondary,
          child: const Icon(Icons.person_add, color: Colors.white),
          onPressed: (depositaries != null)
            ? () => _add(context, depositaries, refresh)
            : null,
        ),
      ),
    ],
  );

  Widget _buildBeingPreparedBody(
    ValueNotifier<int> refreshCountState,
  ) => TimerShownDetector(
    onTimer: () => refreshCountState.value++,
    child: PageScrollView(
      child: Center(
        child: Text(
          "The passphrase deposits are being prepared.\n"
          "Please wait for a while.".i18n,
           textAlign: TextAlign.center,
        ),
      ),
    ),
  );

  Widget _buildBody(
    List<Friend>? depositaries,
    Future<void> refresh([ bool force ]),
    int refreshCount,
  ) => RefreshIndicator(
    onRefresh: () => refresh(true),
    child: TimerShownDetector(
      onInit: refresh,
      onTimer: refresh,
      child: Builder(
        builder: (context) {
          final progress = ProgressHUD.of(context)!;

          if (depositaries == null) {
            Future.microtask(progress.show);
            return emptyPage;
          }
          Future.microtask(progress.dismiss);

          if (depositaries.isEmpty) return PageScrollView(
            child: Center(child: Text("No fiduciaries.".i18n)),
          );
          return _buildList(
            context, depositaries, refresh, refreshCount,
          );
        },
      ),
    ),
  );

  Widget _buildList(
    BuildContext context,
    List<Friend> depositaries,
    Future<void> refresh([ bool force ]),
    int refreshCount,
  ) => ListView.builder(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    itemCount: depositaries.length,
    itemBuilder: (context, index) {
      var friend = depositaries[index];

      return Dismissible(
        key: ValueKey("${friend.id}${refreshCount}"),
        background: _bgPair[0],
        secondaryBackground: _bgPair[1],
        confirmDismiss: (_) => _remove(context, friend),
        onDismissed: (_) => refresh(),
        child: UserCell<Friend>(
          object: friend,
          config: userCellConfigWithDefault(
            onTap: (friend, _) => Navigator.push(
              context, CupertinoPageRoute(
                builder: (_) => FriendPage(friend),
              ),
            ).then((_) => refresh()),
            onError: (e, s) => showError(context, e, s),
          ),
        ),
      );
    },
  );

  Future<void> _add(
    BuildContext context,
    List<Friend> depositaries,
    Future<void> refresh([ bool force ]),
  ) async {
    var selected = await showFriendSelectDialog(
      context,
      title: "Select fiduciaries.".i18n,
      empty: "No fiduciaries.".i18n,
      excludes: depositaries,
      selectionMode: AsyncListSelectionMode.multiple,
    );
    if (selected == null) return;

    await runWithProgress(
      context, () async {
        var passphrase = await passphraseOf(root);
        if (passphrase == null) {
          throw "No passphrase registered in your account.";
        }
        await Future.wait(
          selected.map((f) async {
              if (!f.isLoaded) await f.syncSilently();

              var m2fr = f.meToFriendRoot;
              if (m2fr?.setPassphrase(passphrase) == true) {
                await m2fr!.syncSilently();
              }
          }).toList(),
        );
        return true;
      },
      onError: (e, s) => showError(context, e, s), errorValue: false,
      onFinish: (r) {
        if (r == true) refresh();
      },
    );
  }

  Future<bool> _remove(BuildContext context, Friend friend) async {
    if (!await showConfirmDialog(
        context, "Release the fiduciary".i18n,
        "Are you sure to release this passphrase deposit?".i18n,
    )) return false;

    await runWithProgress(
      context, () async {
        if (!friend.isLoaded) await friend.syncSilently();

        final m2fr = friend.meToFriendRoot;
        if (m2fr == null) return false;

        if (m2fr.setFakePassphrase()) {
          await m2fr.syncSilently();
        }
      },
    );
    return true;
  }
}
