import 'package:intl/intl.dart';
import 'package:personaryFlutter/util/update_list.dart';
import 'package:plr_ui/plr_ui.dart';

String get currentLanguage => (Intl.defaultLocale?.split(".") ?? [ noLang ])[0];

class DummyModel extends Model {
  String? id;
}

class DummySchema extends DummyModel {
  Map<String, dynamic>? titleMap;
  Map<String, dynamic>? descriptionMap;

  TimelineState? timelineState;

  List<OssFile> ontologyFiles;
  List<OssFile> restrictionFiles;
  List<OssFile> constraintFiles;
  List<OssFile> timelineSSFiles;
  List<OssFile> groupSSFiles;
  List<OssFile> channelSSFiles;

  DummySchema({
      this.titleMap,
      this.descriptionMap,
      this.timelineState,
      this.ontologyFiles = const [],
      this.restrictionFiles = const [],
      this.constraintFiles = const [],
      this.timelineSSFiles = const [],
      this.groupSSFiles = const [],
      this.channelSSFiles = const [],
  });

  DummySchema.fromSchema(SchemaBase schema): this(
    titleMap: schema.name?.valueMap,
    descriptionMap: schema.description?.valueMap,
    timelineState: schema.timelineState,
    ontologyFiles: schema.ontologyFiles.toList(),
    restrictionFiles: schema.restrictionFiles.toList(),
    constraintFiles: schema.constraintFiles.toList(),
    timelineSSFiles: schema.timelineSSFiles.toList(),
    groupSSFiles: schema.groupSSFiles.toList(),
    channelSSFiles: schema.channelSSFiles.toList(),
  );

  String? get defaultTitle => valueFor(titleMap, currentLanguage);
  String? get defaultDescription => valueFor(descriptionMap, currentLanguage);

  String? valueFor(Map<String, dynamic>? i18nMap, [ String? lang ]) {
    if (i18nMap == null) return null;

    if (lang != null) {
      var l = lang;
      while (l.isNotEmpty) {
        var v = i18nMap[l];
        if (v != null) return v.toString();

        l = (l.split("_")..removeLast()).join("_");
      }
    }
    var v = i18nMap[noLang];
    if (v != null) return v.toString();
    return null;
  }

  void update(
    TimelineState? ts,
    Iterable<OssFile> o,
    Iterable<OssFile> r,
    Iterable<OssFile> co,
    Iterable<OssFile> t,
    Iterable<OssFile> c,
    Iterable<OssFile> s,
  ) {
    timelineState = ts;

    ontologyFiles    = o.toList();
    restrictionFiles = r.toList();
    constraintFiles = co.toList();
    timelineSSFiles  = t.toList();
    groupSSFiles   = c.toList();
    channelSSFiles = s.toList();
  }

  bool updateSchema(SchemaBase dest) {
    bool updated = false;

    updated |= dest.setTimelineState(timelineState);

    updated |= updateList<OssFile, OssFile>(
      dest.ontologyFiles,
      ontologyFiles,
      equals: (a, b) => a.id == b.id,
      add: (b) => dest.addOntologyFile(b),
      remove: (a) => dest.removeOntologyFile(a) != null,
    );
    updated |= updateList<OssFile, OssFile>(
      dest.restrictionFiles,
      restrictionFiles,
      equals: (a, b) => a.id == b.id,
      add: (b) => dest.addRestrictionFile(b),
      remove: (a) => dest.removeRestrictionFile(a) != null,
    );
    updated |= updateList<OssFile, OssFile>(
      dest.constraintFiles,
      constraintFiles,
      equals: (a, b) => a.id == b.id,
      add: (b) => dest.addConstraintFile(b),
      remove: (a) => dest.removeConstraintFile(a) != null,
    );
    updated |= updateList<OssFile, OssFile>(
      dest.timelineSSFiles,
      timelineSSFiles,
      equals: (a, b) => a.id == b.id,
      add: (b) => dest.addTimelineSSFile(b),
      remove: (a) => dest.removeTimelineSSFile(a) != null,
    );
    updated |= updateList<OssFile, OssFile>(
      dest.groupSSFiles,
      groupSSFiles,
      equals: (a, b) => a.id == b.id,
      add: (b) => dest.addGroupSSFile(b),
      remove: (a) => dest.removeGroupSSFile(a) != null,
    );
    updated |= updateList<OssFile, OssFile>(
      dest.channelSSFiles,
      channelSSFiles,
      equals: (a, b) => a.id == b.id,
      add: (b) => dest.addChannelSSFile(b),
      remove: (a) => dest.removeChannelSSFile(a) != null,
    );

    return updated;
  }
}

class DummyInternalSchema extends DummySchema {
  final String? type;

  DummyInternalSchema(this.type);

  DummyInternalSchema.fromSchema(Schema schema):
    type = schema.type, super.fromSchema(schema);

  @override
  Map<String, dynamic>? get titleMap =>
    super.titleMap ?? internalSchemaNames[type];

  @override
  Map<String, dynamic>? get descriptionMap =>
    super.descriptionMap ?? internalSchemaDescriptions[type];
}

void updateModelList<T extends Model, U extends Model>(
    Iterable<T> src, Iterable<U> old,
    void add(T f), void remove(U f)
    ) {
  var removeList = <U>[];
  for (var f in old) {
    bool remain = false;
    for (var n in src) {
      if (f.id == n.id) {
        remain = true;
        break;
      }
    }
    if (!remain) {
      removeList.add(f);
    }
  }
  for (var f in removeList) {
    remove(f);
  }
  for (var f in src) {
    bool exist = false;
    for (var o in old) {
      if (f.id == o.id) {
        exist = true;
        break;
      }
    }
    if (!exist) {
      add(f);
    }
  }
}

class SubDataSettingConfigCache {
  final DataSetting dataSetting;
  late bool isChannelAutoGeneration;
  late bool isChannelManualGeneration;
  late bool isAutomaticDisclosureToSection;
  late bool isAddExistingChannelToSection;
  late bool isPublishChannelSetting;
  SubDataSettingConfigCache(this.dataSetting) {
    if (dataSetting is SubDataSetting) {
      isChannelAutoGeneration = (dataSetting as SubDataSetting).isChannelAutoGeneration;
      isChannelManualGeneration = (dataSetting as SubDataSetting).isChannelManualGeneration;
      isAutomaticDisclosureToSection = (dataSetting as SubDataSetting).isAutomaticDisclosureToSection;
      isAddExistingChannelToSection = (dataSetting as SubDataSetting).isAddExistingChannelToSection;
      isPublishChannelSetting = (dataSetting as SubDataSetting).isPublishChannelSetting;
    }
    else {
      isChannelAutoGeneration = true;
      isChannelManualGeneration = true;
      isAutomaticDisclosureToSection = false;
      isAddExistingChannelToSection = true;
      isPublishChannelSetting = false;
    }
  }

  bool apply(DataSetting parent) {
    if (dataSetting is SubDataSetting) {
        bool isChanged = false;
        final subDataSetting = dataSetting as SubDataSetting;
        if (subDataSetting.isChannelAutoGeneration != isChannelAutoGeneration) {
          isChanged = true;
          subDataSetting.isChannelAutoGeneration = isChannelAutoGeneration;
        }
        if (subDataSetting.isChannelManualGeneration != isChannelManualGeneration) {
          isChanged = true;
          subDataSetting.isChannelManualGeneration = isChannelManualGeneration;
        }
        if (subDataSetting.isAutomaticDisclosureToSection != isAutomaticDisclosureToSection) {
          isChanged = true;
          subDataSetting.isAutomaticDisclosureToSection = isAutomaticDisclosureToSection;
        }
        if (subDataSetting.isAddExistingChannelToSection != isAddExistingChannelToSection) {
          isChanged = true;
          subDataSetting.isAddExistingChannelToSection = isAddExistingChannelToSection;
        }
        if (subDataSetting.isPublishChannelSetting != isPublishChannelSetting) {
          isChanged = true;
          subDataSetting.isPublishChannelSetting = isPublishChannelSetting;
        }
        return isChanged;
      } else {
        parent.addSubDataSetting(dataSetting,
            isAutomaticDisclosureToSection: isAutomaticDisclosureToSection,
            isChannelAutoGeneration: isChannelAutoGeneration,
            isChannelManualGeneration: isChannelManualGeneration,
            isPublishChannelSetting: isPublishChannelSetting,
            isAddExistingChannelToSection: isAddExistingChannelToSection,
        );
        return true;
      }
  }

  SubDataSettingConfigCache clone() {
    return SubDataSettingConfigCache(dataSetting,)
        ..isChannelAutoGeneration = isChannelAutoGeneration
        ..isChannelManualGeneration = isChannelManualGeneration
        ..isAutomaticDisclosureToSection = isAutomaticDisclosureToSection
        ..isPublishChannelSetting = isPublishChannelSetting
        ..isAddExistingChannelToSection = isAddExistingChannelToSection;
  }
}

class ProfileConditionData {
  SchemaClass schemaClass;
  String? title;

  String? get label => schemaClass.label?.defaultValue;
  String? get propertyId {
    final id = schemaClass.id;
    if (id.isEmpty) return id;

    return id[0].toLowerCase() + id.substring(1);
  }

  ProfileConditionData(this.schemaClass, this.title);

  static final _titlePattern = RegExp(r'^title=(.*)$');

  static ProfileConditionData? from(
      Condition cond, {required Schemata schemata}) {
    if (cond.requirements.length != 1) return null;

    final propId = cond.property;
    if (propId.isEmpty) return null;

    final clazz = '${propId[0].toUpperCase()}${propId.substring(1)}';

    final schemaClass = schemata.classOf(clazz);
    if (schemaClass == null) return null;

    final m = _titlePattern.firstMatch(cond.requirements.first.toString());
    var title = m?.group(1);
    if (title == null) return null;

    return ProfileConditionData(schemaClass, title);
  }

  @override
  String toString() => '${schemaClass.id}|$title';

  bool isMatch(Item item) => (
    item.type == schemaClass.id &&
    item.firstLiteralValueOf(titleProperty) == title
  );

  bool equalToCondition(Condition other) {
    if (propertyId == other.property && other.requirements.length == 1) {
      final req = other.requirements.first.toString();
      final m = _titlePattern.firstMatch(req);
      if (m != null && this.title == m.group(1)) {
        return true;
      }
    }
    return false;
  }
}