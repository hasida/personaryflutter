import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      {
        'en_us': 'Create channel',
        'ja_jp': 'チャネルを作成',
      } +
      {
        'en_us': 'Channel info',
        'ja_jp': 'チャネル情報',
      } +
      {
        'en_us': 'Name',
        'ja_jp': '名前',
      } +
      {
        'en_us': 'Description',
        'ja_jp': '説明',
      } +
      {
        'en_us': 'Timeline',
        'ja_jp': 'タイムライン',
      } +
      {
        'en_us': 'Status',
        'ja_jp': '状態',
      } +
      {
        'en_us': 'Not set',
        'ja_jp': '未設定',
      } +
      {
        'en_us': 'It is set',
        'ja_jp': '設定済',
      } +
      {
        'en_us': 'Writable',
        'ja_jp': '編集可能',
      } +
      {
        'en_us': 'Readable',
        'ja_jp': '閲覧のみ',
      } +
      {
        'en_us': 'Disabled',
        'ja_jp': '無効',
      } +
      {
        'en_us': 'Channel setting',
        'ja_jp': 'チャネル設定',
      } +
      {
        'en_us': 'Create channel setting',
        'ja_jp': '新規チャネル設定を作成',
      } +
      {
        'en_us': 'Select channel setting',
        'ja_jp': '既存のチャネル設定を適用',
      } +
      {
        'en_us': 'Edit channel setting',
        'ja_jp': '現在のチャネル設定を開く',
      } +
      {
        'en_us': 'Channel setting is not set',
        'ja_jp': 'チャネル設定が適用されていません',
      } +
      {
        'en_us': 'Cancel',
        'ja_jp': 'キャンセル',
      } +
      {
        'en_us': 'OK',
        'ja_jp': 'OK',
      } +
      {
        'en_us': 'Referenced channel setting',
        'ja_jp': '参照チャネル設定',
      } +
      {
        'en_us': 'AI setting',
        'ja_jp': 'AI設定',
      } +
      {
        'en_us': 'Schema',
        'ja_jp': 'スキーマ',
      } +
      {
        'en_us': 'Create schema',
        'ja_jp': 'スキーマ作成',
      } +
      {
        'en_us': 'Common schema',
        'ja_jp': '共通スキーマ',
      } +
      {
        'en_us': 'Create new',
        'ja_jp': '新規作成',
      } +
      {
        'en_us': 'Initially created node type',
        'ja_jp': '初期生成項目クラス',
      } +
      {
        'en_us': 'OSS setting',
        'ja_jp': 'OSS設定',
      } +
      {
        'en_us': 'Friends reference',
        'ja_jp': '友達参照',
      } +
      {
        'en_us': 'Ontology',
        'ja_jp': 'オントロジー',
      } +
      {
        'en_us': 'Graph Ontology',
        'ja_jp': 'グラフ用オントロジー',
      } +
      {
        'en_us': 'Restriction',
        'ja_jp': 'オントロジーの限定',
      } +
      {
        'en_us': 'Constraint',
        'ja_jp': '制約',
      } +
      {
        'en_us': 'Timeline style sheet',
        'ja_jp': 'タイムラインのスタイルシート',
      } +
      {
        'en_us': 'Group summary style sheet',
        'ja_jp': 'グループサマリのスタイルシート',
      } +
      {
        'en_us': 'Channel summary style sheet',
        'ja_jp': 'チャネルサマリのスタイルシート',
      } +
      {
        'en_us': 'saving data',
        'ja_jp': 'データの保存中です',
      } +
      {
        'en_us': 'Error',
        'ja_jp': 'エラー',
      } +
      {
        'en_us': 'Please input channel name.',
        'ja_jp': 'チャネル名を入力してください',
      } +
      {
        'en_us': 'Please input channel setting name.',
        'ja_jp': 'チャネル設定名を入力してください',
      } +
      {
        'en_us': 'Embedded channel setting',
        'ja_jp': '埋め込みチャネル設定',
      } +
      {
        'en_us': 'Default sections',
        'ja_jp': 'セクション表示設定',
      } +
      {
        'en_us': 'Friend',
        'ja_jp': '友達',
      } +
      {
        'en_us': 'Channel',
        'ja_jp': 'チャネル',
      } +
      {
        'en_us': 'Disclosed channel setting',
        'ja_jp': '開示されているチャネル設定',
      } +
      {
        'en_us': 'Disclosed schema',
        'ja_jp': '開示されているスキーマ',
      } +
      {
        'en_us': 'Show',
        'ja_jp': '表示する',
      } +
      {
        'en_us': 'Hide',
        'ja_jp': '表示しない',
      } +
      {
        'en_us': 'Channel automatic generation',
        'ja_jp': '自動生成設定',
      } +
      {
        'en_us': 'Channel automatic disclosure',
        'ja_jp': 'チャネルの自動開示',
      } +
      {
        'en_us': 'Channel auto generation type',
        'ja_jp': '自動生成種別',
      } +
      {
        'en_us': 'Automatic generation of channel',
        'ja_jp': 'チャネルの自動生成',
      } +
      {
        'en_us': 'Single',
        'ja_jp': '単一',
      } +
      {
        'en_us': 'Individual',
        'ja_jp': '個別',
      } +
      {
        'en_us': 'Do not generate',
        'ja_jp': '生成しない',
      } +
      {
        'en_us': 'Disclose to other than publisher',
        'ja_jp': '公開者以外への開示',
      } +
      {
        'en_us': 'Allow',
        'ja_jp': '許可する',
      } +
      {
        'en_us': 'Deny',
        'ja_jp': '許可しない',
      } +
      {
        'en_us': 'Manual generation of channel',
        'ja_jp': 'チャネルの手動生成',
      } +
      {
        'en_us': 'Automatic addition of existing channel',
        'ja_jp': '既存チャネルの自動追加',
      } +
      {
        'en_us': 'Disclose in section',
        'ja_jp': 'セクションに開示',
      } +
      {
        'en_us': 'Manual addition of existing channel',
        'ja_jp': '既存チャネルの手動追加',
      } +
      {
        'en_us': 'Publish channel setting',
        'ja_jp': 'チャネル設定の公開',
      } +
      {
        'en_us': 'Disclose',
        'ja_jp': '開示する',
      } +
      {
        'en_us': 'Do not disclose',
        'ja_jp': '開示しない',
      } +
      {
        'en_us': 'Publish',
        'ja_jp': '公開する',
      } +
      {
        'en_us': 'Do not publish',
        'ja_jp': '公開しない',
      } +
      {
        'en_us': 'Top page',
        'ja_jp': 'トップページ',
      } +
      {
        'en_us': 'Yes',
        'ja_jp': 'する',
      } +
      {
        'en_us': 'No',
        'ja_jp': 'しない',
      } +
      {
        'en_us': 'Notification',
        'ja_jp': '通知',
      } +
      {
        'en_us': 'Profile automatic disclosure',
        'ja_jp': 'プロフィールの自動開示',
      } +
      {
        'en_us': 'Profile to disclose',
        'ja_jp': '開示するプロフィール',
      } +
      {
        'en_us': 'Class',
        'ja_jp': 'クラス',
      } +
      {
        'en_us': 'Title',
        'ja_jp': 'タイトル',
      } +
      {
        'en_us': 'AI service URL',
        'ja_jp': 'AIサービスのURL',
      } +
      {
        'en_us': 'Types of personal data',
        'ja_jp': 'パーソナルデータの種類',
      } +
      {
        'en_us': 'AI API key',
        'ja_jp': 'AIのAPIキー',
      } +
      {
        'en_us': 'Invalid URI.',
        'ja_jp': '不正なURI',
      } +
      {
        'en_us': 'Set valid URI to FIDO Notary.',
        'ja_jp': 'FIDO Notaryに正しいURIを設定してください',
      };

  String get i18n => localize(this, t);
}
