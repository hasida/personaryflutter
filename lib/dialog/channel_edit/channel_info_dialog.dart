import 'dart:async';

import 'package:flutter/material.dart';

import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';

import '../../widget.dart' show LocalizedTextInput;
import 'channel_edit.i18n.dart';
import 'data_setting_dialog.dart';

class ChannelInfoDialog extends StatefulWidget {
  final Root root;
  final Channel? channel;
  final DataSetting? dataSetting;
  final NotificationRegistry? notificationRegistry;

  ChannelInfoDialog({
    required this.root,
    this.channel,
    this.dataSetting,
    this.notificationRegistry,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ChannelInfoDialogState();
}

class _ChannelInfoDialogState extends State<ChannelInfoDialog> {
  Channel? _channel;

  final List<MapEntry<String,String>> _titleMap = [const MapEntry('', '')];
  final List<MapEntry<String,String>> _descriptionMap = [const MapEntry('', '')];

  DataSetting? _dataSetting;
  DataSetting? _defaultDataSetting;
  DataSetting? get dataSetting => _dataSetting ?? _defaultDataSetting;
  bool get isFixedDataSetting => widget.dataSetting != null;

  bool get isReadOnly =>
      (_channel != null && _channel!.plrId != widget.root.plrId);

  bool get isTitleInput => _titleMap.every((e) => e.value.isNotEmpty == true);

  @override
  void initState() {
    super.initState();

    if (widget.channel != null){
      _channel = widget.channel;
      _load();

      _channel!.syncSilently().then((value) {
        if (value.updatedGraphIds.isNotEmpty){
          setState(() => _load());
        }
      });
    }
    else {
      if (isFixedDataSetting) {
        /// 固定チャネル設定が存在する場合
        _dataSetting = widget.dataSetting;
        _defaultDataSetting = widget.dataSetting!;

        /// 名前、説明は固定チャネル設定と同じものを使用
        _titleMap.clear();
        var vm = _defaultDataSetting!.name?.valueMap;
        if (vm != null) {
          for (var e in vm.entries) {
            _titleMap.add(MapEntry(e.key, e.value.toString()));
          }
        }
        _descriptionMap.clear();
        vm = _defaultDataSetting!.description?.valueMap;
        if (vm != null) {
          for (var e in vm.entries) {
            _descriptionMap.add(MapEntry(e.key, e.value.toString()));
          }
        }
      } else {
        /// デフォルトのチャネル設定は「連絡」
        systemAccount.then((sa) {
            if (!mounted) return;
            setState(() => _defaultDataSetting = sa.communicationDataSetting);
        });
      }
    }
  }

  void _load() {
    if (_channel == null) return;

    print('_channel.name = ${_channel!.name}');
    if (_channel!.name?.valueMap.isNotEmpty ?? false) {
      _titleMap.clear();
      for (var e in _channel!.name!.valueMap.entries) {
        _titleMap.add(MapEntry(e.key, e.value.toString()));
      }
    }
    if (_channel!.description?.valueMap.isNotEmpty ?? false) {
      _descriptionMap.clear();
      for (var e in _channel!.description!.valueMap.entries) {
        _descriptionMap.add(MapEntry(e.key, e.value.toString()));
      }
    }
    _dataSetting = _channel!.channelDataSetting;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.fromLTRB(20,20,20,8),
      contentPadding: const EdgeInsets.fromLTRB(20,8,20,8),
      insetPadding: const EdgeInsets.all(10),
      title: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Icon(Icons.settings),
          const Padding(padding: const EdgeInsets.only(right: 6)),
          Text((_channel == null) ?
            'Create channel'.i18n : 'Channel info'.i18n,
          ),
        ],
      ),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text('Name'.i18n, style: const TextStyle(color: Colors.blue)),
            ),
            Container(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: LocalizedTextInput(
                _titleMap,
                placeholder: 'Name'.i18n,
                onValueChanged: (i, e) {
                  while (_titleMap.length <= i){
                    _titleMap.add(const MapEntry('', ''));
                  }
                  _titleMap[i] = e;
                },
                onDeleteLang: (lang) {
                  _titleMap.removeWhere((title) => title.key == lang);
                },
                isReadOnly: isReadOnly,
              )
            ),
            const Divider(
              height: 0,
              thickness: 1,
              color: Colors.grey,
            ),
            Container(
              padding: const EdgeInsets.only(top: 16),
              child: Text('Description'.i18n, style: const TextStyle(color: Colors.blue)),
            ),
            Container(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: LocalizedTextInput(
                _descriptionMap,
                placeholder: 'Description'.i18n,
                onValueChanged: (i, e) {
                  while (_descriptionMap.length <= i){
                    _descriptionMap.add(const MapEntry('', ''));
                  }
                  _descriptionMap[i] = e;
                },
                  onDeleteLang: (lang) {
                    _descriptionMap.removeWhere((description) => description.key == lang);
                  },
                isReadOnly: isReadOnly
              ),
            ),
            const Divider(
              height: 0,
              thickness: 1,
              color: Colors.grey,
            ),
            if (!isFixedDataSetting)
              const Divider(
                height: 0,
                thickness: 1,
                color: Colors.grey,
              ),
            if (!isFixedDataSetting)
              Container(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Channel setting'.i18n, style: const TextStyle(color: Colors.blue)),
              ),
            if (dataSetting != null)
              ChangeNotifierProvider<HasOwnerNotifier>(
                key: Key('channel_info_dataSetting_${dataSetting!.id}'),
                create: (_) => HasOwnerNotifier(
                  dataSetting!, root: widget.root)..update(),
                child: Consumer<HasOwnerNotifier>(
                  builder: (context, notifier, child) {
                    var picture = (notifier.picture != null)
                        ? memoryImageOf(notifier.picture!)
                        : const Icon(Icons.person, size: 24);
                    final tile = ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 15),
                      title: Text(
                        notifier.name ?? '',
                        style: const TextStyle(
                          color: Colors.black,
                        ),
                      ),
                      subtitle: Container(
                        padding: const EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              width: 24,
                              height: 24,
                              child: picture,
                            ),
                            Flexible(
                              child: Text(
                                notifier.owner ?? '',
                                style: const TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                    if (isReadOnly) {
                      return InkWell(
                        child: tile,
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (context) => DataSettingDialog(
                              dataSetting: dataSetting,
                              root: widget.root,
                              channelId: _channel?.id,
                              notificationRegistry: widget.notificationRegistry,
                            ),
                          );
                        },
                      );
                    }
                    else {
                      return _buildDataSettingPopup(context, tile);
                    }
                  },
                ),
              )
            else
              _buildDataSettingPopup(context,
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: Text(
                    'Channel setting is not set'.i18n,
                    style: const TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            Container(
              padding: const EdgeInsets.only(top: 16),
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
          child: Text('Cancel'.i18n),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('OK'.i18n),
          onPressed: isReadOnly ? null : () {
            if (!isTitleInput) {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: Text('Error'.i18n),
                  content: Text('Please input channel name.'.i18n),
                  actions: [
                    TextButton(
                      child: Text(
                        'OK'.i18n,
                        style: const TextStyle(color: Colors.blue),
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
              );
              return;
            }
            var f = Future(() async {
              var titleResult = Map<String, dynamic>();
              var descriptionResult = Map<String, dynamic>();
              for (var e in _titleMap){
                titleResult[e.key] = e.value;
              }
              for (var e in _descriptionMap){
                descriptionResult[e.key] = e.value;
              }
              if (_channel == null){
                _channel = await widget.root.newChannelWithDataSetting(
                  _dataSetting ?? _defaultDataSetting!,
                  nameMap: titleResult,
                  descriptionMap: descriptionResult,
                  notificationRegistry: widget.notificationRegistry,
                );
              }
              else {
                _channel!.name!.setValueMap(titleResult);
                _channel!.description!.setValueMap(descriptionResult);
                _channel!.setChannelDataSetting(_dataSetting);
              }

              await widget.root.syncSilently();
              await _channel!.syncSilently();
            });

            Navigator.of(context)
              .push(ProgressModal(context, f, 'saving data'.i18n))
              .then((value) => Navigator.of(context).pop(_channel));
          },
        ),
      ],
    );
  }

  Widget _buildDataSettingPopup(BuildContext context, Widget child)
    => PopupMenuButton(
      child: child,
      enabled: !isReadOnly,
      itemBuilder: (context) {
        var items = (_channel?.generatedFrom == null) ? [
          _DataSettingPopup.create,
          _DataSettingPopup.select,
          if (dataSetting != null) _DataSettingPopup.current,
        ] : [_DataSettingPopup.current];
        return items.map((e) =>
            PopupMenuItem(
              child: Text(e.label),
              value: e,
            )
        ).toList();
      },
      onSelected: (value) {
        switch(value) {
          case _DataSettingPopup.create:
            showDialog(
              context: context,
              builder: (context) => DataSettingDialog(
                root: widget.root, channelId: _channel?.id,
                notificationRegistry: widget.notificationRegistry,
              ),
            ).then((value) {
              if (value != null) {
                setState(() {
                  _dataSetting = value;
                });
              }
            });
            break;

          case _DataSettingPopup.select:
            showDialog<List<DataSetting>>(
              context: context,
              builder: (context) => ChangeNotifierProvider<DataSettingSelectNotifier>(
                create: (context) => DataSettingSelectNotifier(
                  widget.root, isMultiSelection: false,
                  notificationRegistry: widget.notificationRegistry!,
                  excludeList: (_dataSetting != null) ?
                  [_dataSetting!.id] : null,
                )..update(),
                child: DataSettingSelectDialog(
                  title: 'Select channel setting'.i18n,
                  root: widget.root,
                ),
              ),
            ).then((value) {
              if (value != null && value.isNotEmpty){
                setState(() {
                  _dataSetting = value.first;
                });
              }
            });
            break;

          case _DataSettingPopup.current:
            showDialog(
              context: context,
              builder: (context) => DataSettingDialog(
                dataSetting: dataSetting,
                root: widget.root,
                channelId: _channel?.id,
                notificationRegistry: widget.notificationRegistry,
              ),
            ).then((value) {
              if (value != null) {
                setState(() {
                  _dataSetting = value;
                });
              }
            });
            break;
        }
      },
    );
}

enum _DataSettingPopup {
  create, select, current;

  String get label {
    switch (this) {
      case _DataSettingPopup.create: return 'Create channel setting'.i18n;
      case _DataSettingPopup.select: return 'Select channel setting'.i18n;
      case _DataSettingPopup.current: return 'Edit channel setting'.i18n;
    }
  }
}
