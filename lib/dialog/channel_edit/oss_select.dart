import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';

import '../channel_edit/channel_edit.i18n.dart';
import '../channel_edit/channel_edit_util.dart';

typedef OssSelectDialogCallback = Function(
  TimelineState? timelineState,
  Iterable<OssFile> ontologyFiles,
  Iterable<OssFile> restrictionFiles,
  Iterable<OssFile> constraintFiles,
  Iterable<OssFile> timelineSSFiles,
  Iterable<OssFile> groupSSFiles,
  Iterable<OssFile> channelSSFiles,
);

class _OssSelectItem {
  final OssFile ossFile;
  HasProfile? ownerModel;

  _OssSelectItem(this.ossFile, [ this.ownerModel ]);
}

class OssSelectDialog extends StatefulWidget {
  final Root root;
  final TimelineState? timelineState;
  final Iterable<OssFile> ontologyFiles;
  final Iterable<OssFile> restrictionFiles;
  final Iterable<OssFile> constraintFiles;
  final Iterable<OssFile> timelineSSFiles;
  final Iterable<OssFile> groupSSFiles;
  final Iterable<OssFile> channelSSFiles;
  final OssSelectDialogCallback onDone;
  final bool isReadOnly;

  OssSelectDialog({
    required this.root,
    required this.onDone,
    this.timelineState,
    required this.ontologyFiles,
    required this.restrictionFiles,
    required this.constraintFiles,
    required this.timelineSSFiles,
    required this.groupSSFiles,
    required this.channelSSFiles,
    this.isReadOnly = false,
    super.key,
  });

  OssSelectDialog.fromSchema({
    required Root root,
    required OssSelectDialogCallback onDone,
    required Schema schema,
    bool isReadOnly = false,
    Key? key,
  }): this.fromDummySchema(
    root: root,
    onDone: onDone,
    schema: DummySchema.fromSchema(schema),
    isReadOnly: isReadOnly,
    key: key
  );

  OssSelectDialog.fromDummySchema({
    required Root root,
    required OssSelectDialogCallback onDone,
    required DummySchema schema,
    bool isReadOnly = false,
    Key? key,
  }): this(
    root: root,
    onDone: onDone,
    timelineState: schema.timelineState,
    ontologyFiles: schema.ontologyFiles,
    restrictionFiles: schema.restrictionFiles,
    constraintFiles: schema.constraintFiles,
    timelineSSFiles: schema.timelineSSFiles,
    groupSSFiles: schema.groupSSFiles,
    channelSSFiles: schema.channelSSFiles,
    isReadOnly: isReadOnly,
    key: key,
  );

  @override
  State<StatefulWidget> createState() => _OssSelectDialogState();
}

enum _Kind {
  ontology,
  restriction,
  constraint,
  timelineSS,
  channelSS,
  groupSS,
}

class _OssSelectDialogState extends State<OssSelectDialog> {
  TimelineState? timelineState;

  final Map<String, _OssSelectItem> ontologyFiles = {};
  final Map<String, _OssSelectItem> restrictionFiles = {};
  final Map<String, _OssSelectItem> constraintFiles = {};
  final Map<String, _OssSelectItem> timelineSSFiles = {};
  final Map<String, _OssSelectItem> groupSSFiles = {};
  final Map<String, _OssSelectItem> channelSSFiles = {};
  final Set<String> selectedOntology = {};
  final Set<String> selectedRestriction = {};
  final Set<String> selectedConstraint = {};
  final Set<String> selectedTimelineSS = {};
  final Set<String> selectedGroupSS = {};
  final Set<String> selectedChannelSS = {};

  @override
  void initState() {
    super.initState();

    timelineState = widget.timelineState;

    var ossSelectItems = <_OssSelectItem>[];

    Iterable<String> mapOssIds(Iterable<OssFile> ossFiles) => ossFiles.map(
      (e) => e.id
    ).nonNulls;

    Iterable<MapEntry<String, _OssSelectItem>> mapOssFiles(
      Iterable<OssFile> ossFiles,
    ) => ossFiles.map((e) {
        var id = e.id;
        if (id == null) return null;

        _OssSelectItem os = _OssSelectItem(e);
        if (!id.startsWith(systemAccountEmail)) {
          ossSelectItems.add(os);
        }
        return MapEntry(id, os);
      }
    ).nonNulls;

    selectedOntology.addAll(mapOssIds(widget.ontologyFiles));
    ontologyFiles.addEntries(mapOssFiles(widget.ontologyFiles));

    selectedRestriction.addAll(mapOssIds(widget.restrictionFiles));
    restrictionFiles.addEntries(mapOssFiles(widget.restrictionFiles));

    selectedConstraint.addAll(mapOssIds(widget.constraintFiles));
    constraintFiles.addEntries(mapOssFiles(widget.constraintFiles));

    selectedTimelineSS.addAll(mapOssIds(widget.timelineSSFiles));
    timelineSSFiles.addEntries(mapOssFiles(widget.timelineSSFiles));

    selectedGroupSS.addAll(mapOssIds(widget.groupSSFiles));
    groupSSFiles.addEntries(mapOssFiles(widget.groupSSFiles));

    selectedChannelSS.addAll(mapOssIds(widget.channelSSFiles));
    channelSSFiles.addEntries(mapOssFiles(widget.channelSSFiles));

    _loadOss();

    widget.root.friends.listen((event) async {
      final friendList = await event.value.toList();
      await Future.wait(friendList.map((f) => f.syncSilently()));
      for (var oss in ossSelectItems) {
        for (var f in friendList) {
          if (oss.ossFile.id!.startsWith(f.plrId.email)) {
            oss.ownerModel = f;
          }
        }
      }
      setState(() {});
    });
  }

  void _loadOss() async {
    _loadOssI((await systemAccount).publicRoot, null);
    _loadOssI(await publicRootOf(widget.root.storage), widget.root);
  }

  void _loadOssI(PublicRoot publicRoot, HasProfile? entity) async {
    await publicRoot.syncSilently();
    setState(() {
      var oss = publicRoot.oss;
      if (oss == null) return;

      Iterable<MapEntry<String, _OssSelectItem>> mapOssFiles(
        Iterable<OssFile> ossFiles,
      ) => ossFiles.map((e) {
          var id = e.id;
          if (id == null) return null;

          return MapEntry(id, _OssSelectItem(e, entity));
        },
      ).nonNulls;

      ontologyFiles.addEntries(mapOssFiles(oss.ontologyFiles));
      restrictionFiles.addEntries(mapOssFiles(oss.restrictionFiles));
      constraintFiles.addEntries(mapOssFiles(oss.constraintFiles));
      timelineSSFiles.addEntries(mapOssFiles(oss.timelineSSFiles));
      groupSSFiles.addEntries(mapOssFiles(oss.groupSSFiles));
      channelSSFiles.addEntries(mapOssFiles(oss.channelSSFiles));
    });
  }

  @override
  Widget build(BuildContext context) {
    var content = <Widget>[];

    content.add(_createTitleCell('Timeline'.i18n));
    content.add(_createTimelineStateCell());

    content.add(_createTitleCell('Ontology'.i18n));
    ontologyFiles.remove(SystemAccount.baseOntologyOssId.toString());
    for (var e in ontologyFiles.entries) {
      content.add(_createCell(_Kind.ontology, e.value));
    }

    content.add(_createTitleCell('Restriction'.i18n));
    restrictionFiles.remove(SystemAccount.baseRestrictionOssId.toString());
    for (var e in restrictionFiles.entries){
      content.add(_createCell(_Kind.restriction, e.value));
    }

    content.add(_createTitleCell('Constraint'.i18n));
    for (var e in constraintFiles.entries){
      content.add(_createCell(_Kind.constraint, e.value));
    }

    content.add(_createTitleCell('Timeline style sheet'.i18n));
    for (var e in timelineSSFiles.entries){
      content.add(_createCell(_Kind.timelineSS, e.value));
    }

    content.add(_createTitleCell('Group summary style sheet'.i18n));
    for (var e in groupSSFiles.entries){
      content.add(_createCell(_Kind.groupSS, e.value));
    }

    content.add(_createTitleCell('Channel summary style sheet'.i18n));
    for (var e in channelSSFiles.entries){
      content.add(_createCell(_Kind.channelSS, e.value));
    }

    return AlertDialog(
      titlePadding: const EdgeInsets.fromLTRB(0,20,0,0),
      contentPadding: const EdgeInsets.fromLTRB(0,0,0,8),
      insetPadding: const EdgeInsets.fromLTRB(20,20,20,20),
      title: Container(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 18),
        width: MediaQuery.of(context).size.width - 40,
        decoration: const BoxDecoration(
          border: const Border(
              bottom: const BorderSide(color: Colors.grey)
          ),
        ),
        child: Row(
          children: [
            const Icon(Icons.settings),
            const Padding(padding: const EdgeInsets.only(right: 6)),
            Text('OSS setting'.i18n),
          ],
        ),
      ),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: content,
        ),
      ),
      actions: [
        if (!widget.isReadOnly)
          TextButton(
            child: Text('Friends reference'.i18n),
            onPressed: () {
              showDialog<Iterable<Friend>>(
                context: context,
                builder: (ctx) => FriendSelectDialog(
                  root: widget.root,
                  title: 'Friends reference'.i18n,
                  style: FriendListStyle.checkbox,
                  isMultiSelection: true,
                )
              ).then((value) {
                if (value != null){
                  for (var friend in value) {
                    friend.syncSilently().then((_) {
                      friend.publicRoot?.syncSilently().then((_) {
                        _loadOssI(friend.publicRoot!, friend);
                      });
                    });
                  }
                }
              });
            },
          ),
        TextButton(
          child: Text('Cancel'.i18n),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('OK'.i18n),
          onPressed: widget.isReadOnly ? null : () {
            widget.onDone(
              timelineState,
              ontologyFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedOntology.contains(e.id)),
              restrictionFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedRestriction.contains(e.id)),
              constraintFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedConstraint.contains(e.id)),
              timelineSSFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedTimelineSS.contains(e.id)),
              groupSSFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedGroupSS.contains(e.id)),
              channelSSFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedChannelSS.contains(e.id)),
            );
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  Widget _createTitleCell(String title){
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(8, 12, 12, 12),
      color: const Color(0xff43a047),
      child: Text(title,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _createTimelineStateCell() => StatefulBuilder(
    builder: (context, setState) => Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Text("Status".i18n),
          const SizedBox(width: 10),
          DropdownButton(
            items: [ null, ...TimelineState.values ].map(
              (s) => DropdownMenuItem(
                value: s, child: Text((s?.name ?? "Not set").i18n),
              ),
            ).toList(),
            value: timelineState,
            onChanged: (TimelineState? value) => setState(
              () => timelineState = value,
            ),
          ),
        ],
      ),
    ),
  );

  Widget _createCell(_Kind kind, _OssSelectItem item) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
      decoration: const BoxDecoration(
        border: const Border(
            bottom: const BorderSide(color: Colors.grey)
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: ChangeNotifierProvider<HasOwnerNotifier>(
              key: Key('oss_select_provider_${item.ossFile.ossId}_${item.ownerModel?.id}'),
              create: (_) =>
                  HasOwnerNotifier(item.ownerModel, root: widget.root)
                    ..update(),
              child: Consumer<HasOwnerNotifier>(
                builder: (ctx, notifier, child) {
                  var picture = (notifier.picture != null)
                      ? memoryImageOf(notifier.picture!)
                      : const Icon(Icons.person, size: 24);
                  return Row(
                    children: [
                      Checkbox(
                        value: getSelected(kind, item.ossFile.id!),
                        onChanged: widget.isReadOnly
                            ? null
                            : (value) => setSelected(kind, item.ossFile.id!, value ?? false),
                        activeColor: Colors.blue,
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          item.ossFile.title.defaultValue ?? item.ossFile.id!,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      if (item.ownerModel != null)
                        Expanded(
                          flex: 1,
                          child: Container(
                            padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  SizedBox(
                                    width: 24,
                                    height: 24,
                                    child: picture,
                                  ),
                                Expanded(
                                  child: Text(
                                    notifier.owner ?? '',
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool getSelected(_Kind kind, String id){
    switch(kind){
      case _Kind.ontology:
        return selectedOntology.contains(id);

      case _Kind.restriction:
        return selectedRestriction.contains(id);

      case _Kind.constraint:
        return selectedConstraint.contains(id);

      case _Kind.timelineSS:
        return selectedTimelineSS.contains(id);

      case _Kind.groupSS:
        return selectedGroupSS.contains(id);

      case _Kind.channelSS:
        return selectedChannelSS.contains(id);
    }
  }

  void setSelected(_Kind kind, String id, bool value){
    switch (kind){
      case _Kind.ontology:
        if (value){
          selectedOntology.add(id);
        }
        else {
          selectedOntology.remove(id);
        }
        break;

      case _Kind.restriction:
        if (value){
          selectedRestriction.add(id);
        }
        else {
          selectedRestriction.remove(id);
        }
        break;

      case _Kind.constraint:
        if (value){
          selectedConstraint.add(id);
        }
        else {
          selectedConstraint.remove(id);
        }
        break;

      case _Kind.timelineSS:
        if (value){
          selectedTimelineSS.add(id);
        }
        else {
          selectedTimelineSS.remove(id);
        }
        break;

      case _Kind.groupSS:
        if (value){
          selectedGroupSS.add(id);
        }
        else {
          selectedGroupSS.remove(id);
        }
        break;

      case _Kind.channelSS:
        if (value){
          selectedChannelSS.add(id);
        }
        else {
          selectedChannelSS.remove(id);
        }
        break;
    }
    setState((){});
  }
}
