import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget.dart';
import 'show_all_dialog.i18n.dart';

class ShowAllDialog<
  T extends HasId,
  N extends PlrAsyncNotifier<S>,
  S extends StateInterface
> extends StatelessConsumerPlrWidget {
  final PlrAsyncNotifierProvider<N, S> provider;
  final String title;
  final AsyncListItemsGetter<T, S> itemsGetter;
  final List<Widget> buttons;
  final LimitedListBuilder listBuilder;
  ShowAllDialog(
    this.provider, {
      super.key,
      String? title,
      required this.itemsGetter,
      this.buttons = const [],
      required this.listBuilder,
  }): this.title = title ?? "All".i18n;

  late final PlrAsyncNotifier controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    controller = ref.read(provider.notifier);
  }

  Future<void> _refresh([ bool force = false ]) =>
    controller.refresh(force: force);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: Scaffold(
      appBar: _buildAppBar(context, ref),
      body: RefreshIndicator(
        onRefresh: () => _refresh(),
        child: listBuilder(context, ref, false),
      ),
    ),
  );

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) {
    List<T> items; {
      final value = ref.watch(provider).value;
      if (value != null) items = itemsGetter(value);
      else items = const [];
    }
    return AppBar(
      title: Hero(
        tag: title,
        flightShuttleBuilder: textFlightShuttleBuilder,
        child: Text(title + " (${items.length})"),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            children: buttons,
          ),
        ),
      ],
    );
  }
}
