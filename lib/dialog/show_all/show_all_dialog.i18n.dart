import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "All",
    "ja_jp": "全て",
  };

  String get i18n => localize(this, t);
}
