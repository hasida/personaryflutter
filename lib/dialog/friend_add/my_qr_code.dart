import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../style.dart';
import '../../util.dart' show showError;
import '../../widget.dart';
import 'my_qr_code.i18n.dart';

class MyQrCode extends StatelessConsumerPlrWidget {
  final TabController? tabController;
  final TextEditingController? controller;
  const MyQrCode(
    this.tabController,
    this.controller, {
      super.key,
  });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    Future<FriendRequest> getMyRequest() async => publicRoot.toFriendRequest();

    return FutureBuilder<FriendRequest>(
      future: getMyRequest(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          showError(context, snapshot.error!, snapshot.stackTrace);
          return emptyPage;
        }
        if (!snapshot.hasData) {
          return const Center(child: const CircularProgressIndicator());
        }

        var content = Wrap(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Don't show close button when build as a page.
                if (tabController == null) IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () => Navigator.pop(context),
                ),
                FriendRequestPanel(snapshot.data!),
                Padding(
                  padding: horizontalPadding[3],
                  child: Text(
                    "Other user can add you to friend list by scanning this QR code or entering your request code".i18n,
                  ),
                ),
                const SizedBox(height: 10),
              ],
            ),
          ],
        );
        // Centering content when build as a page.
        if (tabController != null) return Center(child: content);
        return content;
      },
    );
  }
}
