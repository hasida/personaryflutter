import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart'
  hide ChangeNotifierProvider, Consumer, Notifier, Provider;
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import '../../page/friend/friend_page.dart' show showAppDefinitionImportDialog;
import 'request_code_field.i18n.dart';

class RequestCodeField extends StatefulConsumerPlrWidget {
  final TabController tabController;
  final TextEditingController controller;
  const RequestCodeField(
    this.tabController,
    this.controller, {
      super.key,
  });

  @override
  ConsumerState<RequestCodeField> createState() => _RequestCodeFieldState();
}

class _RequestCodeFieldState extends PlrConsumerState<RequestCodeField> {
  FriendRequest? friendRequest;

  @override
  Widget build(BuildContext context) => Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        color: Colors.white,
        child: TextFormField(
          controller: widget.controller,
          decoration: InputDecoration(
            hintText: "Enter request code".i18n,
          ),
          autovalidateMode: AutovalidateMode.always,
          validator: (value) {
            if (value == null) return null;

            // onChanged is not called when moved from Scan QR page.
            var r; try {
              r = FriendRequest.fromString(value.trim());
            } on ArgumentError {}

            // May be called in build process.
            if (friendRequest != r) Future.microtask(
              () => setState(() => friendRequest = r),
            );
            // Always valid.
            return null;
          },
        ),
      ),
      const SizedBox(height: 20),
      ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color(0xFFF2B944),
          foregroundColor: Colors.black87,
        ),
        onPressed: (friendRequest != null) ? () async {
          await _registerFriend(context);
          Navigator.pop(context);
        } : null,
        child: Text("Register".i18n),
      ),
    ],
  );

  Future<Friend?> _registerFriend(BuildContext context) => runWithProgress(
    context, () async {
      var isSubscribed = false;

      var friend = await registerFriend(
        root, friendRequest!, infoRegistry: infoRegistry,
        onSubscribeInfoChannel: (_) => isSubscribed = true,
      );

      final publicRoot = friend.publicRoot!;
      final publicRootSyncFuture = publicRoot.syncSilently();

      if (isSubscribed) await showAnnouncementSubscribedDialog(context);

      if (await mayDepositPassphrase(root, friend.meToFriendRoot!)) {
        await showPassphraseDepositedDialog(context, [ friend ]);
      }

      await publicRootSyncFuture;
      if (publicRoot.hasAppDefinitions) {
        await showAppDefinitionImportDialog(context, ref, account, publicRoot);
      }
      return friend;
    }, onError: (e, s) => processNeedNetworkException(context, e, s),
  );
}
