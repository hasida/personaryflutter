import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Scan QR code",
    "ja_jp": "QRコードをスキャン",
  } +
  {
    "en_us": "Enter request code",
    "ja_jp": "リクエスト文字列入力",
  } +
  {
    "en_us": "My QR code",
    "ja_jp": "マイQRコード",
  };

  String get i18n => localize(this, t);
}
