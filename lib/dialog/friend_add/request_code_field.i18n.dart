import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Enter request code",
    "ja_jp": "リクエスト文字列を入力してください",
  } +
  {
    "en_us": "Register",
    "ja_jp": "登録する",
  };

  String get i18n => localize(this, t);
}
