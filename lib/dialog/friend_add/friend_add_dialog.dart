import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget.dart';
import 'friend_add_dialog.i18n.dart';
import 'my_qr_code.dart';
import 'request_code_field.dart';
import 'scan_qr.dart';

class FriendAddDialog extends StatefulConsumerPlrWidget {
  final String? friendRequestString;
  const FriendAddDialog({
      super.key,
      this.friendRequestString,
  });

  @override
  ConsumerState<FriendAddDialog> createState() => _FriendAddDialogState();
}

late int scanQrCodeTabIndex;
late int requestCodeFieldTabIndex;
late int myQrCodeTabIndex;

typedef TabCreator = Widget Function(
  TabController tabController,
  TextEditingController requestCodeFieldController,
);

class _TabDefinition {
  final String label;
  final TabCreator create;
  _TabDefinition(
    String defaultLabel,
    this.create,
  ): this.label = defaultLabel.i18n;
}

class _FriendAddDialogState extends PlrConsumerState<FriendAddDialog>
  with SingleTickerProviderStateMixin {

  late List<_TabDefinition> _tabs;

  late TabController tabController;
  late TextEditingController requestCodeFieldController;

  @override
  void initState() {
    super.initState();

    var requestCodeFieldDefinition = _TabDefinition(
      "Enter request code",
      (tabController, requestCodeFieldController) => RequestCodeField(
        tabController, requestCodeFieldController,
      ),
    );

    if (isMobile || Platform.isMacOS) {
      _tabs = [
        _TabDefinition(
          "Scan QR code",
          (tabController, requestCodeFieldController) => ScanQr(
            tabController, requestCodeFieldController,
          ),
        ),
        requestCodeFieldDefinition,
      ];
      scanQrCodeTabIndex = 0;
      requestCodeFieldTabIndex = 1;
      myQrCodeTabIndex = -1;
    } else {
      _tabs = [
        requestCodeFieldDefinition,
        _TabDefinition(
          "My QR code",
          (tabController, requestCodeFieldController) => MyQrCode(
            tabController, requestCodeFieldController,
          ),
        ),
      ];
      scanQrCodeTabIndex = -1;
      requestCodeFieldTabIndex = 0;
      myQrCodeTabIndex = 1;
    }

    tabController = TabController(length: _tabs.length, vsync: this);
    requestCodeFieldController = TextEditingController();

    if (widget.friendRequestString != null) {
      Future.microtask(() {
          requestCodeFieldController.text = widget.friendRequestString!;
          tabController.animateTo(requestCodeFieldTabIndex);
      });
    }
  }

  @override
  void dispose() {
    super.dispose();

    tabController.dispose();
    requestCodeFieldController.dispose();
  }

  @override
  Widget build(BuildContext context) => SafeArea(
    child: ProgressHUD(
      child: Builder(
        builder: (context) => DefaultTabController(
          length: _tabs.length,
          child: Scaffold(
            appBar: AppBar(),
            body: TabBarView(
              controller: tabController,
              children: [
                for (var t in _tabs) t.create(
                  tabController, requestCodeFieldController,
                ),
              ],
            ),
            bottomNavigationBar: Container(
              color: Colors.white,
              child: TabBar(
                controller: tabController,
                indicatorWeight: 4.0,
                indicatorColor: Colors.blue,
                tabs: [
                  for (var t in _tabs) Tab(text: t.label),
                ],
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
