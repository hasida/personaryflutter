import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "No camera access is granted.",
    "ja_jp": "カメラにアクセスできません",
  } +
  {
    "en_us": "Scan QR code to add friend",
    "ja_jp": "QRコードをスキャンして友達を追加",
  } +
  {
    "en_us": "My QR code",
    "ja_jp": "マイQRコード",
  } +
  {
    "en_us": "Not a PLR QR code.",
    "ja_jp": "PLRのQRコードではありません",
  } +
  {
    "en_us": "Request code",
    "ja_jp": "リクエスト文字列",
  } +
  {
    "en_us":
    "Other user can add you to friend list by scanning this QR code or entering your request code",
    "ja_jp": "他の利用者がこのQRコードをスキャンするか、リクエスト文字列を入力することで、あなたが友達に追加されます。",
  };
  String get i18n => localize(this, t);
}
