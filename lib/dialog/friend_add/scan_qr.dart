import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

import '../../util.dart';
import '../../widget.dart';
import 'friend_add_dialog.dart' show requestCodeFieldTabIndex;
import 'my_qr_code.dart';
import 'scan_qr.i18n.dart';

class ScanQr extends StatefulConsumerPlrWidget {
  final TabController tabController;
  final TextEditingController requestCodeFieldController;
  const ScanQr(
    this.tabController,
    this.requestCodeFieldController, {
      super.key,
  });

  @override
  ConsumerState<ScanQr> createState() => _ScanQrState();
}

class _ScanQrState extends PlrConsumerState<ScanQr>
  with WidgetsBindingObserver {

  late final PermissionObserver? observer;

  final MobileScannerController cameraController = MobileScannerController(
    detectionSpeed: DetectionSpeed.noDuplicates,
  );

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    if (isMobile) {
      observer = PermissionObserver(
         this, Permission.camera, "No camera access is granted.".i18n,
       );
       Future.microtask(() => observer!.observe(context));
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    observer?.onChangeAppLifecycleState(context, state);
  }

  @override
  Future<void> dispose() async {
    observer?.dispose();
    WidgetsBinding.instance.removeObserver(this);

    await cameraController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 4,
          child: Stack(
            children: [
              MobileScanner(
                controller: cameraController,
                onDetect: (capture) {
                  if (capture.barcodes.isEmpty) return;
                  var value = capture.barcodes.first.rawValue;
                  if (value != null) _parseScanData(context, value);
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Align(
                  alignment: Alignment.topRight,
                  child: ValueListenableBuilder<MobileScannerState>(
                    valueListenable: cameraController,
                    builder: (context, state, _) => Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (state.torchState != TorchState.unavailable) Padding(
                          padding: const EdgeInsets.only(right: 5),
                          child: FloatingActionButton(
                            mini: true,
                            heroTag: "torch",
                            child: switch (state.torchState) {
                              TorchState.on => const Icon(
                                Icons.flashlight_on, color: Colors.white,
                              ),
                              _ => const Icon(
                                Icons.flashlight_off, color: Colors.grey,
                              ),
                            },
                            onPressed: () => cameraController.toggleTorch(),
                          ),
                        ),
                        if (isMobile) Padding(
                          padding: const EdgeInsets.only(right: 5),
                          child: FloatingActionButton(
                            mini: true,
                            heroTag: "camera",
                            child: ValueListenableBuilder(
                              valueListenable: cameraController,
                              builder: (_, state, __) => (
                                switch (state.cameraDirection) {
                                  CameraFacing.back =>
                                    const Icon(Icons.camera_rear),
                                  CameraFacing.front =>
                                    const Icon(Icons.camera_front),
                                }
                              ),
                            ),
                            onPressed: () => cameraController.switchCamera(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 16),
                  child: FloatingActionButton.extended(
                    shape: const StadiumBorder(),
                    heroTag: "myQrCode",
                    label: Text(
                      "My QR code".i18n,
                      style: const TextStyle(color: Colors.black),
                    ),
                    onPressed: () => _showMyQrCode(context),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            color: Colors.white,
            child: Center(child: Text("Scan QR code to add friend".i18n)),
          ),
        )
      ],
    );
  }

  Future<void> _parseScanData(BuildContext context, String code) async {
    var friendRequest; try {
      friendRequest = FriendRequest.fromString(code);
    } on ArgumentError {
      showMessage(context, "Not a PLR QR code.".i18n);
      // Wait for closing the snackbar.
      await Future.delayed(const Duration(seconds: 3));
      return;
    }
    // Paste request string and move to request_code_field page.
    widget.requestCodeFieldController.text = friendRequest.toString();
    widget.tabController.animateTo(requestCodeFieldTabIndex);
  }

  Future<void> _showMyQrCode(BuildContext context) async {
    await showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (_) => const MyQrCode(null, null),
    );
  }
}
