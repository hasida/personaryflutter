import "package:i18n_extension/i18n_extension.dart";

import 'package:plr_util/src/util/base_translations.dart' show meTranslation;

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  meTranslation +
  {
    "en_us": "Public channel",
    "ja_jp": "公開チャネル",
  } +
  {
    "en_us": "No public channels.",
    "ja_jp": "公開チャネルがありません",
  } +
  {
    "en_us": "Add public channel.",
    "ja_jp": "公開チャネルを追加",
  } +
  {
    "en_us": "Channel to add",
    "ja_jp": "追加するチャネル",
  } +
  {
    "en_us": "Release",
    "ja_jp": "解除",
  } +
  {
    "en_us": "Release the public channel",
    "ja_jp": "公開チャネルの解除",
  } +
  {
    "en_us": "Are you sure to release the public channel setting?",
    "ja_jp": "この公開チャネルを解除してよろしいですか?",
  };

  String get i18n => localize(this, t);
}
