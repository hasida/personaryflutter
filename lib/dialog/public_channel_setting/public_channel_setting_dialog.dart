import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import 'public_channel_setting_dialog.i18n.dart';

class PublicChannelSettingDialog extends StatelessConsumerPlrWidget {
  PublicChannelSettingDialog({ super.key });

  late final PublicRootSyncProvider _provider;
  late final PublicRootSync _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _provider = publicRootSyncProvider(publicRoot);
    _controller = ref.read(_provider.notifier);
  }

  Future<void> _refresh([ bool force = false ]) =>
    _controller.refresh(force: force);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: Scaffold(
      appBar: _buildAppBar(context, ref),
      body: RefreshIndicator(
        onRefresh: () => _refresh(),
        child: _buildList(context, ref),
      ),
    ),
  );

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Text("Public channel".i18n),
    actions: [
      Container(
        width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
        child: FloatingActionButton(
          heroTag: "hero_add_public_channel",
          tooltip: "Add public channel.".i18n,
          child: const Icon(Icons.add),
          onPressed: () => _add(context, ref),
        ),
      ),
    ],
  );

  final List<Widget> _bgPair = dismissibleBackgroundPairWith(
    Colors.red, const Icon(Icons.public_off, color: Colors.white),
    Text("Release".i18n, style: const TextStyle(color: Colors.white)),
  );

  Widget _buildList(
    BuildContext context,
    WidgetRef ref,
  ) => ChannelList<
    Channel, PublicRootSyncProvider, PublicRootSync, PublicRootState
  >(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    provider: _provider,
    container: publicRoot,
    cellConfig: channelCellConfigWith<Channel>(
      context, ref, _controller,
      useDefaultButtons: false, useDefaultDismissibleConfig: false,
    ).copyWith(
      dismissibleConfig: channelDismissibleConfigWithDefault(
        canDismiss: (_, __, ___, ____, _____) => true,
        background: _bgPair[0], secondaryBackground: _bgPair[1],
        confirm: _confirm,
        objectRemover: _remove,
        onDismissed: (_) => _refresh(),
        onError: (e, s) => showError(context, e, s),
      ),
    ),
    empty: Text("No public channels.".i18n),
    onError: (e, s) => showError(context, e, s),
  );

  Future<Channel?> _add(BuildContext context, WidgetRef ref) async {
    var channel = (
      await showChannelSelectDialog(
        context, root,
        excludes: ref.read(_provider).value?.channels,
        onError: (e, s) => showError(context, e, s),
      )
    )?.first;
    if (channel == null) return null;

    var publicChannel = await runWithProgress(
      context, () => publicRoot.addChannel(channel),
      onError: (e, s) => showError(context, e, s),
    );
    await _refresh();

    return publicChannel;
  }

  Future<bool> _confirm(
    BuildContext context,
    WidgetRef ref,
    Channel channel,
    Entity? referrer,
    AsyncValue<ChannelUpdateState?> state,
    DismissDirection direction,
  ) => showConfirmDialog(
    context, "Release the public channel".i18n,
    "Are you sure to release the public channel setting?".i18n,
  );

  Future<bool> _remove(
    BuildContext context,
    WidgetRef ref,
    PlrAsyncNotifier<ChannelUpdateState?> controller,
    Channel channel,
    Entity? referrer,
    AsyncValue<ChannelUpdateState?> state,
  ) => publicRoot.removeChannel(channel);
}
