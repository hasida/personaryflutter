import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart';
import '../page/main/main_page.dart';
import '../page/main/app_definition.dart' show homeTab;
import '../page/friend/friend_page.dart' show openFriendOrUserPage;

void _mayOpenUser(BuildContext context, Account account, HasProfile user) {
  if (user.plrId == account.root.plrId) {
    MainPage.setTab(context, homeTab, forceBuild: true);
  } else openFriendOrUserPage(context, account, user);
}

List<
  AsyncCellButtonBuilder<HasProfile, N, S>
> defaultDataSubjectButtonBuildersWith<
  N extends UserInfoUpdate, S extends UserInfo?
>(BuildContext context, WidgetRef ref) => [];

AsyncCellConfig<
 HasProfile, UserInfoUpdate, UserInfo?
> dataSubjectCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<HasProfile, UserInfoUpdate, UserInfo?>
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<HasProfile, UserInfoUpdate, UserInfo?>? dismissibleConfig,
}) => dataSubjectCellConfigWithDefault(
  onTap: (user, controller) => _mayOpenUser(
    context, ref.read(plrAccountProvider).value!, user,
  ),
  buttonBuilders: useDefaultButtons ? defaultDataSubjectButtonBuildersWith(
    context, ref,
  ) : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? dataSubjectDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  HasProfile, UserInfoUpdate, UserInfo?
> dataSubjectDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => dataSubjectDismissibleConfigWithDefault(
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => showError(context, e, s),
);
