import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart';

Future<bool> disclosedToUserRemoverWithProgress(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier<UserInfo?> controller,
  DisclosedToUser disclosedToUser,
  Entity? referrer,
  AsyncValue<UserInfo?> state,
) async => await runWithProgress(
  context, () => defaultDisclosedToUserRemover(
    context, ref, controller, disclosedToUser, referrer, state,
  ),
) ?? false;

AsyncCellConfig<
  DisclosedToUser, UserInfoUpdate, UserInfo?
> disclosedToUserCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<
        DisclosedToUser, UserInfoUpdate, UserInfo?
      >
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<
      DisclosedToUser, UserInfoUpdate, UserInfo?
    >? dismissibleConfig,
}) => disclosedToUserCellConfigWithDefault(
  buttonBuilders: useDefaultButtons ? const [] : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? disclosedToUserDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  DisclosedToUser, UserInfoUpdate, UserInfo?
> disclosedToUserDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => disclosedToUserDismissibleConfigWithDefault(
  objectRemover: disclosedToUserRemoverWithProgress,
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => showError(context, e, s),
);
