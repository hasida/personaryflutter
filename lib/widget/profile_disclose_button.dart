import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../widget.dart';
import 'profile_disclose_button.i18n.dart';

class ProfileDiscloseButton extends StatelessPlrWidget {
  final ProfileItemsProvider publicProfileItemsProvider;
  ProfileDiscloseButton(
    super.account, {
      ProfileItemsProvider? publicProfileItemsProvider,
  }): this.publicProfileItemsProvider = publicProfileItemsProvider ??
    profileItemsProvider(account.publicRoot, scanOthers: false);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final publicProfileItemsState =
      ref.watch(publicProfileItemsProvider).value;
    final publicProfileItemsController = ref.read(
      publicProfileItemsProvider.notifier,
    );

    return CompactIconButton(
      icon: const Icon(Icons.settings),
      tooltip: 'Publish profile'.i18n,
      onPressed: (publicProfileItemsState != null)
        ? () async {
          final result = await showDialog<List<ProfileEntry>>(
            context: context,
            builder: (_) => ProfileDiscloseDialog(
              title: 'Publish profile'.i18n,
              root: root,
              selectedList: publicProfileItemsState.flatten.map((e) => e.value),
            ),
          );
          if (result == null) return;

          final newItems = <SchemaClass, List<ProfileItem>>{};
          for (var r in result) {
            (newItems[r.schemaClass] ??= []).add(r.item);
          }
          await runWithProgress(
            context,
            () => ProfileDiscloseManager(publicRoot).updateItems(
              publicProfileItemsState.items, newItems,
            ),
          );
          publicProfileItemsController.refresh(force: true);
        }
        : null,
    );
  }
}
