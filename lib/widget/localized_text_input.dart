import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_extension.dart';

class LocalizedTextInput extends StatefulWidget {
  final List<MapEntry<String,String>> values;
  final Function(int,MapEntry<String, String>)? onValueChanged;
  final Function(String) onDeleteLang;
  final bool isReadOnly;
  final String? placeholder;

  LocalizedTextInput(this.values, {
    this.onValueChanged,
    required this.onDeleteLang,
    this.isReadOnly = false,
    this.placeholder,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LocalizedTextInputState();
}

class _LocalizedTextInputState extends State<LocalizedTextInput> {
  final List<MapEntry<String,String>> _entryList = [];

  bool get isReadOnly => widget.isReadOnly;

  @override
  void initState() {
    super.initState();

    _entryList.addAll(widget.values);
  }

  Set<String> usedKeys = {};

  @override
  Widget build(BuildContext context) {
    var cells = <Widget>[];
    for (var i = 0; i < _entryList.length; ++i){
      usedKeys.add(_entryList[i].key);
      cells.add(
        _LocalizedTextCell(
          key: ValueKey(_entryList[i].key),
          lang: _entryList[i].key,
          value: _entryList[i].value,
          onValueChanged: (key, value) {
            _entryList[i] = MapEntry(key, value);
            widget.onValueChanged!(i, _entryList[i]);
            setState((){});
          },
          onDeleteLang: (key) {
            _entryList.removeWhere((entry) => entry.key == key);
            widget.values.removeWhere((entry) => entry.key == key);
            widget.onDeleteLang(key);
            setState(() {});
          },
          isReadOnly: isReadOnly,
          placeholder: widget.placeholder,
          usedKeys: usedKeys,
        ),
      );
    }

    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(bottom: 10),
      child: Column(
        children: [
          ...cells,
          if (!isReadOnly)
            Container(
              alignment: Alignment.centerLeft,
              height: 36,
              child: IconButton(
                icon: const Icon(Icons.add),
                iconSize: 24,
                padding: EdgeInsets.zero,
                onPressed: () => setState(() {
                  var languageEntry = _Languages.languages.entries
                      .firstWhere((e) => (!usedKeys.contains(e.key)), orElse: null);
                  var entry = new MapEntry(languageEntry.key, '');
                  _entryList.add(entry);
                  widget.onValueChanged!(_entryList.length-1, entry);
                }),
              ),
            ),
        ],
      ),
    );
  }
}

class _LocalizedTextCell extends StatefulWidget {
  final Key? key;
  final String lang;
  final String value;
  final Function(String key, String value) onValueChanged;
  final Function(String key) onDeleteLang;
  final bool isReadOnly;
  final String? placeholder;
  final Set<String>? usedKeys;

  _LocalizedTextCell({
    this.key,
    required this.lang,
    required this.value,
    required this.onValueChanged,
    required this.onDeleteLang,
    this.isReadOnly = true,
    this.placeholder,
    this.usedKeys,
  }):super(key: key);

  @override
  State<StatefulWidget> createState() => _LocalizedTextCellState();
}

class _LocalizedTextCellState extends State<_LocalizedTextCell> {
  late String lang;
  late String value;

  final _controller = TextEditingController();

  bool get isReadOnly => widget.isReadOnly;

  @override
  void initState() {
    super.initState();

    lang  = widget.lang;
    value = widget.value;

    _controller.text = value;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            DropdownButton(
              value: lang,
              items: _Languages.languages.entries
                  .where((e) => ((e.key == lang) || !widget.usedKeys!.contains(e.key)))
                  .map((e) =>
                  DropdownMenuItem(
                    value: e.key,
                    child: Text(e.value),
                  )
              ).toList(),
              onChanged: isReadOnly ? null :
                  (dynamic v) => setState(() {
                widget.usedKeys?.remove(lang);
                widget.usedKeys?.add(v);
                lang = v;
                widget.onValueChanged(lang, value);
              }),
              disabledHint: Text(_Languages.languages[lang]!),
            ),
            if(!isReadOnly && (widget.usedKeys?.length ?? 0) > 1)
              IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  widget.usedKeys?.remove(lang);
                  widget.onDeleteLang(lang);
                  setState(() {});
                },
              ),
          ],
        ),
        Container(
          padding: const EdgeInsets.only(bottom: 8),
          child: TextField(
            controller: _controller,
            maxLines: 1,
            style: TextStyle(
              fontSize: 18,
              color: isReadOnly ? Colors.grey : Colors.black,
            ),
            decoration: InputDecoration(
              hintText: widget.placeholder,
              contentPadding: const EdgeInsets.fromLTRB(3,0,3,0),
              border: const UnderlineInputBorder(
                borderSide: const BorderSide(
                  color: Colors.black,
                ),
              ),
              focusedBorder: const UnderlineInputBorder(
                borderSide: const BorderSide(
                  color: Colors.blue,
                ),
              ),
            ),
            onChanged: (v) {
              value = v;
              widget.onValueChanged(lang, value);
            },
            readOnly: isReadOnly,
          ),
        ),
      ],
    );
  }
}

class _Languages {
  static Translations? __translations;

  static Translations get _translations {
    if (__translations == null) {
      __translations = Translations.byText('key');
      for (var e in __languageMap.entries) {
        __translations = __translations! + {
          'key': e.key,
          ...e.value,
        };
      }
    }
    return __translations!;
  }

  static Map<String,String>? _languages;
  static Map<String,String> get languages {
    if (_languages == null){
      _languages = Map<String,String>();
      for (var e in __languageMap.entries){
        _languages![e.key] = localize(e.key, _translations);
      }
//      print('languages: $_languages');
    }
    return _languages!;
  }

  static const Map<String,Map<String,String>> __languageMap = {
    '':   {'en_us': 'unspecified lang',    'ja_jp': '言語無指定'},
    'en': {'en_us': 'English',             'ja_jp': '英語'},
    'ja': {'en_us': 'Japanese',            'ja_jp': '日本語'},
    'ja-Hrkt': {'en_us': 'Ruby(Japanese)', 'ja_jp': 'フリガナ'},
    'ab': {'en_us': 'Abkhaz',              'ja_jp': 'アブハズ語'},
    'aa': {'en_us': 'Afar',                'ja_jp': 'アファル語'},
    'af': {'en_us': 'Afrikaans',           'ja_jp': 'アフリカーンス語'},
    'ak': {'en_us': 'Akan',                'ja_jp': 'アカン語'},
    'sq': {'en_us': 'Albanian',            'ja_jp': 'アルバニア語'},
    'am': {'en_us': 'Amharic',             'ja_jp': 'アムハラ語'},
    'ar': {'en_us': 'Arabic',              'ja_jp': 'アラビア語'},
    'an': {'en_us': 'Aragonese',           'ja_jp': 'アラゴン語'},
    'hy': {'en_us': 'Armenian',            'ja_jp': 'アルメニア語'},
    'as': {'en_us': 'Assamese',            'ja_jp': 'アッサム語'},
    'av': {'en_us': 'Avaric',              'ja_jp': 'アヴァール語'},
    'ae': {'en_us': 'Avestan',             'ja_jp': 'アヴェスター語'},
    'ay': {'en_us': 'Aymara',              'ja_jp': 'アイマラ語'},
    'az': {'en_us': 'Azerbaijani',         'ja_jp': 'アゼルバイジャン語'},
    'bm': {'en_us': 'Bambara',             'ja_jp': 'バンバラ語'},
    'ba': {'en_us': 'Bashkir',             'ja_jp': 'バシキール語'},
    'eu': {'en_us': 'Basque',              'ja_jp': 'バスク語'},
    'be': {'en_us': 'Belarusian',          'ja_jp': 'ベラルーシ語'},
    'bn': {'en_us': 'Bengali',             'ja_jp': 'ベンガル語'},
    'bh': {'en_us': 'Bihari',              'ja_jp': 'ビハール語'},
    'bi': {'en_us': 'Bislama',             'ja_jp': 'ビスラマ語'},
    'bs': {'en_us': 'Bosnian',             'ja_jp': 'ボスニア語'},
    'br': {'en_us': 'Breton',              'ja_jp': 'ブルトン語'},
    'bg': {'en_us': 'Bulgarian',           'ja_jp': 'ブルガリア語'},
    'my': {'en_us': 'Burmese',             'ja_jp': 'ビルマ語'},
    'ca': {'en_us': 'Catalan',             'ja_jp': 'カタルーニャ語'},
    'ch': {'en_us': 'Chamorro',            'ja_jp': 'チャモロ語'},
    'ce': {'en_us': 'Chechen',             'ja_jp': 'チェチェン語'},
    'ny': {'en_us': 'Chichewa',            'ja_jp': 'チチェワ語'},
    'zh': {'en_us': 'Chinese',             'ja_jp': '中国語'},
    'cv': {'en_us': 'Chuvash',             'ja_jp': 'チュヴァシ語'},
    'kw': {'en_us': 'Cornish',             'ja_jp': 'コーンウォル語'},
    'co': {'en_us': 'Corsican',            'ja_jp': 'コルシカ語'},
    'cr': {'en_us': 'Cree',                'ja_jp': 'クリー語'},
    'hr': {'en_us': 'Croatian',            'ja_jp': 'クロアチア語'},
    'cs': {'en_us': 'Czech',               'ja_jp': 'チェコ語'},
    'da': {'en_us': 'Danish',              'ja_jp': 'デンマーク語'},
    'dv': {'en_us': 'Divehi',              'ja_jp': 'ディベヒ語'},
    'nl': {'en_us': 'Dutch',               'ja_jp': 'オランダ語'},
    'dz': {'en_us': 'Dzongkha',            'ja_jp': 'ゾンカ語'},
    'eo': {'en_us': 'Esperanto',           'ja_jp': 'エスペラント語'},
    'et': {'en_us': 'Estonian',            'ja_jp': 'エストニア語'},
    'ee': {'en_us': 'Ewe',                 'ja_jp': 'エウェ語'},
    'fo': {'en_us': 'Faroese',             'ja_jp': 'フェロー語'},
    'fj': {'en_us': 'Fijian',              'ja_jp': 'フィジー語'},
    'fi': {'en_us': 'Finnish',             'ja_jp': 'フィンランド語'},
    'fr': {'en_us': 'French',              'ja_jp': 'フランス語'},
    'ff': {'en_us': 'Fula',                'ja_jp': 'フラ語'},
    'gl': {'en_us': 'Galician',            'ja_jp': 'ガリシア語'},
    'ka': {'en_us': 'Georgian',            'ja_jp': 'グルジア語'},
    'de': {'en_us': 'German',              'ja_jp': 'ドイツ語'},
    'el': {'en_us': 'Greek',               'ja_jp': 'ギリシャ語'},
    'gn': {'en_us': 'Guaraní',             'ja_jp': 'グアラニー語'},
    'gu': {'en_us': 'Gujarati',            'ja_jp': 'グジャラート語'},
    'ht': {'en_us': 'Haitian',             'ja_jp': 'ハイチ語'},
    'ha': {'en_us': 'Hausa',               'ja_jp': 'ハウサ語'},
    'he': {'en_us': 'Hebrew',              'ja_jp': 'ヘブライ語'},
    'hz': {'en_us': 'Herero',              'ja_jp': 'ヘレロ語'},
    'hi': {'en_us': 'Hindi',               'ja_jp': 'ヒンディー語'},
    'ho': {'en_us': 'Hiri Motu',           'ja_jp': 'ヒリモトゥ語'},
    'hu': {'en_us': 'Hungarian',           'ja_jp': 'ハンガリー語'},
    'ia': {'en_us': 'Interlingua',         'ja_jp': 'インターリングア'},
    'id': {'en_us': 'Indonesian',          'ja_jp': 'インドネシア語'},
    'ie': {'en_us': 'Interlingue',         'ja_jp': 'インターリング'},
    'ga': {'en_us': 'Irish',               'ja_jp': 'アイルランド語'},
    'ig': {'en_us': 'Igbo',                'ja_jp': 'イボ語'},
    'ik': {'en_us': 'Inupiaq',             'ja_jp': 'イヌピア語'},
    'io': {'en_us': 'ido',                 'ja_jp': 'イド語'},
    'is': {'en_us': 'Icelandic',           'ja_jp': 'アイスランド語'},
    'it': {'en_us': 'Italian',             'ja_jp': 'イタリア語'},
    'iu': {'en_us': 'Inuktitut',           'ja_jp': 'イヌイット語'},
    'jv': {'en_us': 'Javanese',            'ja_jp': 'ジャワ語'},
    'kl': {'en_us': 'Kalaallisut',         'ja_jp': 'カラーリット語'},
    'kn': {'en_us': 'Kannada',             'ja_jp': 'カンナダ語'},
    'kr': {'en_us': 'Kanuri',              'ja_jp': 'カヌリ語'},
    'ks': {'en_us': 'Kashmiri',            'ja_jp': 'カシミール語'},
    'kk': {'en_us': 'Kazakh',              'ja_jp': 'カザフ語'},
    'km': {'en_us': 'Khmer',               'ja_jp': 'クメール語'},
    'ki': {'en_us': 'Kikuyu',              'ja_jp': 'キクユ語'},
    'rw': {'en_us': 'Kinyarwanda',         'ja_jp': 'ルワンダ語'},
    'ky': {'en_us': 'Kirghiz',             'ja_jp': 'キルギス語'},
    'kv': {'en_us': 'Komi',                'ja_jp': 'コミ語'},
    'kg': {'en_us': 'Kongo',               'ja_jp': 'コンゴ語'},
    'ko': {'en_us': 'Korean',              'ja_jp': '朝鮮語'},
    'ku': {'en_us': 'Kurdish',             'ja_jp': 'クルド語'},
    'kj': {'en_us': 'Kwanyama',            'ja_jp': 'クワニャマ語'},
    'la': {'en_us': 'Latin',               'ja_jp': 'ラテン語'},
    'lb': {'en_us': 'Luxembourgish',       'ja_jp': 'ルクセンブルク語'},
    'lg': {'en_us': 'Luganda',             'ja_jp': 'ガンダ語'},
    'li': {'en_us': 'Limburgish',          'ja_jp': 'リンブルフ語'},
    'ln': {'en_us': 'Lingala',             'ja_jp': 'リンガラ語'},
    'lo': {'en_us': 'Lao',                 'ja_jp': 'ラーオ語'},
    'lt': {'en_us': 'Lithuanian',          'ja_jp': 'リトアニア語'},
    'lu': {'en_us': 'Luba-Katanga',        'ja_jp': 'ルバ・カタンガ語'},
    'lv': {'en_us': 'Latvian',             'ja_jp': 'ラトビア語'},
    'gv': {'en_us': 'Manx',                'ja_jp': 'マン島語'},
    'mk': {'en_us': 'Macedonian',          'ja_jp': 'マケドニア語'},
    'mg': {'en_us': 'Malagasy',            'ja_jp': 'マダガスカル語'},
    'ms': {'en_us': 'Malay',               'ja_jp': 'マレー語'},
    'ml': {'en_us': 'Malayalam',           'ja_jp': 'マラヤーラム語'},
    'mt': {'en_us': 'Maltese',             'ja_jp': 'マルタ語'},
    'mi': {'en_us': 'Māori',               'ja_jp': 'マオリ語'},
    'mr': {'en_us': 'Marāṭhī',             'ja_jp': 'マラーティー語'},
    'mh': {'en_us': 'Marshallese',         'ja_jp': 'マーシャル語'},
    'mn': {'en_us': 'Mongolian',           'ja_jp': 'モンゴル語'},
    'na': {'en_us': 'Nauru',               'ja_jp': 'ナウル語'},
    'nv': {'en_us': 'Navajo',              'ja_jp': 'ナヴァホ語'},
    'nb': {'en_us': 'Bokmål',              'ja_jp': 'ブークモール語'},
    'nd': {'en_us': 'North Ndebele',       'ja_jp': '北ンデベレ語'},
    'ne': {'en_us': 'Nepali',              'ja_jp': 'ネパール語'},
    'ng': {'en_us': 'Ndonga',              'ja_jp': 'ンドンガ語'},
    'nn': {'en_us': 'Nynorsk',             'ja_jp': 'ニーノシュク語'},
    'no': {'en_us': 'Norwegian',           'ja_jp': 'ノルウェー語'},
    'ii': {'en_us': 'Nuosu',               'ja_jp': 'ノス語'},
    'nr': {'en_us': 'South Ndebele',       'ja_jp': '南ンデベレ語'},
    'oc': {'en_us': 'Occitan',             'ja_jp': 'オック語'},
    'oj': {'en_us': 'Ojibwe',              'ja_jp': 'オジブワ語'},
    'cu': {'en_us': 'Old Church Slavonic', 'ja_jp': '古代教会スラヴ語'},
    'om': {'en_us': 'Oromo',               'ja_jp': 'オロモ語'},
    'or': {'en_us': 'Oriya',               'ja_jp': 'オリヤー語'},
    'os': {'en_us': 'Ossetian',            'ja_jp': 'オセット語'},
    'pa': {'en_us': 'Punjabi',             'ja_jp': 'パンジャーブ語'},
    'pi': {'en_us': 'Pāli',                'ja_jp': 'パーリ語'},
    'fa': {'en_us': 'Persian',             'ja_jp': 'ペルシア語'},
    'pl': {'en_us': 'Polish',              'ja_jp': 'ポーランド語'},
    'ps': {'en_us': 'Pashto',              'ja_jp': 'パシュトー語'},
    'pt': {'en_us': 'Portuguese',          'ja_jp': 'ポルトガル語'},
    'qu': {'en_us': 'Quechua',             'ja_jp': 'ケチュア語'},
    'rm': {'en_us': 'Romansh',             'ja_jp': 'レト・ロマン語'},
    'rn': {'en_us': 'Kirundi',             'ja_jp': 'ルンディ語'},
    'ro': {'en_us': 'Romanian',            'ja_jp': 'ルーマニア語'},
    'ru': {'en_us': 'Russian',             'ja_jp': 'ロシア語'},
    'sa': {'en_us': 'Saṁskṛta',            'ja_jp': 'サンスクリット'},
    'sc': {'en_us': 'Sardinian',           'ja_jp': 'サルデーニャ語'},
    'sd': {'en_us': 'Sindhi',              'ja_jp': 'シンディー語'},
    'se': {'en_us': 'Northern Sami',       'ja_jp': '北部サーミ語'},
    'sm': {'en_us': 'Samoan',              'ja_jp': 'サモア語'},
    'sg': {'en_us': 'Sango',               'ja_jp': 'サンゴ語'},
    'sr': {'en_us': 'Serbian',             'ja_jp': 'セルビア語'},
    'gd': {'en_us': 'Gaelic',              'ja_jp': 'ゲール語'},
    'sn': {'en_us': 'Shona',               'ja_jp': 'ショナ語'},
    'si': {'en_us': 'Sinhala',             'ja_jp': 'シンハラ語'},
    'sk': {'en_us': 'Slovak',              'ja_jp': 'スロバキア語'},
    'sl': {'en_us': 'Slovene',             'ja_jp': 'スロベニア語'},
    'so': {'en_us': 'Somali',              'ja_jp': 'ソマリ語'},
    'st': {'en_us': 'Southern Sotho',      'ja_jp': '南ソト語'},
    'es': {'en_us': 'Spanish',             'ja_jp': 'スペイン語'},
    'su': {'en_us': 'Sundanese',           'ja_jp': 'スンダ語'},
    'sw': {'en_us': 'Swahili',             'ja_jp': 'スワヒリ語'},
    'ss': {'en_us': 'Swati',               'ja_jp': 'スワジ語'},
    'sv': {'en_us': 'Swedish',             'ja_jp': 'スウェーデン語'},
    'ta': {'en_us': 'Tamil',               'ja_jp': 'タミル語'},
    'te': {'en_us': 'Telugu',              'ja_jp': 'テルグ語'},
    'tg': {'en_us': 'Tajik',               'ja_jp': 'タジク語'},
    'th': {'en_us': 'Thai',                'ja_jp': 'タイ語'},
    'ti': {'en_us': 'Tigrinya',            'ja_jp': 'ティグリニャ語'},
    'bo': {'en_us': 'Tibetan',             'ja_jp': 'チベット語'},
    'tk': {'en_us': 'Turkmen',             'ja_jp': 'トルクメン語'},
    'tl': {'en_us': 'Tagalog',             'ja_jp': 'タガログ語'},
    'tn': {'en_us': 'Tswana',              'ja_jp': 'ツワナ語'},
    'to': {'en_us': 'Tonga',               'ja_jp': 'トンガ語'},
    'tr': {'en_us': 'Turkish',             'ja_jp': 'トルコ語'},
    'ts': {'en_us': 'Tsonga',              'ja_jp': 'ツォンガ語'},
    'tt': {'en_us': 'Tatar',               'ja_jp': 'タタール語'},
    'tw': {'en_us': 'Twi',                 'ja_jp': 'トウィ語'},
    'ty': {'en_us': 'Tahitian',            'ja_jp': 'タヒチ語'},
    'ug': {'en_us': 'Uighur',              'ja_jp': 'ウイグル語'},
    'uk': {'en_us': 'Ukrainian',           'ja_jp': 'ウクライナ語'},
    'ur': {'en_us': 'Urdu',                'ja_jp': 'ウルドゥー語'},
    'uz': {'en_us': 'Uzbek',               'ja_jp': 'ウズベク語'},
    've': {'en_us': 'Venda',               'ja_jp': 'ヴェンダ語'},
    'vi': {'en_us': 'Vietnamese',          'ja_jp': 'ベトナム語'},
    'vo': {'en_us': 'Volapük',             'ja_jp': 'ヴォラピュク'},
    'wa': {'en_us': 'Walloon',             'ja_jp': 'ワロン語'},
    'cy': {'en_us': 'Welsh',               'ja_jp': 'ウェールズ語'},
    'wo': {'en_us': 'Wolof',               'ja_jp': 'ウォロフ語'},
    'fy': {'en_us': 'Western Frisian',     'ja_jp': 'フリジア語'},
    'xh': {'en_us': 'Xhosa',               'ja_jp': 'コサ語'},
    'yi': {'en_us': 'Yiddish',             'ja_jp': 'イディッシュ語'},
    'yo': {'en_us': 'Yoruba',              'ja_jp': 'ヨルバ語'},
    'za': {'en_us': 'Zhuang',              'ja_jp': 'チワン語'},
    'zu': {'en_us': 'Zulu',                'ja_jp': 'ズールー語'},
  };
}
