import 'dart:async' show Timer;

import 'package:flutter/material.dart';

class ShownDetector extends StatefulWidget {
  final Widget child;
  final VoidCallback? onInit;
  final VoidCallback? onShown;
  final VoidCallback? onHidden;
  final VoidCallback? onDispose;
  const ShownDetector({
      super.key,
      this.onInit,
      this.onShown,
      this.onHidden,
      this.onDispose,
      required this.child,
  });

  @override
  ShownDetectorState createState() => ShownDetectorState();
}

class ShownDetectorState<T extends ShownDetector> extends State<T>
  with WidgetsBindingObserver  {

  bool? _isShown;
  bool get isShown => _isShown ?? false;

  void _invokeCallback(VoidCallback? callback) {
    if (callback != null) Future.microtask(callback);
  }

  void _invokeOnInit() => _invokeCallback(widget.onInit);
  void _invokeOnShown() => _invokeCallback(widget.onShown);
  void _invokeOnHidden() => _invokeCallback(widget.onHidden);
  void _invokeOnDispose() => _invokeCallback(widget.onDispose);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    _invokeOnInit();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _invokeOnDispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (isShown) {
      if (state == AppLifecycleState.resumed) _invokeOnShown();
    } else if (state == AppLifecycleState.paused) _invokeOnHidden();
  }

  @override
  Widget build(BuildContext context) {
    final isShown = TickerMode.of(context);
    if (_isShown == false) {
      if (isShown) _invokeOnShown();
    } else if (!isShown) _invokeOnHidden();

    _isShown = isShown;

    return widget.child;
  }
}

class TimerShownDetector extends ShownDetector {
  final VoidCallback? onTimer;
  final Duration timerDuration;
  final bool runOnShown;
  const TimerShownDetector({
      super.key,
      super.onInit,
      super.onShown,
      super.onHidden,
      super.onDispose,
      this.onTimer,
      this.timerDuration = const Duration(minutes: 1),
      this.runOnShown = true,
      required super.child,
  });

  @override
  TimerShownDetectorState createState() => TimerShownDetectorState();
}

class TimerShownDetectorState extends ShownDetectorState<TimerShownDetector> {
  Timer? _timer;

  void _setTimer() {
    if (widget.onTimer == null) return;
    _timer ??= Timer.periodic(widget.timerDuration, (_) => widget.onTimer!());
  }

  void _cancelTimer() {
    _timer?.cancel();
    _timer = null;
  }

  @override
  void _invokeOnInit() {
    super._invokeOnInit();
    _setTimer();
  }

  @override
  void _invokeOnShown() {
    super._invokeOnShown();

    if (widget.runOnShown && (widget.onTimer != null)) {
      Future.microtask(widget.onTimer!);
    }
    _setTimer();
  }

  @override
  void _invokeOnHidden() {
    _cancelTimer();
    super._invokeOnHidden();
  }

  void _invokeOnDispose() {
    _cancelTimer();
    super._invokeOnDispose();
  }
}
