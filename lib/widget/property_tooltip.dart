import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_decorated_text/flutter_decorated_text.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';

import '../widget.dart' show divider;

class PropertyTooltip extends StatelessWidget {
  static const defaultSubjectStyle = TextStyle(
    color: Colors.red,
    fontWeight: FontWeight.bold,
  );
  static const defaultObjectStyle = TextStyle(
    color: Colors.blue,
    fontWeight: FontWeight.bold,
  );

  static const defaultMaxWidth = 400.0;

  final String? tooltip;
  final String subject;
  final String object;
  final Widget child;
  final TextStyle? subjectStyle;
  final TextStyle? objectStyle;
  final double maxWidth;
  final List<DecoratorRule>? rules;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final bool? softWrap;
  final double? textScaleFactor;
  final int? maxLines;
  final StrutStyle? strutStyle;
  final TextWidthBasis? textWidthBasis;
  final TextHeightBehavior? textHeightBehavior;
  final bool selectable;

  const PropertyTooltip.maybe(
    this.tooltip,
    this.subject,
    this.object, {
      super.key,
      required this.child,
      this.subjectStyle = defaultSubjectStyle,
      this.objectStyle = defaultObjectStyle,
      this.maxWidth = defaultMaxWidth,
      this.rules,
      this.style,
      this.textAlign,
      this.textDirection,
      this.softWrap,
      this.textScaleFactor,
      this.maxLines,
      this.strutStyle,
      this.textWidthBasis,
      this.textHeightBehavior,
      this.selectable = false,
  });

  @override
  Widget build(BuildContext context) {
    if (tooltip?.isNotEmpty != true) {
      return child;
    }

    return JustTheTooltip(
      child: child,
      content: PropertyTooltipText(
        tooltip!, subject, object,
        subjectStyle: subjectStyle, objectStyle: objectStyle,
        maxWidth: maxWidth, rules: rules, style: style,
        textAlign: textAlign, textDirection: textDirection, softWrap: softWrap,
        textScaleFactor: textScaleFactor, maxLines: maxLines,
        strutStyle: strutStyle, textWidthBasis: textWidthBasis,
        textHeightBehavior: textHeightBehavior, selectable: selectable,
      ),
    );
  }
}

class PropertyTooltipText extends StatelessWidget {
  final String tooltip;
  final String subject;
  final String object;
  final TextStyle? subjectStyle;
  final TextStyle? objectStyle;
  final double maxWidth;
  final List<DecoratorRule>? rules;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final bool? softWrap;
  final double? textScaleFactor;
  final int? maxLines;
  final StrutStyle? strutStyle;
  final TextWidthBasis? textWidthBasis;
  final TextHeightBehavior? textHeightBehavior;
  final bool selectable;

  const PropertyTooltipText(
    this.tooltip,
    this.subject,
    this.object, {
      super.key,
      this.subjectStyle = PropertyTooltip.defaultSubjectStyle,
      this.objectStyle = PropertyTooltip.defaultObjectStyle,
      this.maxWidth = PropertyTooltip.defaultMaxWidth,
      this.rules,
      this.style,
      this.textAlign,
      this.textDirection,
      this.softWrap,
      this.textScaleFactor,
      this.maxLines,
      this.strutStyle,
      this.textWidthBasis,
      this.textHeightBehavior,
      this.selectable = false,
  });

  @override
  Widget build(BuildContext context) {
    var _rules = (rules ?? <DecoratorRule>[])..add(
      DecoratorRule.between(
        start: "[%1", end: "]", removeMatchingCharacters: true,
        style: subjectStyle ?? style ?? const TextStyle(),
      ),
    )..add(
      DecoratorRule.between(
        start: "[%2", end: "]", removeMatchingCharacters: true,
        style: objectStyle ?? style ?? const TextStyle(),
      ),
    );

    Iterable<Widget> texts; {
      var subjectText = subject;
      var objectText = object;

      texts = tooltip.split("\n\n").map(
        (t) => Padding(
          padding: const EdgeInsets.all(5),
          child: DecoratedText(
            text: t.replaceAll(
              "[%1]", "[%1${subjectText}]",
            ).replaceAll(
              "[%2]", "[%2${objectText}]",
            ),
            rules: _rules,
            style: style,
            textAlign: textAlign,
            textDirection: textDirection,
            softWrap: softWrap,
            textScaleFactor: textScaleFactor,
            maxLines: maxLines,
            strutStyle: strutStyle,
            textWidthBasis: textWidthBasis,
            textHeightBehavior: textHeightBehavior,
            selectable: selectable,
          ),
        ),
      );
    }

    var children = <Widget>[];

    var setStates = <Function>[];
    var width = 0.0;

    if (texts.isNotEmpty) {
      children.add(texts.first);

      for (var t in texts.skip(1)) {
        children..add(
          StatefulBuilder(
            builder: (_, setState) {
              setStates.add(setState);

              return Container(
                width: width,
                child: const Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: divider,
                ),
              );
            },
          ),
        )..add(t);
      }
    }

    var columnkey = GlobalKey();

    SchedulerBinding.instance.addPostFrameCallback((_) {
        width = columnkey.currentContext?.size?.width ?? 0;
        for (var f in setStates) {
          f(() {});
        }
      },
    );

    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: maxWidth),
      child: RepaintBoundary(
        key: columnkey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        ),
      ),
    );
  }
}
