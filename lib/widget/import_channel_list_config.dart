import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart';
import 'channel_list_config.dart';

AsyncCellConfig<
 Channel, ChannelUpdate, ChannelUpdateState?
> importChannelCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<Channel, ChannelUpdate, ChannelUpdateState?>
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<
      Channel, ChannelUpdate, ChannelUpdateState?
    >? dismissibleConfig,
}) => importChannelCellConfigWithDefault(
  onTap: (channel, controller) => onChannelCellTap(
    context, ref, channel, controller,
  ),
  onIconTap: (channel, controller) => onChannelCellIconTap(
    context, ref, channel, controller,
  ),
  buttonBuilders: useDefaultButtons ? defaultChannelButtonBuildersWith(
    context, ref,
  ) : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? importChannelDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  Channel, ChannelUpdate, ChannelUpdateState?
> importChannelDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => importChannelDismissibleConfigWithDefault(
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => showError(context, e, s),
);
