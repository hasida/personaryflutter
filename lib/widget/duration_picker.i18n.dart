import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Duration",
    "ja_jp": "期間",
  } +
  {
    "en_us": "days",
    "ja_jp": "日",
  } +
  {
    "en_us": "hours",
    "ja_jp": "時間",
  } +
  {
    "en_us": "min.",
    "ja_jp": "分",
  } +
  {
    "en_us": "sec.",
    "ja_jp": "秒",
  };

  String get i18n => localize(this, t);
}
