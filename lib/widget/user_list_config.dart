import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../dialog/friend_add/friend_add_dialog.dart';
import 'user_list.i18n.dart';

Widget addFriendButtonBuilder<
  T extends HasProfile,
  N extends PlrAsyncNotifier<S>, S extends UserInfo?
>(
  BuildContext context,
  WidgetRef ref,
  N controller,
  T user,
  Entity? referrer,
  AsyncValue<S> state,
  bool isEnabled,
  AsyncCellWrapCallback wrapCallback,
) {
  if (user is! PublicRoot) return Container();

  final account = ref.read(plrAccountProvider).value;
  if ((account == null) || (account.root.plrId == user.plrId)) {
    return _buildAddFriendButton(context, user, false);
  }

  return StreamBuilder(
    stream: account.root.friendOf(user.plrId).handleError((_) {}),
    builder: (context, snapshot) => _buildAddFriendButton(
      context, user,
      (snapshot.connectionState == ConnectionState.done) &&
      (snapshot.data == null),
      account,
    ),
  );
}

Widget _buildAddFriendButton(
  BuildContext context,
  PublicRoot user,
  bool enabled, [
    Account? account,
]) => CompactIconButton(
  icon: const Icon(Icons.person_add),
  tooltip: "Add as a Friend".i18n,
  onPressed: (enabled && (account != null)) ? () => Navigator.push(
    context, CupertinoPageRoute(
      builder: (_) => FriendAddDialog(
        friendRequestString: user.toFriendRequest().toString(),
      ),
      fullscreenDialog: true,
    ),
  ) : null,
);
