import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart';
import '../dialog/channel_edit/channel_edit_util.dart' show DummySchema;
import '../dialog/channel_edit/oss_select.dart';

AsyncCellConfig<Schema, SchemaSync, SchemaState> schemaCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<Schema, SchemaSync, SchemaState>
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<Schema, SchemaSync, SchemaState>? dismissibleConfig,
}) => schemaCellConfigWithDefault(
  onTap: (schema, controller) {
    final root = ref.read(plrAccountProvider).value?.root;
    if (root == null) return;

    final dummySchema = DummySchema.fromSchema(schema);

    showDialog(
      context: context,
      builder: (_) => OssSelectDialog.fromDummySchema(
        root: root, schema: dummySchema,
        onDone: (
          timelineState, ontologyFiles, restrictionFiles, constraintFiles,
          timelineSSFiles, groupSSFiles, channelSSFiles,
        ) {
          dummySchema.update(
            timelineState, ontologyFiles, restrictionFiles, constraintFiles,
            timelineSSFiles, groupSSFiles, channelSSFiles,
          );
          dummySchema.updateSchema(schema);
        },
      ),
    );
  },
  buttonBuilders: useDefaultButtons ? const [] : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? schemaDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<Schema, SchemaSync, SchemaState> schemaDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => schemaDismissibleConfigWithDefault(
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => showError(context, e, s),
);
