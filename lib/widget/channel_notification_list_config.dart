import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../page/channel/channel_page.dart';
import '../page/timeline/timeline_page.dart';
import '../semeditor/widget/channel_semeditor_button.dart';
import '../flavors.dart';
import '../util.dart';
import 'button.dart' show EntityButtonDefToChannelNotificaitonBuilder;

void onChannelNotificationCellTap(
  BuildContext context,
  WidgetRef ref,
  ChannelNotification notification,
  ChannelNotificationUpdate controller,
) => mayOpenTimelinePage(
  context, ref.read(plrAccountProvider).value!, notification.channel!,
  onPop: (_) => controller.refresh(),
);

void onChannelNotificationCellIconTap(
  BuildContext context,
  WidgetRef ref,
  ChannelNotification notification,
  ChannelNotificationUpdate controller,
) => Navigator.push(
  context, CupertinoPageRoute(
    builder: (_) => ChannelPage(notification.channel!),
  ),
).then((_) => controller.refresh());

List<
  AsyncCellButtonBuilder<
    ChannelNotification,
    ChannelNotificationUpdate, ChannelNotificationUpdateState?
  >
> defaultChannelNotificationButtonBuildersWith(
  BuildContext context, WidgetRef ref,
) => [
  if (F.appFlavor != Flavor.PASTELD) ChannelSemEditorButton<
    ChannelNotificationUpdate
  >(
    context, ref.read(plrAccountProvider).value!,
  ).toChannelNotificationBuilder(),
  channelNotificationNotificationButtonBuilder,
];

Future<bool> channelNotificationRemoverWithProgress(
  BuildContext context,
  WidgetRef ref,
  ChannelNotificationUpdate controller,
  ChannelNotification notification,
  Entity? referrer,
  AsyncValue<ChannelNotificationUpdateState?> state,
) async => await runWithProgress(context, () async {
    defaultChannelNotificationRemover(
      context, ref, controller, notification, referrer, state,
    );
  },
) ?? false;

AsyncCellConfig<
  ChannelNotification,
  ChannelNotificationUpdate, ChannelNotificationUpdateState?
> channelNotificationCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  ChannelNotificationsInterface controller, {
    bool useDefaultButtons = true,
    bool useDefaultDismissibleConfig = true,
}) => channelNotificationCellConfigWithDefault(
  onTap: (channelNotification, controller) => onChannelNotificationCellTap(
    context, ref, channelNotification, controller,
  ),
  onIconTap: (
    channelNotification, controller,
  ) => onChannelNotificationCellIconTap(
    context, ref, channelNotification, controller,
  ),
  buttonBuilders: useDefaultButtons ?
    defaultChannelNotificationButtonBuildersWith(context, ref) : const [],
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? channelNotificationDismissibleConfigWith(context, ref, controller)
    : null,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  ChannelNotification,
  ChannelNotificationUpdate, ChannelNotificationUpdateState?
> channelNotificationDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  ChannelNotificationsInterface controller,
) => channelNotificationDismissibleConfigWithDefault(
  objectRemover: channelNotificationRemoverWithProgress,
  // ChannelNotification is automatically removed via controller by Event.
  // onDismissed: (_) => controller.refresh(),
  onError: (e, s) => processNeedNetworkException(context, e, s),
);
