import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart' show showError;

class ProfileView extends ConsumerWidget {
  final ProfileItemsProvider provider;
  final bool showProfiles;
  final bool showAfflications;
  final List<String>? include;
  final List<String>? exclude;
  ProfileView({
      ProfileItemsProvider? provider,
      HasProfile? hasProfile,
      this.showProfiles = true,
      this.showAfflications = true,
      this.include,
      this.exclude,
  }): this.provider = provider ?? profileItemsProvider(
    ArgumentError.checkNotNull(hasProfile, "hasProfile"),
  );

  @override
  Widget build(
    BuildContext context,
    WidgetRef ref,
  ) {
    ref.listen(provider, (_, next) {
        switch (next) {
          case AsyncError(:final error, :final stackTrace): {
            Future.microtask(() => showError(context, error, stackTrace));
          }
        }
    });

    return switch (ref.watch(provider)) {
      AsyncValue(:final value) when (value != null) => _buildProfile(value),
      AsyncLoading() || AsyncData() => const Center(
        child: const CircularProgressIndicator(),
      ),
      _ => Container(),
    };
  }

  Widget _buildProfile(ProfileItemsState value) {
    final dupCheck = <String>{};

    final profileTexts = showProfiles
      ? value.texts.entries.map<Iterable<String>>(
        (e) => (
          include?.contains(e.key.id) ?? true &&
          !(exclude?.contains(e.key.id) ?? false)
        ) ? e.value.where((t) => t.trim().isNotEmpty).map(
          (t) => "${e.key.label?.defaultValue ?? e.key}: ${t}",
        ) : const [],
      ).expand((e) => e.map((t) => dupCheck.add(t) ? Text(t) : null).nonNulls)
      : null;

    dupCheck.clear();

    final affiliationTexts = showAfflications
      ? value.affiliationTexts.where(
        (e) => e.trim().isNotEmpty,
      ).map((t) => dupCheck.add(t) ? Text(t) : null).nonNulls
      : null;

    return Container(
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...?profileTexts, ...?affiliationTexts,
        ],
      ),
    );
  }
}
