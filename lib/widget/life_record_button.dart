import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:plr_ui/plr_ui.dart' as plr show rootSyncProvider;

import '../widget.dart';
import '../page/timeline/timeline_page.dart';
import 'life_record_button.i18n.dart';

class LifeRecordButton extends StatelessPlrWidget {
  final PlrAsyncNotifierProvider<
    RootSyncInterface, RootStateInterface
  > rootSyncProvider;
  final ProfileItemsProvider publicProfileItemsProvider;
  LifeRecordButton(
    super.account, {
      PlrAsyncNotifierProvider<
        RootSyncInterface, RootStateInterface
      >? rootSyncProvider,
      ProfileItemsProvider? publicProfileItemsProvider,
  }):
  this.rootSyncProvider = rootSyncProvider
    ?? plr.rootSyncProvider(account.root),
  this.publicProfileItemsProvider = publicProfileItemsProvider
    ?? profileItemsProvider(account.publicRoot, scanOthers: false);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lifeRecord = ref.watch(rootSyncProvider).value?.lifeRecord;
    final publicProfileItemsState =
      ref.watch(publicProfileItemsProvider).value;

    final publicProfileItemsController = ref.read(
      publicProfileItemsProvider.notifier,
    );

    return CompactIconButton(
      icon: const Icon(Icons.person),
      tooltip: "Life record".i18n,
      onPressed: ((lifeRecord != null) && (publicProfileItemsState != null))
        ? () => mayOpenTimelinePage(
          context, account, lifeRecord,
          onItemUpdated: (editedEntity) => _onItemUpdated(
            context,
            publicProfileItemsState, publicProfileItemsController, editedEntity,
          ),
          onPop: (_) => publicProfileItemsController.refresh(),
        )
        : null,
    );
  }

  void _onItemUpdated(
    BuildContext context,
    ProfileItemsState publicProfileItemsState,
    ProfileItems publicProfileItemsController,
    RecordEntity editedEntity,
  ) async {
    // 新規作成または編集されたプロフィールと同じスキーマクラス、
    // 同じタイトルを持ったプロフィールが既に存在する場合
    // 新規作成または編集されたプロフィールはその開示状況を引き継ぐ
    final editedProfileEntry = ProfileEntry(
      editedEntity.type, ProfileItem(editedEntity.entity!.node),
    );
    final editedProfileKey = nodeHashOf(editedEntity.entity!);

    Schemata? schemata;
    await for (var s in profileSchemata) {
      schemata = s;
    }
    if (schemata == null) return;

    final publicProfileEntryMap = <String, ProfileEntry>{};
    for (final e in publicProfileItemsState.items.entries) {
      publicProfileEntryMap.addEntries(
        e.value.map((i) => MapEntry(nodeHashOf(i), ProfileEntry(e.key, i))),
      );
    }

    final allProfileEntryMap = <String, ProfileEntry>{}; {
      Map<SchemaClass, Iterable<ProfileItem>> profileItems = const {};
      await for (final p in root.profileWith()) {
        profileItems = p;
      }
      for (final e in profileItems.entries) {
        allProfileEntryMap.addEntries(
          e.value.map((i) => MapEntry(nodeHashOf(i), ProfileEntry(e.key, i))),
        );
      }
    }

    if (
      publicProfileEntryMap.values.any(
        (e) => ProfileEntry.sameClassAndTitle(editedProfileEntry, e),
      )
    ) {
      // 公開プロフィールの中に
      // "編集 or 新規作成したプロフィールのタイトルとスキーマが一致する"
      // プロフィール有
      if (!publicProfileEntryMap.containsKey(editedProfileKey)) {
        // 公開プロフィールの中に編集 or 新規作成したプロフィールが存在しない
        // プロフィールを公開プロフィールに追加
        await runWithProgress(
          context,
          () => ProfileDiscloseManager(publicRoot).addItem(
            editedProfileEntry.item,
          ),
        );
        publicProfileItemsController.refresh(force: true);
      }
    } else if(
      allProfileEntryMap.values.any(
        (e) => ProfileEntry.sameClassAndTitle(editedProfileEntry, e),
      )
    ) {
      // 全てのプロフィールの中に
      // "編集 or 新規作成したプロフィールのタイトルとスキーマが一致する"
      // プロフィール有
      if (publicProfileEntryMap.containsKey(editedProfileKey)) {
        // 公開プロフィールの中に編集 or 新規作成したプロフィールが存在する
        // プロフィールを公開プロフィールから削除
        await runWithProgress(
          context,
          () => ProfileDiscloseManager(publicRoot).removeItem(
            editedProfileEntry.item,
          ),
        );
        publicProfileItemsController.refresh(force: true);
      }
    }
  }
}
