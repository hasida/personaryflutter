import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart' show textOrWidget;

import 'duration_picker.i18n.dart';

const _itemExtent = 32.0;
const _pickerWidth = 320.0;
const _pickerHeight = 216.0;
const _magnification = 2.35 / 2.1;
const _squeeze = 1.25;

const _durationPickerMagnification = 34 / 32;
const _durationPickerMinHorizontalPadding = 30.0;
const _durationPickerHalfColumnPadding = 4.0;
const _durationPickerLabelPadSize = 6.0;
const _durationPickerLabelFontSize = 17.0;

const _durationPickerColumnIntrinsicWidth = 106.0;

const _leftSelectionOverlay = CupertinoPickerDefaultSelectionOverlay(
  capEndEdge: false,
);
const _centerSelectionOverlay = CupertinoPickerDefaultSelectionOverlay(
  capStartEdge: false, capEndEdge: false,
);
const _rightSelectionOverlay = CupertinoPickerDefaultSelectionOverlay(
  capStartEdge: false,
);

Future<Duration?> showDurationPickerDialog(
  BuildContext context, {
    Object? title,
    DurationPickerMode? mode,
    Duration initialDuration = Duration.zero,
    bool permitZeroDuration = false,
}) {
  var duration = initialDuration;
  var localizations = MaterialLocalizations.of(context);
  var size = MediaQuery.of(context).size;

  bool isValid() => permitZeroDuration ? true : (duration != Duration.zero);

  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (_) => StatefulBuilder(
      builder: (context, setState) => AlertDialog(
        title: textOrWidget(title ?? "Duration".i18n),
        content: Container(
          width: _pickerWidth,
          height: min(150, size.height),
          child: DurationPicker(
            mode: mode ?? DurationPickerMode.dhm,
            initialDuration: initialDuration,
            onDurationChanged: (d) => setState(() => duration = d),
          ),
        ),
        actions: [
          TextButton(
            child: Text(localizations.cancelButtonLabel),
            onPressed: () => Navigator.pop(context),
          ),
          TextButton(
            child: Text(localizations.okButtonLabel),
            onPressed: isValid() ?
              () => Navigator.pop(context, duration) : null,
          ),
        ],
      ),
    ),
  );
}

enum DurationPickerMode {
  hm, ms, hms, dhm,
}

class DurationPicker extends StatefulWidget {
  final DurationPickerMode mode;
  final Duration initialDuration;
  final int minuteInterval;
  final int secondInterval;
  final AlignmentGeometry alignment;
  final Color? backgroundColor;
  final ValueChanged<Duration>? onDurationChanged;
  DurationPicker({
    super.key,
    this.mode = DurationPickerMode.dhm,
    this.initialDuration = Duration.zero,
    this.minuteInterval = 1,
    this.secondInterval = 1,
    this.alignment = Alignment.center,
    this.backgroundColor,
    this.onDurationChanged,
  });

  @override
  State<DurationPicker> createState() => _DurationPickerState();
}

class _DurationPickerState extends State<DurationPicker> {
  int? selectedDay;
  int? selectedHour;
  int? selectedMinute;
  int? selectedSecond;

  int? lastSelectedDay;
  int? lastSelectedHour;
  int? lastSelectedMinute;
  int? lastSelectedSecond;

  final TextPainter textPainter = TextPainter();
  final List<String> numbers = List<String>.generate(10, (int i) => "${9 - i}");
  late double numberLabelWidth;
  late double numberLabelHeight;
  late double numberLabelBaseline;

  late double dayLabelWidth;
  late double hourLabelWidth;
  late double minuteLabelWidth;
  late double secondLabelWidth;

  late double totalWidth;
  late double pickerColumnWidth;

  @override
  void initState() {
    super.initState();

    if (widget.mode == DurationPickerMode.dhm) {
      selectedDay = widget.initialDuration.inDays;
    }
    if (widget.mode != DurationPickerMode.ms) {
      selectedHour = widget.initialDuration.inHours % 24;
    }
    selectedMinute = widget.initialDuration.inMinutes % 60;

    if (
      (widget.mode != DurationPickerMode.hm) &&
      (widget.mode != DurationPickerMode.dhm)
    ) selectedSecond = widget.initialDuration.inSeconds % 60;

    PaintingBinding.instance.systemFonts.addListener(_handleSystemFontsChange);
  }

  void _handleSystemFontsChange() {
    setState(() {
      textPainter.markNeedsLayout();
      _measureLabelMetrics();
    });
  }

  @override
  void dispose() {
    PaintingBinding.instance.systemFonts
      .removeListener(_handleSystemFontsChange);
    super.dispose();
  }

  @override
  void didUpdateWidget(DurationPicker oldWidget) {
    super.didUpdateWidget(oldWidget);

    assert(
      oldWidget.mode == widget.mode,
      "The DurationPicker's mode cannot change once it's built",
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _measureLabelMetrics();
  }

  double _measureLabelWidth(String label, TextStyle style) {
    textPainter.text = TextSpan(text: label, style: style);
    textPainter.layout();

    return textPainter.maxIntrinsicWidth;
  }

  TextStyle _textStyleFrom(
    BuildContext context, [
      double magnification = 1.0,
  ]) {
    final TextStyle textStyle =
      CupertinoTheme.of(context).textTheme.pickerTextStyle;
    return textStyle.copyWith(
      fontSize: (textStyle.fontSize ?? 14) * magnification,
    );
  }

  void _measureLabelMetrics() {
    final TextStyle textStyle =
      _textStyleFrom(context, _durationPickerMagnification);

    double maxWidth = double.negativeInfinity;
    String? widestNumber;

    for (final String input in numbers) {
      textPainter.textDirection = Directionality.of(context);
      textPainter.text = TextSpan(
        text: input, style: textStyle,
      );
      textPainter.layout();

      if (textPainter.maxIntrinsicWidth > maxWidth) {
        maxWidth = textPainter.maxIntrinsicWidth;
        widestNumber = input;
      }
    }

    textPainter.text = TextSpan(
      text: "$widestNumber$widestNumber",
      style: textStyle,
    );

    textPainter.layout();
    numberLabelWidth = textPainter.maxIntrinsicWidth;
    numberLabelHeight = textPainter.height;
    numberLabelBaseline =
      textPainter.computeDistanceToActualBaseline(TextBaseline.alphabetic);

    if (widget.mode == DurationPickerMode.dhm) {
      dayLabelWidth = _measureLabelWidth("days".i18n, textStyle);
    }
    if (widget.mode != DurationPickerMode.ms)
      hourLabelWidth = _measureLabelWidth("hours".i18n, textStyle);

    minuteLabelWidth = _measureLabelWidth("min.".i18n, textStyle);

    if (
      (widget.mode != DurationPickerMode.hm) &&
      (widget.mode != DurationPickerMode.dhm)
    ) secondLabelWidth = _measureLabelWidth("sec.".i18n, textStyle);
  }

  Widget _buildLabel(String text, EdgeInsetsDirectional pickerPadding) {
    final EdgeInsetsDirectional padding = EdgeInsetsDirectional.only(
      start: numberLabelWidth
             + _durationPickerLabelPadSize
             + pickerPadding.start,
    );

    return IgnorePointer(
      child: Container(
        alignment: AlignmentDirectional.centerStart,
        padding: padding,
        child: SizedBox(
          height: numberLabelHeight,
          child: Baseline(
            baseline: numberLabelBaseline,
            baselineType: TextBaseline.alphabetic,
            child: Text(
              text,
              style: const TextStyle(
                fontSize: _durationPickerLabelFontSize,
                fontWeight: FontWeight.w600,
              ),
              maxLines: 1,
              softWrap: false,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPickerNumberLabel(
    int value,
    EdgeInsetsDirectional padding,
  ) => Container(
    width: _durationPickerColumnIntrinsicWidth + padding.horizontal,
    padding: padding,
    alignment: AlignmentDirectional.centerStart,
    child: Container(
      width: numberLabelWidth,
      alignment: AlignmentDirectional.centerEnd,
      child: Text(
        "${value}",
        softWrap: false, maxLines: 1, overflow: TextOverflow.visible),
    ),
  );

  double _calculateOffAxisFraction(double paddingStart, int position) {
    final double centerPoint = paddingStart + (numberLabelWidth / 2);
    final double pickerColumnOffAxisFraction =
        0.5 - centerPoint / pickerColumnWidth;
    final double durationPickerOffAxisFraction =
        0.5 - (centerPoint + pickerColumnWidth * position) / totalWidth;
    return pickerColumnOffAxisFraction - durationPickerOffAxisFraction;
  }

  Widget _buildDayPicker(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) => CupertinoPicker(
    scrollController: FixedExtentScrollController(initialItem: selectedDay!),
    magnification: _magnification,
    offAxisFraction: _calculateOffAxisFraction(additionalPadding.start, 0),
    itemExtent: _itemExtent,
    backgroundColor: widget.backgroundColor,
    squeeze: _squeeze,
    onSelectedItemChanged: (int index) {
      setState(() {
        selectedDay = index;
        widget.onDurationChanged?.call(
          Duration(
            days: selectedDay!,
            hours: selectedHour!,
            minutes: selectedMinute!,
            seconds: selectedSecond ?? 0,
          ),
        );
      });
    },
    children: List<Widget>.generate(3650, (int index) {
      final String semanticsLabel = "${index} " + "days".i18n;

      return Semantics(
        label: semanticsLabel,
        excludeSemantics: false,
        child: _buildPickerNumberLabel(index, additionalPadding),
      );
    }),
    selectionOverlay: selectionOverlay,
  );

  Widget _buildDayColumn(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) {
    additionalPadding = EdgeInsetsDirectional.only(
      start: max(additionalPadding.start, 0),
      end: max(additionalPadding.end, 0),
    );

    return Stack(
      children: <Widget>[
        NotificationListener<ScrollEndNotification>(
          onNotification: (ScrollEndNotification notification) {
            setState(() => lastSelectedDay = selectedDay);
            return false;
          },
          child: _buildDayPicker(additionalPadding, selectionOverlay),
        ),
        _buildLabel("days".i18n, additionalPadding),
      ],
    );
  }

  Widget _buildHourPicker(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) => CupertinoPicker(
    scrollController: FixedExtentScrollController(initialItem: selectedHour!),
    magnification: _magnification,
    offAxisFraction: _calculateOffAxisFraction(additionalPadding.start, 0),
    itemExtent: _itemExtent,
    backgroundColor: widget.backgroundColor,
    squeeze: _squeeze,
    onSelectedItemChanged: (int index) {
      setState(() {
        selectedHour = index;
        widget.onDurationChanged!(
          Duration(
            days: selectedDay!,
            hours: selectedHour!,
            minutes: selectedMinute!,
            seconds: selectedSecond ?? 0,
          ),
        );
      });
    },
    children: List<Widget>.generate(24, (int index) {
      final String semanticsLabel = "${index} " + "hours".i18n;

      return Semantics(
        label: semanticsLabel,
        excludeSemantics: false,
        child: _buildPickerNumberLabel(index, additionalPadding),
      );
    }),
    selectionOverlay: selectionOverlay,
  );

  Widget _buildHourColumn(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) {
    additionalPadding = EdgeInsetsDirectional.only(
      start: max(additionalPadding.start, 0),
      end: max(additionalPadding.end, 0),
    );

    return Stack(
      children: <Widget>[
        NotificationListener<ScrollEndNotification>(
          onNotification: (ScrollEndNotification notification) {
            setState(() => lastSelectedHour = selectedHour);
            return false;
          },
          child: _buildHourPicker(additionalPadding, selectionOverlay),
        ),
        _buildLabel("hours".i18n, additionalPadding),
      ],
    );
  }

  Widget _buildMinutePicker(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) => CupertinoPicker(
    scrollController: FixedExtentScrollController(
      initialItem: selectedMinute! ~/ widget.minuteInterval,
    ),
    magnification: _magnification,
    offAxisFraction: _calculateOffAxisFraction(
        additionalPadding.start,
        widget.mode == DurationPickerMode.ms ? 0 : 1,
    ),
    itemExtent: _itemExtent,
    backgroundColor: widget.backgroundColor,
    squeeze: _squeeze,
    looping: true,
    onSelectedItemChanged: (int index) {
      setState(() {
        selectedMinute = index * widget.minuteInterval;
        widget.onDurationChanged!(
          Duration(
            days: selectedDay ?? 0,
            hours: selectedHour ?? 0,
            minutes: selectedMinute!,
            seconds: selectedSecond ?? 0,
          ),
        );
      });
    },
    children: List<Widget>.generate(60 ~/ widget.minuteInterval, (int index) {
      final int minute = index * widget.minuteInterval;
      final String semanticsLabel = "${minute}" + "min.".i18n;

      return Semantics(
        label: semanticsLabel,
        excludeSemantics: true,
        child: _buildPickerNumberLabel(minute, additionalPadding),
      );
    }),
    selectionOverlay: selectionOverlay,
  );

  Widget _buildMinuteColumn(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) {
    additionalPadding = EdgeInsetsDirectional.only(
      start: max(additionalPadding.start, 0),
      end: max(additionalPadding.end, 0),
    );

    return Stack(
      children: <Widget>[
        NotificationListener<ScrollEndNotification>(
          onNotification: (ScrollEndNotification notification) {
            setState(() => lastSelectedMinute = selectedMinute);
            return false;
          },
          child: _buildMinutePicker(additionalPadding, selectionOverlay),
        ),
        _buildLabel("min.".i18n, additionalPadding),
      ],
    );
  }

  Widget _buildSecondPicker(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) => CupertinoPicker(
    scrollController: FixedExtentScrollController(
      initialItem: selectedSecond! ~/ widget.secondInterval,
    ),
    magnification: _magnification,
    offAxisFraction: _calculateOffAxisFraction(
        additionalPadding.start,
        widget.mode == DurationPickerMode.ms ? 1 : 2,
    ),
    itemExtent: _itemExtent,
    backgroundColor: widget.backgroundColor,
    squeeze: _squeeze,
    looping: true,
    onSelectedItemChanged: (int index) {
      setState(() {
        selectedSecond = index * widget.secondInterval;
        widget.onDurationChanged!(
          Duration(
            days: selectedDay ?? 0,
            hours: selectedHour ?? 0,
            minutes: selectedMinute!,
            seconds: selectedSecond!,
          ),
        );
      });
    },
    children: List<Widget>.generate(60 ~/ widget.secondInterval, (int index) {
      final int second = index * widget.secondInterval;
      final String semanticsLabel = "${second} " + "sec.".i18n;

      return Semantics(
        label: semanticsLabel,
        excludeSemantics: true,
        child: _buildPickerNumberLabel(second, additionalPadding),
      );
    }),
    selectionOverlay: selectionOverlay,
  );

  Widget _buildSecondColumn(
    EdgeInsetsDirectional additionalPadding,
    Widget selectionOverlay,
  ) {
    additionalPadding = EdgeInsetsDirectional.only(
      start: max(additionalPadding.start, 0),
      end: max(additionalPadding.end, 0),
    );

    return Stack(
      children: <Widget>[
        NotificationListener<ScrollEndNotification>(
          onNotification: (ScrollEndNotification notification) {
            setState(() => lastSelectedSecond = selectedSecond);
            return false;
          },
          child: _buildSecondPicker(additionalPadding, selectionOverlay),
        ),
        _buildLabel("sec.".i18n, additionalPadding),
      ],
    );
  }

  @override
  Widget build(BuildContext context) => LayoutBuilder(
    builder: (context, constraints) {
      late List<Widget> columns;

      if (
        (widget.mode == DurationPickerMode.hms) ||
        (widget.mode == DurationPickerMode.dhm)
      ) {
        pickerColumnWidth =
          _durationPickerColumnIntrinsicWidth +
          (_durationPickerHalfColumnPadding * 2);
        totalWidth = pickerColumnWidth * 3;
      } else {
        totalWidth = _pickerWidth;
        pickerColumnWidth = totalWidth / 2;
      }

      if (constraints.maxWidth < totalWidth) {
        totalWidth = constraints.maxWidth;
        pickerColumnWidth =
        totalWidth / (
          (
            (widget.mode == DurationPickerMode.hms) ||
            (widget.mode == DurationPickerMode.dhm)
          ) ? 3 : 2
        );
      }

      final double baseLabelContentWidth =
        numberLabelWidth + _durationPickerLabelPadSize;
      final double minuteLabelContentWidth =
        baseLabelContentWidth + minuteLabelWidth;

      switch (widget.mode) {
        case DurationPickerMode.hm: {
          final double hourLabelContentWidth =
            baseLabelContentWidth + hourLabelWidth;

          double hourColumnStartPadding =
            pickerColumnWidth
            - hourLabelContentWidth - _durationPickerHalfColumnPadding;
          if (hourColumnStartPadding < _durationPickerMinHorizontalPadding) {
            hourColumnStartPadding = _durationPickerMinHorizontalPadding;
          }
          double minuteColumnEndPadding =
            pickerColumnWidth
            - minuteLabelContentWidth - _durationPickerHalfColumnPadding;
          if (minuteColumnEndPadding < _durationPickerMinHorizontalPadding)
            minuteColumnEndPadding = _durationPickerMinHorizontalPadding;

          columns = [
            _buildHourColumn(
              EdgeInsetsDirectional.only(
                start: hourColumnStartPadding,
                end: pickerColumnWidth
                     - hourColumnStartPadding - hourLabelContentWidth,
              ),
              _leftSelectionOverlay,
            ),
            _buildMinuteColumn(
              EdgeInsetsDirectional.only(
                start: pickerColumnWidth
                       - minuteColumnEndPadding - minuteLabelContentWidth,
                end: minuteColumnEndPadding,
              ),
              _rightSelectionOverlay,
            ),
          ];
          break;
        }
        case DurationPickerMode.ms: {
          final double secondLabelContentWidth =
            baseLabelContentWidth + secondLabelWidth;
          double secondColumnEndPadding =
            pickerColumnWidth
            - secondLabelContentWidth - _durationPickerHalfColumnPadding;
          if (secondColumnEndPadding < _durationPickerMinHorizontalPadding)
            secondColumnEndPadding = _durationPickerMinHorizontalPadding;

          double minuteColumnStartPadding =
            pickerColumnWidth 
            - minuteLabelContentWidth - _durationPickerHalfColumnPadding;
          if (minuteColumnStartPadding < _durationPickerMinHorizontalPadding)
            minuteColumnStartPadding = _durationPickerMinHorizontalPadding;

          columns = [
            _buildMinuteColumn(
              EdgeInsetsDirectional.only(
                start: minuteColumnStartPadding,
                end: pickerColumnWidth
                     - minuteColumnStartPadding - minuteLabelContentWidth,
              ),
              _leftSelectionOverlay,
            ),
            _buildSecondColumn(
              EdgeInsetsDirectional.only(
                start: pickerColumnWidth
                       - secondColumnEndPadding - minuteLabelContentWidth,
                end: secondColumnEndPadding,
              ),
              _rightSelectionOverlay,
            ),
          ];
          break;
        }
        case DurationPickerMode.hms: {
          final double hourColumnEndPadding =
            pickerColumnWidth
            - baseLabelContentWidth - hourLabelWidth
            - _durationPickerMinHorizontalPadding;
          final double minuteColumnPadding =
            (pickerColumnWidth - minuteLabelContentWidth) / 2;
          final double secondColumnStartPadding =
            pickerColumnWidth
            - baseLabelContentWidth - secondLabelWidth
            -_durationPickerMinHorizontalPadding;

          columns = [
            _buildHourColumn(
              EdgeInsetsDirectional.only(
                start: _durationPickerMinHorizontalPadding,
                end: max(hourColumnEndPadding, 0),
              ),
              _leftSelectionOverlay,
            ),
            _buildMinuteColumn(
              EdgeInsetsDirectional.only(
                start: minuteColumnPadding,
                end: minuteColumnPadding,
              ),
              _centerSelectionOverlay,
            ),
            _buildSecondColumn(
              EdgeInsetsDirectional.only(
                start: max(secondColumnStartPadding, 0),
                end: _durationPickerMinHorizontalPadding,
              ),
              _rightSelectionOverlay,
            ),
          ];
          break;
        }
        case DurationPickerMode.dhm: {
          final double hourLabelContentWidth =
            baseLabelContentWidth + hourLabelWidth;

          final double dayColumnEndPadding =
            pickerColumnWidth
            - baseLabelContentWidth - dayLabelWidth
            - _durationPickerMinHorizontalPadding;
          final double hourColumnPadding =
            (pickerColumnWidth - hourLabelContentWidth) / 2;
          final double minuteColumnStartPadding =
            pickerColumnWidth
            - baseLabelContentWidth - minuteLabelWidth
            -_durationPickerMinHorizontalPadding;

          columns = [
            _buildDayColumn(
              EdgeInsetsDirectional.only(
                start: _durationPickerMinHorizontalPadding,
                end: max(dayColumnEndPadding, 0),
              ),
              _leftSelectionOverlay,
            ),
            _buildHourColumn(
              EdgeInsetsDirectional.only(
                start: hourColumnPadding,
                end: hourColumnPadding,
              ),
              _centerSelectionOverlay,
            ),
            _buildMinuteColumn(
              EdgeInsetsDirectional.only(
                start: max(minuteColumnStartPadding, 0),
                end: _durationPickerMinHorizontalPadding,
              ),
              _rightSelectionOverlay,
            ),
          ];
          break;
        }
      }

      final CupertinoThemeData themeData = CupertinoTheme.of(context);
      return MediaQuery.withNoTextScaling(
        child: CupertinoTheme(
          data: themeData.copyWith(
            textTheme: themeData.textTheme.copyWith(
              pickerTextStyle: _textStyleFrom(
                context, _durationPickerMagnification,
              ),
            ),
          ),
          child: Align(
            alignment: widget.alignment,
            child: Container(
              color: CupertinoDynamicColor.maybeResolve(
                widget.backgroundColor, context,
              ),
              width: totalWidth,
              height: _pickerHeight,
              child: DefaultTextStyle(
                style: _textStyleFrom(context),
                child: Row(
                  children: columns.map(
                    (child) => Expanded(child: child)).toList(growable: false),
                ),
              ),
            ),
          ),
        ),
      );
    },
  );
}
