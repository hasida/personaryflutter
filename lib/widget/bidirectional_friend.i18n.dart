import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Friend request",
    "ja_jp": "友達申請",
  } +
  {
    "en_us": "Friend entry was deleted.",
    "ja_jp": "友達登録解除",
  } +
  {
    "en_us": "Friend entry `%s' deleted because the user no more exists.",
    "ja_jp": "利用者「%s」が存在しないので友達登録を解除しました。",
  } +
  {
    "en_us": "Are you sure you want to send this user a friend request?",
    "ja_jp": "このユーザーに友達申請を送信してよろしいですか?",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
