import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  retrievingTranslation +
  unknownTranslation +
  {
    "en_us": "Add as a Friend",
    "ja_jp": "友達に追加",
  };

  String get i18n => localize(this, t);
}
