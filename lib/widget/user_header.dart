import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import 'user_header.i18n.dart';
import 'plr_widget.dart' show StatelessConsumerPlrWidget;

const _leadingImageSize = 64.0;

class UserHeader<U extends HasProfile> extends StatelessConsumerPlrWidget {
  final U user;
  final UserInfoUpdateProvider _provider;
  final List<Widget>? buttons;
  UserHeader(
    this.user, {
      super.key,
      UserInfoUpdateProvider? provider,
      String Function(UserData? userData)? labelOfMe,
      this.buttons,
  }): _provider = provider ?? userInfoUpdateProvider(
    user, labelOfMe: labelOfMe ?? defaultLabelOfMe,
  );

  late final UserInfoUpdate _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _controller = ref.read(_provider.notifier);
  }

  void _initListeners(BuildContext context, WidgetRef ref) {
    final index = userIndexOf(user);
    if (!index.isValid) return;

    ref.listen(
      singleUserDataChangedProvider(index.storageId, index.id),
      (_, next) => _controller.refresh(),
    );
  }

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    _initListeners(context, ref);

    final state = ref.watch(_provider);
    final value = state.value;

    var name = value?.name ?? (
      (state.isLoading ? "Retrieving..." : "Unknown").i18n
    );

    var picture = value?.picture;

    var email;
    if (user is HasPlrId) email = (user as HasPlrId).plrId?.email;

    return ListTile(
      contentPadding: EdgeInsets.zero,
      leading: SizedBox(
        width: _leadingImageSize,
        height: _leadingImageSize,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: (picture == null) ?
            const Icon(Icons.person, size: _leadingImageSize) :
            memoryImageOf(picture),
        ),
      ),
      title: Text(name, overflow: TextOverflow.ellipsis),
      subtitle: (email != null) ?
        Text(email, overflow: TextOverflow.ellipsis) : null,
      trailing: (buttons != null) ? Row(
        mainAxisSize: MainAxisSize.min, children: buttons!,
      ) : null,
    );
  }
}
