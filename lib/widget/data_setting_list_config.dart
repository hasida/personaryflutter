import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart';
import '../dialog/channel_edit/data_setting_dialog.dart';

AsyncCellConfig<
  DataSetting, DataSettingSync, DataSettingState
> dataSettingCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<DataSetting, DataSettingSync, DataSettingState>
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<DataSetting, DataSettingSync, DataSettingState>? dismissibleConfig,
}) => dataSettingCellConfigWithDefault(
  onTap: (dataSetting, controller) {
    final Root root;
    final NotificationRegistry notificationRegistry; {
      final account = ref.read(plrAccountProvider).value;
      if (account == null) return;

      root = account.root;
      notificationRegistry = account.notificationRegistry;
    }

    showDialog(
      context: context,
      builder: (_) => DataSettingDialog(
        root: root, dataSetting: dataSetting,
        notificationRegistry: notificationRegistry,
      ),
    );
  },
  buttonBuilders: useDefaultButtons ? const [] : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? dataSettingDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  DataSetting, DataSettingSync, DataSettingState
> dataSettingDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => dataSettingDismissibleConfigWithDefault(
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => showError(context, e, s),
);
