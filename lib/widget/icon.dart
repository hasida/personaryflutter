import 'package:flutter/material.dart';

Widget copyPassphraseIconOf(
  double size, {
    Color? copyIconColor,
    Color? keyIconColor,
}) => Stack(
  alignment: AlignmentDirectional.center,
  children: [
    Icon(Icons.copy, size: size, color: copyIconColor),
    Align(
      widthFactor: 2,
      alignment: const AlignmentDirectional(.25, .1),
      child: Icon(Icons.vpn_key, size: size / 2, color: keyIconColor),
    ),
  ],
);

Widget depositPassphraseIconOf(
  double size, {
    Color? publishIconColor,
    Color? keyIconColor,
}) => Stack(
  alignment: AlignmentDirectional.center,
  children: [
    Align(
      alignment: const Alignment(0, -.2),
      child: Icon(Icons.publish, size: size / 8 * 7, color: publishIconColor),
    ),
    Align(
      alignment: const Alignment(0, .4),
      child: Icon(Icons.vpn_key, size: size / 2, color: keyIconColor),
    ),
  ],
);

Widget releasePassphraseIconOf(
  double size, {
    Color favoriteIconColor = Colors.pink,
    Color? keyIconColor,
}) => Stack(
  children: [
    Icon(Icons.favorite_border, size: size, color: favoriteIconColor),
    Align(
      alignment: const Alignment(0, -.25),
      child: Icon(Icons.vpn_key, size: size / 2, color: keyIconColor),
    ),
  ],
);

Widget announcementBubbleIconOf(
  double size, [
    Color color = Colors.red,
]) => Stack(
  children: [
    const Icon(Icons.chat_bubble, size: 16, color: Colors.white),
    Icon(Icons.announcement, size: 16, color: color),
  ],
);
final unreadIcon = announcementBubbleIconOf(16);
