import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../page/channel/channel_page.dart';
import '../page/timeline/timeline_page.dart';
import '../semeditor/widget/channel_semeditor_button.dart';
import '../util.dart';
import 'button.dart' show EntityButtonDefToBuilder;

void onChannelCellTap(
  BuildContext context,
  WidgetRef ref,
  Channel channel,
  ChannelUpdate controller,
) => mayOpenTimelinePage(
  context, ref.read(plrAccountProvider).value!, channel,
  onPop: (_) => controller.refresh(),
);

void onChannelCellIconTap(
  BuildContext context,
  WidgetRef ref,
  Channel channel,
  ChannelUpdate controller,
) => Navigator.push(
  context, CupertinoPageRoute(
    builder: (_) => ChannelPage(channel),
  ),
).then((_) => controller.refresh());

List<AsyncCellButtonBuilder<T, N, S>> defaultChannelButtonBuildersWith<
  T extends Channel,
  N extends PlrAsyncNotifier<S>, S extends ChannelUpdateStateInterface?
>(BuildContext context, WidgetRef ref) => [
  ChannelSemEditorButton<N>(
    context, ref.read(plrAccountProvider).value!,
  ).toBuilder(),
  channelNotificationButtonBuilder,
];

Future<bool> channelRemoverWithProgress(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier<ChannelUpdateStateInterface?> controller,
  ChannelBase channel,
  Entity? referrer,
  AsyncValue<ChannelUpdateStateInterface?> state,
) async => await runWithProgress(
  context, () => defaultChannelRemover(
    context, ref, controller, channel, referrer, state,
  ),
) ?? false;

AsyncCellConfig<
 T, ChannelUpdate, ChannelUpdateState?
> channelCellConfigWith<T extends Channel>(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<T, ChannelUpdate, ChannelUpdateState?>
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<T, ChannelUpdate, ChannelUpdateState?>? dismissibleConfig,
}) => channelCellConfigWithDefault(
  onTap: (channel, controller) => onChannelCellTap(
    context, ref, channel, controller,
  ),
  onIconTap: (channel, controller) => onChannelCellIconTap(
    context, ref, channel, controller,
  ),
  buttonBuilders: useDefaultButtons ? defaultChannelButtonBuildersWith(
    context, ref,
  ) : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? channelDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  T, ChannelUpdate, ChannelUpdateState?
> channelDismissibleConfigWith<T extends Channel>(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => channelDismissibleConfigWithDefault(
  objectRemover: channelRemoverWithProgress,
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => processNeedNetworkException(context, e, s),
);
