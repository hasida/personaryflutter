import 'package:barcode/barcode.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../style.dart';
import 'friend_request_panel.i18n.dart';

class FriendRequestPanel extends StatelessWidget {
  final FriendRequest request;
  FriendRequestPanel(this.request);

  Widget build(BuildContext context) {
    var requestUri = request.toUri().toString();

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: leftPadding[3],
          child: Text("Request code".i18n),
        ),
        TextButton(
          style: TextButton.styleFrom(
            foregroundColor: Colors.black87,
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
            ),
          ),
          onPressed: () => Clipboard.setData(
            ClipboardData(text: requestUri),
          ),
          child: Row(
            children: [
              Expanded(child: Text(requestUri)),
              const Icon(Icons.copy),
            ],
          ),
        ),
        Center(
          child: Padding(
            padding: verticalPadding[3],
            child: SvgPicture.string(
              Barcode.qrCode().toSvg(
                requestUri, width: 160, height: 160,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
