import 'dart:async' show StreamController;

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util.dart' show showError;
import '../widget.dart';
import 'bidirectional_friend.i18n.dart';

class BidirectionalFriendIcon extends ConsumerWidget {
  final Friend friend;
  const BidirectionalFriendIcon(this.friend);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(friendSyncProvider(friend));
    switch (state) {
      case AsyncLoading(): return emptyWidget;
      case AsyncError(:final error, :final stackTrace): {
        Future.microtask(() => showError(context, error, stackTrace));
      }
    }

    return StreamBuilder<bool?>(
      stream: _checkIsBidirectional(context, ref, state),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return emptyWidget;

        return CompactIconButton(
          icon: snapshot.data! ?
            const Icon(Icons.favorite_border, color: Colors.pink) :
            const Icon(Icons.bolt, color: Colors.orange),
          onPressed: snapshot.data! ? null : () async {
            if (
              !await showConfirmDialog(
                context, "Friend request".i18n,
                "Are you sure you want to send this user a friend request?".i18n,
              )
            ) return;

            await sendMyRequestByStorage(friend.myStorage, friend);
          },
        );
      },
    );
  }

  Stream<bool?> _checkIsBidirectional(
    BuildContext context,
    WidgetRef ref,
    AsyncValue<FriendState> state,
  ) {
    final controller = StreamController<bool?>();

    final checkExistenceFuture = _checkExistence(context, ref, state);

    Future(() async {
        bool? isBidirectional; try {
          controller.add(await friend.isLocalBidirectional);
          controller.add(isBidirectional = await friend.isBidirectional);
        } catch (_) {
          return;
        }
        if (await checkExistenceFuture) {
          _autoFriendRequest(friend, isBidirectional);
        }
    }).whenComplete(controller.close);

    return controller.stream;
  }

  static final _checkingExistence = <String>{};

  Future<bool> _checkExistence(
    BuildContext context,
    WidgetRef ref,
    AsyncValue<FriendState> state,
  ) async {
    if (!_checkingExistence.add(friend.id)) return false;

    try {
      if (await state.value?.isValid == true) return true;

      if (!context.mounted) return false;

      final rootContext =
        context.findRootAncestorStateOfType<NavigatorState>()?.context;
      if (rootContext == null) return false;

      final account = ref.read(plrAccountProvider).value;
      if (account == null) return false;

      final friendsController = ref.read(friendsProvider.notifier);
      FriendSync? friendSyncController; {
        final p = friendSyncProvider(friend);
        if (ref.exists(p)) friendSyncController = ref.read(p.notifier);
      }

      try {
        if (!await unregisterFriend(
            account.root, friend,
            notificationRegistry: account.notificationRegistry,
            infoRegistry: account.infoRegistry,
          )
        ) return false;
      } catch (_) {
        return false;
      }
      friendsController.refresh();
      friendSyncController?.refresh();

      await showConfirmDialog(
        rootContext, "Friend entry was deleted.".i18n,
        "Friend entry `%s' deleted because the user no more exists.".i18n.fill([
            friend.id,
        ]), showCancelButton: false,
      );
    } finally {
      _checkingExistence.remove(friend.id);
    }
    return false;
  }

  ///自動フレンド申請
  Future<void> _autoFriendRequest(Friend friend, bool? isBidirectional) async {
    var key = 'friend_request_${friend.plrId.toString()}';

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (isBidirectional != false) {
      prefs.remove(key);
      return;
    }

    final now = DateTime.now();

    var list = prefs.getStringList(key);
    var flg = ((list == null) || list.isEmpty);

    if (!flg || (list != null && list.length < 3)) {
      ///自動フレンド申請の実行回数が３回未満の場合、自動でフレンド申請を送る
      flg = true;
      for (final str in list) {
        final dt = DateTime.parse(str);
        if(dt.add(const Duration(milliseconds : 10)).compareTo(now) > 0) {
          ///一日以内にフレンド申請していた場合、自動ではフレンド申請を送らない
          flg = false;
          break;
        }
      }
    }
    if (flg) {
      sendMyRequestByStorage(friend.myStorage, friend);
      list ??= <String>[];
      list.add(DateTime.now().toString());
      prefs.setStringList(key, list);
    }
  }
}
