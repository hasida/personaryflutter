import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart';

Future<bool> disclosedToChannelRemoverWithProgress(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier<ChannelUpdateStateInterface?> controller,
  DisclosedToChannel disclosedToChannel,
  Entity? referrer,
  AsyncValue<ChannelUpdateStateInterface?> state,
) async => await runWithProgress(
  context, () => defaultDisclosedToChannelRemover(
    context, ref, controller, disclosedToChannel, referrer, state,
  ),
) ?? false;

AsyncCellConfig<
  DisclosedToChannel, ChannelUpdate, ChannelUpdateState?
> disclosedToChannelCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<
        DisclosedToChannel, ChannelUpdate, ChannelUpdateState?
      >
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<
      DisclosedToChannel, ChannelUpdate, ChannelUpdateState?
    >? dismissibleConfig,
}) => disclosedToChannelCellConfigWithDefault(
  buttonBuilders: useDefaultButtons ? const [] : buttonBuilders,
  useDefaultDismissibleConfig: false,
  dismissibleConfig: useDefaultDismissibleConfig
    ? disclosedToChannelDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  DisclosedToChannel, ChannelUpdate, ChannelUpdateState?
> disclosedToChannelDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  PlrAsyncNotifier controller,
) => disclosedToChannelDismissibleConfigWithDefault(
  objectRemover: disclosedToChannelRemoverWithProgress,
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => showError(context, e, s),
);
