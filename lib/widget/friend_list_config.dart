import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../page/friend/friend_page.dart';
import '../util.dart';
import 'bidirectional_friend.dart';

void onFriendCellTap(
  BuildContext context,
  WidgetRef ref,
  Friend friend,
  UserInfoUpdate controller,
) => Navigator.push(
  context, CupertinoPageRoute(
    builder: (_) => FriendPage(friend),
  ),
);

Widget friendBidirectionalButtonBuilder(
  BuildContext _,
  WidgetRef __,
  UserInfoUpdate ___,
  Friend friend,
  Entity? ____,
  AsyncValue<UserInfo?> _____,
  bool ______,
  AsyncCellWrapCallback _______,
) => BidirectionalFriendIcon(friend);

final defaultFriendButtonBuilders = <
  AsyncCellButtonBuilder<Friend, UserInfoUpdate, UserInfo?>
>[
  friendBidirectionalButtonBuilder,
  friendNotificationButtonBuilder,
];

Future<bool> friendRemoverWithProgress(
  BuildContext context,
  WidgetRef ref,
  UserInfoUpdate controller,
  Friend friend,
  Entity? referrer,
  AsyncValue<UserInfo?> state,
) async => await runWithProgress(
  context, () => defaultFriendRemover(
    context, ref, controller, friend, referrer, state,
  ),
) ?? false;

AsyncCellConfig<Friend, UserInfoUpdate, UserInfo?> friendCellConfigWith(
  BuildContext context,
  WidgetRef ref,
  FriendsInterface controller, {
    bool useDefaultButtons = true,
    List<
      AsyncCellButtonBuilder<Friend, UserInfoUpdate, UserInfo?>
    > buttonBuilders = const [],
    bool useDefaultDismissibleConfig = true,
    DismissibleConfig<Friend, UserInfoUpdate, UserInfo?>? dismissibleConfig,
}) => userCellConfigWithDefault(
  onTap: (friend, controller) => onFriendCellTap(
    context, ref, friend, controller,
  ),
  buttonBuilders: useDefaultButtons
    ? defaultFriendButtonBuilders : buttonBuilders,
  dismissibleConfig: useDefaultDismissibleConfig
    ? friendDismissibleConfigWith(context, ref, controller)
    : dismissibleConfig,
  onError: (e, s) => showError(context, e, s),
);

DismissibleConfig<
  Friend, UserInfoUpdate, UserInfo?
> friendDismissibleConfigWith(
  BuildContext context,
  WidgetRef ref,
  FriendsInterface controller,
) => friendDismissibleConfigWithDefault(
  objectRemover: friendRemoverWithProgress,
  onDismissed: (_) => controller.refresh(),
  onError: (e, s) => processNeedNetworkException(context, e, s),
);
