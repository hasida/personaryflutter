import 'package:flutter/material.dart' hide SearchController;
import 'package:flutter_riverpod/flutter_riverpod.dart' hide Notifier;
import 'package:plr_ui/plr_ui.dart';

import 'button.i18n.dart';

typedef ButtonCallback<T, N> = void Function(T object, N notifier);

class ButtonDef<T, N extends PlrNotifier> {
  final Widget icon;
  final Color color;
  final String? tooltip;
  final bool force;
  final ButtonCallback<T, N>? onPressed;
  ButtonDef(
    this.icon, {
      this.color = Colors.black,
      this.tooltip,
      this.force = false,
      this.onPressed,
  });
}

class EntityButtonDef<T extends Entity, N extends PlrNotifier>
  extends ButtonDef<T, N> {

  EntityButtonDef(
    super.icon, {
      super.color,
      super.tooltip,
      super.force,
      super.onPressed,
  });
}

AsyncCellButtonBuilder<T, N, S> buttonDefToBuilder<
  T extends Entity, N extends PlrAsyncNotifier<S>, S
>(EntityButtonDef<T, N> def) => (
  BuildContext context,
  WidgetRef ref,
  N controller,
  T object,
  Entity? referrer,
  AsyncValue<S> state,
  bool isEnabled,
  AsyncCellWrapCallback wrapCallback,
) => CompactIconButton(
  icon: def.icon, color: def.color, tooltip: def.tooltip,
  onPressed: _buildOnPressed(
    context, ref, controller, isEnabled, wrapCallback, def, object,
  ),
);

AsyncCellButtonBuilder<
  ChannelNotification, N, S
> buttonDefToChannelNotificationBuilder<
  N extends PlrAsyncNotifier<S>, S extends ChannelNotificationUpdateState?
>(
  EntityButtonDef<Channel, N> def) => (
  BuildContext context,
  WidgetRef ref,
  N controller,
  ChannelNotification object,
  Entity? referrer,
  AsyncValue<S> state,
  bool isEnabled,
  AsyncCellWrapCallback wrapCallback,
) => CompactIconButton(
  icon: def.icon, color: def.color, tooltip: def.tooltip,
  onPressed: _buildOnPressed(
    context, ref, controller, isEnabled, wrapCallback, def, object.channel,
  ),
);

VoidCallback? _buildOnPressed<
  T extends Entity, N extends PlrAsyncNotifier
>(
  BuildContext context,
  WidgetRef ref,
  N controller,
  bool isEnabled,
  AsyncCellWrapCallback wrapCallback,
  EntityButtonDef<T, N> def,
  T? object,
) {
  if (
    (!def.force && !isEnabled) || (def.onPressed == null) || (object == null)
  ) return null;

  final _onPressed = () => def.onPressed!(object, controller);
  return (def.force) ? _onPressed : wrapCallback(context, ref, _onPressed);
}

extension EntityButtonDefToBuilder<
  T extends Entity, N extends PlrAsyncNotifier<S>, S
> on EntityButtonDef<T, N> {
  AsyncCellButtonBuilder<T, N, S> toBuilder() => buttonDefToBuilder(this);
}

extension EntityButtonDefToChannelNotificaitonBuilder<
  N extends PlrAsyncNotifier<S>, S extends ChannelNotificationUpdateState?
> on EntityButtonDef<Channel, N> {
  AsyncCellButtonBuilder<
    ChannelNotification, N, S
  > toChannelNotificationBuilder() =>
    buttonDefToChannelNotificationBuilder(this);
}

class ShowAllButton extends Container {
  ShowAllButton({
      super.width = double.infinity,
      super.height,
      Icon icon = const Icon(Icons.open_in_new),
      Widget? label,
      Color backgroundColor = Colors.grey,
      Color foregroundColor = Colors.white,
      Color borderColor = Colors.grey,
      VoidCallback? onPressed,
  }): super(
    child: TextButton.icon(
      icon: icon,
      label: label ?? Text("Show all".i18n),
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        shape: BeveledRectangleBorder(
          side: BorderSide(color: borderColor),
        ),
        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
      ),
      onPressed: onPressed,
    ),
  );
}

class InlineAddButton extends StatelessWidget {
  final double? width;
  final double? height;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final Color? backgroundColor;
  final Object? heroTag;
  final String? tooltip;
  final VoidCallback? onPressed;
  const InlineAddButton({
      super.key,
      this.width = 24,
      this.height,
      this.margin,
      this.padding,
      this.backgroundColor,
      this.heroTag,
      this.tooltip,
      this.onPressed,
  });

  Widget build(BuildContext context) => Container(
    width: width, height: height, margin: margin, padding: padding,
    child: FloatingActionButton(
      heroTag: heroTag,
      tooltip: tooltip,
      backgroundColor: backgroundColor
        ?? Theme.of(context).colorScheme.secondary,
        child: Icon(
          Icons.add, color: Theme.of(context).colorScheme.onSecondary,
        ),
      onPressed: onPressed,
    ),
  );
}
