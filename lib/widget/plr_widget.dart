import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../util.dart' show showError;
import '../page/sign/sign_page.dart';

bool processError(BuildContext context, AsyncValue value) {
  switch (value) {
    case AsyncError(:final error, :final stackTrace): {
      showError(context, error, stackTrace);
      return true;
    }
  }
  return false;
}

abstract class StatelessPlrWidget extends HookConsumerWidget with AccountMixin {
  final Account account;
  const StatelessPlrWidget(
    this.account, {
      super.key,
  });
}

abstract class StatelessConsumerPlrWidget extends HookConsumerWidget
  with AccountMixin, SetAccountMixin {

  static final _isInitialized = Expando<bool>();

  bool get isInitialized => _isInitialized[this] ?? false;
  set isInitialized(bool isInitialized) =>
    _isInitialized[this] = isInitialized ? true : null;

  const StatelessConsumerPlrWidget({ super.key });

  void init(BuildContext context, WidgetRef ref) {}

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    setAccount(context, ref);

    if (!isInitialized) {
      init(context, ref);
      isInitialized = true;
    }
    return doBuild(context, ref);
  }

  Widget doBuild(BuildContext context, WidgetRef ref);
}

abstract class StatefulPlrWidget extends StatefulHookConsumerWidget
  with AccountMixin {

  final Account account;
  const StatefulPlrWidget(
    this.account, {
      super.key,
  });
}

abstract class PlrState<T extends StatefulPlrWidget> extends ConsumerState<T>
  with AccountMixin {

  Account get account => widget.account;
}

abstract class StatefulConsumerPlrWidget extends StatefulHookConsumerWidget {
  const StatefulConsumerPlrWidget({ super.key });
}

abstract class PlrConsumerState<T extends ConsumerStatefulWidget>
  extends ConsumerState<T> with AccountMixin, SetAccountMixin {

  @override
  void initState() {
    super.initState();
    setAccount(context, ref);
  }
}

mixin SetAccountMixin on AccountMixin {
  static final _accounts = Expando<Account>();

  Account get account => _accounts[this]!;
  set account(Account account) => _accounts[this] = account;

  void setAccount(BuildContext context, WidgetRef ref) {
    var _account = ref.read(plrAccountProvider).value;
    if (_account == null) {
      Future.microtask(
        () => Navigator.pushAndRemoveUntil(
          context, CupertinoPageRoute(
            builder: (_) => const SignPage(),
          ),
          (_) => false,
        ),
      );
      return;
    }
    account = _account;
  }
}
