import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../style.dart';
import '../util/misc.dart' show listHeightOf;
import '../dialog/show_all/show_all_dialog.dart';
import 'button.dart' show ShowAllButton;
import 'layout.i18n.dart';

const divider = const Divider(height: 0, color: Colors.grey);

const limitedListDivider = const Divider(height: 1, indent: 3, endIndent: 3);

class _EmptyPage extends StatelessWidget{
  const _EmptyPage();

  @override
  Widget build(BuildContext context) => Container(
    color: Theme.of(context).colorScheme.surfaceContainer,
  );
}

const emptyWidget = SizedBox.shrink();
const emptyPage = _EmptyPage();

class SectionHeader extends StatelessWidget {
  final String title;
  final EdgeInsets? padding;
  const SectionHeader(
    this.title, {
      super.key,
      this.padding,
  });

  @override
  Widget build(BuildContext context) => Padding(
    padding: padding ?? allPadding[2],
    child: Text(title, style: Theme.of(context).textTheme.headlineLarge),
  );
}

typedef LimitedListBuilder = Widget Function(
  BuildContext context,
  WidgetRef ref,
  bool shrinkWrap,
);

Widget buildLimitedListSection<
  T extends HasId, N extends PlrAsyncNotifier<S>, S extends StateInterface
>(
  BuildContext context,
  WidgetRef ref, {
    required PlrAsyncNotifierProvider<N, S> provider,
    bool readOnly = true,
    required String title,
    String? showAllButtonTooltip,
    Widget? showAllLabel,
    required AsyncListItemsGetter<T, S> itemsGetter,
    List<Widget> Function(List<T> items)? buttonsBuilder,
    required LimitedListBuilder listBuilder,
}) => Consumer(
  key: ValueKey("limited_list_section_${title}"),
  builder: (context, ref, _) {
    final state = ref.watch(provider);

    List<T> items; {
      final value = state.value;
      if (value == null) return Container(
        width: double.infinity,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: const CircularProgressIndicator(),
      );

      if ((items = itemsGetter(value)).isEmpty && readOnly) {
        return emptyWidget;
      }
    }
    final itemsCount = items.length;

    final buttons = buttonsBuilder?.call(items) ?? const [];

    void showAll() => Navigator.push(
      context, CupertinoPageRoute(
        builder: (_) => ShowAllDialog(
          provider, title: title, itemsGetter: itemsGetter,
          buttons: buttons, listBuilder: listBuilder,
        ),
        fullscreenDialog: true,
      ),
    );

    late ValueNotifier<bool> overflowed;

    return Column(
      key: ValueKey("limited_list_column_${title}"),
      children: [
        Row(
          children: [
            Expanded(
              child: Hero(
                tag: title,
                flightShuttleBuilder: textFlightShuttleBuilder,
                child: SectionHeader(
                  "${title}" + ((itemsCount > 0) ? " (${itemsCount})" : ""),
                ),
              ),
            ),
            ...buttons,
            HookBuilder(
              builder: (_) {
                overflowed = useState(false);
                return _buildShowAllButton(
                  tooltip: showAllButtonTooltip ?? "Show all".i18n,
                  onPressed: overflowed.value ? showAll : null,
                );
              },
            ),
          ],
        ),
        buildLimitedList(
          context, ref, showAllLabel: showAllLabel,
          onShowAllPressed: showAll,
          onStateChanged: (state) {
            overflowed.value = state == LimitedScrollViewState.overflowed;
          },
          listBuilder: listBuilder,
        ),
      ],
    );
  },
);


Widget textFlightShuttleBuilder(
  BuildContext flightContext,
  Animation<double> animation,
  HeroFlightDirection flightDirection,
  BuildContext fromHeroContext,
  BuildContext toHeroContext,
) => DefaultTextStyle(
  style: DefaultTextStyle.of(toHeroContext).style,
  child: toHeroContext.widget,
);

Widget _buildShowAllButton({
    String? tooltip,
    VoidCallback? onPressed,
}) => CompactIconButton(
  icon: const Icon(Icons.open_in_new),
  tooltip: tooltip,
  onPressed: onPressed,
);

Widget buildLimitedList(
  BuildContext context,
  WidgetRef ref, {
    Widget? showAllLabel,
    required VoidCallback onShowAllPressed,
    LimitedScrollViewStateCallback? onStateChanged,
    required LimitedListBuilder listBuilder,
}) => LimitedScrollView(
  scroll: false,
  maxHeight: listHeightOf(context),
  trailingBuilder: (_, state) => switch (state) {
    LimitedScrollViewState.overflowed => Column(
      children: [
        limitedListDivider,
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 3),
          child: ShowAllButton(
            label: showAllLabel,
            onPressed: onShowAllPressed,
          ),
        ),
      ],
    ),
    _ => Container(),
  },
  onStateChanged: onStateChanged,
  child: listBuilder(context, ref, true),
);
