import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Open app setting",
    "ja_jp": "アプリ設定を開く",
  };

  String get i18n => localize(this, t);
}
