import 'dart:collection' show Queue;

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:_plr_common/src/privates.dart' show StringCapitalize;

import '../../app.dart';
import '../../util.dart';
import '../../page/main/active_app_definition.dart';
import 'app_definition.i18n.dart';

Future<bool> applyAppDefinition(
  BuildContext context,
  WidgetRef ref,
  Root root,
  AppDefinition appDefinition, {
    ActiveAppDefinitionProvider? provider,
    ActiveAppDefinition? controller,
}) async {
  final roles = appDefinition.roles;
  Future<Map<String, List<ProfileItem>>>? currentRolesFuture;
  if (roles.isNotEmpty) {
    currentRolesFuture = _getCurrentRoles(root);
    if (
      !await showConfirmDialog(
        context, "Confirmation".i18n,
        "This app will add and publish the following roles to your profile:\n"
        "  %s\nAre you sure?.".i18n.fill([
            appDefinition.roles.map(
              (r) => " - ${r.defaultValue.toString()}",
            ).join("\n"),
        ]),
      )
    ) return false;
  }
  if (
    !await showConfirmDialog(
      context, "Confirmation".i18n,
      "Change the app definition and restart this app.\n"
      "Are you sure?.".i18n,
    )
  ) return false;

  if (currentRolesFuture != null) {
    final List<Literal>? addedRoles; try {
      addedRoles = await _mayAddRoles(
        context, ref, root, roles, currentRolesFuture,
      );
    } catch (e, s) {
      processNeedNetworkException(context, e, s);
      return false;
    }
    if (addedRoles == null) return false;
  }

  await (
    controller ?? ref.read(
      (provider ?? activeAppDefinitionProvider(root.storage)).notifier,
    )
  )!.setId(appDefinition.id);

  _restartApp(context);

  return true;
}

Future<Map<String, List<ProfileItem>>> _getCurrentRoles(
  HasProfile hasProfile,
) async {
  Iterable<ProfileItem>? roleItems;
  await for (
    final p in hasProfile.profileWith(
      force: true, classes: const [ ProfileClass.role ],
    )
  ) {
    roleItems = p.values.firstOrNull;
  }
  if (roleItems == null) return const {};

  final m = <String, List<ProfileItem>>{};
  for (final i in roleItems) {
    final name = i.contents.firstOrNull?.text.value.toString();
    if (name == null) continue;

    (m[name] ??= <ProfileItem>[]).add(i);
  }
  return m;
}

Future<List<Literal>?> _mayAddRoles(
  BuildContext context,
  WidgetRef ref,
  Root root,
  Iterable<Literal> roles,
  Future<Map<String, List<ProfileItem>>> currentRolesFuture,
) => runWithProgress(context, () async {
    final lifeRecord = root.lifeRecord;
    if (lifeRecord == null) {
      showMessage(context, "Failed to get the life record.");
      return null;
    }
    await lifeRecord.sync().drain();

    final publicRoot = await root.storage.publicRoot;

    final Disclosure disclosure; {
      final nodeHash = publicRoot.nodeHash;
      disclosure = lifeRecord.disclosures.firstWhereOrNull(
        (d) => d.nodeHash == nodeHash,
      ) ?? lifeRecord.newDisclosure(publicRoot)
        ..title.value = publicProfileDisclosureTitle;

      if (await disclosure.disclose()) await disclosure.syncSilently();
    }
    await publicRoot.sync().drain();

    final roleProperty = ProfileClass.role.uncapitalize();

    final sourceNodeHashes = publicRoot.propertyModelsOf<ProfileItem>(
      roleProperty,
    ).map((i) => i.sourceNodeHash).nonNulls.toSet();

    final currentRoles = await currentRolesFuture;
    final currentRoleNames = currentRoles.keys.toSet();

    final addedRoles = <Literal>[];
    var published = false;

    final messages = Queue<String>();
    Future<void>? showMessagesFuture;
    Future<void> showMessages() async {
      while (messages.isNotEmpty) {
        final m = messages.removeFirst();
        showMessage(context, m);
        await Future.delayed(const Duration(seconds: 1));
      }
      showMessagesFuture = null;
    }
    void queueMessage(String message) {
      messages.add(message);
      if (showMessagesFuture == null) {
        showMessagesFuture = showMessages();
      }
    }

    await Future.wait(
      roles.map((r) async {
          final commonName = r.value.toString();
          final name = r.defaultValue.toString();

          final String message;
          final List<ProfileItem> items;
          if (currentRoleNames.contains(commonName)) {
            message =
              "The role `%s\' is already registered."
              " Use existing item.".i18n.fill([ name ]);
            items = currentRoles[commonName]!;
          } else {
            final item = (
              await lifeRecord.newTimelineItemModel(ProfileClass.role)
            )
            .as<ProfileItem>()
            ..creator = root.id
            ..addPropertyModel(cntProperty, r)
            ..commitNewVersionModel();

            await item.syncSilently();
            item.registerRare();

            addedRoles.add(r);

            message = "The role `%s\' has been added.".i18n.fill([ name ]);
            items = [ item ];
          }
          queueMessage(message);

          published |= await _mayPublish(
            context, disclosure, roleProperty, sourceNodeHashes,
            commonName, name, items, queueMessage,
          );
      }),
    );
    if (published || addedRoles.isNotEmpty) {
      await lifeRecord.syncSilently();
    }
    await Future.wait([
        if (published) Future(() async {
            await disclosure.disclose();
            await disclosure.syncSilently();
        }),
        showMessagesFuture ?? showMessages(),
    ]);
    if (published) {
      ref.read(profileItemsProvider(root).notifier).refresh(force: true);
      ref.read(
        profileItemsProvider(publicRoot, scanOthers: false).notifier,
      ).refresh(force: true);
    }
    return addedRoles;
  },
  onError: (e, s) => showError(context, e, s),
);

Future<bool> _mayPublish(
  BuildContext context,
  Disclosure disclosure,
  String roleProperty,
  Set<String> sourceNodeHashes,
  String commonName,
  String name,
  List<ProfileItem> items,
  void queueMessage(String message),
) async {
  if (items.any((i) => sourceNodeHashes.contains(i.nodeHash))) return false;

  final condition = disclosure.newCondition(roleProperty); {
    if (condition == null) return false;

    final builder = PropertyPathBuilder(condition)..append(cntProperty);
    if (items.first.contents.first.isFile) builder.append(titleProperty);
    builder.lang = noLang;

    condition.addRequirement(StringRequirement(builder.build(), commonName));
  }
  condition.addRequirement(
    TimeRequirement(
      (PropertyPathBuilder(condition)..append(beginProperty)).build(),
      TimeRequirement.now, TimeRequirementType.PartInclude,
      endPropertyPath: (
        PropertyPathBuilder(condition)..append(endProperty)
      ).build(),
      endTime: TimeRequirement.infinity,
    ),
  );
  queueMessage("The role `%s\' has been published.".i18n.fill([ name ]));

  return true;
}

Future<bool> clearAppDefinition(
  BuildContext context,
  WidgetRef ref,
  Root root, {
    ActiveAppDefinitionProvider? provider,
    ActiveAppDefinition? controller,
}) async {
  if (
    !await showConfirmDialog(
      context, "Confirmation".i18n,
      "Unset the app definition and restart the standard app.\n"
      " Are you sure?.".i18n,
    )
  ) return false;

  await (
    controller ?? ref.read(
      (provider ?? activeAppDefinitionProvider(root.storage)).notifier,
    )
  )!.clear();

  _restartApp(context);

  return true;
}

void _restartApp(BuildContext context) => App.restart(context);
