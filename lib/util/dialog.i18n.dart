import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Confirmation",
    "ja_jp": "確認",
  } +
  {
    "en_us":
      "The friend was added as a subscribing user of the `Announcement'.\n\n"
      "You can unsubscribe any time by tapping the config button "
      "at the top right of the `Announcement' page",
    "ja_jp":
      "この友達を、「お知らせ」の購読対象者として追加しました。\n\n"
      "「お知らせ」ページ右上の設定ボタンから、"
      "いつでも購読を解除することができます。",
  } +
  {
    "en_us": "Your passphrase has been deposited.",
    "ja_jp": "パスフレーズを預けました",
  } +
  {
    "en_us": "Your passphrase has been deposited to the friend(s) below, who cannot impersonate you just by your passphrase.",
    "ja_jp": "下記の友達にパスフレーズを預けました(パスフレーズだけであなたになりすますことはできません)。",
  } +
  {
    "en_us": "If you have forgotten your passphrase, please ask one of these depositories to copy it into her clipboard at your friend detail screen in her Personary and send it to you via SMS, etc.\nYou can view and edit the depositories by the `Depository' button below or `Passphrase depositing' dialog in `Settings' screen.",
    "ja_jp": "パスフレーズを忘れた場合は、いずれかのパスフレーズ預け先のPersonaryのあなたの友達詳細画面でパスフレーズをクリップボードにコピーしてSMS等で送ってもらって下さい。\n下記の「預け先一覧」ボタンか、「設定」画面の 「パスフレーズ預け先」ダイアログで、パスフレーズの預け先を確認・設定することができます。",
  } +
  {
    "en_us": "Depositories",
    "ja_jp": "預け先一覧",
  };

  String get i18n => localize(this, t);
}
