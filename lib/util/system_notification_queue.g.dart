// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'system_notification_queue.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$systemNotificationQueueHash() =>
    r'2639fe9282ce7420a3e2760e84149f31b7093cd2';

/// See also [SystemNotificationQueue].
@ProviderFor(SystemNotificationQueue)
final systemNotificationQueueProvider =
    NotifierProvider<SystemNotificationQueue, SystemNotificationQueue>.internal(
  SystemNotificationQueue.new,
  name: r'systemNotificationQueueProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$systemNotificationQueueHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SystemNotificationQueue = Notifier<SystemNotificationQueue>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
