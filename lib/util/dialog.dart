import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide SearchController;
import 'package:plr_ui/plr_ui.dart';

import '../dialog/passphrase_depositing/passphrase_depositing_dialog.dart';
import 'dialog.i18n.dart';

Future<void> showAnnouncementSubscribedDialog(
  BuildContext context,
) => showConfirmDialog(
  context, "Confirmation".i18n,
  "The friend was added as a subscribing user of the `Announcement'.\n\n"
  "You can unsubscribe any time by tapping the config button "
  "at the top right of the `Announcement' page".i18n,
  showCancelButton: false,
);

Future<bool> showPassphraseDepositedDialog(
  BuildContext context,
  Iterable<Friend> friends,
) => showConfirmDialog(
  context, "Your passphrase has been deposited.".i18n,
  CupertinoScrollbar(
    child: SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Your passphrase has been deposited to the friend(s) below, who cannot impersonate you just by your passphrase.".i18n),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Text(
              friends.map((f) => f.plrId.email).join("\n"),
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Text("If you have forgotten your passphrase, please ask one of these depositories to copy it into her clipboard at your friend detail screen in her Personary and send it to you via SMS, etc.\nYou can view and edit the depositories by the `Depository' button below or `Passphrase depositing' dialog in `Settings' screen.".i18n,
          ),
        ],
      ),
    ),
  ),
  okButtonLabel: MaterialLocalizations.of(context).closeButtonLabel,
  cancelButtonLabel: "Depositories".i18n,
  onCancel: () => Navigator.of(context, rootNavigator: true).push(
    CupertinoPageRoute(
      builder: (_) => const PassphraseDepositingDialog(),
      fullscreenDialog: true,
    ),
  ),
);
