import 'package:flutter/material.dart';
import 'package:googleapis/drive/v3.dart' hide App;
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:plr_ui/plr_ui.dart';

import '../app.dart';
import 'show_message.i18n.dart';

void _printError(Object e, StackTrace? s) {
  print(e);
  if (s != null) print(s);
}

bool _inInvalidGrantProcess = false;

void showError(BuildContext context, Object e, [ StackTrace? s ]) {
  if (_inInvalidGrantProcess) return;

  _printError(e, s);

  if (e is InvalidGrantException) {
    // On mobile device,
    // the access token will be refreshed by GoogleSignIn package.
    if (isMobile) return;

    _inInvalidGrantProcess = true;
    _onInvalidGrant(context, e.type, e.userId).whenComplete(
      () => _inInvalidGrantProcess = false,
    );
    return;
  }

  if (
    (e is EntryNotFoundException) ||
    isNetworkException(e) || isExceptionToIgnore(e) || (
      (e is BadRequestException) && (e.cause is DetailedApiRequestError) &&
      (e.cause.status == 500) && (e.cause.message == "Internal Error")
    )
  ) return;

  showMessage(context, e.toString());
}

Future<void> _onInvalidGrant(
  BuildContext context,
  StorageType type,
  String? userId,
) => showDialog(
  context: context,
  builder: (context) => ProgressHUD(
    child: Builder(
      builder: (context) => AlertDialog(
        title: Text("Invalid or insufficient grant to access".i18n),
        content: Text(
          (userId != null) ?
            "Please re-authenticate `%s\' on %s".i18n.fill([
                userId, type.displayName!,
            ]) :
            "Please re-authenticate %s".i18n.fill([ type.displayName! ]),
        ),
        actions: <Widget>[
          TextButton(
            child: Text("Authenticate".i18n),
            onPressed: () async {
              var progress = ProgressHUD.of(context)!;
              progress.show();
              try {
                await type.create(params: { userIdParam: userId });
              } catch (e) {
                showMessage(context, e.toString());
                return;
              } finally {
                progress.dismiss();
              }
              Navigator.pop(context);
              App.restart(context);
            },
          ),
        ],
      ),
    ),
  ),
);

bool processNeedNetworkException(BuildContext context, Object e, StackTrace s) {
  if (!isNetworkException(e)) {
    showError(context, e, s);
    return false;
  }
  _printError(e, s);
  showNeedNetworkDialog(context);

  return true;
}

void showNeedNetworkDialog(BuildContext context) => showDialog(
  context: context,
  builder: (context) => AlertDialog(
    title: Text('Failed to connect network'.i18n),
    content: Text(
      'This action needs network connection.\nRetry later at the place have fine network condition, or check the connectivity to target server.'.i18n,
    ),
    actions: <Widget>[
      TextButton(
        child: Text(MaterialLocalizations.of(context).okButtonLabel),
        onPressed: () => Navigator.pop(context),
      ),
    ],
  ),
);
