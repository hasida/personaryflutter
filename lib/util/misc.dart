import 'dart:io' show Platform;
import 'dart:math' show max;

import 'package:android_id/android_id.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

extension DateTimeNullComparison on DateTime? {
  bool isAfterNullable(DateTime? other) =>
    (this == null) ? false : (other == null) ? true : this!.isAfter(other);

  bool isBeforeNullable(DateTime? other) =>
    (this == null) ? true : (other == null) ? false : this!.isBefore(other);
}

extension StreamFirstLastOrNull<T> on Stream<T> {
  Future<T?> get firstOrNull async {
    try {
      return await first;
    } on StateError {}
    return null;
  }

  Future<T?> firstWhereOrNull(bool test(T element)) async {
    try {
      return await firstWhere(test);
    } on StateError {}
    return null;
  }

  Future<T?> get lastOrNull async {
    try {
      return await last;
    } on StateError {}
    return null;
  }

  Future<T?> lastWhereOrNull(bool test(T element)) async {
    try {
      return await lastWhere(test);
    } on StateError {}
    return null;
  }
}

String encodeQueryParameters(Map<String, String> params) => params.entries.map(
  (e) => "${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}",
).join("&");

Future<String> getDeviceId() async {
  if (Platform.isAndroid) return (await const AndroidId().getId())!;

  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) return (await deviceInfo.iosInfo).identifierForVendor!;
  else if (Platform.isWindows) return (await deviceInfo.windowsInfo).deviceId;
  else if (Platform.isMacOS) return (await deviceInfo.macOsInfo).systemGUID!;
  else if (Platform.isLinux) return (await deviceInfo.linuxInfo).machineId!;

  throw StateError("Failed to get device ID.");
}

const minListHeight = 560.0;

double listHeightOf(BuildContext context) => max(
  MediaQuery.of(context).size.height / 3, minListHeight,
);

Future<bool> updatePublicDisclosure(
  LifeRecord? lifeRecord,
  PublicRoot? publicRoot, {
    VoidCallback? onDisclose,
}) async {
  if ((lifeRecord == null) || (publicRoot == null)) return false;
  await lifeRecord.syncSilently();

  var publicRootId = publicRoot.nodeHash;

  for (var d in lifeRecord.disclosures) {
    if (d.nodeHash == publicRootId) {
      if (await d.disclose().catchError((_) => false)) {
        onDisclose?.call();
        await d.syncSilently();
        return true;
      }
      break;
    }
  }
  return false;
}
