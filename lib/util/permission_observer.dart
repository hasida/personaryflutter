import 'dart:async' show Timer;

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:synchronized/extension.dart';

import 'permission_observer.i18n.dart';

class PermissionObserver {
  final State state;
  final Permission permission;
  final String noGrantMessage;
  final void Function(Permission permission, bool isGranted)? onStateChanged;
  PermissionObserver(
    this.state,
    this.permission,
    this.noGrantMessage, [
      this.onStateChanged,
  ]);

  Timer? _statusTimer;
  ScaffoldFeatureController? _statusController;

  bool _isGranted = false;
  bool get isGranted => _isGranted;
  set isGranted(bool isGranted) {
    if (isGranted == _isGranted) return;
    _isGranted = isGranted;

    onStateChanged?.call(permission, isGranted);
  }

  Future<void> observe(BuildContext context) => synchronized(() async {
      if (!(isGranted = await permission.status.isGranted)) {
        _statusController ??= ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: const Duration(days: 365),
            content: Text(noGrantMessage),
            action: SnackBarAction(
              label: "Open app setting".i18n,
              onPressed: openAppSettings,
            ),
          ),
        )..closed.then((reason) {
            _statusController = null;
            if (
              !state.mounted ||
              (reason == SnackBarClosedReason.action)
            ) return;
            observe(context);
        });
      } else _statusController?.close();

      if (_statusTimer == null) {
        _statusTimer = Timer.periodic(
          const Duration(seconds: 10), (_) => observe(context),
        );
      }
  });

  void onChangeAppLifecycleState(
    BuildContext context,
    AppLifecycleState state,
  ) {
    switch (state) {
      case AppLifecycleState.paused: {
        _statusTimer?.cancel();
        _statusTimer = null;
        break;
      }
      case AppLifecycleState.resumed: observe(context);

      default:
    }
  }

  void dispose() {
    _statusTimer?.cancel();
    _statusController?.close();
  }
}
