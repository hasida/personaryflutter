
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Invalid or insufficient grant to access",
    "ja_jp": "アクセス許可が無効か不足しています",
  } +
  {
    "en_us": "Please re-authenticate `%s\' on %s",
    "ja_jp": "%2\$sの「%1\$s」を再認証してください。",
  } +
  {
    "en_us": "Please re-authenticate %s",
    "ja_jp": "%sを再認証してください。",
  } +
  {
    "en_us": "Authenticate",
    "ja_jp": "認証",
  } +
  {
    "en_us": "Failed to connect network",
    "ja_jp": "ネットワーク接続ができませんでした",
  } +
  {
    "en_us": "This action needs network connection.\nRetry later at the place have fine network condition, or check the connectivity to target server.",
    "ja_jp": "この操作には、ネットワーク接続が必要です。\nネットワークに接続できる環境で再度実行するか、対象のサーバへの接続状況を確認してください。",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
