import 'dart:collection' show Queue;

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:plr_ui/plr_ui.dart' hide Notifier;
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../logic.dart';

part 'system_notification_queue.g.dart';

extension _QueueTakeFirstOrNull<T> on Queue<T> {
  T? removeFirstOrNull() => isNotEmpty ? removeFirst() : null;
}

extension ChannelIndexEvent on Channel {
  static final _indexEvents = Expando<IndexEvent>();

  IndexEvent? get indexEvent => _indexEvents[this];
  set indexEvent(IndexEvent? indexEvent) => _indexEvents[this] = indexEvent;
}

typedef _ListenerCallback = void Function(
  ReceivedNotification notification,
  Map<String, String?> payload,
);

class ChannelIndexNotification {
  final ChannelNotification notification;
  final String itemId;
  const ChannelIndexNotification._(this.notification, this.itemId);
}

class ChannelRequest {
  final Storage storage;
  final String? channelId;
  final Iterable<String> itemTags;
  const ChannelRequest._(
    this.storage,
    this.channelId, [
      this.itemTags = const [],
  ]);
}

class _NotificationController {
  static late final SystemNotificationQueue _queue;
  static void init(SystemNotificationQueue queue) {
    _queue = queue;
  }

  @pragma("vm:entry-point")
  static Future<void> onActionReceived(
    ReceivedAction receivedAction,
  ) => _listener(
    receivedAction,
    onContentUpdates: _queue._addContentUpdates,
    onPassphraseChanged: _queue._addPassphraseChanged,
    onFriend: _queue._addFriend,
    onHealthDataRetrieved: _queue._addHealthDataRetrieved,
    onChannelIndexNotification: _queue._addChannelIndex,
    onChannelDisclosureConsentRequest:
      _queue._addChannelDisclosureConsentRequest,
    onFidoRegistrationTokenResult: _queue._addFidoRegistrationTokenResult,
    onDidIssuanceResult: _queue._addDidIssuanceResult,
    onVcIssuanceResult: _queue._addVcIssuanceResult,
  );

  @pragma("vm:entry-point")
  static Future<void> onNotificationDisplayed(
    ReceivedNotification receivedNotification,
  ) => _listener(
    receivedNotification,
    onPassphraseChanged: _queue._addPassphraseChanged,
  );

  @pragma("vm:entry-point")
  static Future<void> onDismissActionReceived(
    ReceivedAction receivedAction,
  ) => _listener(
    receivedAction,
    onContentUpdates: _queue._checkContentUpdates,
    onPassphraseChanged: _queue._addPassphraseChanged,
    onChannelIndexNotification: _queue._removeChannelIndex,
    onChannelDisclosureConsentRequest:
      _queue._removeChannelDisclosureConsentRequest,
    onFidoRegistrationTokenResult: _queue._removeFidoRegistrationTokenResult,
    onDidIssuanceResult: _queue._removeDidIssuanceResult,
    onVcIssuanceResult: _queue._removeVcIssuanceResult,
    onFinish: _queue._mayDecrementBadgeCounter,
  );

  static Future<void> _listener(
    ReceivedNotification notification, {
      _ListenerCallback? onContentUpdates,
      _ListenerCallback? onPassphraseChanged,
      _ListenerCallback? onFriend,
      _ListenerCallback? onHealthDataRetrieved,
      _ListenerCallback? onChannelIndexNotification,
      _ListenerCallback? onChannelDisclosureConsentRequest,
      _ListenerCallback? onFidoRegistrationTokenResult,
      _ListenerCallback? onDidIssuanceResult,
      _ListenerCallback? onVcIssuanceResult,
      _ListenerCallback? onFinish,
  }) async {
    var payload = notification.payload;
    if (payload == null) return;

    switch (notification.channelKey) {
      case contentUpdatesChannel:
        onContentUpdates?.call(notification, payload); break;
      case accountChannel:
        onPassphraseChanged?.call(notification, payload); break;
      case friendChannel: onFriend?.call(notification, payload); break;
      case healthDataCoordinationChannel:
        onHealthDataRetrieved?.call(notification, payload); break;
      case channelIndexNotificationChannel:
        onChannelIndexNotification?.call(notification, payload); break;
      case requestChannel: switch (parseNotificationRequestType(payload)) {
        case NotificationRequestType.channelDisclosureConsent:
          onChannelDisclosureConsentRequest?.call(notification, payload);
          break;
        case NotificationRequestType.fidoRegistrationTokenResult:
          onFidoRegistrationTokenResult?.call(notification, payload);
          break;
        case NotificationRequestType.didIssuanceResult:
          onDidIssuanceResult?.call(notification, payload);
          break;
        case NotificationRequestType.vcIssuanceResult:
          onVcIssuanceResult?.call(notification, payload);
          break;
        default:
      }
      break;
    }
    onFinish?.call(notification, payload);
  }
}

@Riverpod(keepAlive: true)
class SystemNotificationQueue extends _$SystemNotificationQueue {
  SystemNotificationQueue build() {
    _NotificationController.init(this);

    if (!isDesktop) AwesomeNotifications().setListeners(
      onActionReceivedMethod: _NotificationController.onActionReceived,
      onNotificationDisplayedMethod:
        _NotificationController.onNotificationDisplayed,
      onDismissActionReceivedMethod:
        _NotificationController.onDismissActionReceived,
    );
    return this;
  }

  @override
  bool updateShouldNotify(
    SystemNotificationQueue previous,
    SystemNotificationQueue next,
  ) => true;


  final _contentUpdatesQueue = Queue<Notification>();

  Notification? get nextContentUpdates =>
    _contentUpdatesQueue.removeFirstOrNull();

  Future<T?> _notificationOf<T extends Notification>(
    String? notificationId,
  ) async {
    if (notificationId == null) return null;

    try {
      return await notificationRegistryManager.notificationOf(notificationId);
    } catch (e, s) {
      print(e); print(s);
    }
    return null;
  }

  Future<void> _addContentUpdates(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var nId = parseNotificationNotificationId(payload);
    if (nId == null) return;

    if (_contentUpdatesQueue.any((n) => n.id == nId)) return;

    var n = await _notificationOf(nId);
    if (n == null) return;

    _contentUpdatesQueue.add(n);
    state = this;
  }

  Future<bool> _checkContentUpdates(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var n = await _notificationOf(parseNotificationNotificationId(payload));
    if (n == null) return false;

    var dismissedDate;
    if (
      (notification is ReceivedAction) && (notification.dismissedDate != null)
    ) {
      dismissedDate =
        DateTime.tryParse("${notification.dismissedDate}Z")?.toUtc();
    }
    if (dismissedDate == null) dismissedDate = DateTime.now();

    return await n.setChecked(dismissedDate) ?? false;
  }

  final _passphraseChanged = <int>{};

  bool isPassphraseChanged(Storage storage) => clearPassphraseChanged(storage);

  bool clearPassphraseChanged(Storage storage) =>
    _passphraseChanged.remove(storage.id);

  void _addPassphraseChanged(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) {
    var storageId = parseNotificationStorageId(payload);
    if (storageId == null) return;

    _passphraseChanged.add(storageId);
    state = this;
  }

  final _healthDataRetrievedQueue = Queue<HealthSetting>();

  HealthSetting? get nextHealthDataRetrieved =>
    _healthDataRetrievedQueue.removeFirstOrNull();

  Future<Storage?> _storageOf(Map<String, String?> payload) async {
    var storageId = parseNotificationStorageId(payload);
    if (storageId == null) return null;

    var storageInfo = await storageInfoOf(storageId);
    if (storageInfo == null) return null;

    var storage; try {
      storage = await storageInfo.connect();
    } catch (e, s) {
      print(e); print(s);
      return null;
    }
    if (!storage.isCipherInitialized) return null;

    return storage;
  }

  Future<HealthSetting?> _healthSettingOf(
    Map<String, String?> payload,
    String healthSettingId,
  ) async {
    var storage = await _storageOf(payload);
    if (storage == null) return null;

    try {
      return await storage.healthSettingOf(healthSettingId);
    } catch (e, s) {
      print(e); print(s);
      return null;
    }
  }

  Future<void> _addHealthDataRetrieved(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var healthSettingId = parseNotificationHealthSettingId(payload);
    if (healthSettingId == null) return;

    if (_healthDataRetrievedQueue.any((n) => n.id == healthSettingId)) {
      return;
    }

    var healthSetting = await _healthSettingOf(payload, healthSettingId);
    if (healthSetting == null) return;

    _healthDataRetrievedQueue.add(healthSetting);
    state = this;
  }

  final _friendQueue = Queue<Friend>();

  Friend? get nextFriend => _friendQueue.removeFirstOrNull();

  Future<Friend?> _friendOf(Map<String, String?> payload) async {
    var friendPlrId = parseNotificationFriendPlrId(payload);
    if (friendPlrId == null) return null;

    var storage = await _storageOf(payload);
    if (storage == null) return null;

    Friend? friend;
    await for (
      var r in (await storage.root).friendOf(friendPlrId).handleError((_) {})
    ) {
      friend = r.value;
    }
    return friend;
  }

  Future<void> _addFriend(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var friend = await _friendOf(payload);
    if (friend == null) return;

    _friendQueue.add(friend);
    state = this;
  }

  final _channelIndexQueue = Queue<ChannelIndexNotification>();

  ChannelIndexNotification? get nextChannelIndex =>
    _channelIndexQueue.removeFirstOrNull();

  Future<ChannelIndexNotification?> _channelIndexNotificationOf(
    Map<String, String?> payload,
  ) async {
    var nId = parseNotificationNotificationId(payload);
    if (nId == null) return null;

    var itemId = parseNotificationItemId(payload);
    if (itemId == null) return null;

    if (
      _channelIndexQueue.any(
        (n) => (n.notification.id == nId) && (n.itemId == itemId),
      )
    ) return null;

    var n = await _notificationOf<ChannelNotification>(nId);
    if (n == null) return null;

    return ChannelIndexNotification._(n, itemId);
  }

  Future<void> _addChannelIndex(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var n = await _channelIndexNotificationOf(payload);
    if (n == null) return;

    _channelIndexQueue.add(n);
    state = this;
  }

  Future<Channel?> indexEventChannelOf(
    ChannelIndexNotification? notification,
  ) async {
    if (notification == null) return null;

    Channel? channel = (await notification.notification.model)?.model;
    if (channel == null) return null;

    await channel.syncSilently();

    return channel..indexEvent = channel.indexEventOf(notification.itemId);
  }

  Future<void> _removeChannelIndex(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var channel = await indexEventChannelOf(
      await _channelIndexNotificationOf(payload),
    );
    if (channel == null) return;

    if (channel.indexEvent?.sweepNotificationTimes() == true) {
      await channel.syncSilently();
    }
  }

  Future<void> _addChannelRequest(
    ReceivedNotification notification,
    Map<String, String?> payload,
    Queue<ChannelRequest> queue,
  ) async {
    var storage = await _storageOf(payload);
    if (storage == null) return;

    var channelId = parseNotificationChannelId(payload);
    var itemTags = parseNotificationItemTags(payload);

    if (
      queue.any(
        (n) => (n.storage == storage) && (n.channelId == channelId),
      )
    ) return;

    queue.add(ChannelRequest._(storage, channelId, itemTags));
    state = this;
  }

  final _channelDisclosureConsentRequestQueue = Queue<ChannelRequest>();

  ChannelRequest? get nextChannelDisclosureConsentRequest =>
    _channelDisclosureConsentRequestQueue.removeFirstOrNull();

  Future<void> _addChannelDisclosureConsentRequest(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) => _addChannelRequest(
    notification, payload, _channelDisclosureConsentRequestQueue,
  );

  Future<void> _removeChannelDisclosureConsentRequest(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var storage = await _storageOf(payload);
    var channelId = parseNotificationChannelId(payload);
    if ((storage == null) || (channelId == null)) return;

    await removeChannelDisclosureConsentRequest(storage, channelId);
  }

  final _fidoRegistrationTokenResultQueue = Queue<ChannelRequest>();

  ChannelRequest? get nextFidoRegistrationTokenResult =>
    _fidoRegistrationTokenResultQueue.removeFirstOrNull();

  Future<void> _addFidoRegistrationTokenResult(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) => _addChannelRequest(
    notification, payload, _fidoRegistrationTokenResultQueue,
  );

  Future<void> _removeFidoRegistrationTokenResult(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var storage = await _storageOf(payload);
    var channelId = parseNotificationChannelId(payload);
    if ((storage == null) || (channelId == null)) return;

    await removeFidoRegistrationTokenResult(storage, channelId);
  }

  final _didIssuanceResultQueue = Queue<ChannelRequest>();

  ChannelRequest? get nextDidIssuanceResult =>
    _didIssuanceResultQueue.removeFirstOrNull();

  Future<void> _addDidIssuanceResult(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) => _addChannelRequest(notification, payload, _didIssuanceResultQueue);

  Future<void> _removeDidIssuanceResult(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var storage = await _storageOf(payload);
    var channelId = parseNotificationChannelId(payload);
    if ((storage == null) || (channelId == null)) return;

    await removeDidIssuanceResult(storage, channelId);
  }

  final _vcIssuanceResultQueue = Queue<ChannelRequest>();

  ChannelRequest? get nextVcIssuanceResult =>
    _vcIssuanceResultQueue.removeFirstOrNull();

  Future<void> _addVcIssuanceResult(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) => _addChannelRequest(notification, payload, _vcIssuanceResultQueue);

  Future<void> _removeVcIssuanceResult(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    var storage = await _storageOf(payload);
    var channelId = parseNotificationChannelId(payload);
    if ((storage == null) || (channelId == null)) return;

    await removeVcIssuanceResult(storage, channelId);
  }

  Future<void> _mayDecrementBadgeCounter(
    ReceivedNotification notification,
    Map<String, String?> payload,
  ) async {
    if (
      (notification.channelKey != null) &&
      (notificationChannels[notification.channelKey!]?.channelShowBadge == true)
    ) decrementBadgeCounter();
  }
}
