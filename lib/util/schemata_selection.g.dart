// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schemata_selection.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$schemataSelectionHash() => r'094fb8d2525ac051e1d4b8b83ccc83cb6975b9e3';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$SchemataSelection
    extends BuildlessAutoDisposeAsyncNotifier<SchemataSelectionState?> {
  late final Timeline timeline;
  late final StyleSheetType styleSheetType;
  late final String? keyPrefix;
  late final bool syncRoot;
  late final bool syncNotificationRegistry;

  FutureOr<SchemataSelectionState?> build(
    Timeline timeline, {
    required StyleSheetType styleSheetType,
    String? keyPrefix,
    bool syncRoot = true,
    bool syncNotificationRegistry = false,
  });
}

/// See also [SchemataSelection].
@ProviderFor(SchemataSelection)
const schemataSelectionProvider = SchemataSelectionFamily();

/// See also [SchemataSelection].
class SchemataSelectionFamily
    extends Family<AsyncValue<SchemataSelectionState?>> {
  /// See also [SchemataSelection].
  const SchemataSelectionFamily();

  /// See also [SchemataSelection].
  SchemataSelectionProvider call(
    Timeline timeline, {
    required StyleSheetType styleSheetType,
    String? keyPrefix,
    bool syncRoot = true,
    bool syncNotificationRegistry = false,
  }) {
    return SchemataSelectionProvider(
      timeline,
      styleSheetType: styleSheetType,
      keyPrefix: keyPrefix,
      syncRoot: syncRoot,
      syncNotificationRegistry: syncNotificationRegistry,
    );
  }

  @override
  SchemataSelectionProvider getProviderOverride(
    covariant SchemataSelectionProvider provider,
  ) {
    return call(
      provider.timeline,
      styleSheetType: provider.styleSheetType,
      keyPrefix: provider.keyPrefix,
      syncRoot: provider.syncRoot,
      syncNotificationRegistry: provider.syncNotificationRegistry,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'schemataSelectionProvider';
}

/// See also [SchemataSelection].
class SchemataSelectionProvider extends AutoDisposeAsyncNotifierProviderImpl<
    SchemataSelection, SchemataSelectionState?> {
  /// See also [SchemataSelection].
  SchemataSelectionProvider(
    Timeline timeline, {
    required StyleSheetType styleSheetType,
    String? keyPrefix,
    bool syncRoot = true,
    bool syncNotificationRegistry = false,
  }) : this._internal(
          () => SchemataSelection()
            ..timeline = timeline
            ..styleSheetType = styleSheetType
            ..keyPrefix = keyPrefix
            ..syncRoot = syncRoot
            ..syncNotificationRegistry = syncNotificationRegistry,
          from: schemataSelectionProvider,
          name: r'schemataSelectionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$schemataSelectionHash,
          dependencies: SchemataSelectionFamily._dependencies,
          allTransitiveDependencies:
              SchemataSelectionFamily._allTransitiveDependencies,
          timeline: timeline,
          styleSheetType: styleSheetType,
          keyPrefix: keyPrefix,
          syncRoot: syncRoot,
          syncNotificationRegistry: syncNotificationRegistry,
        );

  SchemataSelectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.timeline,
    required this.styleSheetType,
    required this.keyPrefix,
    required this.syncRoot,
    required this.syncNotificationRegistry,
  }) : super.internal();

  final Timeline timeline;
  final StyleSheetType styleSheetType;
  final String? keyPrefix;
  final bool syncRoot;
  final bool syncNotificationRegistry;

  @override
  FutureOr<SchemataSelectionState?> runNotifierBuild(
    covariant SchemataSelection notifier,
  ) {
    return notifier.build(
      timeline,
      styleSheetType: styleSheetType,
      keyPrefix: keyPrefix,
      syncRoot: syncRoot,
      syncNotificationRegistry: syncNotificationRegistry,
    );
  }

  @override
  Override overrideWith(SchemataSelection Function() create) {
    return ProviderOverride(
      origin: this,
      override: SchemataSelectionProvider._internal(
        () => create()
          ..timeline = timeline
          ..styleSheetType = styleSheetType
          ..keyPrefix = keyPrefix
          ..syncRoot = syncRoot
          ..syncNotificationRegistry = syncNotificationRegistry,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        timeline: timeline,
        styleSheetType: styleSheetType,
        keyPrefix: keyPrefix,
        syncRoot: syncRoot,
        syncNotificationRegistry: syncNotificationRegistry,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<SchemataSelection,
      SchemataSelectionState?> createElement() {
    return _SchemataSelectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SchemataSelectionProvider &&
        other.timeline == timeline &&
        other.styleSheetType == styleSheetType &&
        other.keyPrefix == keyPrefix &&
        other.syncRoot == syncRoot &&
        other.syncNotificationRegistry == syncNotificationRegistry;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, timeline.hashCode);
    hash = _SystemHash.combine(hash, styleSheetType.hashCode);
    hash = _SystemHash.combine(hash, keyPrefix.hashCode);
    hash = _SystemHash.combine(hash, syncRoot.hashCode);
    hash = _SystemHash.combine(hash, syncNotificationRegistry.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SchemataSelectionRef
    on AutoDisposeAsyncNotifierProviderRef<SchemataSelectionState?> {
  /// The parameter `timeline` of this provider.
  Timeline get timeline;

  /// The parameter `styleSheetType` of this provider.
  StyleSheetType get styleSheetType;

  /// The parameter `keyPrefix` of this provider.
  String? get keyPrefix;

  /// The parameter `syncRoot` of this provider.
  bool get syncRoot;

  /// The parameter `syncNotificationRegistry` of this provider.
  bool get syncNotificationRegistry;
}

class _SchemataSelectionProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<SchemataSelection,
        SchemataSelectionState?> with SchemataSelectionRef {
  _SchemataSelectionProviderElement(super.provider);

  @override
  Timeline get timeline => (origin as SchemataSelectionProvider).timeline;
  @override
  StyleSheetType get styleSheetType =>
      (origin as SchemataSelectionProvider).styleSheetType;
  @override
  String? get keyPrefix => (origin as SchemataSelectionProvider).keyPrefix;
  @override
  bool get syncRoot => (origin as SchemataSelectionProvider).syncRoot;
  @override
  bool get syncNotificationRegistry =>
      (origin as SchemataSelectionProvider).syncNotificationRegistry;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
