
import 'package:flutter/material.dart';

class ItemColor {
  static const Color UNCOMPLETE = Color(0xFFF8BBD0);
  static const Color COMPLETED = Color(0xFFE0F2F1);
  static const Color NOTMINE = Color(0xFFFFFFCC);

  // クラスの色
  static Color classColor({bool isCreator = true}) {
    return Colors.white;
  }

  // 内容の色
  // 所有者かどうか、未完成かどうかに対応した色を返す
  static Color contentColor({
      bool isCreator = true,
      bool isUncompleted = false,
  }) {
    return isCreator
        ? isUncompleted
            ? UNCOMPLETE
            : COMPLETED
        : NOTMINE;
  }
}
