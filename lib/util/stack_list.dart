/*
* stacklistの取得を行います
* */
class StackList<E> {
  StackList() : _storage = <E>[]; //初期化子リスト
  final List<E> _storage;
  void push(E element) => _storage.add(element);
  E pop() => _storage.removeLast();
  @override
  String toString() {
    return '--- トップ ---\n'
        '${_storage.reversed.join('\n')}'
        '\n-----------';
  }

  E get peek => _storage.last;
  bool get isEmpty => _storage.isEmpty;
  bool get isNotEmpty => !isEmpty;
  List<E> get all => _storage;

  StackList.of(Iterable<E> elements) : _storage = List<E>.of(elements);
}