import "package:i18n_extension/i18n_extension.dart";
import "package:plr_ui/plr_ui.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  confirmationTranslation +
  {
    "en_us": "Unset the app definition and restart the standard app.\n Are you sure?.",
    "ja_jp": "アプリ定義の設定を解除し、標準アプリで再起動します。\nよろしいですか?",
  } +
  {
    "en_us": "This app will add and publish the following roles to your profile:\n  %s\nAre you sure?.",
    "ja_jp": "このアプリは以下の役割をプロフィールに追加し、公開します:\n  %s\nよろしいですか?",
  } +
  {
    "en_us": "Change the app definition and restart this app.\nAre you sure?.",
    "ja_jp": "アプリ定義を変更してアプリを再起動します。\nよろしいですか?",
  } +
  {
    "en_us": "The role `%s\' has been added.",
    "ja_jp": "役割「%s」が追加されました。",
  } +
  {
    "en_us": "The role `%s\' is already registered. Use existing item.",
    "ja_jp": "役割「%s」は登録済です。既存の項目を利用します。",
  } +
  {
    "en_us": "The role `%s\' has been published.",
    "ja_jp": "役割「%s」を公開しました。",
  } +
  {
    "en_us": "Failed to get the life record.",
    "ja_jp": "生活録を取得できませんでした。",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
