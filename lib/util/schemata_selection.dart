import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/extension.dart';

part 'schemata_selection.freezed.dart';
part 'schemata_selection.g.dart';

const _schemaSourceIndex = 0;
const _styleSheetIndex = 1;

const _keyPrefixAddtion = "_styleSheet.";

@Freezed(copyWith: false)
class SchemataSelectionState
  with _$SchemataSelectionState, TimelineSchemataStateMixin
  implements TimelineSchemataState {

  const factory SchemataSelectionState({
    @Default(const {}) Map<SchemataSource, Schemata?> map,
    String? selectedSchemataSourceId,
    SchemataSource? selectedSchemataSource,
    Schemata? selectedSchemata,
    String? selectedStyleSheetId,
    StyleSheet? selectedStyleSheet,
  }) = _SchemataSelectionState;

  const SchemataSelectionState._();
}

enum StyleSheetType {
  timeline, channelSummary, groupSummary;
  String get keyPrefix => "${name}${_keyPrefixAddtion}";
}

@riverpod
class SchemataSelection extends _$SchemataSelection
  implements PlrAsyncNotifier<SchemataSelectionState?> {

  late SharedPreferences _preferences;
  late String _key;

  late TimelineSchemataProvider _timelineSchemataProvider;
  late TimelineSchemata _timelineSchemataController;
  TimelineSchemataState? _timelineSchemataValue;

  String? _selectedSchemataSourceId, _selectedStyleSheetId;

  FutureOr<SchemataSelectionState?> build(
    Timeline timeline, {
      required StyleSheetType styleSheetType,
      String? keyPrefix,
      bool syncRoot = true,
      bool syncNotificationRegistry = false,
  }) async {
    _preferences = await SharedPreferences.getInstance();
    _key = "${keyPrefix ?? styleSheetType.keyPrefix}${timeline.id}";

    _timelineSchemataProvider = timelineSchemataProvider(
      timeline,
      syncRoot: syncRoot, syncNotificationRegistry: syncNotificationRegistry,
    );
    _timelineSchemataController = ref.read(_timelineSchemataProvider.notifier);

    ref.listen(_timelineSchemataProvider, (_, next) async {
        _timelineSchemataValue = next.value;
        state = await AsyncValue.guard(() => _createValue());
    });
    _timelineSchemataValue = ref.read(_timelineSchemataProvider).value;

    return await _createValue();
  }
  bool get isLoading =>
    state.isLoading || _timelineSchemataController.isLoading;

  Future<void> refresh({ bool force = false }) async {
    _timelineSchemataController.refresh(force: force);

    state = const AsyncValue<SchemataSelectionState?>.loading(
    ).copyWithPrevious(state);

    state = await AsyncValue.guard(() => _createValue());
  }

  Future<bool> setSelectedSchemataSourceId(
    String? selectedSchemataSourceId,
  ) async {
    if (_selectedSchemataSourceId == selectedSchemataSourceId) return false;
    _selectedSchemataSourceId = selectedSchemataSourceId;

    return await _saveAndNotify();
  }

  Future<bool> setSelectedStyleSheetId(String? selectedStyleSheetId) async {
    if (_selectedStyleSheetId == selectedStyleSheetId) return false;
    _selectedStyleSheetId = selectedStyleSheetId;

    return await _saveAndNotify();
  }

  Future<bool> _saveAndNotify() async {
    await _save();

    state = await AsyncValue.guard(() => _createValue(false));
    return true;
  }

  Future<SchemataSelectionState?> _createValue([ bool load = true ]) async {
    if (load) await _load();

    final map = _timelineSchemataValue?.map;
    if (map == null) return null;

    var needSave = false;

    SchemataSource? selectedSchemataSource;
    Schemata? selectedSchemata;
    if (_selectedSchemataSourceId?.isNotEmpty == true) {
      final e = map.entries.firstWhereOrNull(
        (e) => e.key.id == _selectedSchemataSourceId,
      );
      if (e != null) {
        selectedSchemataSource = e.key;
        selectedSchemata = e.value;
      } else {
        _selectedSchemataSourceId = null;
        needSave = true;
      }
    } else {
      final e = map.entries.firstWhereOrNull((e) => e.value != null);
      if (e != null) {
        _selectedSchemataSourceId = e.key.id;
        selectedSchemataSource = e.key;
        selectedSchemata = e.value;
        needSave = true;
      }
    }

    final styleSheets = switch (styleSheetType) {
      StyleSheetType.timeline => selectedSchemata?.timelineStyleSheets,
      StyleSheetType.channelSummary =>
        selectedSchemata?.channelSummaryStyleSheets,
      StyleSheetType.groupSummary => selectedSchemata?.groupSummaryStyleSheets,
    } as Iterable<StyleSheet>?;

    StyleSheet? selectedStyleSheet;
    if (_selectedStyleSheetId?.isNotEmpty == true) {
      selectedStyleSheet = styleSheets?.firstWhereOrNull(
        (ss) => ss.id == _selectedStyleSheetId,
      );
      if (selectedStyleSheet == null) {
        _selectedStyleSheetId = null;
        needSave = true;
      }
    } else {
      selectedStyleSheet = styleSheets?.firstOrNull;
      if (selectedStyleSheet != null) {
        _selectedStyleSheetId = selectedStyleSheet.id;
        needSave = true;
      }
    }
    if (needSave) await _save();

    return SchemataSelectionState(
      map: map,
      selectedSchemataSourceId: _selectedSchemataSourceId,
      selectedSchemataSource: selectedSchemataSource,
      selectedSchemata: selectedSchemata,
      selectedStyleSheetId: _selectedStyleSheetId,
      selectedStyleSheet: selectedStyleSheet,
    );
  }

  Future<void> _load() => synchronized(() async {
      _selectedSchemataSourceId = _selectedStyleSheetId = null;

      final pref = _preferences.getStringList(_key);
      if (pref == null) return;

      String? getId(int index) {
        final id = pref[index];
        if (id.isNotEmpty) return id;
        return null;
      }

      if (pref.length > _schemaSourceIndex) {
        _selectedSchemataSourceId = getId(_schemaSourceIndex);
      } else return;

      if (pref.length > _styleSheetIndex) {
        _selectedStyleSheetId = getId(_styleSheetIndex);
      }
  });

  Future<void> _save() async {
    await synchronized(
    () async => await _preferences.setStringList(
      _key, [
        if (_selectedSchemataSourceId != null) _selectedSchemataSourceId!,
        if (_selectedStyleSheetId != null) _selectedStyleSheetId!,
      ],
    ),
  );
}
}
