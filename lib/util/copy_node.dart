import 'package:personaryFlutter/semeditor/policy/custom_policy.dart';
import 'package:plr_ui/plr_ui.dart';

extension RecordEntityCopytoSemEdit on RecordEntity {
  resetClipboardTimeline() {
    CustomBehaviourPolicy.resetClipboardTimeline();
  }

  copyNode(RecordEntity recordEntity, String plrId) {
     CustomStatePolicy.copyNode(recordEntity, plrId);
  }

  bool get isFirstCopy{
    return CustomStatePolicy.multipleCopiedSelectedNodeId.isEmpty;
  }
}