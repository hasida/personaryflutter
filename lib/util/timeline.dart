import 'dart:async' show Timer;

import 'package:plr_ui/plr_ui.dart';

/// メタデータ更新インターバル
const int _listInterval = 5 * 1000;

extension TimelineListing on Timeline {
  static final __refCount = Expando<int>();

  static final __listTimer = Expando<Timer>();
  static final __lastListTime = Expando<DateTime>();

  static final __listRunning = Expando<bool>();

  int get _refCount => __refCount[this] ?? 0;

  Timer? get _listTimer => __listTimer[this];
  set _listTimer(Timer? listTimer) => __listTimer[this] = listTimer;

  DateTime? get _lastListTime => __lastListTime[this];
  set _lastListTime(DateTime? lastListTime) =>
    __lastListTime[this] = lastListTime;

  bool get _listRunning => __listRunning[this] ?? false;
  set _listRunning(bool listRunning) =>
    __listRunning[this] = (listRunning == true) ? true : null;

  void startListing() {
    if ((__refCount[this] = _refCount + 1) > 1) return;

    _lastListTime = DateTime.now();
    _listTimer = Timer.periodic(
      const Duration(milliseconds: _listInterval), _onListTimer,
    );
  }

  void stopListing() {
    var c = __refCount[this] = _refCount - 1;
    if (c > 0) return;

    _lastListTime = null;

    _listTimer?.cancel();
    _listTimer = null;
  }

  void _onListTimer(Timer listTimer) {
    if (_listRunning) return;
    _listRunning = true;

    Future.wait([
        Future(() async {
            if (storage!.isSearchNodeFilesRunning) return;
            await storage!.searchNodeFiles().drain();
        }),
        Future(() async {
            var requestTime = DateTime.now();
            await entry.netList(
              entryType: EntryType.File, pattern: nodeFileSuffix,
              modifiedAfter: _lastListTime?.subtract(
                const Duration(minutes: 10),
              ),
            ).drain();
            _lastListTime = requestTime;
        }),
    ]).whenComplete(() => _listRunning = false);
  }
}
