import 'package:collection/collection.dart' show IterableExtension;

/// [dest]の中身が[src]と一致するように要素を追加・削除します。
bool updateList<T, U>(
  Iterable<T> dest,
  Iterable<U> src, {
  T add(U value)?,
  bool remove(T value)?,
  bool update(T? a, U b)?,
  required bool equals(T a, U b),
}) {
  bool updated = false;
  final removeList = <T>[];
  for (final d in dest) {
    if (!src.any((e) => equals(d, e))) {
      removeList.add(d);
    }
  }
  if (remove != null) {
    for (final e in removeList) {
      updated |= remove(e);
    }
  }
  for (final d in src) {
    var e = dest.firstWhereOrNull((e) => equals(e, d));
    if (e == null && add != null) {
      e = add(d);
      updated = true;
    }
    if (update != null) {
      updated |= update(e, d);
    }
  }
  return updated;
}
