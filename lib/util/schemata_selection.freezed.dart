// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'schemata_selection.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SchemataSelectionState {
  Map<SchemataSource, Schemata?> get map => throw _privateConstructorUsedError;
  String? get selectedSchemataSourceId => throw _privateConstructorUsedError;
  SchemataSource? get selectedSchemataSource =>
      throw _privateConstructorUsedError;
  Schemata? get selectedSchemata => throw _privateConstructorUsedError;
  String? get selectedStyleSheetId => throw _privateConstructorUsedError;
  StyleSheet<SchemaObject>? get selectedStyleSheet =>
      throw _privateConstructorUsedError;
}

/// @nodoc

class _$SchemataSelectionStateImpl extends _SchemataSelectionState
    with DiagnosticableTreeMixin {
  const _$SchemataSelectionStateImpl(
      {final Map<SchemataSource, Schemata?> map = const {},
      this.selectedSchemataSourceId,
      this.selectedSchemataSource,
      this.selectedSchemata,
      this.selectedStyleSheetId,
      this.selectedStyleSheet})
      : _map = map,
        super._();

  final Map<SchemataSource, Schemata?> _map;
  @override
  @JsonKey()
  Map<SchemataSource, Schemata?> get map {
    if (_map is EqualUnmodifiableMapView) return _map;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_map);
  }

  @override
  final String? selectedSchemataSourceId;
  @override
  final SchemataSource? selectedSchemataSource;
  @override
  final Schemata? selectedSchemata;
  @override
  final String? selectedStyleSheetId;
  @override
  final StyleSheet<SchemaObject>? selectedStyleSheet;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SchemataSelectionState(map: $map, selectedSchemataSourceId: $selectedSchemataSourceId, selectedSchemataSource: $selectedSchemataSource, selectedSchemata: $selectedSchemata, selectedStyleSheetId: $selectedStyleSheetId, selectedStyleSheet: $selectedStyleSheet)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SchemataSelectionState'))
      ..add(DiagnosticsProperty('map', map))
      ..add(DiagnosticsProperty(
          'selectedSchemataSourceId', selectedSchemataSourceId))
      ..add(
          DiagnosticsProperty('selectedSchemataSource', selectedSchemataSource))
      ..add(DiagnosticsProperty('selectedSchemata', selectedSchemata))
      ..add(DiagnosticsProperty('selectedStyleSheetId', selectedStyleSheetId))
      ..add(DiagnosticsProperty('selectedStyleSheet', selectedStyleSheet));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SchemataSelectionStateImpl &&
            const DeepCollectionEquality().equals(other._map, _map) &&
            (identical(
                    other.selectedSchemataSourceId, selectedSchemataSourceId) ||
                other.selectedSchemataSourceId == selectedSchemataSourceId) &&
            (identical(other.selectedSchemataSource, selectedSchemataSource) ||
                other.selectedSchemataSource == selectedSchemataSource) &&
            (identical(other.selectedSchemata, selectedSchemata) ||
                other.selectedSchemata == selectedSchemata) &&
            (identical(other.selectedStyleSheetId, selectedStyleSheetId) ||
                other.selectedStyleSheetId == selectedStyleSheetId) &&
            (identical(other.selectedStyleSheet, selectedStyleSheet) ||
                other.selectedStyleSheet == selectedStyleSheet));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_map),
      selectedSchemataSourceId,
      selectedSchemataSource,
      selectedSchemata,
      selectedStyleSheetId,
      selectedStyleSheet);
}

abstract class _SchemataSelectionState extends SchemataSelectionState {
  const factory _SchemataSelectionState(
          {final Map<SchemataSource, Schemata?> map,
          final String? selectedSchemataSourceId,
          final SchemataSource? selectedSchemataSource,
          final Schemata? selectedSchemata,
          final String? selectedStyleSheetId,
          final StyleSheet<SchemaObject>? selectedStyleSheet}) =
      _$SchemataSelectionStateImpl;
  const _SchemataSelectionState._() : super._();

  @override
  Map<SchemataSource, Schemata?> get map;
  @override
  String? get selectedSchemataSourceId;
  @override
  SchemataSource? get selectedSchemataSource;
  @override
  Schemata? get selectedSchemata;
  @override
  String? get selectedStyleSheetId;
  @override
  StyleSheet<SchemaObject>? get selectedStyleSheet;
}
