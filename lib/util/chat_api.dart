import 'package:dart_openai/dart_openai.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../semeditor/logic/sem_logic.dart';
import 'package:http/http.dart' as http;
import '../env/env.dart';

const url = "https://analyticspf.yakushite.net/semgraph-chat-api/v1/promptTemplate";
const String modelName = "gpt-4-1106-preview";
const String functionCall = "auto";
const promptDuration = Duration(seconds: 40);
const openAIDuration = Duration(seconds: 60);

Future<List<String>> chatAPI(Map body) async {
  logout("suggest_node api in");
  logout("suggestNodeRequest=" + json.encode(body));

  //リクエストデータ
  Map currentGraph = body["currentGraph"];
  String targetNodeId = body["targetNodeId"];
  int nodeSize = body["nodeSize"];

  Map map = {};
  map.addEntries([
    MapEntry("promptTemplateApiKey", Env.SystemPromptApiKey),
    const MapEntry("name", "suggest_node"),
  ]);
  String mapString = json.encode(map);

  // プロンプトの取得
  http.Response res = await http.post(Uri.parse(url), headers: {"Content-Type": "application/json", 'accept': 'application/json'}, body: utf8.encode(mapString), encoding: Encoding.getByName("UTF-8")).timeout(promptDuration);

  Map<String, dynamic> systemPrompt;

  String systemPromptTemplate;
  if (res.statusCode == 200) {
    systemPrompt = json.decode(utf8.decode(res.body.runes.toList()));
    systemPromptTemplate = systemPrompt["systemPromptTemplate"];
  } else {
    return [];
  }

  Map<String, String>? responseFormat = {"type": "json_object",};

  List<Function> functions = [];

  ChatContext chatContext = ChatContext(
    functionCall,
    modelName,
    functions,
    responseFormat,
  );

  // チャット呼び出し
  Map context = {};
  context.addEntries([
    MapEntry("currentGraph", currentGraph),
    MapEntry("targetNodeId", targetNodeId),
    MapEntry("nodeSize", nodeSize),
    MapEntry("systemPrompt", systemPromptTemplate),
  ]);

  logout("context=" + json.encode(context));

  ChatResponse chatResponse = await chatContext.add(context);
  String chatResponseMessage = chatResponse.message;
  logout("chatResponseMessage=" + chatResponseMessage);
  logout("type(chatResponseMessage)=" + chatResponseMessage.runtimeType.toString());
  // 戻り値を取得する
  Map responseMessageDict = json.decode(chatResponseMessage);

  // 戻り値からノード内容のリストを取得する
  if (!responseMessageDict.containsKey("suggestNodeContentList")) {
    logout("retry chat call: no suggestNodeContentList");
    return [];
  }
  List suggestNodeContentList = responseMessageDict["suggestNodeContentList"];

  logout("suggest_node_content_list=" + suggestNodeContentList.toString());
  logout("type(suggest_node_content_list)=" + suggestNodeContentList.runtimeType.toString());

  // もしノード内容がdictの場合は、その中の"content"を取り出す
  List<String> copySuggestNodeContentList = [];
  for (var suggestNodeContent in suggestNodeContentList) {
    if (suggestNodeContent is Map) {
      if (suggestNodeContent.containsKey("content")) copySuggestNodeContentList.add(suggestNodeContent["content"]);
      if (suggestNodeContent.containsKey("nodeContent")) copySuggestNodeContentList.add(suggestNodeContent["nodeContent"]);
    }
    if (suggestNodeContent is String) {
      copySuggestNodeContentList.add(suggestNodeContent);
    }
  }

  // ノードの内容に含まれる改行や空白文字を削除する
  List<String> modifiedSuggestNodeContentList = [];
  for (String suggestNodeContent in copySuggestNodeContentList) {
    modifiedSuggestNodeContentList.add(normalize(suggestNodeContent));
  }

  // もし修正された場合は、ログを表示する
  if (modifiedSuggestNodeContentList != copySuggestNodeContentList) {
    logout("modified_suggest_node_content_list=" + modifiedSuggestNodeContentList.toString());
  }

  // ノード内容が空のものがある場合
  if (modifiedSuggestNodeContentList.contains("")) {
    logout("retry chat call: empty content in suggestNodeContentList");
    return [];
  }

  // アシスタントメッセージ
  String assistantMessage = "";
  if (responseMessageDict.containsKey("assistantMessage")) {
    assistantMessage = responseMessageDict["assistantMessage"];
  }
  logout("assistant_message=" + assistantMessage);
  String modifiedAssistantMessage = normalize(assistantMessage);

  // もし修正された場合は、ログを表示する
  if (modifiedAssistantMessage != assistantMessage) {
    logout("modified_assistant_message=" + modifiedAssistantMessage.toString());
  }
  return modifiedSuggestNodeContentList;
}

String normalize(String text) {
  String modifiedText = text.trim();
  // Unicodeエスケープシーケンス(\uXXXX)が含まれる場合は、変換する
  if (modifiedText.contains("\\u")) {
    modifiedText = utf8.decode(modifiedText.runes.toList());
  }
  return modifiedText;
}

// チャットコンテキスト
class ChatContext {
  String functionCall;
  String modelName;
  List<Function> functions;
  Map<String, String>? responseFormat;

  // チャットコンテキストの作成
  //
  //   Args:
  //       make_prompt: プロンプトを作成する関数
  //       function_call: 関数呼び出しの情報
  //       model_name: モデル名
  //       functions: 関数のリスト
  //       response_format: レスポンスの形式

  ChatContext(
      this.functionCall,
      this.modelName,
      this.functions,
      this.responseFormat,
      );

  Future<ChatResponse> add(Map context) async {
    // ユーザーの発言を追加し、チャットレスポンス(システムの発言や関数呼び出し情報)を返す
    //
    //  Args:
    //   context: 現在の状況
    //
    //  Returns:
    //      チャットレスポンス

    String assistantMessage = "";
    String? functionName = "";
    Map functionArgs = {};

    List<OpenAIChatCompletionChoiceMessageModel> promptList = [
      OpenAIChatCompletionChoiceMessageModel(
        content: [
          OpenAIChatCompletionChoiceMessageContentItemModel.text(makePrompt(context))
        ],
        role: OpenAIChatMessageRole.system,
      )
    ];

    // API呼び出し時刻を記録する
    var stopwatch = Stopwatch()..start();
    // OpenAIクライアントを作成する
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String OpenAiApiKey = prefs.getString("OPEN_AI_API_KEY") ?? "";
    OpenAI.apiKey = OpenAiApiKey;

    OpenAIChatCompletionModel response;
    OpenAI.requestsTimeOut = openAIDuration;
    // OpenAI APIを使用して返答を生成する
    if (functions.length > 0) {
      if (responseFormat != null) {
        response = await OpenAI.instance.chat.create(
          model: modelName,
          messages: promptList,
          // tools: functions,
          toolChoice: functionCall,
          responseFormat: responseFormat,
        );
      } else {
        response = await OpenAI.instance.chat.create(
          model: modelName,
          messages: promptList,
          // tools: functions,
          toolChoice: functionCall,
        );
      }
    } else {
      if (responseFormat != null) {
        response = await OpenAI.instance.chat.create(
          model: modelName,
          messages: promptList,
          responseFormat: responseFormat,
        );
      } else {
        response = await OpenAI.instance.chat.create(
          model: modelName,
          messages: promptList,
        );
      }
    }
    OpenAIChatCompletionChoiceMessageModel message = response.choices[0].message;

    if (message.toolCalls != null) {
      // 関数呼び出しの場合、関数を実行する

      // 関数名
      functionName = message.toolCalls!.first.function.name;
      // 引数
      dynamic functionArgsStr = message.toolCalls!.first.function.arguments;

      print("function_name: " + functionName!);
      print("function_args: " + functionArgsStr);
      print("function_args type: " + functionArgsStr.runtimeType.toString());

      functionArgs = json.decode(functionArgsStr);
      for (var functionArgName in functionArgs.keys) {
        print(functionArgName + ": " + functionArgs[functionArgName].toString());
      }
    } else {
      // 関数呼び出しでない場合、メッセージを取得する
      assistantMessage = message.content!.first.text!;
    }

    // API呼び出しにかかった時間(秒)をログに出力する
    stopwatch.stop();
    logout("ChatContext.add: api_call_time: " + stopwatch.elapsedMilliseconds.toString());

    return ChatResponse(assistantMessage, functionName, functionArgs);
  }
}

// チャットレスポンス
class ChatResponse {
  //チャットレスポンス
  String message;
  String? functionName = "";
  Map functionArgs = {};

  // チャットレスポンスの作成
  //
  // Args:
  //     message: メッセージ
  //     function_name: 関数名
  //     function_args: 関数の引数

  ChatResponse(
      this.message,
      this.functionName,
      this.functionArgs,
      );
}

// 現在の状況からプロンプトを作成する
String makePrompt(Map context) {
  Map currentGraph = context["currentGraph"];
  String targetNodeId = context["targetNodeId"];
  String nodeSize = context["nodeSize"].toString();
  // String systemPrompt = context["systemPrompt"];
  String systemPrompt =
  """
  Please create a list in JSON format with a specified number of content suggestions for nodes that are linked similarly to a specified node in the current graph document.
  Include an explanation for the rationale behind each suggestion.

  Conditions:
  - A graph document is a set of nodes and typed directed links.
  - Each node is a single sentence or phrase.
  - The meanings of the link types follow:
  * Equal: sourceNodes are equal. One may be a summary or detail of the other.
  * Part: sourceNode is the whole, and targetNode is a part or component of it.
  * Member: targetNode is a member or an element of sourceNode.
  * Example: targetNode is an example of sourceNode.
  * Addition: targetNode is also valid in relation to sourceNode.
  * Specific: targetNode is a concretization of sourceNode.
  * Content: sourceNode is thinking, speaking, or believing, or the one who thinks, speaks, or believes, or a document or data representing that thought, speech, or belief. targetNode is the content of that thought, speech, or belief. 
  * Contrast: sourceNodes are in contrast. It's possible or permissible for all sourceNodes to exist, occur, or hold. They cannot be connected with 'even though'.
  * Disjunction: Either of sourceNodes exists, occurs or holds.
  * Dissimilar: sourceNodes are dissimilar.
  * Causes: sourceNode causes targetNode. sourceNode is a cause and targetNode is a result of it.
  * Conclusion: sourceNode implies targetNode. targetNode is inferred from sourceNode.
  * Triggers: targetNode arises or becomes apparent due to sourceNode. sourceNode does not contain specific information about the cause of targetNode.
  * Purpose: sourceNode is a means or method, and targetNode is a purpose of it.
  * Conditional: If sourceNode, then targetNode. It's uncertain whether sourceNode and targetNode are true.
  * Foreground: sourceNode provides a background explanation for targetNode, or a context in which to understand targetNode, but not its cause or reason.
  * Conflict: sourceNodes are difficult or undesirable to consist with each other.
  * Unconditional: Regardless of sourceNode, targetNode exists, occurs, or holds. Use "Compromise" if sourceNode makes it hard for targetNode to exist, occur, or hold.
  * Compromise: Even if sourceNode, still targetNode.
  * Response: targetNode is an answer to sourceNode.
  * Approval: targetNode is an affirmative response to sourceNode.
  * Disapproval: targetNode is a negative response to sourceNode.
  * Solution: sourceNode is a problem, and targetNode is its proposed solution.
  * Before: targetNode temporally follows sourceNode.
  * Sametime: sourceNodes exist, occur, or hold simultaneously.
  * Situation: targetNode is the time, place, or situation where sourceNode exists, occurs, or holds.
  * Object: sourceNode is a state or action, and targetNode is its object.
  * Uncertain: The relationship of sourceNodes is unclear.
  - Below is the current graph document, the node ID of the specified node S, and the number N of new alternative nodes I want you to create.
  - An alternative node is a node A such that if S is connected with another node X via a link, then A is also connected with X via a link with the same type and direction.
  - Each new alternative node should be dissimilar to the other alternative nodes.
  - If S is not empty, the new alternative nodes should be similar to S.
  - The new alternative nodes and the explanation should be in the same language as S. If S is empty, then they should be in the same language as the nodes connected with S.
  - If the language may be both Japanese and Chinese, use Japanese.
  - Please list the new alternative nodes you create under 'suggestNodeContentList' key.
  - Please put the explanation of the new alternative nodes under 'assistantMessage' key.

  Current graph document:
  {current_graph}
  Node ID:
  "{node_id}"
  Number of alternative nodes to create:
  "{node_number}"
  """;
  systemPrompt = systemPrompt.replaceAll("{current_graph}", json.encode(currentGraph));
  systemPrompt = systemPrompt.replaceAll("{node_id}", targetNodeId);
  systemPrompt = systemPrompt.replaceAll("{node_number}", nodeSize);

  return systemPrompt;
}
