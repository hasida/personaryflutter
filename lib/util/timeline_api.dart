import 'dart:collection';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:plr_ui/plr_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String keyProperty = "key";
const String messageProperty = "message";
const String commandProperty = "command";
const String modeProperty = "mode";
const String tokenProperty = "token";
const String userIdProperty = "userId";
const String summaryProperty = "summary";

Future<Map<String, dynamic>?> timelineAPI(BuildContext context, Account account, Timeline timeline, Schemata? timelineSchemata, String text, AIConditionData aiCondition, String prefsTokenKey, String userPlrId) async{
  //送信するMap
  Map<String, dynamic> bodyMap = {};
  bodyMap[keyProperty] = aiCondition.apiKey ?? "";
  bodyMap[messageProperty] = text;
  if (text.isEmpty) {
    bodyMap[commandProperty] = "special";
  }

  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString(prefsTokenKey);
  if (token != null) {
    bodyMap[modeProperty] = "request";
    bodyMap[tokenProperty] = token;
  } else {
    // InitialRequest
    bodyMap[modeProperty] = "initialRequest";
    bodyMap[userIdProperty] = userPlrId;

    Map<String, int> classMap = {};
    List<String> parameterList = aiCondition.parameter!.split("\n");
    for (String parameter in parameterList) {
      List<String> className =  parameter.split("*");
      if (className.length > 2) continue;
      else if (className.length == 1) {
        classMap[className.first] = 1;
      } else {
        if (int.tryParse(className.last) != null) {
          classMap[className.first] = int.parse(className.last);
        } else {
          continue;
        }
      }
    }
    Map<String, dynamic>? summaryMap = {};

    // プロフィール
    for (var key in classMap.keys) {
      if (key.startsWith("@")) {
        String className = key.substring(1);
        Schemata? schemata;
        await for (var s in profileSchemata) {
          schemata = s;
        }
        SchemaClass? schemaClass =  schemata?.classOf(className);
        if (schemaClass == null) continue;
        ProfileItem? nameItem = account.root.lifeRecord?.profileItemsOfClass(schemaClass).first;
        if (nameItem == null) continue;
        var recordEntity = RecordEntity.from(schemata!, nameItem);
        if (recordEntity == null) continue;
        var map = createJson(recordEntity);
        if (map.isEmpty) continue;
        addMap(summaryMap, recordEntity.type.label?.defaultValue ?? className, map);
      }
    }

    HashSet<Item> tmpItems = HashSet<Item>(equals: (a,b) => a.id == b.id, hashCode: (o) => o.id.hashCode);
    await for(var items in (timeline as Channel).timelineItemModelsByCount(DateTime.now(), 50000, direction: false)) {
      tmpItems.addAll(items);
    }

    List<Item> itemList = tmpItems.toList();
    itemList = itemList.where((Item item) => item.begin != null).toList();
    itemList.sort((a, b) => b.begin!.compareTo(a.begin!));

    //クラス
    for (Item item in itemList) {
      if (timelineSchemata == null) continue;
      RecordEntity? recordEntity = RecordEntity.from(timelineSchemata, item);
      if (recordEntity == null) continue;
      String? className = recordEntity.type.id;
      if (classMap.containsKey(className)) {
        if (classMap[className] != 0) {
          var map = createJson(recordEntity);
          if (map.isEmpty) continue;
          addMap(summaryMap, recordEntity.type.label?.defaultValue ?? className, map);
          classMap[className] = classMap[className]! - 1;
        }
      }
    }

    bodyMap[summaryProperty] = summaryMap;
  }

  String bodyMapString = json.encode(bodyMap);

  const promptDuration = Duration(seconds: 40);
  Map<String, dynamic> systemPrompt;
  try {
    http.Response res = await http.post(Uri.parse(aiCondition.url!), headers: {"Content-Type": "application/json", 'accept': 'application/json'}, body: utf8.encode(bodyMapString), encoding: Encoding.getByName("UTF-8")).timeout(promptDuration);
    if (res.statusCode == 200) {
      systemPrompt = json.decode(res.body);
      // systemPrompt = json.decode(utf8.decode(res.body.runes.toList()));
      return systemPrompt;
    } else {
      return null;
    }
  } catch(e) {
    return null;
  }
}

Map<String, dynamic> createJson(var recordEntity) {
  Map<String, dynamic> map = {};
  Entity entity = recordEntity.entity!;
  String key = recordEntity.type.label?.defaultValue ?? recordEntity.type.id;
  for (String propName in entity.propertyNames) {
    // 隠し属性の場合は表示しない
    if (propName.startsWith("_") && propName.length > 1 && propName[1] == propName[1].toLowerCase()) continue;
    for (var ent in entity.propertyModelsOf(propName)) {
      if (!recordEntity.props.containsKey(propName)) {
        addMap(map, propName, (ent as Literal).defaultValue);
        continue;
      }
      for (var record in recordEntity.props[propName]!.records) {
        if (record == null) {
          addMap(map, propName, ent);
          continue;
        }

        RecordData recordData  = record;
        String name = recordData.label;

        String? textFormat;
        String? startText;
        String? endText;
        try {
          textFormat = (recordData as RecordValue).property.format?.defaultValue;
        } catch(e) {
        }
        if (textFormat != null) {
          /// formatから[]前後の文字列取得
          var braceBegin = textFormat.indexOf('[');
          var braceEnd = textFormat.indexOf(']');
          if (0 <= braceBegin && braceBegin < braceEnd) {
            if (0 < braceBegin) {
              startText = textFormat.substring(0, braceBegin);
            }
            endText = textFormat.substring(braceEnd + 1);
          }
          else {
            print("[EntityDialog] Invalid format: '$textFormat'");
          }
        }

        switch (recordData.dataType) {
          case DataType.MMDATA:
            RecordMmdata recordMmdata = (recordData as RecordMmdata);
            Map mmMap = {};
            mmMap["title"] = recordMmdata.title;
            mmMap["comment"] = recordMmdata.comment;
            addMap(map, name, mmMap);

          case DataType.DATETIME:
          case DataType.DATE:
          case DataType.TIME:
            RecordLiteral recordLiteral = (recordData as RecordLiteral);
            addMap(map, name, recordLiteral.text);

          case DataType.BOOLEAN:
            RecordLiteral recordLiteral = (recordData as RecordLiteral);
            var value = recordLiteral.value ? recordLiteral.property.trueLabel?.defaultValue : recordLiteral.property.falseLabel?.defaultValue;
            addMap(map, name, value);

          case DataType.SELECTION:
            RecordLiteral recordLiteral = (recordData as RecordLiteral);

            // 隠し属性の場合は表示しない
            if (recordLiteral.range?.id[1] == recordLiteral.range?.id[1].toLowerCase()) break;
            addMap(map, name, recordLiteral.value?.label?.defaultValue);

          case DataType.STRING:
          case DataType.INTEGER:
          case DataType.POSITIVE_INTEGER:
          case DataType.NON_NEGATIVE_INTEGER:
          case DataType.DECIMAL:
          case DataType.PLR_CHANNEL:
          case DataType.PLR_SCHEMA:
          case DataType.DURATION:
          case DataType.UNKNOWN:
            RecordLiteral recordLiteral = (recordData as RecordLiteral);
            String text = "${startText ?? ''}${recordLiteral.value ?? ''}${endText ?? ''}";
            addMap(map, name, text);

          case DataType.ENTITY:
            name = record.type.label?.defaultValue ?? recordEntity.type.id;

            if (recordData.referer != null) {
              var refererName =  recordData.referer?.label?.defaultValue ?? recordData.referer?.id ?? "";
              addMap(map, refererName, {name : createJson(record)});
            } else {
              addMap(map, name, createJson(record));
            }

          default:
            break;
        }
      }
    }
  }
  if (entity.propertyNames.isEmpty) {
    addMap(map, key, null);
  }
  return map;
}

addMap(Map map, String name, dynamic value) {
  if (value is Map && value.isEmpty) {
    return;
  }

  if (map.containsKey(name)) {
    if (value == null) return;
    map[name].add(value);
  } else {
    if (value == null) {
      map[name] = [];
    } else {
      map[name] = [value];
    }
  }
}
