import 'dart:async' show Timer;
import 'dart:ui' show Rect;

import 'package:plr_ui/plr_ui.dart';
import 'package:window_size/window_size.dart';

const _windowLeftKey = "windowLeft";
const _windowTopKey = "windowTop";
const _windowWidthKey = "windowWidth";
const _windowHeightKey = "windowHeight";

Timer? _windowTaskTimer;

Future<void> mayStartWindowTask([
    Duration interval = const Duration(seconds: 5),
]) async {
  if (!isDesktop) return;

  stopWindowTask();

  await _mayRestoreWindowInfo();
  _windowTaskTimer = Timer.periodic(interval, (_) => _storeWindowInfo());
}

void stopWindowTask() {
  _windowTaskTimer?.cancel();
  _windowTaskTimer = null;
}

double? _lastWindowLeft, _lastWindowTop, _lastWindowWidth, _lastWindowHeight;

Future<void> _mayRestoreWindowInfo() async {
  var frameInfo = (
    await Future.wait<double?>([
        getSettingValue(_windowLeftKey), getSettingValue(_windowTopKey),
        getSettingValue(_windowWidthKey), getSettingValue(_windowHeightKey),
    ])
  ).nonNulls.toList();

  if (frameInfo.length < 4) return;

  ///Window(right座標)
  var windowRight = frameInfo[0] + frameInfo[2];
  ///Window(bottom座標)
  var windowBottom = frameInfo[1] + frameInfo[3];

  final screenList = (await getScreenList());

  ///補正ディスプレイ取得
  var currentScreen = (0 < screenList.length) ? screenList[0] : null;
  var defaultScreenScaleFactor = (null != currentScreen) ? currentScreen.scaleFactor : 1;
  for (var screenTemp in screenList) {
    if (screenTemp.frame.left < frameInfo[0] && frameInfo[0] < screenTemp.frame.right &&
        screenTemp.frame.top < frameInfo[1] && frameInfo[1] < screenTemp.frame.bottom ||
        ((screenTemp.frame.left < windowRight && windowRight < screenTemp.frame.right &&
            screenTemp.frame.top < windowBottom && windowBottom < screenTemp.frame.bottom))) {
      currentScreen = screenTemp;
      break;
    }
  }

  ///Window補正処理
  if (null != currentScreen) {
    var currentScreenScaleFactor = currentScreen.scaleFactor;
    ///Window(left座標)を補正
    frameInfo[0] = correctionWindowFrameLT(currentScreen.frame.left, currentScreen.frame.right, frameInfo[0], windowRight);
    ///Window(top座標)を補正
    frameInfo[1] = correctionWindowFrameLT(currentScreen.frame.top, currentScreen.frame.bottom, frameInfo[1], windowBottom);
    ///Window(width座標)を補正
    frameInfo[2] = correctionWindowFrameWH(currentScreen.frame.width, frameInfo[2], defaultScreenScaleFactor as double, currentScreenScaleFactor);
    ///Window(height座標)を補正
    frameInfo[3] = correctionWindowFrameWH(currentScreen.frame.height, frameInfo[3], defaultScreenScaleFactor, currentScreenScaleFactor);
  }

  setWindowFrame(Rect.fromLTWH(
      (_lastWindowLeft = frameInfo[0]), (_lastWindowTop = frameInfo[1]),
      (_lastWindowWidth = frameInfo[2]), (_lastWindowHeight = frameInfo[3]),
  ));
}
///Window(left or top座標) を補正
double correctionWindowFrameLT(double screenStart, double screenEnd, double windowStart, double windowEnd) {
  ///Window(left or top座標) が ディスプレイ(left or top座標)未満の場合,ディスプレイ(left or top座標)に補正
  windowStart = (windowStart < screenStart) ? screenStart : windowStart;
  ///Window(left or top座標)を補正
  if (screenEnd < windowEnd) {
    ///Window(right or bottom座標)がディスプレイ(right or bottom座標)を超える場合
    if ((windowStart - (windowEnd - screenEnd)) < screenStart) {
      ///補正後のWindow(left or top座標)がディスプレイ(left or top座標)以下の場合
      windowStart = screenStart;
    } else {
      windowStart = windowStart - (windowEnd - screenEnd);
    }
  }
  return windowStart;
}
///Window(width or height座標) を補正
double correctionWindowFrameWH(double screenFrame, double windowFrame, double defaultScreenScaleFactor, double currentScreenScaleFactor) {
  ///ディスプレイ(幅 or 高さ) が Window(幅 or 高さ)を超える場合,ディスプレイ幅に補正
  if (screenFrame < windowFrame) {
    windowFrame = screenFrame;
  }
  ///Window(right or bottom座標)に対して画面の拡縮比率を補正
  return windowFrame / currentScreenScaleFactor * defaultScreenScaleFactor;
}

Future<void> _storeWindowInfo() async {
  var frame = (await getWindowInfo()).frame;
  if (
    (frame.left == _lastWindowLeft) && (frame.top == _lastWindowTop) &&
    (frame.width == _lastWindowWidth) && (frame.height == _lastWindowHeight)
  ) return;

  await Future.wait([
      putSettingValue(_windowLeftKey, _lastWindowLeft = frame.left),
      putSettingValue(_windowTopKey, _lastWindowTop = frame.top),
      putSettingValue(_windowWidthKey, _lastWindowWidth = frame.width),
      putSettingValue(_windowHeightKey, _lastWindowHeight = frame.height),
  ]);
}

