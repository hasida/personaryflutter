import 'dart:ui';

Offset centerPosition(Offset position, Size size) {
  return Offset(position.dx + size.width/2, position.dy + size.height/2);
}
