import 'dart:convert' show json;

import 'package:dart_bbs/dart_bbs.dart';
import 'package:dart_bbs/src/bls_signature/retrieve_bls_key_value.dart';
import 'package:dart_bbs/src/models/vc.dart';
import 'package:plr_ui/plr_ui.dart';

const typeKey = "type";
const nameKey = "name";
const issuerKey = "issuer";
const issuanceDateKey = "issuanceDate";
const credentialSubjectKey = "credentialSubject";

const sexKey = "sex";
const dateOfBirthKey = "dateOfBirth";
const descriptionKey = "description";
const achievementKey = "achievement";
const creditsEarnedKey = "creditsEarned";
const resultKey = "result";
const achievedLevelKey = "achievedLevel";

const verifiableCredentialKey = "verifiableCredential";

const verifiableCredentialType = "VerifiableCredential";
const verifiablePresentationType = "VerifiablePresentation";

const profileType = "Profile";
const achievementSubjectType = "AchievementSubject";
const achievementType = "Achievement";

const profileKeys = [
  nameKey, sexKey, dateOfBirthKey,
];

const achievementSubjectKeys = [
  creditsEarnedKey,
];

const achievementKeys = [
  nameKey, descriptionKey,
];

const resultKeys = [
  achievedLevelKey,
];

const revealedIndexKey = "revealedIndex";

const vpContentType = "application/credential+ld+json";

enum CredentialType {
  verifiableCredential, verifiablePresentation,
}

bool hasCredentials(RecordEntity recordEntity) =>
  getCredentialFiles(recordEntity).isNotEmpty;

Iterable<FileMmdata> getCredentialFiles(RecordEntity recordEntity) =>
  recordEntity.outputtableProperties.map((p) {
      if (!p.property.ranges.any((r) => r.id == menuMmdata.id)) {
        return const <FileMmdata>[];
      }
      return recordEntity.propertyValuesOf(p.property).map((v) {
          var vv = v.valueFor();
          if (
            (vv is FileMmdata) &&
            (vv.format == 'application/credential+ld+json')
          ) return vv;
          return null;
      }).nonNulls;
  }).expand((values) => values);

CredentialType? credentialTypeOf(String credential) {
  List types; {
    var type = json.decode(credential)[typeKey];
    if (type == null) return null;

    if (type is! List) types = [ type ];
    else types = type;
  }

  if (types.contains(verifiableCredentialType)) {
    return CredentialType.verifiableCredential;
  }
  if (types.contains(verifiablePresentationType)) {
    return CredentialType.verifiablePresentation;
  }
  return null;
}

Future<bool> verifyCredential(String credential) async {
  switch (credentialTypeOf(credential)) {
    case CredentialType.verifiableCredential: {
      return await verifyVc(credential);
    }
    case CredentialType.verifiablePresentation: {
      return await verifyVp(credential);
    }
    default: return false;
  }
}

Future<bool> verifyVc(String credential) async {
  var vc = VerifiableCredential(credential);
  var issuerPublicKey = await retrieveBlsKeyValue(vc.getVerificationMethod());

  return await blsVerify(issuerPublicKey, vc.messages, vc.getSignature());
}

Future<bool> verifyVp(String credential) => vpVerify(credential);

extension VerifiableCredentialExt on VerifiableCredential {
  dynamic _getValue(String key) {
    var value = mapVC[key];
    while (value is List) {
      if (value.isEmpty) return null;
      value = value[0];
    }
    return value;
  }

  String? get issuer {
    var value = _getValue(issuerKey);
    if (value == null) return null;

    if (value is Map<String, dynamic>) return value[nameKey];
    else return value.toString();
  }

  String? get issuanceDate => _getValue(issuanceDateKey)?.toString();

  String? get name => _getValue(nameKey)?.toString();

  Iterable<Map<String, dynamic>> get credentialSubjects sync* {
    var values = mapVC[credentialSubjectKey];
    if (values is! List) values = [ values ];

    var revealedIndex = 0;
    for (var v in values) {
      revealedIndex++;
      if (v is! Map<String, dynamic>) continue;
      yield { ...v }..[revealedIndexKey] = revealedIndex;
    }
  }
}

extension JsonRevealedIndex on Map<String, dynamic> {
  int get revealedIndex => this[revealedIndexKey] ?? -1;
}
