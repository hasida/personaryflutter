import 'dart:io' show Platform;

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart' hide Notification;
import 'package:plr_ui/plr_ui.dart';
import 'package:synchronized/synchronized.dart';

import '../logic.dart';
import 'system_notification.i18n.dart';

final _notifications = AwesomeNotifications();

const _notificationIcon = "resource://drawable/notification_icon";

const contentUpdatesChannel = "contentUpdates";
const accountChannel = "account";
const friendChannel = "friend";
const healthDataCoordinationChannel = "healthDataCoordination";
const channelIndexNotificationChannel = "channelIndexNotification";
const requestChannel = "request";

final notificationChannels = {
  contentUpdatesChannel: NotificationChannel(
    channelKey: contentUpdatesChannel,
    channelName: "Content updates".i18n,
    channelDescription: "Notify updates of channel or friend content.".i18n,
    channelShowBadge: true,
    onlyAlertOnce: true,
  ),
  accountChannel: NotificationChannel(
    channelKey: accountChannel,
    channelName: "Account".i18n,
    channelDescription: "Notify about your account, such as passphrase has been changed on other devices.".i18n,
    onlyAlertOnce: true,
  ),
  friendChannel: NotificationChannel(
    channelKey: friendChannel,
    channelName: "Friend".i18n,
    channelDescription: "Notify about friend changes, such as automatically added.".i18n,
    onlyAlertOnce: true,
  ),
  healthDataCoordinationChannel: NotificationChannel(
    channelKey: healthDataCoordinationChannel,
    channelName: "Health data coordination".i18n,
    channelDescription: "Notify about health data coordination.".i18n,
    onlyAlertOnce: true,
  ),
  channelIndexNotificationChannel: NotificationChannel(
    channelKey: channelIndexNotificationChannel,
    channelName: "Notifications by channel retrieval key".i18n,
    channelDescription: "Notify the channel retrieval key at the set notificationLeadTime.".i18n,
    channelShowBadge: true,
    onlyAlertOnce: true,
  ),
  requestChannel: NotificationChannel(
    channelKey: requestChannel,
    channelName: "Requests and Results".i18n,
    channelDescription: "Notify the incoming requests and results.".i18n,
    //channelShowBadge: true,
    onlyAlertOnce: true,
  ),
};

bool _isNotificationsInitialized = false;

Future<void> initializeNotifications() async {
  if (_isNotificationsInitialized || isDesktop) return;

  await _notifications.initialize(
    _notificationIcon, notificationChannels.values.toList(),
  );
  await Future.wait(
    notificationChannels.values.map(_notifications.setChannel).toList(),
  );

  _isNotificationsInitialized = true;
}

Future<bool> requestNotificationsPermission(BuildContext context) async {
  if (isDesktop) return false;

  if (await _notifications.isNotificationAllowed()) return true;

  if (!await showConfirmDialog(
      context, "Request notifications.".i18n,
      "This app will send notifications.\n\nPlease tap 'Setting' button below and allow notifications.".i18n,
      okButtonLabel: "Setting".i18n,
      cancelButtonLabel: "Later".i18n,
  )) return false;

  await _notifications.requestPermissionToSendNotifications();

  return await _notifications.isNotificationAllowed();
}

//

Future<void> onFriendDisplayed(
  NotificationRegistry notificationRegistry,
  Friend friend,
  bool noErrors,
) async {
  var notification = notificationRegistry.friendNotificationOf(friend);
  if (notification == null) return;

  if (noErrors) await notification.setCheckedToNow(sync: false);

  await Future.wait([
      notification.setAccessedToNow(),
      dismissFriendRegisteredNotification(friend),
      dismissPassphraseDepositedNotification(friend),
      dismissChannelDisclosedNotification(friend),
      dismissItemDisclosedNotification(friend),
  ]);
}

Future<void> onChannelDisplayed(
  NotificationRegistry notificationRegistry,
  Channel channel,
  bool noErrors,
) async {
  var notification = notificationRegistry.channelNotificationOf(channel);
  if (notification == null) return;

  var storage = notificationRegistry.storage;

  if (noErrors) await Future.wait([
      notification.setChannelCheckedToNow(sync: false),
      removeFidoRegistrationTokenResult(storage, channel.id),
      removeDidIssuanceResult(storage, channel.id),
  ]);
  await notification.setChannelAccessedToNow();
}

Future<void> onTimelineDisplayed(
  NotificationRegistry notificationRegistry,
  Channel channel,
  bool noErrors,
) async {
  var notification = notificationRegistry.channelNotificationOf(channel);
  if (notification == null) return;

  var storage = notificationRegistry.storage;

  if (noErrors) await Future.wait([
      notification.setTimelineCheckedToNow(sync: false),
      dismissHealthSettingRetrievedNotificationByTimeline(channel),
      removeVcIssuanceResult(storage, channel.id),
  ]);
  await notification.setTimelineAccessedToNow();
}

//

const _notificationIdKey = "notificationId";

String? parseNotificationNotificationId(Map<String, String?> payload) =>
  payload[_notificationIdKey];

extension _NotificationContentUpdatesNotificationId on Notification {
  int get contentUpdatesNotificationId => id.hashCode;
}

Future<bool> mayNotifyContentUpdates(Notification notification) async {
  if (isDesktop) return false;

  //if (!notification.isEnabled || !notification.needNotify) {
  if (
    (await notification.isEnabled != true) || (
      (notification is FriendNotification) &&
      (await notification.needNotify != true)
    ) || (
      (notification is ChannelNotification) &&
      (await notification.needTimelineNotify != true)
    )
  ) {
    dismissContentUpdatesNotification(notification);
    return false;
  }

  late String title;
  if (notification is FriendNotification) {
    title = "Friend content has been updated.";
  } else if (notification is ChannelNotification) {
    //if (notification.needChannelNotify) {
      //if (notification.needTimelineNotify) {
        //title = "Channel and timeline has been updated.";
      //} else title = "Channel content has been updated.";
    //} else if (notification.needTimelineNotify)
    title = "Timeline has been updated.";
  }

  var body = (await notification.title)?.defaultValue?.toString();
  if ((body == null) || body.isEmpty) body = notification.modelId;

  return await _notifications.createNotification(
    content: NotificationContent(
      id: notification.contentUpdatesNotificationId,
      channelKey: contentUpdatesChannel,
      title: title.i18n,
      body: "- ${body}",
      payload: {
        _notificationIdKey: notification.id,
      },
    ),
  );
}

Future<void> dismissContentUpdatesNotification(
  Notification notification,
) async {
  if (isDesktop) return;

  await Future.wait([
      _notifications.dismiss(notification.contentUpdatesNotificationId),
      updateBadgeCounter(),
  ]);
}

Future<void> processContentNotifications(NotificationEvent event) async {
  Future Function(Notification notification) f; {
    switch (event.type) {
      case NotificationEventType.Added:
      case NotificationEventType.Updated: {
        f = mayNotifyContentUpdates;
        break;
      }
      case NotificationEventType.Removed: {
        f = dismissContentUpdatesNotification;
        break;
      }
    }
  }
  await Future.wait(event.notifications.map(f));
}

const _storageIdKey = "storageId";

int? parseNotificationStorageId(Map<String, String?> payload) {
  var storageIdStr = payload[_storageIdKey];
  if (storageIdStr == null) return null;

  try {
    return int.parse(storageIdStr);
  } catch (e, s) {
    print(e); print(s);
    return null;
  }
}

extension _StoragePassphraseChangedNotificationId on Storage {
  int get passphraseChangedNotificationId =>
    "${accountChannel}:${id}:passphraseChanged".hashCode;
}

Future<bool> notifyPassphraseChanged(Storage storage) async {
  if (isDesktop) return false;

  var sb = StringBuffer("- "); {
    var info = await storage.info;
    sb.write(info.email);
    if (info.userName?.isNotEmpty == true) {
      sb..write("(")
        ..write(info.userName)
        ..write(")");
    }
  }

  return await _notifications.createNotification(
    content: NotificationContent(
      id: storage.passphraseChangedNotificationId,
      channelKey: accountChannel,
      title: "Passphrase has been changed.".i18n,
      body: sb.toString(),
      payload: {
        _storageIdKey: "${storage.id}",
      },
    ),
  );
}

Future<void> dismissPassphraseChangedNotification(Storage storage) async {
  if (isDesktop) return;
  await _notifications.dismiss(storage.passphraseChangedNotificationId);
}

const _healthSettingIdKey = "healthSettingId";

const _timelineTypeKey = "_timelineTypeKey";
const _channelType = "Channel";
const _lifeRecordType = "LifeRecord";

int _healthDataRetrievedNotificationIdOf(Uri uri) =>
  "${healthDataCoordinationChannel}:${uri}:retrieved".hashCode;

extension _HealthSettingHealthDataRetrievedNotificationId on HealthSetting {
  int get healthDataRetrievedNotificationId =>
    _healthDataRetrievedNotificationIdOf(timeline.uri);
}

extension _TimelineHealthDataRetrievedNotificationId on Timeline {
  int get healthDataRetrievedNotificationId =>
    _healthDataRetrievedNotificationIdOf(uri);
}

Future<bool> notifyHealthSettingResult(HealthItemsResult result) async {
  if (isDesktop) return false;

  var r = false;

  for (var e in result.results.entries) {
    var result = e.value;
    var setting = e.key;

    print(
      "Health Setting ${setting.id} for ${setting.accountName}: "
      "${result.items.length} item(s) stored.",
    );
    if (result.items.isEmpty) continue;

    var name, timelineType; {
      if (await setting.isChannelTimeline) {
        name =
          (await setting.timelineAsChannel)?.name?.defaultValue?.toString() ??
        setting.timeline.id;
        timelineType = _channelType;
      } else {
        name = "Life record".i18n;
        timelineType = _lifeRecordType;
      }
    }

    r |= await _notifications.createNotification(
      content: NotificationContent(
        id: setting.healthDataRetrievedNotificationId,
        channelKey: healthDataCoordinationChannel,
        title: "Health data has been retrieved.".i18n,
        body: "- ${name}",
        payload: {
          _storageIdKey: "${setting.storage.id}",
          _healthSettingIdKey: "${setting.id}",
          _timelineTypeKey: timelineType,
        },
      ),
    );
  }
  return r;
}

Future<void> dismissHealthSettingRetrievedNotification(
  HealthSetting setting,
) async {
  if (isDesktop) return;
  await _notifications.dismiss(setting.healthDataRetrievedNotificationId);
}

Future<void> dismissHealthSettingRetrievedNotificationByTimeline(
  Timeline timeline,
) async {
  if (isDesktop) return;
  await _notifications.dismiss(timeline.healthDataRetrievedNotificationId);
}

String? parseNotificationHealthSettingId(Map<String, String?> payload) =>
  payload[_healthSettingIdKey];

const _friendPlrIdKey = "friendPlrId";

PlrId? parseNotificationFriendPlrId(Map<String, String?> payload) {
  try {
    return PlrId.fromString(payload[_friendPlrIdKey]);
  } catch (e, s) {
    print(e); print(s);
    return null;
  }
}

extension _FriendNotificationId on HasPlrId {
  String get _id => plrId.toString();

  int get registeredNotificationId =>
    "${friendChannel}:${_id}:registered".hashCode;
  int get passphraseDepositedNotificationId =>
    "${friendChannel}:${_id}:passphraseDeposited".hashCode;
  int get infoChannelSubscribedNotificationId =>
    "${friendChannel}:${_id}:infoChannelSubscribed".hashCode;
  int get channelDisclosedNotificationId =>
    "${friendChannel}:${_id}:channelDisclosed".hashCode;
  int get itemDisclosedNotificationId =>
    "${friendChannel}:${_id}:itemDisclosed".hashCode;
}

Future<bool> notifyFriendRegistered(Friend friend) async {
  if (isDesktop) return false;

  return await _notifications.createNotification(
    content: NotificationContent(
      id: friend.registeredNotificationId,
      channelKey: friendChannel,
      title: "Friend has been registered.".i18n,
      body: "- ${friend.plrId.email}",
      payload: {
        _storageIdKey: "${friend.storage.id}",
        _friendPlrIdKey: "${friend.id}",
      },
    ),
  );
}

Future<void> dismissFriendRegisteredNotification(Friend friend) async {
  if (isDesktop) return;
  await _notifications.dismiss(friend.registeredNotificationId);
}

Future<bool> notifyPassphraseDeposited(Friend friend) async {
  if (isDesktop) return false;

  return await _notifications.createNotification(
    content: NotificationContent(
      id: friend.passphraseDepositedNotificationId,
      channelKey: friendChannel,
      title: "Passphrase has been deposited to the friend.".i18n,
      body: "- ${friend.plrId.email}",
      payload: {
        _storageIdKey: "${friend.storage.id}",
        _friendPlrIdKey: "${friend.id}",
      },
    ),
  );
}

Future<void> dismissPassphraseDepositedNotification(Friend friend) async {
  if (isDesktop) return;
  await _notifications.dismiss(friend.passphraseDepositedNotificationId);
}

Future<bool> notifyInfoChannelSubscribed(SubscribedUser user) async {
  if (isDesktop) return false;

  return await _notifications.createNotification(
    content: NotificationContent(
      id: user.infoChannelSubscribedNotificationId,
      channelKey: friendChannel,
      title: "The info channel of the friend has been subscribed.".i18n,
      body: "- ${user.plrId!.email}",
      payload: {
        _storageIdKey: "${user.storage!.id}",
        _friendPlrIdKey: "${user.id}",
      },
    ),
  );
}

Future<void> dismissInfoChannelSubscribedNotification(Friend friend) async {
  if (isDesktop) return;
  await _notifications.dismiss(friend.infoChannelSubscribedNotificationId);
}

Future<bool> notifyChannelDisclosed(
  Friend friend,
  Iterable<Literal> names,
) async {
  if (isDesktop) return false;

  return await _notifications.createNotification(
    content: NotificationContent(
      id: friend.channelDisclosedNotificationId,
      channelKey: friendChannel,
      title: "Channels has been disclosed to the friend.".i18n,
      body: "- ${friend.plrId.email}: ${names.map(
        (n) => n.defaultValue?.toString(),
      ).nonNulls.join(", ")}",
      payload: {
        _storageIdKey: "${friend.storage.id}",
        _friendPlrIdKey: "${friend.id}",
      },
    ),
  );
}

Future<void> dismissChannelDisclosedNotification(Friend friend) async {
  if (isDesktop) return;
  await _notifications.dismiss(friend.channelDisclosedNotificationId);
}

Future<bool> notifyItemDisclosed(
  Friend friend,
  Iterable<Literal> names,
) async {
  if (isDesktop) return false;

  return await _notifications.createNotification(
    content: NotificationContent(
      id: friend.itemDisclosedNotificationId,
      channelKey: friendChannel,
      title: "Nodes has been disclosed to the friend.".i18n,
      body: "- ${friend.plrId.email}: ${names.map(
        (n) => n.defaultValue.toString(),
      ).join(", ")}",
      payload: {
        _storageIdKey: "${friend.storage.id}",
        _friendPlrIdKey: "${friend.id}",
      },
    ),
  );
}

Future<void> dismissItemDisclosedNotification(Friend friend) async {
  if (isDesktop) return;
  await _notifications.dismiss(friend.itemDisclosedNotificationId);
}

const _itemIdKey = "itemId";

String? parseNotificationItemId(Map<String, String?> payload) =>
  payload[_itemIdKey];

extension _ChannelIndexNotificationNotificationId on ChannelIndex {
  int get channelIndexNotificationNotificationId =>
    "${channelIndexNotificationChannel}:${storageId}:${id}:${itemId}".hashCode;
}

Future<void> processChannelIndexNotifications(
  PlrChannelIndexEvent event,
) async {
  if (isDesktop) return;

  for (var i in event.removed) {
    dismissChannelIndexNotification(i);
  }
  for (var i in [ ...event.added, ...event.updated ]) {
    var n = await notificationRegistryManager.notificationOf(i.notificationId);
    if (n == null) continue;

    var body = (await n.title)?.defaultValue?.toString();
    if ((body == null) || body.isEmpty) body = i.id;

    await _notifications.createNotification(
      content: NotificationContent(
        id: i.channelIndexNotificationNotificationId,
        channelKey: channelIndexNotificationChannel,
        title: "Notification: ".i18n + i.indexText,
        body: "- ${body}",
        payload: {
          _notificationIdKey: "${n.id}",
          _itemIdKey: "${i.itemId}",
        },
      ),
    );
  }
}

Future<void> dismissChannelIndexNotification(ChannelIndex index) async {
  if (isDesktop) return;

  await Future.wait([
      _notifications.dismiss(index.channelIndexNotificationNotificationId),
      updateBadgeCounter(),
  ]);
}

const _requestTypeKey = "requestType";
const _channelIdKey = "channelId";
const _itemTagsKey = "itemTags";

enum NotificationRequestType {
  friend, channelDisclosureConsent,
  fidoRegistrationTokenResult, didIssuanceResult, vcIssuanceResult,
}

NotificationRequestType? parseNotificationRequestType(
  Map<String, String?> payload,
) {
  var requestType = payload[_requestTypeKey];
  if (requestType == null) return null;

  var index = int.tryParse(requestType);
  if (index == null) return null;

  return NotificationRequestType.values[index];
}

String? parseNotificationChannelId(Map<String, String?> payload) =>
  payload[_channelIdKey];

Iterable<String> parseNotificationItemTags(Map<String, String?> payload) =>
  payload[_itemTagsKey]?.split(",") ?? const [];

extension _ChannelNotificationId on ChannelBase {
  int get channelDisclosureConsentRequestNotificationId =>
    "${peer!.storage!.id}:${id}:disclosureConsentRequest".hashCode;

  int get fidoRegistrationTokenResultNotificationId =>
    "${peer!.storage!.id}:${id}:fidoRegistrationTokenResult".hashCode;

  int get didIssuanceResultNotificationId =>
    "${peer!.storage!.id}:${id}:didIssuanceResult".hashCode;

  int get vcIssuanceResultNotificationId =>
    "${peer!.storage!.id}:${id}:vcIssuanceResult".hashCode;
}

Future<List<String>?> _channelAndOwnerNameOf(ChannelBase channel) async {
  var channelId = channel.channelId;
  var email = channel.plrId?.email;
  if ((channelId == null) || (email == null)) return null;

  Future<String> nameOf(Stream dataStream, String fallback) async {
    var data;
    await for (var d in dataStream) {
      data = d;
    }
    return data?.name ?? fallback;
  }
  return await Future.wait([
      nameOf(channel.channelData(), channelId),
      nameOf(channel.userData(), email),
  ]);
}

Future<bool> notifyChannelDisclosureConsentRequest(
  RequestReception requestReception,
  ChannelDisclosureConsentRequest request,
) async {
  if (isDesktop) return false;

  var name;
  var owner; {
    var r = await _channelAndOwnerNameOf(request);
    if (r == null) {
      requestReception
        ..removePropertyModel(request)
        ..syncSilently();
      return false;
    }
    name = r[0];
    owner = r[1];
  }

  return await _notifications.createNotification(
    content: NotificationContent(
      id: request.channelDisclosureConsentRequestNotificationId,
      channelKey: requestChannel,
      title: "A consent to disclosure request for the channel has been received.".i18n,
      body: "- ${name} (${owner})",
      payload: {
        _requestTypeKey:
          "${NotificationRequestType.channelDisclosureConsent.index}",
        _storageIdKey: "${request.peer!.storage!.id}",
        _channelIdKey: "${request.id}",
      },
    ),
  );
}

Future<void> dismissChannelDisclosureConsentRequestNotification(
  ChannelBase channel,
) async {
  if (isDesktop) return;
  await _notifications.dismiss(
    channel.channelDisclosureConsentRequestNotificationId,
  );
}

Future<bool> _notifyRegistrationResult(
  RequestReception requestReception,
  TrustedWebResult result,
  int notificationId,
  String title,
  NotificationRequestType type,
) async {
  if (isDesktop) return false;

  var name;
  var owner; {
    var r = await _channelAndOwnerNameOf(result);
    if (r == null) {
      requestReception
        ..removePropertyModel(result)
        ..syncSilently();
      return false;
    }
    name = r[0];
    owner = r[1];
  }

  var errorMessage = result.errorMessage ?? "";
  if (errorMessage.isNotEmpty) errorMessage = "\n! ${errorMessage}";

  return await _notifications.createNotification(
    content: NotificationContent(
      id: notificationId,
      channelKey: requestChannel,
      title: title.i18n,
      body: "- ${name} (${owner})" + errorMessage,
      payload: {
        _requestTypeKey: "${type.index}",
        _storageIdKey: "${result.peer!.storage!.id}",
        _channelIdKey: "${result.id}",
        _itemTagsKey: result.resultItemTags.join(","),
      },
    ),
  );
}

Future<bool> notifyFidoRegistrationTokenResult(
  RequestReception requestReception,
  TrustedWebResult result,
) => _notifyRegistrationResult(
  requestReception, result,
  result.fidoRegistrationTokenResultNotificationId,
  "A result to issue FIDO registration token has been received.",
  NotificationRequestType.fidoRegistrationTokenResult,
);

Future<void> dismissFidoRegistrationTokenResultNotification(
  ChannelBase channel,
) async {
  if (isDesktop) return;
  await _notifications.dismiss(
    channel.fidoRegistrationTokenResultNotificationId,
  );
}

Future<bool> notifyDidIssuanceResult(
  RequestReception requestReception,
  TrustedWebResult result,
) => _notifyRegistrationResult(
  requestReception, result,
  result.didIssuanceResultNotificationId,
  "A result to issue DID has been received.",
  NotificationRequestType.didIssuanceResult,
);

Future<void> dismissDidIssuanceResultNotification(
  ChannelBase channel,
) async {
  if (isDesktop) return;
  await _notifications.dismiss(channel.didIssuanceResultNotificationId);
}

Future<bool> notifyVcIssuanceResult(
  RequestReception requestReception,
  TrustedWebResult result,
) => _notifyRegistrationResult(
  requestReception, result,
  result.vcIssuanceResultNotificationId,
  "A result to issue certificate has been received.",
  NotificationRequestType.vcIssuanceResult,
);

Future<void> dismissVcIssuanceResultNotification(
  ChannelBase channel,
) async {
  if (isDesktop) return;
  await _notifications.dismiss(channel.vcIssuanceResultNotificationId);
}

//

final _badgeCounterLock = Lock();

Future<int?> updateBadgeCounter() async {
  if (isDesktop || Platform.isAndroid) return null;

  return _badgeCounterLock.synchronized(() async {
      var count = 0;
      for (var r in await notificationRegistryManager.registries) {
        for (var n in await r.notifications) {
          dynamic i = await n.info;
          if (i == null) continue;

          //if (n.isEnabled && n.needNotify) {
          if (i.isEnabled) {
            if (
              ((i is FriendNotificationInfo) && i.needNotify) ||
              ((i is ChannelNotificationInfo) && i.needTimelineNotify)
            ) count++;
          }
        }
      }
      count += (await channelIndexesToNotify()).length;

      await _notifications.setGlobalBadgeCounter(count);

      return count;
  });
}

Future<int?> decrementBadgeCounter() async {
  if (isDesktop || Platform.isAndroid) return null;

  return await _badgeCounterLock.synchronized(
    () async => await _notifications.decrementGlobalBadgeCounter(),
  );
}

Future<void> dismissAllNotifications() async {
  if (isDesktop) return null;

  await Future.wait([
      _notifications.dismissAllNotifications(),
      _notifications.resetGlobalBadge(),
  ]);
}
