import 'package:plr_ui/plr_ui.dart';

import '../logic.dart';

Future<bool?> addChannelDisclosureConsentRequest(
  PublicRoot publicRoot,
  Channel channel,
) async {
  var rr = await requestReceptionOf(publicRoot);
  if (rr == null) return null;

  return rr.addChannelDisclosureConsentRequest(channel);
}

Future<void> dismissChannelDisclosureConsentRequestByChannelId(
  Storage storage,
  String? channelId,
) async {
  if (channelId == null) return;

  var rr = await requestReceptionByStorage(storage);
  if (rr == null) return;

  var req = rr.channelDisclosureConsentRequestOfId(channelId);
  if (req == null) return;

  dismissChannelDisclosureConsentRequestNotification(req);
}

Future<bool> removeChannelDisclosureConsentRequest(
  Storage storage,
  String? channelId,
) async {
  if (channelId == null) return false;

  var rr = await requestReceptionByStorage(storage);
  if (rr == null) return false;

  var req = rr.channelDisclosureConsentRequestOfId(channelId);
  if (req == null) return true;

  dismissChannelDisclosureConsentRequestNotification(req);

  if (!rr.removeChannelDisclosureConsentRequest(req)) return false;
  await rr.syncSilently();

  return true;
}

Future<bool> removeFidoRegistrationTokenResult(
  Storage storage,
  String? channelId,
) async {
  if (channelId == null) return false;

  var rr = await requestReceptionByStorage(storage);
  if (rr == null) return false;

  var req = rr.fidoRegistrationTokenResultOfId(channelId);
  if (req == null) return true;

  dismissFidoRegistrationTokenResultNotification(req);

  if (!rr.removeFidoRegistrationTokenResult(req)) return false;
  await rr.syncSilently();

  return true;
}

Future<bool> removeDidIssuanceResult(
  Storage storage,
  String? channelId,
) async {
  if (channelId == null) return false;

  var rr = await requestReceptionByStorage(storage);
  if (rr == null) return false;

  var req = rr.didIssuanceResultOfId(channelId);
  if (req == null) return true;

  dismissDidIssuanceResultNotification(req);

  if (!rr.removeDidIssuanceResult(req)) return false;
  await rr.syncSilently();

  return true;
}

Future<bool> removeVcIssuanceResult(
  Storage storage,
  String? channelId,
) async {
  if (channelId == null) return false;

  var rr = await requestReceptionByStorage(storage);
  if (rr == null) return false;

  var req = rr.vcIssuanceResultOfId(channelId);
  if (req == null) return true;

  dismissVcIssuanceResultNotification(req);

  if (!rr.removeVcIssuanceResult(req)) return false;
  await rr.syncSilently();

  return true;
}
