import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../logic.dart' show dismissAllNotifications;

Future<bool> removeAccount(WidgetRef ref) async {
  await ref.read(plrStorageProvider.notifier).remove();
  if (ref.read(plrStorageProvider).value != null) {
    return false;
  }
  await dismissAllNotifications();

  return true;
}

Future<void> deleteAccount(WidgetRef ref) async {
  await ref.read(plrStorageProvider.notifier).destroy();
  await dismissAllNotifications();
}
