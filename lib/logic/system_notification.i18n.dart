import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart' show lifeRecordTranslation;

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  lifeRecordTranslation +
  {
    "en_us": "Request notifications.",
    "ja_jp": "通知設定",
  } +
  {
    "en_us": "This app will send notifications.\n\nPlease tap 'Setting' button below and allow notifications.",
    "ja_jp": "このアプリは通知を送信します。\n\n下部の「設定」ボタンをタップして、通知を許可してください。",
  } +
  {
    "en_us": "Setting",
    "ja_jp": "設定",
  } +
  {
    "en_us": "Later",
    "ja_jp": "後で",
  } +
  {
    "en_us": "Content updates",
    "ja_jp": "コンテンツの更新",
  } +
  {
    "en_us": "Notify updates of channel or friend content.",
    "ja_jp": "チャネルや友達のコンテンツの更新を通知します。",
  } +
  {
    "en_us": "Account",
    "ja_jp": "アカウント",
  } +
  {
    "en_us": "Notify about your account, such as passphrase has been changed on other devices.",
    "ja_jp": "他の端末でパスフレーズが更新された時など、アカウントに関する通知を行ないます。",
  } +
  {
    "en_us": "Friend",
    "ja_jp": "友達",
  } +
  {
    "en_us": "Notify about friend changes, such as automatically added.",
    "ja_jp": "友達が自動追加された時など、友達に関する通知を行ないます。",
  } +
  {
    "en_us": "Health data coordination",
    "ja_jp": "ヘルスデータ連携",
  } +
  {
    "en_us": "Notify about health data coordination.",
    "ja_jp": "ヘルスデータ連携に関する通知を行ないます。",
  } +
  {
    "en_us": "Notifications by channel retrieval key",
    "ja_jp": "チャネル検索キーによる通知",
  } +
  {
    "en_us": "Notify the channel retrieval key at the set notificationLeadTime.",
    "ja_jp": "通知リードタイムに基づき、チャネル検索キーによる通知を行ないます。",
  } +
  {
    "en_us": "Requests and Results",
    "ja_jp": "リクエストと結果",
  } +
  {
    "en_us": "Notify the incoming requests and results.",
    "ja_jp": "受信したリクエスト、および結果の通知を行ないます。",
  } +
  {
    "en_us": "Friend content has been updated.",
    "ja_jp": "友達の更新があります。",
  } +
  {
    "en_us": "Channel content has been updated.",
    "ja_jp": "チャネルの更新があります。",
  } +
  {
    "en_us": "Timeline has been updated.",
    "ja_jp": "タイムラインの更新があります。",
  } +
  {
    "en_us": "Channel and timeline has been updated.",
    "ja_jp": "チャネル・タイムラインの更新があります。",
  } +
  {
    "en_us": "Passphrase has been changed.",
    "ja_jp": "パスフレーズが変更されました。",
  } +
  {
    "en_us": "Health data has been retrieved.",
    "ja_jp": "ヘルスデータを取得しました。",
  } +
  {
    "en_us": "Friend has been registered.",
    "ja_jp": "友達が登録されました。",
  } +
  {
    "en_us": "Passphrase has been deposited to the friend.",
    "ja_jp": "友達にパスフレーズが預けられました。",
  } +
  {
    "en_us": "The info channel of the friend has been subscribed.",
    "ja_jp": "友達の広報チャネルを購読しました。",
  } +
  {
    "en_us": "Channels has been disclosed to the friend.",
    "ja_jp": "友達にチャネルを開示しました。",
  } +
  {
    "en_us": "Nodes has been disclosed to the friend.",
    "ja_jp": "友達にノードを開示しました。",
  } +
  {
    "en_us": "Notification: ",
    "ja_jp": "通知: ",
  } +
  {
    "en_us": "A consent to disclosure request for the channel has been received.",
    "ja_jp": "チャネルの開示同意要求が届いています。",
  } +
  {
    "en_us": "A result to issue FIDO registration token has been received.",
    "ja_jp": "FIDO登録トークン発行結果が届いています。",
  } +
  {
    "en_us": "A result to issue DID has been received.",
    "ja_jp": "DID発行結果が届いています。",
  } +
  {
    "en_us": "A result to issue certificate has been received.",
    "ja_jp": "証明書発行結果が届いています。",
  };

  String get i18n => localize(this, t);
}
