import 'dart:ui' show PointerDeviceKind;

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_extension.dart';
import 'package:plr_ui/plr_ui.dart';

import 'theme.dart';
import 'constants.dart' show appName;
import 'page/sign/sign_page.dart';

class _ScrollBehavior extends MaterialScrollBehavior {
  const _ScrollBehavior();

  @override
  Set<PointerDeviceKind> get dragDevices => const {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
    PointerDeviceKind.trackpad,
  };
}

class App extends StatefulWidget {
  static void restart(BuildContext context) =>
    context.findAncestorStateOfType<_AppState>()?.restart();

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  Key key = UniqueKey();
  void restart() => setState(() => key = UniqueKey());

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: _build(context),
    );
  }

  Widget _build(BuildContext context) => MaterialApp(
    localizationsDelegates: const [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      const PlrLocalizationsDelegate(),
    ],
    supportedLocales: const [
      const Locale('en', 'US'), // English
      const Locale('ja', 'JP'), // Japanese
    ],
    scrollBehavior: const _ScrollBehavior(),
    title: appName,
    theme: defaultTheme,
    home: I18n(
      child: const SignPage(),
    ),
    debugShowCheckedModeBanner: false,
  );
}
