import 'package:dart_vlc/dart_vlc.dart';
import 'package:flutter/material.dart' hide Notification;
import 'package:hooks_riverpod/hooks_riverpod.dart' show ProviderScope;
import 'package:personaryFlutter/flavors.dart';
import 'package:plr_ui/plr_ui.dart';

import 'app.dart' show App;
import 'config.dart';
import 'logic.dart';
import 'util.dart';

void onError(Object error, [ StackTrace? stack ]) {
  print(error);
  if (stack != null) print(stack);
}

void main(List<String> args) async {
  await initCommon();

  await startPlr(
    config.copyWith(
      appLinksConfig: PlrAppLinksConfig(appArgs: args),
    ),
  );

  await mayStartWindowTask();

  _initVideo();

  runApp(ProviderScope(child: App()));
}

Future<void> initCommon() async {
  _initFlavor();
  await initializeNotifications();
}

void _initFlavor() {
  //flavor切り替え
  const flavor = String.fromEnvironment('FLAVOR');
  if (flavor == 'pasteld') {
    F.appFlavor = Flavor.PASTELD;
  } else {
    //flavor未指定の場合はPersonaryを指定する
    F.appFlavor = Flavor.PERSONARY;
  }
  print('Flavor:${F.appFlavor}');
}

void _initVideo() {
  if (!isDesktop) return;

  try {
    DartVLC.initialize();
  } catch (e, s) {
    onError(e, s);
  }
}
