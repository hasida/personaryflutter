import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

const defaultColorScheme = Colors.lightBlue;
const defaultBackgroundColor = Color(0xfff5f5f5);
const defaultFocusColor = Color(0xff4283c0);
const defaultTextButtonColor = Colors.blue;
const Color? defaultAppBarBackgroundColor = null;
const Color? defaultAppBarForegroundColor = null;
const defaultNavigationBarBackgroundColor = Color(0xfff5f5f5);
const Color? defaultNavigationBarIndicatorColor = null;
const defaultFabBackgroundColor = Color(0xfff2b944);
const defaultFabForegroundColor = Colors.white;
const defaultCardColor = Colors.white;
const defaultCardElevation = 2.0;
const defaultSectionHeaderColor = Color(0xff4283c0);

ThemeData _createThemeData({
    MaterialColor? colorSchemeSeed,
    Color? backgroundColor,
    Color? focusColor,
    Color? textButtonColor,
    Color? appBarBackgroundColor,
    Color? appBarForegroundColor,
    Color? navigationBarBackgroundColor,
    Color? navigationBarIndicatorColor,
    Color? fabBackgroundColor,
    Color? fabForegroundColor,
    Color? cardColor,
    double? cardElevation,
    Color? sectionHeaderColor,
}) => ThemeData(
  colorSchemeSeed: colorSchemeSeed ?? Colors.lightBlue,
  scaffoldBackgroundColor: backgroundColor ?? defaultBackgroundColor,
  dialogBackgroundColor: backgroundColor ?? defaultBackgroundColor,

  inputDecorationTheme: InputDecorationTheme(
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: focusColor ?? defaultFocusColor),
    ),
  ),

  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(
      foregroundColor: textButtonColor ?? defaultTextButtonColor,
    ),
  ),

  appBarTheme: AppBarTheme(
    backgroundColor: appBarBackgroundColor ?? defaultAppBarBackgroundColor,
    foregroundColor: appBarForegroundColor ?? defaultAppBarForegroundColor,
    surfaceTintColor: Colors.transparent,
    shadowColor: Colors.black,
    elevation: 4,
  ),

  navigationBarTheme: NavigationBarThemeData(
    backgroundColor: navigationBarBackgroundColor
      ?? defaultNavigationBarBackgroundColor,
    indicatorColor: navigationBarIndicatorColor
      ?? defaultNavigationBarIndicatorColor,
  ),

  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: fabBackgroundColor ?? defaultFabBackgroundColor,
    foregroundColor: fabForegroundColor ?? defaultFabForegroundColor,
    shape: const CircleBorder(),
    extendedSizeConstraints: const BoxConstraints.tightFor(
      height: 48,
    ),
  ),

  iconButtonTheme: const IconButtonThemeData(
    style: ButtonStyle(
      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    ),
  ),

  cardTheme: CardTheme(
    color: cardColor ?? defaultCardColor,
    elevation: cardElevation ?? defaultCardElevation,
  ),

  textTheme: TextTheme(
    headlineLarge: TextStyle(
      color: sectionHeaderColor ?? defaultSectionHeaderColor,
      fontSize: 18,
      fontWeight: FontWeight.bold,
    ),
  ),
);
final defaultTheme = _createThemeData();

extension AppDefinitionTheme on AppDefinitionInterface {
  ThemeData get theme => _createThemeData(
    colorSchemeSeed: colorSchemeSeed,
    backgroundColor: backgroundColor,
    focusColor: focusColor,
    textButtonColor: textButtonColor,
    appBarBackgroundColor: appBarBackgroundColor,
    appBarForegroundColor: appBarForegroundColor,
    navigationBarBackgroundColor: navigationBarBackgroundColor,
    navigationBarIndicatorColor: navigationBarIndicatorColor,
    fabBackgroundColor: fabBackgroundColor,
    fabForegroundColor: fabForegroundColor,
    cardColor: cardColor,
    cardElevation: cardElevation,
    sectionHeaderColor: sectionHeaderColor,
  );
}
