import 'flavors.dart';

const appName_personary = "Personary 2021";
const appIcon_personary = "assets/app-icon.png";
const appLegalese_personary = "Assemblogue Inc.";

const appName_pasteld = "PastelD";
const appIcon_pasteld = "assets/app-icon-pasteld.png";
const appLegalese_pasteld = "Meditrina Inc.";
const creatorGPT4 = "GPT-4";
const creatorPAI = "PAI";

final appName = (
  (F.appFlavor == Flavor.PASTELD) ? appName_pasteld : appName_personary
);
final appIcon = (
  (F.appFlavor == Flavor.PASTELD) ? appIcon_pasteld : appIcon_personary
);
final appLegalese = (
  (F.appFlavor == Flavor.PASTELD) ? appLegalese_pasteld : appLegalese_personary
);

final termsOfUseLink = Uri.parse(
  "https://www.assemblogue.com/apps/PersonaryTermsOfUse.html",
);
final privacyPolicyLink = Uri.parse(
  "https://www.assemblogue.com/apps/PersonaryPrivacyPolicy.html",
);
final supportLink = Uri.parse(
  "https://docs.google.com/forms/d/1PMPc52e9Sobt-ykemwTaGGU3_VTOvVF9xmjmJNdY3cg",
);
final GoogleAPIServicesUserDataPolicyLink = Uri.parse(
  "https://developers.google.com/terms/api-services-user-data-policy#additional_requirements_for_specific_api_scopes",
);

final rateLinkIOS = Uri.parse(
  "https://itunes.apple.com/jp/app/id1551318386?mt=8&action=write-review",
);

final rateLinkAndroid = Uri.parse(
  "https://play.google.com/store/apps/details?id=com.assemblogue.plr.app.android.plr",
);
