import 'dart:async';
import 'dart:math';

import 'package:image/src/formats/formats.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:diagram_editor/src/abstraction_layer/rw/canvas_reader.dart';
import "package:intl/intl.dart";
import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../constants.dart';
import '../../util/chat_api.dart';
import '../../util/position.dart';
import '../data/custom_component_data.dart';
import '../model/sem_link_model.dart';
import '../model/sem_node_model.dart';
import '../widget/component/component_size.dart';

extension ItemGraphSetting on Item {
  DataSetting? get graphSetting => firstPropertyModelOf("graphSetting");
  bool get hasGraphSetting => graphSetting != null;
}

/// topGraph のアイテムを取得
/// 存在しない場合は作成する
Future<Item> getTopGraphItem(
    Timeline timeline, String userPlrId, Oss? systemOss, bool enc) async {
  logout("getTopGraphItem start.");
  Item? topGraphItem = timeline.topGraphItem;

  // チャネルにtopGraphLinkがある場合
  if (topGraphItem != null) {
    var hasGraphSetting = topGraphItem.hasGraphSetting;
    if (!hasGraphSetting) {
      await for (final _ in topGraphItem.sync().handleError((_) {})) {
        if (hasGraphSetting = topGraphItem.hasGraphSetting) break;
      }
    }
    if (!hasGraphSetting) {
      makeGraphSetting(topGraphItem, systemOss, userPlrId, enc);
    }
  } else {
    // チャネルにtopGraphLinkがない場合、topGraphItem作成
    logout("no topGraph property.");
    topGraphItem = await createTopGraph(timeline, userPlrId, systemOss, enc);
  }
  logout("getTopGraphItem end.");
  return topGraphItem;
}

/// RootGraphのアイテム作成とtopGraphLinkにアイテム設定
Future<Item> createTopGraph(
    Timeline timeline, String userPlrId, Oss? systemOss, bool enc) async {
  logout("createTopGraph start.");
  // topGraphのアイテムを作成
  Item topGraphItem =
      await createTimelineItem(timeline, userPlrId, "RootGraph", enc);

  // 作成したアイテムをtopGraphとして設定
  //timeline.newLiteral("topGraph", clearOthers: true, value: topGraphItem.id);

  // topGraphLinkにピア設定
  timeline.topGraphItem = topGraphItem;
  timeline.syncSilently();

  // topGraphのアイテムにGraphSettingを作成
  makeGraphSetting(topGraphItem, systemOss, userPlrId, enc);

  logout("createTopGraph end.");
  return topGraphItem;
}

/// チャネルのデータ設定からオントロジーを取得、設定
Future<void> copyOssFileFromChannel(
    DataSetting target, Timeline timeline) async {
  logout("copyOssFileFromChannel start.");
  // チャネルのデータ設定
  DataSetting? channelDataSetting = (timeline as Channel).channelDataSetting;
  if (channelDataSetting == null) {
    logout("channelDataSetting is null.");
    logout("copyOssFileFromChannel end.");
    return;
  }
  await channelDataSetting.syncSilently();
  // 共通から
  Set<OssFile> ontologyFilesSet = channelDataSetting.ontologyFiles.toSet();
  Set<OssFile> restrictionFilesSet =
      channelDataSetting.restrictionFiles.toSet();
  Set<OssFile> constraintFilesSet = channelDataSetting.constraintFiles.toSet();
  // 内部スキーマから
  for (String s in internalSchemaClasses) {
    Schema? schema = channelDataSetting.internalSchemaOf(s);
    if (schema == null) {
      logout("internalSchema[${s}] is null.");
      continue;
    }
    ontologyFilesSet.addAll(schema.ontologyFiles);
    restrictionFilesSet.addAll(schema.restrictionFiles);
    constraintFilesSet.addAll(schema.constraintFiles);
  }

  // targetにDiscourseGraphが含まれていない場合は強制的に設定する
  // この強制的な設定はgraphSetting作成時のみに行うように変更になったのでコメント化
  /*
  OssFile chkDg = ontologyFilesSet.firstWhere(
      (element) => element.title.value == "Discourse Graph",
      orElse: () => null);
  if (chkDg == null) {
    OssFile dg = systemOss.ontologyFiles
        .firstWhere((element) => element.title.value == "Discourse Graph");
    ontologyFilesSet.add(dg);
    logout("copyOssFileFromChannel: Discourse Graph Ontology add forcibly.");
  }
  */

  // コピー先の設定を取得
  Set<String> targetOntologyFilesIds = {};
  Set<String> targetRestrictionFilesIds = {};
  Set<String> targetConstraintFilesIds = {};
  targetOntologyFilesIds.addAll(
    target.ontologyFiles.map((e) => e.id).nonNulls,
  );
  targetRestrictionFilesIds.addAll(
    target.restrictionFiles.map((e) => e.id).nonNulls,
  );
  targetConstraintFilesIds.addAll(
    target.constraintFiles.map((e) => e.id).nonNulls,
  );

  bool isSync = false;
  // targetに含まれていないものをchannelからコピー
  for (var f in ontologyFilesSet) {
    if (!targetOntologyFilesIds.contains(f.id)) {
      logout("copyOssFileFromChannel: ontologyfile[${f.title}] add.");
      target.addOntologyFile(f);
      isSync = true;
    }
  }
  for (var r in restrictionFilesSet) {
    if (!targetRestrictionFilesIds.contains(r.id)) {
      logout("copyOssFileFromChannel: restrictionfile[${r.title}] add.");
      target.addRestrictionFile(r);
      isSync = true;
    }
  }
  for (var c in constraintFilesSet) {
    if (!targetConstraintFilesIds.contains(c.id)) {
      logout("copyOssFileFromChannel: constraintfile[${c.title}] add.");
      target.addConstraintFile(c);
      isSync = true;
    }
  }
  if (isSync) {
    target.syncSilently();
    logout("copyOssFileFromChannel sync.");
  }
  logout("copyOssFileFromChannel end.");
}

/// 親のグラフアイテムのデータ設定からオントロジーを取得、設定
Future<void> copyOssFileFromParent(DataSetting target, Item parent) async {
  logout("copyOssFileFromParent start.");
  // 親グラフノードのデータ設定
  if (!parent.hasProperty("graphSetting")) {
    logout("parent has no graphSetting property.");
    logout("copyOssFileFromParent end.");
    return;
  }
  DataSetting? parentDataSetting =
      parent.firstPropertyModelOf<DataSetting>("graphSetting");
  if (parentDataSetting == null) {
    logout("parent dataSetting is null.");
    logout("copyOssFileFromParent end.");
    return;
  }
  await parentDataSetting.syncSilently();
  // 共通から
  Set<OssFile> ontologyFilesSet = parentDataSetting.ontologyFiles.toSet();
  Set<OssFile> restrictionFilesSet = parentDataSetting.restrictionFiles.toSet();
  Set<OssFile> constraintFilesSet = parentDataSetting.constraintFiles.toSet();

  // 子の設定を取得(ID)
  Set<String> childOntologyFilesIds = {};
  Set<String> childRestrictionFilesIds = {};
  Set<String> childConstraintFilesIds = {};
  childOntologyFilesIds.addAll(
    target.ontologyFiles.map((e) => e.id).nonNulls,
  );
  childRestrictionFilesIds.addAll(
    target.restrictionFiles.map((e) => e.id).nonNulls,
  );
  childConstraintFilesIds.addAll(
    target.constraintFiles.map((e) => e.id).nonNulls,
  );

  bool isSync = false;
  // 子に含まれていないものをコピー
  for (var f in ontologyFilesSet) {
    if (!childOntologyFilesIds.contains(f.id)) {
      logout("copyOssFileFromParent: ontologyfile[${f.title}] add.");
      target.addOntologyFile(f);
      isSync = true;
    }
  }
  for (var r in restrictionFilesSet) {
    if (!childRestrictionFilesIds.contains(r.id)) {
      logout("copyOssFileFromParent: restrictionfile[${r.title}] add.");
      target.addRestrictionFile(r);
      isSync = true;
    }
  }
  for (var c in constraintFilesSet) {
    if (!childConstraintFilesIds.contains(c.id)) {
      logout("copyOssFileFromParent: constraintfile[${c.title}] add.");
      target.addConstraintFile(c);
      isSync = true;
    }
  }
  if (isSync) {
    target.syncSilently();
    logout("copyOssFileFromParent sync.");
  }
  logout("copyOssFileFromParent end.");
}

/// タイムラインアイテムを作成
Future<Item> createTimelineItem(
    Timeline timeline, String userPlrId, String type, bool enc,
    [ sync = true ]) async {
  Item newItem =
      await timeline.newTimelineItemModel(type, begin: DateTime.now(), encrypt: enc);
  newItem.newLiteral("creator", value: userPlrId);
  if (sync) {
    await newItem.commitNewVersion();
    //newItem.syncNewTimelineItems();
    await newItem.syncSilently();
  }
  return newItem;
}

/// itemにgraphSettingを作成
DataSetting makeGraphSetting(Item item, Oss? systemOss, String userPlrId, bool enc) {
  logout("makeGraphSetting start.");
  // graphSettingが既にある場合は削除
  if (item.hasProperty("graphSetting")) {
    item.removePropertiesOf("graphSetting");
  }
  // インナーノードとしてgraphSettingのEntityNodeを作成
  EntityNode graphSettingEntityNode =
      item.newInnerEntity("graphSetting", "GraphSetting", encrypt: enc);
  // DataSettingとして設定
  DataSetting graphDataSetting = DataSetting.create(graphSettingEntityNode, PlrId.fromString(userPlrId));

  graphDataSetting.syncSilently();
  logout("makeGraphSetting end.");
  return graphDataSetting;
}

/// OSSの読み込み
Future<Oss?> loadOsses() async {
  logout("SE loadOsses() start ");

  // システムアカウント
  var publicRoot = (await systemAccount).publicRoot;
  await publicRoot.syncSilently();
  Oss? oss = publicRoot.oss;

  logout("SE loadOsses() end ");
  return oss;
}

/// graphSettingからSchemataを作成
Future<Schemata> loadSemSchemata(Item graphItem) async {
  logout("SE _loadSemSchemata() start ");

  final ossFileSets = graphItem.graphSetting?.ossFiles.toSet();
  if (ossFileSets == null) return Schemata(const []);

  // 下記schemataFromは、baseのオントロジー・制限を必ずschemataに設定する
  Stream<Schemata?> schemataStream = schemataFrom(ossFileSets);
  List<Schemata?> schemataList = [];
  // 上記schemataFromは、オフライン時にstreamが終了しないように見える
  // そのためawait forによる処理をやめた。
  // semanticEditorの場合、schemataは1個しか返ってこない前提になっている。
  //await for (var s in schemataStream) {
  //  schemataList.add(s);
  //}
  schemataStream.listen((event) {
    schemataList.add(event);
    logout("SE _loadSemSchemata() schemataList add.");
  });
  await Future.delayed(Duration.zero);

  // schemata待ち(delay:100msec)
  // ここは結構時間がかかる場合がある（2~5秒待ちはよくあること）
  int loopCounter = 1;
  while (schemataList.length == 0) {
    logout("schemata waiting loop.");
    await Future.delayed(const Duration(milliseconds: 100));
    if (loopCounter == 300) {
      logout("SE _loadSemSchemata() timeout.");
      throw ("timeout");
    }
    loopCounter++;
  }
  logout("SE _loadSemSchemata() end ");

  return schemataList.first ?? Schemata(const []);
}

setLastUpdated(var e) {
  DateTime now = DateTime.now().toLocal();
  e.newLiteral("_lastUpdated", value: now, clearOthers: true);
}

List<SchemaClass>? getLinkList(List<Schemata> schemataList) {
  Set<SchemaClass>? labelSet = {};
  for (Schemata schemata in schemataList) {
    labelSet.addAll(schemata.classOf(SemLinkModel.graphRelationClass)!.subs.toList());
  }
  return labelSet.toList();
}

List<SchemaClass> getPropertyList(List<SchemaClass> classList) {
  List<SchemaClass>?  schemaClassList=[];
  for (SchemaClass schemaClass in classList) {
    if (schemaClass.subs.isEmpty) {
      schemaClassList.add(schemaClass);
    } else {
      schemaClassList.addAll(getPropertyList(schemaClass.subs.toList()));
    }
  }
  return schemaClassList;
}

SchemaClass? getMatchLabel(List<SchemaClass>? classList, String history) {
  if (classList == null) return null;
  classList = getPropertyList(classList);
  for (SchemaClass schemaClass in classList) {
    if (history == schemaClass.id) return schemaClass;
  }
  return null;
}

//不正なデータがあれば補正する
correctArg(SemLinkModel item) {
  if (item.isSymmetric) {
    correctSymmetric(item);
  } else {
    correctAsymmetric(item, SemLinkModel.arg1Property, SemLinkModel.arg2Property);
    correctAsymmetric(item, SemLinkModel.arg2Property, SemLinkModel.arg1Property);
  }
  item.linkEntity.syncSilently();
}

//対称な種別で不正なデータがあれば補正する
correctSymmetric(SemLinkModel item) {
  // 対称リンクだがarg2に1個以上登録がある⇒すべてarg1に移す
  List<Entity> targetList = item.linkEntity.propertyModelsOf<Entity>(SemLinkModel.arg2Property).toList();
  for (Entity target in targetList) {
    item.linkEntity.removePropertyModelFrom(SemLinkModel.arg2Property, target);
    item.linkEntity.addPropertyModel<Entity>(SemLinkModel.arg1Property, target);
  }
}

//非対称な種別で不正なデータがあれば補正する
correctAsymmetric(SemLinkModel item, String sourceProperty, String targetProperty) {
  List<Entity> sourceList = item.linkEntity.propertyModelsOf<Entity>(sourceProperty).toList();
  List<Entity> targetList = item.linkEntity.propertyModelsOf<Entity>(targetProperty).toList();
  List<String> targetIdList = List.generate(targetList.length, (i) => targetList[i].id!);
  bool isDuplicate = true;
  // 非対称リンクだがarg1やarg2に複数登録されている場合
  while (item.linkEntity.propertyModelsOf<Entity>(sourceProperty).length > 1) {
    if (isDuplicate) {
      int count = 0;
      for (Entity source in sourceList) {
        // 同じノードが登録されている場合は、優先的に消去
        if (targetIdList.contains(source.id)) {
          item.linkEntity.removePropertyModelFrom(sourceProperty, source);
          break;
        } else {
          count += 1;
        }
      }
      //その他のノードも登録が一つになるまで消去
      if (count == sourceList.length) isDuplicate = false;
    } else {
      bool isComplete = item.linkEntity.removePropertyModelFrom(sourceProperty, sourceList.last);
      if (!isComplete) break;
      //登録が0個の端点があればそちらにノードの登録を移す
      if (targetList.isEmpty) item.linkEntity.addPropertyModel(targetProperty, sourceList.last);
    }
  }
}

/// ノード追加
void addPosition(SemNodeModel model, bool enc, Graph graph, OccurrenceType? occurrence, {RecordEntity? recordEntity}) {
  Entity item;

  if (recordEntity == null) {
    item = model.recordEntity!.entity!;
  } else {
    item = recordEntity.entity!;
  }
  addElementAndSync(graph, item as Item, occurrence, coordinates: Point(model.posOffset?.dx ?? 200, model.posOffset?.dy ?? 200));
}

Future<void> addElementAndSync(Graph graph,Item item, OccurrenceType? occurrence, {Point<double>? coordinates, Function? syncAdd}) async {
  await graph.addElement(item, coordinates: coordinates, occurrence: occurrence).then((element) {
    element.commitAttributes().then((_) {
      element.syncAttributesSilently();
      if (syncAdd != null) syncAdd(element.id);
    });
  });
}

GraphElement? getElement(Graph graph, String nodeId) {
  String parseNodeId = nodeId;
  //#からノードIDが始まる場合があるので、その場合はノードIDの#を削除する
  if (nodeId.startsWith(uriFragmentSign)) {
    parseNodeId = nodeId.substring(uriFragmentSign.length);
  }
  return graph.elementByIdOf(parseNodeId);
}

/// ノード移動
Future<void> setPosition(Graph graph, String componentId, Offset offset, {Function? syncDelete}) async {
  await graph.syncSilently();
  GraphElement? element = getElement(graph, componentId);
  if (element == null) {
    if (syncDelete != null) syncDelete(componentId);
    return;
  }

  element.setCoordinates(Point(offset.dx, offset.dy));
  await element.syncAttributesSilently().then((_) {
    if (syncDelete != null) syncDelete(element.id);
  });
}

Future<void> deleteElement(Graph graph, GraphElement element, {Function? syncDelete}) async {
  element.delete();
  await element.syncAttributesSilently();
  element.commitAttributes().then((_) {
    if (syncDelete != null) syncDelete(element.id);
  });
}

/// ノード削除
Future<void> deleteNode(Graph graph, RecordEntity delNode, bool isUpdate, Timeline timeline, {Function? syncDelete}) async {
  await Future.wait(graph.elements.map((element) async {
    String? id = element.id;
    if (id == null) return;

    // ノード削除
    if (id == delNode.entity!.id) {
      timeline.removePropertyFrom(SemNodeModel.graphLinkProperty, element.node);
      await deleteElement(graph, element, syncDelete: syncDelete);
      timeline.syncSilently();
      if (graph.elements.isEmpty) {
        timeline.removePropertyFrom(SemNodeModel.graphLinkProperty, graph.item.node);
        timeline.syncSilently();
      }
      return;
    }

    await element.syncSilently();

    String? targetId = getTargetId(element);
    List<String> componentIdList = getSourceIdList(element);
    if (targetId != null) componentIdList.add(targetId);

    // 接続リンクの削除
    for (String componentId in componentIdList) {
      if (componentId == delNode.entity!.id) {
        deleteLink(graph, timeline, element.id!, true, syncDelete: syncDelete);
      }
    }
  }));
}

/// リンク作成
Future<Entity> addLink(Graph graph, RecordEntity sourceNode, RecordEntity targetNode,
    String type, bool enc, String creator, Timeline timeline, bool isSymmetric, OccurrenceType? occurrence, {Function? syncAdd}) async {
  Item item = await createTimelineItem(timeline, creator, type, enc, false);

  setLastUpdated(item);

  item.addPropertyModel<Entity>(SemLinkModel.arg1Property, sourceNode.entity!);
  if (isSymmetric) {
    //シンメトリの場合
    item.addPropertyModel<Entity>(SemLinkModel.arg1Property, targetNode.entity!);
  } else {
    item.addPropertyModel<Entity>(SemLinkModel.arg2Property, targetNode.entity!);
  }

  item.commitNewVersion().whenComplete(
        () => item.syncSilently(),
  );

  addElementAndSync(graph, item, occurrence, syncAdd: syncAdd);

  return item;
}

/// リンクの線分追加
void branchLink(Entity e, RecordEntity sourceNode, {Function? syncDelete, Function? syncAdd}) async {
  setLastUpdated(e);
  e.addPropertyModel<Entity>(SemLinkModel.arg1Property, sourceNode.entity!);
  await e.commitNewVersion().whenComplete(
        () => e.syncSilently().then((_) {
          if (syncDelete != null) syncDelete(e.id);
          if (syncAdd != null) syncAdd(e.id);
        }),
  );
}

/// リンクの線分を異なるノードに付け替える
void reconnectLink(Entity e, Entity prevNodeEntity, Entity nextNodeEntity, {Function? syncDelete, Function? syncAdd}) async {
  await updatePath(e);
  setLastUpdated(e);

  if (e.removePropertyFrom(SemLinkModel.arg1Property, prevNodeEntity.node)) {
    e.addPropertyModel<Entity>(SemLinkModel.arg1Property, nextNodeEntity);
  }
  if (e.removePropertyFrom(SemLinkModel.arg2Property, prevNodeEntity.node)) {
    e.addPropertyModel<Entity>(SemLinkModel.arg2Property, nextNodeEntity);
  }

  await e.commitNewVersion().whenComplete(
        () => e.syncSilently().then((_) {
          if (syncDelete != null) syncDelete(e.id);
          if (syncAdd != null) syncAdd(e.id);
        }),
  );
}

/// リンク反転
Future<void> reverseLink(Entity e, bool isSymmetric, {Function? syncDelete}) async {
  if (isSymmetric) return;
  String? creator = e.firstLiteralValueOf("creator");
  String editor = e.masterStorage.plrId.toString();
  await updatePath(e);
  setLastUpdated(e);

  Entity n1 = e.firstPropertyModelOf<Entity>(SemLinkModel.arg1Property)!;
  Entity n2 = e.firstPropertyModelOf<Entity>(SemLinkModel.arg2Property)!;
  e.removePropertyModelFrom(SemLinkModel.arg1Property, n1);
  e.removePropertyModelFrom(SemLinkModel.arg2Property, n2);
  e.addPropertyModel<Entity>(SemLinkModel.arg1Property, n2);
  e.addPropertyModel<Entity>(SemLinkModel.arg2Property, n1);

  if (creator != editor) {
    e.newLiteral("editor", clearOthers: true, value: editor);
  } else if (e.firstLiteralValueOf("editor") != null) {
    e.removePropertiesOf("editor");
  }

  await e.commitNewVersion().whenComplete(
    () => e.syncSilently().then((_) {
      if (syncDelete != null) syncDelete(e.id);
    })
  );
}

/// シンメトリックと非シンメトリックの交替
Future<bool> changeLink(Entity e, Entity nodeEntity, bool nowIsSymmetric) async {
  await updatePath(e);
  if (nowIsSymmetric) {
    //arg1が1個arg2が1個であることを確認する。
    if (getSourceIdList(e).length == 1 && getTargetId(e) != null) {
      //非シンメトリックをシンメトリックへ
      if (e.removePropertyFrom(SemLinkModel.arg2Property, nodeEntity.node)) {
        e.addPropertyModel<Entity>(SemLinkModel.arg1Property, nodeEntity);
        return true;
      }
    }
  } else {
    if (getSourceIdList(e).length == 2 && getTargetId(e) == null) {
      //シンメトリックを非シンメトリックへ
      //矢印の向きは不定
      if (e.removePropertyFrom(SemLinkModel.arg1Property, nodeEntity.node)) {
        e.addPropertyModel<Entity>(SemLinkModel.arg2Property, nodeEntity);
        return true;
      }
    }
  }
  return false;
}

Future<void> updatePath(Entity e) async {
  List<Item> sourceList = e.propertyModelsOf<Item>(SemLinkModel.arg1Property).toList();
  List<Item> targetList = e.propertyModelsOf<Item>(SemLinkModel.arg2Property).toList();

  for (Item source in sourceList) {
    await source.node.syncSilently();
  }

  for (Item target in targetList) {
    await target.node.syncSilently();
  }
}

/// リンクプロパティ変更
Future<void> changeLinkClass(Entity e, SchemaClass schemaClass, {bool? isChangeLink, Entity? nodeEntity, bool? nowIsSymmetric, Function? syncDelete}) async{
  if (isChangeLink == true) {
    bool isComplete = await changeLink(e, nodeEntity!, nowIsSymmetric!);
    if (!isComplete) return;
  }

  String? creator = e.firstLiteralValueOf("creator");
  String editor = e.masterStorage.plrId.toString();
  e.type = schemaClass.id;
  setLastUpdated(e);

  if (creator != editor) {
    e.newLiteral("editor", clearOthers: true, value: editor);
  } else if (e.firstLiteralValueOf("editor") != null) {
    e.removePropertiesOf("editor");
  }

  await e.commitNewVersion().whenComplete(
    () async => await e.syncSilently().then((_) {
      if (syncDelete != null) syncDelete(e.id);
    }),
  );
}

/// リンク削除
Future<void> deleteLink(Graph graph, Timeline timeline, String nodeId, bool doCommit, {Function? syncDelete}) async {
  await graph.syncSilently();
  GraphElement? element = getElement(graph, nodeId);
  if (element == null) {
    if (syncDelete != null) syncDelete(nodeId);
    return;
  }
  setLastUpdated(element);
  deleteElement(graph, element, syncDelete: syncDelete);
}

/// リンク線分削除
Future<void> deleteBranch(Graph graph, Timeline timeline, Entity e, Entity nodeEntity, {Function? syncDelete, Function? syncAdd}) async {
  await updatePath(e);
  setLastUpdated(e);
  e.removePropertyFrom(SemLinkModel.arg1Property, nodeEntity.node);
  int arg1 = e.propertyModelsOf<Item>(SemLinkModel.arg1Property).toList().length;
  int arg2 = e.propertyModelsOf<Item>(SemLinkModel.arg2Property).toList().length;
  //リンク線分が2未満の場合はリンク自体を削除
  if (arg1 + arg2 < 2) {
    deleteLink(graph, timeline, e.id!, true, syncDelete: syncDelete);
  }
  e.commitNewVersion().whenComplete(
        () => e.syncSilently().then((_){
          if (syncDelete != null) syncDelete(e.id);
          if (syncAdd != null) syncAdd(e.id);
        }),
  );
}

/// 使用スキーマタ取得
List<Schemata> allSchemata(Schemata graphSchemata, Schemata rootGraphSchemata, Schemata timelineSchemata, bool isTopGraph) {
  List<Schemata> graphSchemataList = [];
  List<Schemata> schemataList = [graphSchemata, timelineSchemata];
  if (!isTopGraph) schemataList.add(rootGraphSchemata);

  for(Schemata a in schemataList) {
    if (a.classOf(SemLinkModel.graphRelationClass) != null) {
      if (a.classOf(SemLinkModel.graphRelationClass)!.subs.isNotEmpty) {
        graphSchemataList.add(a);
      }
    }
  }
  return graphSchemataList;
}

//arg1のidリスト
List<String> getSourceIdList(Entity e) {
  List<String> sourceComponentList = [];
  for (Item source in e.propertyModelsOf<Item>(SemLinkModel.arg1Property)) {
    sourceComponentList.add(source.id!);
  }
  return sourceComponentList;
}

//arg2のidリスト
String? getTargetId(Entity e) {
  Item? target = e.firstPropertyModelOf<Item>(SemLinkModel.arg2Property);
  return target?.id;
}


void logout(String msg) {
  String now =
      DateFormat('yyyy/MM/dd HH:mm:ss', "ja_JP").format(DateTime.now());
  print("${now} ${msg}");
}

void calcNodePosition(ComponentData componentData, Offset pos, BuildContext context) {
  calcNodeSize(componentData, context);
  double width = componentData.size.width;
  double height = componentData.size.height;
  componentData.position = Offset(pos.dx - width/2, pos.dy - height/2);
}

/// ノードのサイズ計算
ComponentSize calcNodeSize(ComponentData componentData, BuildContext context) {
  MyComponentData myData = componentData.data as MyComponentData;
  String dispClassText = myData.dispClass!;
  String dispCntSize = myData.dispCnt!;
  double w = 0;
  double h = 0;
  ComponentSize cs = ComponentSize(context, dispClassText, dispCntSize);

  // Classの幅を基準にする。
  w = cs.classSize.width;

  for(DisplayCnt dispCnt in myData.dispCntList!){
    ComponentSize dispCntCS = ComponentSize(context, dispCnt.title, dispCnt.detail);

    dispCnt.width = dispCntCS.size.width;
    dispCnt.height = dispCntCS.size.height;

    // ノードサイズのBorder補正
    double dispCntW = dispCntCS.size.width;
    w = max(dispCntW,w);

    if (dispCnt.image != null) {
      w = max(MyComponentData.defaultNodeWidth,w);
    }
    //double dispCntH = dispCntCS.size.height;
    //h += dispCntH;
  }

  /// Class再計算
  ComponentSize csize = ComponentSize(context, dispClassText, dispCntSize, width: w);
  h = csize.classSize.height;

  if (dispClassText.isNotEmpty) {
    h += ComponentSize.componentPadding * 2;
  }

  /// Title・Detail再計算
  for(DisplayCnt dispCnt in myData.dispCntList!) {
    ComponentSize dispCntCS = ComponentSize(context, dispCnt.title, dispCnt.detail, width: w);
    dispCnt.width = dispCntCS.size.width;
    dispCnt.height = dispCntCS.size.height;
    if(dispCnt.title.isNotEmpty && dispCnt.detail.isNotEmpty) {
      // titleとdetail間のボーダー高さの拡大
      dispCnt.height += myData.secondBorderWidth;
      dispCnt.height += myData.borderWidth;
    }

    if (dispCnt.title.isNotEmpty || dispCnt.detail.isNotEmpty) {
      dispCnt.height += ComponentSize.componentPadding * 2;
      h += dispCnt.height;
    }
    // 中身のボーダー高さの拡大
    h += myData.secondBorderWidth * 2;


    // アイテムにイメージがある場合はイメージ分の高さを拡大
    if (dispCnt.image != null) {
      var image = decodeImage(dispCnt.image!)!;
      // イメージの高さを取得
      double imageHeight = image.height.toDouble();
      h += imageHeight;
      h += ComponentSize.componentPadding * 2;
    }
  }

  // ボーダー幅分の拡大（普通のノードの場合は1.0*2、ハイパーノードの場合は5.0*2）
  // ボーダー高さ分の拡大（1.0*2）
  w = w + (myData.borderWidth * 2);
  h = h + (myData.borderWidth * 2);

  // 内側のボーダー幅分の拡大
  w += (myData.secondBorderWidth * 2);
  w += (ComponentSize.componentPadding * 2);

  cs.size = Size(w, h);
  componentData.setSize(cs.size);
  myData.dispClassSize = csize.classSize;
  myData.dispCntSize = csize.cntSize;

  //print("recalc size:${Size(w, h)}  class:${cs.classSize}  cnt:${cs.cntSize}");

  return cs;
}

//リンクラベルのサイズ計算
void calcLinkLabelSize(ComponentData linkLabel, BuildContext context) {
  MyComponentData myData = linkLabel.data as MyComponentData;
  List<LinkTypeProperty>? typeList = myData.typeList;
  ComponentSize cs = ComponentSize(context, "", "", isLinkLabel: true, typeList: typeList);
  linkLabel.size = cs.size ;
  myData.dispClassSize = cs.classSize;
  myData.typeCntSize = cs.cntSize;
}

//AIによるノード生成
Future<bool> createAI(String targetComponentId, int nodeSize, Graph graph, List<Schemata> schemataList, String plrId, Schemata timelineSchemata, Timeline timeline, bool isNodeEncrypt, CanvasReader canvasReader) async {
  Map<String, dynamic> graphMap = {};
  List<dynamic> nodeList = [];
  List<dynamic> linkList = [];
  Map<String, int> visitedComponentIdList = {};
  Map<String, bool> visitedLabelIdList = {};
  try {
    ComponentData componentData = canvasReader.model.getComponent(targetComponentId);

    //　n段階までたどってnodeListとlinkListに入れるidを探索
    void idSearch(String componentId, int n) {
      if (visitedComponentIdList.containsKey(componentId) && visitedComponentIdList[componentId]! >= n) return;
      visitedComponentIdList[componentId] = n;
      if (0 < n) {
        for (Connection link1 in componentData.connections) {
          String labelId = link1.otherComponentId;
          visitedLabelIdList[labelId] = true;
          ComponentData linkLabel = canvasReader.model.getComponent(labelId);
          for (Connection link2 in linkLabel.connections) {
            idSearch(link2.otherComponentId, n - 1);
          }
        }
      }
    }

    idSearch(targetComponentId, 2);

    //リクエストbody作成
    for (String componentId in visitedComponentIdList.keys) {
      ComponentData componentData = canvasReader.model.getComponent(componentId);
      GraphElement element = getElement(graph, (componentData.data as MyComponentData).nodeId!)!;
      RecordEntity? temp;
      SemNodeModel model = SemNodeModel(plrId);
      for (var schemata in schemataList) {
        temp ??= RecordEntity.from(schemata, element);
      }
      model.recordEntity = temp;
      //ノードのコメントが?か？の場合は空文字にする
      if (model.dispCntList.first.detail == "?" || model.dispCntList.first.detail == "？") model.dispCntList.first.detail = "";
      Map map = {};
      map.addEntries([
        MapEntry("node", "#${element.id}"),
        MapEntry("content", "${model.dispCntList.first.detail}"),
      ]);
      nodeList.add(map);
    }

    for (String  labelId in visitedLabelIdList.keys) {
      ComponentData linkLabel = canvasReader.model.getComponent(labelId);
      MyComponentData myLinkLabel = (linkLabel.data as MyComponentData);
      for (LinkTypeProperty linkType in myLinkLabel.typeList!) {
        GraphElement element = getElement(graph, linkType.elementId)!;
        SchemaClass? schemaClass;
        List<Item> sourceItemList = element.propertyModelsOf<Item>(SemLinkModel.arg1Property).toList();
        List<Item> targetItemList = element.propertyModelsOf<Item>(SemLinkModel.arg2Property).toList();

        for (Schemata schemata in schemataList) {
          schemaClass ??= schemata.classOf(element.type)!;
        }
        Map map = {};
        if (linkType.isSymmetric!) {
          List<String> sourceIdList = [];
          for (Item sourceItem in sourceItemList) {
            sourceIdList.add("#${sourceItem.id}");
          }
          map.addEntries([
            MapEntry("sourceNodes", sourceIdList),
            MapEntry("label", "${schemaClass!.id}"),
          ]);
        } else {
          map.addEntries([
            MapEntry("sourceNode", "#${sourceItemList.first.id}"),
            MapEntry("targetNode", "#${targetItemList.first.id}"),
            MapEntry("label", "${schemaClass!.id}"),
          ]);
        }
        linkList.add(map);
      }
      Map map = {};
      map.addEntries([
        MapEntry("nodeList", nodeList),
        MapEntry("linkList", linkList)
      ]);
      graphMap.addEntries([
        MapEntry("currentGraph", map),
        MapEntry("targetNodeId", "#${(componentData.data as MyComponentData).itemEntity!.id}"),
        MapEntry("nodeSize", nodeSize),
      ]);
    }

    //レスポンスからノードとリンクを作成
    SemNodeModel model = SemNodeModel(plrId);
    List<String> messageList = await chatAPI(graphMap);
    if (messageList.isEmpty) return false;
    List indexList = List.generate(messageList.length, (i) => i);
    await Future.wait(indexList.map((i) async {
      RecordEntity? recordEntity = await RecordEntity.createMessage(timelineSchemata, messageList[i]);
      recordEntity!.creator = creatorGPT4;
      //ノードをまわりに作成
      await recordEntity.save(timeline, false, onSyncFinished: (value) async {
        model.posOffset = centerPosition(componentData.position, componentData.size) + Offset(150 * cos(i * 2 * pi / messageList.length), 150 * sin(i * 2 * pi / messageList.length));
        addPosition(model, isNodeEncrypt, graph, recordEntity: recordEntity, OccurrenceType.created);
      });
      //リンク作成
      for (Connection link1 in componentData.connections) {
        String labelId = link1.otherComponentId;
        ComponentData linkLabel = canvasReader.model.getComponent(labelId);
        MyComponentData myLinkLabel = (linkLabel.data as MyComponentData);
        for (LinkTypeProperty linkType in myLinkLabel.typeList!) {
          String endLabel = linkType.linkEntity!.type!;
          bool isSymmetric = linkType.isSymmetric!;
          Entity? linkEntity;
          for (Connection link2 in linkLabel.connections) {
            String  otherComponentId = link2.otherComponentId;
            if (targetComponentId != otherComponentId) {
              RecordEntity AINode = recordEntity;
              ComponentData otherComponentData = canvasReader.model.getComponent(otherComponentId);
              RecordEntity otherRecordEntity = (otherComponentData.data as MyComponentData).itemEntity!;
              RecordEntity sourceNode = otherRecordEntity;
              RecordEntity targetNode = AINode;

              String? targetNodeId = linkType.targetNodeId;
              if (targetNodeId != null && (otherComponentData.data as MyComponentData).nodeId == targetNodeId) {
                sourceNode = AINode;
                targetNode = otherRecordEntity;
              }

              if (linkEntity == null) {
                linkEntity = await addLink(graph, sourceNode, targetNode, endLabel, isNodeEncrypt, recordEntity.creator!, timeline, isSymmetric, OccurrenceType.created);
              } else {
                branchLink(linkEntity, otherRecordEntity);
              }
            }
          }
        }
      }
    }));
    return true;
  } catch(e) {
    return false;
  }
}
