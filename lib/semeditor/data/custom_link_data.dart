import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

class MyLinkData {
  String startLabel;
  String endLabel;
  bool? isSymmetric;

  Entity? linkEntity;
  bool isHighlightVisible = false;
  bool isCutLink = false;
  Offset tapPosition = Offset.zero;

  GlobalKey key = GlobalKey();
  MyLinkData({
    this.startLabel = '',
    this.endLabel = '',
    this.isSymmetric,
    this.linkEntity,
  });

  MyLinkData.copy(MyLinkData customData)
      : this(
          startLabel: customData.startLabel,
          endLabel: customData.endLabel,
          isSymmetric: customData.isSymmetric,
        );
}
