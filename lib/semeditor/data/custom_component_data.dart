import 'dart:typed_data' show Uint8List;

import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

import '../model/sem_node_model.dart';
import '../../util/item_color.dart';
import '../policy/custom_policy.dart';

class MyComponentData {
  // パッケージデフォルト
  Color color;
  Color borderColor;
  double borderWidth;

  double secondBorderWidth;

  String text;
  Alignment textAlignment;
  double? textSize;

  bool isHighlightVisible = false;

  // semEditor

  /// アイテム
  RecordEntity? itemEntity;

  /// アイテムのid
  String? nodeId;

  /// 所有者が自分かどうか
  bool isMe;

  /// クラス名の表示値
  String? dispClass;
  /// クラス名のサイズ
  Size? dispClassSize;

  //TODO
  // 本来はdispCntListをすべて出力しないといけないが、
  // 現状のセマンティックエディタの表示は、初めの項目のみ表示する
  /// 内容表示リスト
  List<DisplayCnt>? dispCntList;

  /// 内容リストの初めの項目の表示内容
  String? dispCnt;
  /// 内容リストの初めの項目のイメージ
  Uint8List? dispCntImage;
  /// 内容のサイズ
  Size? dispCntSize;

  ///　複数種別のリスト
  List<LinkTypeProperty>? typeList;

  Size? typeCntSize;

  /// 編集中フラグ
  bool isEditing = false;

  /// タップ位置
  Offset? tapPosition;

  ///　リンクラベルかどうか
  bool isLinkLabel;

  ///　切り取られるリンクラベルかどうか
  bool isCutLinkLabel = false;

  bool isCutNode = false;

  OnLinkTapUp? onLinkTapUpHandler;
  OnLinkLongPressStart? onLinkLongPressStartHandler;

  // ノードの定数
  ///　ノードの標準幅
  static const double defaultNodeWidth = 140.0;
  ///ノードの標準縦横比
  static const double aspectRatio = 16 / 9;
  /// ノードの枠の幅（標準）
  static const double defaultNodeBorder = 1.0;
  /// ノードの枠の幅（ハイパーノード）
  static const double hyperNodeBorder = 5.0;
  /// プロフィールアイコンの幅・高さ
  static const double profileIconSize = 30.0;
  /// アイテム画像の幅・高さ
  static const double itemImageSize = 120.0;
  /// アイテム画像の枠の幅・高さ
  static const double itemImageBorder = 1.0;
  /// ノードの内側の枠の幅
  static const double secondNodeBorder = 0.3;

  static const double highlightPosition = 2;

  MyComponentData({
    this.color = Colors.white,
    this.borderColor = Colors.black,
    this.borderWidth = defaultNodeBorder,
    this.secondBorderWidth = secondNodeBorder,
    this.text = '', // 未使用
    this.textAlignment = Alignment.center,
    this.textSize,
    this.itemEntity,
    this.isMe = false,
    this.dispClass,
    this.dispCntList,
    this.typeList,
    this.tapPosition,
    this.isLinkLabel = false,
    this.onLinkTapUpHandler,
    this.onLinkLongPressStartHandler,
  });

  void initialize() {
    // nodeId
    if (itemEntity != null) {
      this.nodeId = itemEntity!.entity?.id;
    }

    this.color = ItemColor.contentColor(isCreator: this.isMe);

    // 簡易表示
    if (dispCntList?.isNotEmpty == true) {
      dispCnt = 
          (dispCntList!.first.title.isEmpty ? "" : dispCntList!.first.title) +
            (dispCntList!.first.detail.isEmpty ? "" :
              ((dispCntList!.first.title.isEmpty ? "" : '\n') + dispCntList!.first.detail));
      dispCntImage = dispCntList!.first.image;
    } else {
      dispCnt = "";
      dispCntImage = null;
    }
  }

  MyComponentData.copy(MyComponentData customData)
      : this(
          color: customData.color,
          borderColor: customData.borderColor,
          borderWidth: customData.borderWidth,
          secondBorderWidth: customData.secondBorderWidth,
          text: customData.text,
          textAlignment: customData.textAlignment,
          textSize: customData.textSize,
        );

  switchHighlight() {
    isHighlightVisible = !isHighlightVisible;
  }

  showHighlight() {
    isHighlightVisible = true;
  }

  hideHighlight() {
    isHighlightVisible = false;
  }
}

class LinkTypeProperty {
  String elementId;
  //種別名
  String? type;
  bool? isSymmetric;
  Entity? linkEntity;
  //矢印の角度
  double? angle;
  //接続先(arg2)のnodeId
  String? targetNodeId;
  String? tooltip;
  String? subject;
  String? object;
  bool isWriting = false;
  int syncingCount = 0;

  LinkTypeProperty (
      this.elementId,
      {
        this.type,
        this.isSymmetric,
        this.linkEntity,
        this.angle,
        this.targetNodeId,
        this.tooltip,
        this.subject,
        this.object,
  });
}
