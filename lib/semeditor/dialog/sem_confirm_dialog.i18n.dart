import 'package:i18n_extension/i18n_extension.dart';
import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      meTranslation +
      {
        'en_us': 'Confirm',
        'ja_jp': '確認',
      } +
      {
        'en_us': 'Are you sure to delete this node?',
        'ja_jp': 'このノードを削除してよろしいですか?',
      } +
      {
        'en_us': 'Are you sure to delete this link?',
        'ja_jp': 'このリンクを削除してよろしいですか?',
      };

  String get i18n => localize(this, t);
}
