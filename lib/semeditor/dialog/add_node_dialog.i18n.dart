import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      {
        'en_us': 'Input message',
        'ja_jp': 'メッセージを入力してください',
      } +
      {
        'en_us': 'Other type',
        'ja_jp': '他の種別',
      } +
      {
        'en_us': 'node share',
        'ja_jp': '共有',
      } +
      {
        'en_us': 'node duplicate',
        'ja_jp': '複製',
      };

      String get i18n => localize(this, t);
}
