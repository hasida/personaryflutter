import 'package:flutter/material.dart';

import '../dialog/add_node_dialog.i18n.dart';
import '../widget/option_icon.dart';

/// ノード追加用ダイアログ
/// dialog.dartのshowTextFieldDialogを改修
Future<AddNodeType?> showAddNodeDialog(
  BuildContext context,
  String title,
  bool isShowOtherType, {
  String? labelText,
  String? hintText,
  String? initialText,
  bool initialSelectAll = false,
  bool allowEmpytText = false,
  bool autofocus = true,
  bool showCancelButton = true,
  bool showShareButton = false,
  bool showDuplicateButton = false,
  String? okButtonLabel,
  String? cancelButtonLabel,
}) {
  var localizations = MaterialLocalizations.of(context);

  title = "Input message".i18n;

  var controller = TextEditingController();
  if (initialText != null) {
    controller.text = initialText;
    if (initialSelectAll)
      controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: initialText.length,
      );
  }

  var decoration = const InputDecoration(
    //labelText: labelText,
    //hintText: hintText,
    border: const OutlineInputBorder(),
    focusedBorder: const OutlineInputBorder(),
  );

  bool isValid() {
    if (allowEmpytText) return true;
    return controller.text.isNotEmpty;
  }

  //print("isShowOtherType: ${isShowOtherType}");

  var focusNode = FocusNode();

  return showDialog(
    context: context,
    builder: (context) => StatefulBuilder(
      builder: (context, setState) => AlertDialog(
        title: Text(title),
        content: TextField(
          controller: controller,
          autofocus: autofocus,
          focusNode: focusNode,
          decoration: decoration,
          maxLines: null,
          onChanged: (_) => setState(() {}),
          onSubmitted: (_) {
            if (isValid())
              Navigator.pop(context, AddNodeType(ButtonType.MESSAGE, controller.text));
            else
              focusNode.requestFocus();
          },
        ),
        actions: [
          if (showShareButton)
            OptionIcon(
              color: Colors.grey.withOpacity(0.7),
              iconData: Icons.share,
              tooltip: 'node share'.i18n,
              size: 40,
              onPressed: () {
                Navigator.pop(context, AddNodeType(ButtonType.SHARE, ""));
              },
            ),
          if (showDuplicateButton)
            OptionIcon(
              color: Colors.grey.withOpacity(0.7),
              iconData: Icons.paste,
              tooltip: 'node duplicate'.i18n,
              size: 40,
              onPressed: () {
                Navigator.pop(context, AddNodeType(ButtonType.DUP, ""));
              },
            ),
          if (isShowOtherType)
            TextButton(
              child: Text("Other type".i18n),
              onPressed: () => Navigator.pop(context, AddNodeType(ButtonType.OTHER, "")),
            ),
          if (showCancelButton)
            TextButton(
              child: Text(cancelButtonLabel ?? localizations.cancelButtonLabel),
              onPressed: () => Navigator.pop(context),
            ),
          TextButton(
              child: Text(okButtonLabel ?? localizations.okButtonLabel),
              onPressed: isValid()
                  ?
                  //() => Navigator.pop(context, controller.text) : null,
                  () =>
                      Navigator.pop(context, AddNodeType(ButtonType.MESSAGE, controller.text))
                  : null),
        ],
      ),
    ),
  );
}

enum ButtonType {
  DUP,
  SHARE,
  MESSAGE,
  OTHER
}

class AddNodeType {
  final ButtonType mType;
  final String message;

  AddNodeType(this.mType, this.message);
}
