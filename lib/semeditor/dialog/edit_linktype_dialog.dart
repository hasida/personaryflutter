import 'package:flutter/material.dart';
import 'package:personaryFlutter/semeditor/logic/sem_logic.dart';
import 'dart:convert' as convert;
import 'package:plr_ui/plr_ui.dart';
import 'package:plr_ui/src/entity_dialog/entity_dialog.i18n.dart';
import '../model/sem_link_model.dart';
import '../widget/option_icon.dart';

// 現在はプロパティ固定
const classIds = [
  "GraphRelation",
];

/// リンクのプロパティ編集用ダイアログ
Future<SchemaClass?> showEditLinkTypeDialog(
    BuildContext parentcbx,
    List<Schemata> schemataList,
    List<String>? historyList,
    String dispLabel,
    bool? isSymmetric,
    {
      OnPressDelete? onPressDelete,
      OnPressSwitch? onPressSwitch,
      OnPressConnect? onPressConnect,
    }
) async {
  List<SchemaClass> ClassList = [];
  for (Schemata schemata in schemataList) {
    ClassList.addAll(classIds.map(
          (i) => schemata.classOf(i),
    ).nonNulls.toList());
  }

  SchemaClass? resultClass;

  int initialLength = ClassList.length;
  while (initialLength <= ClassList.length) {
    resultClass = null;
    List<SchemaClass> rootClassList;
    List<String>? historyClassList;
  if (initialLength == ClassList.length) {
      rootClassList = ClassList;
      historyClassList = historyList;
    } else {
      rootClassList = [ClassList.last];
    }
    await showDialog(
      context: parentcbx,
      builder: (context) => EditLinkTypeDialog(
        context, schemataList, rootClassList, historyClassList, dispLabel, isSymmetric,
          onPressDelete: onPressDelete,
          onPressSwitch: onPressSwitch,
          onPressConnect: onPressConnect,
      ),
    ).then((Class) {
        if (Class != null) {
          resultClass = Class;
          // [>]で次の画面へ
          if (!resultClass!.isInputtable) {
            ClassList.add(Class); //次の画面へ
          } else {
            //アイテム選択
            ClassList.clear();
            return;
          }
        } else {
          ClassList.removeLast();
        }
    });
  }
  return resultClass;
}

/// リンクのプロパティ編集用ダイアログ本体
/// 子が11個以上だと子ダイアログを出すようにしている。
// プロパティが入力可能(inputtable)だと
// ・その子はないものと仮定している。
// ・それ自身が選択可能だと仮定している。
// 逆に入力可能ではない場合は、子があると仮定している。

typedef OnPressDelete = void Function(BuildContext context);
typedef OnPressSwitch = void Function();
typedef OnPressConnect = void Function(BuildContext context);

const _margin = 0.0;
const _width = 95.0;
const _height = 20.0;
const _arrowwidth = 12.0; //">"の幅
const _itemHeight = 0.0;

class EditLinkTypeDialog extends StatefulWidget {
  final BuildContext parentcbx;
  final List<Schemata> schemataList;
  final List<SchemaClass> rootClassList;
  final List<String>? historyClassList;
  final String dispLabel;
  final bool? isSymmetric;
  final OnPressDelete? onPressDelete;
  final OnPressSwitch? onPressSwitch;
  final OnPressConnect? onPressConnect;

  // コンストラクタ
  EditLinkTypeDialog(this.parentcbx, this.schemataList, this.rootClassList, this.historyClassList, this.dispLabel, this.isSymmetric, {this.onPressDelete, this.onPressSwitch, this.onPressConnect});

  @override
  State<EditLinkTypeDialog> createState() => _EditLinkTypeDialogState();
}

class _EditLinkTypeDialogState extends State<EditLinkTypeDialog> {
    BuildContext get parentcbx => widget.parentcbx;
    List<Schemata> get schemataList => widget.schemataList;
    List<SchemaClass> get rootClassList => widget.rootClassList;
    List<String>? get historyClassList => widget.historyClassList;
    String get dispLabel => widget.dispLabel;
    bool? get isSymmetric => widget.isSymmetric;
    OnPressDelete? get onPressDelete => widget.onPressDelete;
    OnPressSwitch? get onPressSwitch => widget.onPressSwitch;
    OnPressConnect? get onPressConnect => widget.onPressConnect;

    late double textScale;
    late double margin;
    late double width;
    late double height;
    late double arrowwidth;
    late double itemHeight;

  @override
  Widget build(BuildContext context) {
    textScale = MediaQuery.textScalerOf(context).scale(1.0);
    margin = _margin * textScale;
    width = _width * textScale;
    height = _height * textScale;
    arrowwidth = _arrowwidth * textScale;
    itemHeight = _itemHeight;

    List<Widget> list = [];

    if (historyClassList != null) {
    list.add(createHistoryWidget(context));
    }

    List<String> classIdList = [];
    for (SchemaClass rootClass in rootClassList) {
      for (var rc in rootClass.subs) {
        if (!classIdList.contains(rc.id)) {
          Widget? cw = createEntityWidget(context, schemataList, rc);
          if (cw != null) list.add(cw);
          classIdList.add(rc.id);
        }
      }
    }
    return AlertDialog(
      contentPadding: const EdgeInsets.all(10),
      actionsPadding: const EdgeInsets.all(10),
      insetPadding: const EdgeInsets.all(10),
      title: Row(
        children: [const Icon(Icons.create), Text('Type :'.i18n)],
      ),
      content: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             SingleChildScrollView(
               scrollDirection: Axis.horizontal,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: list,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(left: 5.0, top: 2.5, right: 5.0),
                      child: TextButton(
                        child: Text('Cancel'.i18n),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: onPressDelete != null,
                    child:OptionIcon(
                        color: Colors.red.withOpacity(0.7),
                        iconData: Icons.delete_forever,
                        tooltip: 'delete'.i18n,
                        onPressed:() {
                          if(onPressDelete != null) {
                            onPressDelete!(context);
                          }
                        }),
                  ),
                  Visibility(
                    visible: onPressSwitch != null,
                    child: OptionIcon(
                      color: Colors.grey.withOpacity(0.7),
                      iconData: Icons.change_circle_outlined,
                      tooltip: 'switch'.i18n,
                      onPressed: () async {
                        if(onPressSwitch != null) onPressSwitch!();
                        Navigator.pop(context);
                      }
                    )
                  ),
                  Visibility(
                      visible: onPressConnect != null,
                      child: OptionIcon(
                          color: Colors.grey.withOpacity(0.7),
                          iconData: Icons.arrow_right_alt,
                          tooltip: 'connect'.i18n,
                          onPressed: () async {
                            if(onPressConnect != null) onPressConnect!(context);
                            Navigator.pop(context);
                          },
                      )
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget createHistoryWidget(BuildContext context) {
    List<DropdownMenuItem<SchemaClass>> list = [];
    List<SchemaClass>? classList = getLinkList(schemataList);
    for (String historyClass in historyClassList!) {
      var map = convert.json.decode(historyClass);
      SchemaClass? schemaClass = getMatchLabel(classList, map["id"]);
      DropdownMenuItem<SchemaClass> history;
      //OSS設定されていない場合の処理
      if (schemaClass == null) {
        map.remove("id");
        history =
            DropdownMenuItem<SchemaClass>(
              child: Text(SchemaLiteral(map).defaultValue, style: const TextStyle(color: Colors.grey)),
              enabled : false,
            );
      //OSS設定されている場合の処理
      } else {
        Schemata? schemata;
        for (Schemata sc in schemataList) {
          if (sc.classOf(schemaClass.id) != null) {
            schemata = sc;
          }
        }

        bool isSameType = isSymmetric == null ? true : (isSymmetric == schemaClass.isDescendantOf(schemata!.classOf(SemLinkModel.symmetricRelationClass)));

        if (!isSameType) {
          history =
              DropdownMenuItem<SchemaClass>(
                child: Text(schemaClass.label?.defaultValue, style: const TextStyle(color: Colors.grey)),
                enabled : false,
              );
        } else {
          history =
              DropdownMenuItem<SchemaClass>(
                child: Text(schemaClass.label?.defaultValue),
                value: schemaClass,
              );
        }
      }
      list.add(history);
    }

    return Container(
        width: width,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.white)
        ),
        child: DropdownButton<SchemaClass>(
          hint: Text('history'.i18n),
          items: list,
          onChanged: (SchemaClass? value) {
            Navigator.pop(context, value);
          },
        ));
  }

  Widget? createEntityWidget(BuildContext context,List<Schemata> schemataList, SchemaClass type) {
    if (type.isInputtable) {
      late Schemata schemata;
      for (Schemata sc in schemataList) {
        if (sc.classOf(type.id) != null) {
          schemata = sc;
        }
      }

      bool isSameType = isSymmetric == null ? true : (isSymmetric == type.isDescendantOf(schemata.classOf(SemLinkModel.symmetricRelationClass)));

      return Container(
        alignment: Alignment.topLeft,
        child: createChoice(context, type, false, isSameType: isSameType),
      );
    } else if (type.subs.length > 10) {
      return Container(
        alignment: Alignment.topLeft,
        child: createChoice(context, type, true),
      );
    } else {
      if (type.subs.length == 0) return null;

      List<Widget> list = [];
      var temporaryHeight = 0.0;

      if (itemHeight > 0.0) {
        temporaryHeight = itemHeight;
        itemHeight = 0.0;
      }

      for (var p in type.subs) {
        Widget? cw = createEntityWidget(context, schemataList, p);
        if ( cw != null) list.add(cw);
      }
      Widget widget = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          createChoice(context, type, false, totalHeight: itemHeight),
          //createChoice(context, type),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: list,
          ),
        ],
      );
      if (temporaryHeight > 0.0) {
        itemHeight += temporaryHeight;
      }
      return widget;
    }
  }

  Widget createChoice(BuildContext context, SchemaClass type, bool nextFlg,
      {bool isSameType = true, double totalHeight = 0.0}) {
    bool isSelected = false;

    double dispHeight = height;
    double dispWidth = width;
    if (!nextFlg) {
      dispWidth = width + arrowwidth;
    }

    /// 表示文字列のwidthとheight取得
    TextSpan ts = new TextSpan(
        // ignore: null_aware_before_operator
        text: (type.label?.defaultValue),
        style: Theme.of(context).textTheme.bodyLarge);
    TextPainter tp =
        new TextPainter(text: ts, textDirection: TextDirection.ltr);
    tp.layout();
    var textWidth = tp.width * textScale;
    var textHeight = tp.height * textScale;
    dispHeight = textHeight + (margin * 2);

    /// 改行対応
    var widthPerChar = (textWidth / (ts.text!.length)).ceil();
    var widthPerLine = (dispWidth - (margin * 2) - 1);
    var stringPerLine = (widthPerLine / widthPerChar).floor();
    if ((ts.text!.length / stringPerLine).ceil() > 1) {
      dispHeight = dispHeight * (ts.text!.length / stringPerLine).ceil();
    }

    if (totalHeight != 0.0) {
      /// totalHeight設定あり(=親項目)
      dispHeight = totalHeight;

      /// さらなる親項目に備えて項目高さと上下マージン分で_totalHeightを更新
      //itemHeight = dispHeight + (margin * 2);
      itemHeight = dispHeight + (margin);
    } else {
      /// 親項目に備え項目高さと上下マージン分で_totalHeightに合算
      itemHeight += dispHeight + (margin * 2);
    }

    bool isInputable = type.isInputtable;
    isSelected = isInputable &&
        dispLabel != "" &&
        dispLabel == (type.label?.defaultValue ?? "");

    return InkWell(
      child: MouseRegion(
        cursor:
        (isSameType && (isInputable || nextFlg)) ? SystemMouseCursors.click : SystemMouseCursors.basic,
        child: Container(
          margin: EdgeInsets.all(margin),
          height: dispHeight,
          decoration: BoxDecoration(
            border: Border.all(
              color: (isSameType && (isInputable || nextFlg)) ? Colors.black : Colors.grey,
              width: 0.5,
            ),
          ),
          child: SizedBox(
            height: dispHeight,
            width: dispWidth + (nextFlg ? arrowwidth : 0.0),
            child: Container(
              color: ((isSelected) ? Colors.orange : Colors.white),
              child: Row(
                children: [
                  Container(
                    child: Container(
                      alignment: Alignment.center,
                      width: dispWidth,
                      // color: ((isSelected) ? Colors.orange : Colors.white),
                      child: Text(
                        (type.label?.defaultValue ?? ""),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: (isSameType && (isInputable || nextFlg)) ? Colors.black : Colors.grey,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: nextFlg ? arrowwidth : 0.0,
                    color: Colors.white,
                    child: Text(
                      nextFlg ? ">" : "",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        height: 1.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      //onTap: () => onTapButton(type, isSelected),
      onTap: () async {
        if (!isSameType) return;
        if (isInputable) {
          //print("onTap: ${type.label?.defaultValue}");
          Navigator.pop(context, type);
        } else if (nextFlg) {
          Navigator.pop(context, type);
        }
      },
    );
  }
}


///// 以下は1回目修正ソース
/*
///表示調整用クラス
class DispPropety {
  DispPropety({this.hierarchy, this.parentid, this.clildCount, this.type});
  int hierarchy;
  String parentid;
  int clildCount;
  double hegiht;
  double textwidth;
  SchemaProperty type;
}

///表示調整用マップ
Map<String, DispPropety> dispMapProperty = new Map();
///ダイアログWidth(Max)
double dialogWidth;

/// リンクのプロパティ編集用ダイアログ
Future<SchemaProperty> showEditLinkTypeDialog(
  BuildContext context,
  Schemata schemata,
) {
  //List<SchemaProperty> propList;
  //double _totalHeight = 0.0;

  //propList = schemata.allProperties.toList();

  //List<Widget> list = [];
  SchemaProperty root = schemata.propertyOf("graphRelation");
  //list.add(createEntityWidget(context, root));

  // ダイアログの幅
  dialogWidth = MediaQuery.of(context).size.width * 0.8;
  
  // 表示調整用マップの初期化
  dispMapProperty.clear();
  // 表示調整用マップ作製、ルートをtempRootKeyという名前にしている
  createDispMap(root, 0, "tempRootKey");

  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      contentPadding: const EdgeInsets.all(10),
      actionsPadding: const EdgeInsets.all(10),
      insetPadding: const EdgeInsets.all(10),
      title: Row(
        children: [const Icon(Icons.create), Text('Type :'.i18n)],
      ),
      content: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  createEntityWidget(context, root),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(left: 5.0, top: 2.5, right: 5.0),
                      child: TextButton(
                        child: Text('Cancel'.i18n),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

//表示マップ作成
void createDispMap(SchemaProperty type, int hierarchy, String parentid) {
  hierarchy++;
  DispPropety c = new DispPropety(
      hierarchy: hierarchy, parentid: parentid, clildCount: 0, type: type);
  dispMapProperty[type.id] = c;

  String pid = parentid;

  if (type.isInputtable) {
    while (true) {
      if (!dispMapProperty.containsKey(pid)) {
        break;
      }
      dispMapProperty[pid].clildCount++;
      pid = dispMapProperty[pid].parentid;
    }
  }
  for (var p in type.subs) {
    createDispMap(p, hierarchy, type.id);
  }
}

//ダイアログアイテムWidget
Widget createEntityWidget(BuildContext context, SchemaProperty type) {
  double width = (MediaQuery.of(context).textScaleFactor) * 90.0;
  double minwidth = (MediaQuery.of(context).textScaleFactor) * 70.0;
  double height = (MediaQuery.of(context).textScaleFactor) * 30.0;
  double textHeight = 20.0;
  const double margin = 2.5;
  const double padding = 3.0;
  const double linewidth = 0.5;

  Map<int, double> widthMap = new Map();

  //階層ごとのアイテム数、必要Height、アイテムの幅の取得
  for (var c in dispMapProperty.values) {
    if (!widthMap.containsKey(c.hierarchy)) {
      widthMap[c.hierarchy] = 1;
    } else {
      widthMap[c.hierarchy]++;
    }
    //高さ計算
    c.hegiht = c.clildCount <= 1
        ? height
        : height * c.clildCount + margin * 2 * (c.clildCount - 1);

    /// 表示文字列のwidth取得
    TextSpan ts = new TextSpan(
        text: c.type.label?.defaultValue,
        style: Theme.of(context).textTheme.bodyLarge);
    TextPainter tp =
        new TextPainter(text: ts, textDirection: TextDirection.ltr);
    tp.layout();
    c.textwidth = (tp.width + (padding * 2) + (linewidth * 2)) *
        (MediaQuery.of(context).textScaleFactor);
    textHeight = tp.height * MediaQuery.of(context).textScaleFactor;
  }
  //１つしか存在しない階層は削除 (widthMapをテンポラリ使用)
  widthMap..removeWhere((int key, double value) => value < 2.0);

  //階層が多い時の対応
  if ((width * widthMap.length) + (margin * (widthMap.length - 1)) >
      dialogWidth) {
    width = dialogWidth / (widthMap.length) - margin;
    minwidth = (width - 5) > 0 ? width - 5 : 5;
  }

  //widthMapに各階層の最大幅の取得
  for (var c in dispMapProperty.values) {
    if (!widthMap.containsKey(c.hierarchy)) {
      continue;
    }
    if (c.textwidth > widthMap[c.hierarchy]) {
      widthMap[c.hierarchy] = c.textwidth;
    }
  }

  //各階層Max幅の合計
  double sumwidth = 0.0;
  var widthMap2 = {...widthMap};
  widthMap.forEach((key, value) {
    widthMap2[key] = width > value ? width : value;
    sumwidth += widthMap2[key];
    sumwidth += margin;
  });

  //各階層の合計幅がダイアログ幅を超えていたら小さくできるものは小さくする
  if (sumwidth > dialogWidth) {
    widthMap.forEach((key, value) {
      if (value <= width) {
        widthMap2[key] = value < minwidth ? minwidth : value;
        sumwidth -= (width - widthMap2[key]);
      }
    });
  }

  //それでも超えている場合、全部デフォルト幅（Width）にして改行する
  if (sumwidth > dialogWidth) {
    for (int key in widthMap.keys) {
      double value = widthMap[key];
      widthMap2[key] = width;
      if (value > width) {
        sumwidth -= value - width;
        for (var c in dispMapProperty.values) {
          if (key != c.hierarchy) {
            continue;
          }
          if (c.textwidth > width) {
            int rownow = c.clildCount < 1 ? 1 : c.clildCount;
            int row = (c.textwidth / width).ceil();
            if (row > (rownow)) {
              c.hegiht += textHeight * (row - rownow);
              //遡って親の高さを調整
              String pid = c.parentid;
              while (true) {
                if (!dispMapProperty.containsKey(pid)) {
                  break;
                }
                dispMapProperty[pid].hegiht += textHeight * (row - rownow);
                pid = dispMapProperty[pid].parentid;
              }
            }
          }
        } //for (var c in dispMapProperty.values)
        if (sumwidth <= dialogWidth) {
          break;
        }
      }
    }
  }
  List<Widget> columnList = [];
  widthMap2.forEach((var hierarchy, var rowwidth) {
    List<Widget> celList = [];
    dispMapProperty.forEach((var colkey, var colvalue) {
      if (colvalue.hierarchy == hierarchy) {
        Widget colwidget = InkWell(
          child: MouseRegion(
              cursor: colvalue.type.isInputtable
                  ? SystemMouseCursors.click
                  : SystemMouseCursors.basic,
              child: Container(
                margin: EdgeInsets.all(margin),
                padding: EdgeInsets.all(padding),
                decoration: BoxDecoration(
                  border: Border.all(
                    color:
                        colvalue.type.isInputtable ? Colors.black : Colors.grey,
                    width: linewidth,
                  ),
                ),
                height: colvalue.hegiht,
                width: rowwidth,
                child: Text(
                  colvalue.type.label?.defaultValue ?? "",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge.merge(TextStyle(
                        color: colvalue.type.isInputtable
                            ? Colors.black
                            : Colors.grey,
                      )),
                ),
              ),
          ),
            /*
            child: Container(
              margin: const EdgeInsets.all(margin),
              padding: const const EdgeInsets.all(padding),
              decoration: BoxDecoration(
                border: Border.all(
                  color:
                      colvalue.type.isInputtable ? Colors.black : Colors.grey,
                  width: linewidth,
                ),
              ),
              height: colvalue.hegiht,
              width: rowwidth,
              child: Text(
                colvalue.type.label?.defaultValue ?? "",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyLarge.merge(TextStyle(
                      color: colvalue.type.isInputtable
                          ? Colors.black
                          : Colors.grey,
                    )),
              ),
            ),
            */
            onTap: () {
              if (colvalue.type.isInputtable) {
                print("onTap: ${type.label?.defaultValue}");
                Navigator.pop(context, colvalue.type);
              } else {
                null;
              }
            });
        celList.add(colwidget);
      }
    });

    Widget column = Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: celList,
    );
    columnList.add(column);
  });

  return Row(
      crossAxisAlignment: CrossAxisAlignment.start, children: columnList);
}
*/

///// 以下はオリジナルソース
/*
Widget createEntityWidget(BuildContext context, SchemaProperty type) {
  if (type.isInputtable) {
    return Container(
      alignment: Alignment.topLeft,
      child: createChoice(context, type),
    );
  } else {
    List<Widget> list = [];
    var temporaryHeight = 0.0;
    /*
      if (_totalHeight > 0.0) {
        temporaryHeight = _totalHeight;
        _totalHeight = 0.0;
      }
      */
    for (var p in type.subs) {
      list.add(createEntityWidget(context, p));
    }
    Widget widget = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //createChoice(context, type, totalHeight: _totalHeight),
        createChoice(context, type),
        Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: list,
        ),
      ],
    );
    /*
      if (temporaryHeight > 0.0) {
        _totalHeight += temporaryHeight;
      }
      */
    return widget;
  }
}

Widget createChoice(BuildContext context, SchemaProperty type,
    {double totalHeight = 0.0}) {
  bool isSelected = false;

  // /// ラベルがnullの場合は空コンテナを返す
  // if (type.label?.defaultValue == null) return Container();
  /*
    for (SchemaClass b in _selectedList) {
      if (type.id == b.id) {
        isSelected = true;
      }
    }
    */

  var margin = 2.5;
  double width = 80.0;
  double height = 20.0;

  /// 表示文字列のwidthとheight取得
  TextSpan ts = new TextSpan(
      text: type.label?.defaultValue,
      style: Theme.of(context).textTheme.bodyLarge);
  TextPainter tp = new TextPainter(text: ts, textDirection: TextDirection.ltr);
  tp.layout();
  var textWidth = tp.width * textScaleFactor;
  var textHeight = tp.height * textScaleFactor;
  height = textHeight + (margin * 2);

  /// 改行対応
  var widthPerChar = (textWidth / (ts.text.length)).ceil();
  var widthPerLine = (width - (margin * 2) - 1);
  var stringPerLine = (widthPerLine / widthPerChar).floor();
  if ((ts.text.length / stringPerLine).ceil() > 1) {
    height = height * (ts.text.length / stringPerLine).ceil();
  }

  //
  //  if (totalHeight != 0.0) {
  //    /// totalHeight設定あり(=親項目)
  //    height = totalHeight;
  //
  //    /// さらなる親項目に備えて項目高さと上下マージン分で_totalHeightを更新
  //    _totalHeight = height + (margin * 2);
  //  } else {
  //    /// 親項目に備え項目高さと上下マージン分で_totalHeightに合算
  //    _totalHeight += height + (margin * 2);
  //  }
  //

  bool isInputable = type.isInputtable;

  return InkWell(
    child: Container(
      width: width,
      margin: const EdgeInsets.all(margin),
      decoration: BoxDecoration(
        border: Border.all(
          color: isInputable ? Colors.black : Colors.grey,
          width: 0.5,
        ),
      ),
      child: Container(
        alignment: Alignment.center,
        height: height,
        //color: ((isSelected) ? Colors.orange : Colors.white),
        child: Text(
          type.label?.defaultValue ?? "",
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: isInputable ? Colors.black : Colors.grey,
          ),
        ),
      ),
    ),
    //onTap: () => onTapButton(type, isSelected),
    onTap: () {
      if (isInputable) {
        print("onTap: ${type.label?.defaultValue}");
        Navigator.pop(context, type);
      } else {
        null;
      }
    },
  );
}
*/

  /*
  void onTapButton(SchemaClass type, bool isSelected) {
    setState(() {
      if (_maxCardinality == 1) {
        /// 属性値が1の場合は即決定する
        if (_selectedList.length > 0) _selectedList.clear();
        _selectedList.add(type);
        _recordData.selectedList = _selectedList;
        Navigator.pop(context);
      } else if (isSelected) {
        _selectedList.remove(type);
      } else {
        _selectedList.add(type);
      }
    });
  }
  */

