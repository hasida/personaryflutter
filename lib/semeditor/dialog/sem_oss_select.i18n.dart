import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      {
        'en_us': 'Cancel',
        'ja_jp': 'キャンセル',
      } +
      {
        'en_us': 'OK',
        'ja_jp': 'OK',
      } +
      {
        'en_us': 'OSS setting',
        'ja_jp': 'OSS設定',
      } +
      {
        'en_us': 'Friends reference',
        'ja_jp': '友達参照',
      } +
      {
        'en_us': 'saving data',
        'ja_jp': 'データの保存中です',
      } +
      {
        'en_us': 'Ontology',
        'ja_jp': 'オントロジー',
      } +
      {
        'en_us': 'Restriction',
        'ja_jp': 'オントロジーの限定',
      } +
      {
        'en_us': 'Constraint',
        'ja_jp': '制約',
      } +
      {
        'en_us': 'Graph ontology',
        'ja_jp': 'グラフ用オントロジー',
      };

  String get i18n => localize(this, t);
}
