import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';

import '../../dialog/channel_edit/channel_edit_util.dart';

import 'sem_oss_select.i18n.dart';

typedef SemOssSelectDialogCallback = Function(
  Iterable<OssFile> ontologyFiles,
  Iterable<OssFile> restrictionFiles,
  Iterable<OssFile> constraintFiles,
  Iterable<OssFile> timelineSSFiles,
  Iterable<OssFile> groupSSFiles,
  Iterable<OssFile> channelSSFiles,
);

/// OSSセレクトダイアログ（セマンティックエディタ用）
/// OssSelectDialogを基に作成
/// セマンティックエディタでは、スタイルシートは扱わないので表示しない
/// 本来はスタイルシート関連は削除してもよいのだが、今後のために残しておく
/// 将来的にはグラフ用オントロジーの対応が考えられる
class SemOssSelectDialog extends StatefulWidget {
  final Root root;
  final Iterable<OssFile> ontologyFiles;
  final Iterable<OssFile> restrictionFiles;
  final Iterable<OssFile> constraintFiles;
  final Iterable<OssFile> timelineSSFiles;
  final Iterable<OssFile> groupSSFiles;
  final Iterable<OssFile> channelSSFiles;
  final Iterable<OssFile> rootontologyFiles;
  final Iterable<OssFile> rootrestrictionFiles;
  final Iterable<OssFile> rootconstraintFiles;
  final SemOssSelectDialogCallback onDone;
  final bool isReadOnly;
  final DataSetting? parent;
  final Schemata? schemata;
  final bool isParentChannel;

  SemOssSelectDialog({
    required this.root,
    required this.onDone,
    required this.ontologyFiles,
    required this.restrictionFiles,
    required this.constraintFiles,
    required this.timelineSSFiles,
    required this.groupSSFiles,
    required this.channelSSFiles,
    required this.rootontologyFiles,
    required this.rootrestrictionFiles,
    required this.rootconstraintFiles,
    this.isReadOnly = false,
    this.parent,
    this.schemata,
    this.isParentChannel = false,
    super.key,
  });

  SemOssSelectDialog.fromDummySchema({
    required Root root,
    required SemOssSelectDialogCallback onDone,
    required DummySchema schema,
    required DummySchema rootschema,
    bool isReadOnly = false,
    DataSetting? parent,
    Schemata? schemata,
    bool isParentChannel = false,
    Key? key,
  }): this(
    root: root,
    onDone: onDone,
    ontologyFiles: schema.ontologyFiles,
    restrictionFiles: schema.restrictionFiles,
    constraintFiles: schema.constraintFiles,
    timelineSSFiles: schema.timelineSSFiles,
    groupSSFiles: schema.groupSSFiles,
    channelSSFiles: schema.channelSSFiles,
    rootontologyFiles: rootschema.ontologyFiles,
    rootrestrictionFiles: rootschema.restrictionFiles,
    rootconstraintFiles: rootschema.constraintFiles,
    isReadOnly: isReadOnly,
    parent: parent,
    schemata: schemata,
    isParentChannel: isParentChannel,
    key: key,
  );

  State<StatefulWidget> createState() => _SemOssSelectDialogState();
}

enum _SemKind {
  ontology,
  restriction,
  constraint,
  timelineSS,
  channelSS,
  groupSS,
}

class _SemOssSelectItem {
  final OssFile ossFile;
  HasProfile? ownerModel;

  _SemOssSelectItem(this.ossFile, [ this.ownerModel ]);
}

class _SemOssSelectDialogState extends State<SemOssSelectDialog> {
  final Map<String, _SemOssSelectItem> ontologyFiles = {};
  final Map<String, _SemOssSelectItem> restrictionFiles = {};
  final Map<String, _SemOssSelectItem> constraintFiles = {};
  final Map<String, _SemOssSelectItem> timelineSSFiles = {};
  final Map<String, _SemOssSelectItem> groupSSFiles = {};
  final Map<String, _SemOssSelectItem> channelSSFiles = {};
  final Set<String> selectedOntology = {};
  final Set<String> selectedRestriction = {};
  final Set<String> selectedConstraint = {};
  final Set<String> selectedTimelineSS = {};
  final Set<String> selectedGroupSS = {};
  final Set<String> selectedChannelSS = {};

  // 親グラフで選択されたもの
  Set<String> parentOntologyFilesIds = {};
  Set<String> parentRestrictionFilesIds = {};
  Set<String> parentConstraintFilesIds = {};

  // 選択されたオントロジー、限定、制約 ソート後のリスト
  List<MapEntry<String, _SemOssSelectItem>> sortOntologyTmp = [];
  List<MapEntry<String, _SemOssSelectItem>> sortRestrictionTmp = [];
  List<MapEntry<String, _SemOssSelectItem>> sortConstraintTmp = [];

  @override
  void initState() {
    super.initState();
    var ossSelectItems = <_SemOssSelectItem>[];

    Iterable<String> mapOssIds(Iterable<OssFile> ossFiles) => ossFiles.map(
      (e) => e.id
    ).nonNulls;

    Iterable<MapEntry<String, _SemOssSelectItem>> mapOssFiles(
      Iterable<OssFile> ossFiles,
    ) => ossFiles.map((e) {
        var id = e.id;
        if (id == null) return null;

        _SemOssSelectItem os = _SemOssSelectItem(e);
        if (!id.startsWith(systemAccountEmail)) {
          ossSelectItems.add(os);
        }
        return MapEntry(id, os);
      }
    ).nonNulls;

    selectedOntology.addAll(mapOssIds(widget.ontologyFiles));
    ontologyFiles.addEntries(mapOssFiles(widget.ontologyFiles));

    selectedRestriction.addAll(mapOssIds(widget.restrictionFiles));
    restrictionFiles.addEntries(mapOssFiles(widget.restrictionFiles));

    selectedConstraint.addAll(mapOssIds(widget.constraintFiles));
    constraintFiles.addEntries(mapOssFiles(widget.constraintFiles));

    selectedTimelineSS.addAll(mapOssIds(widget.timelineSSFiles));
    timelineSSFiles.addEntries(mapOssFiles(widget.timelineSSFiles));

    selectedGroupSS.addAll(mapOssIds(widget.groupSSFiles));
    groupSSFiles.addEntries(mapOssFiles(widget.groupSSFiles));

    selectedChannelSS.addAll(mapOssIds(widget.channelSSFiles));
    channelSSFiles.addEntries(mapOssFiles(widget.channelSSFiles));

    // 親の設定を取得
    // 親がチャネルの場合dataSettingがない場合もある
    if (widget.parent != null) {
      parentOntologyFilesIds.addAll(mapOssIds(widget.parent!.ontologyFiles));
      parentRestrictionFilesIds.addAll(
        mapOssIds(widget.parent!.restrictionFiles),
      );
      parentConstraintFilesIds.addAll(
        mapOssIds(widget.parent!.constraintFiles),
      );
    }

    if (widget.isParentChannel==false) {
      parentOntologyFilesIds.addAll(mapOssIds(widget.rootontologyFiles));
      parentRestrictionFilesIds.addAll(mapOssIds(widget.rootrestrictionFiles));
      parentConstraintFilesIds.addAll(mapOssIds(widget.rootconstraintFiles));
    }

    // 選択したスキーマの設定を取得
    if (widget.schemata != null) {
      for (var ontology in widget.schemata!.ontologies) {
        if (ontology.ossId != null) {
          parentOntologyFilesIds.add(ontology.ossId!);
        }
      }
      for (var restriction in widget.schemata!.restrictionRoots) {
        if (restriction.ossId != null) {
          parentRestrictionFilesIds.add(restriction.ossId!);
        }
      }
      for (var constraint in widget.schemata!.constraintRoots) {
        if (constraint.ossId != null) {
          parentConstraintFilesIds.add(constraint.ossId!);
        }
      }
    }

    _loadOss();

    widget.root.friends.listen((event) async {
      final friendList = await event.value.toList();
      await Future.wait(friendList.map((f) => f.syncSilently()));
      for (var oss in ossSelectItems) {
        for (var f in friendList) {
          if (oss.ossFile.id!.startsWith(f.plrId.email)) {
            oss.ownerModel = f;
          }
        }
      }
      setState(() {});
    });
  }

  void _loadOss() async {
    var publicRoot = (await systemAccount).publicRoot;
    _loadOssI(publicRoot, null);

    publicRoot = await publicRootOf(widget.root.storage);
    _loadOssI(publicRoot, widget.root);
  }

  void _loadOssI(PublicRoot publicRoot, HasProfile? entity) async {
    await publicRoot.syncSilently();
    setState(() {
      var oss = publicRoot.oss;
      if (oss == null) return;

      Iterable<MapEntry<String, _SemOssSelectItem>> mapOssFiles(
        Iterable<OssFile> ossFiles,
      ) => ossFiles.map((e) {
          var id = e.id;
          if (id == null) return null;

          return MapEntry(id, _SemOssSelectItem(e, entity));
        },
      ).nonNulls;

      ontologyFiles.addEntries(mapOssFiles(oss.ontologyFiles));
      restrictionFiles.addEntries(mapOssFiles(oss.restrictionFiles));
      constraintFiles.addEntries(mapOssFiles(oss.constraintFiles));
      timelineSSFiles.addEntries(mapOssFiles(oss.timelineSSFiles));
      groupSSFiles.addEntries(mapOssFiles(oss.groupSSFiles));
      channelSSFiles.addEntries(mapOssFiles(oss.channelSSFiles));
    });

    // "基本"削除後にソートを行う
    ontologyFiles.remove(SystemAccount.baseOntologyOssId.toString());
    List<MapEntry<String, _SemOssSelectItem>> ontologyTmp = ontologyFiles.entries.toList();
    sortOntologyTmp = itemSort(ontologyTmp, _SemKind.ontology);

    restrictionFiles.remove(SystemAccount.baseRestrictionOssId.toString());
    List<MapEntry<String, _SemOssSelectItem>> restrictionTmp = restrictionFiles.entries.toList();
    sortRestrictionTmp = itemSort(restrictionTmp, _SemKind.restriction);

    List<MapEntry<String, _SemOssSelectItem>> constraintTmp = constraintFiles.entries.toList();
    sortConstraintTmp = itemSort(constraintTmp, _SemKind.constraint);
  }

  @override
  Widget build(BuildContext context) {
    var content = <Widget>[];

    content.add(_createTitleCell('Ontology'.i18n));
    for (var e in sortOntologyTmp) {
      content.add(_createCell(_SemKind.ontology, e.value));
    }

    content.add(_createTitleCell('Restriction'.i18n));
    for (var e in sortRestrictionTmp) {
      content.add(_createCell(_SemKind.restriction, e.value));
    }

    content.add(_createTitleCell('Constraint'.i18n));
    for (var e in sortConstraintTmp) {
      content.add(_createCell(_SemKind.constraint, e.value));
    }

    // セマンティックエディタでは、スタイルシートは扱わないので表示しない
    // content.add(_createTitleCell('Timeline style sheet'.i18n));
    // for (var e in timelineSSFiles.entries) {
    //   content.add(_createCell(_SemKind.timelineSS, e.value));
    // }

    // content.add(_createTitleCell('Group summary style sheet'.i18n));
    // for (var e in groupSSFiles.entries) {
    //   content.add(_createCell(_SemKind.groupSS, e.value));
    // }

    // content.add(_createTitleCell('Channel summary style sheet'.i18n));
    // for (var e in channelSSFiles.entries) {
    //   content.add(_createCell(_SemKind.channelSS, e.value));
    // }

    return AlertDialog(
      titlePadding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
      insetPadding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
      title: Container(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 18),
        width: MediaQuery.of(context).size.width - 40,
        decoration: const BoxDecoration(
          border: const Border(bottom: const BorderSide(color: Colors.grey)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(Icons.settings),
            const Padding(padding: const EdgeInsets.only(right: 6)),
            Text('OSS setting'.i18n),
          ],
        ),
      ),
      content: SingleChildScrollView(
        child: Column(
          children: content,
        ),
      ),
      actions: [
        if (!widget.isReadOnly)
          TextButton(
            child: Text('Friends reference'.i18n),
            onPressed: () {
              showDialog<Iterable<Friend>>(
                  context: context,
                  builder: (ctx) => FriendSelectDialog(
                        root: widget.root,
                        title: 'Friends reference'.i18n,
                        style: FriendListStyle.checkbox,
                        isMultiSelection: true,
                      )).then((value) {
                if (value != null) {
                  for (var friend in value) {
                    friend.syncSilently().then((_) {
                      friend.publicRoot!.syncSilently().then((_) {
                        _loadOssI(friend.publicRoot!, friend);
                      });
                    });
                  }
                }
              });
            },
          ),
        TextButton(
          child: Text('Cancel'.i18n),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('OK'.i18n),
          onPressed: () {
            Future<void> future = widget.onDone(
              ontologyFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedOntology.contains(e.id)),
              restrictionFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedRestriction.contains(e.id)),
              constraintFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedConstraint.contains(e.id)),
              timelineSSFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedTimelineSS.contains(e.id)),
              groupSSFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedGroupSS.contains(e.id)),
              channelSSFiles.values
                  .map((e) => e.ossFile)
                  .where((e) => selectedChannelSS.contains(e.id)),
            );
            //Navigator.of(context).pop();

            /// 非同期処理中はモーダルを出す
            /// 完了したら戻ってくるのでエンティティダイアログを閉じる
            Navigator.of(context)
                .push(ProgressModal(context, future, 'saving data'.i18n))
                .then((value) => Navigator.of(context).pop());
          },
        ),
      ],
    );
  }

  Widget _createTitleCell(String title) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(8, 12, 12, 12),
      color: const Color(0xff43a047),
      child: Text(
        title,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _createCell(_SemKind kind, _SemOssSelectItem item) {
    // 親グラフに含まれるかチェック
    bool isIncludeParent = checkParent(kind, item.ossFile.id);

    // 強制チェック、対象はなくなったのでコメント化
    /*
    if (kind == _SemKind.ontology) {
      OssFile ossFile = item.ossFile;
      if (ossFile.title.value == "Base!") {
        isIncludeParent = true;
      }
      if (ossFile.title.value == "Discourse Graph") {
        isIncludeParent = true;
      }
    }
    if (kind == _SemKind.restriction) {
      OssFile ossFile = item.ossFile;
      if (ossFile.title.value == "Base!") {
        isIncludeParent = true;
      }
    }
    */
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
      decoration: const BoxDecoration(
        border: const Border(bottom: const BorderSide(color: Colors.grey)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(
            value: _getSelected(kind, item.ossFile.id),
            onChanged: (widget.isReadOnly || isIncludeParent)
                ? null
                : (value) => _setSelected(kind, item.ossFile.id!, value ?? false),
            activeColor: Colors.blue,
          ),
          Expanded(
            child: ChangeNotifierProvider<HasOwnerNotifier>(
              create: (_) =>
                  HasOwnerNotifier(item.ownerModel, root: widget.root)
                    ..update(),
              child: Consumer<HasOwnerNotifier>(
                builder: (ctx, notifier, child) {
                  var picture = (notifier.picture != null)
                      ? memoryImageOf(notifier.picture!)
                      : const Icon(Icons.person, size: 24);
                  return Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          item.ossFile.title.defaultValue ?? item.ossFile.id!,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      if (item.ownerModel != null)
                        Expanded(
                          flex: 1,
                          child: Container(
                            padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                SizedBox(
                                  width: 24,
                                  height: 24,
                                  child: picture,
                                ),
                                Expanded(
                                  child: Text(
                                    notifier.owner ?? '',
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool _getSelected(_SemKind kind, String? id) {
    switch (kind) {
      case _SemKind.ontology:
        return selectedOntology.contains(id) | checkParent(kind, id);

      case _SemKind.restriction:
        return selectedRestriction.contains(id) | checkParent(kind, id);

      case _SemKind.constraint:
        return selectedConstraint.contains(id) | checkParent(kind, id);

      case _SemKind.timelineSS:
        return selectedTimelineSS.contains(id);

      case _SemKind.groupSS:
        return selectedGroupSS.contains(id);

      case _SemKind.channelSS:
        return selectedChannelSS.contains(id);
    }
  }

  void _setSelected(_SemKind kind, String id, bool value) {
    switch (kind) {
      case _SemKind.ontology:
        if (value) {
          selectedOntology.add(id);
        } else {
          selectedOntology.remove(id);
        }
        break;

      case _SemKind.restriction:
        if (value) {
          selectedRestriction.add(id);
        } else {
          selectedRestriction.remove(id);
        }
        break;

      case _SemKind.constraint:
        if (value) {
          selectedConstraint.add(id);
        } else {
          selectedConstraint.remove(id);
        }
        break;

      case _SemKind.timelineSS:
        if (value) {
          selectedTimelineSS.add(id);
        } else {
          selectedTimelineSS.remove(id);
        }
        break;

      case _SemKind.groupSS:
        if (value) {
          selectedGroupSS.add(id);
        } else {
          selectedGroupSS.remove(id);
        }
        break;

      case _SemKind.channelSS:
        if (value) {
          selectedChannelSS.add(id);
        } else {
          selectedChannelSS.remove(id);
        }
        break;
    }
    setState(() {});
  }

  /// 親グラフの存在チェック
  bool checkParent(_SemKind kind, String? id) {
    bool result = false;
    switch (kind) {
      case _SemKind.ontology:
        if (parentOntologyFilesIds.contains(id)) {
          result = true;
        }
        break;

      case _SemKind.restriction:
        if (parentRestrictionFilesIds.contains(id)) {
          result = true;
        }
        break;

      case _SemKind.constraint:
        if (parentConstraintFilesIds.contains(id)) {
          result = true;
        }
        break;

      default:
    }
    return result;
  }

  /// ソート
  List<MapEntry<String, _SemOssSelectItem>> itemSort(List<MapEntry<String, _SemOssSelectItem>> tmp, _SemKind kind) {
    tmp.sort((a, b) {
      var a_readonly = checkParent(kind, a.value.ossFile.id);
      var b_readonly = checkParent(kind, b.value.ossFile.id);
      var a_selected = _getSelected(kind, a.value.ossFile.id);
      var b_selected = _getSelected(kind, b.value.ossFile.id);
      if (a_readonly && b_readonly) return 0;
      if (a_readonly) return -1;
      if (b_readonly) return 1;
      if (a_selected) return -1;
      if (b_selected) return 1;
      return 0;
    });
    return tmp;
  }
}
