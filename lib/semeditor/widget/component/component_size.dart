import 'dart:math';

import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../data/custom_component_data.dart';

class ComponentSize {
  static const double _aspectRatio = MyComponentData.aspectRatio;
  static const double _minWidth    = MyComponentData.defaultNodeWidth;
  static const double _minHeight   = _minWidth / _aspectRatio;
  static const double _mobileFontSize = 12;
  static const double _mobileFontHeight = 1.4;
  static const double _pcFontSize = 14;
  static const double _pcFontHeight = 1.2;
  static const double componentPadding = 5;
  static const double linkLabelSideMargin = 2;
  static const double linkLabelBottomMargin = 2;

  static const TextStyle mobileTextStyle = const TextStyle(fontSize: _mobileFontSize, height: _mobileFontHeight);
  static const TextStyle pcTextStyle = const TextStyle(fontSize: _pcFontSize, height: _pcFontHeight);
  static TextStyle cutMobileTextStyle = TextStyle(fontSize: _mobileFontSize, height: _mobileFontHeight, color: Colors.black.withOpacity(0.3));
  static TextStyle cutPcTextStyle = TextStyle(fontSize: _pcFontSize, height: _pcFontHeight, color: Colors.black.withOpacity(0.3));
  static final TextStyle linkLabelMobileTextStyle = mobileTextStyle.copyWith(color: Colors.white);
  static final TextStyle selectedLinkLabelMobileTextStyle = mobileTextStyle.copyWith(color: Colors.red);
  static TextStyle cutSelectedLinkLabelMobileTextStyle = selectedLinkLabelMobileTextStyle.copyWith(color:selectedLinkLabelMobileTextStyle.color?.withOpacity(0.3));
  static final TextStyle linkLabelPcTextStyle = pcTextStyle.copyWith(color: Colors.white);
  static final TextStyle selectedLinkLabelPcTextStyle = pcTextStyle.copyWith(color: Colors.red);
  static TextStyle cutSelectedLinkLabelPcTextStyle = selectedLinkLabelPcTextStyle.copyWith(color:cutSelectedLinkLabelPcTextStyle.color?.withOpacity(0.3));
  static final TextStyle _componentSizeTextStyle = pcTextStyle.copyWith(fontFamily: "M PLUS 1p");

  final BuildContext context;
  final String       classText;
  final String       cntText;

  Size size = const Size(0, 0);
  Size classSize = const Size(0, 0);
  Size cntSize = const Size(0, 0);

  double? width;
  bool? isLinkLabel;
  List<LinkTypeProperty>? typeList;
  static const String typeKey = "type";

  ComponentSize(this.context, this.classText, this.cntText, {this.width, this.isLinkLabel, this.typeList,}) {
    if (isLinkLabel == true && typeList?.isNotEmpty == true) {
      String? text;
      double maxWidth = 0;
      double height = 0;
      for (LinkTypeProperty linkType in typeList!) {
        maxWidth = max(maxWidth, _getSize(linkType.type!, isLinkLabel: isLinkLabel).width);
        height += _getHeight(linkType.type!, size.width);
        text = text == null ? linkType.type: text +'\n'+ linkType.type!;
      }
      size  = _getSize(text!, isLinkLabel: isLinkLabel);
      size = Size(maxWidth, size.height);
      double aveHeight = height / typeList!.length;
      if (height < size.height) {
        aveHeight = size.height / typeList!.length;
      }
      cntSize = Size(size.width, aveHeight);
      size  = Size(size.width + aveHeight + linkLabelSideMargin * 2, size.height + ComponentSize.linkLabelBottomMargin * typeList!.length);
    } else {
      if(width != null) {
        if (classText.isEmpty && cntText.isEmpty) {
          size = Size(width!, 0);
        } else if (classText.isEmpty) {
          double height = _getLetterHeight(cntText, width!);
          cntSize = Size(width!, height);
          size  = cntSize;
        } else if (cntText.isEmpty) {
          double height = _getLetterHeight(classText, width!);
          classSize = Size(width!, height);
          size  = classSize;
        } else {
          double height1 = _getLetterHeight(classText, width!);
          double height2 = _getLetterHeight(cntText, width!);
          double height  = height1 + height2;
          classSize = Size(width!, height1);
          cntSize = Size(width!, height2);
          size = Size(width!, height);
        }
      } else if (classText.isEmpty && cntText.isEmpty) {
        size = const Size(_minWidth, 0);
      } else if (classText.isEmpty) {
        cntSize = _getSize(cntText);
        size  = cntSize;
      } else if (cntText.isEmpty) {
        classSize = _getSize(classText, isLinkLabel: isLinkLabel);
        size = classSize;
      } else {
        size  = _getSize(classText + '\n' + cntText);
        double height1 = _getHeight(classText, size.width);
        double height2 = _getHeight(cntText, size.width);
        double height  = height1 + height2;
        if (height < size.height) {
          height1 = size.height * height1 / height;
          height2 = size.height * height2 / height;
        }
        classSize = Size(size.width, height1);
        cntSize = Size(size.width, height2);
      }
    }
  }

  //
  // 調整後のサイズ取得
  //
  Size _getSize(String text, {bool? isLinkLabel}) {
    TextSpan ts;
    if (isLinkLabel == true) {
      ts = TextSpan(text: text, style: isMobile ? mobileTextStyle: pcTextStyle);
    } else {
      ts = TextSpan(text: text.padRight((text.length + 1),"　"), style: _componentSizeTextStyle);
    }
    TextPainter tp = TextPainter(text: ts, textDirection: TextDirection.ltr);

    return (isLinkLabel == true) ? _getTextSize(tp, 0): _adjustSize(_getTextSize(tp, 0), tp);
  }

  //
  // 幅指定時の高さ取得
  //
  double _getHeight(String text, double maxWidth) {
    TextSpan    ts = TextSpan(text: text, style: _componentSizeTextStyle);
    TextPainter tp   = TextPainter(text: ts, textDirection: TextDirection.ltr);
    Size        size = _getTextSize(tp, maxWidth);

    return size.height;
  }

  //
  // M PLUS 1pがない時の高さ取得
  //
  double _getLetterHeight(String text, double maxWidth) {
    TextSpan ts;
    if (isMobile) {
      ts = TextSpan(text: text, style: mobileTextStyle);
    } else {
      ts = TextSpan(text: text, style: pcTextStyle);
    }
    TextPainter tp   = TextPainter(text: ts, textDirection: TextDirection.ltr);
    Size        size = _getTextSize(tp, maxWidth);

    return size.height + 2;
  }

  //
  // サイズ取得
  //
  Size _getTextSize(TextPainter tp, double maxWidth) {

    // テキストサイズを固定する
    double sf = 1.0;

    if (0 < maxWidth) {
      tp.layout(maxWidth: maxWidth / sf);
    } else {
      tp.layout();
    }

    return Size(tp.width * sf + 2, tp.height * sf);
  }

  //
  // サイズ調整
  //
  Size _adjustSize(Size oldSize, TextPainter tp) {
    if (oldSize.width <= _minWidth) {
      // 最低幅以下 ①②
      return Size(oldSize.width, _max(_minHeight, oldSize.height));
    } else {
      // 最低幅超え
      if (oldSize.height <= _minHeight) {
        // 最低高以下
        // 最低幅で折り返し
        Size newSize = _getTextSize(tp, _minWidth);
        if (newSize.height <= _minHeight) {
          // 最低高以下 ③
          return const Size(_minWidth, _minHeight);
        } else {
          // 最低高超え ④
          return _getWrappingSize(Size(oldSize.width, _minHeight), tp);
        }
      } else {
        // 最低高超え
        if (oldSize.aspectRatio <= _aspectRatio) {
          // 幅が比率以下 ⑤
          return oldSize;
        } else {
          // 幅が比率超え ⑥
          return _getWrappingSize(oldSize, tp);
        }
      }
    }
  }

  //
  // 折り返しサイズ取得
  //
  Size _getWrappingSize(Size oldSize, TextPainter tp) {
    Size newSize = _adjustWrappingSize(oldSize, tp, 100);
    newSize      = _adjustWrappingSize(newSize, tp, 10);
    newSize      = _adjustWrappingSize(newSize, tp, 1);
    return newSize;
  }

  //
  // 折り返しサイズ調整
  //
  Size _adjustWrappingSize(Size oldSize, TextPainter tp, double pitch) {
    double ratioDiff = oldSize.aspectRatio - _aspectRatio;
    if (ratioDiff == 0) {
      return oldSize;
    } else if (ratioDiff < 0) {
      pitch *= -1;
    }
    double width   = oldSize.width - pitch;
    Size   tmpSize = oldSize;

    while (width >= _minWidth) {
      Size newSize = _getTextSize(tp, width);
      if (newSize.height < _minHeight) {
        newSize = Size(newSize.width, _minHeight);
      }
      double newDiff = newSize.aspectRatio - _aspectRatio;

      if (ratioDiff.abs() < newDiff.abs()) {
        break;
      }

      ratioDiff = newDiff;
      tmpSize   = newSize;
      width    -= pitch;
    }

    return tmpSize;
  }

  //
  // 最大値
  //
  double _max(double num1, double num2) {
    if (num1 >= num2) {
      return num1;
    } else {
      return num2;
    }
  }
}
