import 'package:flutter/material.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../../page/timeline/image_modal.dart';
import '../../../page/timeline/timeline_state_base.dart';
import '../../../page/timeline/video_modal.dart';
import '../../../page/timeline/timeline_item.dart';
import '../../../widget/property_tooltip.dart';
import '../../policy/custom_policy.dart';
import '../../widget/component/component_size.dart';
import '../../data/custom_component_data.dart';

class BaseComponentBody extends StatelessWidget {
  final ComponentData componentData;
  final CustomPainter? componentPainter;

  const BaseComponentBody({
    super.key,
    required this.componentData,
    this.componentPainter,
  });

  @override
  Widget build(BuildContext context) {
    final MyComponentData customData = componentData.data;

    // 下段(cnt)のBoxDecoration
    Border? cntBorder;
    customData.typeList?.sort((a,b) => a.elementId.compareTo(b.elementId));

    if (customData.dispClass?.isNotEmpty == true) {
      cntBorder = Border(
        top: BorderSide(
          color: customData.borderColor,
          width: MyComponentData.itemImageBorder,
        ),
      );
    } else {
      cntBorder = null;
    }

    return GestureDetector(
      child:Container(
        decoration: BoxDecoration(
          color: customData.isLinkLabel ? customData.isCutLinkLabel ? Colors.grey[600]!.withOpacity(0.3) : Colors.grey[600]!.withOpacity(0.7)
              : Colors.white,
          border: Border.all(
            color: customData.isLinkLabel ? customData.borderColor: customData.isCutNode ? customData.borderColor.withOpacity(0.1): customData.borderColor.withOpacity(1),
            width: customData.borderWidth,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //リンクラベル
            if (customData.typeList != null)
              Container(
                child: Column(
                  children: [
                    for (int i= 0; i < customData.typeList!.length; i++)
                      PropertyTooltip.maybe(
                        customData.typeList![i].tooltip,
                        customData.typeList![i].subject ?? "",
                        customData.typeList![i].object ?? "",
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTapUp: (TapUpDetails details) {
                            TapUpDetails detail = new TapUpDetails(globalPosition: details.globalPosition, localPosition: details.localPosition + Offset(0, (customData.typeCntSize!.height + ComponentSize.linkLabelBottomMargin) * i), kind: details.kind);
                            for (LinkTypeProperty linkType in customData.typeList!) {
                              if (customData.typeList![i].elementId != linkType.elementId) {
                                CustomStatePolicy.isSelectedTypeMap[linkType.elementId] = false;
                              }
                            }
                            if (!customData.typeList![i].isWriting) CustomStatePolicy.isSelectedTypeMap[customData.typeList![i].elementId] = true;
                            componentData.updateComponent();
                            customData.onLinkTapUpHandler!(componentData.id, detail);
                          },

                          onLongPressStart: (LongPressStartDetails details) {
                            CustomStatePolicy.longPressType = customData.typeList![i];
                            LongPressStartDetails detail = new LongPressStartDetails(globalPosition: details.globalPosition, localPosition: details.localPosition + Offset(0, (customData.typeCntSize!.height + ComponentSize.linkLabelBottomMargin) * i));
                            customData.onLinkLongPressStartHandler!(componentData.id, detail);
                          },
                          child: Row(
                            children: [
                              !customData.typeList![i].isSymmetric! ? Container(
                                height: customData.typeCntSize!.height,
                                width: customData.typeCntSize!.height,
                                margin: const EdgeInsets.only(left: ComponentSize.linkLabelSideMargin),
                                child: customData.typeList![i].angle == null ? null
                                    : Transform.rotate(
                                  angle: customData.typeList![i].angle!,
                                  child:  Transform.scale(
                                      scaleX: 0.7,
                                      child: Icon(
                                        size: customData.typeCntSize!.height,
                                        color:Colors.white,
                                        Icons.navigation,
                                      ),
                                  ),
                                ),
                              )
                                  : Container(
                                margin: const EdgeInsets.only(left: ComponentSize.linkLabelSideMargin),
                              ),
                              Container(
                                height: customData.typeCntSize!.height,
                                width: !customData.typeList![i].isSymmetric! ? customData.typeCntSize!.width: customData.typeCntSize!.width + customData.typeCntSize!.height,
                                margin: const EdgeInsets.only(right: ComponentSize.linkLabelSideMargin, bottom: ComponentSize.linkLabelBottomMargin),
                                child: Text(
                                  customData.typeList![i].type!,
                                  textAlign: TextAlign.center,
                                  textHeightBehavior:  const TextHeightBehavior(
                                    applyHeightToLastDescent: false,
                                    applyHeightToFirstAscent: false,
                                  ),
                                  style: isMobile ?
                                  CustomStatePolicy.isSelectedTypeMap[customData.typeList![i].elementId] == true
                                      ? customData.isCutLinkLabel == true ? ComponentSize.cutSelectedLinkLabelMobileTextStyle: ComponentSize.selectedLinkLabelMobileTextStyle
                                      : ComponentSize.linkLabelMobileTextStyle
                                  :CustomStatePolicy.isSelectedTypeMap[customData.typeList![i].elementId] == true
                                      ? customData.isCutLinkLabel == true ? ComponentSize.cutSelectedLinkLabelPcTextStyle: ComponentSize.selectedLinkLabelPcTextStyle
                                  : ComponentSize.linkLabelPcTextStyle,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ),

            /// クラス
            if (customData.dispClass?.isNotEmpty == true)
              if (customData.dispCntImage != null)
              for (var dispCnt in customData.dispCntList!)
                GestureDetector(
                  // onTapDownの場合ノード選択が反応しないため、onLongPressStartを使用
                  onLongPressStart: storePosition,
                  onLongPress: () => showPopupMenu(context, dispCnt.fileNames, customData.isMe, customData.tapPosition),
                  child: Container(
                    margin: const EdgeInsets.all(ComponentSize.componentPadding),
                    child: Text(
                      customData.dispClass!,
                      style: isMobile ? customData.isCutNode ? ComponentSize.cutMobileTextStyle: ComponentSize.mobileTextStyle
                          : customData.isCutNode ? ComponentSize.cutPcTextStyle: ComponentSize.pcTextStyle,
                ),
                    width: customData.dispClassSize!.width,
                    height: customData.dispClassSize!.height,
                  ),
                ),
            if (customData.dispCntImage == null)
              if (customData.dispClass?.isNotEmpty == true)
              Container(
                margin: const EdgeInsets.all(ComponentSize.componentPadding),
                child: Text(
                  customData.dispClass!,
                  style: isMobile ? ComponentSize.mobileTextStyle : ComponentSize.pcTextStyle,
                ),
                width: customData.dispClassSize!.width,
                height: customData.dispClassSize!.height,
              ),
            /// テキスト
            for (var dispCnt in customData.dispCntList!)
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: const Color(0xffaaaaaa),
                    width: customData.secondBorderWidth,
                  ),
                ),
                child: Column(
                  children: [
                    /// タイトル・コメント
                    if (customData.dispCntList?.isNotEmpty == true)
                      GestureDetector(
                        // onTapDownの場合ノード選択が反応しないため、onLongPressStartを使用
                        onLongPressStart: storePosition,
                        onLongPress: () => showPopupMenu(context, dispCnt.fileNames, customData.isMe, customData.tapPosition),
                        child: Container(
                          padding: const EdgeInsets.only(top: ComponentSize.componentPadding, bottom: ComponentSize.componentPadding),
                          decoration: BoxDecoration(
                            border: cntBorder,
                            color: customData.isCutNode ? customData.color.withOpacity(0.3) : customData.color.withOpacity(1),
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // タイトル
                                if (dispCnt.title.isNotEmpty)
                                  Container(
                                    margin: const EdgeInsets.only(left: ComponentSize.componentPadding, right: ComponentSize.componentPadding),
                                    width: dispCnt.width,
                                    child: AutoConvertUrlAndEmailToLinkText(
                                      dispCnt.title,
                                      textStyle: isMobile ? !customData.isLinkLabel && customData.isCutNode ? ComponentSize.cutMobileTextStyle: ComponentSize.mobileTextStyle
                                          : !customData.isLinkLabel && customData.isCutNode ? ComponentSize.cutPcTextStyle: ComponentSize.pcTextStyle,
                                    ),
                                  ),

                                // タイトルとコメントの間にボーダーを入れる
                                if ((dispCnt.detail.isNotEmpty) && (dispCnt.title.isNotEmpty))
                                  Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        top: BorderSide(
                                          color: Colors.white,
                                          width: customData.secondBorderWidth,
                                        ),
                                      ),
                                    ),
                                  ),

                                // コメント
                                if (dispCnt.detail.isNotEmpty)
                                  Container(
                                    margin: const EdgeInsets.only(left: ComponentSize.componentPadding, right: ComponentSize.componentPadding),
                                    width: dispCnt.width,
                                    child: AutoConvertUrlAndEmailToLinkText(
                                      dispCnt.detail,
                                      textStyle: isMobile ? !customData.isLinkLabel && customData.isCutNode ? ComponentSize.cutMobileTextStyle: ComponentSize.mobileTextStyle
                                          : !customData.isLinkLabel && customData.isCutNode ? ComponentSize.cutPcTextStyle: ComponentSize.pcTextStyle,
                                    ),
                                  ),
                              ]),
                          height: dispCnt.height,
                        ),
                      ),

                    /// MEDIA
                    if (dispCnt.image != null)
                      GestureDetector(
                        // onTapDownの場合ノード選択が反応しないため、onLongPressStartを使用
                        onLongPressStart : storePosition,
                        onTap: () {
                          onThumbnailTap(context,dispCnt.dataFormat,customData.isMe);
                        },
                        onLongPress: () => showPopupMenu(context, dispCnt.fileNames,customData.isMe, customData.tapPosition),
                          child: (dispCnt.dataFormat?.asFile.format?.startsWith('video/') ?? false) ?
                          Container(
                            color: Colors.black,
                            margin: const EdgeInsets.all(ComponentSize.componentPadding),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                const Icon(
                                  Icons.movie,
                                  color: Colors.white,
                                  size: 100,
                                ),
                                Container(
                                  child: const Icon(
                                    Icons.play_arrow,
                                    color: Colors.black,
                                    size: 40,
                                  ),
                                ),
                              ],
                            ),
                          ) : Center(
                            child:Container(
                              margin: const EdgeInsets.all(ComponentSize.componentPadding),
                              child: memoryImageOf(
                                dispCnt.image!,
                              ),
                            ),
                          )
                      ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );

    // 元の処理、念のためコメントにして残しておく
    /*
    return GestureDetector(
      child: CustomPaint(
        painter: componentPainter,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 4),
          child: Align(
            alignment: customData.textAlignment,
            child: Text(
              customData.text,
              style: TextStyle(fontSize: customData.textSize),
            ),
          ),
        ),
      ),
    );
    */
  }

  void onThumbnailTap(BuildContext context, dispCntImageFormat, isMe) async {
    // cnt属性のデータを取得(リストの最初の要素)
    if (dispCntImageFormat.asFile.format?.startsWith('image') ?? false) {
      Navigator.push(context, ImageModal.fromMmdata(dispCntImageFormat!.asFile));
    }
    else if (dispCntImageFormat.asFile.format?.startsWith('video') ?? false) {
      Navigator.push(context, VideoModal.fromMmdata(dispCntImageFormat!.asFile));
    } else {
      showDownloadDialog(context, [dispCntImageFormat], isMe);
    }
  }

  Future<void> showPopupMenu(BuildContext context, dispFileName, isMe, tapPosition) async {
    var ancestorContext =
        context.findAncestorStateOfType<TimelineStateBase>()?.context;
    if (ancestorContext == null) return;
    context = ancestorContext;

    /// ignore: unused_local_variable
    bool isImage = false;
    bool isVideo = false;
    bool isOthers = false;

    for (var mmdata in dispFileName.mmdatas) {
      if (!mmdata.isFile) continue;

      final downloadPermission =
      mmdata.asFile.peer?.firstLiteralValueOf(downloadPermissionProperty);
      if (!isMe && downloadPermission == false) {
        continue;
      }


      var format = mmdata.asFile.format;
      if (format?.startsWith('image/') ?? false) {
        /// ファイル形式が画像
        isImage = true;
        break;
      }
      else if (format?.startsWith('video/') ?? false) {
        /// ファイル形式が動画
        isVideo = true;
        break;
      } else isOthers = true;
    }

    /// ポップアップメニュー項目設定
    var _menuList = [

      /// ファイルが含まれれば『ダウンロード』を表示
      if (isImage)
        TimelineItemPopup.DOWNLOAD_IMAGE,
      if (isVideo || isOthers)
        TimelineItemPopup.DOWNLOAD_FILE,

    ];
    if (_menuList.isEmpty) return;

    RenderBox overlay = Overlay.of(context).context.findRenderObject() as RenderBox;
    TimelineItemPopup? selected;
    selected = await showMenu(
        context: context,
        position: RelativeRect.fromRect(
          tapPosition & const Size(-20, -20),
          Offset.zero & overlay.size,
        ),
        items: _menuList.map((TimelineItemPopup menuItem) {
          return new PopupMenuItem<TimelineItemPopup>(
            child: Text(menuItem.label),
            value: menuItem,
          );
        }).toList()
    ).catchError((_) => null);

    if (selected == null) return;

    switch (selected) {
      case TimelineItemPopup.DOWNLOAD_IMAGE:
      case TimelineItemPopup.DOWNLOAD_FILE:
        /// 画像のダウンロード
        FocusScope.of(context).unfocus();
        showDownloadDialog(context, dispFileName.mmdatas, isMe);
        break;
      default:
        break;
    }
  }

  void storePosition(LongPressStartDetails details) {
    MyComponentData customData = componentData.data;
    customData.tapPosition = details.globalPosition;
  }
}
