import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget/button.dart';

import '../page/semedit_main.dart';

class ChannelSemEditorButton<N extends PlrNotifier>
    extends EntityButtonDef<Channel, N> {
  final Account account;

  ChannelSemEditorButton(
    BuildContext context,
    this.account,
  ) : super(
    Image.asset(
      'assets/sem_editor.png',
      width: 24,
      height: 24,
      filterQuality: FilterQuality.medium,
    ),
    onPressed: (channel, _) => Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
          SemEditMain(
            account,
            channel,
          ),
      ),
    ),
  );
}
