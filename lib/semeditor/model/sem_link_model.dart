import 'package:plr_ui/plr_ui.dart';
import 'package:flutter/material.dart';

class SemLinkModel {
  final GlobalKey? key;

  final Entity linkEntity;
  final Schemata schemata;

  late String dispText;
  late bool isSymmetric;

  static const String  arg1Property = "arg1";
  static const String  arg2Property = "arg2";
  static const String  graphRelationClass = "GraphRelation";
  static const String  symmetricRelationClass = "SymmetricRelation";

  SemLinkModel(this.linkEntity, this.schemata, {this.key}) {
    String? type = linkEntity.type;
    SchemaClass? schemaClass; {
      if (type != null) schemaClass = schemata.classOf(type);
    }
    dispText = _setDispText(type, schemaClass);
    isSymmetric = _setSymmetricType(schemaClass);
  }

  String _setDispText(String? type, SchemaClass? schemaClass) {
    String text = (
      schemaClass == null
        ? type
        : schemaClass.label?.defaultValue
    ) ?? "";

    // 旧データ保存形式対応
    if (text.endsWith("Property")) {
      List<String> texts = text.split("-");
      text = texts[0];
    }

    return text;
  }

  bool _setSymmetricType(SchemaClass? schemaClass) {
    return schemaClass != null && schemaClass.isDescendantOf(schemata.classOf(symmetricRelationClass));
  }
}
