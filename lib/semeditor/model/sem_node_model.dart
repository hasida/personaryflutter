import 'dart:typed_data' show Uint8List;

import 'package:plr_ui/plr_ui.dart';
import 'package:flutter/material.dart';


class SemNodeModel {
  final GlobalKey? key;

  RecordEntity? _recordEntity;
  final String plrId;

  /// クラス名表示欄テキスト
  String? dispClass;

  static const String  graphLinkProperty = "graphLink";

  /// 所有者が自分かどうか
  bool get isMe => (plrId == (_recordEntity?.creator ?? plrId));


  set recordEntity(RecordEntity? recordEntity) {
    _recordEntity = recordEntity;
    _setDispClass();
    _setDispCntList();
  }

  RecordEntity? get recordEntity => _recordEntity;

  /// 内容表示欄
  List<DisplayCnt> dispCntList = [];

  /// 位置
  Offset? _posOffset;
  Offset? get posOffset => _posOffset;
  set posOffset(Offset? posOffset) {
    _posOffset = posOffset;
  }

  SemNodeModel(
      this.plrId,
      {this.key});

  void _setDispClass() {
    if (recordEntity?.entity?.type == "M") {
      // Mクラスの場合はクラス名表示なし
      dispClass = "";
    } else {
      FormatResult? formatResult = recordEntity?.format();
      if (formatResult != null) {
        // クラス名
        dispClass = formatResult.typeLabel ?? '';
        // クラス名の後ろにフォーマット結果を追加
        if (formatResult.text?.isNotEmpty == true) {
          dispClass = dispClass! + (dispClass!.isNotEmpty ? ' ' : '') + formatResult.text!;
        }
      }
    }
  }

  void _setDispCntList() {
    FormatResult? formatResult = recordEntity?.format();
    if (formatResult != null && formatResult.hasMmdata) {
      // cnt属性のデータを取得
      List<Mmdata> mmdataList = formatResult.mmdatasById("cnt");
      for (var mmdata in mmdataList) {
        DisplayCnt dispCnt = DisplayCnt();

        dispCnt.fileNames = formatResult;
        // コメントのみの場合
        if (mmdata.isLiteral && mmdata.text.isNotEmpty) {
          dispCnt.detail = mmdata.asLiteral.text.value;
        }

        // タイトルや画像を追加した場合(ファイルノードの場合)
        if (mmdata.isFile) {
          // ファイルノード時のタイトル部
          if (mmdata.asFile.text.isNotEmpty &&
              mmdata.asFile.text.defaultValue != "") {
            dispCnt.title = mmdata.asFile.text.defaultValue;
          }
          // ファイルノード時のコメント部
          if (mmdata.asFile.comment.isNotEmpty &&
              mmdata.asFile.comment.defaultValue != "") {
            dispCnt.detail = mmdata.asFile.comment.defaultValue;
          }
          // ファイルノード時のサムネイル（アイコン）
          if (mmdata.asFile.thumbnailData != null &&
              mmdata.asFile.thumbnailData!.length != 0) {
            dispCnt.image = mmdata.asFile.thumbnailData;
            dispCnt.dataFormat = mmdata;
          }
        }
        dispCntList.add(dispCnt);
      }
    }
  }
}

/// cnt表示項目
class DisplayCnt {
  double width = 0;
  double height = 0;
  String title = "";
  String detail = "";
  Uint8List? image;
  Mmdata? dataFormat;
  FormatResult? fileNames;
}
