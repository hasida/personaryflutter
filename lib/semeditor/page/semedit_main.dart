import 'dart:async';
import 'dart:typed_data' show Uint8List;

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:flutter_breadcrumb/flutter_breadcrumb.dart';
import 'package:personaryFlutter/semeditor/policy/custom_policy.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:synchronized/extension.dart';

import '../../page/timeline/timeline_page.dart';
import '../../page/timeline/timeline_state_base.dart';
import '../../util/show_message.dart';
import '../../dialog/channel_edit/channel_edit_util.dart';

import '../../util/stack_list.dart';
import '../../widget/layout.dart' show emptyWidget;
import '../dialog/sem_oss_select.dart';
import '../logic/sem_logic.dart';
import '../model/sem_node_model.dart';
import '../model/sem_link_model.dart';
import '../policy/my_policy_set.dart';
import '../policy/minimap_policy.dart';
import '../widget/option_icon.dart';
import 'semedit_main.i18n.dart';

/// セマンティックエディタメイン画面
// ignore: must_be_immutable
class SemEditMain extends TimelinePageBase {
  /// グラフアイテム（ハイパーノード時）
  Item? graphItemHyper;

  /// 親のグラフアイテム（ハイパーノード時）
  Item? parentGraphItemHyper;

  bool isMultipleSelectionOn = false;

  SemEditMain(super.account, super.timeline, {super.key});

  SemEditMain.hyperNode(
    super.account, super.timeline, Item item, Item parent, bool isMultipleSelectionOn,
      {super.key}) {
    this.graphItemHyper = item;
    this.parentGraphItemHyper = parent;
    this.isMultipleSelectionOn = isMultipleSelectionOn;
  }

  @override
  ConsumerState<SemEditMain> createState() => _SemEditMainState();
}

class _SemEditMainState extends TimelineStateBase<SemEditMain> {
  /// diagram_editor
  late DiagramEditorContext diagramEditorContext;
  late DiagramEditorContext diagramEditorContextMiniMap;

  MyPolicySet myPolicySet = MyPolicySet();
  MiniMapPolicySet miniMapPolicySet = MiniMapPolicySet();
  final StreamController<bool> loadAiStreamController = new StreamController<bool>.broadcast();
  final StreamController<bool> copyStreamController = new StreamController<bool>.broadcast();
  final StreamController<bool> duplicateStreamController = new StreamController<bool>.broadcast();
  final StreamController<bool> readyToReconnectLinkStreamController = new StreamController<bool>.broadcast();
  final StreamController<Offset?> multipleSelectDragPositionController = new StreamController<Offset?>.broadcast();

  bool isMiniMapVisible = false;
  bool isMenuVisible = false;
  bool isOptionsVisible = false;

  /// 描画中フラグ
  /// タイムラインの二重読み込み防止と画面プログレスインジケータで使用
  bool isDrawGraphing = false;
  final StreamController<bool> drawGraphController = StreamController();

  /// システムアカウントのOss
  Oss? systemOss;

  /// グラフ用Schemata
  late Schemata graphSchemata;

  /// rootグラフ用Schemata
  static late Schemata rootGraphSchemata;

  /// タイムライン用Schemata
  Schemata? get timelineSchemata => super.selectedSchemata;

  /// グラフアイテム
  Item? graphItem;
  Graph? graph;

  static DataSetting? rootGraphSetting;

  // 削除確認用ワーク
  Set<String> saveNodeIdList = {};
  //List<String> saveLinkIdList = [];

  /// 初回描画フラグ(位置づけに使う)
  bool isFirstDraw = true;

  //初回描画時のタイトル追加フラグ
  bool isFirstDrawTitle = true;

  late bool isTopGraph;

  /// AppBar
  // late AppBarTitle appBarTitle;
  // final StreamController<AppBarTitle> appBarTitleController = StreamController();

  AppBarTitle appBarTitle = AppBarTitle();
  final StreamController<StackList<AppBarTitle>> appBarTitleController = StreamController<StackList<AppBarTitle>>();

  /// Appbartitleにスタックリストを持たせる
  static StackList<AppBarTitle> appBarStackTitle = StackList<AppBarTitle>();

  /// appBarのスクロール制御
  ScrollController appBarScrollController = ScrollController();
  bool appBarScrolled = false;

  /// タイマオブジェクト
  Timer? _semTimer;

  /// 周期タイマ(5sec)
  final int _syncInterval = 5;

  /// デバッグ用
  late DebugSts debugSts;

  /// コピーしたノードデータ
  ComponentData? copiedNodeData;

  GlobalKey editorKey = GlobalKey();

  EntitySyncNotifier? notifier;

  @override
  void loadTimelineItems({DateTime? dateTime, bool force = false, bool direction = false}) async {}

  @override
  Future<void> updateItems([bool force = false]) async {}

  @override
  void initState() {
    logout("SE initState() start");

    //TODO
    // デバッグ用
    // デバッグログはlogicのメソッドを変更すること
    debugSts = DebugSts();
    debugSts.isEncrypt = true;
    //debugSts.isLogPrint = false;
    debugSts.isRemoveGraphSettingButton = false;
    logout("debugSts create.");

    myPolicySet.isMultipleSelectionOn = widget.isMultipleSelectionOn;
    myPolicySet.multipleSelected = [];
    myPolicySet.multipleSelectDragPositionController = multipleSelectDragPositionController;
    myPolicySet.loadAiStreamController = loadAiStreamController;
    myPolicySet.copyStreamController = copyStreamController;
    myPolicySet.duplicateStreamController = duplicateStreamController;
    myPolicySet.readyToReconnectLinkStreamController = readyToReconnectLinkStreamController;

    myPolicySet.onCanvasForegroundHandler.add(myPolicySet.showLinkWidgetsOnCanvasForeground);
    myPolicySet.onLinkTapUpHandler = myPolicySet.onLinkTapUp;
    myPolicySet.onLinkLongPressStartHandler = myPolicySet.onLinkLongPressStart;

    // diagram_editor初期化
    diagramEditorContext = DiagramEditorContext(
      policySet: myPolicySet,
    );
    diagramEditorContextMiniMap = DiagramEditorContext.withSharedModel(
        diagramEditorContext,
        policySet: miniMapPolicySet);

    super.initState();

    // トップグラフ
    isTopGraph = widget.graphItemHyper == null;

    // スクロールをされたことを検知する。
    appBarScrollController.addListener((){
      appBarScrolled = true;
    });

    logout("SE initState() end");
  }

  @override
  Future<void> didPush() async {
    logout("SE didPush() start.");

    /// スキーマ取得
    final getSchemataFuture = getSchemata();

    // システムアカウントからOssとして公開されているファイルを取得
    systemOss = await loadOsses();

    // グラフ描画
    // タイムライン読み込み
    await _initGraph(getSchemataFuture);

    logout("SE didPush() end");
  }

  @override
  void didPop() {
    logout("SE didPop() start. graphItem=${graphItem}");
    super.didPop();
    logout("SE didPop() end.");
  }

  @override
  void didPushNext() {
    logout("SE didPushNext() start. graphItem=${graphItem}");
    super.didPushNext();
    logout("SE didPushNext() end.");
  }

  @override
  void didPopNext() {
    logout("SE didPopNext() start. graphItem=${graphItem}");
    super.didPopNext();
    logout("SE didPopNext() end.");
  }

  @override
  void dispose() {
    logout("SE dispose() start ");

    drawGraphController.close();
    loadAiStreamController.close();
    copyStreamController.close();
    duplicateStreamController.close();
    readyToReconnectLinkStreamController.close();

    multipleSelectDragPositionController.close();

    saveSelectedSchema();

    /// POP時の更新
    print("POP");
    appBarTitleController.close();
    // タイマオブジェクト破棄
    _semTimer?.cancel();

    super.dispose();
    logout("SE dispose() end ");
  }

  /// 周期タイマ処理
  @override
  void onTimer(Timer timer) {
    logout("SE onTimer() start ");

    // 親クラスのタイマー処理は実行しない

    logout("SE onTimer() end ");
  }

  /// 周期タイマ処理
  void onSemTimer() async {
    logout("SE onSemTimer() start ");

    // タイムラインアイテムロードと描画
    await drawGraph(false);

    /// タイマスタート
    if (mounted) {
      _semTimer = Timer(
        Duration(seconds: _syncInterval),
        onSemTimer,
      );
    }
    logout("SE onSemTimer() end ");
  }

  Future<void> _processElements(
    Iterable<GraphElement> elements,
    Future<void> callback(Iterable<GraphElement> elements),
  ) async {
    Timer? timer;
    Completer<void>? completer;

    Future<void> _callback() {
      void cleanup() {
        timer?.cancel();
        completer?.complete();
        timer = null;
        completer = null;
      }
      cleanup();

      if (elements.any((e) => !e.isLoaded)) return Future.value();

      completer = Completer<void>();
      timer = Timer(
        Duration.zero, () => callback(elements).whenComplete(cleanup),
      );
      return completer!.future;
    }

    _callback();
    await Future.wait((await pickToSync(elements)).map((e) async {
          await for (var _ in e.sync().handleError((_) {})) {
            await _callback();
          }
    }));
  }

  Future<void> _initGraph(Future<void> getSchemataFuture) async {
    try {
      if (isTopGraph) await _initTopGraph();
      else await _initHyper();

      logout("graphItem: ${graphItem!.id}");

      final _graph = await graphItem!.getOrCreateGraph();
      if (graphItem!.isChanged) graphItem!.sync().handleError((_) {});

      await getSchemataFuture;

      graphSchemata = timelineSchemata = await loadSemSchemata(graphItem!);

      // debug用
      /*
      logout("ontologySizes: ${graphSchemata.ontologies.length}");
      for (Ontology o in graphSchemata.ontologies) {
        logout("${o.label}");
      }
      logout("restrictionSizes: ${graphSchemata.restrictions.length}");
      for (Restriction r in graphSchemata.restrictions) {
        logout("${r.label} - ${r.definitions}");
      }
      logout("constraintsSizes: ${graphSchemata.constraints.length}");
      for (Constraint c in graphSchemata.constraints) {
        logout("${c.label} - ${c.conditions}");
      }
      */

      // ハイパーノードからpop後にスキーマを取得したいが、popを行っているクラスはTimelineStateBaseを継承していないため、
      // myPolicySet経由で呼び出せるようにする
      myPolicySet.getSchemata = getSchemata;

      // 描画用Canvas関連初期化
      myPolicySet.initForDraw(
        context,
        account,
        timeline,
        debugSts.isEncrypt,
        timelineSchemata!,
        graphSchemata,
        rootGraphSchemata,
        userPlrId,
        graphItem!,
        _graph,
        isTopGraph,
        editorKey,
        diagramEditorContext,
      );
      graph = _graph;
    } catch (e) {
      logout("Error = ${e.toString()}");
    }

    /// 描画・タイマスタート
    onSemTimer();
  }

  Future<void> _initTopGraph() async {
    var isSyncError = false;

    var topGraphItem = timeline.topGraphItem;
    if (topGraphItem == null) {
      await for (
        final _ in timeline.sync().handleError((_) => isSyncError = true)
      ) {
        if ((topGraphItem = timeline.topGraphItem) != null) break;
      }
    }
    if (topGraphItem != null) {
      var hasGraphSetting = topGraphItem.hasGraphSetting;
      if (!hasGraphSetting) {
        await for (
          final _ in topGraphItem.sync().handleError((_) => isSyncError = true)
        ) {
          if (hasGraphSetting = topGraphItem.hasGraphSetting) break;
        }
      }
      if (hasGraphSetting) {
        await _setTopGraphItem(topGraphItem);
        return;
      }
    }

    if (isSyncError || !isConnected) {
      // 同期に失敗、かつTopGraph、またはgraphSettingがない場合は
      // ダイアログを表示
      await _showNetworkConnectionRequiredDialog(context);
      Navigator.pop(context);
      return;
    }

    if (topGraphItem == null) {
      // RootGraphがまだ作られていないチャネルの場合、
      // AlertDialogを表示させる。
      if (await _showCreateNewGraphDialog(context)) {
        Navigator.pop(context);
        return;
      }
      topGraphItem = await createTopGraph(
        timeline, userPlrId, systemOss, debugSts.isEncrypt,
      );
    } else {
      makeGraphSetting(topGraphItem, systemOss, userPlrId, debugSts.isEncrypt);
    }
    await _setTopGraphItem(topGraphItem);
  }

  Future<void> _setTopGraphItem(Item topGraphItem) async {
    CustomStatePolicy.isLoadAi = false;
    loadAiStreamController.sink.add(false);
    showMessage(context,'Long-press the background to create a node.'.i18n);

    rootGraphSchemata = await loadSemSchemata(topGraphItem);
    rootGraphSetting = topGraphItem.graphSetting;

    graphItem = topGraphItem;
  }

  Future<bool> _showCreateNewGraphDialog(BuildContext context) async => (
    await showDialog<bool>(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Are you sure to create new graph?'.i18n,),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, true),
            child: Text('Cancel'.i18n),
          ),
          TextButton(
            onPressed: () =>Navigator.pop(context, false),
            child: Text('OK'.i18n),
          )
        ],
      ),
    )
  ) ?? true;

  Future<void> _showNetworkConnectionRequiredDialog(
    BuildContext context,
  ) => showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text('error'.i18n),
      content: Text('Network connection required'.i18n),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: Text('OK'.i18n),
        ),
      ],
    ),
  );

  Future<void> _initHyper() async {
    // ハイパーノードの場合
    graphItem = widget.graphItemHyper!;

    // あらかじめグラフアイテムの同期をしておく
    await graphItem!.syncSilently();

    // graphSettingの存在チェック
    if (!graphItem!.hasGraphSetting) {
      // 存在しない場合は、はじめてハイパーノードとして開いた場合

      // 親のグラフアイテムへのpeerを作成
      //TODO このpeerは、ハイパーノードをタブから開く実装をするときに使うはず
      //graphItem.addProperty("parent", widget.parentGraphItemHyper.node);

      // graphSettingを作成する。
      makeGraphSetting(
        graphItem!, systemOss, userPlrId, debugSts.isEncrypt,
      );
      //graphItem.syncSilently();

      // タイムラインノードにハイパーノードのpeerを作成
      //TODO このpeerは、ハイパーノードをタブから開く実装をするときに使うはず
      //timeline.addProperty("hyperGraphLink", graphItem.node);
      //timeline.syncSilently();
    }
    /* 親からコピーせず、選択したスキーマから参照
    else {
      graphSetting =
        graphItem!.firstPropertyModelOf<DataSetting>("graphSetting");
    }
    // 親グラフからgraphSettingの内容をコピーする
    if (graphSetting != null) {
      await copyOssFileFromParent(
        graphSetting, widget.parentGraphItemHyper!,
      );
    }
    */
  }

  /// タイムラインをロードしてグラフ描画する
  Future<void> drawGraph(bool isForce) async {
    logout("SE drawGraph() start");

    // 多重起動防止
    if (isDrawGraphing == true) {
      if (isForce) {
        int loopCount = 1;
        while (isDrawGraphing == true) {
          logout("drawGraph Waiting loop. count=${loopCount}");

          /// timeline取得中の場合(delay:1000msec)
          await Future.delayed(const Duration(milliseconds: 1000))
              .catchError((e, s) => showError(context, e, s));

          if (loopCount == 20) {
            logout("drawGraph Waiting loop timeout.");
            throw ("timeout.");
          }
          loopCount++;
        }
      } else {
        logout("SE drawGraph() end. nodraw. DrawGraph is still running.");
        return;
      }
    }
    if ((graph == null) || !mounted) return;

    isDrawGraphing = true;
    drawGraphController.sink.add(true);

    try {
      // タイムライン読み込み、読み込んだアイテムは描画対象であれば描画する
      //loadTimelineItem(timeline, context);

      // アイテム読み込みと描画(Peerを使う)
      await loadItemAndDraw();
      //drawLink();

      if (isFirstDrawTitle) {
        if (isTopGraph) appBarStackTitle = StackList<AppBarTitle>();
        // AppBarのタイトル
        // AppBarのタイトルのタイトル取得
        AppBarTitle tmp = getAppBarTitle();

        // それぞれのタイトルなどを取得する。
        appBarTitle.titleText = tmp.titleText;
        appBarTitle.itemId = tmp.itemId;
        appBarTitle.titleImage = tmp.titleImage;
        appBarTitle.types = tmp.types;

        print(appBarStackTitle);
        appBarStackTitle.push(appBarTitle);
        appBarTitleController.sink.add(appBarStackTitle);
        isFirstDrawTitle = false;
      }
    } catch (e) {
      logout("Error = ${e.toString()}");
    }
    isDrawGraphing = false;
    if (!drawGraphController.isClosed) drawGraphController.sink.add(false);

    logout("SE drawGraph() end");
  }

  /// タイムライン読み込み
  /// コールバックで描画ロジックの呼び出しも行う
  /*
  void loadTimelineItem(Timeline timeline, BuildContext context) async {
    logout("SE loadTimeLineItem() start.");

    // タイムライン読み込み中フラグ
    bool isPastGetting = true;
    bool isFutureGetting = true;

    // グラフアイテムの位置情報リスト
    List<Entity> posEntityList =
        graphItem.propertyModelsOf<Entity>("@node").toList();
    // グラフアイテムのリンク情報リスト
    List<Entity> linkEntityList =
        graphItem.propertyModelsOf<Entity>("@link").toList();

    DateTime now = DateTime.now();

    /// タイムラインアイテムロード、過去と未来を実行する
    /// 過去
    var pastNodes = timeline.timelineItemModelsByCount(now, 1000,
        direction: false, skipDeleted: false);
    //var pastNodes = timeline.recentTimelineItemModels(skipDeleted: false);
    pastNodes.handleError((e, s) {
      showError(context, e, s);
    }).listen(
      (event) {
        logout("Past listen start. Item count=${event.length}");
        event.forEach((item) {
          // アイテムチェックとノード描画
          checkItemForDraw(item, posEntityList);
        });
        // リンクリストの取得とリンクの描画
        getLinkListForDraw(linkEntityList);
        // 初回描画時のみ位置づけ
        if (isFirstDraw) {
          myPolicySet.positionView();
          isFirstDraw = false;
        }
        logout("Past listen end.");
      },
      cancelOnError: true,
    ).onDone(() {
      isPastGetting = false;
      logout("Past onDone.");
    });

      /// 未来
      var futureNodes = timeline.timelineItemModelsByCount(now, 1000,
          direction: true, skipDeleted: false);
      futureNodes.handleError((e, s) {
        showError(context, e, s);
      }).listen(
        (event) {
          logout("Future listen start. Item count=${event.length}");
          event.forEach((item) {
            // アイテムチェックとノード描画
            checkItemForDraw(item, posEntityList);
          });
          // リンクリストの取得とリンクの描画
          getLinkListForDraw(linkEntityList);
          // 初回描画時のみ位置づけ
          if (isFirstDraw) {
            myPolicySet.positionView();
            isFirstDraw = false;
          }
          logout("Future listen end.");
        },
        cancelOnError: true,
      ).onDone(() {
        isFutureGetting = false;
        logout("Future onDone.");
      });

    int loopCount = 1;
    while (isPastGetting || isFutureGetting) {
      //logout("loadtimeline Waiting loop. count=${loopCount}");
      // timeline取得中の場合(delay:200msec)
      await Future.delayed(const Duration(milliseconds: 200));
      if (loopCount == 100) {
        logout("loadTimeLineItem Waitingloop timeout.");
        throw ("timeout.");
      }
      loopCount++;
    }

    isDrawGraphing = false;
    drawGraphController.sink.add(false);

    logout("SE loadTimeLineItem end.");
  }
  */

  /// アイテム読み込みと描画
  Future<void> loadItemAndDraw() async {
    final schemataList = allSchemata(
      graphSchemata, rootGraphSchemata, timelineSchemata!, isTopGraph,
    );
    final schemata0 = schemataList.elementAtOrNull(0);
    final graphRelationClass0 = schemata0?.classOf(
      SemLinkModel.graphRelationClass,
    );

    await for (final elementResult in graph!.syncElements()) {
      if (!mounted) return;

      await synchronized(
        () => _processElements(elementResult.value.all, (elements) async {
            if (!mounted) return;

            // 削除確認
            // saveListと比較して削除されているノード、リンクを取得する
            final nowNodeIdList = <String>{};

            // リンク描画
            // 描画対象チェックはこのタイミングではできないので後続処理でやっている
            final semLinkList = <SemLinkModel>[];

            for (final element in elements) {
              final linkType = element.type;
              if (schemata0?.classOf(linkType)?.isDescendantOf(graphRelationClass0) != true) {
                // 削除チェック
                if (element.isDeleted) {
                  // 描画済みノードの削除
                  myPolicySet.removeDrawNode(element.id);
                } else if (element.node.isDeleted) {
                  deleteElement(graph!, element);
                } else {
                  RecordEntity? recordEntity;
                  RecordEntity? temp = RecordEntity.from(graphSchemata, element);
                  if (temp != null) {
                    recordEntity = temp;
                  } else {
                    temp = RecordEntity.from(rootGraphSchemata, element);
                  }
                  if (temp != null) {
                    recordEntity = temp;
                  } else {
                    recordEntity = RecordEntity.from(timelineSchemata!, element);
                  }
                  if (recordEntity != null) {
                    SemNodeModel model = SemNodeModel(userPlrId);
                    model.recordEntity = recordEntity;
                    model.posOffset = Offset(element.xCoordinate, element.yCoordinate);
                    // 描画ノードの追加または更新
                    myPolicySet.addDrawNode(model);
                  }
                }
                nowNodeIdList.add(element.id!);
              }
              if (linkType == null) return;

              for (final schemata in schemataList) {
                // graphSchemataを参照し、存在しないプロパティの場合は表示対象外とする
                if (schemata.classOf(linkType)?.isDescendantOf(schemata.classOf(SemLinkModel.graphRelationClass)) == true) {
                  semLinkList.add(SemLinkModel(element, schemata));
                  break;
                }
              }
            }

            // 削除対象ノードリストを作成
            final deleteNodeIdList = saveNodeIdList;
            deleteNodeIdList.removeAll(nowNodeIdList);

            // nodeListのセーブ。あとで、削除確認のための比較用
            saveNodeIdList = nowNodeIdList;

            // 削除対象ノードの削除
            for (final id in deleteNodeIdList) {
              myPolicySet.removeDrawNode(id);
            }

            // 初回描画時のみ位置づけ
            if (isFirstDraw) {
              myPolicySet.positionView();
              isFirstDraw = false;
            }
            await myPolicySet.addDrawLink(semLinkList);
        }),
      );
    }
  }

  /// リンク描画
  /// 描画対象チェックはこのタイミングではできないので後続処理でやっている
  /*
  void drawLink() {
    //logout("SE drawLink start.");
    // グラフアイテムのリンク情報リスト
    List<Entity> linkEntityList =
        graphItem.propertyModelsOf<Entity>("@link").toList();
    List<SemLinkModel> semLinkList = [];
    for (Entity e in linkEntityList) {
      SemLinkModel linkModel = SemLinkModel(e, graphSchemata);
      semLinkList.add(linkModel);
    }
    myPolicySet.addDrawLink(semLinkList);
    //logout("SE drawLink end.");
  }
  */

  /// 取得したアイテムを描画対象かチェック
  /// 対象の場合は描画ロジックをコール
  /// タイムライン読み込みのコールバック（listen）から呼ばれる
  /*
  void checkItemForDraw(Item item, List<Entity> posEntityList) {
    //logout("SE checkItemForDraw start.");
    // 削除チェック
    if (item.isDeleted) {
      // 描画済みノードの削除
      myPolicySet.removeDrawNode(item.id);
    } else {
      // 描画対象アイテムかチェック
      // 存在しない場合は描画対象外
      Entity posEntity = posEntityList.firstWhere(
          (e) => e.firstLiteralValueOf("node") == item.id,
          orElse: () => null);
      if (posEntity == null) {
        // グラフノードのノード（@nodeプロパティ）には登録されていない
        logout("Item not included in graphItem. id=${item.id}");
        //logout("SE checkItemForDraw end.");
        return;
      }

      // RecordEntity生成によるオントロジー存在チェック
      // 存在しない場合は描画対象外
      RecordEntity recordEntity = RecordEntity.from(graphSchemata, item);
      if (recordEntity == null) {
        // クラスがgraphSettingに含まれない場合は表示対象ではない
        logout(
            "ItemClass not inclueded in ontology. id=${item.id} class=${item.type}");
        //logout("SE checkItemForDraw end.");
        return;
      }

      // 描画用モデル作成
      SemNodeModel model =
          SemNodeModel(recordEntity, posEntity, userPlrId, userProfileMap);

      // 描画ノードの追加または更新
      myPolicySet.addDrawNode(model);
    }
    //logout("SE checkItemForDraw end.");
  }
  */

  /// リンクリストの取得と描画ロジックのコール
  /// 描画対象チェックはこのタイミングではできないので後続処理でやっている
  /*
  void getLinkListForDraw(List<Entity> linkEntityList) {
    //logout("SE getLinkListForDraw start.");
    List<SemLinkModel> semLinkList = [];
    for (Entity e in linkEntityList) {
      SemLinkModel linkModel = SemLinkModel(e, graphSchemata);
      semLinkList.add(linkModel);
    }
    myPolicySet.addDrawLink(semLinkList);
    //logout("SE getLinkListForDraw end.");
  }
  */

  /// appBarの表示項目を取得
  AppBarTitle getAppBarTitle() {
    AppBarTitle appBarTitle = AppBarTitle();

    String? graphType = graphItem!.type;

    appBarTitle.types = graphItem!.type ?? "";
    appBarTitle.itemId = graphItem!.id ?? "";
    if (graphType == "RootGraph") {
      // RootGraphの場合
    } else if (graphType == "M") {
      // Mクラスの場合はcntの内容を出す

      //RecordEntity re = RecordEntity.from(graphSchemata, graphItem);
      RecordEntity? re = RecordEntity.from(timelineSchemata!, graphItem!);
      if (re != null) {
        FormatResult formatResult = re.format();
        if (formatResult.hasMmdata) {
          // cnt属性のデータを取得(リストの最初の要素)
          List<Mmdata> mmdataList = formatResult.mmdatasById("cnt");
          Mmdata mmdata = mmdataList.first;

          // Mクラスがコメントのみの場合
          if (mmdata.isLiteral && mmdata.text.isNotEmpty) {
            appBarTitle.titleText = (mmdata.asLiteral.text.value ?? "");
          }

          // タイトルや画像を追加した場合
          if (mmdata.isFile) {
            String text = "";
            // ファイルノード時のタイトル部
            if (mmdata.asFile.text.isNotEmpty &&
              mmdata.asFile.text.defaultValue != "") {
              text = mmdata.asFile.text.defaultValue ?? "";
            }
            // ファイルノード時のコメント部
            if (mmdata.asFile.comment.isNotEmpty &&
              mmdata.asFile.comment.defaultValue != "") {
              if (text.isEmpty) {
                text = mmdata.asFile.comment.defaultValue ?? "";
              } else {
                text += ", " + (mmdata.asFile.comment.defaultValue ?? "");
              }
            }

            appBarTitle.titleText = text;

            // ファイルノード時のサムネイル（アイコン）
            var thumbnailData = mmdata.asFile.thumbnailData;
            if (thumbnailData != null && thumbnailData.isNotEmpty) {
              appBarTitle.titleImage = thumbnailData;
            }
          }
        }
      }
    } else {
      // 上記以外は、クラス名（format指定）を出す
      //RecordEntity re = RecordEntity.from(graphSchemata, graphItem);
      RecordEntity? re = RecordEntity.from(timelineSchemata!, graphItem!);
      if (re != null) {
        FormatResult formatResult = re.format();
        String text = "";
        // クラス名
        if (formatResult.typeLabel != null) {
          text = formatResult.typeLabel!;
        }
        // クラス名の後ろにフォーマット結果を追加
        if (formatResult.text?.isNotEmpty == true) {
          text += ' ' + formatResult.text!;
        }
        appBarTitle.titleText = text;
      }
    }
    return appBarTitle;
  }

  /// Record Entity
  /*
  @override
  RecordEntity getRecordEntity(Item item) {
    return RecordEntity.from(graphSchemata, item);
  }

  @override
  RecordEntity createRecordEntity([String typeId]) {
    return RecordEntity.create(graphSchemata, typeId ?? "Event");
  }
  */

  @override
  Widget build(BuildContext context) {
    logout("SE build() start");
    String channelName = (timeline as Channel).name?.defaultValue;

    // appbar
    final appBar = AppBar(
      //title: Text(channelName + appBarTitle),
      leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          tooltip: 'Back'.i18n,
          color: Colors.black,
          onPressed: () {
            if (isFirstDrawTitle == false) appBarStackTitle.pop();
            Navigator.pop(context);
        }
      ),
      title: Container(
        height: 30,
        child:
             StreamBuilder(
                  stream: appBarTitleController.stream,
                  builder:
                      (BuildContext context, AsyncSnapshot<StackList<AppBarTitle>> snapshot) {
                    logout("appBar Stream Builder");

                    // データを取得する部分、水平方向に文字をスクロールさせる
                    if (snapshot.hasData) {
                      // スクロールをしていなければ、最後尾までスクロールする。
                      //　レイアウトされるまで、最後尾のポジションが不明なので定期的に監視する
                      Timer.periodic( const Duration(milliseconds: 100), (timer) {
                        if(appBarScrollController.hasClients && !appBarScrolled) {
                          appBarScrollController.jumpTo(appBarScrollController.position.maxScrollExtent);
                          // jumpToでappBarScrolledがtrueになるため、ここでfalseに戻す
                          appBarScrolled = false;
                          timer.cancel();
                        }
                      });

                      print("現在："+appBarTitle.titleText);

                      return Row(
                        children: [
                          //　RootGraphの部分はスクロールせずに表示させる。
                      ConstrainedBox(
                              constraints: const BoxConstraints(maxWidth: 300.0),
                              child:SingleChildScrollView(
                                  // controller: appBarScrollController,
                                  scrollDirection: Axis.horizontal,
                                  child: BreadCrumb(
                                    items: <BreadCrumbItem>[
                                        BreadCrumbItem(
                                          content: InkWell(
                                              child: Text(
                                                channelName,
                                                overflow: TextOverflow.clip,
                                                maxLines: 1,
                                              ),
                                              // RootGraphの場合はクリックさせない。
                                              onTap: (appBarTitle.types == "") || (appBarTitle.types == "RootGraph") ? null : (){
                                                Navigator.of(context).popUntil((route){
                                                  bool isPop = route.settings.name == null;
                                                  if (!isPop) appBarStackTitle.pop();
                                                  return isPop;
                                                });
                                              },
                                            ),
                                          ),

                                    ],),
                                  )),
                          const SizedBox(width: 15.0),
                      // スクロールを行う箇所
                      Expanded(
                          child: SingleChildScrollView(
                            controller: appBarScrollController,
                            scrollDirection: Axis.horizontal,
                          child:Row(
                            children: [
                              BreadCrumb(
                                items: snapshot.data!.all.map((e) =>
                                    BreadCrumbItem(
                                      content: ConstrainedBox(
                                        constraints: const BoxConstraints(maxWidth: 300.0),
                                        child:InkWell(
                                          child: Text(
                                            e.titleText,
                                            overflow: TextOverflow.clip,
                                            maxLines: 1,
                                          ),
                                          onTap: (appBarTitle.itemId == e.itemId) || (e.titleText.isEmpty) ? null : (){
                                            Navigator.of(context).popUntil((route){
                                              bool isPop = (e.itemId == route.settings.name) || (route.settings.name == null);
                                              if (!isPop) appBarStackTitle.pop();
                                              return isPop;
                                            });
                                          },
                                        ),
                                      ),
                                    )
                                ).toList(),
                                divider: const Icon(Icons.chevron_right),
                              )
                            ],
                          ),
                        )
                      ),
                        ],
                      );

                    }else{
                      return Container();
                    }
                  }),
      ),
      actions: <Widget>[
        // プログレスインジケータ
        TimelineAppBarProgress(drawGraphController.stream),
        // デバッグ用GraphSettting削除ボタン
        if (debugSts.isRemoveGraphSettingButton) const SizedBox(width: 5.0),
        if (debugSts.isRemoveGraphSettingButton)
          IconButton(
              icon: const Icon(Icons.clear),
              onPressed: () async {
                logout("clearGraphSetting start.");
                // 確認ダイアログ
                var ret = await showConfirmDialog(
                    context, "graphSetting削除確認", 'デバッグ機能です。削除後はこのグラフから抜けてください');
                if (ret) {
                  // graphSetting削除
                  graphItem!.removePropertiesOf("graphSetting");
                }
              }),
        const SizedBox(width: 5.0),
        // graphSetting設定
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
        IconButton(
          icon: const Icon(Icons.settings),
          onPressed: () async {
            // DummySchema作成
            DataSetting? graphSettingEntity =
                graphItem!.firstPropertyModelOf<DataSetting>("graphSetting");
            if (graphSettingEntity == null) return;

            DummySchema _commonSchema =
                DummySchema.fromSchema(graphSettingEntity);
            DummySchema _rootSchema =
                DummySchema.fromSchema(rootGraphSetting!);
            // 親グラフまたはチャネルのOSS設定取得
            DataSetting? parentDataSetting;
              // チャネル設定を返す
              parentDataSetting = (timeline as Channel).channelDataSetting;
              if (parentDataSetting == null) {
                logout("parentDataSetting is null.");
              } else {
                await parentDataSetting.syncSilently();
              }

            await showDialog(
                context: context,
                builder: (ctx) => SemOssSelectDialog.fromDummySchema(
                      root: widget.root,
                      schema: _commonSchema,
                      rootschema: _rootSchema,
                      isReadOnly: false,
                      parent: parentDataSetting,
                      schemata: selectedSchemata,
                      isParentChannel:
                          widget.graphItemHyper == null ? true : false,
                      onDone: (o, r, co, t, c, s) async {
                        logout("ontologyDialog onDone");
                        _commonSchema.update(null, o, r, co, t, c, s);
                        _commonSchema.updateSchema(graphSettingEntity);
                        logout("ontologyDialog call syncSilently");
                        await graphSettingEntity.syncSilently();

                        // 再読み込み、強制モード（編集中フラグによらず読み込みを行う）
                        logout("ontologyDialog call drawGraph");
                        await drawGraph(true);
                      },
                    ));
            logout("ontologyDialog close");
          },
        ),
    const SizedBox(width: 5.0),
          ],
        )
      ],
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(APPBARBOTTOM_FONTSIZE * MediaQuery.textScalerOf(context).scale(1.0)),
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Padding(
            padding:
            const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 1.0),
            child: Row(
              children: <Widget>[
                const Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      ]
                  ),
                  flex: 2,
                ),
                /// スタイルシート
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                if ((timeline is! Channel) || (selectedSchema != null)) {
                      showSchemaAndStyleSheetSelectDialog();
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.only(left: 1, right: 1),
                    child: TimelineSchemaAndStyleSheet(
                      selectedSchemataAndStyleSheet.stream,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    myPolicySet.context = context;

    logout("SE build() end");
    final mediaQuery = MediaQuery.of(context);
    return MediaQuery(
      data: mediaQuery.copyWith(textScaler: TextScaler.noScaling,),
      child: Scaffold(
        appBar: appBar,
        body: StreamBuilder(
          stream: hasMenuStream,
          builder: (_, snapshot) => switch (selectedSchemata?.timelineState) {
            null => FutureBuilder(
              future: Future.delayed(const Duration(seconds: 3)),
              builder: (_, snapshot) => switch (snapshot.connectionState) {
                ConnectionState.done => Align(
                  alignment: Alignment.center,
                  child: Text(
                    'Retrieving the schema.\nPlease wait a while...'.i18n,
                  ),
                ),
                _ => emptyWidget,
              },
            ),
            TimelineState.Disabled => Align(
              alignment: Alignment.center,
              child: Text(
                'Timeline is disabled.\nPlease select another schema.'.i18n,
              ),
            ),
            _ => getEditor(),
          },
        ),
      ),
      /*
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        tooltip: "add node",
        onPressed: () {
          myPolicySet.addNode(null);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      */
    );
  }

  // 編集画面(diagram_editor)
  Widget getEditor() {
    logout("got a schema");
    return (SafeArea(
      child: Stack(
        children: [
          Container(color: Colors.grey),
          Positioned(
            child: Padding(
              padding: const EdgeInsets.all(2),
              child: DiagramEditor(
                key: editorKey,
                diagramEditorContext: diagramEditorContext,
              ),
            ),
          ),
          Positioned(
            right: 16,
            top: 16,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isMiniMapVisible = !isMiniMapVisible;
                    });
                  },
                  child: Container(
                    color: Colors.grey[300],
                    child: Padding(
                      padding: const EdgeInsets.all(4),
                      child: Text(
                          isMiniMapVisible ? 'hide minimap'.i18n : 'show minimap'.i18n),
                    ),
                  ),
                ),
                Visibility(
                  visible: isMiniMapVisible,
                  child: Container(
                    width: 320,
                    height: 240,
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                        color: Colors.black,
                        width: 2,
                      )),
                      child: DiagramEditor(
                        diagramEditorContext: diagramEditorContextMiniMap,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 12, left: 24),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  /*
                  OptionIcon(
                    color: Colors.grey.withOpacity(0.7),
                    iconData: isOptionsVisible ? Icons.menu_open : Icons.menu,
                    shape: BoxShape.rectangle,
                    onPressed: () {
                      setState(() {
                        isOptionsVisible = !isOptionsVisible;
                      });
                    },
                  ),
                  const SizedBox(width: 8),
                  Visibility(
                    visible: isOptionsVisible,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                   */
                  OptionIcon(
                    tooltip: 'reset view'.i18n,
                    color: Colors.grey.withOpacity(0.7),
                    iconData: Icons.replay,
                    //onPressed: () => myPolicySet.resetView(),
                    onPressed: () => myPolicySet.positionView(),
                  ),
                  // const SizedBox(width: 8),
                        /*
                        OptionIcon(
                          tooltip: 'delete all',
                          color: Colors.grey.withOpacity(0.7),
                          iconData: Icons.delete_forever,
                          onPressed: () => myPolicySet.removeAll(context),
                        ),
                        const SizedBox(width: 8),
                        const SizedBox(width: 8),
                        */
                  // OptionIcon(
                  //   tooltip: myPolicySet.isMultipleSelectionOn
                  //       ? 'cancel multiselection'.i18n
                  //       : 'enable multiselection'.i18n,
                  //   color: Colors.grey.withOpacity(0.7),
                  //   iconData: myPolicySet.isMultipleSelectionOn
                  //       ? Icons.group_work
                  //       : Icons.group_work_outlined,
                  //   onPressed: () {
                  //     setState(() {
                  //       if (myPolicySet.isMultipleSelectionOn) {
                  //         myPolicySet.turnOffMultipleSelection();
                  //       } else {
                  //         myPolicySet.turnOnMultipleSelection();
                  //       }
                  //     });
                  //   },
                  // ),
                  const SizedBox(width: 8),
                  OptionIcon(
                    tooltip: 'select all'.i18n,
                    color: Colors.grey.withOpacity(0.7),
                    iconData: Icons.select_all,
                    onPressed: () =>
                      setState(() {
                        myPolicySet.selectAll();
                      }),
                  ),
                  StreamBuilder<bool>(
                      stream: copyStreamController.stream,
                      builder: (_, snapshot) {
                        return Visibility(
                          // visible: myPolicySet.canCopyMultiple,
                          visible: false,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: OptionIcon(
                              tooltip: 'copy'.i18n,
                              color: Colors.grey.withOpacity(0.7),
                              iconData: Icons.copy,
                              onPressed: () =>
                                  setState(() {
                                    CustomStatePolicy.isCutAndPaste = false;
                                    bool ret = myPolicySet.multipleCopySelected();
                                    //Toast表示
                                    if(ret) {
                                      showMessage(context, 'The node has been memorized in the clipboard.'.i18n);
                                    }
                                  }),
                            ),
                          ),
                        );
                      }
                  ),

                  StreamBuilder<bool>(
                      stream: copyStreamController.stream,
                      builder: (_, snapshot) {
                        return Visibility(
                          // visible: myPolicySet.canCutMultiple,
                          visible: false,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: OptionIcon(
                              tooltip: 'cut and paste'.i18n,
                              color: Colors.grey.withOpacity(0.7),
                              iconData: Icons.cut,
                              onPressed: () =>
                                  setState(() {
                                    CustomStatePolicy.isCutAndPaste = true;
                                    bool ret = myPolicySet.multipleCopySelected();
                                    //Toast表示
                                    if(ret) {
                                      showMessage(context, 'The node has been memorized in the clipboard.'.i18n);
                                    }
                                  }),
                            ),
                          ),
                        );
                      }
                  ),

                  StreamBuilder<bool>(
                      stream: duplicateStreamController.stream,
                      builder: (_, snapshot) {
                        return Visibility(
                          visible: myPolicySet.canDuplicateMultipleCopied,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: OptionIcon(
                              tooltip: 'discard'.i18n,
                              color: Colors.grey.withOpacity(0.7),
                              iconData: Icons.content_paste_off,
                              onPressed: () =>
                                  setState(() {
                                    myPolicySet.resetClipboardGraph();
                                  }),
                            ),
                          ),
                        );
                      }
                  ),
                  /*
                      ],
                    ),
                  ),
                   */
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}

/// タイムライン読み込み中のプログレスサークル
class TimelineAppBarProgress extends StatelessWidget {
  final Stream<bool> loadTimelineStream;

  TimelineAppBarProgress(this.loadTimelineStream, {super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: loadTimelineStream,
        builder: (context, snapShot) {
          if (snapShot.hasData && snapShot.data!) {
            return const Center(child: const CircularProgressIndicator());
          } else {
            return Container();
          }
        });
  }
}

/// AppBarのタイトル
class AppBarTitle {
  String titleText = "";
  Uint8List? titleImage;
  String itemId = "";
  String types = "";


  @override
  String toString() {
    return titleText + ' ' +itemId+' ' +types;
  }
}

/// デバッグ状態
class DebugSts {
  /// 暗号化フラグ（暗号化する場合true）
  bool isEncrypt = false;

  /// ログフラグ（ログ出力する場合true）今は機能しないのでコメント化
  //bool isLogPrint = false;

  /// graphSetting削除ボタン（ボタン表示時はtrue）
  bool isRemoveGraphSettingButton = false;
}
