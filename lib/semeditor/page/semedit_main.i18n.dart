import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      {
        'en_us': 'error',
        'ja_jp': 'エラー',
      } +
      {
        'en_us': 'Network connection required',
        'ja_jp': 'ネットワーク接続が必要です',
      } +
      {
        'en_us': 'OK',
        'ja_jp': 'OK',
      } +
      {
        'en_us': 'Are you sure to create new graph?',
        'ja_jp': 'グラフを新規作成します。よろしいですか？',
      } +
      {
        'en_us': 'Cancel',
        'ja_jp': 'キャンセル',
      } +
      {
        'en_us': 'Long-press the background to create a node.',
        'ja_jp': 'ノードを作るには背景を長押しして下さい。',
      } +
      {
        'en_us': 'Retrieving the schema.\nPlease wait a while...',
        'ja_jp': 'スキーマの取得中です。少しお待ちください。',
      } +
      {
        'en_us': 'Timeline is disabled.\nPlease select another schema.',
        'ja_jp': 'タイムラインが無効です。\n他のスキーマを選択してください。',
      } +
      {
        'en_us': 'reset view',
        'ja_jp': '表示をリセット',
      } +
      {
        'en_us': 'enable multiselection',
        'ja_jp': '複数選択モードにする',
      } +
      {
        'en_us': 'cancel multiselection',
        'ja_jp': '通常モードにする',
      } +
      {
        'en_us': 'select all',
        'ja_jp': '全選択',
      } +
      {
        'en_us': 'copy',
        'ja_jp': 'コピー',
      } +
      {
        'en_us': 'duplicate',
        'ja_jp': '複製',
      } +
      {
        'en_us': 'cut and paste',
        'ja_jp': '切り取り',
      } +
      {
        'en_us': 'share',
        'ja_jp': '共有',
      } +
      {
        'en_us': 'discard',
        'ja_jp': '破棄',
      } +
      {
        'en_us': 'Back',
        'ja_jp': '戻る',
      } +
      {
        'en_us': 'The node has been memorized in the clipboard.',
        'ja_jp': 'ノードをクリップボードに記憶しました。',
      } +
      {
        'en_us': 'show minimap',
        'ja_jp': 'ミニマップ表示',
      } +
      {
        'en_us': 'hide minimap',
        'ja_jp': 'ミニマップ非表示',
      };

  String get i18n => localize(this, t);
}
