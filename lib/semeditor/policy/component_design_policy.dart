import 'package:diagram_editor/diagram_editor.dart';
import 'package:flutter/material.dart';

import '../widget/component/rect_component.dart';

mixin MyComponentDesignPolicy implements ComponentDesignPolicy {
  @override
  Widget? showComponentBody(ComponentData componentData) {
    switch (componentData.type) {
      case 'rect':
        return RectBody(componentData: componentData);
      case 'body':
        return RectBody(componentData: componentData);
      default:
        return null;
    }
  }
}
