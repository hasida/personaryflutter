import 'package:i18n_extension/i18n_extension.dart';
import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      meTranslation +
      {
        'en_us': 'Confirm',
        'ja_jp': '確認',
      } +
      {
        'en_us': 'Are you sure to delete this link?',
        'ja_jp': 'このリンクを削除してよろしいですか?',
      } +
      {
        'en_us': 'edit',
        'ja_jp': '編集',
      } +
      {
        'en_us': 'delete',
        'ja_jp': '削除',
      } +
      {
        'en_us': 'switch',
        'ja_jp': '入れ替え',
      } +
      {
        'en_us': 'connect',
        'ja_jp': '接続',
      } +
      {
        'en_us': 'reconnect',
        'ja_jp': '再接続',
      } +
      {
        'en_us': 'A link will be created. To specify the destination node, either choose an existing node or create a new node by long-pressing the background.',
        'ja_jp': 'リンクを作ります。終点として既存のノードを選ぶか、背景を長押しして新ノードを作って下さい。',
      } +
      {
        'en_us': 'A link will be reconnected. To specify the destination node, either choose an existing node or create a new node by long-pressing the background.',
        'ja_jp': 'リンクを再接続します。終点として既存のノードを選ぶか、背景を長押しして新ノードを作って下さい。',
      };

  String get i18n => localize(this, t);
}
