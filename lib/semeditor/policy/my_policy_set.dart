import 'package:diagram_editor/diagram_editor.dart';
import 'canvas_policy.dart';
import 'canvas_widgets_policy.dart';
import 'component_design_policy.dart';
import 'component_policy.dart';
import 'component_widgets_policy.dart';
import 'custom_policy.dart';
import 'init_policy.dart';
import 'link_attachment_policy.dart';
import 'link_widgets_policy.dart';
import 'load_plr_policy.dart';
import 'my_canvas_control_policy.dart';
import 'my_link_control_policy.dart';
//import 'my_link_joint_control_policy.dart';

class MyPolicySet extends PolicySet
    with
        MyInitPolicy,
        MyCanvasPolicy,
        MyComponentPolicy,
        MyComponentDesignPolicy,
        MyLinkControlPolicy,
        //MyLinkJointControlPolicy, // リンクの接合部機能は使わないためコメント化
        MyLinkWidgetsPolicy,
        MyLinkAttachmentPolicy,
        MyCanvasWidgetsPolicy,
        MyComponentWidgetsPolicy,
        //
        MyCanvasControlPolicy,
        //
        LoadPlrPolicy,
        //
        CustomStatePolicy,
        CustomBehaviourPolicy {}
