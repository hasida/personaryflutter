import 'dart:async';
import 'dart:math';
import 'package:collection/collection.dart' show IterableExtension, SetEquality;
import 'package:diagram_editor/diagram_editor.dart';
import 'package:flutter/material.dart';
import 'package:personaryFlutter/util/item_color.dart';
import 'package:personaryFlutter/util/position.dart';
import 'package:plr_ui/plr_ui.dart';

import '../model/sem_node_model.dart';
import '../model/sem_link_model.dart';
import '../data/custom_component_data.dart';
import '../logic/sem_logic.dart';
import 'custom_policy.dart';

const double labelFontSize = 15;

mixin LoadPlrPolicy implements PolicySet, CustomStatePolicy {

  /// diagram_editor側へ初期データ設定
  void initForDraw(
      BuildContext context,
      Account account,
      Timeline timeline,
      bool isNodeEncrypt,
      Schemata timelineSchemata,
      Schemata graphSchemata,
      Schemata rootGraphSchemata,
      String plrId,
      Item graphItem,
      Graph graph,
      bool isTopGraph,
      GlobalKey editorKey,
      DiagramEditorContext diagramEditorContext,
  ) {
    //logout("initForDraw start.");
    // コンテキスト
    this.context = context;
    // PLR共通
    this.account = account;
    this.timeline = timeline;
    this.isNodeEncrypt = isNodeEncrypt;
    this.timelineSchemata = timelineSchemata;
    this.graphSchemata = graphSchemata;
    this.rootGraphSchemata = rootGraphSchemata;
    this.plrId = plrId;
    this.graphItem = graphItem;
    this.graph = graph;
    this.isTopGraph = isTopGraph;
    this.editorKey = editorKey;
    this.diagramEditorContext = diagramEditorContext;

    //logout("initForDraw end.");
  }

  /// ノード描画
  void addDrawNode(SemNodeModel model) {
    String? nodeId = model.recordEntity?.entity?.id;
    //logout("addDrawNode(policy) start.: ${nodeId}");
    if (nodeId == null) return;

    // ドラッグ中にノードを更新すると、予期せぬノードが編集中になってしまうため、ドラッグ中はノードを更新しない。
    if(isDragging) return;

    if (syncingList.contains(nodeId) || syncingList.contains(parseId(nodeId))) return;

    //旧型式("#"付き)と新形式("#"なし)の片方が描画されていればもう片方は描画しない
    String parseNodeId = parseId(nodeId);
    if (drawMap.containsKey(parseNodeId)) {
      if (CustomStatePolicy.multipleCopiedSelectedNodeId.contains(parseNodeId)) {
        CustomStatePolicy.multipleCopiedSelectedNodeId.remove(parseNodeId);
        CustomStatePolicy.multipleCopiedSelectedNodeId.add(nodeId);
        CustomStatePolicy.multipleCopiedNodeData[nodeId] =
        CustomStatePolicy.multipleCopiedNodeData[parseNodeId];
        CustomStatePolicy.multipleCopiedNodeData.remove(parseNodeId);
      }
      drawMap[nodeId] = drawMap[parseNodeId]!;
      drawMap.remove(parseNodeId);
    }

    ComponentData componentData;
    // すでに描画されているかチェック
    if (drawMap.containsKey(nodeId)) {
      // 描画されているので、描画済みのComponentData取得
      componentData = canvasReader.model.getComponent(drawMap[nodeId]!);
      var myComponentData = componentData.data as MyComponentData;

      // 選択されている場合はデータ更新・描画しない
      if (myComponentData.isEditing) {
        logout("addDrawNode: node selected. No action. id=${nodeId}");
        return;
      }

      // パラメータから各種値を設定
      myComponentData.itemEntity = model.recordEntity;
      myComponentData.isMe = model.isMe;
      myComponentData.dispClass = model.dispClass;
      myComponentData.dispCntList = model.dispCntList;

      myComponentData.initialize();
      calcBorderSize(componentData,posOffset: model.posOffset);

      // ノードのサイズ計算
      calcNodePosition(componentData, model.posOffset!, context);
      componentData.updateComponent();
      updateLinkLabelPosition(componentData.id);
      canvasWriter.model.moveComponentToTheBack(componentData.id);
      canvasWriter.model.updateComponentLinks(drawMap[nodeId]!);

      logout("addDrawNode: update id=${nodeId} class=${model.dispClass} cnt=${componentData.data.dispCnt} offset=${model.posOffset} size=${componentData.size}");
    } else {
      // 描画されていないので新規追加
      componentData = ComponentData(
        size: const Size(120, 72),
        minSize: const Size(80, 64),
        position: model.posOffset!,
        data: MyComponentData(
          color: ItemColor.classColor(),
          borderColor: Colors.black,
          borderWidth: 1.0,
          secondBorderWidth: 0.3,
          itemEntity: model.recordEntity,
          isMe: model.isMe,
          dispClass: model.dispClass,
          dispCntList: model.dispCntList,
        ),
        type: 'rect',
      );

      // nodeMapに追加
      drawMap[nodeId] = componentData.id;

      (componentData.data as MyComponentData).initialize();
      calcBorderSize(componentData,posOffset: model.posOffset);

      // ノードのサイズ計算
      calcNodePosition(componentData, model.posOffset!, context);

      // 描画
      canvasWriter.model.addComponent(componentData);
      canvasWriter.model.moveComponentToTheBack(componentData.id);
      logout("addDrawNode: new id=${nodeId} class=${model.dispClass} cnt=${componentData.data.dispCnt} offset=${model.posOffset} size=${componentData.size}");
    }
    //logout("addDrawNode(policy) end.");
  }

  /// ノード削除
  void removeDrawNode(String? nodeId) {
    if (nodeId == null) return;

    //logout("removeDrawNode(policy) start.: ${nodeId}");
    if (!drawMap.containsKey(nodeId)) {
      // すでに削除済みなので何もしない
      logout("removeDrawNode: node removed yet. No action. id=${nodeId}");
      return;
    }


    // 描画を削除
    canvasWriter.model.removeComponent(drawMap[nodeId]!);
    // drawMapから削除
    drawMap.remove(nodeId);
    logout("removeDrawNode: remove. id=${nodeId}");
  }

  /// リンクの描画
  /// ノードの描画がすべて終わってからコールする
  Future<void> addDrawLink(List<SemLinkModel> linkEntityList) async {
    //logout("addDrawLink start.");
    // ドラッグ中にリンクを更新すると、予期せぬリンクが編集中になってしまうため、ドラッグ中はリンクを更新しない。
    if(isDragging) return;

    // コンポーネントリスト
    Iterable<ComponentData> componentDataList = canvasReader.model.getAllComponents().values;

    Map<String, ComponentData> linkLabelMap = {};

    for (ComponentData componentData in componentDataList) {
      if ((componentData.data as MyComponentData).isLinkLabel) {
        for (LinkTypeProperty linkType in (componentData.data as MyComponentData).typeList!) {
          linkLabelMap[linkType.elementId] = componentData;
        }
      }
    }

    List<ComponentData> deleteList = [];

    //リンクラベルにあってPLRにない種別はリンクラベルから削除する
    for (ComponentData componentData in componentDataList) {
      if ((componentData.data as MyComponentData).isLinkLabel) {
        Set<String> connectionSet = {};
        for (Connection con in componentData.connections) {
          connectionSet.add((canvasReader.model.getComponent(con.otherComponentId).data as MyComponentData).nodeId!);
        }
        Set<String> plrSet = {};
        List<LinkTypeProperty> typeList = [];
        typeList += (componentData.data as MyComponentData).typeList!;
        for (LinkTypeProperty linkType in typeList) {
          if (linkType.syncingCount != 0) continue;

          GraphElement? element = getElement(graph, linkType.elementId);
          if (element != null) {
            plrSet.addAll(getSourceIdList(element).toSet());
            String? targetId = getTargetId(element);
            if (targetId != null) plrSet.add(targetId);
          } else {
            deleteList.add(componentData);
            continue;
          }
          if (!const SetEquality().equals(connectionSet, plrSet)) {
            (componentData.data as MyComponentData).typeList!.remove(linkType);
            if ((componentData.data as MyComponentData).typeList!.isEmpty) deleteList.add(componentData);
            componentData.updateComponent();
          }
        }
      }
    }

    for (ComponentData componentData in deleteList) {
      canvasWriter.model.removeComponent(componentData.id);
    }

    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);

    // リンクの配置
    for (SemLinkModel item in linkEntityList) {
      if (syncingList.contains(item.linkEntity.id)) continue;

      correctArg(item);
      Set<ComponentData> componentSet = {};
      Set<String> sourceComponentIdSet = {};
      String? targetComponentId;
      List<Item> shareItemList = [];
      // リンクの始点
      for (Item source in item.linkEntity.propertyModelsOf<Item>(SemLinkModel.arg1Property)) {
        ComponentData? sourceComponent = componentDataList.firstWhereOrNull(
                (element) => (element.data as MyComponentData).nodeId == source.id);
        if (sourceComponent == null || !drawMap.containsValue(sourceComponent.id)) {
          shareItemList.add(source);
        } else {
          componentSet.add(sourceComponent);
          sourceComponentIdSet.add((sourceComponent.data as MyComponentData).nodeId!);
        }
      }

      // リンクの終点
      for (Item target in item.linkEntity.propertyModelsOf<Item>(SemLinkModel.arg2Property)) {
        ComponentData? targetComponent = componentDataList.firstWhereOrNull(
                (element) => (element.data as MyComponentData).nodeId == target.id);
        if (targetComponent == null || !drawMap.containsValue(targetComponent.id)) {
          shareItemList.add(target);
        } else {
          componentSet.add(targetComponent);
          targetComponentId = (targetComponent.data as MyComponentData).nodeId;
        }
      }

      //リンクLの端点がいずれも出現していない場合は、共有されない
      if (componentSet.isEmpty) continue;
      //自動共有（同じリンク(の出現)を持つグラフを開いた際には、リンクの端点のノードがなければ自動的に共有する。）
      for (int i = 0; i < shareItemList.length; i++) {
        SemNodeModel model = SemNodeModel(plrId);
        RecordEntity? recordEntity;
        Offset position = centerPosition(componentSet.first.position, componentSet.first.size);
        await addElementAndSync(graph, shareItemList[i], OccurrenceType.shared, coordinates: Point(position.dx + 20 * (i+1) + 1, position.dy + 20 * (i+1) + 1));

        GraphElement? element = getElement(graph, shareItemList[i].id!);
        await element!.syncAttributesSilently();
        await element.node.syncSilently();
        for (Schemata schemata in schemataList) {
          recordEntity ??= RecordEntity.from(schemata, element);
        }
        model.recordEntity = recordEntity;
        model.posOffset = Offset(element.xCoordinate, element.yCoordinate);
        addDrawNode(model);
        ComponentData component = canvasReader.model.getComponent(drawMap[shareItemList[i].id]!);
        componentSet.add(component);
      }

      Set<String> componentIdSet = {};

      bool isDraw = false;
      //リンクラベルがPLRと異なるかどうか確認
      if (linkLabelMap.containsKey(item.linkEntity.id)) {
        ComponentData linkLabel  = linkLabelMap[item.linkEntity.id]!;
        for (Connection con in linkLabel.connections) {
          ComponentData componentData = canvasReader.model.getComponent(con.otherComponentId);
          componentIdSet.add((componentData.data as MyComponentData).nodeId!);
        }
        for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
          if (item.linkEntity.id == linkType.elementId) {
            String? targetId = linkType.targetNodeId;
            Set<String> sourceIdSet = componentIdSet.difference({targetId});
            if (const SetEquality().equals(sourceIdSet, sourceComponentIdSet) && targetId == targetComponentId && item.dispText == linkType.type) {
              isDraw = true;
            }
          }
        }
      }

      if (isDraw) continue;


      List<LinkTypeProperty> diffTypeList = [LinkTypeProperty(item.linkEntity.id!, type: item.dispText, isSymmetric: item.isSymmetric, linkEntity: item.linkEntity, targetNodeId: targetComponentId)];

      for (LinkTypeProperty linkType in CustomStatePolicy.linkElementMap.keys) {
        if (linkType.elementId == item.linkEntity.id) {
          CustomStatePolicy.linkElementMap[diffTypeList.first] = CustomStatePolicy.linkElementMap[linkType];
          CustomStatePolicy.linkElementMap.remove(linkType);
          break;
        }
      }

      //想定より端点が少ないリンクは削除する
      if (componentSet.length < 2) {
        deleteLink(graph, timeline, item.linkEntity.id!, true);
        continue;
      }

      // PLRと異なるので新規追加、又は更新
      ComponentData linkLabel = drawLink(componentSet.toList(), diffTypeList, true)!;

      //リンクラベルの更新
      setLinkLabelPosition(linkLabel);
      drawCutLink();
      for (Connection link in linkLabel.connections) {
        canvasWriter.model.updateLink(link.connectionId);
      }
      canvasWriter.model.moveComponentToTheFront(linkLabel.id);
      linkLabel.updateComponent();

      // logout("addDrawLink: draw. source=$n1Id target=$n2Id text=${item.dispText} id=${linkId}");
    // }));
    }
    //logout("addDrawLink(policy) end.");
  }


  // ComponentData取得
  ComponentData? getComponentData(String? nodeId) {
    // すでに描画されているかチェック
    if (drawMap.containsKey(nodeId)) {
      // 描画されているので、描画済みのComponentData取得
      return canvasReader.model.getComponent(drawMap[nodeId]!);
    }
    return null;
  }
}
