import 'package:diagram_editor/diagram_editor.dart';
import 'package:flutter/material.dart';
import 'package:personaryFlutter/semeditor/data/custom_component_data.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';

import '../../widget/property_tooltip.dart';
import '../data/custom_link_data.dart';
import '../logic/sem_logic.dart';
import '../widget/option_icon.dart';
import 'custom_policy.dart';
import 'link_widgets_policy.i18n.dart';
import 'my_link_control_policy.dart';

const double labelFontSize = 15;

mixin MyLinkWidgetsPolicy implements LinkWidgetsPolicy, CustomStatePolicy, MyLinkControlPolicy {
  // @override
  // List<Widget> showWidgetsWithLinkData(
  //     BuildContext context, LinkData linkData) {
  //   double linkLabelSize = 32;
  //   /*
  //   var linkStartLabelPosition = labelPosition(
  //     linkData.linkPoints.first,
  //     linkData.linkPoints[1],
  //     linkLabelSize / 2,
  //     false,
  //   );
  //   */
  //
  //   var sourceId = linkData.sourceComponentId;
  //   var targetId = linkData.targetComponentId;
  //   ComponentData source = canvasReader.model.getComponent(sourceId);
  //   ComponentData target = canvasReader.model.getComponent(targetId);
  //   String _subject = (source.data as MyComponentData).dispCnt!.isNotEmpty ? source.data.dispCnt : source.data.dispClass;
  //   String _object = (target.data as MyComponentData).dispCnt!.isNotEmpty ? target.data.dispCnt : target.data.dispClass;
  //
  //   var linkMiddleLabelPosition = labelPosition(
  //     centerPosition(source.position, source.size),
  //     centerPosition(target.position, target.size),
  //     linkLabelSize / 2,
  //     linkData.data.endLabel,
  //   );
  //
  //   // 改行対応
  //   _subject = _subject.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ");
  //   _object = _object.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ");
  //
  //   // 用例省略
  //   if (_subject.length > 15) {
  //     _subject = _subject.substring(0, 14) + "...";
  //   }
  //
  //   if (_object.length > 15) {
  //     _object = _object.substring(0, 14) + "...";
  //   }
  //
  //   List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
  //   SchemaClass? schemaClass;
  //   for (Schemata schemata in schemataList) {
  //     schemaClass = schemata.classOf((linkData.data as MyLinkData).linkEntity!.type);
  //     if (schemaClass != null) break;
  //   }
  //
  //   if ((linkData.data as MyLinkData).linkEntity == null ) {
  //     return [];
  //   }
  //
  //   // 現状、End側のみリンクのプロパティ表示に使用している。
  //   // 将来的には、start側を「Invalid」表示用に使用すればよい
  //   // もちろん、表示座標計算を正しく修正する必要はある。
  //   return [
  //     /*
  //     label(
  //       linkStartLabelPosition,
  //       linkData.data.startLabel,
  //       linkLabelSize,
  //     ),
  //     */
  //     label(
  //       linkMiddleLabelPosition,
  //       linkData.data.endLabel,
  //       linkLabelSize,
  //       linkData,
  //         schemaClass,
  //       _subject,
  //       _object
  //     ),
  //   ];
  // }

  //最前面にリンクオプションを表示
  List<Widget> showLinkWidgetsOnCanvasForeground(c) {
    //全てのリンクに対するProviderを用意して監視する。
    return canvasReader.model.getAllLinks().values.map((LinkData link) {

      return ChangeNotifierProvider<LinkData>.value(
        value: link,
        child: Consumer<LinkData>(
          builder: (context, linkData, child) =>
            //選択されているリンクなら、リンクオプションを表示する。
            (selectedLinkId == linkData.id)
                ? showLinkOptions(context, multipleSelectedLink)
                : Container()
        )
      );
    }).toList();
  }

  Widget showLinkOptions(BuildContext context, List<String?> linkDataIdList) {
    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
    LinkData linkData = canvasReader.model.getLink(selectedLinkId!);
    MyLinkData myLinkData = (linkData.data as MyLinkData);
    if (myLinkData.linkEntity == null) return Container();
    SchemaClass? schemaClass;
    for (Schemata schemata in schemataList) {
      schemaClass = schemata.classOf(myLinkData.linkEntity!.type);
      if (schemaClass != null) break;
    }

    ComponentData linkLabel = getLinkLabel(linkData);

    if (schemaClass == null) return Container();
    List<LinkData> linkDataList = [];
    for (String? id in linkDataIdList) {
      if (id != null && canvasReader.model.linkExist(id)) linkDataList.add(canvasReader.model.getLink(id));
    }

    if (linkDataList.length == 0) return Container();
    Offset nPos = canvasReader.state.toCanvasCoordinates((canvasReader.model.getLink(selectedLinkId!).data as MyLinkData).tapPosition);

    return Positioned(
      left: nPos.dx,
      top: nPos.dy,
      child: Column(
        children: [
      StreamBuilder<bool>(
        stream: readyToReconnectLinkStreamController.stream,
        builder: (_, snapshot) {
          return Visibility(
            visible: !isReadyToReconnectLink,
            child: Row(
              children: [
                Visibility(
                  visible: linkLabel.connections.length - multipleSelectedLink.length > 1,
                  child:
                  OptionIcon(
                    color: Colors.red.withOpacity(0.7),
                    iconData: Icons.delete_forever,
                    tooltip: 'delete'.i18n,
                    onPressed: () async {
                      showSelected(); //debug
                      // 削除確認ダイアログ
                      var ret = await showConfirmDialog(context, 'Confirm'.i18n,
                          'Are you sure to delete this link?'.i18n);
                      if (ret) {
                        List<ComponentData> prevComponentList = [];

                        for (Connection con in linkLabel.connections) {
                          prevComponentList.add(canvasReader.model.getComponent(con.otherComponentId));
                        }

                        List<ComponentData> nextComponentList = [];
                        nextComponentList += prevComponentList;
                        //リンクの線分を削除
                        for (LinkData linkData in linkDataList) {
                          multipleSelectedLink.remove(linkData.id);
                          for (String componentId in [linkData.sourceComponentId, linkData.targetComponentId]) {
                            if (!(canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) {
                              ComponentData componentData = canvasReader.model.getComponent(componentId);
                              MyLinkData data = (linkData.data as MyLinkData);
                              deleteBranch(graph,timeline, data.linkEntity!, (componentData.data as MyComponentData).itemEntity!.entity!,syncDelete: syncDelete, syncAdd: syncAdd);
                              nextComponentList.remove(canvasReader.model.getComponent(componentId));
                            }
                          }
                        }

                        LinkData linkData = linkDataList.first;
                        MyLinkData myLinkData = (linkData.data as MyLinkData);

                        LinkTypeProperty? linkTypeProperty;
                        for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
                          if (linkType.elementId == myLinkData.linkEntity!.id) linkTypeProperty = linkType;
                        }

                        ComponentData? prevLinkLabel = drawLink(prevComponentList, [linkTypeProperty!], false);
                        ComponentData nextLinkLabel = drawLink(nextComponentList, [linkTypeProperty], true)!;

                        syncingList.add(linkTypeProperty.elementId);
                        linkTypeProperty.syncingCount += 1;

                        List<ComponentData> linkLabelList = [nextLinkLabel];
                        if (prevLinkLabel != null) linkLabelList.add(prevLinkLabel);
                        for (ComponentData linkLabel in linkLabelList) {
                          SchemaClass? schemaClass = getSchemaClass(linkTypeProperty);
                          setHistory(schemaClass);
                          setLinkLabelPosition(linkLabel);
                          linkLabel.updateComponent();
                          for (Connection link in linkLabel.connections) {
                            canvasWriter.model.updateLink(link.connectionId);
                          }
                        }
                        drawCutLink();
                        // リンクオプション消去
                        hideAllLinkHighlights();
                      }
                      showSelected(); //debug
                    },
                  ),
                ),
                Visibility(
                  visible: multipleSelectedLink.length == 1,
                  child:
                  const SizedBox(width: 8),
                ),
                Visibility(
                    visible: multipleSelectedLink.length == 1,
                    child:
                    OptionIcon(
                      color: Colors.grey.withOpacity(0.7),
                      iconData: Icons.compare_arrows,
                      tooltip: 'reconnect'.i18n,
                      onPressed: () async {
                        isReadyToReconnectLink = true;
                        selectedLinkLabel = linkLabel;
                        readyToReconnectLinkStreamController.sink.add(true);
                        //Toast表示
                        showMessage(context, 'A link will be reconnected. To specify the destination node, either choose an existing node or create a new node by long-pressing the background.'.i18n);
                      },
                    )
                ),
              ],
            ),
          );
        }),
        ],
      ),
    );
  }

  Widget label(Offset position, String label, double size, LinkData linkData, SchemaClass? schemaClass, String source, String target) {
    return Positioned(
      key: (linkData.data as MyLinkData).key,
      left: position.dx,
      top: position.dy,
      child: PropertyTooltip.maybe(
        schemaClass?.tooltip?.valueFor(currentLanguage),
        source,
        target,
        key: Key("link_label_${linkData.id}"),
        child: GestureDetector(
          onLongPressStart: (LongPressStartDetails details) {
            // ラベルをクリックした場合、リンクをクリックした処理を行う
            // アイコン表示場所としてリンクラベル下の位置を取得
            // onLinkTapUp = linkを押したときの処理
            Offset linkIcon = position.translate(0, size * canvasReader.state.scale);
            LongPressStartDetails detail = new LongPressStartDetails(globalPosition: linkIcon, localPosition: details.localPosition + position);
            onLinkLongPressStart(linkData.id, detail);
          },
          onTapUp: (TapUpDetails details) {
            Offset linkIcon = position.translate(0, size * canvasReader.state.scale);
            TapUpDetails detail = new TapUpDetails(globalPosition: linkIcon, localPosition: details.localPosition + position, kind: details.kind);
            onLinkTapUp(linkData.id, detail);
          },
          child: Container(
            //padding: EdgeInsets.all(3.0),
            color: Colors.grey[600]!.withOpacity(0.7),
            child: Center(
              child: Text(
                label,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: labelFontSize * canvasReader.state.scale, //rep 10->15
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /*
  Offset labelPosition(
    Offset point1,
    Offset point2,
    double labelSize,
    bool left,
  ) {
    var normalized = VectorUtils.normalizeVector(point2 - point1);

    return canvasReader.state.toCanvasCoordinates(point1 -
        Offset(labelSize, labelSize) +
        normalized * labelSize +
        VectorUtils.getPerpendicularVectorToVector(normalized, left) *
            labelSize);
  }
  */

// @override
  // Widget showOnLinkTapWidget(
  //     BuildContext context, LinkData linkData, Offset tapPosition) {
  //   return Positioned(
  //     left: tapPosition.dx,
  //     top: tapPosition.dy,
  //     child: Row(
  //       children: [
  //         GestureDetector(
  //           onTap: () {
  //             canvasWriter.model.removeLink(linkData.id);
  //           },
  //           child: Container(
  //               decoration: BoxDecoration(
  //                 color: Colors.red.withOpacity(0.7),
  //                 shape: BoxShape.circle,
  //               ),
  //               width: 32,
  //               height: 32,
  //               child: Center(child: Icon(Icons.close, size: 20))),
  //         ),
  //         const SizedBox(width: 8),
  //         GestureDetector(
  //           onTap: () {
  //             showEditLinkDialog(
  //               context,
  //               linkData,
  //             );
  //           },
  //           child: Container(
  //               decoration: BoxDecoration(
  //                 color: Colors.grey.withOpacity(0.7),
  //                 shape: BoxShape.circle,
  //               ),
  //               width: 32,
  //               height: 32,
  //               child: Center(child: Icon(Icons.edit, size: 20))),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
