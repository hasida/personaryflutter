import 'package:flutter/material.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:plr_ui/plr_ui.dart';

import '../logic/sem_logic.dart';
import '../model/sem_node_model.dart';
import 'custom_policy.dart';
import 'load_plr_policy.dart';
import 'component_policy.dart';

mixin MyCanvasPolicy implements CanvasPolicy, CustomStatePolicy, LoadPlrPolicy, MyComponentPolicy, CustomBehaviourPolicy {
  Account get account;

  // セマンティックエディタのBuildContext
  late BuildContext context;

  @override
  onCanvasTap() {
    turnOffMultipleSelection();
    hideAllLinkHighlights();
    isReadyToConnect = false;
    isReadyToBranch = false;
    isReadyToReconnectLink = false;
    selectedComponentId = null;

    showSelected(); //debug
  }

  @override
  onCanvasLongPressStart(LongPressStartDetails details) {
    Offset offset =
        canvasReader.state.fromCanvasCoordinates(details.localPosition);
    addNode(offset);
  }

  /*
  RecordData cloneNewNode(RecordEntity recordEntity){
    for (var prop in recordEntity.props.entries){
      print("key:${prop.key} value:${prop.value}");
      for(var record in prop.value.records){
        print("key:${prop.key} value:${prop.value}");
        record = cloneNewNode(record as RecordEntity);
      }
    }
  }
   */

  /// ノード追加
  void addNode(Offset? offset) async {
    showSelected(); //debug

    // 座標が指定されていない場合
    if (offset == null) {
      offset = const Offset(200.0, 200.0);
    }

    RecordEntity? recordEntity;
    SemNodeModel model = SemNodeModel(plrId);
    model.posOffset = Offset(offset.dx, offset.dy);

    // メッセージダイアログ暫定対策
    recordEntity = await RecordEntity.createMessage(graphSchemata, "");
    RecordEntity? rootRecordEntity = RecordEntity.create(rootGraphSchemata, "Event");
    RecordEntity? othersRecordEntity = RecordEntity.create(timelineSchemata, "Event");
    AddNodeType? addNodeType;
    recordEntity = await showDialog(
      context: context,
      builder: (context) => EntityDialog(
        account, EntityDialogSettings(), timeline, recordEntity!, false, true,
        onSyncFinished: (value) async {
          addPosition(model, isNodeEncrypt, graph, OccurrenceType.created);
        },
        isCheckSemEditor: true,
        isCheckNew: true,
        isTopGraph : isTopGraph,
        isShowShareButton: canShareMultipleCopied,
        onPressShare: () {
          addNodeType = AddNodeType(ButtonType.SHARE, "");
        },
        isShowDuplicateButton: canDuplicateMultipleCopied,
        onPressDuplicate: () {
          addNodeType = AddNodeType(ButtonType.DUP, "");
        },
        othersRecordEntity: othersRecordEntity,
        rootRecordEntity: rootRecordEntity,
      ),
    );

    // if (recordEntity == null && addNodeType == null) {
    //   return;
    // }

    // DUPLICATE
    if (addNodeType?.mType == ButtonType.DUP) {
      duplicateMultipleCopied(offset);
    }

    if (addNodeType?.mType == ButtonType.SHARE) {
      shareMultipleCopied(offset);
    }

    if (addNodeType?.mType == ButtonType.MESSAGE) {
      // メッセージクラス作成
      // recordEntity =
      //    await RecordEntity.createMessage(graphSchemata, addNodeType.message);
      recordEntity =
      await RecordEntity.createMessage(timelineSchemata, addNodeType!.message);
      await recordEntity!.save(timeline, false, onSyncFinished: (value) async {
        addPosition(model, isNodeEncrypt, graph, OccurrenceType.created);
      });
    }

    // 位置情報EntityNode
    if (recordEntity != null) {
      // 同期
      recordEntity.entity!.syncSilently().then((value) {
        timeline.syncSilently().then((value) => null);
      });

      // ここでRecordEntityをつくりなおさないと、
      // SemNodeModel内の処理でmmdataの値がとれない
      // 原因は後で調査したほうがいいかも
      RecordEntity? temp = RecordEntity.from(graphSchemata, recordEntity.entity!);
      if (temp != null) {
        recordEntity = temp;
      } else {
        temp = RecordEntity.from(rootGraphSchemata, recordEntity.entity!);
      }
      if (temp != null) {
        recordEntity = temp;
      } else {
        recordEntity =
            RecordEntity.from(timelineSchemata, recordEntity.entity!);
      }

      //Entity posEntity =
      //    addPosition(graphItem, recordEntity.entity, offset, isNodeEncrypt);

      //SemNodeModel model =
          //SemNodeModel(recordEntity, plrId, offSet: Offset(offset.dx, offset.dy));

      model.recordEntity = recordEntity;

      await graphItem.syncSilently();
      timeline.addProperty(SemNodeModel.graphLinkProperty, graphItem.node);
      timeline.syncSilently();

      // ノードの追加
      addDrawNode(model);

      String? nodeId = model.recordEntity?.entity?.id;

      if (drawMap.containsKey(nodeId)) {
        hideAllHighlights();
        if (isReadyToConnect) {
          connectComponents(selectedComponentId, drawMap[nodeId]!).then((connected) {
            if (connected) {
              isReadyToConnect = false;
              selectedComponentId = null;
            }
          });
        }

        if (isReadyToBranch) {
          createBranch(drawMap[nodeId]!);
          hideAllLinkHighlights();
        }
        if (isReadyToReconnectLink) {
          drawReconnect(drawMap[nodeId]!);
          hideAllLinkHighlights();
        }
      }
      // リンクオプション消去
      hideLinkOption();

    } else {
      // ダイアログでキャンセルした場合
      // isReadyConnectを解除する
      isReadyToConnect = false;
    }
    showSelected(); //debug
  }
}
enum ButtonType {
  DUP,
  SHARE,
  MESSAGE,
  OTHER
}

class AddNodeType {
  final ButtonType mType;
  final String message;

  AddNodeType(this.mType, this.message);
}
