import 'package:i18n_extension/i18n_extension.dart';
import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static var t = Translations.byText('en_us') +
      meTranslation +
      {
        'en_us': 'Confirm',
        'ja_jp': '確認',
      } +
      {
        'en_us': 'Are you sure to delete this node?',
        'ja_jp': 'このノードを削除してよろしいですか?',
      } +
      {
        'en_us': 'delete',
        'ja_jp': '削除',
      } +
      {
        'en_us': 'edit',
        'ja_jp': '編集',
      } +
      {
        'en_us': 'internal graph',
        'ja_jp': '内部グラフ',
      } +
      {
        'en_us': 'copy node',
        'ja_jp': 'コピー',
      } +
      {
        'en_us': 'copy',
        'ja_jp': 'コピー',
      } +
      {
        'en_us': 'cut and paste',
        'ja_jp': '切り取り',
      } +
      {
        'en_us': 'connect',
        'ja_jp': '接続',
      } +
      {
        'en_us': 'change',
        'ja_jp': '変更',
      } +
      {
        'en_us': 'The node has been memorized in the clipboard.',
        'ja_jp': 'ノードをクリップボードに記憶しました。',
      } +
      {
        'en_us': 'A link will be created. To specify the destination node, either choose an existing node or create a new node by long-pressing the background.',
        'ja_jp': 'リンクを作ります。終点として既存のノードを選ぶか、背景を長押しして新ノードを作って下さい。',
      } +
      {
        'en_us': 'Generated by AI',
        'ja_jp': 'AIによる生成',
      } +
      {
        'en_us': 'Generating',
        'ja_jp': '生成中',
      } +
      {
        'en_us': 'Generated failed',
        'ja_jp': '生成に失敗しました',
      };

  String get i18n => localize(this, t);
}
