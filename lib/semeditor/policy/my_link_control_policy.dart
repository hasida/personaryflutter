import 'package:diagram_editor/diagram_editor.dart';
import 'package:flutter/material.dart';
import 'package:personaryFlutter/semeditor/policy/link_widgets_policy.i18n.dart';
import 'package:personaryFlutter/semeditor/data/custom_component_data.dart';
import 'package:personaryFlutter/semeditor/data/custom_link_data.dart';
import 'package:plr_ui/plr_ui.dart';

import '../dialog/edit_linktype_dialog.dart';
import '../logic/sem_logic.dart';
import '../model/sem_link_model.dart';
import 'custom_policy.dart';

mixin MyLinkControlPolicy implements LinkPolicy, CustomStatePolicy {
  late bool isLinkLabel;
  @override
  onLinkTapUp(String id, TapUpDetails details) async {
    isLinkLabel = isCheckLinkLabel(id);
    bool isNotIntend = notIntendCheck(id, details);
    //　意図しない地点がタップされた場合に無視する
    if (isNotIntend) return;
    hideAllHighlights();
    isReadyToReconnectLink = false;
    isReadyToBranch = false;
    if (isLinkLabel) {
      ComponentData linkLabel = canvasReader.model.getComponent(id);
      (linkLabel.data as MyComponentData).isEditing = true;
      List<LinkData> linkDataList = [];
      MyComponentData myLinkLabelData = (linkLabel.data as MyComponentData);
      selectedLinkLabel = linkLabel;
      LinkTypeProperty? linkTypeProperty;
      for (LinkTypeProperty linkType in myLinkLabelData.typeList!) {
        if (CustomStatePolicy.isSelectedTypeMap[linkType.elementId] == true) {
          linkTypeProperty = linkType;
        }
      }

      if (linkTypeProperty!.isWriting) return;
      List<Connection> connectionList = [];
      bool isSelectedLink = false;
      for (Connection con in linkLabel.connections) {
        connectionList.add(con);
        isSelectedLink |= multipleSelectedLink.contains(con.connectionId);
      }

      String? targetNodeId = linkTypeProperty.targetNodeId;
      String dispText = linkTypeProperty.type!;
      bool isSymmetric = linkTypeProperty.isSymmetric!;
      Entity? linkEntity = linkTypeProperty.linkEntity;
      for (Connection con in connectionList) {
        ComponentData componentData =  canvasReader.model.getComponent(con.otherComponentId);
        LinkData linkData = canvasReader.model.getLink(con.connectionId);
        canvasWriter.model.removeLink(linkData.id);
        multipleSelectedLink.remove(linkData.id);
        String? linkId;
        if ((componentData.data as MyComponentData).nodeId == targetNodeId) {
          linkId = connectLink(linkLabel, componentData, dispText, isSymmetric, linkEntity);
        } else {
          linkId = connectLink(componentData, linkLabel, dispText, isSymmetric, linkEntity);
        }
        if (linkId != null) {
          if ((linkData.data as MyLinkData).isHighlightVisible) {
            highlightLink(linkId, (linkData.data as MyLinkData).tapPosition);
          }
        }
      }
      drawCutLink();
      //リンク線分を選択した状態である場合は選択した種別に変更する
      if (isSelectedLink) {
        return;
      }
      for (Connection con in linkLabel.connections) {
        LinkData linkData = canvasReader.model.getLink(con.connectionId);
        linkDataList.add(linkData);
      }

      if (isReadyToConnect) {
        bool isComplete = createBranch(selectedComponentId!);
        if (!isComplete) return;
        selectedComponentId = null;
        isReadyToConnect = false;
        hideAllLinkHighlights();
        return;
      }

      List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
      bool isBranch = linkLabel.connections.length != 2;
      List<String>? historyList = await getHistory();
      // ダイアログ表示
      SchemaClass? schemaClass = await showEditLinkTypeDialog(context, schemataList, historyList, dispText, isBranch ? isSymmetric: null,

          onPressDelete: (BuildContext context) async {
            showSelected();//debug
            // 削除確認ダイアログ
            var ret = await showConfirmDialog(context, 'Confirm'.i18n,
                'Are you sure to delete this link?'.i18n);
            if (ret) {
              List<ComponentData> componentList = [];

              for (Connection con in linkLabel.connections) {
                componentList.add(canvasReader.model.getComponent(con.otherComponentId));
              }

              //リンクの線分を削除
              for (LinkData linkData in linkDataList) {
                multipleSelectedLink.remove(linkData.id);
              }

              LinkData linkData = linkDataList.first;
              MyLinkData myLinkData = (linkData.data as MyLinkData);

              LinkTypeProperty? linkTypeProperty;

              for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
                if (linkType.elementId == myLinkData.linkEntity!.id!) linkTypeProperty = linkType;
              }

              //リンクラベル再描画
              ComponentData? prevLinkLabel = drawLink(componentList, [linkTypeProperty!], false);
              CustomStatePolicy.isSelectedTypeMap.remove(linkTypeProperty.elementId);
              syncingList.add(linkTypeProperty.elementId);
              if (prevLinkLabel != null) {
                setLinkLabelPosition(prevLinkLabel);
                prevLinkLabel.updateComponent();
                for (Connection link in prevLinkLabel.connections) {
                  canvasWriter.model.updateLink(link.connectionId);
                }
              }

              drawCutLink();

              SchemaClass? schemaClass = getSchemaClass(linkTypeProperty);
              setHistory(schemaClass);

              // PLR
              deleteLink(graph, timeline, myLinkData.linkEntity!.id!, true, syncDelete: syncDelete);
              if (CustomStatePolicy.copyGraph?.id == graph.id) CustomStatePolicy.linkElementMap.remove(linkTypeProperty);
              Navigator.pop(context);
            }
        },
        onPressSwitch: isSymmetric ? null :() {
          showSelected(); //debug
          ComponentData? sourceComponent;
          ComponentData? targetComponent;

          MyComponentData myData = (linkLabel.data as MyComponentData);
          for (Connection con in linkLabel.connections) {
            String otherNodeId = (canvasReader.model.getComponent(con.otherComponentId).data as MyComponentData).nodeId!;
            if (otherNodeId != targetNodeId) {
              sourceComponent = canvasReader.model.getComponent(drawMap[linkTypeProperty!.targetNodeId]!);
              targetComponent = canvasReader.model.getComponent(con.otherComponentId);
              linkTypeProperty.targetNodeId = otherNodeId;
            }
          }
          syncingList.add(linkTypeProperty!.elementId);
          //同じ向きの種別が複数になる場合、1個にまとめる
          for (LinkTypeProperty linkType in myData.typeList!) {
            if (linkTypeProperty.elementId != linkType.elementId
                && linkTypeProperty.type == linkType.type
                && linkTypeProperty.targetNodeId== linkType.targetNodeId
            ) {
              deleteLink(graph, timeline, linkTypeProperty.elementId, true, syncDelete: syncDelete);
              List<LinkTypeProperty> diffTypeList = [linkTypeProperty];
              drawLink([sourceComponent!, targetComponent!], diffTypeList, false);
              linkLabel.updateComponent();
              return;
            }
          }

          // PLR
          linkTypeProperty.isWriting = true;
          reverseLink((linkDataList.first.data as MyLinkData).linkEntity!, false, syncDelete: syncDelete).then((_) {
            linkTypeProperty!.isWriting = false;
          });

          setAngle(linkLabel, linkTypeProperty.elementId);
          linkLabel.updateComponent();
          SchemaClass? schemaClass = getSchemaClass(linkTypeProperty);
          setHistory(schemaClass);
          hideAllLinkHighlights();
          },
        onPressConnect: !isSymmetric ? null :(BuildContext context) async {
        isReadyToBranch = true;
        selectedLinkLabel = linkLabel;
        showSelected();
        //Toast表示
        showMessage(context, 'A link will be created. To specify the destination node, either choose an existing node or create a new node by long-pressing the background.'.i18n);
        },
      );

      setHistory(schemaClass);

      Schemata? schemata;
      for (Schemata sc in schemataList) {
        if (sc.classOf(schemaClass?.id) != null)  {
          schemata = sc;
        }
      }

      if (schemaClass != null) {
        linkTypeProperty.isWriting = true;
        // 描画
        String type = schemaClass.id;
        bool nowIsSymmetric = schemata!.classOf(type)!.isDescendantOf(schemata.classOf(SemLinkModel.symmetricRelationClass));

        // テキスト
        String text = schemata.classOf(type) == null
            ? type
            : schemata.classOf(type)!.label!.defaultValue;

        MyComponentData myComponentData = (linkLabel.data as MyComponentData);

        linkTypeProperty.linkEntity = await updateLinkEntity(linkTypeProperty.elementId);
        isSymmetric = schemata.classOf(linkTypeProperty.linkEntity!.type)!.isDescendantOf(schemata.classOf(SemLinkModel.symmetricRelationClass));

        // ラベルのテキスト変更
        for (LinkTypeProperty linkType in myComponentData.typeList!) {
          if ((isSymmetric && linkType.type == text)
          || (!isSymmetric && !nowIsSymmetric && linkType.type == text && linkType.targetNodeId == targetNodeId)) {
            hideAllLinkHighlights();
            linkTypeProperty.isWriting = false;
            return;
          }
        }

        Entity? targetNode =  isSymmetric
        //シンメトリックから非シンメトリックに変更する際は矢印の向きはランダムでよい
            ? (canvasReader.model.getComponent(drawMap[getSourceIdList(linkTypeProperty.linkEntity!).first]!).data as MyComponentData).itemEntity!.entity
            : (canvasReader.model.getComponent(drawMap[getTargetId(linkTypeProperty.linkEntity!)]!).data as MyComponentData).itemEntity!.entity;

        linkDataList = [];
        for (Connection con in linkLabel.connections) {
          linkDataList.add(canvasReader.model.getLink(con.connectionId));
        }

        // 現在のリンク削除と作成
        for (LinkData linkData in linkDataList) {
          ComponentData sourceComponent = canvasReader.model.getComponent(linkData.sourceComponentId);
          ComponentData targetComponent = canvasReader.model.getComponent(linkData.targetComponentId);
          canvasWriter.model.removeLink(linkData.id);
          String newLinkId = connectLink(sourceComponent, targetComponent, text, nowIsSymmetric, (linkData.data as MyLinkData).linkEntity)!;
          if (selectedLinkId == linkData.id) {
            selectedLinkId = newLinkId;
          }
          multipleSelectedLink.remove(linkData.id);
          multipleSelectedLink.add(newLinkId);
        }

        linkTypeProperty.type = text;
        linkTypeProperty.isSymmetric = nowIsSymmetric;

        //シンメトリック種別と非シンメトリック種別を交替した場合
        bool isChangeLink = isSymmetric != nowIsSymmetric;
        syncingList.add(linkTypeProperty.elementId);

        // PLR
        changeLinkClass(linkTypeProperty.linkEntity!, schemaClass, syncDelete: syncDelete, isChangeLink: isChangeLink, nodeEntity: targetNode, nowIsSymmetric: nowIsSymmetric).then((_) {
          linkTypeProperty!.isWriting = false;
        });

        if (isChangeLink) {
          if (nowIsSymmetric) {
            linkTypeProperty.targetNodeId = null;
          } else {
            setAngle(linkLabel, linkTypeProperty.elementId, targetNodeId: targetNode!.id!);
          }
        }

        calcLinkLabelSize(linkLabel, context);
        setLinkLabelPosition(linkLabel);
        linkLabel.updateComponent();
        for (Connection con in linkLabel.connections) {
          canvasWriter.model.updateLink(con.connectionId);
        }
        hideAllLinkHighlights();
        drawCutLink();
      } else {
        if (!isReadyToBranch) hideAllLinkHighlights();
      }
    } else {
      selectedComponentId = null;
      isReadyToConnect = false;
      canvasWriter.model.hideAllLinkJoints();
      canvasWriter.model.showLinkJoints(id);

      turnHighlightLink(id, canvasReader.state.fromCanvasCoordinates(details.localPosition));

      showSelected(); //debug
    }
  }

  @override
  onLinkLongPressStart(String id, LongPressStartDetails details) {
    isLinkLabel = isCheckLinkLabel(id);
    bool isNotIntend = notIntendCheck(id, details);
    //　意図しない地点がタップされた場合に無視する
    if (isNotIntend) return;
    if (isLinkLabel) {
      hideAllLinkHighlights();
      longPressLinkLabelId = id;
      canvasWriter.model.updateComponent(id);
    } else {
      turnHighlightLink(id, canvasReader.state.fromCanvasCoordinates(details.globalPosition));
    }

    showSelected(); //debug
  }

  /// リンクの長押しでリンクの折れ線を作る処理
  /// 使用しない機能なのでコメント化
  /*
  var segmentIndex;

  @override
  onLinkScaleStart(String linkId, ScaleStartDetails details) {
    hideLinkOption();
    canvasWriter.model.hideAllLinkJoints();
    canvasWriter.model.showLinkJoints(linkId);
    segmentIndex = canvasReader.model
        .determineLinkSegmentIndex(linkId, details.localFocalPoint);
    if (segmentIndex != null) {
      canvasWriter.model
          .insertLinkMiddlePoint(linkId, details.localFocalPoint, segmentIndex);
      canvasWriter.model.updateLink(linkId);
    }
  }

  @override
  onLinkScaleUpdate(String linkId, ScaleUpdateDetails details) {
    if (segmentIndex != null) {
      canvasWriter.model.setLinkMiddlePointPosition(
          linkId, details.localFocalPoint, segmentIndex);
      canvasWriter.model.updateLink(linkId);
    }
  }

  @override
  onLinkLongPressStart(String linkId, LongPressStartDetails details) {
    hideLinkOption();
    canvasWriter.model.hideAllLinkJoints();
    canvasWriter.model.showLinkJoints(linkId);
    segmentIndex = canvasReader.model
        .determineLinkSegmentIndex(linkId, details.localPosition);
    if (segmentIndex != null) {
      canvasWriter.model
          .insertLinkMiddlePoint(linkId, details.localPosition, segmentIndex);
      canvasWriter.model.updateLink(linkId);
    }
  }

  @override
  onLinkLongPressMoveUpdate(String linkId, LongPressMoveUpdateDetails details) {
    if (segmentIndex != null) {
      canvasWriter.model.setLinkMiddlePointPosition(
          linkId, details.localPosition, segmentIndex);
      canvasWriter.model.updateLink(linkId);
    }
  }
  */

  //　意図しない地点がタップされているか判定
  bool notIntendCheck(String Id, var details) {
    bool isNotIntend = true;
    if (isLinkLabel) {
      ComponentData linkLabel = canvasReader.model.getComponent(Id);
      Offset labelPosition = linkLabel.position;
      Offset displacement = (editorKey.currentContext!.findRenderObject() as RenderBox).localToGlobal(Offset.zero);
      Offset localPosition = canvasReader.state.toCanvasCoordinates(labelPosition + details.localPosition);
      Offset globalPosition = details.globalPosition - displacement;
      // 意図通りにリンクラベルが押せているか判定
      if (10 > (localPosition - globalPosition).distance) {
        isNotIntend = false;
      }

    } else {
      LinkData linkData = canvasReader.model.getLink(Id);
      Offset sourcePoint = canvasReader.state.toCanvasCoordinates(linkData.linkPoints.first);
      Offset targetPoint = canvasReader.state.toCanvasCoordinates(linkData.linkPoints.last);
      double sourceDistance = (details.localPosition - sourcePoint).distance;
      double targetDistance = (details.localPosition - targetPoint).distance;
      double linkDistance = (sourcePoint - targetPoint).distance;

      //意図通りにリンクが押せているか判定
      if ( 10 > (linkDistance - (sourceDistance + targetDistance)).abs()) {
        isNotIntend = false;
      }
    }

    return isNotIntend;
  }

  // ラベルかどうか判定
  bool isCheckLinkLabel(String componentId) {
    if (canvasReader.model.canvasModel.linkExists(componentId)) return false;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) {
      return true;
    } else {
      return false;
    }
  }
}
