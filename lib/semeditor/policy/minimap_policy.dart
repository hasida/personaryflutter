import 'package:diagram_editor/diagram_editor.dart';
import 'package:flutter/material.dart';

import '../policy/component_design_policy.dart';

class MiniMapPolicySet extends PolicySet
    with
        MiniMapInitPolicy,
        CanvasControlPolicy,
        MyComponentDesignPolicy,
        MiniMapCanvasPolicy {}

mixin MiniMapInitPolicy implements InitPolicy {
  @override
  initializeDiagramEditor() {
    canvasWriter.state.setMinScale(0.025);
    canvasWriter.state.setMaxScale(0.25);
    canvasWriter.state.setScale(0.1);
    canvasWriter.state.setCanvasColor(Colors.grey[300]!.withOpacity(0.9));
    canvasWriter.state.setPosition(const Offset(80, 60));
  }
}

mixin MiniMapCanvasPolicy implements CanvasPolicy {
  @override
  onCanvasTapUp(TapUpDetails details) {
    //print("minimap tap ${details.localPosition}");
  }
}
