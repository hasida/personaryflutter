import 'dart:async';
import 'dart:math';

import '../../util/position.dart';
import 'package:flutter/material.dart';
import 'package:diagram_editor/diagram_editor.dart';

import '../data/custom_component_data.dart';
import 'custom_policy.dart';

mixin MyCanvasWidgetsPolicy implements CanvasWidgetsPolicy, CustomStatePolicy {
  //タップした地点
  Offset? firstPosition;
  //ドラッグしている地点
  Offset? secondPosition;
  StreamController<Offset?> get multipleSelectDragPositionController;

  @override
  List<Widget> showCustomWidgetsOnCanvasBackground(BuildContext context) {
    return [
      Visibility(
        visible: isGridVisible,
        child: const CustomPaint(
          size: Size.infinite,
          /*  painter: GridPainter(
            offset: canvasReader.state.position / canvasReader.state.scale,
            scale: canvasReader.state.scale,
            lineWidth: (canvasReader.state.scale < 1.0)
                ? canvasReader.state.scale
                : 1.0,
            matchParentSize: false,
            lineColor: Colors.blue[900],
          ),*/
        ),
      ),
      DragTarget<ComponentData>(
        builder: (_, __, ___) => Container(),
        onAcceptWithDetails: (DragTargetDetails<ComponentData> details) =>
            _onAcceptWithDetails(details, context),
      ),
      GestureDetector(
        onTap: () {
          onCanvasTap();
        },
        onDoubleTapDown: (TapDownDetails detail) {
          if (selectedComponentId != null) hideComponentHighlight(selectedComponentId!);
          hideAllLinkHighlights();
          isReadyToConnect = false;
          isReadyToBranch = false;
          isReadyToReconnectLink = false;
          selectedComponentId = null;
          firstPosition = detail.localPosition;
        },
        onDoubleTapCancel: () {
          isMultipleSelectDragging = true;
        },
        onLongPressStart: (LongPressStartDetails detail) {
          isMultipleSelectDragging = false;
          multipleSelectDragPositionController.sink.add(null);
          onCanvasLongPressStart(detail);
        },
        onScaleStart: (ScaleStartDetails detail) {
          if (!isMultipleSelectDragging) {
            onCanvasScaleStart(detail);
          }
        },
        onScaleUpdate: (ScaleUpdateDetails detail) {
          if (isMultipleSelectDragging) {
            multipleSelectDragPositionController.sink.add(detail.localFocalPoint);
            secondPosition = detail.localFocalPoint;
          } else {
            onCanvasScaleUpdate(detail);
          }
        },
        onScaleEnd: (ScaleEndDetails detail) {
          multipleSelectDragPositionController.sink.add(null);
          //矩形の範囲内にあるノードを選択状態にする
          if (isMultipleSelectDragging) {
            bool isSelect = false;
            Offset canvasFirstPosition = canvasReader.state.fromCanvasCoordinates(firstPosition!);
            Offset canvasSecondPosition = canvasReader.state.fromCanvasCoordinates(secondPosition!);
            canvasReader.model.getAllComponents().values.forEach((component) {
              Offset componentCenterPosition = centerPosition(component.position, component.size);
              if ([canvasFirstPosition.dx, canvasSecondPosition.dx].reduce(min) <= componentCenterPosition.dx && componentCenterPosition.dx <= [canvasFirstPosition.dx, canvasSecondPosition.dx].reduce(max)) {
                if ([canvasFirstPosition.dy, canvasSecondPosition.dy].reduce(min) <= componentCenterPosition.dy && componentCenterPosition.dy <= [canvasFirstPosition.dy, canvasSecondPosition.dy].reduce(max)) {
                  if (!(component.data as MyComponentData).isLinkLabel) {
                    isSelect = true;
                    addComponentToMultipleSelection(component.id);
                    highlightComponent(component.id);
                  }
                }
              }
            });
            if (isSelect) turnOnMultipleSelection();
          } else {
            onCanvasScaleEnd(detail);
          }
          isMultipleSelectDragging = false;
        },
      ),
    ];
  }

  void _onAcceptWithDetails(
    DragTargetDetails details,
    BuildContext context,
  ) {
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset localOffset = renderBox.globalToLocal(details.offset);
    ComponentData componentData = details.data;
    Offset componentPosition =
        canvasReader.state.fromCanvasCoordinates(localOffset);
    String componentId = canvasWriter.model.addComponent(
      ComponentData(
        position: componentPosition,
        data: MyComponentData.copy(componentData.data),
        size: componentData.size,
        minSize: componentData.minSize,
        type: componentData.type,
      ),
    );
    canvasWriter.model.moveComponentToTheFrontWithChildren(componentId);
  }


  @override
  List<Widget> showCustomWidgetsOnCanvasForeground(BuildContext context) {
    List<Widget> widgetList = [];

    for(OnCanvasForeground onCanvasForeground in onCanvasForegroundHandler){
      widgetList.addAll(onCanvasForeground(context));
    }

    widgetList.add(
        StreamBuilder<Offset?>(
            stream: multipleSelectDragPositionController.stream,
            builder: (context, snapshot) {
              if (!isMultipleSelectDragging) return Container();
              return rectangle();
            }
        )
    );

    return widgetList;
  }

  Widget rectangle() {
    return Positioned(
        left: [firstPosition!.dx, secondPosition!.dx].reduce(min),
        top: [firstPosition!.dy, secondPosition!.dy].reduce(min),
        width: (firstPosition!.dx - secondPosition!.dx).abs(),
        height: (firstPosition!.dy - secondPosition!.dy).abs(),

        child: Container(
          color: Colors.blue.withOpacity(0.1),
        )
    );
  }
}
