import 'dart:async';
import 'dart:math';

import 'package:collection/collection.dart' show SetEquality;
import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:personaryFlutter/semeditor/data/custom_link_data.dart';
import 'package:personaryFlutter/semeditor/policy/load_plr_policy.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util/item_color.dart';
import '../../util/position.dart';
import '../data/custom_component_data.dart';
import '../logic/sem_logic.dart';
import '../model/sem_link_model.dart';
import '../model/sem_node_model.dart';

typedef OnCanvasForeground = List<Widget> Function(BuildContext context);
typedef OnLinkTapUp = Function(String id, TapUpDetails details);
typedef OnLinkLongPressStart = Function(String id, LongPressStartDetails details);

mixin CustomStatePolicy implements PolicySet {
  bool isGridVisible = true;

  late void Function() getSchemata;

  List<String> bodies = [
    'rect',
  ];

  String? selectedComponentId;

  bool isDragging = false;
  bool isMultipleSelectDragging = false;
  //複数選択モード
  bool isMultipleSelectionOn = false;
  //選択されているノード
  List<String?> multipleSelected = [];
  // 選択されているリンク
  List<String?> multipleSelectedLink = [];

  // 切り取り時かどうか
  static bool isCutAndPaste = false;
  //コピーしたグラフを保存しておく
  static Graph? copyGraph;

  // リンク接続時かどうか
  bool isReadyToConnect = false;
  // 枝分かれリンク接続時かどうか
  bool isReadyToBranch = false;
  // リンク再接続時かどうか
  bool isReadyToReconnectLink = false;

  // 選択されているリンクラベル
  ComponentData? selectedLinkLabel;
  // 現在選択されているリンク
  String? selectedLinkId;

  //ロングタップされているリンクラベルのID
  String? longPressLinkLabelId;
  //ロングタップされている種別
  static LinkTypeProperty? longPressType;

  static Map<String, bool> isSelectedTypeMap = {};


  // AIノード生成中かどうか
  static bool isLoadAi = false;
  // ignore: close_sinks
  late StreamController<bool> loadAiStreamController;
  // ignore: close_sinks
  late StreamController<bool> copyStreamController;
  // ignore: close_sinks
  late StreamController<bool> duplicateStreamController;
  // ignore: close_sinks
  late StreamController<bool> readyToReconnectLinkStreamController;

  List<OnCanvasForeground> onCanvasForegroundHandler = [];
  OnLinkTapUp? onLinkTapUpHandler;
  OnLinkLongPressStart? onLinkLongPressStartHandler;

  // ignore: close_sinks
  late StreamController<Offset?> multipleSelectDragPositionController;

  late GlobalKey editorKey;

  late DiagramEditorContext diagramEditorContext;

  // PLR用
  /// アカウント
  late Account account;

  /// タイムライン
  late Timeline timeline;

  /// ノード暗号化フラグ
  late bool isNodeEncrypt;

  /// スキーマタ
  late Schemata timelineSchemata;
  late Schemata graphSchemata;
  late Schemata rootGraphSchemata;

  /// ログイン者PLRID
  late String plrId;

  /// グラフノード
  late Item graphItem;
  late Graph graph;

  late bool isTopGraph;

  /// 描画済みノードのMap（ノードId:コンポーネントId）
  Map<String, String> drawMap = {};

  /// コンテキスト
  late BuildContext context;

  List<String> syncingList = [];

  // hyperNodeをまたいだデータを保持したいため、staticにしている
  // コピー時に選択されていたノード
  static List<String?> multipleCopiedSelectedNodeId = [];
  // 切り取られる予定のノード
  static List<String?> drawCutNodeId = [];
  static List<ComponentData> multipleTimelineCopiedSelectedData = [];

  // コピーされた全てのノードのマップ
  static Map<String, ComponentData?> multipleCopiedNodeData = Map<String, ComponentData?>();
  // コピーされたリンクラベル
  // コピーされたノードと共有するノードのマップ
  //切り取りが完了したかどうか
  static bool isDoneCut = false;

  //共有するリンクのリスト
  static Map<LinkTypeProperty, GraphElement?> linkElementMap = {};

  /// デバッグ用
  /// 選択中のノードのノードIDを表示
  void showSelected() {
    List<String?> selectedList = [];
    canvasReader.model.getAllComponents().values.forEach((component) {
      if ((component.data as MyComponentData).isEditing) {
        selectedList.add((component.data as MyComponentData).nodeId);
      }
    });
    logout("<selectedNode>: ${selectedList} <selectedLinkId>: ${selectedLinkId}");
  }

  syncDelete(String id) {
    syncingList.remove(id);
  }

  syncAdd(String id) {
    canvasReader.model.getAllComponents().values.forEach((component) {
      MyComponentData myComponentData = (component.data as MyComponentData);
      if (myComponentData.isLinkLabel) {
        for (LinkTypeProperty linkType in myComponentData.typeList!) {
          if (linkType.elementId == id) linkType.syncingCount -= 1;
        }
      }
    });
  }

  //旧型式("#"付き)と新形式("#"なし)のIDを交代して出力する
  String parseId(String nodeId) {
    if (nodeId.startsWith(uriFragmentSign)) {
      return nodeId.substring(uriFragmentSign.length);
    } else {
      return "${uriFragmentSign}${nodeId}";
    }
  }

  // 全てのコンポーネントの強調表示を解除
  hideAllHighlights() {
    multipleSelected = [];
    canvasWriter.model.hideAllLinkJoints();
    hideLinkOption();
    canvasReader.model.getAllComponents().values.forEach((component) {
      if (component.data.isHighlightVisible) {
        hideComponentHighlight(component.id);
      }
    });
    copyStreamController.sink.add(false);
  }

  // コンポーネントを強調表示
  highlightComponent(String componentId) {
    canvasReader.model.getComponent(componentId).data.showHighlight();
    canvasReader.model.getComponent(componentId).updateComponent();
    canvasWriter.model.moveComponentToTheFront(componentId);

    // 編集開始
    canvasReader.model.getComponent(componentId).data.isEditing = true;
  }

  // コンポーネントの強調表示を解除
  hideComponentHighlight(String componentId) {
    canvasReader.model.getComponent(componentId).data.hideHighlight();
    canvasReader.model.getComponent(componentId).updateComponent();
    canvasWriter.model.moveComponentToTheBack(componentId);

    // 編集終了
    canvasReader.model.getComponent(componentId).data.isEditing = false;
  }

  // リンクを選択表示
  highlightLink(String linkId, Offset position) {
    LinkData selectedLinkData = canvasReader.model.getLink(linkId);
    ComponentData linkLabel = getLinkLabel(selectedLinkData);
    MyComponentData myLinkLabel = (linkLabel.data as MyComponentData);
    if (multipleSelectedLink.length != 0) hideAllLinkHighlights();

    //リンクラベルの種別が選択されていない場合は種別を設定する
    bool isSelectedType = false;
    myLinkLabel.typeList!.sort((a,b) => a.elementId.compareTo(b.elementId));

    for (LinkTypeProperty linkType in myLinkLabel.typeList!) {
      isSelectedType |= isSelectedTypeMap.containsKey(linkType.elementId);
    }
    if (!isSelectedType) {
      isSelectedTypeMap[myLinkLabel.typeList!.first.elementId] = true;
      for (Connection con in linkLabel.connections) {
        LinkData linkData = canvasReader.model.getLink(con.connectionId);
        (linkData.data as MyLinkData).endLabel = myLinkLabel.typeList!.first.type!;
        (linkData.data as MyLinkData).isSymmetric = myLinkLabel.typeList!.first.isSymmetric;
        (linkData.data as MyLinkData).linkEntity = myLinkLabel.typeList!.first.linkEntity;
        linkData.updateLink();
      }
    }

    (selectedLinkData.data as MyLinkData).isHighlightVisible = true;
    selectedLinkData.linkStyle.color = (selectedLinkData.data as MyLinkData).isCutLink ? Colors.red.withOpacity(0.3): Colors.red;
    selectedLinkData.updateLink();
    linkLabel.updateComponent();
    if (!multipleSelectedLink.contains(linkId)) multipleSelectedLink.add(linkId);
    showLinkOption(linkId, position: position);
  }

  // リンクの選択を解除
  hideLinkHighlight(String linkId) {
    multipleSelectedLink.remove(linkId);
    LinkData linkData = canvasReader.model.getLink(linkId);
    MyLinkData myLinkData = (linkData.data as MyLinkData);
    myLinkData.isHighlightVisible = false;
    linkData.linkStyle.color = myLinkData.isCutLink ? Colors.black.withOpacity(0.3): Colors.black;
    ComponentData linkLabel = getLinkLabel(linkData);
    hideLinkOption();

    //選択されているリンクが他になければリンクラベルの選択を解除する
    bool isSelectedLink = false;
    for (Connection con in linkLabel.connections) {
      isSelectedLink |= multipleSelectedLink.contains(con.connectionId);
    }

    if (!isSelectedLink) {
      for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
        isSelectedTypeMap.remove(linkType.elementId);
      }
      myLinkData.endLabel = "";
      myLinkData.isSymmetric = true;
      myLinkData.linkEntity = null;
    }

    linkData.updateLink();
    linkLabel.updateComponent();
  }

  // リンク選択の状態を交替する
  turnHighlightLink(String linkId, Offset position) {
    LinkData linkData = canvasReader.model.getLink(linkId);
    if ((linkData.data as MyLinkData).isHighlightVisible) {
      hideLinkHighlight(linkId);
    } else {
      highlightLink(linkId, position);
    }
    linkData.updateLink();
  }

  // 全てのリンクの選択を解除
  hideAllLinkHighlights() {
    canvasWriter.model.hideAllLinkJoints();
    hideLinkOption();
    hideLongPress();
    canvasReader.model.getAllLinks().values.forEach((link) {
      hideLinkHighlight(link.id);
    });
  }

  // 複数選択モードを交替
  turnOnMultipleSelection() {
    isMultipleSelectionOn = true;
    isReadyToConnect = false;

    if (selectedComponentId != null) {
      addComponentToMultipleSelection(selectedComponentId);
      selectedComponentId = null;
    }
  }

  // 複数選択モードをオフ
  turnOffMultipleSelection() {
    isMultipleSelectionOn = false;
    hideAllHighlights();
  }

  addComponentSingle(String? componentId) {
    if (!multipleSelected.contains(componentId)) {
      hideAllHighlights();
      multipleSelected.add(componentId);
      canvasReader.model.getComponent(componentId!).updateComponent();
    }
  }

  addComponentToMultipleSelection(String? componentId) {
    if (!multipleSelected.contains(componentId)) {
      multipleSelected.add(componentId);
      multipleSelected.forEach((componentId) {
        canvasReader.model.getComponent(componentId!).updateComponent();
      });
    }
    copyStreamController.sink.add(true);
  }

  removeComponentFromMultipleSelection(String componentId) {
    multipleSelected.remove(componentId);
    multipleSelected.forEach((componentId) {
      canvasReader.model.getComponent(componentId!).updateComponent();
    });
    copyStreamController.sink.add(false);
  }

  String duplicate(ComponentData componentData) {
    var cd = ComponentData(
      type: componentData.type,
      size: componentData.size,
      minSize: componentData.minSize,
      data: MyComponentData.copy(componentData.data),
      position: componentData.position + const Offset(20, 20),
    );
    String id = canvasWriter.model.addComponent(cd);
    return id;
  }

  showLinkOption(String linkId, {Offset? position}) {
    selectedLinkId = linkId;
    // 編集開始
    // リンクの元先を編集中へ
    LinkData linkData = canvasReader.model.getLink(linkId);
    if (position != null) (linkData.data as MyLinkData).tapPosition = position;
    var sourceId = linkData.sourceComponentId;
    var targetId = linkData.targetComponentId;
    var source = canvasReader.model.getComponent(sourceId);
    var target = canvasReader.model.getComponent(targetId);
    (source.data as MyComponentData).isEditing = true;
    (target.data as MyComponentData).isEditing = true;
  }

  hideLinkOption() {
    // 編集終了
    // 選択中のリンクの元先を非編集へ

    for (ComponentData componentData in canvasReader.model.getAllComponents().values) {
      (componentData.data as MyComponentData).isEditing = false;
    }

    if (multipleSelectedLink.isNotEmpty) {
      selectedLinkId = multipleSelectedLink.last;
    } else {
      selectedLinkId = null;
    }
  }

  hideLongPress() {
    if (longPressLinkLabelId != null && (canvasReader.model.canvasModel.componentExists(longPressLinkLabelId!)))
      canvasWriter.model.updateComponent(longPressLinkLabelId!);
    longPressLinkLabelId = null;
  }

  Future<Entity?> updateLinkEntity(String elementId) async {
    //リンクエンティティは残したまま　必要に応じて更新する形に変更する
    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
    GraphElement? element = getElement(graph,elementId);
    if (element == null) {
      await graph.syncSilently();
      element = getElement(graph,elementId);
      if (element == null) return null;
    }

    SemLinkModel? linkModel;
    for (Schemata schemata in schemataList) {
      if (schemata.classOf(element.type) != null) {
        Item link = Item(element.node);
        linkModel = SemLinkModel(link, schemata);
      }
      if (linkModel == null) {
        await element.node.syncSilently();
        Item link = Item(element.node);
        linkModel = SemLinkModel(link, schemata);
      }
    }
    return linkModel!.linkEntity;
  }

  Future setHistory(SchemaClass? schemaClass) async {
    if (schemaClass == null) return;

    const int saveLength = 10;
    const id = "History";
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>  historyList = [];
    if (prefs.getStringList(id) != null) {
      historyList.addAll(prefs.getStringList(id) as Iterable<String>);
    }
    int num = -1 ;
    for (int i = 0; i < [historyList.length, saveLength].reduce(min); i++) {
      var map = convert.json.decode(historyList[i]);
      if (schemaClass.id == map["id"]) {
        num = i;
      }
    }

    //ヒストリにすでにあるものを選択した場合
    if (num != -1 ) historyList.removeAt(num);
    //ヒストリにないものを選択し、ヒストリの保存個数を超えた場合
    if (saveLength == historyList.length) historyList.removeAt(historyList.length -1);
    var historyMap = schemaClass.label!.baseMap;
    historyMap['id'] = schemaClass.id;
    String historyString = convert.json.encode(schemaClass.label!.baseMap);
    historyList.insert(0, historyString);
    prefs.setStringList(id, historyList);
  }


  /// 選択スキーマ読込み
  Future<List<String>?> getHistory() async {
    const id = "History";
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(id);
  }

  SchemaClass? getSchemaClass(LinkTypeProperty linkTypeProperty) {
    SchemaClass? schemaClass;
    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
    for (Schemata schemata in schemataList) {
      schemaClass ??= schemata.classOf(linkTypeProperty.linkEntity!.type);
    }
    return schemaClass;
  }

  // タイムラインでのノードのコピー
  static copyNode(RecordEntity recordEntity , String plrId) {
    SemNodeModel  model = SemNodeModel(plrId);
    ComponentData componentData = ComponentData(
      size: const Size(120, 72),
      minSize: const Size(80, 64),
      position: Offset.zero,
      data: MyComponentData(
        color: ItemColor.classColor(),
        borderColor: Colors.black,
        borderWidth: 1.0,
        secondBorderWidth: 0.3,
        itemEntity: recordEntity,
        isMe: model.isMe,
        dispClass: model.dispClass,
        dispCntList: model.dispCntList,
      ),
      type: 'rect',
    );

    CustomStatePolicy.multipleTimelineCopiedSelectedData.add(componentData);
    String nodeId = recordEntity.entity!.id!;
    CustomStatePolicy.multipleCopiedSelectedNodeId.add(nodeId);
    CustomStatePolicy.multipleCopiedNodeData[nodeId] = componentData;
  }

  static final _graphs = Expando<Graph>();
  Graph? _graphOf(Item item) => _graphs[item] ??= item.graph;

  // ハイパーノードの中身によってノードの枠の幅を決める
  calcBorderSize(ComponentData componentData, {Offset? posOffset}) {
    MyComponentData myComponentData = (componentData.data as MyComponentData);
    Item? item;
    if (myComponentData.itemEntity != null) {
      item = myComponentData.itemEntity?.entity as Item;

      final graph = _graphOf(item);

      if (graph != null) {
        graph.syncElements().listen((elementResult) {
          if(!elementResult.value.all.isEmpty) {
            myComponentData.borderWidth = MyComponentData.hyperNodeBorder;
            if (myComponentData.isLinkLabel) {
              calcLinkLabelSize(componentData, context);
            } else {
              calcNodePosition(componentData, posOffset!, context);
            }
          } else {
            myComponentData.borderWidth = MyComponentData.defaultNodeBorder;
          }
        });
      } else {
        myComponentData.borderWidth = MyComponentData.defaultNodeBorder;
      }
    }
  }

  updateLinkLabelPosition (String componentId) {
    //リンクやラベルの座標を更新して描画する
    ComponentData componentData = canvasReader.model.getComponent(componentId);
    for (Connection con1 in componentData.connections) {
      String linkId = con1.otherComponentId;
      ComponentData linkLabel = canvasReader.model.getComponent(linkId);
      setLinkLabelPosition(linkLabel);
      linkLabel.updateComponent();
      for (Connection con2 in linkLabel.connections) {
        canvasWriter.model.updateLink(con2.connectionId);
      }
    }
  }

  //切り取り時に色を薄くする
  drawCutLink() {
    // 全てのリンクの切り取り状態を初期化する
    canvasReader.model.getAllLinks().values.forEach((linkData) {
      (linkData.data as MyLinkData).isCutLink = false;
      linkData.linkStyle.color = linkData.linkStyle.color.withOpacity(1);
      linkData.updateLink();
    });

    canvasReader.model.getAllComponents().values.forEach((component) {
      MyComponentData myComponentData = (component.data as MyComponentData);
      if (myComponentData.isLinkLabel) {
        myComponentData.isCutLinkLabel = false;
        myComponentData.color = myComponentData.color.withOpacity(1);
        component.updateComponent();
        //リンクラベルと線分の色を薄くする
        for (Connection link in component.connections) {
          ComponentData node = canvasReader.model.getComponent(link.otherComponentId);
          if (CustomStatePolicy.copyGraph?.id == graph.id && CustomStatePolicy.drawCutNodeId.contains((node.data as MyComponentData).nodeId)) {
            myComponentData.isCutLinkLabel = true;
            myComponentData.color = myComponentData.color.withOpacity(0.3);
            component.updateComponent();
            for (Connection link in component.connections) {
              LinkData linkData = canvasReader.model.getLink(link.connectionId);
              (linkData.data as MyLinkData).isCutLink = true;
              linkData.linkStyle.color = linkData.linkStyle.color.withOpacity(0.3);
              linkData.updateLink();
            }
          }
        }
      } else {
        myComponentData.isCutNode = false;
        if (CustomStatePolicy.copyGraph?.id == graph.id && CustomStatePolicy.drawCutNodeId.contains(myComponentData.nodeId)) {
          myComponentData.isCutNode = true;
        }
        component.updateComponent();
      }
    });
  }

  //リンクラベルの位置を更新
  Offset setLinkLabelPosition(ComponentData linkLabel) {
    double? left;
    double? right;
    double? top;
    double? bottom;

    for (Connection con in linkLabel.connections) {
      String componentId = con.otherComponentId;
      ComponentData componentData = canvasReader.model.getComponent(componentId);
      Offset position = centerPosition(componentData.position, componentData.size);
      if (left == null) left = position.dx;
      left = min(left ,position.dx);
      if (right == null) right = position.dx;
      right = max(right ,position.dx);
      if (top == null) top = position.dy;
      top = min(top ,position.dy);
      if (bottom == null) bottom = position.dy;
      bottom = max(bottom ,position.dy);
    }

    if (linkLabel.connections.isNotEmpty) {
      Offset initLinkLabelPosition = Offset((right! + left!) / 2 , (bottom! + top!) / 2) - Offset(linkLabel.size.width/2, linkLabel.size.height/2);
      //初回描画時はリンクの位置を更新する
      if (linkLabel.position == Offset.zero) {
        linkLabel.position = initLinkLabelPosition;
        for (Connection con in linkLabel.connections) {
          canvasWriter.model.updateLink(con.connectionId);
        }
      }
      changeLinkLabelPosition(linkLabel, initLinkLabelPosition);
    }
    setAllAngle(linkLabel);
    return linkLabel.position;
  }

  //リンクラベルの中心の位置を変える
  void changeLinkLabelPosition(ComponentData linkLabel, Offset initLinkLabelPosition) {
    double space = (linkLabel.data as MyComponentData).typeCntSize!.height/2;

    //端点が2個でない場合は位置を変えない
    if (linkLabel.connections.length != 2) {
      linkLabel.position = initLinkLabelPosition;
      return;
    }
    Set<ComponentData> overlapSet = {};
    overlapSet.addAll(getComponentsOverlapLinkLabel(linkLabel, initLinkLabelPosition, space));
    //リンクラベルがノードに重なる(近すぎる)事が無い場合は位置を変えない
    if (overlapSet.length == 0) {
      linkLabel.position = initLinkLabelPosition;
      return;
    }
    overlapSet.addAll(getComponentsOverlapLinkLabel(linkLabel, linkLabel.position, space));

    List<ComponentData> overlapList = overlapSet.toList();
    //リンクラベルがノードに重なる(近すぎる)個数によって位置を変える
    int state = overlapList.length;

    List<ComponentData> notOverlapList = [];
    for (Connection con in linkLabel.connections) {
      String componentId = con.otherComponentId;
      ComponentData componentData = canvasReader.model.getComponent(componentId);
      if (!overlapList.contains(componentData)) notOverlapList.add(componentData);
    }

    //端点のノードが重なる場合は線分の中点にリンクラベルの中点を置く
    if (isCheckOverlap(linkLabel.connections.first.otherComponentId, linkLabel.connections.last.otherComponentId)) {
      state = 2;
    }

    if (state == 1) {
      //リンクラベルの位置がノードに重なる(近すぎる)場合、ラベルの高さ分の概ね半分を開けた位置に
      Offset position = centerPosition(notOverlapList.first.position, notOverlapList.first.size);
      Offset overPosition = centerPosition(overlapList.first.position, overlapList.first.size);
      Offset dif =  position - overPosition;
      double angle = atan2(dif.dx, dif.dy) - pi/2;
      double r1 = getMinRadius(angle, overlapList.first.size.width/2, overlapList.first.size.height/2);
      double r2 = getMinRadius(angle, linkLabel.size.width/2, linkLabel.size.height/2);
      double R = sqrt(pow(dif.dx,2) + pow(dif.dy,2));

      linkLabel.position =  overPosition + Offset((r1+r2+space)/R * dif.dx, (r1+r2+space)/R * dif.dy) - Offset(linkLabel.size.width/2, linkLabel.size.height/2);
    } else if (state == 2) {
      //リンクラベルの位置がどちらのノードにも重なる(近すぎる)場合、線分の中点にリンクラベルの中点を置く
      Offset point = Offset.zero;
      for (Connection con in linkLabel.connections) {
        LinkData linkData = canvasReader.model.getLink(con.connectionId);
        ComponentData componentData = canvasReader.model.getComponent(linkData.sourceComponentId);
        if ((componentData.data as MyComponentData).isLinkLabel) {
          point += linkData.linkPoints.last;
        } else {
          point += linkData.linkPoints.first;
        }
      }
      linkLabel.position = point/2 - Offset(linkLabel.size.width/2, linkLabel.size.height/2);
    }
  }

  //コンポーネントの同士が重なっているかを判定
  bool isCheckOverlap(String sourceComponentId, String targetComponentId) {
    bool isOverlap = false;
    ComponentData sourceComponentData = canvasReader.model.getComponent(sourceComponentId);
    ComponentData targetComponentData = canvasReader.model.getComponent(targetComponentId);
    Offset sourcePosition = centerPosition(sourceComponentData.position, sourceComponentData.size);
    Offset targetPosition = centerPosition(targetComponentData.position, targetComponentData.size);

    double dx = (sourcePosition.dx - targetPosition.dx).abs();
    double dy = (sourcePosition.dy - targetPosition.dy).abs();
    if (dx < sourceComponentData.size.width/2 + targetComponentData.size.width/2) {
      if (dy < sourceComponentData.size.height/2 + targetComponentData.size.height/2) {
        isOverlap = true;
      }
    }
    return isOverlap;
  }

  //入力された位置がノードに重なる(近すぎる)場合、そのノードを出力する
  List<ComponentData> getComponentsOverlapLinkLabel(ComponentData linkLabel, Offset linkLabelPosition, space) {
    List<ComponentData> overlapList = [];
    Offset linkLabelCenterPosition = linkLabelPosition + Offset(linkLabel.size.width/2, linkLabel.size.height/2);

    for (Connection con in linkLabel.connections) {
      ComponentData componentData = canvasReader.model.getComponent(con.otherComponentId);
      Offset position = centerPosition(componentData.position, componentData.size);
      Offset dif =  position - linkLabelCenterPosition;
      double angle = atan2(dif.dx, dif.dy) - pi/2;
      double r1 = getMinRadius(angle, componentData.size.width/2, componentData.size.height/2);
      double r2 = getMinRadius(angle, linkLabel.size.width/2, linkLabel.size.height/2);
      double R = sqrt(pow(dif.dx,2) + pow(dif.dy,2));

      if (R - (r1+r2) < space) overlapList.add(componentData);
    }
    return overlapList;
  }

  double getMinRadius(double angle ,double width, double height) {
    double r = cos(angle) == 0 ? double.infinity : (width / cos(angle)).abs();
    r = min(r, sin(angle) == 0 ? double.infinity : (height / sin(angle)).abs());
    return r;
  }

  //そのリンクと接続しているリンクラベルを取得する
  ComponentData getLinkLabel(LinkData linkData) {
    ComponentData? linkLabel;
    for (String componentId in [linkData.sourceComponentId, linkData.targetComponentId]) {
      if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) {
        linkLabel = canvasReader.model.getComponent(componentId);
      }
    }
    return linkLabel!;
  }

  bool isCheckExistLink(String sourceComponentId, String targetComponentId) {
    // すでにあるリンクと同じリンクを作成できなくする処理(diagram_editorパッケージのデフォルト処理)
    if (canvasReader.model.getComponent(sourceComponentId).connections.any(
            (connection) =>
        (connection is ConnectionOut) &&
            (connection.otherComponentId == targetComponentId))) {
      return true;
    }

    // すでにあるリンクの逆向きのリンクを作成できなくする処理
    if (canvasReader.model.getComponent(targetComponentId).connections.any(
            (connection) =>
        (connection is ConnectionOut) &&
            (connection.otherComponentId == sourceComponentId))) {
      return true;
    }
    return false;
  }

  //リンクを作成
  String? connectLink(ComponentData sourceComponent, ComponentData targetComponent, String dispText, bool isSymmetric, Entity? linkEntity) {
    bool isExistLink = isCheckExistLink(sourceComponent.id, targetComponent.id);
    if (isExistLink) return null;

    String linkId = canvasWriter.model.connectTwoComponents(
      sourceComponentId: sourceComponent.id,
      targetComponentId: targetComponent.id,
      linkStyle: LinkStyle (
        arrowType: (targetComponent.data as MyComponentData).isLinkLabel ? ArrowType.none : ArrowType.centerCircle,
        backArrowType: (sourceComponent.data as MyComponentData).isLinkLabel ? ArrowType.none : ArrowType.centerCircle,
        arrowSize: 4,
        backArrowSize: 4,
      ),
      data: MyLinkData(
        endLabel: dispText,
        isSymmetric: isSymmetric,
        linkEntity: linkEntity,
      ),
    );

    canvasWriter.model.updateLink(linkId);
    return linkId;
  }

  //リンクラベルを追加・更新し、リンクを接続する
  ComponentData? drawLink(List<ComponentData> componentList, List<LinkTypeProperty> diffTypeList, bool isAfter) {
    Set<String> nodeIdSet = {};
    for (ComponentData component in componentList) {
      nodeIdSet.add((component.data as MyComponentData).nodeId!);
    }

    ComponentData? linkLabel = updateLinkLabel(nodeIdSet, diffTypeList, isAfter);

    if (isAfter) {
      if (linkLabel == null) {
        linkLabel = addLinkLabel(diffTypeList);
      }
    } else {
      if (linkLabel == null) return null;
    }

    for (ComponentData component in componentList) {
      connectLink(component, linkLabel, "", true, null);
    }

    return linkLabel;
  }

  //入力された集合のみが接続されているリンクの種別を更新する
  //引数（操作する集合,操作するリンクの種別,リンクの種別を追加するか、削除するか）
  ComponentData? updateLinkLabel(Set<String> nodeIdSet, List<LinkTypeProperty> diffTypeList, bool isAfter) {
    ComponentData? linkLabel;
    List<ComponentData> componentList = [];
    componentList += canvasReader.model.getAllComponents().values.toList();
    for (ComponentData component in componentList) {
      if ((component.data as MyComponentData).isLinkLabel) {
        Set<String> set = {};
        List<Connection> connectionList = [];
        connectionList += component.connections;
        for (Connection con in connectionList) {
          set.add((canvasReader.model.getComponent(con.otherComponentId).data as MyComponentData).nodeId!);
        }
        if (const SetEquality().equals(nodeIdSet, set)) {
          linkLabel = component;
          MyComponentData myComponentData =  (linkLabel.data as MyComponentData);
          //リンク種別を追加
          if (isAfter) {
            for (LinkTypeProperty linkType in diffTypeList) {
              bool isDeleteLink = false;
              //既に存在している種別（向きも同じ）なら追加しない
              for (int i = 0; i < myComponentData.typeList!.length; i++) {
                if (linkType.elementId != myComponentData.typeList![i].elementId && linkType.type == myComponentData.typeList![i].type && linkType.targetNodeId == myComponentData.typeList![i].targetNodeId) {
                  deleteLink(graph, timeline, linkType.elementId, true);
                  isDeleteLink = true;
                }
                if (linkType.elementId == myComponentData.typeList![i].elementId) {
                  myComponentData.typeList![i] = linkType;
                  isDeleteLink = true;
                }
              }
              if (isDeleteLink) continue;
              myComponentData.typeList!.add(linkType);
            }
          // リンク種別を削除
          } else {
            for (LinkTypeProperty linkType in diffTypeList) {
              myComponentData.typeList!.remove(linkType);
            }
            if (myComponentData.typeList!.isEmpty) {
              canvasWriter.model.removeComponent(linkLabel.id);
              return null;
            }
          }
          calcLinkLabelSize(linkLabel, context);
          setLinkLabelPosition(linkLabel);
          linkLabel.updateComponent();
          break;
        }
      }
    }
    return linkLabel;
  }

  //リンクラベル作成
  ComponentData addLinkLabel(List<LinkTypeProperty> typeList) {
    //ラベル作成
    ComponentData linkLabel = ComponentData(
      size: const Size(120, 72),
      minSize: const Size(80, 64),
      data: MyComponentData(
        color: ItemColor.classColor(),
        borderColor: Colors.transparent,
        borderWidth: 0.0,
        secondBorderWidth: 0.3,
        itemEntity: null,
        isMe: true,
        dispClass: "",
        dispCntList: [],
        isLinkLabel: true,
        typeList: typeList,
        onLinkTapUpHandler: onLinkTapUpHandler,
        onLinkLongPressStartHandler: onLinkLongPressStartHandler,
      ),
      type: 'rect',
    );

    // ラベルの描画
    canvasWriter.model.addComponent(linkLabel);

    (linkLabel.data as MyComponentData).initialize();
    calcBorderSize(linkLabel);

    calcLinkLabelSize(linkLabel, context);

    canvasWriter.model.addComponent(linkLabel);
    return linkLabel;
  }

  setAllAngle(ComponentData linkLabel) {
    for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
      setAngle(linkLabel, linkType.elementId);
    }
  }

  //リンクラベルの矢印の向きを更新
  setAngle(ComponentData linkLabel, String elementId, {String? targetNodeId}) {
    LinkTypeProperty? linkTypeProperty;

    for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
      if (linkType.elementId == elementId) linkTypeProperty = linkType;
    }
    if (linkTypeProperty!.isSymmetric!) {
      linkTypeProperty.angle = 0.0;
    } else {
      targetNodeId = targetNodeId ?? linkTypeProperty.targetNodeId ?? getTargetId(linkTypeProperty.linkEntity!);
      if (targetNodeId == null) return;
      ComponentData targetComponent = canvasReader.model.getComponent(drawMap[targetNodeId]!);
      Offset differentPosition = centerPosition(targetComponent.position, targetComponent.size) - centerPosition(linkLabel.position, linkLabel.size);
      double angle = pi - atan2(differentPosition.dx, differentPosition.dy);
      linkTypeProperty.angle = angle;
      linkTypeProperty.targetNodeId = targetNodeId;
    }
  }

  //線分追加
  bool createBranch(String componentId) {
    bool isExistLink = isCheckExistLink(componentId, selectedLinkLabel!.id);
    if (isExistLink) return false;

    ComponentData componentData = canvasReader.model.getComponent(componentId);
    LinkData selectedLinkData = canvasReader.model.getLink(selectedLinkLabel!.connections.first.connectionId);
    MyLinkData selectedMyLinkData = (selectedLinkData.data as MyLinkData);
    Entity linkEntity = selectedMyLinkData.linkEntity!;
    RecordEntity node = (componentData.data as MyComponentData).itemEntity!;

    if (!selectedMyLinkData.isSymmetric!) return false;

    List<ComponentData> componentList = [];

    (selectedLinkLabel!.data as MyComponentData).isEditing = false;

    for (Connection con in selectedLinkLabel!.connections) {
      ComponentData componentData = canvasReader.model.getComponent(con.otherComponentId);
      componentList.add(componentData);
    }

    //リンクラベルを再描画
    LinkTypeProperty? linkTypeProperty;
    for (LinkTypeProperty linkType in (selectedLinkLabel!.data as MyComponentData).typeList!) {
      if (linkType.elementId ==  linkEntity.id) linkTypeProperty = linkType;
    }
    List<LinkTypeProperty> diffTypeList = [linkTypeProperty!];

    hideAllLinkHighlights();
    ComponentData? prevLinkLabel = drawLink(componentList, diffTypeList, false);
    componentList.add(componentData);
    ComponentData? nextLinkLabel = drawLink(componentList, diffTypeList, true)!;
    branchLink(linkEntity, node, syncDelete: syncDelete, syncAdd: syncAdd);

    syncingList.add(linkTypeProperty.elementId);
    linkTypeProperty.syncingCount += 1;

    SchemaClass? schemaClass = getSchemaClass(linkTypeProperty);

    setHistory(schemaClass);

    //リンクラベルを更新
    List<ComponentData> linkLabelList = [nextLinkLabel];
    if (prevLinkLabel != null) linkLabelList.add(prevLinkLabel);
    for (ComponentData linkLabel in linkLabelList) {
      setLinkLabelPosition(linkLabel);
      linkLabel.updateComponent();
      for (Connection link in linkLabel.connections) {
        canvasWriter.model.updateLink(link.connectionId);
      }
    }
    drawCutLink();
    isReadyToBranch = false;
    return true;
  }

  //リンク再接続
  bool drawReconnect(String nextComponentId) {
    bool isExistLink = isCheckExistLink(nextComponentId, selectedLinkLabel!.id);
    if (isExistLink) return false;

    LinkData linkData = canvasReader.model.getLink(selectedLinkId!);
    MyLinkData selectedMyLinkData = (linkData.data as MyLinkData);
    ComponentData nextComponent = canvasReader.model.getComponent(nextComponentId);
    ComponentData? prevComponent;
    for (String id in [linkData.sourceComponentId, linkData.targetComponentId]) {
      ComponentData componentData = canvasReader.model.getComponent(id);
      if (!(componentData.data as MyComponentData).isLinkLabel) {
        prevComponent = componentData;
      }
    }

    //PLR書き込み
    reconnectLink(selectedMyLinkData.linkEntity!, (prevComponent!.data as MyComponentData).itemEntity!.entity!, (nextComponent.data as MyComponentData).itemEntity!.entity!, syncDelete: syncDelete, syncAdd: syncAdd);

    List<ComponentData> prevComponentList = [];
    List<ComponentData> nextComponentList = [];

    String? targetNodeId;
    MyComponentData myLinkData = (selectedLinkLabel!.data as MyComponentData);
    for (Connection con in selectedLinkLabel!.connections) {
      LinkData linkData = canvasReader.model.getLink(con.connectionId);
      ComponentData componentData = canvasReader.model.getComponent(con.otherComponentId);
      for (LinkTypeProperty linkType in myLinkData.typeList!) {
        if (linkType.elementId == (linkData.data as MyLinkData).linkEntity!.id) targetNodeId = linkType.targetNodeId;
      }
      prevComponentList.add(componentData);
      if (con.otherComponentId == prevComponent.id) {
        nextComponentList.add(nextComponent);
      } else {
        nextComponentList.add(componentData);
      }
    }

    String elementId = selectedMyLinkData.linkEntity!.id!;

    hideLinkHighlight(linkData.id);

    LinkTypeProperty? linkTypeProperty;

    for (LinkTypeProperty linkType in myLinkData.typeList!) {
      if (linkType.elementId == elementId) linkTypeProperty = linkType;
    }

    SchemaClass? schemaClass = getSchemaClass(linkTypeProperty!);

    setHistory(schemaClass);

    //リンクラベルを再描画
    ComponentData? prevLinkLabel = drawLink(prevComponentList, [linkTypeProperty], false);
    if (targetNodeId == (prevComponent.data as MyComponentData).nodeId) {
      targetNodeId = (nextComponent.data as MyComponentData).nodeId;
      linkTypeProperty.targetNodeId= targetNodeId;
    }
    ComponentData nextLinkLabel = drawLink(nextComponentList, [linkTypeProperty], true)!;

    syncingList.add(linkTypeProperty.elementId);
    linkTypeProperty.syncingCount += 1;

    //リンクラベルを更新
    setAngle(nextLinkLabel, elementId, targetNodeId: targetNodeId);
    List<ComponentData> linkLabelList = [nextLinkLabel];
    if (prevLinkLabel != null) linkLabelList.add(prevLinkLabel);
    for (ComponentData linkLabel in linkLabelList) {
      setLinkLabelPosition(linkLabel);
      linkLabel.updateComponent();
      for (Connection link in linkLabel.connections) {
        canvasWriter.model.updateLink(link.connectionId);
      }
    }
    drawCutLink();
    isReadyToReconnectLink = false;
    return true;
  }

  //ツールチップの文章の一部を取得
  ({String? subject, String? object}) getTooltipNode(ComponentData linkLabel, elementId) {
    String? _object;
    String? _subject;

    String? targetNodeId;
    if ((linkLabel.data as MyComponentData).typeList != null) {
      for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
        if (linkType.elementId == elementId) targetNodeId = linkType.targetNodeId;
      }
    }

    for (Connection con in linkLabel.connections) {
      ComponentData com = canvasReader.model.getComponent(con.otherComponentId);

      String text = (com.data as MyComponentData).dispCnt!.isNotEmpty ? com.data.dispCnt : com.data.dispClass;
      // 改行対応
      text = text.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ");
      // 用例省略
      if (text.length > 15) {
        text = text.substring(0, 14) + "...";
      }

      if ((com.data as MyComponentData).nodeId == targetNodeId) {
        if (_object == null) {
          _object = text;
        } else {
          _object += ",${text}";
        }
      } else {
        if (_subject == null) {
          _subject = text;
        } else {
          _subject += ",${text}";
        }
      }
    }
    if (_object == null) _object = "";
    return (subject: _subject, object: _object);
  }
}

mixin CustomBehaviourPolicy implements PolicySet, CustomStatePolicy, LoadPlrPolicy {
  removeAll(BuildContext context) {
    canvasWriter.model.removeAllComponents();
  }

  resetView() {
    canvasWriter.state.resetCanvasView();
  }

  /// ノードの位置から位置づける
  positionView() {
    double posX = 0.0;
    double posY = 0.0;
    double? result;

    List<ComponentData> componentDataList =
        canvasReader.model.getAllComponents().values.toList();

    if (componentDataList.length != 0) {
      double minX = double.maxFinite;
      double minY = double.maxFinite;
      double maxX = double.negativeInfinity;
      double maxY = double.negativeInfinity;
      double screenWidth;
      double screenHeight;
      double width;
      double height;
      double componentRatio;
      double screenRatio;
      late double maxNodeWidth;
      late double maxNodeHeight;
      double centerX;
      double centerY;

      for (var data in componentDataList) {
        double x = data.position.dx;
        double y = data.position.dy;
        if (x < minX) minX = x;
        if (y < minY) minY = y;
        if (x > maxX) maxX = x; maxNodeWidth = data.size.width;
        if (y > maxY) maxY = y; maxNodeHeight = data.size.height;
      }

      var appBarHeight = AppBar().preferredSize.height;

      // component全体のサイズ
      width = maxX - minX + maxNodeWidth;
      height = maxY - minY + maxNodeHeight;

      // 画面のサイズ
      screenWidth = MediaQuery.of(context).size.width - 100;
      screenHeight = MediaQuery.of(context).size.height - (appBarHeight * 2) - 100;

      // 比率
      componentRatio = height / width;
      screenRatio = screenHeight / screenWidth;

      if (screenRatio >= componentRatio) {
        result = screenWidth / width;
      } else {
        result = screenHeight / height;
      }

      // 拡大防止
      if (result > 1.0) {
        result = 1.0;
      }

      centerX = (screenWidth - (width * result)) / 2;
      centerY = (screenHeight - (height * result)) / 2;

      posX = -(minX * result) + centerX + 50;
      posY = -(minY * result) + centerY + 50;
    }
    logout("positionView: offsetX: ${posX}  offsetY:${posY}");
    canvasWriter.state.setPosition(Offset(posX, posY));
    canvasWriter.state.setScale(result ?? 1.0);
    canvasWriter.state.updateCanvas();
  }

  //コピーされたコンポーネントの中で一番左にあるコンポーネントの位置を出力
  Offset leftComponents() {
    double? left;
    double? right;
    double? top;
    double? bottom;
    ComponentData? leftComponent;
    //複数のコンポーネントの中央の位置
    Offset offset = Offset.zero;
    Offset leftPosition = offset;
    for (ComponentData? componentData in CustomStatePolicy.multipleCopiedNodeData.values.toList()) {
      if (CustomStatePolicy.multipleTimelineCopiedSelectedData.contains(componentData)) continue;

      Offset position = centerPosition(componentData!.position, componentData.size);
      if (left == null || position.dx < left) {
        left = position.dx;
        leftComponent = componentData;
      }
      if (right == null) right = position.dx;
      right = max(right, position.dx);
      if (top == null) top = position.dy;
      top = min(top, position.dy);
      if (bottom == null) bottom = position.dy;
      bottom = max(bottom, position.dy);
    }

    if (CustomStatePolicy.multipleCopiedNodeData.length != CustomStatePolicy.multipleTimelineCopiedSelectedData.length) {
      offset = Offset((right! + left!) / 2, (bottom! + top!) / 2);
    }

    int count = 0;
    for (ComponentData? componentData in CustomStatePolicy.multipleCopiedNodeData.values.toList()) {
      //タイムラインからコピーしたコンポーネントには位置がないためコンポーネントの中央の位置の右下に配置する
      if (CustomStatePolicy.multipleTimelineCopiedSelectedData.contains(componentData)) {
        count += 1;
        componentData!.position = offset + Offset(count*5, count*5);
      }
    }
    if (leftComponent != null) leftPosition = leftComponent.position;
    return leftPosition;
  }

  removeSelected() {
    multipleSelected.forEach((compId) {
      canvasWriter.model.removeComponent(compId!);
    });
    multipleSelected = [];
  }

  //複数選択モードでの複製
  duplicateMultipleCopied(Offset tapPosition) async {
    List<String> duplicated = [];
    Map<String, String> componentIdMap = {};
    Offset displacement = leftComponents() - tapPosition;

    await Future.forEach<ComponentData?>(CustomStatePolicy.multipleCopiedNodeData.values, (value) async {
      SemNodeModel model = SemNodeModel(plrId);
      model.posOffset = value!.position - displacement;
      MyComponentData myComponentData = value.data;
      RecordEntity copiedEntity = myComponentData.itemEntity!;

      //エンティティを複製して、作成者と日時を更新する
      RecordEntity recordEntity = copiedEntity.duplicate();
      recordEntity.creator = plrId;
      DateTime now = DateTime.now().toLocal();
      if (recordEntity.end != null) {
        Duration dur = recordEntity.end!.difference(recordEntity.begin!);
        recordEntity.end = now.add(dur);
      }
      recordEntity.begin = now;

      print("save start: copied node:${(value.data as MyComponentData).dispCnt}");

      //PLR書き込み
      await recordEntity.save(timeline, true, onSyncFinished: (value) async {
        print("recordEntity: ${recordEntity.id} entity.id: ${recordEntity.entity!.id} node: ${recordEntity.entity!.node.id}");
        //位置情報の書き込み
        addPosition( model, isNodeEncrypt, graph, OccurrenceType.duplicated);
      });

      model.recordEntity = recordEntity;

      addDrawNode(model);

      String nodeId = recordEntity.entity!.id!;

      //複製したノードを一時的に記憶しておく
      duplicated.add(nodeId);
      componentIdMap[myComponentData.itemEntity!.entity!.id!] = drawMap[nodeId]!;
    });
    print("CustomStatePolicy.multipleCopiedNodeData.forEach done");

    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);

    //リンクの複製（貼り付け時の情報に依存する）
    List<ComponentData> componentList = [];
    Map<String, String> drawnNode = {};
    for (LinkTypeProperty linkType in CustomStatePolicy.linkElementMap.keys) {
      componentList = [];
      ComponentData? target;
      Entity e = linkType.linkEntity!;
      List<String> sourceIdList = getSourceIdList(e);
      String? targetId = getTargetId(e);
      Iterable<ComponentData> componentDataList = canvasReader.model.getAllComponents().values;
      List<String> newIdList = [];

      //リンクの終点(arg2)
      ComponentData? targetComponent;
      for (ComponentData componentData in componentDataList) {
        if (componentData.id == componentIdMap[targetId]) targetComponent = componentData;
      }
      if (targetComponent == null || !drawMap.containsValue(targetComponent.id)) {
        if (targetId != null) {
          newIdList.add(targetId);
        }
      }
      if (targetComponent != null) {
        target = targetComponent;
        componentList.add(targetComponent);
      }

      // リンクの始点(arg1)
      for (String sourceId in sourceIdList) {
        ComponentData? sourceComponent;
        for (ComponentData componentData in componentDataList) {
          if (componentData.id == componentIdMap[sourceId]) sourceComponent = componentData;
        }
        if (sourceComponent == null || !drawMap.containsValue(sourceComponent.id)) {
          newIdList.add(sourceId);
        } else {
          componentList.add(sourceComponent);
        }
      }

      int count = 0;
      for (String? nodeId in duplicated) {
        for (ComponentData componentData in componentList) {
          if (nodeId == (componentData.data as MyComponentData).nodeId) count += 1;
        }
      }

      if (CustomStatePolicy.isCutAndPaste) {
        //コピー元グラフにあるリンクの内、いずれかの端点ノードが切り取り対象になっているリンクのみ抽出して貼り付けを行う
        if (count == 0) continue;
      } else {
        //コピー元グラフにあるリンクのうち、全ての端点ノードがコピー対象になっているリンクのみ抽出して貼り付けを行う
        if (count != ((targetId == null) ? sourceIdList.length: sourceIdList.length + 1)) continue;
      }

      for (int i = 0; i < newIdList.length; i++) {
        if (drawnNode.containsKey(newIdList[i])) {
          ComponentData component = canvasReader.model.getComponent(drawMap[drawnNode[newIdList[i]]]!);
          componentList.add(component);
          duplicated.add(drawnNode[newIdList[i]]!);
          continue;
        }
        //まだ描画されていないコンポーネントの場合は、新しく作成する
        SemNodeModel model = SemNodeModel(plrId);
        RecordEntity? copiedEntity;
        GraphElement? element = CustomStatePolicy.copyGraph!.elementByIdOf(newIdList[i]);
        await element!.syncAttributesSilently();
        await element.node.syncSilently();
        for (Schemata schemata in schemataList) {
          copiedEntity ??= RecordEntity.from(schemata, element);
        }
        RecordEntity recordEntity = copiedEntity?.duplicate() as RecordEntity;
        await recordEntity.save(timeline, true, onSyncFinished: (value) async {
          print("recordEntity: ${recordEntity.id} entity.id: ${recordEntity.entity!.id} node: ${recordEntity.entity!.node.id}");
          //位置情報の書き込み
          addPosition( model, isNodeEncrypt, graph, OccurrenceType.duplicated);
        });
        model.recordEntity = recordEntity;
        Offset position =  centerPosition(componentList.first.position, componentList.first.size);
        model.posOffset = Offset(position.dx + 20 * (i+1) + 1, position.dy + 20 * (i+1) + 1);
        addDrawNode(model);
        ComponentData component = canvasReader.model.getComponent(drawMap[recordEntity.entity!.id]!);
        componentList.add(component);
        duplicated.add(recordEntity.entity!.id!);
        drawnNode[newIdList[i]] = recordEntity.entity!.id!;
      }

      String endLabel = linkType.linkEntity!.type!;
      bool isSymmetric =linkType.isSymmetric!;
      String type = linkType.type!;
      Entity? linkEntity;

      //リンクの向きを保持する
      for (int i = 0; i < componentList.length -1; i++) {
        if (!isSymmetric) {
          ComponentData? targetComponentData = target;
          ComponentData? sourceComponentData;
          if (targetComponentData != null) {
            sourceComponentData = componentList[i];
            for (String id in componentIdMap.keys) {
              if (componentIdMap[id] == componentList[i].id) {
                sourceComponentData = componentList[i+1];
              }
            }
          } else {
            targetComponentData = componentList[i];
            sourceComponentData = componentList[i+1];
            for (String id in componentIdMap.keys) {
              if (componentIdMap[id] == componentList[i].id) {
                targetComponentData = componentList[i+1];
                sourceComponentData = componentList[i];
              }
            }
          }

          RecordEntity targetNode = (targetComponentData!.data as MyComponentData).itemEntity!;
          RecordEntity sourceNode = (sourceComponentData!.data as MyComponentData).itemEntity!;
          linkEntity = await addLink(graph, sourceNode, targetNode, endLabel, isNodeEncrypt, plrId, timeline, isSymmetric, OccurrenceType.duplicated, syncAdd: syncAdd);
        } else {
          ComponentData targetComponentData = componentList[i];
          ComponentData sourceComponentData = componentList[i+1];
          RecordEntity targetNode = (targetComponentData.data as MyComponentData).itemEntity!;
          RecordEntity sourceNode = (sourceComponentData.data as MyComponentData).itemEntity!;
          //PLR書き込み
          if (i == 0) {
            linkEntity = await addLink(graph, sourceNode, targetNode, endLabel, isNodeEncrypt, plrId, timeline, isSymmetric, OccurrenceType.duplicated, syncAdd: syncAdd);
          } else {
            branchLink(linkEntity!, sourceNode, syncAdd: syncAdd);
          }
        }
      }
      LinkTypeProperty linkTypeProperty = LinkTypeProperty(linkEntity!.id!, type: type, isSymmetric: isSymmetric, linkEntity: linkEntity);
      SchemaClass? schemaClass = getSchemaClass(linkTypeProperty);
      setHistory(schemaClass);
      linkTypeProperty.syncingCount += componentList.length -1;

      ComponentData newLabel = drawLink(componentList, [linkTypeProperty], true)!;
      setLinkLabelPosition(newLabel);
      newLabel.updateComponent();

      for (Connection link in newLabel.connections) {
        canvasWriter.model.updateLink(link.connectionId);
      }
    }
    //選択状態を解除
    hideAllHighlights();

    //複製されたノードを選択状態にする。
    duplicated.forEach((nodeId) {
      addComponentToMultipleSelection(drawMap[nodeId]);
      highlightComponent(drawMap[nodeId]!);
      canvasWriter.model.moveComponentToTheFront(drawMap[nodeId]!);
    });

    //ノードとリンクを切り取る
    if (CustomStatePolicy.isCutAndPaste) {
      multipleCut();
    }
  }

  //複数選択モードでの共有（貼り付け時の情報に依存する）
  shareMultipleCopied(Offset tapPosition) async {
    List<String> shared = [];
    hideAllLinkHighlights();
    Offset displacement = leftComponents() - tapPosition;

    await Future.forEach<ComponentData?>(CustomStatePolicy.multipleCopiedNodeData.values, (value) async {
      SemNodeModel model = SemNodeModel(plrId);
      MyComponentData myComponentData = value?.data;
      RecordEntity recordEntity = myComponentData.itemEntity!;
      String nodeId = recordEntity.entity!.id!;
      //現キャンバスに存在するノードはスキップ。
      if (drawMap[nodeId] != null) {
        return;
      }

      model.posOffset = value!.position - displacement;

      //PLR書き込み
      addPosition(model, isNodeEncrypt, graph, recordEntity: recordEntity, OccurrenceType.shared);
      model.recordEntity = recordEntity;

      if (syncingList.contains(nodeId) || syncingList.contains(parseId(nodeId))) {
        syncingList.remove(nodeId);
        syncingList.remove(parseId(nodeId));
      }

      addDrawNode(model);

      //共有したノードを一時的に記憶しておく
      shared.add(nodeId);

    });

    print("SHARE CustomStatePolicy.multipleCopiedNodeData.forEach done");

    List<ComponentData> componentList = [];

    for (LinkTypeProperty linkType in CustomStatePolicy.linkElementMap.keys) {
      componentList = [];
      Entity e = linkType.linkEntity!;
      GraphElement? element = CustomStatePolicy.linkElementMap[linkType];

      List<String> sourceIdList = getSourceIdList(e);
      String? targetId = getTargetId(e);

      Iterable<ComponentData> componentDataList = canvasReader.model.getAllComponents().values;
      // リンクの始点(arg1)
      for (String sourceId in sourceIdList) {
        ComponentData? sourceComponent;
        for (ComponentData componentData in componentDataList) {
          if (componentData.id == drawMap[sourceId]) sourceComponent = componentData;
        }
        if (sourceComponent == null || !drawMap.containsValue(sourceComponent.id)) continue;
        componentList.add(sourceComponent);
      }
      //リンクの終点(arg2)
      ComponentData? targetComponent;
      for (ComponentData componentData in componentDataList) {
        if (componentData.id == drawMap[targetId]) targetComponent = componentData;
      }
      if (!(targetComponent == null || !drawMap.containsValue(targetComponent.id))) {
        componentList.add(targetComponent);
      }

      int count = 0;
      for (String? nodeId in CustomStatePolicy.multipleCopiedSelectedNodeId) {
        for (ComponentData componentData in componentList) {
          if (nodeId == (componentData.data as MyComponentData).nodeId) count += 1;
        }
      }

      if (CustomStatePolicy.isCutAndPaste) {
        //コピー元グラフにあるリンクの内、いずれかの端点ノードが切り取り対象になっているリンクのみ抽出して貼り付けを行う
        if (count == 0) continue;
      } else {
        //コピー元グラフにあるリンクのうち、全ての端点ノードがコピー対象になっているリンクのみ抽出して貼り付けを行う
        if (count != ((targetId == null) ? sourceIdList.length: sourceIdList.length + 1)) continue;
      }

      SchemaClass? schemaClass = getSchemaClass(linkType);
      setHistory(schemaClass);
      addElementAndSync(graph, element!, OccurrenceType.shared, syncAdd: syncAdd);

      if (componentList.length < 2) continue;
      linkType.syncingCount += 1;
      ComponentData newLabel = drawLink(componentList, [linkType], true)!;
      if ((newLabel.data as MyComponentData).typeList?.any((value) => value.elementId == linkType.elementId) == false) {
        linkType.syncingCount -= 1;
      }

      setLinkLabelPosition(newLabel);
      newLabel.updateComponent();
      for (Connection link in newLabel.connections) {
        canvasWriter.model.updateLink(link.connectionId);
      }
    }

    //選択状態を解除
    hideAllHighlights();

    //共有されたノードを選択状態にする。
    shared.forEach((nodeId) {
      addComponentToMultipleSelection(drawMap[nodeId]);
      highlightComponent(drawMap[nodeId]!);
      canvasWriter.model.moveComponentToTheFront(drawMap[nodeId]!);
    });

    //ノードとリンクを切り取る
    if (CustomStatePolicy.isCutAndPaste) {
      multipleCut();
    }
    drawCutLink();
  }

  //複製可能条件
  bool get canDuplicateMultipleCopied {
    // 削除されていないコピーノードが1つ以上ある。
    return CustomStatePolicy.multipleCopiedSelectedNodeId.isNotEmpty;
  }

  //共有可能条件
  bool get canShareMultipleCopied {
    // 削除されていないコピーノードの内、現キャンバスに存在しないノードが1つ以上ある。
    // スキーマによる非表示ノードも検索対象。
    int count = 0;
    for (LinkTypeProperty linkTypeProperty in CustomStatePolicy.linkElementMap.keys) {
      for (ComponentData componentData in canvasReader.model.getAllComponents().values) {
        if ((componentData.data as MyComponentData).isLinkLabel) {
          for (LinkTypeProperty linkType in (componentData.data as MyComponentData).typeList!) {
            if (linkTypeProperty.elementId == linkType.elementId) {
              count += 1;
              break;
            }
          }
        }
      }
    }

    bool isAllLink = count == CustomStatePolicy.linkElementMap.length;
    return CustomStatePolicy.multipleCopiedSelectedNodeId.isNotEmpty && (CustomStatePolicy.multipleCopiedSelectedNodeId.any((nodeId) => (nodeId != null && !drawMap.containsKey(nodeId) && !drawMap.containsKey(parseId(nodeId))) || !isAllLink));
  }

  //複数コピー可能条件
  bool get canCopyMultiple {
    // 削除されていないコピーノードの内、現キャンバスに存在しないノードが1つ以上ある。
    // スキーマによる非表示ノードも検索対象。
    return multipleSelected.isNotEmpty;
  }

  bool get canCutMultiple {
    return multipleSelected.isNotEmpty;
  }

  static resetClipboard() {
    //コピー内容をクリア
    CustomStatePolicy.multipleCopiedSelectedNodeId = [];
    CustomStatePolicy.drawCutNodeId = [];
    CustomStatePolicy.multipleTimelineCopiedSelectedData = [];
    CustomStatePolicy.multipleCopiedNodeData = {};
    CustomStatePolicy.linkElementMap = {};
    CustomStatePolicy.isDoneCut = false;
  }

  static resetClipboardTimeline() {
    resetClipboard();
    CustomStatePolicy.copyGraph = null;
    CustomStatePolicy.isCutAndPaste = false;
  }

  resetClipboardGraph() {
    resetClipboard();
    CustomStatePolicy.copyGraph = graph;
    duplicateStreamController.sink.add(true);

    //切り取りの色を戻す
    drawCutLink();
  }

  //複数選択モードでのコピー
  bool multipleCopySelected() {
    resetClipboardGraph();

    //コピーできない
    if(multipleSelected.length == 0) return false;

    //選択中のノードをコピー
    multipleSelected.forEach((componentId) {
      ComponentData componentData = canvasReader.model.getComponent(componentId!);
      String nodeId = (componentData.data as MyComponentData).nodeId!;
      CustomStatePolicy.multipleCopiedSelectedNodeId.add(nodeId);
      CustomStatePolicy.multipleCopiedNodeData[nodeId] = componentData;
    });

    if (CustomStatePolicy.isCutAndPaste) {
      //選択されたノードに接続されたリンクをコピー
      for (String? nodeId in CustomStatePolicy.multipleCopiedSelectedNodeId) {
        CustomStatePolicy.drawCutNodeId.add(nodeId);
        ComponentData componentData = canvasReader.model.getComponent(drawMap[nodeId]!);
        MyComponentData data = (componentData.data as MyComponentData);
        data.color = data.color.withOpacity(0.3);
        data.borderColor = data.borderColor.withOpacity(0.1);
        for (Connection link in componentData.connections) {
          String labelId = link.otherComponentId;
          ComponentData linkLabel = canvasReader.model.getComponent(labelId);
          for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
            GraphElement? element = getElement(graph,linkType.elementId);
            CustomStatePolicy.linkElementMap[linkType] = element;
          }

        }
      }
    } else {
      //全ての端点が選択されているリンクをコピー
      Set<String> linkLabelIdSet = {};
      Map<String, int> labelIdMap = {};
      for (ComponentData? componentData in CustomStatePolicy.multipleCopiedNodeData.values) {
        for (Connection link in componentData!.connections) {
          String labelId = link.otherComponentId;
          if (labelIdMap.containsKey(labelId)) {
            labelIdMap[labelId] = labelIdMap[labelId]! + 1;
          } else {
            labelIdMap[labelId] = 1;
          }
        }
      }
      for (String labelId in labelIdMap.keys) {
        if (labelIdMap[labelId] == canvasReader.model.getComponent(labelId).connections.length) linkLabelIdSet.add(labelId);
      }

      for (String labelId in linkLabelIdSet) {
        ComponentData linkLabel = canvasReader.model.getComponent(labelId);
        for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
          GraphElement? element = getElement(graph, linkType.elementId);
          CustomStatePolicy.linkElementMap[linkType] = element;
        }
      }
    }
    hideAllHighlights();
    drawCutLink();

    return true;
  }

  //切り取り
  multipleCut() async {
    CustomStatePolicy.drawCutNodeId = [];
    if (!CustomStatePolicy.isDoneCut) {
      Set<String> linkLabelIdSet = {};
    await Future.forEach<String?>(CustomStatePolicy.multipleCopiedSelectedNodeId, (nodeId) async {
      ComponentData componentData = CustomStatePolicy.multipleCopiedNodeData[nodeId]!;

      if (drawMap.containsValue(componentData.id)) {
        if (drawMap.containsKey((componentData.data as MyComponentData).itemEntity!.entity!.id!)) {
          List<Connection> connectionList = [];
          connectionList += componentData.connections;
          for (Connection link in connectionList) {
            String labelId = link.otherComponentId;
            if (linkLabelIdSet.contains(labelId)) continue;
            ComponentData linkLabel = canvasReader.model.getComponent(labelId);
            canvasWriter.model.removeLink(link.connectionId);
            List<String> deleteLinkList = [];
            for (Connection link in linkLabel.connections) {
              deleteLinkList.add(link.connectionId);
            }
            for (String linkId in deleteLinkList) {
              canvasWriter.model.removeLink(linkId);
            }

            for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
              syncingList.add(linkType.elementId);
            }

            canvasWriter.model.removeComponent(labelId);
            linkLabelIdSet.add(labelId);
          }
        }
        selectedComponentId = null;
        //複数選択から削除
        removeComponentFromMultipleSelection(componentData.id);

        canvasWriter.model.removeComponent(componentData.id);
        drawMap.remove(nodeId);
      }
      syncingList.add(nodeId!);
      deleteNode(CustomStatePolicy.copyGraph!, (componentData.data as MyComponentData).itemEntity!, true, timeline, syncDelete: syncDelete);
    });
    CustomStatePolicy.isDoneCut = true;
    }
  }

  selectAll() {
    //全選択時は複数選択モードに移行
    turnOnMultipleSelection();

    var components = canvasReader.model.canvasModel.components.keys;
    hideAllLinkHighlights();
    components.forEach((componentId) {
      if (!(canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) {
        addComponentToMultipleSelection(componentId);
        highlightComponent(componentId);
      }
    });
  }
}
