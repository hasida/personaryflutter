import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:personaryFlutter/dialog/api_dialog/api_dialog.dart';
import 'package:personaryFlutter/semeditor/policy/load_plr_policy.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';
import 'package:diagram_editor/src/widget/component.dart';

import '../../constants.dart';
import '../../util/position.dart';
import '../../widget/property_tooltip.dart';
import '../data/custom_link_data.dart';
import '../logic/sem_logic.dart';
import '../model/sem_node_model.dart';
import '../data/custom_component_data.dart';
import '../widget/option_icon.dart';
import '../page/semedit_main.dart';
import 'component_widgets_policy.i18n.dart';
import 'custom_policy.dart';

mixin MyComponentWidgetsPolicy
    implements ComponentWidgetsPolicy, CustomStatePolicy, LoadPlrPolicy, CustomBehaviourPolicy{

  StreamController<bool> get loadAiStreamController;
  Account get account;
  Uint8List? picture;

  @override
  Widget showCustomWidgetWithComponentDataUnder(
      BuildContext context, ComponentData componentData) {

    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
    MyComponentData myComponentData = (componentData.data as MyComponentData);
    if (myComponentData.isLinkLabel) {
      for (LinkTypeProperty linkType in myComponentData.typeList!) {
        SchemaClass? schemaClass;
        for (Schemata schemata in schemataList) {
          schemaClass = schemata.classOf(linkType.linkEntity?.type);
          if (schemaClass != null) break;
        }
        linkType.tooltip = schemaClass?.tooltip?.valueFor(currentLanguage);
        ({String? subject, String? object}) tooltipNode = getTooltipNode(componentData, linkType.elementId);
        linkType.subject = tooltipNode.subject;
        linkType.object = tooltipNode.object;
      }
    }

    Offset componentPosition =
    canvasReader.state.toCanvasCoordinates(componentData.position);
    //Offset componentPosition = canvasReader.state.toCanvasCoordinates(
    //   componentData.position + componentData.size.bottomLeft(Offset.zero)-Offset(0,0));
    MyComponentData data = componentData.data;
    if (!data.isMe) {
      return ChangeNotifierProvider<RecordEntityCreatorNotifier>(
          key: Key('component_widget_provider_${data.itemEntity!.creator}'),
          create: (_) => RecordEntityCreatorNotifier(
            data.itemEntity!, timeline: timeline,
          )..update(),
        child: Consumer<RecordEntityCreatorNotifier>(
          builder: (context, notifier, child) {
            if (notifier.picture != null) picture = notifier.picture;
            return Positioned(
                  left: componentPosition.dx - 35 * canvasReader.state.scale,
                  top: componentPosition.dy + 5 * canvasReader.state.scale,
                  child: Row(children: [
                    data.itemEntity!.creator == creatorGPT4
                    ? Image.asset(
                      "assets/gpt4.webp",
                      height: MyComponentData.profileIconSize * canvasReader.state.scale,
                      width: MyComponentData.profileIconSize * canvasReader.state.scale,
                      fit: BoxFit.contain,
                      gaplessPlayback: true,
                    )
                        : picture == null
                        ? Image.asset(
                      "assets/user-img-dammy.jpg",
                      height: MyComponentData.profileIconSize * canvasReader.state.scale,
                      width: MyComponentData.profileIconSize * canvasReader.state.scale,
                      fit: BoxFit.contain,
                      gaplessPlayback: true,
                    )
                        : Image.memory(
                      picture!,
                      height: MyComponentData.profileIconSize * canvasReader.state.scale,
                      width: MyComponentData.profileIconSize * canvasReader.state.scale,
                      fit: BoxFit.contain,
                      gaplessPlayback: true,
                    ),
                  ])
              );
          }));
    }
    return Container();
  }

  @override
  Widget showCustomWidgetWithComponentDataOver(BuildContext context, ComponentData componentData) {
    // bool isJunction = componentData.type == 'junction';
    // bool showOptions = (!isMultipleSelectionOn) && (!isReadyToConnect) && !isJunction;
    bool isLastMultipleSelected =  multipleSelected.isNotEmpty && multipleSelected.last == componentData.id;

    if (longPressLinkLabelId == componentData.id) return linkLabelTooltip(componentData);
    if ((componentData.data as MyComponentData).isLinkLabel) hideLink(componentData);

    return Visibility(
      visible: componentData.data.isHighlightVisible,
      child: Stack(
        children: [
          if (isLastMultipleSelected) componentTopOptions(componentData, context),
          if (isLastMultipleSelected) componentBottomOptions(componentData),
          frontComponent(componentData),
          highlight(
              componentData, isMultipleSelectionOn ? (componentData.id == selectedComponentId ? Colors.green : Colors.cyan) : Colors.red),
          // if (showOptions) resizeCorner(componentData),
          //if (isJunction && !isReadyToConnect) junctionOptions(componentData),
        ],
      ),
    );
  }

  //コンポーネントを前面に描画する
  Widget frontComponent(ComponentData componentData) {
    return ChangeNotifierProvider<ComponentData>.value(
      value: componentData,
      child: Component(
        policy: this.diagramEditorContext.policySet,
      ),
    );
  }

  //リンクラベルの接続部分がリンクラベルの内部にある場合
  void hideLink(ComponentData linkLabel) {
    if (linkLabel.connections.length !=2) return;
    int count = 0;
    for (Connection con in linkLabel.connections) {
      Offset point = Offset.zero;
      LinkData linkData = canvasReader.model.getLink(con.connectionId);
      ComponentData componentData = canvasReader.model.getComponent(linkData.sourceComponentId);
      if ((componentData.data as MyComponentData).isLinkLabel) {
        point = linkData.linkPoints.last;
      } else {
        point = linkData.linkPoints.first;
      }
      Offset position = centerPosition(linkLabel.position, linkLabel.size);
      double dx = (position.dx - point.dx).abs();
      double dy = (position.dy - point.dy).abs();
      if (dx < linkLabel.size.width/2) {
        if (dy < linkLabel.size.height/2) {
          count += 1;
        }
      }
    }
    if (count == 2) {
      for (Connection con in linkLabel.connections) {
        LinkData linkData = canvasReader.model.getLink(con.connectionId);
        linkData.linkStyle.color = Colors.transparent;
      }
    } else {
      for (Connection con in linkLabel.connections) {
        LinkData linkData = canvasReader.model.getLink(con.connectionId);
        Color c = Colors.black;
        if ((linkData.data as MyLinkData).isHighlightVisible) {
          c = Colors.red;
        }
        if ((linkData.data as MyLinkData).isCutLink) {
          c = c.withOpacity(0.3);
        }
        linkData.linkStyle.color = c;
      }
    }
  }

  Widget componentTopOptions(ComponentData componentData, context) {
    Offset componentPosition = canvasReader.state.toCanvasCoordinates(componentData.position);
    return Positioned(
      left: componentPosition.dx - 24,
      top: componentPosition.dy - 48,
      child: Row(
        children: [
          Visibility(
            visible: !isReadyToConnect,
            child: OptionIcon(
              color: Colors.red.withOpacity(0.7),
              iconData: Icons.delete_forever,
              tooltip: 'delete'.i18n,
              size: 40,
              onPressed: () async {
                showSelected(); //debug
                // 確認ダイアログ
                var ret = await showConfirmDialog(context, 'Confirm'.i18n,
                    'Are you sure to delete this node?'.i18n);
                if (ret) {
                  //選択されたノードのリストを作成
                  List<String?> selectedIdList = multipleSelected;
                  if (selectedIdList.length == 0) selectedIdList.add(selectedComponentId);
                  multipleSelected = [];
                  selectedComponentId = null;
                  for (String? id in selectedIdList) {
                    ComponentData componentData = canvasReader.model.getComponent(id!);
                    RecordEntity delNode = (componentData.data as MyComponentData).itemEntity!;
                    // PLR
                    deleteNode(graph, delNode, true, timeline, syncDelete: syncDelete);
                    String deleteNodeId = delNode.entity!.id!;

                    List<Connection> connectionList = [];
                    connectionList += componentData.connections;
                    for (Connection link in connectionList) {
                      if (!canvasReader.model.componentExist(link.otherComponentId)) continue;
                      ComponentData linkLabel = canvasReader.model.getComponent(link.otherComponentId);

                      List<String> deleteLinkList = [];
                      for (Connection link in linkLabel.connections) {
                        deleteLinkList.add(link.connectionId);
                      }
                      for (String linkId in deleteLinkList) {
                        canvasWriter.model.removeLink(linkId);
                      }
                      canvasWriter.model.removeComponent(linkLabel.id);

                      for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
                        syncingList.add(linkType.elementId);
                      }

                    }
                    canvasWriter.model.removeComponent(id);
                    syncingList.add(delNode.id);
                    selectedComponentId = null;
                    //複数選択から削除
                    removeComponentFromMultipleSelection(id);
                    // drawMapから削除
                    drawMap.remove(deleteNodeId);
                    //　古いノードがあれば削除
                    drawMap.remove(parseId(deleteNodeId));

                    //コピーノードから削除
                    if (CustomStatePolicy.copyGraph?.id == graph.id) {
                      CustomStatePolicy.multipleCopiedSelectedNodeId.remove(deleteNodeId);
                      CustomStatePolicy.multipleCopiedNodeData.remove(deleteNodeId);
                    }
                  }
                  drawCutLink();
                } else {
                  // キャンセル時、ノード選択も解除する
                  hideAllHighlights();
                }
                showSelected(); //debug
              },
            ),
          ),
          /*
          SizedBox(width: 12),
          OptionIcon(
            color: Colors.grey.withOpacity(0.7),
            iconData: Icons.copy,
            tooltip: 'duplicate',
            size: 40,
            onPressed: () {
              String newId = duplicate(componentData);
              canvasWriter.model.moveComponentToTheFront(newId);
              selectedComponentId = newId;
              hideComponentHighlight(componentData.id);
              highlightComponent(newId);
            },
          ),
          */
          Visibility(
            visible: !isReadyToConnect,
            child: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: OptionIcon(
                color: Colors.grey.withOpacity(0.7),
                iconData: Icons.edit,
                tooltip: 'edit'.i18n,
                size: 40,
                //onPressed: () => showEditComponentDialog(context, componentData),
                onPressed: () async {
                  MyComponentData myComponentData = componentData.data;

                  // entityDialogの戻り値判断用
                  // recordEntityのIDが同じなら変更、変わっている場合は新規作成
                  RecordEntity itemEntity = myComponentData.itemEntity!;
                  String? currentId = itemEntity.entity!.id;

                  /// 所有者判定
                  /// 自分が所有者であればtrue、それ以外の場合はfalseを返す
                  bool isCreator = (myComponentData.isMe);
                  // TODO: スキーマの選択機能をSemanticEditorにも適用
                  RecordEntity? rootRecordEntity = RecordEntity.create(rootGraphSchemata, "Event");
                  RecordEntity? othersRecordEntity = RecordEntity.create(timelineSchemata, "Event");
                  RecordEntity? recordEntity = await showDialog(
                    context: context,
                    builder: (context) => EntityDialog(
                        account, EntityDialogSettings(), timeline, itemEntity, false, isCreator,
                        isCheckSemEditor: true,
                        isTopGraph : isTopGraph,
                        othersRecordEntity: othersRecordEntity,
                        rootRecordEntity: rootRecordEntity
                    ),
                  );

                  // 選択ノード解除
                  hideAllHighlights();

                  if (recordEntity != null) {
                    // 同期
                    recordEntity.entity!.syncSilently().then((value) => null);
                      timeline.syncSilently().then((value) => null);

                    Offset offset;
                    SemNodeModel model = SemNodeModel(plrId);
                    if (currentId != recordEntity.entity!.id) {
                      // Idが異なる場合は、ノード新規作成

                      // 新規作成の場合ここでRecordEntityをつくりなおさないと、
                      // SemNodeModel内の処理でmmdataの値がとれない
                      // 原因は後で調査したほうがいいかも
                      //recordEntity = RecordEntity.from(graphSchemata, recordEntity.entity);
                      recordEntity = RecordEntity.from(timelineSchemata, recordEntity.entity!);

                      // 元の座標の右下に新ノードを作成する
                      offset = centerPosition(componentData.position, componentData.size) + const Offset(10, 10);
                      addPosition(model, isNodeEncrypt, graph, OccurrenceType.created);
                    } else {
                      offset = centerPosition(componentData.position, componentData.size);
                      // 元のノードの位置Entity取得
                    }

                    model.posOffset = offset;
                    model.recordEntity = recordEntity;

                    // 描画ノードの追加または更新
                    addDrawNode(model);

                    // ノードが空の場合は削除する
                    if (myComponentData.dispClass!.isEmpty && myComponentData.dispCnt!.isEmpty && myComponentData.dispCntImage == null) {
                      canvasWriter.model.removeComponent(componentData.id);
                      selectedComponentId = null;
                      //選択から削除
                      removeComponentFromMultipleSelection(componentData.id);

                      syncingList.add(myComponentData.nodeId!);

                      //PLR
                      deleteNode(graph, recordEntity!, false, timeline, syncDelete: syncDelete);

                      List<String> linkIdList = [];
                      for (Connection link in componentData.connections) {
                        linkIdList.add(link.connectionId);
                      }
                      for (String linkId in linkIdList) {
                        canvasWriter.model.removeLink(linkId);
                      }

                      drawMap.remove(recordEntity.entity!.id);
                    }
                  } else {
                    // entity_dialogでキャンセルされた場合
                    // 何もしない
                  }
                  showSelected(); //debug
                },
              ),
            ),
          ),
          /*
          OptionIcon(
            color: Colors.grey.withOpacity(0.7),
            iconData: Icons.link_off,
            tooltip: 'remove links',
            size: 40,
            onPressed: () =>
                canvasWriter.model.removeComponentConnections(componentData.id),
          ),
          */
          Visibility(
            visible: canCopyMultiple && !isReadyToConnect,
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: OptionIcon(
                tooltip: 'copy'.i18n,
                color: Colors.grey.withOpacity(0.7),
                iconData: Icons.copy,
                onPressed: () {
                      CustomStatePolicy.isCutAndPaste = false;
                      bool ret = multipleCopySelected();
                      //Toast表示
                      if(ret) {
                        showMessage(context, 'The node has been memorized in the clipboard.'.i18n);
                      }
                    }
                    ),
            ),
          ),
          Visibility(
            visible: canCutMultiple && !isReadyToConnect,
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: OptionIcon(
                tooltip: 'cut and paste'.i18n,
                color: Colors.grey.withOpacity(0.7),
                iconData: Icons.cut,
                onPressed: () {
                      CustomStatePolicy.isCutAndPaste = true;
                      bool ret = multipleCopySelected();
                      //Toast表示
                      if(ret) {
                        showMessage(context, 'The node has been memorized in the clipboard.'.i18n);
                      }
                    },
              ),
            ),
          ),
          Visibility(
            visible: !isReadyToConnect,
            child: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: OptionIcon(
                color: Colors.grey.withOpacity(0.7),
                iconData: Icons.launch,
                tooltip: 'internal graph'.i18n,
                size: 40,
                onPressed: () async {
                  RecordEntity itemEntity =
                  (componentData.data as MyComponentData).itemEntity!;
                  Entity? hyperNodeItem = itemEntity.entity;
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                      settings: RouteSettings(name: hyperNodeItem?.id),
                      builder: (context) => SemEditMain.hyperNode(
                          account, timeline, hyperNodeItem as Item, graphItem, false),
                    ),
                  );
                  getSchemata();
                },
              ),
            ),
          ),
          Visibility(
            visible: (selectedComponentId != null || multipleSelected.length == 1)
                && (componentData.data as MyComponentData).itemEntity?.type.id == "M"
                && componentData.connections.length != 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: StreamBuilder<bool>(
                stream: loadAiStreamController.stream,
                builder: (_, snapshot){
                  return OptionIcon(
                      color: Colors.grey.withOpacity(0.7),
                      iconData: CustomStatePolicy.isLoadAi ? Icons.loop : Icons.add,
                      tooltip: CustomStatePolicy.isLoadAi ? 'Generating'.i18n : 'Generated by AI'.i18n,
                      size: 40,
                      onPressed: CustomStatePolicy.isLoadAi ? null : () async {
                        if (CustomStatePolicy.isLoadAi == true) return;
                        CustomStatePolicy.isLoadAi = true;
                        loadAiStreamController.sink.add(true);

                        const String prefsApiKey = "OPEN_AI_API_KEY";
                        bool ret = await apiDialog(context, prefsApiKey);

                        List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
                        //AIによるグラフノードの自動生成
                        if (ret) {
                          bool isCreate = await createAI(componentData.id, 5, graph, schemataList, plrId, timelineSchemata, timeline, isNodeEncrypt, canvasReader);
                          if (!isCreate) showMessage(context,'Generated failed'.i18n);
                        }
                        CustomStatePolicy.isLoadAi = false;
                        loadAiStreamController.sink.add(false);
                      }
                  );
                },
              ) ,
            ),
          ),
        ],
      ),
    );
  }

  Widget componentBottomOptions(ComponentData componentData) {
    Offset componentBottomLeftCorner = canvasReader.state.toCanvasCoordinates(
        componentData.position + componentData.size.bottomLeft(Offset.zero));

    MyComponentData? data = componentData.data;
    // リンクボタン描画判定
    // TODO graphSchemata、timelineSchemataにgraphRelationのメンバが存在するか判定
    // のちに。リンク元のクラスで判定する必要あり
    bool isDispLinkButton = true;
    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);
    if (schemataList.length == 0) {
      isDispLinkButton = false;
    }

    return ChangeNotifierProvider<RecordEntityCreatorNotifier>(
        create: (_) => RecordEntityCreatorNotifier(
      data!.itemEntity!, timeline: timeline,
    )..update(),
    child: Consumer<RecordEntityCreatorNotifier>(
     builder: (context, notifier, child) => Positioned(
      left: componentBottomLeftCorner.dx - 16,
      top: componentBottomLeftCorner.dy + 8,
      child: Row(
        children: [
          /*
        OptionIcon(
          color: Colors.grey.withOpacity(0.7),
          iconData: Icons.arrow_upward,
          tooltip: 'bring to front',
          size: 24,
          shape: BoxShape.rectangle,
          onPressed: () =>
              canvasWriter.model.moveComponentToTheFront(componentData.id),
        ),
        const SizedBox(width: 12),
        OptionIcon(
          color: Colors.grey.withOpacity(0.7),
          iconData: Icons.arrow_downward,
          tooltip: 'move to back',
          size: 24,
          shape: BoxShape.rectangle,
          onPressed: () =>
              canvasWriter.model.moveComponentToTheBack(componentData.id),
        ),
        const SizedBox(width: 40),
        */
          // ノードの所有者表示
          Container(
            width: 120,
            child:
            data!.isMe
               ? Text('Me'.i18n) : Text(notifier.name ??
                ((data.itemEntity!.creator == creatorGPT4) ? creatorGPT4 : ''))
          ),
          const SizedBox(width: 10),
          if (isDispLinkButton)
          Visibility(
            visible: !isReadyToConnect,
            child: OptionIcon(
              color: Colors.grey.withOpacity(0.7),
              iconData: Icons.arrow_right_alt,
              tooltip: 'connect'.i18n,
              size: 40,
              onPressed: () {
                isReadyToConnect = true;
                componentData.updateComponent();
                selectedComponentId = componentData.id;
                showSelected();
                //Toast表示
                showMessage(context, 'A link will be created. To specify the destination node, either choose an existing node or create a new node by long-pressing the background.'.i18n);
              },
            ),
          ),
        ],
      ),
    )));
  }

  Widget highlight(ComponentData componentData, Color color) {
    return Positioned(
      left: canvasReader.state
          .toCanvasCoordinates(componentData.position - const Offset(MyComponentData.highlightPosition, MyComponentData.highlightPosition))
          .dx,
      top: canvasReader.state
          .toCanvasCoordinates(componentData.position - const Offset(MyComponentData.highlightPosition, MyComponentData.highlightPosition))
          .dy,
      child: GestureDetector(
        onTapDown: (TapDownDetails details) {
          onComponentTapDown(componentData.id, details);
        },
        onTapUp: (TapUpDetails details) {
          onComponentTapUp(componentData.id, details);
        },
        onTap: () {
          onComponentTap(componentData.id);
        },
        onScaleStart:(ScaleStartDetails details) {
          onComponentScaleStart(componentData.id, details);
        },
        onScaleUpdate:(ScaleUpdateDetails details) {
          onComponentScaleUpdate(componentData.id, details);
        },
        onScaleEnd:(ScaleEndDetails details) {
          onComponentScaleEnd(componentData.id, details);
        },
        child: CustomPaint(
          painter: ComponentHighlightPainter(
            width: (componentData.size.width + MyComponentData.highlightPosition * 2) * canvasReader.state.scale,
            height: (componentData.size.height + MyComponentData.highlightPosition * 2) * canvasReader.state.scale,
            color: color,
          ),
          child: Container(
            width: (componentData.size.width + MyComponentData.highlightPosition * 2) * canvasReader.state.scale,
            height: (componentData.size.height + MyComponentData.highlightPosition * 2) * canvasReader.state.scale,
          ),
        ),
      ),
    );
  }

  resizeCorner(ComponentData componentData) {
    Offset componentBottomRightCorner = canvasReader.state.toCanvasCoordinates(
        componentData.position + componentData.size.bottomRight(Offset.zero));
    return Positioned(
      left: componentBottomRightCorner.dx - 12,
      top: componentBottomRightCorner.dy - 12,
      child: GestureDetector(
        onPanUpdate: (details) {
          canvasWriter.model.resizeComponent(
              componentData.id, details.delta / canvasReader.state.scale);
          canvasWriter.model.updateComponentLinks(componentData.id);
        },
        child: MouseRegion(
          cursor: SystemMouseCursors.resizeDownRight,
          child: Container(
            width: 24,
            height: 24,
            color: Colors.transparent,
            child: Center(
              child: Container(
                width: 8,
                height: 8,
                decoration: BoxDecoration(
                  color: Colors.black,
                  border: Border.all(color: Colors.grey[200]!),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget linkLabelTooltip (ComponentData componentData) {
    LinkTypeProperty? longPressType = CustomStatePolicy.longPressType;
    Offset position = canvasReader.state.toCanvasCoordinates(Offset(componentData.position.dx,
        componentData.position.dy + componentData.size.height + 30));
    return Positioned(
      left: position.dx,
      top: position.dy,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
          ),
          color: Colors.white,
        ),
        child: PropertyTooltipText(
          longPressType?.tooltip ?? "",
          longPressType?.subject ?? "",
          longPressType?.object ?? "",
        ),
      ),
    );
  }

  // junction はセマンティックエディタでは使わない
  /*
  Widget junctionOptions(ComponentData componentData) {
    Offset componentPosition =
        canvasReader.state.toCanvasCoordinates(componentData.position);
    return Positioned(
      left: componentPosition.dx - 24,
      top: componentPosition.dy - 48,
      child: Row(
        children: [
          OptionIcon(
            color: Colors.grey.withOpacity(0.7),
            iconData: Icons.delete_forever,
            tooltip: 'delete',
            size: 32,
            onPressed: () {
              canvasWriter.model.removeComponent(componentData.id);
              selectedComponentId = null;
            },
          ),
          const SizedBox(width: 8),
          OptionIcon(
            color: Colors.grey.withOpacity(0.7),
            iconData: Icons.arrow_right_alt,
            tooltip: 'connect',
            size: 32,
            onPressed: () {
              isReadyToConnect = true;
              componentData.updateComponent();
            },
          ),
        ],
      ),
    );
  }
  */
}
