import 'package:flutter/material.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:personaryFlutter/util/misc.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:collection/collection.dart' show SetEquality;

import '../../util/position.dart';
import '../dialog/edit_linktype_dialog.dart';
import '../logic/sem_logic.dart';
import '../data/custom_component_data.dart';
import '../model/sem_link_model.dart';
import 'custom_policy.dart';

mixin MyComponentPolicy implements ComponentPolicy, CustomStatePolicy {
  late TapDownDetails detail;
  //detail.globalPositionとlocalPositionの差
  late Offset displacement = (editorKey.currentContext!.findRenderObject() as RenderBox).localToGlobal(Offset.zero);
  //意図していない動きをしているかどうか
  late bool isNotIntend;

  @override
  onComponentTapDown(String componentId, TapDownDetails details) {
    detail = details;
  }

  @override
  onComponentTapUp(String componentId, TapUpDetails details) {
    if (canvasReader.model.canvasModel.linkExists(componentId)) return ;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) {
      ComponentData linkLabel = canvasReader.model.getComponent(componentId);
      linkLabel.data.isEditing = true;
      onLinkTapUp(linkLabel.id, details);
    }
  }

  @override
  onComponentTap(String componentId) async{
    if (canvasReader.model.canvasModel.linkExists(componentId)) return;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) return;

    ComponentData componentData = canvasReader.model.getComponent(componentId);
    Offset componentPosition = componentData.position;
    Offset localPosition = canvasReader.state.toCanvasCoordinates(componentPosition) + detail.localPosition;
    if ((componentData.data as MyComponentData).isHighlightVisible) localPosition -= const Offset(MyComponentData.highlightPosition, MyComponentData.highlightPosition) * canvasReader.state.scale;
    Offset globalPosition = detail.globalPosition - displacement;

    //　意図しない地点がタップされた場合に無視する
    if ( 1 < (localPosition - globalPosition).distance) {
      isNotIntend = true;
      return;
    } else {
      isNotIntend = false;
    }

    hideLinkOption();
    if (multipleSelected.contains(componentId)) {
      removeComponentFromMultipleSelection(componentId);
      hideComponentHighlight(componentId);
      if (selectedComponentId == componentId) {
        isReadyToConnect = false;
        selectedComponentId = null;
      }
    } else {
      if (isMultipleSelectionOn) {
        addComponentToMultipleSelection(componentId);
      } else {
        addComponentSingle(componentId);
      }
      highlightComponent(componentId);
    }

    if (isReadyToConnect) {
      connectComponents(selectedComponentId, componentId).then((connected) {
        if (connected) {
          selectedComponentId = null;
          isReadyToConnect = false;
          hideAllHighlights();
        }
      });
    }

    if (isReadyToBranch) {
      createBranch(componentId);
    }
    if (isReadyToReconnectLink) {
      drawReconnect(componentId);
    }
    hideAllLinkHighlights();

    showSelected(); //debug
  }

  @override
  onComponentLongPressStart(String componentId, LongPressStartDetails details) {
    if (canvasReader.model.canvasModel.linkExists(componentId)) return;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) {
      onLinkLongPressStart(componentId, details);
    }
  }

  @override
  onComponentScaleEnd(String componentId, ScaleEndDetails details) {
    //logout("onComponentScaleEnd componentId:${componentId} details:${details}");
    //　意図しない地点がタップされた場合に無視する
    if (isNotIntend) return;
    if (canvasReader.model.canvasModel.linkExists(componentId)) return;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) return;
    isDragging = false;

    ComponentData componentData = canvasReader.model.getComponent(componentId);
    Offset offset = centerPosition(componentData.position, componentData.size);


    // 編集終了
    (componentData.data as MyComponentData).isEditing = false;

    // PLR書き込み
    multipleSelected.forEach((compId) {
      var cmp = canvasReader.model.getComponent(compId!);
      syncingList.add((cmp.data as MyComponentData).nodeId!);
      Offset offset = centerPosition(cmp.position, cmp.size);
      setPosition(graph, (cmp.data as MyComponentData).nodeId!, offset, syncDelete: syncDelete);
    });

    logout("onComponentScaleEnd componentId:${componentId} pos:${offset} details:${details}");
    showSelected(); //debug
  }

  late Offset lastFocalPoint;

  @override
  onComponentScaleStart(componentId, details) {
    logout("onComponentScaleStart componentId:${componentId} details:${details}");

    ComponentData componentData = canvasReader.model.getComponent(componentId);
    Offset componentPosition = componentData.position;
    Offset localPosition = canvasReader.state.toCanvasCoordinates(componentPosition) + details.localFocalPoint;
    if ((componentData.data as MyComponentData).isHighlightVisible) localPosition -= const Offset(MyComponentData.highlightPosition, MyComponentData.highlightPosition) * canvasReader.state.scale;
    Offset globalPosition = details.focalPoint - displacement;

    //　意図しない地点がタップされた場合に無視する
    if ( 1 < (localPosition - globalPosition).distance) {
      isNotIntend = true;
      return;
    } else {
      isNotIntend = false;
    }

    if (canvasReader.model.canvasModel.linkExists(componentId)) return;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) return;

    isDragging = true;

    hideAllLinkHighlights();
    isReadyToBranch = false;
    isReadyToReconnectLink = false;

    //コンポーネントの現在位置を保持しておき、更新時の移動量計測に利用する。
    //移動量は複数選択モードでも変わらないので、一つ保持していればよい。
    lastFocalPoint = details.localFocalPoint;
    if (isMultipleSelectionOn) {
      //新しいコンポーネントなら追加
      addComponentToMultipleSelection(componentId);
    } else {
      addComponentSingle(componentId);
    }
    highlightComponent(componentId);

    showSelected(); //debug
  }

  @override
  onComponentScaleUpdate(componentId, details) {
    //logout("onComponentScaleUpdate componentId: ${componentId}  details: ${details}");
    //　意図しない地点がタップされた場合に無視する
    if (isNotIntend) return;
    if (canvasReader.model.canvasModel.linkExists(componentId)) return;
    if ((canvasReader.model.getComponent(componentId).data as MyComponentData).isLinkLabel) return;

    Offset positionDelta = details.localFocalPoint - lastFocalPoint;

    multipleSelected.forEach((compId) {
      var cmp = canvasReader.model.getComponent(compId!);
      canvasWriter.model.moveComponent(compId, positionDelta);
      cmp.connections.forEach((connection) {
        if (connection is ConnectionOut &&
            multipleSelected.contains(connection.otherComponentId)) {
          canvasWriter.model.moveAllLinkMiddlePoints(connection.connectionId, positionDelta);
        }
      });
      updateLinkLabelPosition(compId);
    });

    lastFocalPoint = details.localFocalPoint;
  }

  //ノード同士を接続
  Future<bool> connectComponents(String? sourceComponentId, String targetComponentId) async {
    String? text;
    String type;
    if (sourceComponentId == null || sourceComponentId == targetComponentId) {
      return false;
    }

    /// リンク追加
    ComponentData sourceComponent =
        canvasReader.model.getComponent(sourceComponentId);
    ComponentData targetComponent =
        canvasReader.model.getComponent(targetComponentId);

    List<Schemata> schemataList = allSchemata(graphSchemata, rootGraphSchemata, timelineSchemata, isTopGraph);

    SchemaClass? schemaClass;

    List<LinkTypeProperty> linkTypeList = [];

    for (Connection con in sourceComponent.connections) {
      ComponentData linkLabel = canvasReader.model.getComponent(con.otherComponentId);
      for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
        linkTypeList.add(linkType);
      }
    }

    List<String>? historyList = await getHistory();

    if (linkTypeList.isNotEmpty) {
      DateTime? lastUpdatedDateTime;
      for (LinkTypeProperty linkType in linkTypeList) {
        if (linkType.isSymmetric == true) continue;
        String? dateTimeString = linkType.linkEntity!.firstLiteralValueOf("_lastUpdated");
        DateTime? dateTime;

        if (dateTimeString != null) {
          dateTime = DateTime.parse(dateTimeString).toLocal();
        }

        //最終更新時刻が最新のスキーマクラスを取得する。
        //最終更新時刻が一つもない場合は、いずれかのスキーマクラスを取得する。
        if (dateTime == null && schemaClass == null) {
          schemaClass = getSchemaClass(linkType);
        } else if (dateTime.isAfterNullable(lastUpdatedDateTime)) {
          lastUpdatedDateTime = dateTime;
          schemaClass = getSchemaClass(linkType);
        }
      }
    }

    ComponentData? pastLinkLabel;
    Set<String> connectSet = {sourceComponentId, targetComponentId};
    for (ComponentData componentData in canvasReader.model.getAllComponents().values) {
      if ((componentData.data as MyComponentData).isLinkLabel) {
        Set<String> set = {};
        for (Connection con in componentData.connections) {
          set.add(con.otherComponentId);
        }
        if (const SetEquality().equals(connectSet, set)) {
          pastLinkLabel = componentData;
        }
      }
    }

    //既にある種別が自動選択される場合は、種別選択ダイアログを表示する
    bool isSameType = false;
    if (pastLinkLabel != null) {
      for (LinkTypeProperty linkType in (pastLinkLabel.data as MyComponentData).typeList!) {
        isSameType |= schemaClass?.label?.defaultValue == getSchemaClass(linkType)?.label?.defaultValue;
      }
    }

    if (schemaClass == null || isSameType) schemaClass = await showEditLinkTypeDialog(context, schemataList, historyList, "?", null);

    setHistory(schemaClass);

    // リンクオプション消去
    hideLinkOption();

    Schemata? schemata;
    for (Schemata sc in schemataList) {
      if (sc.classOf(schemaClass?.id) != null) {
        schemata = sc;
      }
    }
    bool isSymmetric;
    if (schemaClass != null) {
      // 描画
      type = schemaClass.id;
      isSymmetric = schemata!.classOf(type)!.isDescendantOf(schemata.classOf(SemLinkModel.symmetricRelationClass));

      // テキスト
      text = schemata.classOf(type) == null
          ? type
          : schemata.classOf(type)!.label!.defaultValue;

    } else {
      text = "?";
      type = "Uncertain";
      isSymmetric = true;
    }

    if (pastLinkLabel != null) {
      //既に同じ向きの種別がある場合は作成しない
      for (LinkTypeProperty linkType in (pastLinkLabel.data as MyComponentData).typeList!) {
        if (isSymmetric && linkType.type == text
            || !isSymmetric && linkType.type == text && linkType.targetNodeId == (targetComponent.data as MyComponentData).nodeId) {
          hideComponentHighlight(targetComponentId);
          return false;
        }
      }
    }

    // PLR
    RecordEntity sourceNode = (sourceComponent.data as MyComponentData).itemEntity!;
    RecordEntity targetNode = (targetComponent.data as MyComponentData).itemEntity!;
    Entity? linkEntity = await addLink(graph, sourceNode, targetNode, type, isNodeEncrypt, plrId, timeline, isSymmetric, OccurrenceType.created, syncAdd: syncAdd);

    LinkTypeProperty linkTypeProperty = LinkTypeProperty(linkEntity.id!,type: text, isSymmetric: isSymmetric, linkEntity: linkEntity);
    linkTypeProperty.syncingCount += 1;
    //ラベル描画
    ComponentData? linkLabel = drawLink([sourceComponent, targetComponent], [linkTypeProperty], true);
    if (linkLabel == null) {
      hideComponentHighlight(targetComponentId);
      return false;
    }

    setLinkLabelPosition(linkLabel);
    linkLabel.updateComponent();
    for (Connection link in linkLabel.connections) {
      canvasWriter.model.updateLink(link.connectionId);
    }

    drawCutLink();

    setAngle(linkLabel, linkEntity.id!, targetNodeId: targetNode.entity!.id!);
    linkLabel.updateComponent();

    for (LinkTypeProperty linkType in (linkLabel.data as MyComponentData).typeList!) {
      if (linkType.elementId == linkEntity.id) {
        linkType.isWriting = true;
        linkType.linkEntity!.syncSilently().then((_) {
          linkType.isWriting = false;
        });
      }
    }

    showSelected(); //debug

    return true;
  }
}
