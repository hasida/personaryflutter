enum Flavor {
  PERSONARY,
  PASTELD,
}

///フレーバーの設定や定数など
class F {
  static late Flavor appFlavor;
  static const diaryDataSettingId = '#20210307051321_poWC';
  static const specialistDataSettingId = '#20210501070622_paP4';
  static const secretariatDataSettingId = '#20210501070519_Lb4r';
  static const forumDataSettingId = '#20211027143543_55U7';
  static const noExtraDisclosureLiteral = 'NoExtraDisclosureFlag';
  static const noExtraChannelDisclosureLiteral = 'NoExtraChannelDisclosureFlag';

  ///ログイン時管理者とフレンド登録を行うかどうか
  static bool get isDoAdminFriendRegister {
    switch (appFlavor) {
      case Flavor.PASTELD:
        return true;
      case Flavor.PERSONARY:
        return false;
    }
  }

  ///ログイン画面に背景画像を設定するかどうか
  static bool get isDisplayBackgroundImage {
    switch (appFlavor) {
      case Flavor.PASTELD:
        return true;
      case Flavor.PERSONARY:
        return false;
    }
  }

  ///ログイン画面の背景画像パス
  static String get backGroundImagePath {
    switch (appFlavor) {
      case Flavor.PASTELD:
        return 'assets/pasteld-background.jpg';
      case Flavor.PERSONARY:
        return '';
    }
  }

  /**
   * 管理者のPLRIDを返す
   */
  static String get administratorPlrId {
    final String requestString = F.requestString;
    if (requestString == '') return '';

    ///文頭からセミコロンまでを取得
    final String? stringAfterRegexp = RegExp(r'.+;').stringMatch(requestString);
    if (stringAfterRegexp == null) return '';

    ///セミコロンを削除した文字列を返す
    final deletedSemicolon = stringAfterRegexp.length - 1;
    return stringAfterRegexp.substring(0, deletedSemicolon);
  }

  /**
   * 専用アプリの場合、管理者と自動的にフレンド登録する必要がある
   * 管理者のリクエスト文字列を返す
   */
  static String get requestString {
    switch (appFlavor) {
      case Flavor.PASTELD:
        return 'googleDrive:pasteld001@gmail.com;1wR4f6t0z9MhwkNehyRY_t_rYKdDK_Lh7';
      case Flavor.PERSONARY:
        return '';
    }
  }

}
