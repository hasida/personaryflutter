import 'package:background_fetch/background_fetch.dart' show HeadlessTask;
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import 'main.dart' show initCommon, onError;
import 'logic.dart';

final automaticFriends = {
  RegExp(r"^googleDrive:.*@st\.spec\.ed\.jp$"): FriendRequest.fromString(
    "googleDrive:saitama_e-portfolio@st.spec.ed.jp;1wTP9MweDQNISI8R7dtKBo8lyo-UXe9vO",
  ),
  RegExp(r"^googleDrive:hasida\.plr.@gmail\.com$"): FriendRequest.fromString(
    "googleDrive:hasidak@gsk.or.jp;1bEsul1DUijd9skwuEysiUDJGgNa2BO8n",
  ),
};

final PlrConfig config = PlrConfig(
  notificationConfig: const PlrNotificationConfig(
    skipNotInSearchResultFriends: true,
  ),
  periodicTaskConfig: PlrPeriodicTaskConfig(
    databaseUpdateConfig: const PlrDatabaseUpdateConfig(
      friendsPolicy: UpdateDatabasePolicy.unregistered,
      skipNotInSearchResultFriends: true,
    ),
    requestReceptionConfig: const PlrRequestReceptionConfig(
      checkFriend: false,
    ),
    automaticFriendConfig: PlrAutomaticFriendConfig(
      automaticFriends: automaticFriends,
    ),
    headlessTask: headlessTask,
  ),
);

Future<void> headlessTask(HeadlessTask task) async {
  await initCommon();

  await runPlrHeadlessTask(
    task, config.toHeadless(),
    onRunPeriodicTask: () => ProviderContainer().listen<AsyncValue<Object>>(
      plrMessageProvider, (_, next) => next.maybeWhen(
        data: handleMessage,
        error: onError,
        orElse: () {},
      ),
    ),
  );
}

bool handleMessage(Object message) {
  if (message is Log) print(message.message);
  else if (message is NotificationEvent) {
    processContentNotifications(message);
  }
  else if (message is HealthItemsResult) {
    notifyHealthSettingResult(message);
  }
  else if (message is PlrProcessMailqResult) {
    if (message.result == true) {
      print("All queued mails have been sent successfully.");
    } else if (message.result == false) {
      print("Some queued mails failed to be sent.");
    }
  }
  else if (message is PlrRequestReceptionResult) {
    var rr = message.requestReception;

    for (var r in message.channelDisclosureConsentRequests) {
      notifyChannelDisclosureConsentRequest(rr, r);
    }
    for (var r in message.fidoRegistrationTokenResults) {
      notifyFidoRegistrationTokenResult(rr, r);
    }
    for (var r in message.didIssuanceResults) {
      notifyDidIssuanceResult(rr, r);
    }
    for (var r in message.vcIssuanceResults) {
      notifyVcIssuanceResult(rr, r);
    }
    for (var r in message.disclosureRequestTargets) {
      if (r.item is ChannelDisclosureRequestItem) notifyChannelDisclosed(
        r.friend, r.item.objects,
      );
      else if (r.item is ItemDisclosureRequestItem) notifyItemDisclosed(
        r.friend, r.item.objects,
      );
    }
  }
  else if (message is PlrFriendRegisteredEvent) {
    notifyFriendRegistered(message.friend);
  }
  else if (message is PlrInfoChannelSubscribedEvent) {
    notifyInfoChannelSubscribed(message.user);
  }
  else if (message is PlrPassphraseDepositedEvent) {
    notifyPassphraseDeposited(message.friend);
  }
  else if (message is PlrPassphraseChanged) {
    notifyPassphraseChanged(message.storage);
  }
  else if (message is PlrChannelIndexEvent) {
    processChannelIndexNotifications(message);
  }
  else {
    print(message);
    return false;
  }
  return true;
}
