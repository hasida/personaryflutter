import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Others Section",
    "ja_jp": "他者セクション",
  };

  String get i18n => localize(this, t);
}
