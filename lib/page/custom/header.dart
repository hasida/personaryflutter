part of 'custom_page.dart';

const _largePictureSize = 240.0;
const _largePictureIcon = CircleAvatar(
  radius: _largePictureSize / 2,
  backgroundColor: Color(0xff3d9970),
  child: SizedBox(
    height: _largePictureSize,
    child: const Icon(
      Icons.person, color: Colors.white, size: _largePictureSize,
    ),
  ),
);

abstract class _Header extends _Base {
  _Header(super.applCustom, { super.key });

  Widget _buildHeader(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) => Column(
    children: [
      _buildBlock(
        switch (header) {
          ApplHeader.s => _buildSmallHeader(
            context, ref, controllers, header,
          ),
          ApplHeader.m => _buildMiddleHeader(
            context, ref, controllers, header,
          ),
          ApplHeader.l => _buildLargeHeader(
            context, ref, controllers, header),
        },
      ),
      divider,
    ],
  );

  Widget _buildSmallHeader(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) => _buildUserHeader(context, ref, controllers, header);

  Widget _buildMiddleHeader(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      _buildUserHeader(context, ref, controllers, header),
      _buildProfileView(context, ref, controllers, header),
    ],
  );

  Widget _buildUserHeader(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) => UserHeader(
    root,
    buttons: [
      LifeRecordButton(account),
      ProfileDiscloseButton(account),
    ],
  );

  Widget _buildProfileView(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) {
    final provider = profileItemsProvider(root);
    controllers.addController(context, ref, provider);

    return ProfileView(
      provider: provider,
      include: const [ genderClass, birthClass, homeClass ],
      exclude: const [ nameClass, pictureClass ],
    );
  }

  Widget _buildLargeHeader(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) {
    final _rootSyncProvider = rootSyncProvider(root);
    controllers.addController(context, ref, _rootSyncProvider);

    final publicProfileItemsProvider = profileItemsProvider(
      publicRoot, scanOthers: false,
    );
    final publicProfileItemsController = ref.read(
      publicProfileItemsProvider.notifier,
    );
    controllers.addController(context, ref, publicProfileItemsProvider);

    ref.listen(_rootSyncProvider, (_, next) {
        switch (next) {
          case AsyncData(isLoading: false): {
            updatePublicDisclosure(
              root.lifeRecord, publicRoot,
              onDisclose:
                () => publicProfileItemsController.refresh(force: true),
            );
          }
        }
    });

    return Container(
      width: double.infinity,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          _buildLargeProfile(context, ref, controllers, header),
          Positioned(
            top: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Padding(
                padding: allPadding[1],
                child: Row(
                  children: [
                    LifeRecordButton(
                      account,
                      rootSyncProvider: _rootSyncProvider,
                      publicProfileItemsProvider: publicProfileItemsProvider,
                    ),
                    ProfileDiscloseButton(
                      account,
                      publicProfileItemsProvider: publicProfileItemsProvider,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLargeProfile(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplHeader header,
  ) => HookConsumer(
    builder: (context, ref, _) {
      final userInfoProvider = userInfoUpdateProvider(root);
      controllers.addController(context, ref, userInfoProvider);

      final userInfo = ref.watch(userInfoProvider).value;

      final provider = profileItemsProvider(root);
      controllers.addController(context, ref, provider);

      final state = ref.watch(provider);
      final value = state.value;

      final Widget name; {
        final n = (value != null) ? recentNameTextOf(value.items) : (
          userInfo?.name ?? null
        );
        name = Text(
          (n != null) ? "${n} (${root.plrId.email})" : root.plrId.email,
          overflow: TextOverflow.ellipsis,
        );
      }

      final pictureFile = (value != null)
        ? recentPictureFileOf(value.items) : null;

      final Widget picture; {
        Widget _buildLoadingPicture() {
          final picture = pictureFile?.thumbnailData ?? userInfo?.picture;
          return Stack(
            alignment: AlignmentDirectional.center,
            children: [
              (picture != null)
                ? SizedBox(
                  width: double.infinity,
                  height: _largePictureSize,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: memoryImageOf(picture),
                  ),
                )
                : _largePictureIcon,
              const CircularProgressIndicator(),
            ],
          );
        }

        if (value != null) {
          AsyncSnapshot<Uint8List>? snapshot;
          if (pictureFile != null) snapshot = useStream(
            useMemoized(
              () => pictureFile.load().handleError(
                (e, s) => showError(context, e, s),
              ),
            ),
          );
          if (snapshot != null) picture = switch (snapshot.connectionState) {
            ConnectionState.active || ConnectionState.done => SizedBox(
              width: double.infinity,
              height: _largePictureSize,
              child: Stack(
                alignment: AlignmentDirectional.center,
                fit: StackFit.passthrough,
                children: [
                  FittedBox(
                    fit: BoxFit.contain,
                    child: memoryImageOf(snapshot.data!),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => Navigator.push(
                        context, ImageModal.fromMmdata(pictureFile!),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            _ => _buildLoadingPicture(),
          };
          else picture = _largePictureIcon;
        } else picture = _buildLoadingPicture();
      }
      return Column(
        children: [
          Padding(
            padding: bottomPadding[1],
            child: picture,
          ),
          name,
        ],
      );
    },
  );
}
