part of 'custom_page.dart';

const openInBrowserIcon = const Icon(
  Icons.open_in_browser,
);

final _zeroDateTime = DateTime(0);

abstract class _Section extends _SectionBase {
  _Section(super.applCustom, { super.key });

  Widget _buildSections(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    List<ApplSectionInterface> sections,
  ) {
    final children = <Widget>[];

    List<Widget>? w;
    void mayAddHorizWidget() {
      if (w == null) return;

      final scrollController = ScrollController();
      children.add(
        Padding(
          padding: const EdgeInsetsDirectional.only(bottom: 5),
          child: Scrollbar(
            controller: scrollController,
            child: SingleChildScrollView(
              controller: scrollController,
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsetsDirectional.only(bottom: 10),
              child: Row(children: w!),
            ),
          ),
        ),
      );
      w = null;
    }

    for (final s in sections) {
      final direction = s.direction;
      final hasTitle = (
        (s.titleLabel?.defaultValue?.isNotEmpty == true) || s.hasTitleImage
      );
      if ((direction != ApplDirection.horiz) || hasTitle) {
        mayAddHorizWidget();
        if (hasTitle) children.add(_buildSectionHeader(context, ref, s));
      }

      final sectionWidget = _buildSection(context, ref, controllers, s);
      switch (direction) {
        case ApplDirection.horiz: (w ??= []).add(sectionWidget);
        default: children.add(_buildBlock(sectionWidget));
      }
    }
    mayAddHorizWidget();

    return Column(
      children: children,
    );
  }

  Widget _buildSection(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplSectionInterface section,
  ) => switch (section) {
    ApplLinkInterface() => _buildLink(
      context, ref, controllers, section,
    ),
    ApplMyChannelSInterface() => _buildMyChannelS(
      context, ref, controllers, section,
    ),
    ApplOthersSInterface() => _buildOthersS(
      context, ref, controllers, section,
    ),
    ApplInOthersChannelSInterface() => _buildInOthersChannelS(
      context, ref, controllers, section,
    ),
    ApplAnnouncementSInterface() => _buildAnnouncementS(
      context, ref, controllers, section,
    ),
    _ => emptyWidget,
  };

  Widget _buildLink(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplLinkInterface section,
  ) {
    final link = section.link;
    final onTap = (link != null)
      ? () => openWebViewPage(
        context, section.cardTitleLabel?.defaultValue?.toString() ?? "", link,
      )
      : null;

    return _mayAssignCardTitleTooltip(
      section,
      child: _buildCardTitleCell(
        context, ref, controllers, section, onTap: onTap,
        vertButtons: [ openInBrowserIcon ],
        horizEmpty: openInBrowserIcon,
      ),
    );
  }

  Widget _buildMyChannelS(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplMyChannelSInterface section,
  ) {
    final myCS1 = section.myCS1;
    if (myCS1 == null) return emptyWidget;

    final compact = section.direction == ApplDirection.horiz;

    return HookBuilder(
      builder: (context) {
        final Channel channel; {
          final r = _autoGenerate(context, ref, controllers, myCS1);
          if (r == null) {
            return const Center(child: CircularProgressIndicator());
          }
          if (r.isEmpty) return emptyWidget;

          channel = r[0];
        }

        final dataEntryButton = (section.dataEntry != null)
          ? ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context)
                .floatingActionButtonTheme.backgroundColor,
              foregroundColor: Theme.of(context)
                .floatingActionButtonTheme.foregroundColor,
              padding: const EdgeInsets.symmetric(horizontal: 8),
              minimumSize: const Size(36, 36),
              elevation: 2,
            ),
            onPressed: () => _addItem(context, ref, controllers, channel),
            child: Text(
              section.dataEntry!.defaultValue?.toString() ?? "＋",
            ),
          )
          : null;

        if (section.hasCardTitle) return _mayAssignCardTitleTooltip(
          section,
          child: _buildCardTitleCell(
            context, ref, controllers, section,
            onTap: () => mayOpenTimelinePage(context, account, channel),
            buttons: (dataEntryButton != null) ? [ dataEntryButton ] : null,
            empty: const Icon(Icons.timeline, ),
          ),
        );

        return _mayAssignTitleTooltip(
          section, useTitle: false,
          child: ChannelCell<Channel>(
            ref: ref, object: channel,
            config: channelCellConfigWithDefault(
              onTap: (channel, _) => mayOpenTimelinePage(
                context, account, channel,
              ),
              buttonBuilders: [
                if (dataEntryButton != null)
                  (_0, _1, _2, _3, _4, _5, _6, _7) => dataEntryButton,
              ],
            ),
            compact: compact,
          ),
        );
      },
    );
  }

  Widget _buildOthersS(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplOthersSInterface section,
  ) {
    final role = section.role;
    final roleName = role?.value.toString();
    if (roleName?.isNotEmpty != true) return emptyWidget;

    final myCSs = section.myCSs.toList();
    final myCS1s = section.myCS1s.toList();
    final css = section.css.toList();
    if (myCSs.isEmpty && myCS1s.isEmpty && css.isEmpty) return emptyWidget;

    final compact = section.direction == ApplDirection.horiz;

    return HookConsumer(
      builder: (context, ref, _) {
        final roleFriends = _roleFriendsOf(
          context, ref, controllers, section, roleName!,
        );
        if (roleFriends == null) {
          return const Center(child: CircularProgressIndicator());
        } else if (roleFriends.isEmpty) return emptyWidget;

        final children = roleFriends.map(
          (f) => _buildOthersSCell(
            controllers, section, myCSs, myCS1s, css, compact, f,
          ),
        ).toList();

        return switch (compact) {
          true => Row(children: children),
          false => Column(children: children),
        };
      },
    );
  }

  Widget _buildOthersSCell(
    Set<PlrNotifier> controllers,
    ApplOthersSInterface section,
    List<DataSetting> myCSs,
    List<DataSetting> myCS1s,
    List<DataSetting> css,
    bool compact,
    Friend friend,
  ) => HookConsumer(
    builder: (context, ref, _) {
      final userInfoProvider = userInfoUpdateProvider(
        friend, labelOfMe: defaultLabelOfMe,
      );
      controllers.addController(context, ref, userInfoProvider);

      final friendProvider = friendSyncProvider(friend);
      controllers.addController(context, ref, friendProvider);

      final friendValue = ref.watch(friendProvider).value;

      final myChannels = _generateMyChannels(
        context, ref, friendValue?.meToFriendRoot, myCSs, true, controllers,
      );
      final myChannel1s = _generateMyChannels(
        context, ref, friendValue?.meToFriendRoot, myCS1s, false, controllers,
      );
      final friendChannels = targetChannelsOf(
        context, ref, friendValue?.friendToMeRoot, css, true, controllers,
      );

      final enabled = myChannels.isNotEmpty || myChannel1s.isNotEmpty ||
        friendChannels.isNotEmpty;

      final onTap = enabled ? () => Navigator.push(
        context, MaterialPageRoute(
          builder: (_) => OthersSectionPage(section, friend, userInfoProvider),
        ),
      ) : null;

      final name = ref.watch(userInfoProvider).value?.name ?? friend.id;

      if (section.hasCardTitle) {
        final additionalText = ": ${name}";
        return _mayAssignCardTitleTooltip(
          section, additionalText: additionalText,
          child: _buildCardTitleCell(
            context, ref, controllers, section,
            additionalText: additionalText,
            onTap: onTap,
            empty: leadingPersonIcon,
          ),
        );
      }

      return Tooltip(
        message: name,
        child: UserCell(
          ref: ref, object: friend, provider: userInfoProvider,
          showEmail: false,
          config: userCellConfigWithDefault(
            onTap: enabled ? (_, __) => onTap!() : null,
          ),
          compact: compact,
        ),
      );
  });

  Widget _buildInOthersChannelS(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplInOthersChannelSInterface section,
  ) {
    final role = section.role;
    final roleName = role?.value.toString();
    if (roleName?.isNotEmpty != true) return emptyWidget;

    final cs = section.cs;
    if (cs == null) return emptyWidget;

    final sec = section.sec;
    if (sec == null) return emptyWidget;

    final friendsOnly = sec == ApplSec.sec2;
    final compact = section.direction == ApplDirection.horiz;

    return HookConsumer(
      builder: (context, ref, _) {
        final FriendsState? friendsValue;
        if (friendsOnly) {
          controllers.addController(context, ref, friendsProvider);
          friendsValue = ref.watch(friendsProvider).value;
        } else friendsValue = null;

        final roleFriends = _roleFriendsOf(
          context, ref, controllers, section, roleName!,
        );
        if ((roleFriends == null) || (friendsOnly && (friendsValue == null))) {
          return const Center(child: CircularProgressIndicator());
        }
        if (roleFriends.isEmpty) return emptyWidget;

        final friendIds = (friendsOnly)
          ? friendsValue!.list.map((f) => f.id).toSet() : null;

        return _buildInOthersChannelSCells(
          context, ref,
          controllers, section, cs, sec, compact, roleFriends, friendIds,
        );
      },
    );
  }

  Widget _buildInOthersChannelSCells(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplInOthersChannelSInterface section,
    DataSetting cs,
    ApplSec sec,
    bool compact,
    List<Friend> roleFriends,
    Set<String>? friendIds,
  ) => Consumer(
    builder: (context, ref, _) {
      final channelProviderMap = SplayTreeMap<String, ChannelState>();

      var hasUncompleted = false;
      for (final f in roleFriends) {
        final friendProvider = friendSyncProvider(f);
        controllers.addController(context, ref, friendProvider);

        final friendValue = ref.watch(friendProvider).value;
        if (friendValue == null) {
          hasUncompleted = true;
          continue;
        }

        for (
          HasChannels? r in [
            friendValue.publicRoot, friendValue.friendToMeRoot,
        ]) {
          for (
            final c in targetChannelsOf(
              context, ref, r, [ cs ], false, controllers,
            )
          ) {
            final id = c.id;
            if ((id != null) && !channelProviderMap.containsKey(id)) {
              final channelProvider = channelSyncProvider(c);
              controllers.addController(context, ref, channelProvider);

              final channelValue = ref.watch(channelProvider).value;
              if (channelValue == null) {
                hasUncompleted = true;
                continue;
              }
              channelProviderMap[id] = channelValue;
            }
          }
        }
      }

      final children = [
        ...switch (sec) {
          ApplSec.sec0 || ApplSec.sec2 => _buildDisclosedUserCells(
            context, ref, sec, compact, friendIds, channelProviderMap.values,
          ),
          ApplSec.sec1 || ApplSec.sec2 => _buildChannelCells(
            context, ref, sec, compact, channelProviderMap.values,
          ),
        },
        if (hasUncompleted) const Center(child: CircularProgressIndicator()),
      ];
      return switch (compact) {
        true => Row(children: children),
        false => Column(children: children),
      };
    }
  );

  Iterable<Widget> _buildDisclosedUserCells(
    BuildContext context,
    WidgetRef ref,
    ApplSec sec,
    bool compact,
    Set<String>? friendIds,
    Iterable<ChannelState> channelValues,
  ) {
    final config = disclosedToUserCellConfigWithDefault(
      onTap: (u, _) => openFriendOrUserPage(context, account, u),
      onError: (e, s) => showError(context, e, s),
    );

    final d2uProviderMap = SplayTreeMap<String, UserInfoUpdateProvider>();
    for (final v in channelValues) {
      for (final u in v.realDisclosedToUsers) {
        final id = u.id;
        if (
          (id != null) && !d2uProviderMap.containsKey(id) &&
          (friendIds?.contains(id) != false)
        ) {
          d2uProviderMap[id] = userInfoUpdateProvider(
            u, labelOfMe: config.labelOfMe,
          );
        }
      }
    }

    return d2uProviderMap.values.map(
      (p) => Consumer(
        builder: (context, ref, _) {
          final user = p.user as DisclosedToUser;
          final userInfo = ref.watch(p).value;
          return Tooltip(
            message: userInfo?.name ?? user.id,
            child: UserCell(
              key: key, ref: ref, config: config, object: user, provider: p,
              showEmail: false, compact: compact,
            ),
          );
        },
      ),
    );
  }

  Iterable<Widget> _buildChannelCells(
    BuildContext context,
    WidgetRef ref,
    ApplSec sec,
    bool compact,
    Iterable<ChannelState> channelValues,
  ) {
    final config = channelCellConfigWithDefault<Channel>(
      onTap: (channel, controller) => mayOpenTimelinePage(
        context, account, channel, onPop: (_) => controller.refresh(),
      ),
      onError: (e, s) => showError(context, e, s),
    );

    final channelProviderMap = <String, ChannelUpdateProvider>{};
    for (final v in channelValues) {
      for (final c in v.channels) {
        final id = c.id;
        if (
          (id != null) && !channelProviderMap.containsKey(id)) {
          channelProviderMap[id] = channelUpdateProvider(
            c, labelOfMe: config.labelOfMe,
          );
        }
      }
    }

    final channelValueMap = Map<
      ChannelUpdateProvider, ChannelUpdateState?
    >.fromIterable(
      channelProviderMap.values, value: (p) => ref.watch(p).value,
    );
    final sortedProviders = channelValueMap.keys.toList(growable: false)..sort(
      (p1, p2) => (channelValueMap[p2]?.modified ?? _zeroDateTime).compareTo(
        channelValueMap[p2]?.modified ?? _zeroDateTime,
      ),
    );

    return sortedProviders.map(
      (p) => Consumer(
        builder: (context, ref, _) {
          final channel = p.channel as Channel;
          final channelValue = channelValueMap[p];
          return Tooltip(
            message: channelValue?.name ?? channel.id,
            child: ChannelCell(
              key: key, ref: ref, config: config,
              object: channel, provider: p, compact: compact,
            ),
          );
        },
      ),
    );
  }

  Widget _buildAnnouncementS(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplAnnouncementSInterface section,
  ) {
    final provider = infoRegistrySyncProvider(infoRegistry);
    controllers.addController(context, ref, provider);

    return InfoList(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      provider: provider,
    );
  }
}
