part of 'custom_page.dart';

abstract class _SectionBase extends _Header {
  _SectionBase(super.applCustom, { super.key });

  T? _processSnapshot<T>(BuildContext context, AsyncSnapshot<T?> snapshot) {
    if (snapshot.hasError) {
      showError(context, snapshot.error!, snapshot.stackTrace);
      return null;
    }
    return snapshot.data;
  }

  Widget _buildSectionHeader(
    BuildContext context,
    WidgetRef ref,
    ApplSectionInterface section,
  ) {
    final title = section.titleLabel?.defaultValue?.toString();

    return _mayAssignTitleTooltip(
      section, useTitle: false,
      child: Row(
        children: [
          if (section.hasTitleImage) HookBuilder(
            builder: (context) => Padding(
              padding: leftPadding[2],
              child: _titleImageOf(
                context, section, width: 24, height: 24),
            ),
          ),
          if (title != null) SectionHeader(
            title, padding: leftPadding[2],
          ),
        ],
      ),
    );
  }

  Widget _imageOf(
    BuildContext context,
    Stream<Widget> imageWidgetStream, {
      Mmdata? mmdata,
      double width = 48.0,
      double height = 48.0,
      Widget? empty,
  }) {
    final snapshot = useStream(
      useMemoized(
        () => imageWidgetStream.handleError(
          (e, s) => showError(context, e, s),
        ),
        (mmdata != null) ? [ mmdata ] : const [],
      ),
    );

    final data = snapshot.data;
    if (data != null) return SizedBox(
      width: width,
      height: height,
      child: data,
    );
    return switch (snapshot.connectionState) {
      ConnectionState.done => empty ?? emptyWidget,
      _ => SizedBox(
        width: width,
        child: LinearProgressIndicator(minHeight: height),
      ),
    };
  }

  Widget _titleImageOf(
    BuildContext context,
    HasApplTitleInterface hasApplTitle, {
      double width = 48.0,
      double height = 48.0,
      Widget? empty,
  }) => _imageOf(
    context, hasApplTitle.titleImageWidget,
    mmdata: hasApplTitle.applTitle,
    width: width, height: height, empty: empty,
  );

  Widget _cardTitleImageOf(
    BuildContext context,
    HasApplCardTitleInterface hasApplCardTitle, {
      double width = 48.0,
      double height = 48.0,
      Widget? empty,
  }) => _imageOf(
    context, hasApplCardTitle.cardTitleImageWidget,
    mmdata: hasApplCardTitle.applCardTitle,
    width: width, height: height, empty: empty,
  );

  Widget _buildCardTitleCell(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplSectionInterface section, {
      String? additionalText,
      List<Widget>? buttons,
      List<Widget>? horizButtons,
      List<Widget>? vertButtons,
      VoidCallback? onTap,
      Widget? empty,
      Widget? horizEmpty,
      Widget? vertEmpty,
  }) => switch (section.direction) {
    ApplDirection.horiz => _buildHorizCardTitleCell(
      context, ref, controllers, section,
      additionalText: additionalText,
      buttons: horizButtons ?? buttons, onTap: onTap,
      empty: horizEmpty ?? empty,
    ),
    _ => _buildVertCardTitleCell(
      context, ref, controllers, section,
      additionalText: additionalText,
      buttons: vertButtons ?? buttons, onTap: onTap,
      empty: vertEmpty ?? empty,
    ),
  };

  Widget _buildHorizCardTitleCell(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplSectionInterface section, {
      String? additionalText,
      List<Widget>? buttons,
      VoidCallback? onTap,
      Widget? empty,
  }) {
    Widget content = IntrinsicWidth(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                SizedBox(
                  width: 100, height: 100,
                  child: FittedBox(
                    child: HookBuilder(
                      builder: (context) => _cardTitleImageOf(
                        context, section, empty: empty,
                      ),
                    ),
                  ),
                ),
                if (buttons?.isNotEmpty == true) Container(
                  margin: const EdgeInsets.only(left: 5),
                  child: Column(children: buttons!),
                ),
              ],
            ),
          ),
          Container(
            width: 0,
            height: 30,
            padding: const EdgeInsets.symmetric(horizontal: 5),
            alignment: Alignment.center,
            child: AutoSizeText(
              (section.cardTitleLabel?.defaultValue?.toString() ?? "") +
              (additionalText ?? ""),
              minFontSize: 10, maxLines: 2, overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );

    if (onTap != null) content = InkWell(
      onTap: onTap,
      child: content,
    );
    else content = Opacity(
      opacity: .5,
      child: content,
    );
    return Card(child: content);
  }

  Widget _buildVertCardTitleCell(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplSectionInterface section, {
      String? additionalText,
      List<Widget>? buttons,
      VoidCallback? onTap,
      Widget? empty,
  }) => Card(
    child: ListTile(
      contentPadding: const EdgeInsets.all(5),
      leading: Leading(
        HookBuilder(
          builder: (context) => _cardTitleImageOf(
            context, section, empty: empty,
          ),
        ),
      ),
      title: Text(
        (section.cardTitleLabel?.defaultValue?.toString() ?? "") +
        (additionalText ?? ""),
      ),
      trailing: (buttons?.isNotEmpty == true) ? Row(
        mainAxisSize: MainAxisSize.min, children: buttons!,
      ) : null,
      onTap: onTap,
      enabled: onTap != null,
    ),
  );

  List<Friend>? _roleFriendsOf(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplSectionInterface section,
    String roleName,
  ) {
    final lastRoleFriends = useRef<List<Friend>?>(null);

    final roleFriendsSnapshot = _roleFriendsSnapshotOf(
      controllers, section, roleName,
    );

    switch (roleFriendsSnapshot.connectionState) {
      case ConnectionState.none || ConnectionState.waiting: {
        return lastRoleFriends.value;
      }
      default: {
        final f = _processSnapshot(context, roleFriendsSnapshot)
          ?? lastRoleFriends.value;
        if (f == null) return const [];

        return lastRoleFriends.value = roleFriendsSnapshot.data!;
      }
    }
  }

  AsyncSnapshot<List<Friend>> _roleFriendsSnapshotOf(
    Set<PlrNotifier> controllers,
    ApplSectionInterface section,
    String roleName,
  ) => useFuture(
    useMemoized(
      () async {
        final roleFriends = await userRoleFriendsOf(storage, roleName, noLang);

        if (
          (section is! HasApplRoleVerification) ||
          !(section as HasApplRoleVerification).applRoleVerification
        ) return roleFriends;

        final applOwner = await _applOwner;
        if (applOwner == null) return const [];

        return (
          await Future.wait(
            roleFriends.map(
              (f) async {
                if (!await _verifyRole(f, roleName, applOwner)) return null;
                return f;
              }
            )
          )
        ).nonNulls.toList();
      }, [
        createRefreshState(controllers).value,
      ],
    ),
  );

  Future<PlrId?> get _applOwner async {
    if (applCustom is! ApplCustom) return null;

    final _applCustom = applCustom as ApplCustom;
    final _storage = _applCustom.storage;
    if (_storage == null) return null;

    final String? owner; try {
      owner = (await _applCustom.localOrNetMeta).value.owner;
    } catch (e, s) {
      print(e); print(s);
      return null;
    }
    if (owner == null) return null;

    return PlrId(storage.type, owner);
  }

  Future<bool> _verifyRole(
    Friend friend,
    String roleName,
    PlrId applOwner,
  ) async => (friend.plrId == applOwner);

  List<Channel> _generateMyChannels(
    BuildContext context,
    WidgetRef ref,
    MeToFriendRoot? m2fr,
    Iterable<DataSetting> myCSs,
    bool useDestTag,
    Set<PlrNotifier> controllers,
  ) => myCSs.map(
    (ds) => _generateMyChannel(
      context, ref, m2fr, ds, useDestTag, controllers,
    ),
  ).nonNulls.toList();

  Channel? _generateMyChannel(
    BuildContext context,
    WidgetRef ref,
    MeToFriendRoot? m2fr,
    DataSetting ds,
    bool useDestTag,
    Set<PlrNotifier> controllers,
  ) {
    if (m2fr == null) return null;

    final destTag = useDestTag ? m2fr.friend.id : null;

    final autoGenerateSnapshot = _autoGenerateSnapshotOf(
      controllers, ds,
      toDiscloses: [ m2fr ], forceDisclose: true, destTag: destTag,
    );

    return switch (autoGenerateSnapshot.connectionState) {
      ConnectionState.none || ConnectionState.waiting =>
        m2fr.generatedChannelWith(ds, destTag: destTag),
      _ => _processSnapshot(context, autoGenerateSnapshot)
        ?? m2fr.generatedChannelWith(ds, destTag: destTag),
    };
  }

  List<Channel>? _autoGenerate(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    DataSetting dataSetting, {
      Iterable<HasChannelsEditable>? toDiscloses,
      bool forceDisclose = false,
      String? destTag,
      bool sync = true,
      Duration sendTimeout = const Duration(minutes: 1),
  }) {
    final autoGenerateSnapshot = _autoGenerateSnapshotOf(
      controllers, dataSetting,
      toDiscloses: toDiscloses, forceDisclose: forceDisclose,
      destTag: destTag, sync: sync, sendTimeout: sendTimeout,
    );

    final Channel? c; switch (autoGenerateSnapshot.connectionState) {
      case ConnectionState.none || ConnectionState.waiting: {
        c = root.generatedChannelWith(dataSetting);
        if (c == null) return null;
      }
      default: {
        c = _processSnapshot(context, autoGenerateSnapshot)?.channel
          ?? root.generatedChannelWith(dataSetting);
        if (c == null) return [];
      }
    }
    return [ c ];
  }

  AsyncSnapshot<Channel?> _autoGenerateSnapshotOf(
    Set<PlrNotifier> controllers,
    DataSetting dataSetting, {
      Iterable<HasChannelsEditable>? toDiscloses,
      bool forceDisclose = false,
      String? destTag,
      bool sync = true,
      Duration sendTimeout = const Duration(minutes: 1),
  }) {
    final refreshState = createRefreshState(controllers);

    return useFuture(
      useMemoized(
        () async {
          if (!dataSetting.isLoaded || refreshState.isForced) {
            await dataSetting.syncSilently();
            if (!dataSetting.isLoaded) return null;
          }
          return (
            await dataSetting.autoGenerate(
              toDiscloses: toDiscloses, forceDisclose: forceDisclose,
              destTag: destTag,
              sync: sync, sendTimeout: sendTimeout,
            )
          )?.channel;
        }, [ refreshState.value ]),
    );
  }

  Future<RecordEntity?> _addItem(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    Channel channel,
  ) => showDialog(
    context: context,
    builder: (_) => Consumer(
      builder: (context, ref, _) {
        final provider = schemataSelectionProvider(
          channel,
          styleSheetType: StyleSheetType.timeline,
          keyPrefix: "select_schema_",
        );

        final controller = ref.read(provider.notifier);
        controllers.addController(context, ref, provider);

        final value = ref.watch(provider).value;
        if (value == null) {
          if (controller.isLoading) return const Center(
            child: CircularProgressIndicator(),
          );
          Future.microtask(() {
              Navigator.pop(context);
              showConfirmDialog(
                context, "Unable to add items to the timeline.".i18n,
                "No applicable schemata could be found.".i18n,
                showCancelButton: false,
              );
          });
          return emptyWidget;
        }

        var schemata = value.selectedSchemata;
        if (schemata?.timelineState != TimelineState.Writable) {
          return _buildSchemataSelectDialog(context, controller, value);
        }
        return _buildEntityDialog(context, channel, schemata!);
      },
    ),
  );

  Widget _buildSchemataSelectDialog(
    BuildContext context,
    SchemataSelection controller,
    SchemataSelectionState value,
  ) {
    final schemataSources = value.map.entries.where(
      (e) => e.value?.timelineState == TimelineState.Writable,
    ).map((e) => e.key).toList();

    return SimpleDialog(
      title: Text("Select a schema".i18n),
      children: schemataSources.map(
        (ss) => SimpleDialogOption(
          onPressed: () => controller.setSelectedSchemataSourceId(ss.id),
          child: Text(ss.name?.defaultValue?.toString() ?? "Unknown".i18n),
        ),
      ).toList(),
    );
  }

  Widget _buildEntityDialog(
    BuildContext context,
    Channel channel,
    Schemata schemata,
  ) {
    final recordEntity = RecordEntity.create(schemata, eventClass);
    if (recordEntity == null) {
      Future.microtask(() {
          Navigator.pop(context);
          showConfirmDialog(
            context, "Unable to add items to the timeline.".i18n,
            "There are no applicable classes.".i18n,
            showCancelButton: false,
          );
      });
      return emptyWidget;
    }
    return EntityDialog(
      account, EntityDialogSettings(), channel, recordEntity, false, true,
    );
  }
}
