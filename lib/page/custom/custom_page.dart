import 'dart:collection' show SplayTreeMap;
import 'dart:typed_data' show Uint8List;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:plr_ui/src/timeline/image_modal.dart';

import '../../widget.dart';
import '../../style.dart';
import '../../util.dart';
import '../../dialog/friend_add/friend_add_dialog.dart';
import '../timeline/timeline_page.dart' show mayOpenTimelinePage;
import '../friend/friend_page.dart' show openFriendOrUserPage;
import '../web_view/web_view_page.dart';
import 'custom_page.i18n.dart';
import 'others_section_page.dart';
import 'privates.dart';

part 'base.dart';
part 'header.dart';
part 'section.dart';
part 'section_base.dart';

class CustomPage extends _Section {
  CustomPage(super.applCustom, { super.key });

  void _initListeners(BuildContext context, WidgetRef ref) {
    if (_applCustomSyncProvider != null) ref.listen(
      _applCustomSyncProvider!, (_, next) => processError(context, next),
    );
  }

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    _initListeners(context, ref);

    final ApplCustomInterface? applCustomValue;
    if (_applCustomSyncProvider != null) {
      applCustomValue = ref.watch(_applCustomSyncProvider!).value;
    } else applCustomValue = applCustom;

    if (applCustomValue == null) {
      return const Center(child: CircularProgressIndicator());
    }

    final controllers = <PlrNotifier>{};
    Future<void> refresh([ bool force = false ]) => Future.wait([
        if (_applCustomSyncController != null)
          _applCustomSyncController!.refresh(force: force),
        ...controllers.map((c) => c.refresh(force: force)),
    ]);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            applCustomValue.titleLabel?.defaultValue?.toString() ?? "",
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () => refresh(true),
          child: TimerShownDetector(
            onTimer: refresh,
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: Padding(
                  padding: EdgeInsets.only(
                    bottom: (applCustomValue.addFriend != null) ? 64 : 0,
                  ),
                  child: _buildPage(context, ref, controllers, applCustomValue),
                ),
              ),
            ),
          ),
        ),
        floatingActionButton: (applCustomValue.addFriend != null)
          ? FloatingActionButton.extended(
            shape: const StadiumBorder(),
            onPressed: () => Navigator.of(
              context, rootNavigator: true,
            ).push(
              MaterialPageRoute(
                builder: (_) => const FriendAddDialog(),
                fullscreenDialog: true,
              ),
            ),
            label: Text(
              applCustomValue.addFriend!.defaultValue?.toString() ?? "＋",
            ),
          ) : null,
      ),
    );
  }

  Widget _buildPage(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
    ApplCustomInterface applCustomValue,
  ) => Container(
    width: double.infinity,
    child: Column(
      children: [
        if (applCustomValue.header != null) _buildBlock(
          _buildHeader(
            context, ref, controllers, applCustomValue.header!,
          ),
        ),
        _buildSections(
          context, ref, controllers, applCustomValue.sections,
        ),
      ],
    ),
  );
}
