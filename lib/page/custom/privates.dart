import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart' show showError;
import '../../widget.dart';
import '../timeline/timeline_page.dart' show mayOpenTimelinePage;
import '../friend/friend_page.dart' show openFriendOrUserPage;

extension AddController on Set<PlrNotifier> {
  void addController(
    BuildContext context,
    WidgetRef ref,
    PlrAsyncNotifierProvider provider,
  ) {
    add(ref.read(provider.notifier));
    ref.listen(provider, (_, next) => processError(context, next));
  }
}

extension IntValueNotifierIsForced on ValueNotifier<int> {
  bool get isForced => value.isEven;
}

class _RefreshController implements PlrNotifier {
  ValueNotifier<int> state;
  _RefreshController(this.state);

  @override
  Future<void> refresh({ bool force = false }) async {
    final isForced = state.isForced;
    if ((isForced && force) || (!isForced && !force)) state.value += 2;
    else state.value++;
  }
}

ValueNotifier<int> createRefreshState(Set<PlrNotifier> controllers) {
  final refreshState = useState(0);
  controllers.add(_RefreshController(refreshState));

  return refreshState;
}

List<Channel> targetChannelsOf(
  BuildContext context,
  WidgetRef ref,
  HasChannels? hasChannels,
  Iterable<DataSetting> css,
  bool useDestTag,
  Set<PlrNotifier> controllers,
) {
  if (hasChannels == null) return const [];

  final dsIds = css.map((ds) => ds.id).toSet();

  final destTag = useDestTag ? switch (hasChannels) {
    FriendToMeRoot() => hasChannels.friend.myId,
    MeToFriendRoot() => hasChannels.friend.friendId,
    _ => null,
  } : null;

  final provider = hasChannelsSyncProvider(hasChannels);
  controllers.addController(context, ref, provider);

  final value = ref.watch(provider).value;

  if (useDestTag) return value?.channels.where(
    (c) => (
      (c.destTag == destTag) && dsIds.contains(c.generatedFrom)
    ),
  ).toList() ?? const [];

  final channelValueMap = Map<Channel, ChannelState?>.fromIterable(
    value?.channels ?? const [],
    value: (c) => ref.watch(channelSyncProvider(c)).value,
  );

  return channelValueMap.entries.map(
    (e) => (
      dsIds.contains(e.key.generatedFrom) ||
      dsIds.contains(e.value?.channelDataSetting?.id)
    ) ? e.key : null,
  ).nonNulls.toList();
}

Widget buildChannelsSection(
  BuildContext context,
  WidgetRef ref,
  Account account,
  HasChannels hasChannels,
  Set<PlrNotifier> controllers, {
    AsyncListItemsGetter<
      Channel, HasChannelsState
    > itemsGetter = defaultChannelListItemsGetter,
    Widget? empty = emptyWidget,
    String? showAllButtonTooltip,
}) => Consumer(
  builder: (context, ref, _) {
    final provider = hasChannelsSyncProvider(hasChannels);
    controllers.addController(context, ref, provider);

    return ChannelList<
      Channel, HasChannelsSyncProvider, HasChannelsSync, HasChannelsState
    >(
      shrinkWrap: true,
      provider: provider,
      container: hasChannels,
      itemsGetter: itemsGetter,
      cellConfig: channelCellConfigWithDefault(
        onTap: (channel, controller) => mayOpenTimelinePage(
          context, account, channel, onPop: (_) => controller.refresh(),
        ),
        onError: (e, s) => showError(context, e, s),
      ),
      empty: empty,
    );
  },
);

Widget buildDisclosedToUsersSection(
  BuildContext context,
  WidgetRef ref,
  Account account,
  Channel channel,
  Set<PlrNotifier> controllers, {
    AsyncListItemsGetter<
      DisclosedToUser, ChannelState
    > itemsGetter = defaultDisclosedToUserListItemsGetter,
    Widget? empty = emptyWidget,
}) => Consumer(
  builder: (context, ref, _) {
    final provider = channelSyncProvider(channel);
    controllers.addController(context, ref, provider);

    return DisclosedToUserList<ChannelSyncProvider, ChannelSync, ChannelState>(
      shrinkWrap: true,
      provider: provider,
      container: channel,
      itemsGetter: itemsGetter,
      itemBuilder: (
        key, context, ref, config, item, referrer, forceRefreshOnCreate,
        selected,
      ) => UserCell(
        key: key, ref: ref, config: config, object: item, referrer: referrer,
        forceRefreshOnCreate: forceRefreshOnCreate, selected: selected,
        showEmail: false,
      ),
      cellConfig: disclosedToUserCellConfigWithDefault(
        onTap: (disclosedToUser, _) => openFriendOrUserPage(
          context, account, disclosedToUser,
        ),
        onError: (e, s) => showError(context, e, s),
      ),
      empty: empty,
    );
  },
);
