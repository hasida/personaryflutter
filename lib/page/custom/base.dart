part of 'custom_page.dart';

abstract class _Base extends StatelessConsumerPlrWidget {
  final ApplCustomInterface applCustom;
  _Base(this.applCustom, { super.key });

  late final ApplCustomSyncProvider? _applCustomSyncProvider;
  late final ApplCustomSync? _applCustomSyncController;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _applCustomSyncProvider = (applCustom is ApplCustom)
      ? applCustomSyncProvider(applCustom as ApplCustom) : null;
    _applCustomSyncController = (_applCustomSyncProvider != null)
      ? ref.read(_applCustomSyncProvider!.notifier) : null;
  }

  Widget _buildBlock(Widget content) => Padding(
    padding: bottomPadding[3],
    child: content,
  );

  Widget _mayAssignTooltip({
      Literal? tooltip,
      bool useTitle = true,
      Literal? title,
      String? additionalText,
      required Widget child,
  }) {
    var message = tooltip?.defaultValue?.toString() ?? (
      useTitle ? title?.defaultValue?.toString() : null
    );
    if (additionalText != null) {
      message = (message ?? "") + additionalText;
    }
    if (message == null) return child;

    return Tooltip(
      message: message,
      child: child,
    );
  }

  Widget _mayAssignTitleTooltip(
    HasApplTitleInterface hasApplTitle, {
      bool useTitle = true,
      String? additionalText,
      required Widget child,
  }) => _mayAssignTooltip(
    tooltip: hasApplTitle.commentText,
    useTitle: useTitle, title: hasApplTitle.titleLabel,
    additionalText: additionalText,
    child: child,
  );

  Widget _mayAssignCardTitleTooltip(
    HasApplCardTitleInterface hasApplCardTitle, {
      bool useCardTitle = true,
      String? additionalText,
      required Widget child,
  }) => _mayAssignTooltip(
    tooltip: hasApplCardTitle.cardCommentText,
    useTitle: useCardTitle, title: hasApplCardTitle.cardTitleLabel,
    additionalText: additionalText,
    child: child,
  );
}
