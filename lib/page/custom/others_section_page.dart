import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../style.dart';
import '../../widget.dart';
import 'others_section_page.i18n.dart';
import 'privates.dart';

class OthersSectionPage extends StatelessConsumerPlrWidget {
  final ApplOthersSInterface section;
  final Friend friend;
  final UserInfoUpdateProvider _userInfoUpdateProvider;
  OthersSectionPage(
    this.section,
    this.friend,
    this._userInfoUpdateProvider, {
      super.key,
  });

  late final UserInfoUpdate _userInfoUpdateController;

  late final ProfileItemsProvider _profileItemsProvider;
  late final ProfileItems _profileItemsController;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _userInfoUpdateController = ref.read(_userInfoUpdateProvider.notifier);

    _profileItemsProvider = profileItemsProvider(friend);
    _profileItemsController = ref.read(_profileItemsProvider.notifier);
  }

  void _initListeners(BuildContext context, WidgetRef ref) {
    ref.listen(
      _userInfoUpdateProvider, (_, next) => processError(context, next),
    );
    ref.listen(
      _profileItemsProvider, (_, next) => processError(context, next),
    );
  }

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    _initListeners(context, ref);

    final controllers = <PlrNotifier>{};
    Future<void> _refresh([ bool force = false ]) => Future.wait([
        _userInfoUpdateController.refresh(force: force),
        _profileItemsController.refresh(force: force),
        ...controllers.map((c) => c.refresh(force: force)),
    ]);

    final title = (
      (
        section.cardTitleLabel ?? section.titleLabel
      )?.defaultValue.toString() ?? "Others Section".i18n
    ) + ": ${ref.watch(_userInfoUpdateProvider).value?.name ?? friend.id}";

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _refresh,
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: _buildPage(context, ref, controllers),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPage(
    BuildContext context,
    WidgetRef ref,
    Set<PlrNotifier> controllers,
  ) {
    final f2mDsIds = section.css.map((ds) => ds.id).toSet();

    final m2fDestTag = friend.friendId;
    final m2fDsIds = section.myCSs.map((ds) => ds.id).toSet();

    final m2fDsIds1 = section.myCS1s.map((ds) => ds.id).toSet();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UserHeader(friend, provider: _userInfoUpdateProvider),

        Padding(
          padding: bottomPadding[3],
          child: ProfileView(
            provider: _profileItemsProvider,
            exclude: const [ nameClass, pictureClass ],
          ),
        ),

        divider,
        const SizedBox(height: 5),

        if (friend.friendToMeRoot != null) buildChannelsSection(
          context, ref, account, friend.friendToMeRoot!, controllers,
          itemsGetter: (value) => defaultChannelListItemsGetter(value).where(
            (c) => f2mDsIds.contains(c.generatedFrom),
          ).toList(),
        ),
        if (friend.meToFriendRoot != null) buildChannelsSection(
          context, ref, account, friend.meToFriendRoot!, controllers,
          itemsGetter: (value) => defaultChannelListItemsGetter(value).where(
            (c) => (
              (c.destTag == m2fDestTag) && m2fDsIds.contains(c.generatedFrom)
            ) || (
              (c.destTag == null) && m2fDsIds1.contains(c.generatedFrom)
            ),
          ).toList(),
        ),
      ],
    );
  }
}
