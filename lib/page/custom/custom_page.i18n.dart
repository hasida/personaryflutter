import 'package:i18n_extension/i18n_extension.dart';
import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  unknownTranslation +
  {
    "en_us": "Select a schema",
    "ja_jp": "スキーマを選択してください",
  } +
  {
    "en_us": "Unable to add items to the timeline.",
    "ja_jp": "タイムラインにアイテムを追加できません。",
  } +
  {
    "en_us": "No applicable schemata could be found.",
    "ja_jp": "適用できるスキーマが見付かりませんでした。",
  } +
  {
    "en_us": "There are no applicable classes.",
    "ja_jp": "適用できるクラスがありません。",
  };

  String get i18n => localize(this, t);
}
