import 'dart:async';
import 'dart:io' as io;
import 'dart:typed_data';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart'
  hide ChangeNotifierProvider, Consumer, Notifier, Provider;
import 'package:googleapis/people/v1.dart' as people;
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as img;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:personaryFlutter/flavors.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import '../main/main_page.dart';
import 'profile_page.i18n.dart';

// const bool GO_TO_MAINPAGE = true;
// const bool GO_TO_PREPAGE = false;

class ProfilePage extends StatefulPlrWidget {
  final Account _account;
  final bool _nextPage;

  const ProfilePage(this._account, this._nextPage, {super.key})
      : super(_account);

  @override
  ConsumerState<ProfilePage> createState() => _ProfileRegisterFormState();
}

class _ProfileRegisterFormState extends PlrState<ProfilePage> {
  Map<String, List<Item>> items = {};
  Map<String, List<Item>> registered_items = {};
  Item? nicknameItem;
  String? _familyName = '';
  String? _givenName = '';
  DateTime _birthday = DateTime.now();
  DateTime now = DateTime.now();
  String? _gender = '';
  String _birthDate = 'Birth date'.i18n;
  Image? _image;
  late io.File _fileImage;
  Uint8List? bytes;
  String _imgMimeType = "image/jpeg";

  String? registered_nickName = '';
  String? registered_familyName = '';
  String? registered_givenName = '';
  String registered_birthDate = 'Birth date'.i18n;
  DateTime registered_birthday = DateTime.now();
  String registered_gender = '';
  Image? registered_image;


  String? _nickname = '';
  String relationValue = '';
  String careDate = 'Care date'.i18n;

  final nicknameController = TextEditingController();
  final familyNameController = TextEditingController();
  final givenNameController = TextEditingController();

  Future<void> _updateItems() async {
    Map<String, bool> _isRegisteredValues = {
      nameClass: false,
      pictureClass: false,
      birthClass: false,
      genderClass: false,
    };

    if (mounted) {
      ///登録済みプロフィール情報を各項目に追記
      var userProfile = profileOf(root,
          classes: [nameClass, pictureClass, birthClass, genderClass]);
      await userProfile.handleError((e, s) => showError(context, e, s)).listen(
        (event) {
          event.forEach((profileKey, profileValue) {
            if (profileValue.last.entries.isNotEmpty) {
              if (profileKey.id == nameClass) {
                if (F.appFlavor == Flavor.PASTELD) {
                  var ni = profileValue.lastWhereOrNull((i) =>
                  i.firstPropertyModelOf<Literal>(titleProperty)?.defaultValue=="Nickname".i18n);
                  if (ni != null) {
                    _isRegisteredValues[profileKey.id] = true;
                    nicknameController.text = _nickname = registered_nickName
                    = ni.firstPropertyModelOf<Literal>(givenNameProperty)?.defaultValue;
                    (registered_items["Nickname"] ??= <Item>[]).add(ni);
                  }
                }
                var na = profileValue.lastWhereOrNull((i) => i.hasProperty(titleProperty)==false);
                if (na == null) na = profileValue.lastWhereOrNull((i) =>
                i.firstPropertyModelOf<Literal>(titleProperty)?.defaultValue!="Nickname".i18n);
                print(na);
                if (na != null) {
                  _isRegisteredValues[profileKey.id] = true;
                  familyNameController.text = _familyName = registered_familyName
                  = na.firstPropertyModelOf<Literal>(familyNameProperty)?.defaultValue;
                  givenNameController.text = _givenName = registered_givenName
                  = na.firstPropertyModelOf<Literal>(givenNameProperty)?.defaultValue;
                  (registered_items[nameClass] ??= <Item>[]).add(na);
                }
              } else if (profileKey.id == pictureClass) {
                bytes = recentPictureFileOf(event)?.thumbnailData;
                _image = registered_image = (bytes != null) ?
                  Image.memory(bytes!, fit: BoxFit.contain) : null;
                _isRegisteredValues[profileKey.id] = true;
                (registered_items[pictureClass] ??= <Item>[]).add(
                  profileValue.last,
                );
              } else if (profileKey.id == birthClass) {
                _birthday = registered_birthday =
                  profileValue.last.begin?.toLocal() ?? _birthday;
                _birthDate = registered_birthDate =
                  DateFormat.yMMMd().format(_birthday);
                _isRegisteredValues[profileKey.id] = true;
                (registered_items[birthClass] ??= <Item>[]).add(
                  profileValue.last,
                );
              } else if (profileKey.id == genderClass) {
                var ge = profileValue.last;
                _gender = registered_gender = localizationGender(
                  ge.firstLiteralValueOf<String>(genderProperty),
                );
                _isRegisteredValues[profileKey.id] = true;
                (registered_items[genderClass] ??= <Item>[]).add(ge);
              }
            }
          });
        },
        onError: (e, s) {
          showError(context, e, s);
        },
      ).asFuture();

      ///登録データがない項目は、外部サービスから取得したプロフィール情報で各項目を上書き
      Future<people.Person?> _ret = getGoogleProfiles(widget._account.storage);
      await _ret.then((ret) async {
        if (_isRegisteredValues[nameClass] == false) {
          if (ret?.names?.last.familyName != null)
            familyNameController.text = (_familyName = ret!.names!.last.familyName)!;
          if (ret?.names?.last.givenName != null)
            givenNameController.text = (_givenName = ret!.names!.last.givenName)!;
        }
        if (_isRegisteredValues[birthClass] == false) {
          if (ret?.birthdays?.last.date?.year != null &&
              ret?.birthdays?.last.date?.month != null &&
              ret?.birthdays?.last.date?.day != null) {
            _birthday = DateTime(ret!.birthdays!.last.date!.year!,
                ret.birthdays!.last.date!.month!, ret.birthdays!.last.date!.day!);
            _birthDate = (DateFormat.yMMMd().format(_birthday)).toString();
          }
        }
        if (_isRegisteredValues[genderClass] == false) {
          if (ret?.genders?.last.value != null) _gender = localizationGender(ret!.genders!.last.value);
        }

        if (_isRegisteredValues[pictureClass] == false) {
          if (ret!.photos!.last.url != null) {
            http.get(Uri.parse(ret.photos!.last.url!)).then((response) {
              _imgMimeType = response.headers["content-type"] ?? "image/jpeg";
// lookupMimeType(Uri.parse(uri).path, headerBytes: data);
              bytes = response.bodyBytes;
              setState(() {
                _image = Image.memory(bytes!, fit: BoxFit.contain);
              });
            });
          }
        }
        setState(() {});
      });
    }
  }

  ///日付を選択
  Future<void> _selectDate(BuildContext context, String type) async {
    DateTime? date = await showDatePicker(
      context: context,
      locale: const Locale("ja"),
      initialDatePickerMode: DatePickerMode.year,
//最初に年から入力
      initialDate: DateTime(now.year - 17),
      firstDate: DateTime(now.year - 130, now.month, now.day),
      lastDate: DateTime(now.year, now.month, now.day),
    );
    if (date != null) {
      setState(() {
        if (type == 'Birth date'.i18n) {
          _birthday = date;
          _birthDate = (DateFormat.yMMMd().format(date)).toString();
        } else if (type == 'Care date'.i18n) {
          careDate = (DateFormat.yMMMd().format(date)).toString();
        }
      });
    }
  }

  /// 登録前に名前が入力されているか確認
  Future<bool> enteredProfile(BuildContext mainContext) async {
    bool isEnteredRequiredItem = false;
    String _message = "";
    if (F.appFlavor == Flavor.PASTELD) {
      if (_givenName == '' && _nickname == '') {
        isEnteredRequiredItem = true;
        _message = "Enter your given name and nickname.".i18n;
      } else if (_givenName == '') {
        isEnteredRequiredItem = true;
        _message = "Enter your given name.".i18n;
      } else if (_nickname == '') {
        isEnteredRequiredItem = true;
        _message = "Enter your nickname.".i18n;
      }
    } else {
      if (_givenName == '') {
        isEnteredRequiredItem = true;
        _message = "Enter your given name.".i18n;
      }
    }
    if (isEnteredRequiredItem) {
      showDialog(
        context: mainContext,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text("Name not entered.".i18n),
            content: Text(_message),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text("OK".i18n),
                isDestructiveAction: true,
                onPressed: () => Navigator.pop(context),
              ),
            ],
          );
        },
      );
      return false;
    } else {
      await registerProfile(mainContext);
      return true;
    }
  }

  Future<bool> _chkDiscloseSpecifiedProfileItems(PublicRoot publicRoot, String property) async {
    await publicRoot.syncSilently();
    try {
      await Future.wait([
        _hasProfileItem(publicRoot, property),
      ], eagerError: true,
      );
    } on bool catch (_) {
      return false;
    }
    return true;
  }

  Future<void> _hasProfileItem(ProfileContainer container, String name) async {
    try {
      await Future.wait(
        container.profileItemsOf(name).map((i) async {
          await i.syncSilently();
          if (!i.isDeleted) {
            throw true;
          }
        }).toList(), eagerError: true,
      );
    } on bool catch (_) {
      return;
    }
    throw false;
  }

  /// LifeRecordと入力データの登録
  Future<void> registerProfile(BuildContext mainContext) async {
    var lifeRecord = root.lifeRecord!;
    final progress = ProgressHUD.of(mainContext)!;
    Map<String, bool> _isRegisterValues = {
      nameClass: false,
      pictureClass: false,
      birthClass: false,
      genderClass: false,
      "Nickname": false,
    };

    progress.show(); try {
      //すでに登録を試みている場合はスキップ
      if (items.isEmpty) {
        //登録前プロフィールデータと一致しない場合、登録する
        if (_nickname != registered_nickName && F.appFlavor == Flavor.PASTELD)
          _isRegisterValues["Nickname"] = true;
        if (_familyName != registered_familyName ||
            _givenName != registered_givenName)
          _isRegisterValues[nameClass] = true;
        if (_image != registered_image)
          _isRegisterValues[pictureClass] = true;
        if (_birthDate != registered_birthDate)
          _isRegisterValues[birthClass] = true;
        if (_gender != registered_gender)
          _isRegisterValues[genderClass] = true;

//登録前プロフィールデータのうち、画像と名前が公開されていなければ登録・公開する
        if (!await _chkDiscloseSpecifiedProfileItems(
            widget._account.publicRoot, nameProperty)) {
          if (F.appFlavor == Flavor.PASTELD)
            _isRegisterValues["Nickname"] = true;
          else
            _isRegisterValues[nameClass] = true;
        }
        if (!await _chkDiscloseSpecifiedProfileItems(
            widget._account.publicRoot, pictureProperty))
          _isRegisterValues[pictureClass] = true;
      }

      await Future.wait(_isRegisterValues.entries.map((e) async {
        if (e.value) {
          var registered_item = registered_items[e.key]?.last;
          Item newItem;
          if (registered_item == null) {
            var type = e.key;
            if (e.key=="Nickname") type = nameClass;
            newItem = await lifeRecord.newTimelineItemModel(
              type, begin: _birthday,
            )..creatorPlrId = root.plrId;
          }else{
            newItem = registered_item;
          }
          if (e.key == nameClass) {
            if (newItem == registered_item) {
              if (newItem.hasProperty(givenNameProperty))
                newItem.removePropertiesOf(givenNameProperty);
              if (newItem.hasProperty(familyNameProperty))
                newItem.removePropertiesOf(familyNameProperty);
            }
            (items[nameClass] ??= <Item>[]).add(newItem);
            newItem..newLiteralModel(
                familyNameProperty, value: _familyName)..newLiteralModel(
                givenNameProperty, value: _givenName);
          } else if (e.key == pictureClass) {
            if (newItem == registered_item) {
              if (newItem.hasProperty(cntProperty)) {
                print(newItem.removePropertiesOf(cntProperty));
              }
            }
            (items[pictureClass] ??= <Item>[]).add(newItem);
            (await newItem.newFileModel(cntProperty, format: _imgMimeType))
              ..putData(bytes!)
              ..setThumbnailImage(img.decodeImage(bytes!)!);
          } else if (e.key == genderClass) {
            if (newItem == registered_item) {
              if (newItem.hasProperty(genderProperty))
                newItem.removePropertiesOf(genderProperty);
            }
            (items[genderClass] ??= <Item>[]).add(newItem);
            newItem.newLiteralModel(genderProperty,
                value: anglicizationGender(_gender));
          } else if (e.key == birthClass) {
            if (newItem == registered_item) {
              if (newItem.hasProperty(birthProperty))
                newItem.removePropertiesOf(birthProperty);
              newItem.setBegin(_birthday);
            }
            (items[birthClass] ??= <Item>[]).add(newItem);
          } else if (e.key == "Nickname") {
            if (newItem == registered_item) {
              if (newItem.hasProperty(givenNameProperty))
                newItem.removePropertiesOf(givenNameProperty);
            }
            (items[nameClass] ??= <Item>[]).add(newItem);
            newItem..newLiteralModel(
                givenNameProperty, value: _nickname)..newLiteralModel(
                titleProperty, valueMap: { "": "Nickname", "ja": "ニックネーム"}
            );
            nicknameItem = newItem;
          }
          await newItem.commitNewVersion();
        }
      }).toList());

      try {
        //プロフィールアイテムをLifeRecordに登録
        if (!await syncItem(items.values.expand((i) => i), lifeRecord)) {
          // ネットワークがないと進めないことをユーザに告知。
          showNeedNetworkDialog(mainContext);
          return;
        }
        //公開処理するアイテムを登録されたprofileから取得してprofileItemsに格納
        String uncapitalize(String str) => "${str[0].toLowerCase()}${str.substring(1)}";

        List<ProfileItem> profileItems = [];
        {
          //(PastelDの場合) nameClassのうち特定アイテム（ニックネーム）のみ公開
          Map<String, List<Item>> disclose_items = {};
          if (items[pictureClass] != null)
            (disclose_items[pictureClass] ??= <Item>[]).addAll(items[pictureClass]!);
          if (F.appFlavor == Flavor.PASTELD) {
            if (nicknameItem != null)
              (disclose_items[nameClass] ??= <Item>[]).add(nicknameItem!);
          } else {
            if (items[nameClass] != null)
              (disclose_items[nameClass] ??= <Item>[]).addAll(items[nameClass]!);
          }

          for (var e in disclose_items.entries) {
            var ids = e.value.map((i) => i.id);
            profileItems.addAll(
              root.lifeRecord!.profileItemsOf(uncapitalize(e.key)).where((i) => ids.contains(i.id)),
            );
          }
        }

        //公開処理
        try {
          await ProfileDiscloseManager(publicRoot).addItems(profileItems);
        } catch (e) {
          print(e);
        }
      } catch(e) {
        print(e);
      }
    } catch (e) {
      progress.dismiss();
      print(e);
      print('Profile Data could not record and disclose.');

      widget._nextPage ?
        Navigator.pushAndRemoveUntil(
          mainContext,
          CupertinoPageRoute(
            builder: (_) => const MainPage(isSignIn: true),
          ),
          (_) => false
        ) :
        Navigator.pop(mainContext);
    } finally {
      // 自分のユーザデータベースを強制更新しておく。
      await root.userData(true).drain();

      progress.dismiss();
      widget._nextPage ?
        Navigator.pushAndRemoveUntil(
          mainContext,
          CupertinoPageRoute(
            builder: (_) => const MainPage(isSignIn: true),
          ),
          (_) => false
        ) :
        Navigator.pop(mainContext);
    }
  }

  Future<bool> syncItem(Iterable<Item> myItems, LifeRecord lifeRecord) async {
    await lifeRecord
        .syncNewTimelineItems()
        .handleError((e, s) => showError(context, e, s))
        .drain();
    if (myItems.isEmpty) return true;

    // アイテムが送信され、URIが決定するまで待機する
    if (!await myItems.first.waitForSent(timeout: const Duration(minutes: 1))) {
      return false;
    }
    for (var i in myItems) {
      i.registerRare();
    }
    await lifeRecord.syncSilently();

    return true;
  }

  ///性別の表記フォーマットの調整
  String? anglicizationGender(gender) {
    if (storage.type == storageTypeOf("googleDrive")) {
      if (gender == 'Male' || gender == 'Male'.i18n)
        gender = 'Male';
      else if (gender == 'Female' || gender == 'Female'.i18n)
        gender = 'Female';
      else if (gender == 'Other' || gender == 'Other'.i18n) gender = 'Other';
      else gender = '';
    }

    return gender;
  }
  String localizationGender(String? gender) {
    if (gender == "male" || gender == "Male")
      gender = 'Male'.i18n;
    else if (gender == "female" || gender == "Female")
      gender = 'Female'.i18n;
    else if (gender == "unspecified" || gender == "Other")
      gender = 'Other'.i18n;
    else gender = '';

    return gender;
  }

  @override
  Widget build(BuildContext context) => SafeArea(
    child: ProgressHUD(
      child: FutureBuilder(
          future: registeredStorageInfos,
          builder: (context, snapshot) {
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  'Input your information.'.i18n,
                ),
              ),
              body: Container(
                child: Scrollbar(
                  thumbVisibility: false,
                  child: SingleChildScrollView(
                    child: Container(
                      padding: const EdgeInsets.all(30),
                      child: Column(
                        children: <Widget>[
                          if (F.appFlavor == Flavor.PASTELD)
                          Center(
                              child: Text(
                                  "Input your information. The profile image and the nickname will be published.".i18n))
                          else
                          Center(
                          child: Text(
                          "Input your information. The profile image and the name will be published.".i18n)),
                          const Divider(
                            height: 20,
                            thickness: 5,
                            indent: 20,
                            endIndent: 20,
                          ),
                          if (F.appFlavor == Flavor.PASTELD)
                            Center(
                                child: SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height * 0.2,
                                  child: Image.asset(
                                    F.backGroundImagePath,
                                  ),
                                )
                            ),
                          if (F.appFlavor == Flavor.PASTELD)
                            const Divider(
                              height: 20,
                              thickness: 5,
                              indent: 20,
                              endIndent: 20,
                            ),
                          Column(children: <Widget>[
                            Container(
                              width: double.infinity,
                              margin: const EdgeInsets.only(bottom: 20),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    color: Colors.lightGreen[200],
                                    child: Text(
                                      'Profile image'.i18n,
                                      style: const TextStyle(
                                        decoration:
                                        TextDecoration.underline,
                                        decorationColor: Colors.lightGreen,
                                      ),
                                    ),
                                  ),
                                  Container(
                                      width: 150,
                                      height: 150,
                                      margin:
                                      const EdgeInsets.only(top: 10),
                                      child:
                                      InkWell(
                                          onTap: () async {
                                            if (isMobile && io.Platform.isIOS) {
                                              final _picker = ImagePicker();
                                              XFile? _pickedFile =
                                              await _picker.pickImage(
                                                  source:
                                                  ImageSource.gallery);
                                              if (_pickedFile != null) {
                                                _fileImage =
                                                    io.File(_pickedFile.path);
                                                bytes = await _fileImage
                                                    .readAsBytes();
                                                setState(() {
                                                  _image = Image.file(_fileImage);
                                                });
                                              }
                                            } else {
                                              FilePickerResult? result =
                                              await FilePicker.platform
                                                  .pickFiles(
                                                type: FileType.custom,
                                                allowedExtensions: [
                                                  'jpg',
                                                  'jpeg',
                                                  'png',
                                                  'gif'
                                                ],
                                              );
                                              if (result != null) {
                                                setState(() {
                                                  _fileImage = io.File(
                                                      result.files.single.path!);
                                                  bytes = _fileImage
                                                      .readAsBytesSync();
                                                  _image = Image.file(_fileImage);
                                                });
                                              }
                                            }
                                          },
                                          child: _displaySelectionImageOrGrayImage())
                                  ),
                                ],
                              ),
                            )
                          ]),
                          if (F.appFlavor == Flavor.PASTELD)
                            Column(children: <Widget>[
                              Container(
                                width: double.infinity,
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      color: Colors.lightGreen[200],
                                      child: Text(
                                        'Nickname'.i18n,
                                        style: const TextStyle(
                                          decoration:
                                          TextDecoration.underline,
                                          decorationColor:
                                          Colors.lightGreen,
                                        ),
                                      ),
                                    ),
                                    TextField(
                                      enabled: true,
                                      obscureText: false,
                                      maxLines: 1,
                                      maxLength: 100,
                                      decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        hintText: 'Nickname'.i18n,
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.lightGreen,
                                          ),
                                          ),
                                        ),
                                        onChanged: (text) {
                                          _nickname = text;
                                        },
                                        controller: nicknameController
                                    )
                                  ],
                                ),
                              )
                            ]),
                          Column(children: <Widget>[
                            Container(
                              width: double.infinity,
// height: 60,
// color: Colors.lightGreen[200],
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    color: Colors.lightGreen[200],
                                    child: Text(
                                      'Family name'.i18n,
                                      style: const TextStyle(
                                        decoration:
                                        TextDecoration.underline,
                                        decorationColor: Colors.lightGreen,
                                      ),
                                    ),
                                  ),
                                  TextField(
                                      enabled: true,
                                      obscureText: false,
                                      maxLines: 1,
                                      maxLength: 100,
                                      decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        hintText: 'Family name'.i18n,
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.lightGreen,
                                          ),
                                        ),
                                      ),
                                      onChanged: (text) {
                                        _familyName = text;
                                      },
                                      controller: familyNameController),
                                ],
                              ),
                            )
                          ]),
                          Column(children: <Widget>[
                            Container(
                              width: double.infinity,
// height: 60,
// color: Colors.lightGreen[200],
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    color: Colors.lightGreen[200],
                                    child: Text(
                                      'Given name'.i18n,
                                      style: const TextStyle(
                                        decoration:
                                        TextDecoration.underline,
                                        decorationColor: Colors.lightGreen,
                                      ),
                                    ),
                                  ),
                                  TextField(
                                      enabled: true,
                                      obscureText: false,
                                      maxLines: 1,
                                      maxLength: 100,
                                      decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        hintText: 'Given name'.i18n,
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.lightGreen,
                                          ),
                                        ),
                                      ),
                                      onChanged: (text) {
                                        _givenName = text;
                                      },
                                      controller: givenNameController),
                                ],
                              ),
                            )
                          ]),
                          const Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Text(''),
                          ),
                          Container(
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  color: Colors.lightGreen[200],
                                  child: Text(
                                    'Gender'.i18n,
                                    style: const TextStyle(
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.lightGreen,
                                    ),
                                  ),
                                ),
                                Container(
                                  child: DropdownButton<String>(
                                    value: _gender,
                                    icon: const Icon(Icons.arrow_downward),
                                    underline: Container(
                                      height: 2,
                                      color: Colors.black54,
                                    ),
                                    onChanged: (String? newValue) {
                                      setState(() {
                                        _gender = newValue;
                                      });
                                    },
                                    items: <String>[
                                      '',
                                      'Male'.i18n,
                                      'Female'.i18n,
                                      'Other'.i18n,
                                    ].map<DropdownMenuItem<String>>(
                                            (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Text(''),
                          ),
                          Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(16.0),
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 1,
                                      child: Container(
                                          color: Colors.lightGreen[200],
                                          child: Text(
                                            'Birth date'.i18n,
                                            style: const TextStyle(
                                              decoration:
                                              TextDecoration.underline,
                                              decorationColor:
                                              Colors.lightGreen,
                                            ),
                                          ))),
                                  Expanded(
                                    flex: 4,
                                    child: new TextField(
// enabled: false,
                                        readOnly: true,
                                        style: const TextStyle(fontSize: 14),
                                        controller: TextEditingController(
                                            text: ('${_birthDate}')),
                                        decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          hintText: 'Birth date'.i18n,
                                          enabledBorder: const OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightGreen,
                                            ),
                                          ),
                                        ),
                                        onTap: () {
                                          _selectDate(
                                              context, 'Birth date'.i18n);
                                        }),
                                  ),
// Expanded(
//   flex: 1,
//   child: IconButton(
//       icon: Icon(Icons.date_range),
//       onPressed: () => _selectDate(
//           context, 'Birth date'.i18n)),
// ),
                                ],
                              ),
                            ),
                          ),
                          // if (F.appFlavor == Flavor.PASTELD)
                          //   const Divider(
                          //     height: 20,
                          //     thickness: 5,
                          //     indent: 20,
                          //     endIndent: 20,
                          //   ),
                          // if (F.appFlavor == Flavor.PASTELD)
                          //   Column(
                          //       crossAxisAlignment:
                          //           CrossAxisAlignment.start,
                          //       children: [
                          //         const Padding(
                          //           padding: const EdgeInsets.all(5.0),
                          //           child: Text(''),
                          //         ),
                          //         Container(
                          //             color: Colors.tealAccent,
                          //             child: Text(
                          //               'いつから介護していますか？',
                          //               style: const TextStyle(
                          //                 decoration:
                          //                     TextDecoration.underline,
                          //                 decorationColor:
                          //                     Colors.lightGreen,
                          //               ),
                          //             )),
                          //         Container(
                          //           width: double.infinity,
                          //           padding: const EdgeInsets.all(16.0),
                          //           child: Center(
                          //             child: Row(
                          //               children: <Widget>[
                          //                 Expanded(
                          //                   flex: 5,
                          //                   child: new TextField(
                          //                     readOnly: true,
                          //                     style:
                          //                         const TextStyle(fontSize: 14),
                          //                     controller:
                          //                         TextEditingController(
                          //                             text:
                          //                                 ('${careDate}')),
                          //                     decoration: InputDecoration(
                          //                       border:
                          //                           const OutlineInputBorder(),
                          //                       hintText: 'いつから介護していますか？',
                          //                       enabledBorder:
                          //                           const OutlineInputBorder(
                          //                         borderSide: BorderSide(
                          //                           color:
                          //                               Colors.tealAccent,
                          //                         ),
                          //                       ),
                          //                     ),
                          //                     onTap: () {
                          //                       _selectDate(context,
                          //                           'Care date'.i18n);
                          //                     },
                          //                   ),
                          //                 ),
                          //               ],
                          //             ),
                          //           ),
                          //         ),
                          //       ]),
                          // if (F.appFlavor == Flavor.PASTELD)
                          //   const Padding(
                          //     padding: const EdgeInsets.all(5.0),
                          //     child: Text(''),
                          //   ),
                          // if (F.appFlavor == Flavor.PASTELD)
                          //   Container(
                          //     width: double.infinity,
                          //     child: Column(
                          //       crossAxisAlignment:
                          //           CrossAxisAlignment.start,
                          //       children: [
                          //         Container(
                          //           color: Colors.tealAccent,
                          //           child: Text(
                          //             '被介護者の続柄',
                          //             style: const TextStyle(
                          //               decoration:
                          //                   TextDecoration.underline,
                          //               decorationColor: Colors.lightGreen,
                          //             ),
                          //           ),
                          //         ),
                          //         DropdownButton<String>(
                          //           value: relationValue,
                          //           icon: const Icon(Icons.arrow_downward),
                          //           underline: Container(
                          //             height: 2,
                          //             color: Colors.black54,
                          //           ),
                          //           onChanged: (String newValue) {
                          //             setState(() {
                          //               relationValue = newValue;
                          //             });
                          //           },
                          //           items: <String>[
                          //             '',
                          //             '父母',
                          //             '配偶者またはパートナー',
                          //             '兄弟',
                          //             '祖父母',
                          //             '親',
                          //             'その他',
                          //           ].map<DropdownMenuItem<String>>(
                          //               (String value) {
                          //             return DropdownMenuItem<String>(
                          //               value: value,
                          //               child: Text(value),
                          //             );
                          //           }).toList(),
                          //         ),
                          //       ],
                          //     ),
                          //   ),
                          // if (F.appFlavor == Flavor.PASTELD)
                          //   Column(children: <Widget>[
                          //     Container(
                          //       width: double.infinity,
                          //       child: Column(
                          //         crossAxisAlignment:
                          //             CrossAxisAlignment.start,
                          //         children: <Widget>[
                          //           Container(
                          //             color: Colors.tealAccent,
                          //             child: Text(
                          //               '被介護者の名前（姓）',
                          //               style: const TextStyle(
                          //                 decoration:
                          //                     TextDecoration.underline,
                          //                 decorationColor:
                          //                     Colors.tealAccent,
                          //               ),
                          //             ),
                          //           ),
                          //           TextField(
                          //             enabled: true,
                          //             obscureText: false,
                          //             maxLines: 1,
                          //             maxLength: 100,
                          //             decoration: InputDecoration(
                          //               border: const OutlineInputBorder(),
                          //               hintText: '被介護者の名前（姓）',
                          //               enabledBorder: const OutlineInputBorder(
                          //                 borderSide: BorderSide(
                          //                   color: Colors.tealAccent,
                          //                 ),
                          //               ),
                          //             ),
                          //             onChanged: (text) {
                          //               _careReceiverFamilyName = text;
                          //             },
                          //           ),
                          //         ],
                          //       ),
                          //     )
                          //   ]),
                          // if (F.appFlavor == Flavor.PASTELD)
                          //   Column(children: <Widget>[
                          //     Container(
                          //       width: double.infinity,
                          //       child: Column(
                          //         crossAxisAlignment:
                          //             CrossAxisAlignment.start,
                          //         children: <Widget>[
                          //           Container(
                          //             color: Colors.tealAccent,
                          //             child: Text(
                          //               '被介護者の名前（名）',
                          //               style: const TextStyle(
                          //                 decoration:
                          //                     TextDecoration.underline,
                          //                 decorationColor:
                          //                     Colors.tealAccent,
                          //               ),
                          //             ),
                          //           ),
                          //           TextField(
                          //             enabled: true,
                          //             obscureText: false,
                          //             maxLines: 1,
                          //             maxLength: 100,
                          //             decoration: InputDecoration(
                          //               border: const OutlineInputBorder(),
                          //               hintText: '被介護者の名前（名）',
                          //               enabledBorder: const OutlineInputBorder(
                          //                 borderSide: BorderSide(
                          //                   color: Colors.tealAccent,
                          //                 ),
                          //               ),
                          //             ),
                          //             onChanged: (text) {
                          //               _careReceiverGivenName = text;
                          //             },
                          //           )
                          //         ],
                          //       ),
                          //     )
                          //   ]),
                          if (F.appFlavor == Flavor.PASTELD)
                            const Divider(
                              height: 20,
                              thickness: 5,
                              indent: 20,
                              endIndent: 20,
                            ),
                          if (F.appFlavor == Flavor.PASTELD)
                            const Center(
                                child: const Text(
                                    "入力したニックネームとプロフィール画像は一般公開されます。\n他の入力情報（姓名など）は事務局に開示されます。")),
                          const Divider(
                            height: 20,
                            thickness: 5,
                            indent: 20,
                            endIndent: 20,
                          ),
                          Row(children: <Widget>[
// Expanded(
//   flex: 1,
//   child: _ProfileDiscloseButton2(
//       context, account, items, enteredProfile),
// ),
                            Expanded(
                              flex: 1,
                              child: new ElevatedButton(
                                child: Text('OK'.i18n),
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.lightGreen,
                                  foregroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                onPressed: () {
                                  enteredProfile(context);
                                },
                              ),
                            ),
                          ]),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
    ),
  );

  @override
  void initState() {
    super.initState();
    addPendingStorage(storage);

    _updateItems();
  }

  @override
  void dispose() {
    super.dispose();
    removePendingStorage(storage);
  }

  ///イメージ画像があれば表示、なければ灰色のBOXを表示
  Widget _displaySelectionImageOrGrayImage() {
    if (_image == null) {
      return Container(
        decoration: BoxDecoration(
          color: const Color(0xffdfdfdf),
          border: Border.all(
            width: 2,
            color: const Color(0xff000000),
          ),
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: const Color(0xff000000),
          ),
        ),
        child: ClipRRect(
          child: _image,
        ),
      );
    }
  }
}

///外部サービスからプロフィール情報を取得
Future<people.Person?> getGoogleProfiles(Storage storage) async {
  if (storage.type != storageTypeOf("googleDrive")) {
// Show error, or let the user to select target Google Drive account.
    return null;
  }
  var person = people.PeopleServiceApi(storage.connection);
  people.Person ret = await person.people
      .get('people/me', personFields: 'names,birthdays,genders,photos');
  return ret;
}
