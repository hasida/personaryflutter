import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
      {
        "en_us": "Set up a profile",
        "ja_jp": "プロフィール登録",
      } +
      {
        "en_us": "Male",
        "ja_jp": "男性",
      } +
      {
        "en_us": "Female",
        "ja_jp": "女性",
      } +
      {
        "en_us": "Other",
        "ja_jp": "その他",
      } +
      {
        "en_us": "Gender",
        "ja_jp": "性別",
      }
      +
      {
        "en_us": "Birth date",
        "ja_jp": "生年月日",
      }
      +
      {
        "en_us": "Name not entered.",
        "ja_jp": "名前が未記入です",
      }
      +
      {
        "en_us": "Enter your given name.",
        "ja_jp": "「名」を入力してください。",
      }
      +
      {
        "en_us": "Enter your nickname.",
        "ja_jp": "「ニックネーム」を入力してください。",
      }
      +
      {
        "en_us": "Enter your given name and nickname.",
        "ja_jp": "「名」と「ニックネーム」を入力してください。",
      }
      +
      {
        "en_us": "OK",
        "ja_jp": "決定",
      }
      +
      {
        "en_us": "Profile image",
        "ja_jp": "プロフィール画像",
      }
      +
      {
        "en_us": "Select your image",
        "ja_jp": "画像を選択",
      }
      +
      {
        "en_us": "Nickname",
        "ja_jp": "ニックネーム",
      }
      +
      {
        "en_us": "Family name",
        "ja_jp": "姓",
      }
      +
      {
        "en_us": "Given name",
        "ja_jp": "名",
      }
      +
      {
        "en_us": "Address",
        "ja_jp": "住所",
      }
      +
      {
        "en_us": "Phone number",
        "ja_jp": "電話番号",
      }

      +
      {
        "en_us": "Relationship with the caregiver",
        "ja_jp": "被介護者との関係",
      }
      +
      {
        "en_us": "Family(Father/Mother/Brother)",
        "ja_jp": "家族（父/母/兄弟）",
      }
      +
      {
        "en_us": "Relative",
        "ja_jp": "親戚",
      }
      +
      {
        "en_us": "Wife/Husband",
        "ja_jp": '妻/夫',
      }
      +
      {
        "en_us": "Care history",
        "ja_jp": "介護歴",
      }
      +
      {
        "en_us": "Within half a year",
        "ja_jp": "半年以内",
      }
      +
      {
        "en_us": "Less than a year",
        "ja_jp": "1年未満",
      }
      +
      {
        "en_us": "Less than three years",
        "ja_jp": "3年未満",
      }
      +
      {
        "en_us": "More than three years",
        "ja_jp": "3年以上",
      }
      +
      {
        "en_us": "Questions to ask experts and experienced people",
        "ja_jp": "専門家や経験者にききたいこと",
      }
      +
      {
        "en_us": "Expertise in care",
        "ja_jp": "介護の専門知識",
      }
      +
      {
        "en_us": "How to take care of dementia",
        "ja_jp": "認知症への対応方法",
      }
      +
      {
        "en_us": "Long-Term care insurance system and available services",
        "ja_jp": "介護保険制度と使えるサービス",
      }
      +
      {
        "en_us": "The future",
        "ja_jp": "将来のこと",
      }
      +
      {
        "en_us": "Other (nutrition/exercise/etc.)",
        "ja_jp": "その他（栄養/運動/他）",
      }
      +
      {
        "en_us": "",
        "ja_jp": "",
      }
      +
      {
        "en_us": "Input your information.",
        "ja_jp": "利用者情報を入力してください。",
      }
      +
      {
        "en_us": "Input your information. The profile image and the nickname will be published.",
        "ja_jp": "利用者情報を入力してください。プロフィール画像とニックネームは公開されます。",
      }
      +
      {
        "en_us": "Input your information. The profile image and the name will be published.",
        "ja_jp": "利用者情報を入力してください。プロフィール画像と姓名は公開されます。",
      }
      +
      {
        "en_us": "Caregiver",
        "ja_jp": "介護者",
      }
      +
      {
        "en_us": "CareReceiver",
        "ja_jp": "被介護者",
      }
      +
      {
        "en_us": "Yes",
        "ja_jp": "はい",
      }
      +
      {
        "en_us": "No",
        "ja_jp": "いいえ",
      }
      +
      {
        "en_us": "Care date",
        "ja_jp": "介護開始日",
      };

  String get i18n => localize(this, t);
}
