import 'dart:collection';

import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart' show TapGestureRecognizer;
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:personaryFlutter/flavors.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../constants.dart';
import '../../logic.dart';
import '../../style.dart';
import '../../util.dart';
import '../../widget.dart';
import '../main/main_page.dart';
import '../web_view/web_view_page.dart';
import '../profile/profile_page.dart';
import 'sign_page.i18n.dart';

class SignPage extends StatelessWidget {
  const SignPage();

  void _initListeners(
    BuildContext context,
    WidgetRef ref,
    ValueNotifier<bool> isSignIn,
  ) {
    var progress = ProgressHUD.of(context)!;

    ref.listen(
      plrAccountProvider, (_, next) {
        switch (next) {
          case AsyncData(:final value) when value != null: {
            _cleanupPassphraseChanged(context, ref, value.storage);
            progress.dismiss();
            _nextPage(context, value, isSignIn.value);
          }
        }
      },
    );
    ref.listen(plrStorageProvider, (_, next) {
        switch (next) {
          case AsyncData(:final value) when value == null: {
            progress.dismiss();
            isSignIn.value = true;
          }
          case AsyncError(:final error, :final stackTrace): {
            if (error is FileSystemUpgradeException) {
              _showFileSystemUpgradeErrroDialog(
                context, error, stackTrace,
              ).whenComplete(() => _tryConnect(context, ref));
            }
            else if (
              !processNeedNetworkException(context, error, stackTrace) &&
              !(error is InvalidGrantException)
            ) {
              _cleanupStorages(context, ref);
              progress.dismiss();
              isSignIn.value = true;
            }
          }
          case _: progress.show();
        }
    });
  }

  @override
  Widget build(BuildContext context) => SafeArea(
    child: ProgressHUD(
      child: HookConsumer(
        builder: (context, ref, _) {
          useEffect(() {
              Future.microtask(() => _tryConnect(context, ref));
              return null;
            }, const [],
          );

          final isSignIn = useState(false);

          _initListeners(context, ref, isSignIn);

          return switch (isSignIn.value) {
            true => _buildSignPage(context, ref),
            false => emptyPage,
          };
        },
      ),
    ),
  );

  Widget _buildSignPage(
    BuildContext context,
    WidgetRef ref,
  ) => switch (F.appFlavor) {
    Flavor.PERSONARY => _buildPersonarySignPage(context, ref),
    Flavor.PASTELD => _buildPasteldSignPage(context, ref),
  };

  Widget _buildPersonarySignPage(
    BuildContext context,
    WidgetRef ref,
  ) => Scaffold(
    body: Padding(
      padding: horizontalPadding[10],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: bottomPadding[8],
              child: Text(
                appName,
                style: const TextStyle(
                  color: Color(0xff4283c0), fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: bottomPadding[8],
              child: SizedBox(
                width: 330,
                height: 50,
                child: _buildSignInButton(context, ref),
              ),
            ),
          ],
        ),
      ),
    ),
  );

  Widget _buildPasteldSignPage(
    BuildContext context,
    WidgetRef ref,
  ) => Scaffold(
    body: SingleChildScrollView(
      child: Padding(
        padding: horizontalPadding[10],
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                child: Image.asset(
                  F.backGroundImagePath,
                ),
              ),
              Padding(
                padding: bottomPadding[8],
                child: SizedBox(
                  width: 330,
                  height: 50,
                  child: _buildSignInButton(context, ref),
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );

  Widget _buildSignInButton(
    BuildContext context,
    WidgetRef ref,
  ) => Consumer(
    builder: (context, ref, _) => ElevatedButton(
      child: Text(
        'Sign In'.i18n,
        style: const TextStyle(
          fontSize: 18, fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: switch (ref.watch(plrStorageProvider)) {
        AsyncData(:final value) when value != null  => null,
        AsyncLoading() => null,
        _ => () async {
          if (await _showSignInDialog(context)) _create(context, ref);
        },
      },
    ),
  );

  void _tryConnect(BuildContext context, WidgetRef ref) {
    ref.read(plrStorageProvider.notifier).select(
      onStorageInfosRetrieved: (infos) => infos.lastOrNull,
      onNeedPassphrase: (storage, lastResult) =>
        _onNeedPassphrase(context, ref, storage, lastResult),
    );
  }

  void _create(BuildContext context, WidgetRef ref) {
    ref.read(plrStorageProvider.notifier).create(
      storageTypeOf("googleDrive"),
      onFoundPlrFolderCadidates: (candidates) =>
        simpleFoundPlrFolderCandidatesHandler(context, candidates),
      onNeedInitialPassphrase:
        () => simpleNeedInitialPassphraseHandler(context),
      onNeedPassphrase: (storage, lastResult) =>
        _onNeedPassphrase(context, ref, storage, lastResult),
      onRestoreProgress: (bytesLoaded, bytesTotal) =>
        simpleDatabaseRestoreProgressHandler(
          context, ref, bytesLoaded, bytesTotal,
        ),
    );
  }

  Future<dynamic> _onNeedPassphrase(
    BuildContext context,
    WidgetRef ref,
    Storage storage,
    dynamic lastResult,
  ) async {
    var r = simpleNeedPassphraseHandler(context, ref, storage, lastResult);
    if (r is PlrStorageRemoveOrder) {
      _cleanupPassphraseChanged(context, ref, storage);
    }
    return r;
  }

  Future<void> _showFileSystemUpgradeErrroDialog(
    BuildContext context,
    Object error,
    StackTrace? stackTrace,
  ) => showConfirmDialog(
    context,
    "An error occurred while upgrading the file system.".i18n,
    "A stable network connection is required to continue logging in. Please try again in a location with a good communication environment.\n\nDetails: %s".i18n.fill([ error.toString() ]),
    showCancelButton: false,
    okButtonLabel: MaterialLocalizations.of(context).continueButtonLabel,
  );

  Future<void> _cleanupStorages(
    BuildContext context,
    WidgetRef ref, [
      Storage? except,
  ]) async {
    final storageController = ref.read(plrStorageProvider.notifier);

    try {
      for (var i in await registeredStorageInfos) {
        if (i.id != except?.id) await storageController.removeByInfo(i);
      }
    } catch (e, s) {
      print("$e\n$s");
    }
  }

  Future<void> _nextPage(
    BuildContext context,
    Account account,
    bool isSignIn,
  ) async {
    /// 名前と画像が公開状態の場合MainPageに遷移
    /// そうでない場合ProfilePageに遷移
    var isDiscloseSpecifiedProfileItems = (
      await runWithProgress(
        context, () => _chkDiscloseSpecifiedProfileItems(account.publicRoot),
      )
    )!;

    Navigator.pushAndRemoveUntil(
      context,
      CupertinoPageRoute(
        builder: (_) => isDiscloseSpecifiedProfileItems
          ? MainPage(key: ObjectKey(account), isSignIn: isSignIn)
          : ProfilePage(account, true),
      ),
      (_) => false,
    );
  }

  Future<bool> _chkDiscloseSpecifiedProfileItems(PublicRoot publicRoot) async {
    await publicRoot.syncSilently();
    try {
      await Future.wait([
          _hasProfileItem(publicRoot, nameProperty),
          _hasProfileItem(publicRoot, pictureProperty),
        ], eagerError: true,
      );
    } on bool catch (_) {
      return false;
    }
    return true;
  }

  Future<void> _hasProfileItem(ProfileContainer container, String name) async {
    try {
      await Future.wait(
        container.profileItemsOf(name).map((i) async {
            await i.syncSilently();
            if (!i.isDeleted) {
              throw true;
            }
        }).toList(), eagerError: true,
      );
    } on bool catch (_) {
      return;
    }
    throw false;
  }

  Future<void> _cleanupPassphraseChanged(
    BuildContext context,
    WidgetRef ref,
    Storage storage,
  ) async {
    ref.read(systemNotificationQueueProvider).clearPassphraseChanged(storage);
    await dismissPassphraseChangedNotification(storage);
  }

  Future<bool> _showSignInDialog(
    BuildContext context,
  ) async => await showDialog(
    context: context,
    builder: (context) {
      return CustomDialog(context);
    },
  ) ?? false;
}

abstract class _DialogBase extends StatelessWidget {
  final BuildContext myContext;
  _DialogBase(this.myContext);

  AppBar _buildAppBar() => AppBar(
    centerTitle: true,
    title: Text('Sign In'.i18n),
    leading: TextButton(
      child: const Icon(Icons.arrow_back),
      onPressed: () => Navigator.of(myContext).pop()
    ),
  );
}

class CustomDialog extends _DialogBase {
  CustomDialog(super.myContext);

  Widget build(BuildContext context) => SizedBox(
    // contentPadding: EdgeInsets.zero,
    // insetPadding: const EdgeInsets.all(32),
    child: Navigator(
      onGenerateRoute: (_) => MaterialPageRoute(
        fullscreenDialog: true,
        builder: (context) => SizedBox(
          width: 400,
          child: Scaffold(
            appBar: _buildAppBar(),
            body: Column(
              children: [
                Expanded(
                  flex: 9,
                  child: ListView(
                    children: <Widget>[
                      Container(height: 10),
                      RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children:  <TextSpan>[
                            TextSpan(
                              text: 'Please allow access \n to your Google account.'.i18n,
                              style: const TextStyle(
                                fontSize: 15, color: Colors.black54,
                              ),
                            ),
                          ],
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: bottomPadding[8],
                        child: SvgPicture.asset('assets/tutorial_img1.svg'),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal:10, vertical: 40,
                        ),
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                text: "In order to use your personal information registered with Google, please grant the PLR library embedded in this app to access your Google account when you first log in.".i18n,
                                style: const TextStyle(
                                  fontSize: 15, color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                Expanded(
                  flex: 1, // 割合.
                  child: Container(
                    color: Colors.white,
                    // alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: SizedBox(
                        width: double.infinity,
                        // height: 50,
                        child:  ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => NextDialog(myContext),
                              ),
                            );
                          },
                          child: Text('Next'.i18n),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}

class NextDialog extends _DialogBase {
  NextDialog(super.myContext);

  Widget build(BuildContext context) => SizedBox(
    width: 400,
    child: Scaffold(
      appBar: _buildAppBar(),
      body: Column(
        children:[
          Expanded(
            flex: 9, // 割合.
            child: ListView(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(height: 10),
                RichText(
                  text: TextSpan(
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Please check all items on the following screen.'.i18n,
                        style: const TextStyle(
                          fontSize: 15, color: Colors.black54,
                        ),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                SvgPicture.asset('assets/tutorial_img2.svg'),
                Padding(
                  padding: horizontalPadding[4],
                  child:
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children:  <TextSpan>[
                        TextSpan(
                          text: "Others (including us) cannot access your personal information, unless you have explicitly permitted it.".i18n,
                          style: const TextStyle(
                            fontSize: 15, color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: allPadding[2],
                  child: _agreementTextOf(context),
                ),
              ],
            ),
          ),

          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              // alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: SizedBox(
                  width: double.infinity,
                  // height: 50,
                  child:  InkWell(
                    child:FittedBox(
                      fit: BoxFit.contain,
                      child: SvgPicture.asset(
                        'assets/google-sign-btn.svg',
                        // width: double.infinity,
                      ),
                    ),
                    onTap: () => Navigator.pop(myContext, true),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );

  RichText _agreementTextOf(BuildContext context) => RichText(
    textAlign: TextAlign.center,
    text: TextSpan(
      style: const TextStyle(color: Colors.grey),
      children: [
        TextSpan(text: "You will agree to ".i18n),
        _linkTextOf(context, "the Terms of use", termsOfUseLink),
        TextSpan(text: " and ".i18n),
        _linkTextOf(context, "the Privacy policy", privacyPolicyLink),
        TextSpan(text: ' by tapping the "Sign in with Google" below.'.i18n),
        TextSpan(
          text: '\nApp’s use and transfer of information received from Google APIs to any other app will adhere to '.i18n,
        ),
        _linkTextOf(context, "Google API Services User Data Policy",
          GoogleAPIServicesUserDataPolicyLink),
        TextSpan(text: ', including the Limited Use requirements.'.i18n),
      ],
    ),
  );

  TextSpan _linkTextOf(BuildContext context, String text, Uri link) {
    text = text.i18n;

    return TextSpan(
      text: text,
      style: const TextStyle(
        color: Color(0xff0d213f),
        fontWeight: FontWeight.bold,
      ),
      recognizer: TapGestureRecognizer()
        ..onTap = () => openWebViewPage(
          context, text.i18n, link,
        ),
    );
  }
}
