import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") + {
    "en_us": "You will agree to ",
    "ja_jp": "下の「Sign in with Google」をタップすると"
  } +
  {
    "en_us": "the Terms of use",
    "ja_jp": "利用規約",
  } +
  {
    "en_us": " and ",
    "ja_jp": "と",
  } +
  {
    "en_us": "the Privacy policy",
    "ja_jp": "プライバシーポリシー",
  } +
  {
    "en_us": ' by tapping the "Sign in with Google" below.',
    "ja_jp": "に同意したことになります。",
  } +
  {
    "en_us": '\nApp’s use and transfer of information received from Google APIs to any other app will adhere to ',
    "ja_jp": "\nGoogle APIから受け取った情報の使用および他のアプリへの転送は、使用制限の要件を含め、",
  } +
  {
    "en_us": 'Google API Services User Data Policy',
    "ja_jp": "Google API Services User Data Policy",
  } +
  {
    "en_us": ', including the Limited Use requirements.',
    "ja_jp": "に準拠します。",
  } +
  {
    "en_us": "Failed to remove account.",
    "ja_jp": "アカウントの削除に失敗しました。",
  } +
  {
    "en_us": "In order to use your personal information registered with Google, please grant the PLR library embedded in this app to access your Google account when you first log in.",
    "ja_jp": "Googleに登録したあなたの個人情報をこのアプリでお使いいただくため、このアプリに組み込まれたPLRライブラリに最初のログインの際にGoogleのアカウントへのアクセスを許可して下さい。",
  } +
  {
    "en_us": "Others (including us) cannot access your personal information, unless you have explicitly permitted it.",
    "ja_jp": "あなた以外の者(当社を含む)がこのアプリであなたの個人情報にアクセスできるのは、アクセスする旨をあなたにお知らせしてあなたがそのアクセスのための明示的な操作を行なった場合だけです。",
  } +
  {
    "en_us": "Sign In",
    "ja_jp": "サインイン",
  } +
  {
    "en_us": "Important",
    "ja_jp": "重要",
  } +
  {
    "en_us": "Next",
    "ja_jp": "次へ",
  } +
  {
    "en_us": "An error occurred while upgrading the file system.",
    "ja_jp": "ファイルシステムのバージョンアップ中にエラーが発生しました",
  } +
  {
    "en_us": "A stable network connection is required to continue logging in. Please try again in a location with a good communication environment.\n\nDetails: %s",
    "ja_jp": "ログインを継続するためには、安定したネットワーク接続が必要です。通信環境の良い場所で、再度お試しください。\n\n詳細: %s",
  } +
  {
    "en_us": "Please allow access \n to your Google account.",
    "ja_jp": "Googleアカウントへの\nアクセスを許可してください",
  } +
  {
    "en_us": "Please check all items on the following screen.",
    "ja_jp": "下記のような画面で\n全ての項目にチェックしてください",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
