import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:plr_ui/plr_ui.dart';

void openWebViewPage(
  BuildContext context,
  String title,
  Uri initialUrl,
) => isMobile ? Navigator.push(
  context, CupertinoPageRoute(builder: (_) => WebViewPage(title, initialUrl)),
) : launchUrl(initialUrl);

class _WebViewStateNotifier with ChangeNotifier {
  WebViewController controller;
  _WebViewStateNotifier(this.controller);

  bool canGoBack = false, canGoForward = false;

  Future<void> update() async {
    canGoBack = await controller.canGoBack();
    canGoForward = await controller.canGoForward();
    notifyListeners();
  }
}

class WebViewPage extends StatelessWidget {
  final String title;
  final Uri initialUrl;
  final bool overrideAppBarLeading;
  late final WebViewController controller;
  late final _WebViewStateNotifier _notifier;
  WebViewPage(
    this.title,
    this.initialUrl, {
      super.key,
      this.overrideAppBarLeading = true,
  }) {
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onPageStarted: (_) => _notifier.update(),
          onNavigationRequest: (request) {
            var url = request.url.toLowerCase();
            if (
              url.startsWith("mailto:") || url.startsWith("tel:") ||
              url.endsWith(".pdf")
            ) {
              try {
                launchUrl(Uri.parse(request.url));
              } on FormatException {}
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        )
      )
      ..loadRequest(initialUrl);

    _notifier = _WebViewStateNotifier(controller);
  }

  @override
  Widget build(BuildContext context) => PopScope(
    canPop: false,
    onPopInvokedWithResult: (didPop, _) async {
      if (didPop) return;

      if (await controller.canGoBack()) {
        controller.goBack();
        return;
      }
      Navigator.pop(context);
    },
    child: SafeArea(
      child: ChangeNotifierProvider<_WebViewStateNotifier>.value(
        value: _notifier,
        child: Scaffold(
          appBar: AppBar(
            leading: overrideAppBarLeading ? IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ) : null,
            title: Text(title),
            actions: [
              Consumer<_WebViewStateNotifier>(
                builder: (context, value, _) => IconButton(
                  icon: const Icon(Icons.arrow_back),
                  onPressed: value.canGoBack ?
                    () => controller.goBack() : null,
                ),
              ),
              Consumer<_WebViewStateNotifier>(
                builder: (context, value, _) => IconButton(
                  icon: const Icon(Icons.arrow_forward),
                  onPressed: value.canGoForward ?
                    () => controller.goForward() : null,
                ),
              ),
              Consumer<_WebViewStateNotifier>(
                builder: (context, value, _) => IconButton(
                  icon: const Icon(Icons.refresh),
                  onPressed: () => controller.reload(),
                ),
              ),
            ],
          ),
          body: WebViewWidget(
            controller: controller,
          ),
        ),
      ),
    ),
  );
}
