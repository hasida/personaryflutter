import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget.dart' show StatelessConsumerPlrWidget;
import 'channel_detail/channel_detail.dart';
import 'channel_page.i18n.dart';

class ChannelPage extends StatelessConsumerPlrWidget {
  final Channel channel;
  final bool? consentRequested;

  const ChannelPage(
    this.channel, {
      this.consentRequested,
  });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: Scaffold(
      appBar: AppBar(
        title: Text('Channel detail'.i18n),
      ),
      body: ChannelDetail(
        account, channel, consentRequested: consentRequested,
      ),
    ),
  );
}

