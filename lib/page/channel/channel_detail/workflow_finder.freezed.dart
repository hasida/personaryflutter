// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'workflow_finder.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$WorkflowFinderState {
  Workflow? get workflow => throw _privateConstructorUsedError;
  bool get isMyTurn => throw _privateConstructorUsedError;
}

/// @nodoc

class _$WorkflowFinderStateImpl extends _WorkflowFinderState {
  const _$WorkflowFinderStateImpl({this.workflow, this.isMyTurn = false})
      : super._();

  @override
  final Workflow? workflow;
  @override
  @JsonKey()
  final bool isMyTurn;

  @override
  String toString() {
    return 'WorkflowFinderState(workflow: $workflow, isMyTurn: $isMyTurn)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WorkflowFinderStateImpl &&
            const DeepCollectionEquality().equals(other.workflow, workflow) &&
            (identical(other.isMyTurn, isMyTurn) ||
                other.isMyTurn == isMyTurn));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(workflow), isMyTurn);
}

abstract class _WorkflowFinderState extends WorkflowFinderState {
  const factory _WorkflowFinderState(
      {final Workflow? workflow,
      final bool isMyTurn}) = _$WorkflowFinderStateImpl;
  const _WorkflowFinderState._() : super._();

  @override
  Workflow? get workflow;
  @override
  bool get isMyTurn;
}
