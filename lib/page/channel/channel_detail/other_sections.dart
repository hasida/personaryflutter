part of 'channel_detail.dart';

abstract class _OtherSections extends _DisclosedToSections {
  /*
  Widget _buildImportsSection(
    BuildContext context,
    ChannelState? channelValue,
  ) {
    final title = "Imports".i18n;

    List<Widget> buildButtons(List<Channel> items) => [
      if (!readOnly) InlineAddButton(
        heroTag: "hero_add_imports", tooltip: "Add imports".i18n,
        onPressed: () async {
          var currentChannelIds = (
            items.map((c) => c.id).toSet()..add(channel.id)
          ).nonNulls;

          var channels = await _showChannelNotificationSelectDialog(
              context, title: title, excludeIds: currentChannelIds,
          );
          if (channels == null) return;

          for (final c in channels) {
            channel.addImport(c);
          }
          _channelSyncController.refresh();
        },
      ),
    ];

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => ImportChannelList<ChannelSyncProvider, ChannelSync, ChannelState>(
      key: const ValueKey("import_list"),
      shrinkWrap: shrinkWrap,
      provider: _channelSyncProvider,
      container: channel,
      cellConfig: importChannelCellConfigWith(
        context, ref, _channelSyncController,
      ),
    );

    return buildLimitedListSection(
      context, ref, provider: _channelSyncProvider, readOnly: readOnly,
      title: title, showAllButtonTooltip: "Show all imports".i18n,
      itemsGetter: defaultImportChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }
  */

  Widget _buildDataSettingsSection(
    BuildContext context,
    ChannelState? channelValue,
  ) {
    final title = "Disclosed channel settings".i18n;

    List<Widget> buildButtons(List<Channel> items) => [
      if (!readOnly) InlineAddButton(
        heroTag: "hero_add_channel_settings",
        tooltip: "Disclose channel settings".i18n,
        onPressed: () async {
          final dataSettings = await showDialog<List<DataSetting>>(
            context: context,
            builder: (_) => p.ChangeNotifierProvider<DataSettingSelectNotifier>(
              create: (_) => DataSettingSelectNotifier(
                root, isMultiSelection: true,
                notificationRegistry: notificationRegistry,
                excludeList: channelValue?.dataSettings.map((e) => e.id)
                  ?? const [],
              )..update(),
              child: DataSettingSelectDialog(
                title: "Disclose channel settings".i18n,
                root: root,
              ),
            ),
          );
          if (dataSettings == null) return;

          for (var ds in dataSettings) {
            channel.addDataSetting(ds);
          }
          _channelSyncController.refresh();
        },
      ),
    ];

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => DataSettingList<ChannelSyncProvider, ChannelSync, ChannelState>(
      key: const ValueKey("data_setting_list"),
      shrinkWrap: shrinkWrap,
      provider: _channelSyncProvider,
      container: channel,
      cellConfig: dataSettingCellConfigWith(
        context, ref, _channelSyncController,
      ),
    );

    return buildLimitedListSection(
      context, ref, provider: _channelSyncProvider, readOnly: readOnly,
      title: title, showAllButtonTooltip: "Show all channel settings".i18n,
      itemsGetter: defaultImportChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Widget _buildSchemataSection(
    BuildContext context,
    ChannelState? channelValue,
  ) {
    final title = "Disclosed schemata".i18n;

    List<Widget> buildButtons(List<Channel> items) => [
      if (!readOnly) InlineAddButton(
        heroTag: "hero_add_schemata", tooltip: "Disclose schemata".i18n,
        onPressed: () async {
          var schemata = await showDialog<List<Schema>>(
            context: context,
            builder: (ctx) => SchemaSelectDialog(
              title: "Disclose schemata".i18n,
              root: root,
              isMultiSelection: true,
              excludeList: channel.schemata,
            ),
          );
          if (schemata == null) return;

          for (final s in schemata) {
            channel.addSchema(s);
          }
          _channelSyncController.refresh();
        },
      ),
    ];

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => SchemaList<ChannelSyncProvider, ChannelSync, ChannelState>(
      key: const ValueKey("schema_list"),
      shrinkWrap: shrinkWrap,
      provider: _channelSyncProvider,
      container: channel,
      cellConfig: schemaCellConfigWith(
        context, ref, _channelSyncController,
      ),
    );

    return buildLimitedListSection(
      context, ref, provider: _channelSyncProvider, readOnly: readOnly,
      title: title, showAllButtonTooltip: "Show all schemata".i18n,
      itemsGetter: defaultImportChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Widget _buildDataSubjectsSection(
    BuildContext context,
    ChannelState? channelValue,
  ) {
    final title = "Data subjects".i18n;

    List<Widget> buildButtons(List<HasProfile> items) => [
      if (!readOnly) InlineAddButton(
        heroTag: "hero_add_data_subjects", tooltip: "Add data subjects".i18n,
        onPressed: () async {
          final users = await _showSelectDisclosedToUsersDialog(context);
          if (users == null) return;

          for (var u in users) {
            channel.addDataSubjectOf(u);
          }
          _channelSyncController.refresh();
        },
      ),
    ];

    final allUnconsentedUsers =
      channelValue?.realDisclosedToUsers.where(
        (u) => !u.isConsented,
      ).toList() ?? const [];
    final allUnconsentedChannels =
      channelValue?.realDisclosedToChannels.where(
        (c) => !c.isConsented,
      ).toList() ?? const [];

    final contentInfo = <
      HasProfile, (bool, List<DisclosedToUser>, List<DisclosedToChannel>, bool)
    >{};

    (bool, List<DisclosedToUser>, List<DisclosedToChannel>, bool) consentInfoOf(
      HasProfile item,
    ) {
      if (contentInfo[item] != null) return contentInfo[item]!;

      final plrId = (item as HasPlrId).plrId;
      final isMe = item is Channel;

      bool isNotConsented(DisclosedTo d2) {
        if (isMe) return !d2.isOwnerConsented;
        return !d2.hasConsentedUserOf(plrId);
      }
      final unconsentedUsers =
        allUnconsentedUsers.where(isNotConsented).toList();
      final unconsentedChannels =
        allUnconsentedChannels.where(isNotConsented).toList();

      return (
        isMe, unconsentedUsers, unconsentedChannels,
        unconsentedUsers.isNotEmpty || unconsentedChannels.isNotEmpty,
      );
    }

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => DataSubjectList<
      ChannelSyncProvider, ChannelSync, ChannelState
    >(
      key: const ValueKey("data_subject_list"),
      shrinkWrap: shrinkWrap,
      provider: _channelSyncProvider,
      container: channel,
      cellConfig: dataSubjectCellConfigWith(
        context, ref, _channelSyncController,
        useDefaultButtons: false,
        buttonBuilders: [
          (context, _, _1, user, _3, state, _5, _6) {
            final (
              isMe, unconsentedUsers, unconsentedChannels, hasUnconsented
            ) = consentInfoOf(user);

            if (isMe || !hasUnconsented) return emptyWidget;

            Future<bool?> _hasRequest() async {
              var rr = await requestReceptionOf(user as PublicRoot);
              if (rr == null) return null;
              await rr.syncSilently();
              return rr.hasChannelDisclosureConsentRequestOf(channel);
            }

            return HookBuilder(
              builder: (context) {
                final hasRequest = useFuture(useMemoized(
                    () => _hasRequest(), [
                      isMe, unconsentedUsers, unconsentedChannels,
                      hasUnconsented
                    ],
                )).data;

                return switch (hasRequest) {
                  null => CompactIconButton(
                    icon: const Icon(Icons.send_outlined),
                    tooltip: "Retrieving consent request state...".i18n,
                  ),
                  true => CompactIconButton(
                    icon: const Icon(Icons.send),
                    tooltip: "Consent request was already sent.".i18n,
                  ),
                  false => CompactIconButton(
                    icon: const Icon(Icons.send),
                    tooltip: "Send consent request.".i18n,
                    onPressed: () async {
                      if (
                        !await showConfirmDialog(
                          context, "Send consent request".i18n,
                          "Send disclosure consent request"
                          " for this channel to `%s', are you ok?".i18n.fill(
                            [ state.value?.name ?? user.id ?? "" ],
                          ),
                        )
                      ) return;

                      _requestChannelDisclosureConsent(user as DisclosedToUser);
                    },
                  ),
                };
              },
            );
          },
          (context, _, _1, user, _3, _4, _5, _6) {
            final (
              _, unconsentedUsers, unconsentedChannels, hasUnconsented
            ) = consentInfoOf(user);

            if (hasUnconsented) return CompactIconButton(
              icon: const Icon(Icons.checklist_rtl),
              tooltip: "Show the list of un-consented disclosed to.".i18n,
              onPressed: () => (
                (isOwner && (user is Channel)) ||
                ((user is DisclosedToUser) && (user.plrId == root.plrId))
              )
              ? _showConsentDialog(false)
              : _showUnconsentedDialog(
                context, unconsentedUsers, unconsentedChannels,
              ),
            );
            else return CompactIconButton(
              icon: const Icon(Icons.check_circle),
              color: Colors.green,
              tooltip: "All disclosures were consented.".i18n,
            );
          },
        ],
      ),
    );

    return buildLimitedListSection(
      context, ref, provider: _channelSyncProvider, readOnly: readOnly,
      title: title, showAllButtonTooltip: "Show all data subjects".i18n,
      itemsGetter: defaultDataSubjectListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }
}
