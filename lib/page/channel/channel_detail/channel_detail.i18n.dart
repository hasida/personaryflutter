import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText('en_us') +
  retrievingTranslation +
  unknownTranslation +
  meTranslation +
  deepLinkTooltipTranslation +
  deepLinkCopiedTranslation +
  discardRequestTranslation +
  discardRequestConfirmTranslation +
  {
    "en_us": "Owner",
    "ja_jp": "所有者",
  } +
  {
    "en_us": "Profile",
    "ja_jp": "プロフィール",
  } +
  {
    "en_us": "Disclose profile",
    "ja_jp": "プロフィールの開示",
  } +
  {
    "en_us": "No disclosed profile",
    "ja_jp": "開示されているプロフィールはありません",
  } +
  {
    "en_us": "Set up mail notification of timeline update.",
    "ja_jp": "タイムライン更新のメール通知を設定します。",
  } +
  {
    "en_us": "You'll send mail to notify other users of timeline update.",
    "ja_jp": "タイムラインの更新を他の利用者にメールで通知します。",
  } +
  {
    "en_us": "You'll not send mail to notify other users of timelineupdate.",
    "ja_jp": "タイムラインの更新をメールで通知しません。",
  } +
  {
    "en_us": "Add channels",
    "ja_jp": "チャネルの追加",
  } +
  {
    "en_us": "Show all channels",
    "ja_jp": "全てのチャネルを表示します",
  } +
  {
    "en_us": "Disclosed channels",
    "ja_jp": "開示元チャネル",
  } +
  {
    "en_us": "Add disclosed channels",
    "ja_jp": "開示元チャネルの追加",
  } +
  {
    "en_us": "Show all disclosed channels",
    "ja_jp": "開示元チャネルを全て表示します",
  } +
  {
    "en_us": "Disclose channels",
    "ja_jp": "チャネルの開示",
  } +
  {
    "en_us": "Disclosed to users",
    "ja_jp": "開示先利用者",
  } +
  {
    "en_us": "Add disclosed to users",
    "ja_jp": "開示先利用者の追加",
  } +
  {
    "en_us": "Show all disclosed to users",
    "ja_jp": "全ての開示先利用者を表示します",
  } +
  {
    "en_us": "Disclosed to channels",
    "ja_jp": "開示先チャネル",
  } +
  {
    "en_us": "Add disclosed to channels",
    "ja_jp": "開示先チャネルの追加",
  } +
  {
    "en_us": "Show all disclosed to channels",
    "ja_jp": "全ての開示先チャネルを表示します",
  } +
  {
    "en_us": "All data subjects were consented.",
    "ja_jp": "全てのデータ主体が同意しています。",
  } +
  {
    "en_us": "The dislosure to the data subject is always available.",
    "ja_jp": "データ主体への開示は常に有効です。",
  } +
  {
    "en_us": "Some data subjects were not consented.",
    "ja_jp": "いずれかのデータ主体が同意していません。",
  } +
  {
    "en_us": "Show the list of un-consented data subjects.",
    "ja_jp": "未同意のデータ主体リストを表示します。",
  } +
  {
    "en_us": "Un-consented data subjects",
    "ja_jp": "未同意のデータ主体",
  } +
  {
    "en_us": "Consent this disclosure.",
    "ja_jp": "この開示に同意します。",
  } +
  {
    "en_us": "Are you sure to consent to this disclosure?",
    "ja_jp": "この開示に同意してよろしいですか？",
  } +
  {
    "en_us": "Need to register friend",
    "ja_jp": "友達の登録が必要です",
  } +
  {
    "en_us": "To disclose this channel, you need to friend the following users:\n\n%s",
    "ja_jp": "このチャネルを開示するために、下記のユーザを友達にする必要があります:\n\n%s",
  } +
  {
    "en_us": "Register",
    "ja_jp": "登録",
  } +
  {
    "en_us": "Un-consent this disclosure.",
    "ja_jp": "この開示への同意を取り消します。",
  } +
  {
    "en_us": "Are you sure to un-consent to this disclosure?",
    "ja_jp": "この開示への同意を取り消してよろしいですか？",
  } +
  {
    "en_us": "Remove the disclosure to this user.",
    "ja_jp": "この利用者への開示を解除します。",
  } +
  {
    "en_us": "Are you sure to delete the disclosure to this user?",
    "ja_jp": "以下の利用者への開示を解除してよろしいですか？",
  } +
  {
    "en_us": "Disclosed to friends",
    "ja_jp": "開示先の友達",
  } +
  {
    "en_us": "No friends to disclose.",
    "ja_jp": "開示先の友達がいません",
  } +
  {
    "en_us": "Disclosed to channels",
    "ja_jp": "開示先チャネル",
  } +
  {
    "en_us": "Add disclosed to channels",
    "ja_jp": "開示先チャネルの追加",
  } +
  {
    "en_us": "No channels to disclose.",
    "ja_jp": "開示先のチャネルがありません",
  } +
  {
    "en_us": "Imports",
    "ja_jp": "インポート",
  } +
  {
    "en_us": "Add imports",
    "ja_jp": "インポートの追加",
  } +
  {
    "en_us": "Show all imports",
    "ja_jp": "全てのインポートを表示します",
  } +
  {
    "en_us": "Disclosed channel settings",
    "ja_jp": "開示されているチャネル設定",
  } +
  {
    "en_us": "Disclose channel settings",
    "ja_jp": "チャネル設定の開示",
  } +
  {
    "en_us": "Show all channel settings",
    "ja_jp": "全てのチャネル設定を表示します",
  } +
  {
    "en_us": "Disclosed schemata",
    "ja_jp": "開示されているスキーマ",
  } +
  {
    "en_us": "Disclose schemata",
    "ja_jp": "スキーマの開示",
  } +
  {
    "en_us": "Show all schemata",
    "ja_jp": "全てのスキーマを表示します",
  } +
  {
    "en_us": "Data subjects",
    "ja_jp": "データ主体",
  } +
  {
    "en_us": "Add data subjects",
    "ja_jp": "データ主体の追加",
  } +
  {
    "en_us": "Show all data subjects",
    "ja_jp": "全てのデータ主体を表示します",
  } +
  {
    "en_us": "No data subjects to add.",
    "ja_jp": "追加するデータ主体がありません",
  } +
  {
    "en_us": "Retrieving consent request state...",
    "ja_jp": "同意リクエストの状態を取得中...",
  } +
  {
    "en_us": "Send consent request.",
    "ja_jp": "同意リクエストを送信します。",
  } +
  {
    "en_us": "Consent request was already sent.",
    "ja_jp": "同意リクエストは送信済みです。",
  } +
  {
    "en_us": "Show the list of un-consented disclosed to.",
    "ja_jp": "未同意の開示先リストを表示します。",
  } +
  {
    "en_us": "All disclosures were consented.",
    "ja_jp": "全ての開示に同意されています。",
  } +
  {
    "en_us": "Un-consented disclosed to",
    "ja_jp": "未同意の開示先",
  } +
  {
    "en_us": "No un-consented disclosed to.",
    "ja_jp": "未同意の開示先はありません",
  } +
  {
    "en_us": "Later",
    "ja_jp": "後で",
  } +
  {
    "en_us": "Consent",
    "ja_jp": "同意",
  } +
  {
    "en_us": "Consent all selected disclosures collectively. Are you ok?",
    "ja_jp": "選択された開示先にまとめて同意します。よろしいですか?",
  } +
  {
    "en_us": "Send consent request",
    "ja_jp": "同意リクエストの送信",
  } +
  {
    "en_us": "Send disclosure consent request for this channel to `%s\", are you ok?",
    "ja_jp": "このチャネルの開示同意リクエストを「%s」に送信します。よろしいですか?",
  } +
  {
    "en_us": "Failed to retrieve the request reception node of the data subject.",
    "ja_jp": "データ主体のリクエスト受付ノードが取得できませんでした。",
  } +
  {
    "en_us": "Remove data subject.",
    "ja_jp": "データ主体を削除します。",
  } +
  {
    "en_us": "Reference",
    "ja_jp": "開示元",
  } +
  {
    "en_us": "Disclose to",
    "ja_jp": "開示先",
  } +
  {
    "en_us": "Are you sure you want to undisclose this profile?",
    "ja_jp": "以下のプロフィールの開示を解除してよろしいですか？",
  } +
  {
    "en_us": "saving data...",
    "ja_jp": "データの保存中です...",
  } +
  {
    "en_us": "Cooperation string copied to clipboard.",
    "ja_jp": "連携文字列をクリップボードにコピーしました。",
  } +
  {
    "en_us": "Show channel disclosure consent dialog.",
    "ja_jp": "チャネル開示同意ダイアログを開きます。",
  } +
  {
    "en_us": "Create channel",
    "ja_jp": "チャネルを作成",
  } +
  {
    "en_us": "Add existing channel",
    "ja_jp": "既存のチャネルを追加",
  } +
  {
    "en_us": "Delete from list",
    "ja_jp": "リストから削除",
  } +
  {
    "en_us": "Are you sure to delete this channel from the list?",
    "ja_jp": "このチャネルをリストから削除してよろしいですか？",
  } +
  {
    "en_us": "Successfully deleted the channel from list.",
    "ja_jp": "チャネルのリスト削除に成功しました。",
  } +
  {
    "en_us": "Failed to delete the channel from list.",
    "ja_jp": "チャネルのリスト削除に失敗しました。",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
