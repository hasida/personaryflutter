part of 'channel_detail.dart';

enum AddChannelPopup {
  create, existing;

  String get label {
    switch (this) {
      case AddChannelPopup.create: return "Create channel".i18n;
      case AddChannelPopup.existing: return "Add existing channel".i18n;
    }
  }
}

final _bgPair = dismissibleBackgroundPairWith(
  Colors.red, const Icon(Icons.delete, color: Colors.white),
  Text(
    "Delete from list".i18n, style: const TextStyle(color: Colors.white),
  ),
);

abstract class _ChannelSctions extends _Header {
  Widget _buildChannelSections(
    BuildContext context,
    ChannelState? channelValue,
    DataSettingState? channelDataSettingValue,
    Set<DisplaySection>? displaySections,
  ) => HookConsumer(
    builder: (context, ref, _) {
      final _channelStates = channelValue?.channels.map(
        (c) {
          return ref.watch(channelSyncProvider(c)).value;
        }
      ).toList() ?? const [];

      final subDataSettings = channelDataSettingValue?.subDataSettings
        ?? const [];

      final subChannelMap = useState(
        Map<String, List<String>?>.fromIterable(
          subDataSettings, key: (sds) => sds.id, value: (_) => null,
        ),
      );

      if (
        _channelStates.any((cs) => cs == null) ||
        subChannelMap.value.values.any((l) => l == null)
      ) return const Center(
        child: CircularProgressIndicator(),
      );

      return Column(
        children: [
          ...subDataSettings.map(
            (sds) => _buildSubChannelsSection(
              context, ref, channelValue, sds,
              _channelStates.cast<ChannelState>(), subChannelMap,
            ),
          ),

          if (channelDataSettingValue?.hasSubDataSettings == true)
            const Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Divider(
                color: Colors.grey,
              ),
            ),

          _buildDisclosedChannelsSection(
            context, ref, channelValue, subChannelMap,
          ),
        ],
      );
  });

  Widget _buildSubChannelsSection(
    BuildContext context,
    WidgetRef ref,
    ChannelState? channelValue,
    SubDataSetting subDataSetting,
    List<ChannelState> channelStates,
    ValueNotifier<Map<String, List<String>?>> subChannelMap,
  ) {
    final List<String> subChannelIds; {
      final sdsId = subDataSetting.id;
      subChannelIds = channelStates.where(
        (cs) => cs.channelDataSetting?.id == sdsId,
      ).map((cs) => cs.channel.id).nonNulls.toList()..sort();

      if (!listEquals(subChannelMap.value[sdsId], subChannelIds)) {
        subChannelMap.value[sdsId] = subChannelIds;
        if (!subChannelMap.value.values.any((l) => l == null)) {
          subChannelMap.value = Map.of(subChannelMap.value);
        }
      }
    }

    return Consumer(
      key: ValueKey("sub_channel_consumer_${subDataSetting.id}"),
      builder: (context, ref, _) {
        List<Channel> getItems(ChannelState value) => value.channels.where(
          (c) => subChannelIds.contains(c.id),
        ).toList();

        final _subDataSettingSyncProvider =
          subDataSettingSyncProvider(subDataSetting);
        final subDataSettingState = ref.watch(_subDataSettingSyncProvider);
        final subDataSettingValue = subDataSettingState.value;

        ref.listen(_subDataSettingSyncProvider, (_, next) async {
            switch (next) {
              case AsyncData(:final value, isLoading: false): {
                if (
                  await _processSubDataSetting(value, getItems(channelValue!))
                ) {
                  await _checkOnlyOneMyChannelInSection();
                  _channelSyncController.refresh();
                }
              }
            }
        });

        final title = switch (subDataSettingState) {
          AsyncValue(:final value) when (
            value?.name != null
          )=> value!.name!.defaultValue ?? "",
          AsyncLoading() => "Retrieving...".i18n,
          _ => "Unknown".i18n,
        };

        List<Widget> buildButtons(List<Channel> items) {
          if (
            (subDataSettingValue == null) || (
              !subDataSettingValue.isAddExistingChannelToSection &&
              !subDataSetting.isChannelManualGeneration
            )
          ) return [];

          final popupItems = [
            if (subDataSettingValue.isChannelManualGeneration)
            AddChannelPopup.create,
            if (subDataSetting.isAddExistingChannelToSection)
            AddChannelPopup.existing,
          ];
          if (popupItems.isEmpty) return [];

          if (popupItems.length == 1) return [
            InlineAddButton(
              heroTag: "hero_add_sub_channel" + subDataSetting.id,
              tooltip: "Add channels".i18n,
              onPressed: () => _onAddButtonPressed(
                context, subDataSetting, popupItems.first,
              ),
            ),
          ];
          return [
            PopupMenuButton(
              child: InlineAddButton(
                heroTag: "hero_add_sub_channel" + subDataSetting.id,
                tooltip: "Add channels".i18n,
              ),
              itemBuilder: (_) => popupItems.map(
                (i) => PopupMenuItem(
                  child: Text(i.label),
                ),
              ).toList(),
              onSelected: (selected) => _onAddButtonPressed(
                context, subDataSetting, selected,
              ),
            ),
          ];
        }

        Widget buildList(
          BuildContext context,
          WidgetRef ref,
          bool shrinkWrap,
        ) => ChannelList<
          Channel, ChannelSyncProvider, ChannelSync, ChannelState
        >(
          key: ValueKey("sub_channel_list_${subDataSetting.id}"),
          shrinkWrap: shrinkWrap,
          provider: _channelSyncProvider,
          container: channel,
          itemsGetter: getItems,
          itemBuilder: (
            key, _, ref, config, item, referrer, forceRefreshOnCreate, selected,
          ) => ChannelCell(
            key: key, ref: ref, config: config,
            object: item, referrer: referrer,
            forceRefreshOnCreate: forceRefreshOnCreate, selected: selected,
            subDataSettingSyncProvider: _subDataSettingSyncProvider,
          ),
          cellConfig: channelCellConfigWith(
            context, ref, _channelSyncController,
            useDefaultButtons: false,
            buttonBuilders: [
              buildWorkflowTrailing,
              ...defaultChannelButtonBuildersWith(context, ref),
            ],
            useDefaultDismissibleConfig: false,
            dismissibleConfig: channelDismissibleConfigWith(
              context, ref, _channelSyncController,
            ).copyWith(
              canDismiss: (context, ref, channel, referrer, state) => (
                (subDataSettingValue != null) && defaultChannelCanDismiss(
                  context, ref, channel, referrer, state,
                ) && !subDataSettingValue.isAutomaticDisclosureToSection && (
                  !subDataSettingValue.isChannelAutoGeneration ||
                  !isOwner || (
                    state.value
                      ?.onlyOneMyChannelInSectionTargets.isEmpty == true
                  )
                )
              ),
              confirm: (context, _1, _2, _3, _4, _5) => showConfirmDialog(
                context, "Delete channel list".i18n,
                "Are you sure to delete this channel from list?".i18n,
              ),
              background: _bgPair[0],
              secondaryBackground: _bgPair[1],
            ),
          ),
        );

        return buildLimitedListSection(
          context, ref,
           provider: _channelSyncProvider, readOnly: readOnly,
          title: title, showAllButtonTooltip: "Show all channels".i18n,
          itemsGetter: getItems,
          buttonsBuilder: buildButtons, listBuilder: buildList,
        );
      },
    );
  }

  Future<bool> _processSubDataSetting(
    SubDataSettingState value,
    List<Channel> subChannels,
  ) async {
    // 対象（親）が自分のチャネルでない場合
    if (!isOwner) {
      if (value.isAutomaticDisclosureToSection) {
        // 自動開示が有効の場合
        // (現在は区別がないが将来的には自分のチャネルと他利用者のチャネルで
        // 設定項目を分ける)
        return await _autoDiscloseSubChannels(
          value, mine: true, discloseAll: true,
        );
      }
      return false;
    }

    var result = false;
    if (value.isAutomaticDisclosureToSection) {
      // 自動開示が有効の場合
      // (現在は区別がないが将来的には自分のチャネルと他利用者のチャネルで
      // 設定項目を分ける)
      result |= await _autoDiscloseSubChannels(
        value, mine: true, discloseAll: true,
      );

      // 他利用者のチャネルはdiscloseAllに関係なく全て開示される
      result |= await _autoDiscloseSubChannels(value, mine: false);
    }

    if (value.isChannelAutoGeneration) {
      // 自動生成が有効の場合
      // セクションのチャネルから自分のチャネルを抽出
      final _subChannels = subChannels.where((c) => c.plrId == root.plrId);
      if (_subChannels.isEmpty) {
        // 開示されていない場合
        // まず既存チャネルを探して見つかれば自動開示
        final added = await _autoDiscloseSubChannels(
          value, mine: true, discloseAll: false,
        );
        if (!added) {
          // 無かった場合は自動生成
          final result = await value.subDataSetting.autoGenerate(root: root);
          final c = result?.channel;

          if (c != null) {
            await c.waitForSent();
            await channel.addChannel(c);
          }
        }
        result = true;
      }
      else if (_subChannels.length > 1) {
        // 自分のチャネルが複数開示されていた場合
        if (!value.isChannelManualGeneration) {
          // 手動生成がOFFなら
          // 空でなく壊れていない最新の1つ(すべて空なら最古のもの)を残して
          // 他は開示解除する
          var found = false;
          final futures = <Future<bool>>[];
          for (final c in _subChannels) {
            if (!found) {
              // 最古のものに到達した時点ですべて空なら確定で最古のものを残す
              if (c == _subChannels.last) break;

              if (await _checkChannelNotEmpty(c)) {
                // 空でなければ開示解除しない
                found = true;
                continue;
              }
            }
            // 空か、既により新しい空でないものがあれば開示解除
            futures.add(channel.removeChannel(c));
          }
          await Future.wait(futures);
          result = true;
        }
      }
      else {
        // 既に自分のチャネルが開示されていた場合
        // 特に何もしない
      }
    }

    if (
      !value.isAutomaticDisclosureToSection &&
      !value.isAddExistingChannelToSection
    ) {
      // 自動開示・手動開示がともにOFFの場合
      final futures = <Future>[];
      for (final c in subChannels) {
        if (c.plrId == root.plrId) {
          if (
            !value.isChannelAutoGeneration &&
            !value.isChannelManualGeneration
          ) {
            // 自動生成・手動生成がともにOFFなら自分のチャネルの開示を解除
            futures.add(channel.removeChannel(c));
          }
        }
        else {
          // フレンドのチャネルの場合
          futures.add(channel.removeChannel(c));
        }
      }
      if (futures.isNotEmpty) {
        await Future.wait(futures);
        result = true;
      }
    }
    return result;
  }

  /// 関連チャネル設定が適用されたチャネルを自動的に追加します。
  /// [mine]がtrueなら自分の、falseなら他利用者のチャネルが対象となります。
  /// [mine]がtrueで該当するチャネルが複数ある場合は
  /// [discloseAll]がtrueならすべて、falseなら最古の1つを開示します。
  /// [mine]がfalseの場合、[discloseAll]に関係なくすべてを開示します。
  Future<bool> _autoDiscloseSubChannels(
    SubDataSettingState value, {
      required bool mine,
      bool discloseAll = false,
  }) async {
    final allChannels = await getAllChennels();

    final matchedChannels = allChannels[value.id]?.where(
      (c) => c.plrId == root.plrId,
    ).where((c) => channel.channelOf(c.channelId) == null).toList();
    if (matchedChannels == null) return false;

    for (final c in matchedChannels.toList()) {
      if (!(await _checkChannelIsValid(c))) matchedChannels.remove(c);
    }
    if (matchedChannels.isEmpty) return false;

    for (final c in matchedChannels) {
      if (mine && !discloseAll && (c != matchedChannels.last)) {
        // 対象が自身のチャネルでdiscloseAllがfalseの場合、
        // チャネルが空なら次のチャネルに処理を飛ばす
        if (!(await _checkChannelNotEmpty(c))) continue;
      }
      await channel.addChannel(c);
      if (mine && !discloseAll) break;
    }
    return true;
  }

  // 自分のチャネルと自分に開示されているチャネル全てをチャネル設定ごとに
  // 振り分けて取得
  Map<String, Set<Channel>>? _allChannels;
  Future<Map<String, Set<Channel>>> getAllChennels(
  ) => "allChannels".synchronized(() async {
      if (_allChannels != null) return _allChannels!;

      // 振り分け用クラスを使用
      final distributedChannels = DistributedChannels();
      var channelNotifications =
        await notificationRegistry.listChannelNotifications();
      if (channelNotifications.isNotEmpty) {
        // 既にNotificationRegistryにデータがあればそのまま読ませる
        await distributedChannels.addNotifications(channelNotifications);
      }
      else {
        // NotificationRegistryにデータがなければ収集（レアケース）
        final channels = <Channel>{};
        final removedChannels = <Channel>{};
        final s = root.collectChannelNotifications(
          notificationRegistry: notificationRegistry,
        );
        await for (final event in s.handleError((_) {})) {
          for (final n in event.notifications) {
            Channel? ch = (await n.model)?.model;
            if (ch == null) continue;
            if (event.type == CollectChannelNotificationsEventType.Removed) {
              // 順に収集していくので、チャネルの削除も飛んでくる
              removedChannels.add(ch);
            }
            else {
              channels.add(ch);
            }
          }
        }
        channels.removeWhere(
          (a) => removedChannels.any((b) => a.channelId == b.channelId),
        );
        await distributedChannels.addChannels(channels);
      }

      // 指定時間内はキャッシュに保存
      _allChannels = distributedChannels.data;
      Future.delayed(
        const Duration(minutes: _allChannelsCacheLifetimeMinutes),
      ).then(
        (_) => "allChannels".synchronized(() => _allChannels = null)
      );
      return distributedChannels.data;
    }
  );

  Future<bool> _checkChannelNotEmpty(Channel c) async {
    final s = c.timelineItemsByCount(DateTime.now(), 1);
    await for (final e in s.handleError((_) {})) {
      if (e.isNotEmpty) return true;
    }
    return false;
  }

  Future<bool> _checkChannelIsValid(Channel c) async {
    if (!c.isLoaded) await c.syncSilently();
    return !c.isDeleted && (c.info?.id != null) && (c.name != null);
  }

  Future<void> _onAddButtonPressed(
    BuildContext context,
    SubDataSetting subDataSetting,
    AddChannelPopup selected,
  ) async {
    switch (selected) {
      case AddChannelPopup.create: {
        _showCreateChannelDialog(context, subDataSetting);
      }
      case AddChannelPopup.existing: {
        _showAddExitingChannelSelectDialog(context, subDataSetting);
      }
    }
  }

  Future<void> _showCreateChannelDialog(
    BuildContext context,
    DataSetting dataSetting,
  ) async {
    final c = await showDialog(
      context: context,
      builder: (_) => ChannelInfoDialog(
        root: root, dataSetting: dataSetting,
        notificationRegistry: notificationRegistry,
      ),
    );
    if (c == null) return;

    await runWithProgress(
      context, () async {
        await channel.addChannel(c);
        await channel.checkOnlyOneMyChannelInSection();
      },
      onError: (e, s) => showError(context, e, s),
      onFinish: (results) => _channelSyncController.refresh(),
    );
  }

  Future<void> _showAddExitingChannelSelectDialog(
    BuildContext context,
    DataSetting dataSetting,
  ) async {
    final currentChannelIds = ref.read(
      _channelSyncProvider,
    ).value?.channels.map((c) => c.id).nonNulls.toList();

    final dataSettingId = dataSetting.dataSettingId;

    final channels = (
      await showChannelNotificationSelectDialog(
        context,
        title: "Add existing channel".i18n,
        excludeIds: currentChannelIds,
        test: (n) async {
          var channel = n.channel;
          if (channel == null) {
            final r = await n.anyModel;
            if (r == null) return false;

            channel = n.channel = r.model;
            n.referrer = r.referrer;
            if (channel.plrId == channel.masterStorage.plrId) {
              n.myChannel = channel;
            }
          }
          if (channel.isLoaded) {
            return channel.channelDataSetting?.dataSettingId == dataSettingId;
          }
          await for (final _ in channel.sync().handleError((_) {})) {
            if (channel.channelDataSetting?.dataSettingId == dataSettingId) {
              return true;
            }
          }
          return false;
        },
        selectionMode: AsyncListSelectionMode.multiple,
        onError: (e, s) => showError(context, e, s),
      )
    )?.map((n) => n.channel).nonNulls;

    if (channels == null) return;

    await _addChannels(channels);
  }

  Widget _buildDisclosedChannelsSection(
    BuildContext context,
    WidgetRef ref,
    ChannelState? channelValue,
    ValueNotifier<Map<String, List<String>?>> subChannelMap,
  ) {
    final provider = channelSyncWithChannelSearchProvider(channel);
    final controller = ref.read(provider.notifier);

    final subChannelIds =
      subChannelMap.value.values.nonNulls.expand((l) => l).toSet();

    List<Channel> getItems(ChannelState value) =>
      defaultChannelListItemsGetter(value).where(
        (c) => (c.id != null) && !subChannelIds.contains(c.id),
      ).toList();

    if (channelValue != null) _checkReferences(getItems(channelValue));

    final title = "Disclosed channels".i18n;

    List<Widget> buildButtons(List<Channel> items) => [
      SearchButton(
        provider: provider,
        heroTag: "hero_search_channel", compact: true, useChannelDatabase: true,
        onPressed: () async {
          if (
            !await showSearchChannelsDialog(context, controller)
          ) return;

          await runWithProgress(context, controller.search);
        },
      ),
      InlineAddButton(
        heroTag: "hero_add_disclosed_channel",
        tooltip: "Add disclosed channels".i18n,
        onPressed: () async {
          var excludeIds = channelValue?.channels.map(
            (c) => c.id,
          ).nonNulls.toSet() ?? {}; {
            final channelId = channel.id;
             if (channelId != null) excludeIds.add(channelId);
          }

          final channels = await _showChannelNotificationSelectDialog(
            context, title: "Disclose channels".i18n, excludeIds: excludeIds,
          );
          if (channels == null) return;

          await _addChannels(channels);
      }),
    ];

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => ChannelList<
      Channel,
      ChannelSyncWithChannelSearchProvider, ChannelSyncWithChannelSearch,
      ChannelState
    >(
      key: const ValueKey("disclosed_channel_list"),
      shrinkWrap: shrinkWrap,
      provider: provider,
      container: channel,
      itemsGetter: getItems,
      cellConfig: channelCellConfigWith(context, ref, controller),
    );

    return buildLimitedListSection(
      context, ref, provider: provider, readOnly: readOnly,
      title: title, showAllButtonTooltip: "Show all disclosed channels".i18n,
      itemsGetter: getItems,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Set<String> _referencesChecked = {};

  Future<void> _checkReferences(List<Channel> channels) async {
    await Future.wait(
      channels.where(
        (c) => !_referencesChecked.contains(c),
      ).map(_checkReference),
    );
    _referencesChecked = channels.map((c) => c.id).nonNulls.toSet();
  }

  Future<bool> _checkReference(Channel channel) async {
    if (!root.isLoaded) await root.syncSilently();

    var result = false;
    for (
      final d2c
      in channel.onlyOneMyChannelInSectionTargetModels.where(
        (d2c) => d2c.plrId == root.plrId,
      )
    ) {
      final c = root.channelOf(d2c.id);

      var invalid = false;
      if (c == null) invalid = true;
      else {
        await c.syncSilently();
        if (
          (c.info == null) || c.isRemoved || (c.channelOf(channel.id) == null)
        ) invalid = true;
      }
      if (invalid) {
        result |= channel.removeOnlyOneMyChannelInSection(d2c.id);
      }
    }
    if (result) await channel.syncSilently();

    return result;
  }

  Future<void> _addChannels(
    Iterable<Channel> channels,
  ) => runWithProgress(
    context, () => Future.wait(
      channels.map(
        (c) => channel.addChannelAndRequestDisclosureConsent(c, root: root),
      ).toList(),
    ),
    onError: (e, s) => showError(context, e, s),
    onFinish: (results) => _channelSyncController.refresh(),
  );
}
