import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../../widget/layout.dart' show emptyWidget;

part 'workflow_finder.g.dart';
part 'workflow_finder.freezed.dart';

@Freezed(copyWith: false)
class WorkflowFinderState with _$WorkflowFinderState {
  const factory WorkflowFinderState({
      Workflow? workflow,
      @Default(false) bool isMyTurn,
  }) = _WorkflowFinderState;

  const WorkflowFinderState._();

  bool get found => workflow != null;

  String? get label  => workflow?.label;
  String? get text   => workflow?.text;
}

void _dumpError(dynamic e, dynamic s) {
  print("[WorkflowFinder] Error: $e");
  if (s != null) print(s);
}

@riverpod
class WorkflowFinder extends _$WorkflowFinder {
  @override
  FutureOr<WorkflowFinderState> build(
    Channel channel,
  ) async {
    final channelValue = ref.watch(channelSyncProvider(channel)).value;
    if (channelValue == null) return const WorkflowFinderState();

    final channelDataSetting = channelValue.channelDataSetting;
    final DataSettingState? channelDataSettingValue;
    if (channelDataSetting != null) {
      channelDataSettingValue = ref.watch(
        dataSettingSyncProvider(channelDataSetting),
      ).value;
    } else channelDataSettingValue = null;

    final schemataList = await _loadSchemata(
      channelValue, channelDataSettingValue,
    );
    final (workflow, isMyTurn) = await _searchWorkflow(
      channelValue, channelDataSettingValue, schemataList,
    );
    return WorkflowFinderState(workflow: workflow, isMyTurn: isMyTurn);
  }

  Future<List<Schemata>> _loadSchemata(
    ChannelState channelValue,
    DataSettingState? channelDataSettingValue,
  ) async {
    final channel = channelValue.channel;

    final hasOssFiles = <HasOssFiles>{};
    if (channelDataSettingValue != null) {
      var channelPlrId = channel.plrId;
      if (
        (channelPlrId != null) && (
          (channelPlrId == channel.masterStorage.plrId) ||
          (channelPlrId == channel.storage?.plrId)
        )
      ) {
        hasOssFiles.addAll(
          channelDataSettingValue.internalSchemata.where(
            (e) => e.type == selfSchemaClass,
          ),
        );
      }
      else {
        if (channel.isPublic) hasOssFiles.addAll(
          channelDataSettingValue.internalSchemata.where(
            (e) => e.type == publicSchemaClass,
          ),
        );
        else hasOssFiles.addAll(
          channelDataSettingValue.internalSchemata.where(
            (e) => e.type == generalSchemaClass,
          ),
        );
        hasOssFiles.addAll(
          channelDataSettingValue.schemata.where(
            (e) => e.type != selfSchemaClass,
          )
        );
      }
    }
    hasOssFiles.addAll(channelValue.schemata);

    return (
      await Future.wait(
        hasOssFiles.map((oss) async {
            var ossId = oss.id;
            if (ossId == null) return null;

            Schemata? schemata;
            await for (
              final s in schemataOf([
                  oss,
                  // チャネル設定があれば共通スキーマと合わせて読み込み
                  if (channelDataSettingValue != null)
                    channelDataSettingValue.dataSetting,
              ]).handleError(_dumpError)
            ) {
              schemata = s;
            }
            return schemata;
          },
        ),
      )
    ).nonNulls.toList();
  }

  Future<(Workflow?, bool)> _searchWorkflow(
    ChannelState channelValue,
    DataSettingState? channelDataSettingValue,
    List<Schemata> schemataList,
) async {
    final workflows = channelValue.workflows;
    if (workflows.isEmpty) return (null, false);

    workflows.sort((a, b) {
        final aBegin = a.begin;
        final bBegin = b.begin;
        if (aBegin == null) {
          if (bBegin == null) return 0;
          else return 1;
        }
        else if (bBegin == null) return -1;
        else return aBegin.compareTo(bBegin);
    });

    for (final w in workflows) {
      final propId = w.propId;
      if (propId == null) continue;

      for (final s in schemataList) {
        // 手番（入力可能属性）のワークフローアイテムがあれば優先
        final prop = s.propertyOf(propId);
        if (prop?.isInputtable == true) {
          return (w, true);
        }
      }
    }
    return (workflows.first, false);
  }
}

final Color _myTurnWorkflowBgColor = Colors.pink[100]!;
const Color _noTurnWorkflowBgColor = Color(0xFFFFE0B2);

Widget buildWorkflowTrailing(
  BuildContext context,
  WidgetRef ref,
  ChannelUpdate controller,
  Channel object,
  Entity? referrer,
  AsyncValue<ChannelUpdateState?> state,
  bool isEnabled,
  AsyncCellWrapCallback wrapCallback,
) {
  if (referrer is! Channel) return emptyWidget;

  final workflowFinderValue = ref.watch(
    workflowFinderProvider(referrer),
  ).value;
  if (workflowFinderValue?.found != true) return emptyWidget;

  return SizedBox(
    width: MediaQuery.of(context).size.width * 0.4,
    child: Container(
      color: workflowFinderValue!.isMyTurn
        ? _myTurnWorkflowBgColor : _noTurnWorkflowBgColor,
      padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(
              workflowFinderValue.label ?? "",
              style: const TextStyle(fontWeight: FontWeight.bold),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Expanded(
            child: Text(
              workflowFinderValue.text ?? "",
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    ),
  );
}
