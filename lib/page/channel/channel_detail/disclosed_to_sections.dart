part of 'channel_detail.dart';

abstract class _DisclosedToSections extends _ChannelSctions {
  Widget _buildDisclosedToUsersSection(
    BuildContext context,
    ChannelState? channelValue,
  ) {
    final provider = channelSyncWithDisclosedToUserSearchProvider(channel);
    final controller = ref.read(provider.notifier);

    final title = "Disclosed to users".i18n;

    List<Widget> buildButtons(List<DisclosedToUser> items) => [
      SearchButton(
        provider: provider,
        heroTag: "hero_search_disclosed_to_user",
        compact: true, useChannelDatabase: true,
        onPressed: () async {
          if (
            !await showSearchUsersDialog(context, controller)
          ) return;

          await runWithProgress(context, controller.search);
        },
      ),
      InlineAddButton(
        heroTag: "hero_add_disclosed_to_user",
        tooltip: "Add disclosed to users".i18n,
        onPressed: () async {
          final excludeIds = channelValue?.realDisclosedToUsers.map(
            (u) => u.id,
          ).nonNulls.toSet() ?? {}; {
            final ownerPlrId = channel.plrId;
            if (ownerPlrId != null) excludeIds.add(ownerPlrId.toString());
          }

          final friends = await showFriendSelectDialog(
            context, title: "Disclosed to friends".i18n,
            empty: "No friends to disclose.".i18n,
            excludeIds: excludeIds,
            selectionMode: AsyncListSelectionMode.multiple,
            showSelectAll: false,
            onError: (e, s) => showError(context, e, s),
          );
          if (friends == null) return;

          await runWithProgress(
            context, () => Future.wait(
              friends.map((f) async {
                  if (!f.isLoaded) await f.syncSilently();
                  await f.meToFriendRoot?.addChannelAndRequestDisclosureConsent(
                    channel, root: root,
                  );
              }),
            ),
            onError: (e, s) => processNeedNetworkException(context, e, s),
            onFinish: (_) => controller.refresh(),
          );
        }),
    ];

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => DisclosedToUserList<
      ChannelSyncWithDisclosedToUserSearchProvider,
      ChannelSyncWithDisclosedToUserSearch, ChannelState
    >(
      key: const ValueKey("disclosed_to_user_list"),
      shrinkWrap: shrinkWrap,
      provider: provider,
      container: channel,
      cellConfig: disclosedToUserCellConfigWith(
        context, ref, controller,
      ).copyWith(
        onTap: (disclosedToUser, _) => openFriendOrUserPage(
          context, account, disclosedToUser,
        ),
        buttonBuilders: [
          addFriendButtonBuilder,
          ..._createConsentButtonBuilders(context, channelValue),
        ],
      ),
    );

    return buildLimitedListSection(
      context, ref, provider: provider, readOnly: false,
      title: title, showAllButtonTooltip: "Show all disclosed to users".i18n,
      itemsGetter: defaultDisclosedToUserListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Widget _buildDisclosedToChannelsSection(
    BuildContext context,
    ChannelState? channelValue,
  ) {
    final provider = channelSyncWithDisclosedToChannelSearchProvider(channel);
    final controller = ref.read(provider.notifier);

    final title = "Disclosed to channels".i18n;

    List<Widget> buildButtons(List<DisclosedToChannel> items) => [
      SearchButton(
        provider: provider,
        heroTag: "hero_search_disclosed_to_channel",
        compact: true, useChannelDatabase: true,
        onPressed: () async {
          if (
            !await showSearchChannelsDialog(context, controller)
          ) return;

          await runWithProgress(context, controller.search);
        },
      ),
      InlineAddButton(
        heroTag: "hero_add_disclosed_to_channel",
        tooltip: "Add disclosed to channels".i18n,
        onPressed: () async {
          final excludeIds = channelValue?.realDisclosedToChannels.map(
            (c) => c.id,
          ).nonNulls.toSet() ?? {}; {
            var channelId = channel.id;
            if (channelId != null) excludeIds.add(channelId);
          }

          var channels = await showChannelSelectDialog(
            context, root, title: "Disclosed to channels".i18n,
            empty: "No channels to disclose.".i18n,
            excludeIds: excludeIds,
            selectionMode: AsyncListSelectionMode.multiple,
            showSelectAll: false,
            onError: (e, s) => showError(context, e, s),
          );
          if (channels == null) return;

          await runWithProgress(
            context, () => Future.wait(
              channels.map((c) async {
                  await c.syncSilently();
                  await c.addChannelAndRequestDisclosureConsent(
                    channel, root: root,
                  );
              }),
            ),
            onError: (e, s) => showError(context, e, s),
            onFinish: (_) => controller.refresh(),
          );
        }),
    ];

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => DisclosedToChannelList<
      ChannelSyncWithDisclosedToChannelSearchProvider,
      ChannelSyncWithDisclosedToChannelSearch, ChannelState
    >(
      key: const ValueKey("disclosed_to_channel_list"),
      shrinkWrap: shrinkWrap,
      provider: provider,
      container: channel,
      cellConfig: disclosedToChannelCellConfigWith(
        context, ref, controller,
      ).copyWith(
        onTap: (disclosedToChannel, _) => _mayOpenTimeline(
          context, disclosedToChannel.id,
        ),
        onIconTap: (disclosedToChannel, _) => _mayOpenChannel(
          context, disclosedToChannel.id,
        ),
        buttonBuilders: _createConsentButtonBuilders(context, channelValue),
      ),
    );

    return buildLimitedListSection(
      context, ref, provider: provider, readOnly: false,
      title: title, showAllButtonTooltip: "Show all disclosed to channels".i18n,
      itemsGetter: defaultDisclosedToChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  List<AsyncCellButtonBuilder<T, N, S>> _createConsentButtonBuilders<
    T extends DisclosedTo, N extends PlrAsyncNotifier, S
  >(BuildContext context, ChannelState? channelValue, ) {
    final me = root.plrId;

    final dataSubjects = channelValue?.dataSubjects ?? const [];
    final isDataSubject = dataSubjects.any((u) => u.plrId == me);

    bool isDataSubjectItem(T item) => (
      (item is DisclosedToUser) &&
      dataSubjects.any((u) => u.plrId == item.plrId)
    );

    final buttonBuilders = <AsyncCellButtonBuilder<T, N, S>>[];

    if (isOwner || isDataSubject) buttonBuilders.add(
      (context, ref, _2, object, _4, _5, isEnabled, wrapCallback) {
        if (object.isConsented) {
          final _isDataSubjectItem = isDataSubjectItem(object);
          return CompactIconButton(
            icon: const Icon(Icons.check_circle),
            color: Colors.green,
            tooltip: _isDataSubjectItem ?
              "The dislosure to the data subject is always available.".i18n :
              "Un-consent this disclosure.".i18n,
            onPressed: (!_isDataSubjectItem && isEnabled) ? wrapCallback(
              context, ref, () => showConfirmDialog(
                context, (
                  (T == DisclosedToUser)
                    ? "Disclosed to users" : "Disclosed to channels"
                ).i18n,
                "Are you sure to un-consent to this disclosure?".i18n,
                onOk: () async {
                  if (await unconsent([ object ])) {
                    _channelSyncController.refresh();
                  }
                },
              ),
            ) : null,
          );
        }
        else {
          final consentedByMe = (
            isOwner ? object.isOwnerConsented : object.hasConsentedUserOf(me)
          );
          return Row(
            children: [
              CompactIconButton(
                icon: const Icon(Icons.do_not_disturb),
                color: consentedByMe ? Colors.green : Colors.red,
                tooltip: (
                  consentedByMe
                    ? "Un-consent this disclosure."
                    : "Consent this disclosure."
                ).i18n,
                onPressed: isEnabled ? wrapCallback(
                  context, ref, () => showConfirmDialog(
                    context, (
                      (T == DisclosedToUser)
                        ? "Disclosed to users" : "Disclosed to channels"
                    ).i18n,
                    consentedByMe
                      ? "Are you sure to un-consent to this disclosure?".i18n
                      : "Are you sure to consent to this disclosure?".i18n,
                    onOk: () async {
                      var doRefresh = true;
                      if (consentedByMe) {
                        doRefresh = await unconsent([ object ]);
                      } else await consent([ object ]);

                      if (doRefresh) _channelSyncController.refresh();
                    },
                  ),
                ) : null,
              ),
              if (isOwner) CompactIconButton(
                icon: const Icon(Icons.checklist_rtl),
                tooltip: "Show the list of un-consented data subjects.".i18n,
                onPressed: isEnabled ? wrapCallback(
                  context, ref, () => _showUnconsented(
                    context, dataSubjects,
                    object.consentedUsers, object.isOwnerConsented,
                  ),
                ) : null,
              ),
            ],
          );
        }
      }
    );
    else buttonBuilders.add(
      (context, ref, _2, object, _4, _5, isEnabled, wrapCallback) {
        if (object.isConsented) return CompactIconButton(
          icon: const Icon(Icons.check_circle),
          color: Colors.green,
          tooltip: isDataSubjectItem(object)
            ? "The dislosure to the data subject is always available.".i18n
            : "All data subjects were consented.".i18n,
        );
        else return CompactIconButton(
          icon: const Icon(Icons.do_not_disturb),
          color: Colors.red,
          tooltip: "Some data subjects were not consented.".i18n,
        );
      },
    );
    return buttonBuilders;
  }

  void _showUnconsented(
    BuildContext context,
    Iterable<DisclosedToUser> dataSubjects,
    Iterable<DisclosedToUser> consentedUsers,
    bool isOwnerConsented,
  ) => showDialog(
    context: context, builder: (context) => AlertDialog(
      title: Text("Un-consented data subjects".i18n),
      content: Container(
        width: double.maxFinite,
        child: DataSubjectList<
          ChannelSyncProvider, ChannelSync, ChannelState
        >(
          shrinkWrap: true,
          provider: _channelSyncProvider,
          container: channel,
          cellConfig: dataSubjectCellConfigWithDefault(
            useDefaultDismissibleConfig: false,
          ),
          itemsGetter: (value) => [
            if (!isOwnerConsented) channel,
            ...value.dataSubjects.where(
              (u) => !consentedUsers.contains(u),
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
          child: Text(MaterialLocalizations.of(context).closeButtonLabel),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    ),
  );
}
