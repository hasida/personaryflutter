import 'dart:async';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show listEquals;
import 'package:flutter/material.dart' hide Tab;
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart' as p;
import 'package:synchronized/extension.dart';

import '../../../logic.dart';
import '../../../style.dart';
import '../../../util.dart';
import '../../../widget.dart';
import '../../../dialog/channel_edit/channel_info_dialog.dart';
import '../../../semeditor/page/semedit_main.dart';
import '../../channel_summary/channel_summary_page.dart';
import '../../friend/friend_page.dart';
import '../../timeline/timeline_page.dart';
import '../channel_page.dart';
import '../util.dart';
import 'channel_detail.i18n.dart';
import 'workflow_finder.dart';

part 'base.dart';
part 'header.dart';
part 'channel_sections.dart';
part 'disclosed_to_sections.dart';
part 'other_sections.dart';

// 全チャネルの情報をキャッシュしておく制限時間
const int _allChannelsCacheLifetimeMinutes = 10;

class ChannelDetail extends StatefulPlrWidget {
  final Channel channel;
  final bool? consentRequested;

  ChannelDetail(
    super.account,
    this.channel, {
      super.key,
      this.consentRequested,
    });

  @override
  ConsumerState<ChannelDetail> createState() => _ChannelDetailState();
}

class _ChannelDetailState extends _OtherSections {
  void _initListeners(BuildContext context) {
    ref.listen(_channelSyncProvider, (_, next) async {
        if (processError(context, next)) return;

        switch (next) {
          case AsyncData(:final value, isLoading: false): {
            onChannelDisplayed(notificationRegistry, channel, !value.hadErrors);
            autoDisclose(root, account, channel);
            if (value.updatedIds.contains(rootGraphId) == true) {
              if (await _checkOnlyOneMyChannelInSection()) {
                _channelSyncController.refresh();
              }
            }
          }
        }
    });
    ref.listen(
      _channelProfileItemsProvider, (_, next) => processError(context, next),
    );
    ref.listen(
      _disclosedProfileItemsProvider, (_, next) => processError(context, next),
    );
    ref.listen(
      _timelineSchemataProvider, (_, next) => processError(context, next),
    );
  }

  Future<void> _refresh([bool force = false]) => synchronized(() async {
      final _publicProfileItemsProvider = ref.read(
        _publicProfileItemsProviderProvider,
      );
      await Future.wait([
          _channelSyncController.refresh(force: force),
          _channelProfileItemsController.refresh(force: force),
          _disclosedProfileItemsController.refresh(force: force),
          if (_publicProfileItemsProvider != null) ref.read(
            _publicProfileItemsProvider.notifier,
          ).refresh(force: force),
          _timelineSchemataController.refresh(),
      ]);
  });

  @override
  Widget build(BuildContext context) {
    _initListeners(context);

    return RefreshIndicator(
      onRefresh: () => _refresh(true),
      child: TimerShownDetector(
        onTimer: _channelSyncController.refresh,
        child: PageScrollView(
          child: Container(
            padding: allPadding[3],
            child: _buildBody(context),
          ),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    final channelValue = ref.watch(_channelSyncProvider).value;
    if (channelValue == null) return const Center(
      child: CircularProgressIndicator(),
    );

    final channelDataSettingProvider = _setupChannelDataSetting(channelValue);

    final DataSettingState? channelDataSettingValue;
    final Set<DisplaySection>? displaySections;
    if (channelDataSettingProvider != null) {
      channelDataSettingValue = ref.watch(channelDataSettingProvider).value;
      displaySections = channelDataSettingValue?.displaySections.toSet()
        ?? const {};
    } else {
      channelDataSettingValue = null;
      displaySections = null;
    }

    final timelineSchemataValue = ref.watch(_timelineSchemataProvider).value;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildChannelDetailHeader(
          context, channelValue, timelineSchemataValue,
        ),

        const Padding(
          padding: const EdgeInsets.only(top: 12),
          child: const Divider(height: 0, thickness: 2, color: Colors.grey),
        ),

        _buildChannelDetailOwner(context),

        const Divider(color: Colors.grey),

        _buildChannelSections(
          context, channelValue, channelDataSettingValue, displaySections,
        ),

        _buildDisclosedToUsersSection(context, channelValue),

        _buildDisclosedToChannelsSection(context, channelValue),

        /*
        if (displaySections?.contains(DisplaySection.channel) != false)
          _buildImportsSection(context, channelValue),
        */

        if (displaySections?.contains(DisplaySection.data_setting) != false)
          _buildDataSettingsSection(context, channelValue),

        if (displaySections?.contains(DisplaySection.schema) != false)
          _buildSchemataSection(context, channelValue),

        if (!readOnly) _buildDataSubjectsSection(context, channelValue),
      ],
    );
  }

  DataSettingSyncProvider? _setupChannelDataSetting(
    ChannelState channelValue,
  ) {
    final channelDataSetting = channelValue.channelDataSetting;
    if (channelDataSetting == null) return null;

    final provider = dataSettingSyncProvider(channelDataSetting);
    ref.listen(provider, (_, next) {
        if (next.value?.isTopPage == true) {
          if (root.setTopPageChannelId(channel.id)) {
            root.syncSilently();
          }
        }
    });
    return provider;
  }
}
