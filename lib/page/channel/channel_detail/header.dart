part of 'channel_detail.dart';

abstract class _Header extends _Base {
  Widget _buildChannelDetailHeader(
    BuildContext context,
    ChannelState? channelValue,
    TimelineSchemataState? timelineSchemataValue,
  ) {
    final isEnabled = timelineSchemataValue?.schemata.any(
      (s) => s.timelineState != TimelineState.Disabled,
    ) ?? false;

    final isMailTimelineUpdates =
      channelValue?.isMailTimelineUpdates ?? false;

    return Container(
      key: const ValueKey("channel_detail_header"),
      padding: const EdgeInsets.only(right: 6),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Text(
              channelValue?.name?.defaultValue ?? channel.channelId,
              style: const TextStyle(fontSize: 24),
            ),
          ),
          // Add semanticeditor
          _buildSemEditButton(context, isEnabled),
          _buildMailTimelineUpdatesButton(context, isMailTimelineUpdates),
          _buildCopyDeepLinkButton(context),
          // 自分のチャネルの場合のみ表示
          if (isOwner) _buildCopyCooperationStringButton(context),
          if (
            isOwner || channel.dataSubjects.any((u) => u.plrId == root.plrId)
          ) _buildChannelDisclosureConsentDialogButton(context),
          _buildTimelineButton(context, isEnabled, timelineSchemataValue),
          if (
            timelineSchemataValue?.schemata.any(
              (s) => s.channelSummaryStyleSheets.isNotEmpty,
            ) == true
          ) _buildChannelSummaryButton(context, isEnabled),
          _buildSettingsButton(context),
        ],
      ),
    );
  }

  Widget _buildSemEditButton(
    BuildContext context,
    bool isEnabled,
  ) => CompactIconButton(
    icon: Opacity(
      opacity: isEnabled ? 1.0 : .5,
      child: Image.asset(
        "assets/sem_editor.png",
        width: 24, height: 24, filterQuality: FilterQuality.medium,
      ),
    ),
    onPressed: () => isEnabled ? Navigator.push(
      context, MaterialPageRoute(
        builder: (_) => SemEditMain(account, channel),
      ),
    ) : null,
  );

  Widget _buildMailTimelineUpdatesButton(
    BuildContext context,
    bool isMailTimelineUpdates,
  ) => CompactIconButton(
    icon: Icon(
      Icons.outgoing_mail,
      color: isMailTimelineUpdates ? Colors.black : Colors.grey,
    ),
    tooltip: "Set up mail notification of timeline update.".i18n,
    onPressed: () {
      if (channel.setMailTimelineUpdates(!isMailTimelineUpdates)) {
        _channelSyncController.refresh();
      }
      showMessage(
        context, isMailTimelineUpdates
          ? "You'll not send mail to notify other users of timelineupdate.".i18n
          : "You'll send mail to notify other users of timeline update.".i18n,
      );
    },
  );

  Widget _buildCopyDeepLinkButton(
    BuildContext context,
  ) => CompactIconButton(
    icon: const Icon(Icons.link),
    tooltip: "Copy deep link to clipboard.".i18n,
    onPressed: () => Clipboard.setData(
      ClipboardData(text: channel.toUri().toString()),
    ).then((_) => showMessage(context, "Deep link copied to clipboard.".i18n)),
  );

  Widget _buildCopyCooperationStringButton(
    BuildContext context,
  ) => CompactIconButton(
    icon: const Icon(Icons.content_copy),
    onPressed: () => Clipboard.setData(
      ClipboardData(
        text: "${channel.masterStorage.plrId.toString()}\n${channel.id ?? ""}",
      )
    ).then((_) => showMessage(
        context, "Cooperation string copied to clipboard.".i18n,
    )),
  );

  Widget _buildChannelDisclosureConsentDialogButton(
    BuildContext context,
  ) => CompactIconButton(
    icon: const Icon(Icons.checklist_rtl),
    tooltip: "Show channel disclosure consent dialog.".i18n,
    onPressed: () => _showConsentDialog(false),
  );

  Widget _buildTimelineButton(
    BuildContext context,
    bool isEnabled,
    TimelineSchemataState? timelineSchemataValue,
  ) => CompactIconButton(
    icon: Icon(
      Icons.timeline, color: isEnabled ? Colors.black : Colors.grey,
    ),
    onPressed: () => isEnabled ? Navigator.push(
      context, CupertinoPageRoute(
        builder: (_) => TimelinePage(
          account, channel, schemataSources: timelineSchemataValue?.sources,
        ), // timelineState is already checked.
      ),
    ) : null,
  );

  Widget _buildChannelSummaryButton(
    BuildContext context,
    bool isEnabled,
  ) => CompactIconButton(
    icon: Icon(
      Icons.bar_chart, color: isEnabled ? Colors.black : Colors.grey,
    ),
    onPressed: () => isEnabled ? Navigator.push(
      context, CupertinoPageRoute(
        builder: (_) => ChannelSummaryPage(channel),
      ),
    ) : null,
  );

  Widget _buildSettingsButton(
    BuildContext context,
  ) => CompactIconButton(
    icon: const Icon(Icons.settings),
    onPressed: () => showDialog(
      context: context, builder: (ctx) => ChannelInfoDialog(
        root: root, channel: channel,
        notificationRegistry: notificationRegistry,
      ),
    ).then((c) {
        if (c != null) {
          _channelSyncController.refresh();
          _timelineSchemataController.refresh();
        }
      },
    ),
  );

  Widget _buildChannelDetailOwner(BuildContext context) => Column(
    key: const ValueKey("channel_owner"),
    children: [
      UserHeader(
        channel, buttons: [
          _ProfileDiscloseButton(
            account, channel,
            _disclosedProfileItemsProvider, publicProfileItemsProvider,
          ),
        ],
      ),

      ProfileView(
        provider: _channelProfileItemsProvider,
        include: !readOnly ? const [ genderClass, birthClass ] : null,
        exclude: const [ nameClass, pictureClass ],
      ),
    ],
  );
}

class _ProfileDiscloseButton extends StatelessPlrWidget {
  final Channel channel;
  final ProfileItemsProvider disclosedProfileItemsProvider;
  final ProfileItemsProvider? publicProfileItemsProvider;
  const _ProfileDiscloseButton(
    super.account,
    this.channel,
    this.disclosedProfileItemsProvider,
    this.publicProfileItemsProvider,
  );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final disclosedProfileItemsState = ref.watch(
      disclosedProfileItemsProvider,
    ).value;
    final disclosedProfileItemsController = ref.read(
      disclosedProfileItemsProvider.notifier,
    );

    ProfileItemsState? publicProfileItemsState;
    if (publicProfileItemsProvider != null) {
      publicProfileItemsState = ref.watch(publicProfileItemsProvider!).value;
    }

    return IconButton(
      icon: const Icon(Icons.settings, color: Colors.black),
      onPressed: (
        (disclosedProfileItemsState != null) &&
        (publicProfileItemsState != null)
      ) ? () async {
        final result = await showDialog<List<ProfileEntry>>(
          context: context,
          builder: (_) => ProfileDiscloseDialog(
            title: "Disclose profile".i18n,
            root: root,
            selectedList: disclosedProfileItemsState.flatten.map(
              (e) => e.value,
            ),
            excludeList: publicProfileItemsState!.flatten.map((e) => e.value),
          ),
        );
        if (result == null) return;

        final newItems = <SchemaClass, List<ProfileItem>>{};
        for (var r in result) {
          (newItems[r.schemaClass] ??= []).add(r.item);
        }
        await runWithProgress(
          context,
          () => ProfileDiscloseManager(channel).updateItems(
            disclosedProfileItemsState.items, newItems,
          ),
        );
        disclosedProfileItemsController.refresh(force: true);
      } : null,
    );
  }
}
