// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workflow_finder.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$workflowFinderHash() => r'88e5df5999e37625f2eebf3e6d919bcc642a511a';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$WorkflowFinder
    extends BuildlessAutoDisposeAsyncNotifier<WorkflowFinderState> {
  late final Channel channel;

  FutureOr<WorkflowFinderState> build(
    Channel channel,
  );
}

/// See also [WorkflowFinder].
@ProviderFor(WorkflowFinder)
const workflowFinderProvider = WorkflowFinderFamily();

/// See also [WorkflowFinder].
class WorkflowFinderFamily extends Family<AsyncValue<WorkflowFinderState>> {
  /// See also [WorkflowFinder].
  const WorkflowFinderFamily();

  /// See also [WorkflowFinder].
  WorkflowFinderProvider call(
    Channel channel,
  ) {
    return WorkflowFinderProvider(
      channel,
    );
  }

  @override
  WorkflowFinderProvider getProviderOverride(
    covariant WorkflowFinderProvider provider,
  ) {
    return call(
      provider.channel,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'workflowFinderProvider';
}

/// See also [WorkflowFinder].
class WorkflowFinderProvider extends AutoDisposeAsyncNotifierProviderImpl<
    WorkflowFinder, WorkflowFinderState> {
  /// See also [WorkflowFinder].
  WorkflowFinderProvider(
    Channel channel,
  ) : this._internal(
          () => WorkflowFinder()..channel = channel,
          from: workflowFinderProvider,
          name: r'workflowFinderProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$workflowFinderHash,
          dependencies: WorkflowFinderFamily._dependencies,
          allTransitiveDependencies:
              WorkflowFinderFamily._allTransitiveDependencies,
          channel: channel,
        );

  WorkflowFinderProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.channel,
  }) : super.internal();

  final Channel channel;

  @override
  FutureOr<WorkflowFinderState> runNotifierBuild(
    covariant WorkflowFinder notifier,
  ) {
    return notifier.build(
      channel,
    );
  }

  @override
  Override overrideWith(WorkflowFinder Function() create) {
    return ProviderOverride(
      origin: this,
      override: WorkflowFinderProvider._internal(
        () => create()..channel = channel,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        channel: channel,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<WorkflowFinder, WorkflowFinderState>
      createElement() {
    return _WorkflowFinderProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is WorkflowFinderProvider && other.channel == channel;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, channel.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin WorkflowFinderRef
    on AutoDisposeAsyncNotifierProviderRef<WorkflowFinderState> {
  /// The parameter `channel` of this provider.
  Channel get channel;
}

class _WorkflowFinderProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<WorkflowFinder,
        WorkflowFinderState> with WorkflowFinderRef {
  _WorkflowFinderProviderElement(super.provider);

  @override
  Channel get channel => (origin as WorkflowFinderProvider).channel;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
