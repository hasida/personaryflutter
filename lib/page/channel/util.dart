import 'package:plr_ui/plr_ui.dart';

bool needOwnerConsent(Root root, Channel channel) {
  var plrId = channel.plrId;

  if (
    (plrId == null) || (
      (plrId != root.plrId) && (plrId != channel.storage?.plrId) &&
      (plrId != channel.masterStorage.plrId)
    )
  ) return false;

  return channel.disclosedToChannels(true).any(
    (d2c) => !d2c.isOwnerConsented,
  ) || channel.disclosedToUsers(true).any(
    (d2u) => !d2u.isOwnerConsented,
  );
}
