import 'package:collection/collection.dart' show ListEquality;
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../style.dart';
import '../../util.dart' show showError;
import '../../widget.dart';
import '../timeline/timeline_item.dart';
import 'item_sets_page.i18n.dart';

final _listEquals = const ListEquality().equals;

int _compareItem(Item i1, Item i2) {
  var b1 = i1.begin;
  var b2 = i2.begin;
  if (b1 == null) {
    if (b2 == null) return i1.id!.compareTo(i2.id!);
    return 1;
  }
  else if (b2 == null) return -1;
  else return b1.compareTo(b2);
}

class ItemSetsPage<T extends FriendRoot> extends StatelessConsumerPlrWidget {
  final T friendRoot;
  ItemSetsPage(this.friendRoot, { super.key });

  late final FriendRootSyncProvider _provider;
  late final FriendRootSync _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _provider = friendRootSyncProvider(friendRoot);
    _controller = ref.read(_provider.notifier);
  }

  Future<void> _refresh([ bool force = false ]) =>
    _controller.refresh(force: force);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    ref.listen(_provider, (_, next) => processError(context, next));

    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(context, ref),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _refresh,
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: _buildPage(context, ref),
              ),
            ),
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Consumer(
      builder: (context, ref, _) {
        var sb = StringBuffer("Nodes disclosed".i18n);

        var count = switch (ref.watch(_provider)) {
          AsyncData(:final value) => value.itemSets.fold<int>(
            0, (v, e) => v + e.items.length,
          ),
          _ => 0,
        };
        if (count > 0) sb..write(" (")..write(count)..write(")");

        return Text(sb.toString());
      },
    ),
  );

  Widget _buildPage(
    BuildContext context,
    WidgetRef ref,
  ) {
    final state = ref.watch(_provider);
    final value = state.value;
    if (value == null) return emptyPage;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: value.itemSets.map(
        (s) => _buildItemsSection(context, value, s),
      ).toList(),
    );
  }

  Widget _progressOrNoSchemata(AsyncSnapshot snapshot) {
    if (snapshot.connectionState != ConnectionState.done) {
      return const Center(child: const CircularProgressIndicator());
    }
    return Center(child: Text("Failed to load schemata.".i18n));
  }

  Widget _buildItemsSection(
    BuildContext context,
    FriendRootState value,
    ItemSet itemSet,
  ) => HookBuilder(
    builder: (context) {
      final snapshot = useStream(
        useMemoized(
          () => itemSet.schemataSourcesWith(notificationRegistry).handleError(
            (e, s) => showError(context, e, s),
          ),
        ),
      );
      if (snapshot.data?.isNotEmpty != true) {
        return _progressOrNoSchemata(snapshot);
      }

      var schemataSource = snapshot.data![0];

      return HookBuilder(
        key: ValueKey(schemataSource),
        builder: (context) {
          final snapshot = useStream(
            useMemoized(
              () => schemataSource.create().handleError(
                (e, s) => showError(context, e, s),
              ),
            ),
          );
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildItemSetHeader(context, value, snapshot, itemSet),
              _buildItems(context, value, snapshot, itemSet),
            ],
          );
        },
      );
    },
  );

  Widget _buildItemSetHeader(
    BuildContext context,
    FriendRootState rootValue,
    AsyncSnapshot<Schemata?> snapshot,
    ItemSet itemSet,
  ) => Consumer(
    builder: (context, ref, _) {
      final provider = channelUpdateProvider(itemSet.infoAsReferencing);
      ref.listen(provider, (_, next) => processError(context, next));

      final controller = ref.read(provider.notifier);
      if (rootValue.isForced) controller.refresh(force: true);

      final state = ref.watch(provider);
      final value = state.value;

      String? name;
      if (value?.isRemoved == true) name = "[Removed]".i18n;
      else {
        name = value?.name;
        if (name == null) {
          if (state.isLoading) {
            name = "Retrieving...".i18n;
          } else name = "Unknown".i18n;
        }
      }
      var header = SectionHeader(name);

      var description = value?.description;
      if ((description == null) || description.isEmpty) return header;

      return Tooltip(
        message: description,
        child: header,
      );
    },
  );

  final List<Widget> bgPair = dismissibleBackgroundPairWith(
    Colors.red, const Icon(Icons.delete, color: Colors.white),
    Text("Undisclose".i18n, style: const TextStyle(color: Colors.white)),
  );

  Widget _buildItems(
    BuildContext context,
    FriendRootState rootValue,
    AsyncSnapshot<Schemata?> snapshot,
    ItemSet itemSet,
  ) {
    var schemata = snapshot.data;
    if (schemata == null) return _progressOrNoSchemata(snapshot);

    List<Item> sortedItems() => itemSet.items.toList()..sort(_compareItem);

    return HookBuilder(
      builder: (context) {
        final items = useState(sortedItems());

        void mayUpdateItemsOrder() => Future.microtask(() async {
            var newItems = sortedItems().toList();
            if (!_listEquals(items.value, newItems)) {
              items.value = newItems;
            }
        });

        return Column(
          children: items.value.map(
            (item) => Dismissible(
              key: ValueKey(item.id),
              background: bgPair[0],
              secondaryBackground: bgPair[1],
              confirmDismiss: (_) async {
                if (friendRoot is! MeToFriendRoot) return false;
                return showConfirmDialog(
                  context, "Undisclose".i18n,
                  "Undisclose the node, are you sure?".i18n,
                );
              },
              onDismissed: (_) => runWithProgress(
                context, () => (friendRoot as MeToFriendRoot).removeItem(item),
                onError: (e, s) => showError(context, e, s),
                onFinish: (r) => _refresh(),
              ),
              child: Consumer(
                builder: (context, ref, _) {
                  final provider = entitySyncProvider(item);
                  ref.listen(
                    provider, (_, next) => processError(context, next),
                  );

                  final controller = ref.read(provider.notifier);
                  if (rootValue.isForced) controller.refresh(force: true);

                  final state = ref.watch(provider);
                  mayUpdateItemsOrder();

                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: _createContent(itemSet, schemata, item, state),
                    ),
                  );
                },
              ),
            ),
          ).toList(),
        );
      },
    );
  }

  void _dummyCallback(RecordEntity? e) {}

  Widget _createContent(
    ItemSet itemSet,
    Schemata schemata,
    Item item,
    AsyncValue<EntityState?> state,
  ) {
    final value = state.value;
    if (value?.isDeleted == true) return Center(child: Text("[Removed]".i18n));

    var recordEntity = RecordEntity.from(schemata, item);
    if (recordEntity == null) {
      if (state.isLoading) {
        return const Center(child: const CircularProgressIndicator());
      } else return Center(child: Text("Cannot display the node.".i18n));
    }

    return TimelineItem(
      account, recordEntity, false, itemSet, root.plrId.toString(), true,
      schemata, _dummyCallback, _dummyCallback, _dummyCallback, fullDate: true,
    );
  }
}
