import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  unknownTranslation +
  removedTranslation +
  retrievingTranslation +
  {
    "en_us": "Nodes disclosed",
    "ja_jp": "開示ノード",
  } +
  {
    "en_us": "Failed to load schemata.",
    "ja_jp": "スキーマが読み込めません。",
  } +
  {
    "en_us": "Cannot display the node.",
    "ja_jp": "ノードが表示できません。",
  } +
  {
    "en_us": "Undisclose",
    "ja_jp": "開示停止",
  } +
  {
    "en_us": "Undisclose the node, are you sure?",
    "ja_jp": "このノードの開示を停止します。よろしいですか?",
  };

  String get i18n => localize(this, t);
}
