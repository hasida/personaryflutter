import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart'
  hide ChangeNotifierProvider, Consumer, Notifier, Provider;
import 'package:personaryFlutter/dialog/channel_edit/data_setting_dialog.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';

import '../../util.dart';
import '../../widget.dart';
import '../../style.dart';
import 'data_settings_page.i18n.dart';

class DataSettingsPage extends StatelessPlrWidget {
  DataSettingsPage(super.account, { super.key });
  final _bgPair = dismissibleBackgroundPairWith(
    Colors.red, const Icon(Icons.delete, color: Colors.white),
    Text(
      "Delete channel setting".i18n,
      style: const TextStyle(color: Colors.white)
    ),
  );
  Future<void> _refresh(BuildContext context, [ bool force = false ]) {
    return Future.wait([
        context.read<DataSettingPublishNotifier>().update(force),
    ]);
  }

  void updateDisclose(BuildContext context) async {
    var publicRootId = publicRoot.nodeHash;
    var lifeRecord = root.lifeRecord!;
    await lifeRecord.syncSilently();

    for (var d in lifeRecord.disclosures) {
      if (d.nodeHash == publicRootId) {
        if (await d.disclose()){
          await d.syncSilently();
        }
      }
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) => SafeArea(
    child: MultiProvider(
      providers: [
        ChangeNotifierProvider<DataSettingPublishNotifier>(
            create: (context) => DataSettingPublishNotifier(
              root, onError: (e, s) => showError(context, e, s),
            )..update(),
        ),
      ],
      child: _buildPage(context),
    ),
  );

  Widget _buildPage(BuildContext context) =>
    Builder(
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: Consumer<DataSettingPublishNotifier>(
            builder: (_, value, __) {
              var sb = StringBuffer("Channel settings".i18n);
              var count = value.dataSettings.length;
              if (count > 0) {
                sb..write(" (")..write(count)..write(")");
              }
              return Text(sb.toString());
            },
          ),
          actions: [
            Container(
              width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
              child:FloatingActionButton(
                heroTag: "hero_create_datasetting",
                tooltip: "Create channel setting".i18n,
                child: const Icon(Icons.add),
                onPressed: () async {
                  showDialog(
                    context: context,
                    builder: (context) => DataSettingDialog(
                      root: root,
                      notificationRegistry: notificationRegistry,
                    ),
                  ).then((value) {
                    if (value != null) _refresh(context);
                  });
                },
              ),
            ),
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () => _refresh(context, true),
          child: TimerShownDetector(
            onTimer: () => _refresh(context),
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: Column(
                  children: [
                    divider,
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Consumer<DataSettingPublishNotifier>(
                              builder: (_, value, __) {
                                if (value.dataSettings.isEmpty && value.loaded)
                                  return Center(child: Text("No channel settings.".i18n));

                                  var content = <Widget>[];
                                  var dataSettings = value.dataSettings.toList();
                                  dataSettings.sort((a, b) {
                                    if (a.plrId != b.plrId) {
                                      if ((b.plrId == null) || (a.plrId == root.plrId)) {
                                        return -1;
                                      } else if ((a.plrId == null) || (b.plrId == root.plrId)) {
                                        return 1;
                                      }
                                      return a.plrId!.email.compareTo(b.plrId!.email);
                                    }
                                    return a.dataSettingId.compareTo(b.dataSettingId);
                                  });

                                  for (var dataSetting in dataSettings) {
                                    bool public = value.publicDataSettings.contains(dataSetting.dataSettingId);
                                    content.add(_createDataSettingCell(context, dataSetting, public, root));
                                  }

                                  return SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        ...content,
                                        if (!value.loaded)
                                          const CircularProgressIndicator(),
                                      ],
                                    ),
                                  );
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  Widget _createDataSettingCell(BuildContext context, DataSetting dataSetting, bool public, Root root) {

    return Card(
      elevation: 2,
      child: Stack(
        children: [
          ChangeNotifierProvider<HasOwnerNotifier>(
            key: Key('data_settings_id__${dataSetting.id.toString()}'),
            create: (_) => HasOwnerNotifier(dataSetting, root: root)..update(),
            child: Consumer<HasOwnerNotifier>(
              builder: (ctx, notifier, child) {
                var widget;
                if (dataSetting.plrId == root.plrId) {
                  widget = _createDismissibleTile(context, dataSetting, public, root, notifier);
                } else {
                  widget = _createListTile(context, dataSetting, public, root, notifier);
                }
                return widget;
              },
            ),
          ),
        ],
      ),
    );
  }
  Widget _createListTile(BuildContext context, DataSetting dataSetting, bool public, Root root, HasOwnerNotifier notifier) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 5),
      leading: InkWell(
        child: (notifier.picture != null)
            ? memoryImageOf(notifier.picture!)
            : const Icon(Icons.person, size: 40),
      ),
      title: Row(
        children: [
          Expanded(
              child: Text(
                  dataSetting.name?.defaultValue ?? dataSetting.dataSettingId ?? "",
                  overflow: TextOverflow.ellipsis
              )
          ),
        ],
      ),
      subtitle: Text(
        notifier.owner ?? '',
        style: const TextStyle(
          color: Colors.black,
          fontSize: 16,
        ),
        overflow: TextOverflow.ellipsis,
      ),
      trailing: SizedBox(
        width: 50,
        height: 50,
        child: IconButton(
          icon: public
              ? (
                dataSetting.prohibit
                  ? const Icon(Icons.public, color: Colors.black)
                  : const Icon(Icons.recycling, color: Colors.black)
                )
              : const Icon(Icons.lock, color: Colors.grey),
          onPressed: () {
            final notifier = context.read<DataSettingPublishNotifier>();

            if (public) {
              if (dataSetting.prohibit) {
                notifier.setProhibit(dataSetting, false);
              }
              else {
                notifier.removePublicDataSetting(dataSetting);
              }
            } else {
              notifier.addPublicDataSetting(dataSetting);
            }
          },
        ),
      ),
      onTap: (dataSetting.plrId == root.plrId)
          ? () => showDialog(
        context: context,
        builder: (ctx) => DataSettingDialog(
          root: root,
          dataSetting: dataSetting,
          notificationRegistry: notificationRegistry,
        ),
      )
          : null,
      selectedTileColor: Theme.of(context).highlightColor,
    );
  }
  Widget _createDismissibleTile(BuildContext context, DataSetting dataSetting, bool public, Root root, HasOwnerNotifier notifier) {
    return Dismissible(
      key: ValueKey(dataSetting.id),
      background: _bgPair[0],
      secondaryBackground: _bgPair[1],
      confirmDismiss: (_) async {
        if (!await showConfirmDialog(
          context,
          "Remove channel setting".i18n,
          "Are you sure you want to delete this channel setting?".i18n,
        )) return false;
        return await _remove(dataSetting);
      },
      onDismissed: (_) => _refresh(context, true),
      child: _createListTile(context, dataSetting, public, root, notifier),
    );
  }
  Future<bool> _remove(DataSetting dataSetting) async {
    final dataSettings = await root.storage.dataSettings;
    publicRoot.removeDataSetting(dataSetting);
    return dataSettings.eliminateDataSetting(dataSetting);
  }
}
