import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  lifeRecordTranslation +
  {
    "en_us": "Channel settings",
    "ja_jp": "チャネル設定",
  } +
  {
    "en_us": "No channel settings.",
    "ja_jp": "チャネル設定がありません",
  } +
  {
    "en_us": "Publish profile",
    "ja_jp": "プロフィールの公開",
  } +
  {
    "en_us": "Delete channel setting",
    "ja_jp": "チャネル設定削除",
  } +
  {
    "en_us": "Remove channel setting",
    "ja_jp": "チャネル設定削除",
  } +
  {
    "en_us": "Are you sure you want to delete this channel setting?",
    "ja_jp": "チャネル設定を削除してよろしいですか？",
  } +
  {
    "en_us": "Create channel setting",
    "ja_jp": "新規チャネル設定を作成",
  };

  String get i18n => localize(this, t);
}
