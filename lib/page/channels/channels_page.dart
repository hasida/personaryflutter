import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../config.dart';
import '../../util.dart';
import '../../widget.dart';
import '../channel/channel_page.dart';
import 'channels_page.i18n.dart';

class ChannelsPage extends StatelessConsumerPlrWidget {
  ChannelsPage({ super.key });

  final _provider = channelNotificationsWithSearchProvider();
  late final ChannelNotificationsWithSearch _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _controller = ref.read(_provider.notifier);
    _checkChannelTransition(context);
  }

  Future<void> _refresh([
      bool force = false,
  ]) => _controller.refresh(
    force: force && !config.useSearchDrivenUpdate,
    skipNotInSearchResultFriends: true,
  );

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: Scaffold(
      appBar: _buildAppBar(context, ref),
      body: RefreshIndicator(
        onRefresh: () => _refresh(true),
        child: _buildList(context, ref),
      ),
    ),
  );

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Consumer(
      builder: (context, ref, _) => Text(
        "Channels".i18n +
        switch (ref.watch(_provider)) {
          AsyncData(:final value) when (
            (value != null) && (value.count > 0)
          ) => " (${value.count})",
          _ => "",
        },
      ),
    ),
    actions: [
      SearchButton(
        provider: _provider, color: Colors.grey, useChannelDatabase: true,
        onPressed: () async {
          if (!await showSearchChannelsDialog(context, _controller)) {
            return;
          }

          // Reset forced flag before redraw list.
          _controller.resetForced();

          await runWithProgress(context, _controller.search);
        },
      ),
    ],
  );

  Widget _buildList(
    BuildContext context,
    WidgetRef ref,
  ) => ChannelNotificationList(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    provider: _provider,
    cellConfig: channelNotificationCellConfigWith(context, ref, _controller),
    onError: (e, s) => showError(context, e, s),
  );

  Future<void> _checkChannelTransition(BuildContext context) async {
    root.syncSilently().then((result) {
      if (root.topPageChannelId == null) return;

      var topPageChannel = root.channelOf(root.topPageChannelId);
      if (topPageChannel == null) {
        root.clearTopPageChannelId();
        return;
      }
      Navigator.push(
        context, CupertinoPageRoute(
          builder: (__) => ChannelPage(topPageChannel),
      ));
    });
  }
}
