import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Announcement",
    "ja_jp": "お知らせ",
  } +
  {
    "en_us": "Configure subscribed user.",
    "ja_jp": "購読対象ユーザを設定",
  };

  String get i18n => localize(this, t);
}
