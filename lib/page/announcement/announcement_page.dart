import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget.dart';
import '../../dialog/subscribed_users/subscribed_users_dialog.dart';
import 'announcement_page.i18n.dart';

class AnnouncementPage extends StatelessConsumerPlrWidget {
  AnnouncementPage({ super.key });

  late final InfoRegistrySyncProvider _provider;
  late final InfoRegistrySync _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _provider = infoRegistrySyncProvider(infoRegistry);
    _controller = ref.read(_provider.notifier);
  }

  Future<void> _refresh([
      bool force = false,
  ]) => _controller.refresh(force: force);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    ref.listen(_provider, (_, next) => processError(context, next));

    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(context, ref),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _refresh,
            child: InfoList(
              key: const PageStorageKey(0),
              physics: const AlwaysScrollableScrollPhysics(),
              provider: _provider,
            ),
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Consumer(
      builder: (context, ref, _) => Text(
        "Announcement".i18n +
        switch (ref.watch(_provider)) {
          AsyncData(:final value) when (
            value.matchedEntityCount > 0
          ) => " (${value.matchedEntityCount})",
          _ => "",
        },
      ),
    ),
    actions: [
      Container(
        width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
        child: FloatingActionButton(
          heroTag: "add_subscrived_user",
          child: const Stack(
            alignment: AlignmentDirectional.topEnd,
            children: const [
              const Icon(Icons.person, size: 24),
              const Icon(Icons.settings, size: 8),
            ],
          ),
          tooltip: "Configure subscribed user.".i18n,
          onPressed: () => Navigator.push(
            context, CupertinoPageRoute(
              builder: (_) => SubscribedUsersDialog(),
              fullscreenDialog: true,
            ),
          ).then((addedUsers) => _renewAddedUsers(context, addedUsers)),
        ),
      ),
    ],
  );

  //

  Future<void> _renewAddedUsers(
    BuildContext context,
    Iterable<SubscribedUser>? addedUsers,
  ) async {
    if (addedUsers?.isNotEmpty != true) return;
    var profile; try {
      await for (var p in (await storage.root).profileWith()) {
        profile = p;
      }
    } catch (_) {
      return;
    }
    await Future.wait(
      addedUsers!.map((u) async {
          await for (var _ in u.renew(sync: false, profile: profile)) {
            _refresh();
          }
      }).toList(),
    );
  }
}
