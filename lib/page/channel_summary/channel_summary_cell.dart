import 'package:intl/intl.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:plr_util/src/channel_summary/channel_summary.i18n.dart';

import 'channel_summary_element.dart';

class ChannelSummaryCell {
  ChannelSummaryElement element;

  Schemata get schemata => element.table.notifier.schemata;
  ChannelSummaryStyleSheet get styleSheet => element.table.notifier.styleSheet;

  List<SchematicEntity>? get items => _items;

  dynamic get value => _value;

  String? get text => _text;

  dynamic get scopeValue => _scopeValue;

  dynamic get scopeUnit => _scopeUnit;

  String? get format => element.define.format!.defaultValue;
  final SummaryFormatter _formatter;

  Iterable<String> get targetClasses => _formatter.targetClasses;

  List<SchematicEntity>? _items;
  dynamic _value;
  String? _text;
  dynamic _scopeValue;
  dynamic _scopeUnit;
  SummaryVariable? _formatterResult;

  ChannelSummaryCell(this.element)
      : _formatter = SummaryFormatter(element.define.format!.defaultValue);

  void update(List<SchematicEntity> items, dynamic scopeValue,
      [dynamic scopeUnit]) {
    _items = _formatter.filter(items);
    if (_items!.isNotEmpty) {
      _items!.sort((a, b) => a.entity!.begin!.compareTo(b.entity!.begin!));
    }

    _scopeValue = scopeValue;
    _scopeUnit = scopeUnit;

    _formatterResult = _formatter.format(_items!, scopeValue);
    _value = _formatterResult?.value;
    if ((_value is DateTime) && _formatterResult!.text == null) {
      _text = _getDateFormat().format(_value);
    } else if (_value is SchemaClass) {
      _text = (_value as SchemaClass).label!.defaultValue;
    } else if ((_value is num) && !(_value is int)) {
      _text = (_value as num).toStringAsFixed(2);
    } else {
      _text = _formatterResult?.text ?? '';
    }
  }

  num? get numValue => (value is num) ? value : num.tryParse(value.toString());

  DateFormat _getDateFormat() {
    final term = styleSheet.period!;
    if (term.unit == _scopeUnit && term.amount == 1) {
      if (term.unit == SchemaPeriodUnit.Year) {
        return DateFormat('MM-dd HH:mm'.i18n);
      } else if (term.unit == SchemaPeriodUnit.Month ||
          term.unit == SchemaPeriodUnit.Week) {
        return DateFormat('dd HH:mm'.i18n);
      } else if (term.unit == SchemaPeriodUnit.Day) {
        return DateFormat('HH:mm'.i18n);
      } else if (term.unit == SchemaPeriodUnit.Hour) {
        return DateFormat('mm:ss'.i18n);
      } else {
        return DateFormat('ss'.i18n);
      }
    } else if (_scopeUnit == SchemaPeriodUnit.Year) {
      return DateFormat('yyyy'.i18n);
    } else if (_scopeUnit == SchemaPeriodUnit.Month) {
      if (term.unit == SchemaPeriodUnit.Year && term.amount > 1) {
        return DateFormat('yyyy-MM'.i18n);
      } else {
        return DateFormat('MM'.i18n);
      }
    } else if (_scopeUnit == SchemaPeriodUnit.Week ||
        _scopeUnit == SchemaPeriodUnit.Day) {
      if (term.unit == SchemaPeriodUnit.Year) {
        if (term.amount > 1) {
          return DateFormat('yyyy-MM-dd'.i18n);
        } else {
          return DateFormat('MM-dd'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Month) {
        if (term.amount > 12) {
          return DateFormat('yyyy-MM-dd'.i18n);
        } else if (term.amount > 1) {
          return DateFormat('MM-dd'.i18n);
        } else {
          return DateFormat('dd'.i18n);
        }
      } else if (_scopeUnit == SchemaPeriodUnit.Day &&
          term.unit == SchemaPeriodUnit.Week) {
        if (term.amount > 1) {
          return DateFormat('MM-dd'.i18n);
        } else {
          return DateFormat('EEEE'.i18n);
        }
      } else {
        return DateFormat('dd'.i18n);
      }
    } else if (_scopeUnit == SchemaPeriodUnit.Hour ||
        _scopeUnit == SchemaPeriodUnit.Minute) {
      if (term.unit == SchemaPeriodUnit.Year) {
        if (term.amount > 1) {
          return DateFormat('yyyy-MM-dd HH:mm'.i18n);
        } else {
          return DateFormat('MM-dd HH:mm'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Month) {
        if (term.amount > 12) {
          return DateFormat('yyyy-MM-dd HH:mm'.i18n);
        } else if (term.amount > 1) {
          return DateFormat('MM-dd HH:mm'.i18n);
        } else {
          return DateFormat('dd HH:mm'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Week) {
        if (term.amount > 1) {
          return DateFormat('MM-dd HH:mm'.i18n);
        } else {
          return DateFormat('EEEE HH:mm'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Day) {
        if (term.amount > 1) {
          return DateFormat('dd HH:mm'.i18n);
        } else {
          return DateFormat('HH:mm'.i18n);
        }
      } else {
        return DateFormat('HH:mm'.i18n);
      }
    } else if (_scopeUnit == SchemaPeriodUnit.Second) {
      if (term.unit == SchemaPeriodUnit.Year) {
        if (term.amount > 1) {
          return DateFormat('yyyy-MM-dd HH:mm:ss'.i18n);
        } else {
          return DateFormat('MM-dd HH:mm:ss'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Month) {
        if (term.amount > 12) {
          return DateFormat('yyyy-MM-dd HH:mm:ss'.i18n);
        } else if (term.amount > 1) {
          return DateFormat('MM-dd HH:mm:ss'.i18n);
        } else {
          return DateFormat('dd HH:mm:ss'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Week) {
        if (term.amount > 1) {
          return DateFormat('MM-dd HH:mm:ss'.i18n);
        } else {
          return DateFormat('EEEE HH:mm:ss'.i18n);
        }
      } else if (term.unit == SchemaPeriodUnit.Day) {
        if (term.amount > 1) {
          return DateFormat('dd HH:mm:ss'.i18n);
        } else {
          return DateFormat('HH:mm:ss'.i18n);
        }
      } else {
        return DateFormat('HH:mm:ss'.i18n);
      }
    } else {
      return DateFormat('yyyy-MM-dd HH:mm:ss'.i18n);
    }
  }

  SchematicEntity? getCurrentItem() {
    if (_items != null && _items!.isNotEmpty && _formatter.rule != null) {
      switch (_formatter.rule) {
        case SummaryVariableRule.ini:
        case SummaryVariableRule.date:
        case SummaryVariableRule.time:
        case SummaryVariableRule.unknown:
          return _items!.first;

        case SummaryVariableRule.fin:
          return _items!.last;

        default:
          break;
      }
    }
    return null;
  }

  RecordEntity? get recordEntity {
    final schemata = element.table.notifier.schemata;
    final currentItem = getCurrentItem();
    if (currentItem != null) {
      return RecordEntity.from(schemata, currentItem.entity!);
    } else {
      if (targetClasses.isEmpty) {
        return null;
      } else {
        final type =
            (targetClasses.length == 1) ? targetClasses.first : eventClass;
        final recordEntity = RecordEntity.create(schemata, type);
        if (recordEntity != null) {
          if (_scopeValue is DateTime) {
            recordEntity.begin = _scopeValue;
          } else {
            recordEntity.begin = element.table.notifier.summaryPeriod!.begin;
          }
        }
        return recordEntity;
      }
    }
  }

  void updateItem(SchematicEntity item) {
    var filtered = _formatter.filter([item]);
    if (filtered.isNotEmpty) {
      _items!.removeWhere((e) => e.entity!.nodeHash == item.entity!.nodeHash);
      _items!.add(item);
    }
  }
}
