import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText('en_us') +
      meTranslation +
      {
        'en_us': 'Channel',
        'ja_jp': 'チャネル',
      } +
      {
        'en_us': 'Schema',
        'ja_jp': 'スキーマ',
      } +
      {
        'en_us': 'Style',
        'ja_jp': 'スタイル',
      } +
      {
        'en_us': 'Select schema',
        'ja_jp': 'スキーマ選択',
      } +
      {
        'en_us': 'Me',
        'ja_jp': '自分',
      };

  String get i18n => localize(this, t);
}
