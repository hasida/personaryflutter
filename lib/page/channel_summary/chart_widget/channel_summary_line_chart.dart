import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/line_chart.dart';
import 'package:mp_chart/mp/controller/line_chart_controller.dart';
import 'package:mp_chart/mp/core/data/line_data.dart';
import 'package:mp_chart/mp/core/data_interfaces/i_line_data_set.dart';
import 'package:mp_chart/mp/core/data_set/line_data_set.dart';
import 'package:mp_chart/mp/core/entry/entry.dart';
import 'package:mp_chart/mp/core/enums/axis_dependency.dart';
import 'package:mp_chart/mp/core/enums/legend_orientation.dart';
import 'package:mp_chart/mp/core/utils/color_utils.dart';
import 'package:mp_chart/mp/core/value_formatter/default_value_formatter.dart';

import 'chart_data.dart';
import 'chart_value_formatter.dart';

class ChannelSummaryLineChart extends StatelessWidget {
  final ChartData chartData;

  ChannelSummaryLineChart({ required this.chartData });

  @override
  Widget build(BuildContext context) {
    LineChartController controller =  _createController();
    _initLineData(controller);
    return Scaffold(body: _initLineChart(controller));
  }

  LineChartController _createController() {
    return LineChartController(
      axisLeftSettingFunction: (axisLeft, controller) {
        axisLeft!
          ..setAxisMaximum(chartData.leftMax!)
          ..setAxisMinimum(chartData.leftMin!)
          ..drawLabels = false
          ..drawGridLines = false
          ..drawAxisLine = false;
      },
      axisRightSettingFunction: (axisRight, controller) {
        axisRight!
          ..setAxisMaximum(chartData.rightMax ?? 10)
          ..setAxisMinimum(chartData.rightMin ?? 10)
          ..drawLabels = false
          ..drawGridLines = false
          ..drawAxisLine = false;
      },
      xAxisSettingFunction: (xAxis, controller) {
        xAxis!
          ..drawLabels = false // メモリの表示
          ..drawGridLines = false
          ..drawAxisLine = false
          ..setAxisMaximum(
              chartData.leftChartData[0].values.length - 1.toDouble())
          ..setAxisMinimum(0);
      },
      legendSettingFunction: (legend, controller) {
        legend!
          ..enabled = true // 凡例の表示
          ..formSize = 5 // 凡例サイズ
          ..yOffset = 1
          ..xOffset = 0
          ..orientation = LegendOrientation.HORIZONTAL //凡例縦・横表示切り替え
          ..textSize = 10; //凡例の文字サイズ
      },
      drawGridBackground: false,
      dragXEnabled: false,
      dragYEnabled: false,
      highlightPerDragEnabled: false,
      scaleXEnabled: false,
      scaleYEnabled: false,
      pinchZoomEnabled: false,
      highLightPerTapEnabled: false,
      gridBackColor: ColorUtils.WHITE,
      backgroundColor: ColorUtils.WHITE,
      extraBottomOffset: 0,
      extraLeftOffset: 0,
      extraRightOffset: 0,
      extraTopOffset: 0,
      doubleTapToZoomEnabled: false,
    );
  }

  void _initLineData(LineChartController controller) async {
    List<ILineDataSet> dataSetList = [];
    chartData.leftChartData.forEach((chartData) {
      List<Entry> entryList = [];
      for (int i = 0; i < chartData.values.length; i++) {
        if (chartData.values[i] != null)
          entryList.add(Entry(
            x: i.toDouble(),
            y: chartData.values[i]?.toDouble(),
          ));
      }
      var dataSet = LineDataSet(entryList, chartData.title);
      dataSet.setAxisDependency(AxisDependency.LEFT);
      dataSet.setColor1(chartData.color);
      dataSet.setCircleColor(chartData.color);
      dataSet.setValueTextColor(chartData.color);
      dataSet.setLineWidth(1);
      dataSet.setCircleRadius(2);
      dataSet.setCircleSize(1);
      dataSet.setDrawCircleHole(false);
      if(chartData.values.nonNulls.every((value) => value is int)) {
        dataSet.setValueFormatter(IntegerValueFormatter());
      } else {
        dataSet.setValueFormatter(DefaultValueFormatter(1));
      }
      dataSetList.add(dataSet);
    });

    chartData.rightChartData.forEach((chartData) {
      List<Entry> entryList = [];
      for (int i = 0; i < chartData.values.length; i++) {
        if (chartData.values[i] != null) {
          entryList.add(Entry(
            x: i.toDouble(),
            y: chartData.values[i]?.toDouble(),
          ));
        } else {
          //entryList.add(Entry(x:i.toDouble(),y: 0.0));
        }
      }
      var dataSet = LineDataSet(entryList, chartData.title);
      dataSet.setAxisDependency(AxisDependency.RIGHT);
      dataSet.setColor1(chartData.color);
      dataSet.setCircleColor(chartData.color);
      dataSet.setValueTextColor(chartData.color);
      dataSet.setLineWidth(1);
      dataSet.setCircleRadius(2);
      dataSet.setCircleSize(1);
      dataSet.setDrawCircleHole(false);
      if(chartData.values.nonNulls.every((value) => value is int)) {
        dataSet.setValueFormatter(IntegerValueFormatter());
      } else {
        dataSet.setValueFormatter(DefaultValueFormatter(0));
      }
      dataSetList.add(dataSet);
    });
    controller.setViewPortOffsets(50, 35, 50, 25);
    controller.data = LineData.fromList(dataSetList);
  }

  Widget _initLineChart(LineChartController controller) {
    var lineChart = LineChart(controller);
    return lineChart;
  }
}
