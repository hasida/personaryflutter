import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/bar_chart.dart';
import 'package:mp_chart/mp/controller/horizontal_bar_chart_controller.dart';
import 'package:mp_chart/mp/core/adapter_android_mp.dart';
import 'package:mp_chart/mp/core/data/bar_data.dart';
import 'package:mp_chart/mp/core/data_set/bar_data_set.dart';
import 'package:mp_chart/mp/core/entry/bar_entry.dart';
import 'package:mp_chart/mp/core/enums/axis_dependency.dart';
import 'package:mp_chart/mp/core/utils/color_utils.dart';

class ChannelSummaryHorizontalBarChartHeader extends StatelessWidget {
  final double height;
  final double max;
  final double min;
  final Color color;
  final TypeFace light = TypeFace(
    fontFamily: "OpenSans", fontWeight: FontWeight.w500,
  );

  ChannelSummaryHorizontalBarChartHeader({
    required this.height,
    required this.max,
    required this.min,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    HorizontalBarChartController controller = _createController();
    _initLineData(controller);
    return Scaffold(body: _initLineChart(controller));
  }

  HorizontalBarChartController _createController() {
    return HorizontalBarChartController(
      axisLeftSettingFunction: (axisLeft, controller) {
        axisLeft!
          ..setAxisMaximum(max)
          ..setAxisMinimum(min)
          ..setLabelCount2(8, false)
          ..typeface = light
          ..textColor = color
          ..textSize = 16
          ..drawLabels = true
          ..drawTopYLabelEntry = false
          ..drawBottomYLabelEntry = false
          ..drawGridLines = false
          ..drawAxisLine = false
          ..granularityEnabled = false;
      },
      axisRightSettingFunction: (axisRight, controller) {
        axisRight!.enabled = false; // メモリの表示
      },
      xAxisSettingFunction: (xAxis, controller) {
        xAxis!.enabled = false;
      },
      legendSettingFunction: (legend, controller) {
        legend!.enabled = false;
      },
      minOffset: 0,
      extraTopOffset: (height - 20) / 2,
      extraRightOffset: 60,
      drawGridBackground: false,
      dragXEnabled: false,
      dragYEnabled: false,
      highlightPerDragEnabled: false,
      scaleXEnabled: false,
      scaleYEnabled: false,
      pinchZoomEnabled: false,
      highLightPerTapEnabled: false,
      gridBackColor: ColorUtils.WHITE,
      backgroundColor: ColorUtils.WHITE,
      drawValueAboveBar: true,
      doubleTapToZoomEnabled: false,
    );
  }

  void _initLineData(HorizontalBarChartController controller) async {
    List<BarEntry> entryList = [];
    entryList.add(BarEntry(
      x: 0,
      y: -1,
    ));
    var dataSet = BarDataSet(entryList, '');
    dataSet.setAxisDependency(AxisDependency.LEFT);
    //色を透明にしメモリに値が表示されるのを防ぐ
    dataSet.setValueTextColor(Colors.white.withOpacity(0));
    controller.data = BarData([dataSet]);
    controller.setViewPortOffsets(0, 0, 0, 0);
  }

  Widget _initLineChart(HorizontalBarChartController controller) {
    var lineChart = BarChart(controller);
    return lineChart;
  }
}
