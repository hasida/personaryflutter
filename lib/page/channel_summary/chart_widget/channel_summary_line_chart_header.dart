import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/line_chart.dart';
import 'package:mp_chart/mp/controller/line_chart_controller.dart';
import 'package:mp_chart/mp/core/adapter_android_mp.dart';
import 'package:mp_chart/mp/core/data/line_data.dart';
import 'package:mp_chart/mp/core/data_set/line_data_set.dart';
import 'package:mp_chart/mp/core/entry/entry.dart';

class ChannelSummaryLineChartHeader extends StatelessWidget {
  final double width;
  final double max;
  final double min;
  final Color color;
  final TypeFace light = TypeFace(
    fontFamily: "OpenSans", fontWeight: FontWeight.w500,
  );

  ChannelSummaryLineChartHeader({
      required this.width,
      required this.max,
      required this.min,
      required this.color,
  });

  @override
  Widget build(BuildContext context) {
    LineChartController controller = _createController();
    _initLineData(controller);
    return Scaffold(body: _initLineChart(controller));
  }

  LineChartController _createController() {
    return LineChartController(
      axisLeftSettingFunction: (axisLeft, controller) {
        axisLeft!
          ..setAxisMaximum(max)
          ..setAxisMinimum(min)
          ..setLabelCount2(6, false)
          ..textColor = color
          ..textSize = 16
          ..drawLabels = true
          ..drawGridLines = false
          ..drawAxisLine = false
          ..granularityEnabled = false;
      },
      axisRightSettingFunction: (axisRight, controller) {
        axisRight!.enabled = false;
      },
      xAxisSettingFunction: (xAxis, controller) {
        xAxis!
          ..axisMinimum = 1
          ..enabled = false;
      },
      legendSettingFunction: (legend, controller) {
        legend!.enabled = false; // 凡例の表示
      },
      drawGridBackground: false,
      dragXEnabled: false,
      dragYEnabled: false,
      highlightPerDragEnabled: false,
      scaleXEnabled: false,
      scaleYEnabled: false,
      pinchZoomEnabled: false,
      highLightPerTapEnabled: false,
      doubleTapToZoomEnabled: false,
    );
  }

  void _initLineData(LineChartController controller) async {
    List<Entry> entryList = [];
    entryList.add(Entry(
      x: 0.0,
      y: -10.0,
    ));
    var set = LineDataSet(entryList, '');
    //色を透明にしメモリに値が表示されるのを防ぐ
    set.setValueTextColor(Colors.white.withOpacity(0));
    set.setCircleColor(Colors.white.withOpacity(0));
    controller.data = LineData.fromList([set]);
    controller.setViewPortOffsets(width / 2 + 10, 35, width / 2 - 10, 25);
  }

  Widget _initLineChart(LineChartController controller) {
    var lineChart = LineChart(controller);
    return lineChart;
  }
}
