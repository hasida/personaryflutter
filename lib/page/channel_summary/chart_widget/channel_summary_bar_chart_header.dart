import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/bar_chart.dart';
import 'package:mp_chart/mp/controller/bar_chart_controller.dart';
import 'package:mp_chart/mp/core/adapter_android_mp.dart';
import 'package:mp_chart/mp/core/data/bar_data.dart';
import 'package:mp_chart/mp/core/data_set/bar_data_set.dart';
import 'package:mp_chart/mp/core/entry/bar_entry.dart';
import 'package:mp_chart/mp/core/enums/axis_dependency.dart';
import 'package:mp_chart/mp/core/utils/color_utils.dart';

class ChannelSummaryBarChartHeader extends StatelessWidget {
  final double width;
  final Color color;
  final double max;
  final double min;

  ChannelSummaryBarChartHeader({
    required this.width,
    required this.color,
    required this.max,
    required this.min,
  });

  final TypeFace light =
      TypeFace(fontFamily: "OpenSans", fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    BarChartController controller = _createController();
    _initLineData(controller);
    return Scaffold(body: _initLineChart(controller));
  }

  BarChartController _createController() {
    return BarChartController(
      axisLeftSettingFunction: (axisLeft, controller) {
        axisLeft!
          ..setAxisMaximum(max)
          ..setAxisMinimum(min)
          ..setLabelCount2(7, false)
          ..typeface = light
          ..textColor = color
          ..textSize = 16
          ..drawLabels = true
          ..drawTopYLabelEntry = true
          ..drawBottomYLabelEntry = false
          ..drawGridLines = false
          ..drawAxisLine = false
          ..granularityEnabled = false;
      },
      axisRightSettingFunction: (axisRight, controller) {
        axisRight!.enabled = false;
      },
      xAxisSettingFunction: (xAxis, controller) {
        xAxis!.enabled = false;
      },
      legendSettingFunction: (legend, controller) {
        legend!.enabled = false;
      },
      drawGridBackground: false,
      dragXEnabled: false,
      dragYEnabled: false,
      highlightPerDragEnabled: false,
      scaleXEnabled: false,
      scaleYEnabled: false,
      pinchZoomEnabled: false,
      highLightPerTapEnabled: false,
      gridBackColor: ColorUtils.WHITE,
      backgroundColor: ColorUtils.WHITE,
      doubleTapToZoomEnabled: false,
    );
  }

  void _initLineData(BarChartController controller) async {
    List<BarEntry> entryList = [];
    entryList.add(BarEntry(
      x: 0,
      y: 0,
    ));
    var dataSet = BarDataSet(entryList, '');
    dataSet.setAxisDependency(AxisDependency.LEFT);
    dataSet.setColor1(Colors.blue);
    //色を透明にしメモリに値が表示されるのを防ぐ
    dataSet.setValueTextColor(Colors.white.withOpacity(0));
    controller.data = BarData([
      dataSet,
    ])..barWidth = 0.1;
    controller.setViewPortOffsets(width / 2 + 10, 35, width / 2 - 10, 0);
  }

  Widget _initLineChart(BarChartController controller) {
    var barChart = BarChart(controller);
    return barChart;
  }
}
