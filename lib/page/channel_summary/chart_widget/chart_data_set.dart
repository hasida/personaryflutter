import 'dart:ui';

class ChartDataSet {
  final String title;
  final Color color;
  final List<num?> values;

  ChartDataSet({
    required this.title,
    required this.color,
    required this.values,
  });
}
