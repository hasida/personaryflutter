import 'chart_data_set.dart';

class ChartData {
  List<ChartDataSet> leftChartData = [];
  double? leftMax;
  double? leftMin;
  List<ChartDataSet> rightChartData = [];
  double? rightMax;
  double? rightMin;
}
