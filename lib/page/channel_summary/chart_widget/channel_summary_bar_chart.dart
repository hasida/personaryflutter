import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/bar_chart.dart';
import 'package:mp_chart/mp/controller/bar_chart_controller.dart';
import 'package:mp_chart/mp/core/data/bar_data.dart';
import 'package:mp_chart/mp/core/data_interfaces/i_bar_data_set.dart';
import 'package:mp_chart/mp/core/data_set/bar_data_set.dart';
import 'package:mp_chart/mp/core/entry/bar_entry.dart';
import 'package:mp_chart/mp/core/enums/axis_dependency.dart';
import 'package:mp_chart/mp/core/enums/legend_horizontal_alignment.dart';
import 'package:mp_chart/mp/core/enums/legend_orientation.dart';
import 'package:mp_chart/mp/core/enums/legend_vertical_alignment.dart';
import 'package:mp_chart/mp/core/utils/color_utils.dart';
import 'package:mp_chart/mp/core/value_formatter/default_value_formatter.dart';

import 'chart_data.dart';
import 'chart_value_formatter.dart';


class ChannelSummaryBarChart extends StatelessWidget {
  final ChartData chartData;

  ChannelSummaryBarChart({ required this.chartData });

  @override
  Widget build(BuildContext context) {
    BarChartController controller = createController();
    initLineData(controller);
    return Scaffold(body: _initLineChart(controller));
  }

  BarChartController createController() {
    return BarChartController(
      axisLeftSettingFunction: (axisLeft, controller) {
        axisLeft!
          ..setAxisMaximum(chartData.leftMax ?? 10)
          ..setAxisMinimum(chartData.leftMin ?? 10)
          ..setLabelCount2(7, true)
          ..drawLabels = false
          ..drawTopYLabelEntry = true
          ..drawBottomYLabelEntry = false
          ..drawGridLines = false
          ..drawAxisLine = false;
      },
      axisRightSettingFunction: (axisRight, controller) {
        axisRight!
          ..enabled = false
          ..setAxisMaximum(chartData.rightMax ?? 10)
          ..setAxisMinimum(chartData.rightMin ?? 10)
          ..drawLabels = false // メモリの表示
          ..drawGridLines = false
          ..drawAxisLine = false;
      },
      xAxisSettingFunction: (xAxis, controller) {
        xAxis!
          ..drawLabels = false // メモリの表示
          ..drawGridLines = false
          ..drawAxisLine = false;
      },
      legendSettingFunction: (legend, controller) {
        legend!
          ..enabled = true // 凡例の表示
          ..formSize = 5 // 凡例サイズ
          ..yOffset = 1
          ..xOffset = 2
          ..horizontalAlignment = LegendHorizontalAlignment.LEFT
          ..verticalAlignment = LegendVerticalAlignment.BOTTOM
          ..orientation = LegendOrientation.HORIZONTAL //凡例縦・横表示切り替え
          ..textSize = 10; //凡例の文字サイズ
      },
      drawGridBackground: false,
      dragXEnabled: false,
      dragYEnabled: false,
      highlightPerDragEnabled: false,
      scaleXEnabled: false,
      scaleYEnabled: false,
      pinchZoomEnabled: false,
      highLightPerTapEnabled: false,
      gridBackColor: ColorUtils.WHITE,
      backgroundColor: ColorUtils.WHITE,
      drawValueAboveBar: true,
      doubleTapToZoomEnabled: false,
    );
  }

  void initLineData(BarChartController controller) async {
    List<IBarDataSet> dataSetList = [];
    chartData.leftChartData.forEach((chartData) {
      List<BarEntry> entryList = [];
      if (chartData.values.nonNulls.length > 0) {
        for (int i = 0; i < chartData.values.length; i++) {
          entryList.add(BarEntry(
            x: i.toDouble(),
            y: chartData.values[i]?.toDouble() ?? 0,
          ));
        }
      }
      var dataSet = BarDataSet(entryList, chartData.title);
      dataSet.setAxisDependency(AxisDependency.LEFT);
      dataSet.setColor1(chartData.color);
      dataSet.setValueTextColor(chartData.color);
      if(chartData.values.nonNulls.every((value) => value is int)) {
        dataSet.setValueFormatter(IntegerValueFormatter());
      } else {
        dataSet.setValueFormatter(DefaultValueFormatter(1));
      }
      dataSetList.add(dataSet);
    });

    chartData.rightChartData.forEach((chartData) {
      List<BarEntry> entryList = [];
      if (chartData.values.nonNulls.length > 0) {
        for (int i = 0; i < chartData.values.length; i++) {
          entryList.add(BarEntry(
            x: i.toDouble(),
            y: chartData.values[i]?.toDouble() ?? 0,
          ));
        }
      }
      var dataSet = BarDataSet(entryList, chartData.title);
      dataSet.setAxisDependency(AxisDependency.RIGHT);
      dataSet.setColor1(chartData.color);
      dataSet.setValueTextColor(chartData.color);
      if(chartData.values.nonNulls.every((value) => value is int)) {
        dataSet.setValueFormatter(IntegerValueFormatter());
      } else {
        dataSet.setValueFormatter(DefaultValueFormatter(1));
      }
      dataSetList.add(dataSet);
    });

    controller.setViewPortOffsets(0, 35, 0, 0);
    controller.data = BarData(dataSetList)..barWidth = 0.1;
    var count =
        chartData.leftChartData.length + chartData.rightChartData.length;
    if (count > 1) {
      //複数グラフの場合、bar毎の間隔を計算で出す
      double barSpace = 0.2 * pow(0.5, count - 2);
      double groupSpace = 1 - (barSpace + 0.1) * count;
      // (bar幅+bar毎のスペース) * 個数 + グループ毎のスペース
      // 6個の場合：(0.1+0.0125) * 6 + 0.325 = 1
      // 5個の場合：(0.1+0.025) * 5 + 0.4 = 1
      // 4個の場合：(0.1+0.05) * 4 + 0.4 = 1
      // 3個の場合：(0.1+0.1) * 3 + 0.4 = 1
      // 2個の場合：(0.1+0.2) * 2 + 0.4 = 1
      controller.groupBars(-0.5 /*グラフの位置を補正*/, groupSpace, barSpace);
    }
  }

  Widget _initLineChart(BarChartController controller) {
    var lineChart = BarChart(controller);
    return lineChart;
  }
}
