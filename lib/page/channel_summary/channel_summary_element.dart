import 'package:plr_ui/plr_ui.dart';

import 'channel_summary_cell.dart';
import 'channel_summary_scope.dart';
import 'channel_summary_table.dart';

class ChannelSummaryElement {
  final ChannelSummaryTable table;

  final ChannelSummaryDefinition define;
  final List<ChannelSummaryScope> scopes = [];
  List<List<ChannelSummaryCell>>? data;

  Iterable<ChannelSummaryScope> get verticalScopes => scopes
      .where((scope) => scope.direction == SummaryScopeDirection.vertical);

  Iterable<ChannelSummaryScope> get horizontalScopes => scopes
      .where((scope) => scope.direction == SummaryScopeDirection.horizontal);

  int rowSpan = 1;
  int colSpan = 1;

  ChannelSummaryElement(this.table, this.define) {
    scopes.addAll(
        define.scopes.map((scopeDefine) => ChannelSummaryScope(scopeDefine)));
  }

  void update(List<SchematicEntity> entityList, DateTime? selectedDate) {
    var cell = ChannelSummaryCell(this);
    cell.update(entityList, null);
    data = [
      [cell]
    ];

    for (var scope in scopes) {
      data = scope.filter(data!, selectedDate);
    }
  }
}
