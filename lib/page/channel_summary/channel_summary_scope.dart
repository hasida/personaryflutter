import 'dart:math';

import 'package:collection/collection.dart';
import 'package:plr_ui/plr_ui.dart';

import 'channel_summary_cell.dart';

enum SummaryScopeDirection {
  vertical,
  horizontal,
  none,
}

final _scopeRegExp = RegExp(r'^(H|V)?(\[[%$\w-]+\])?(.*)$');
final _temporalRegExp = RegExp(r'^([0-9]*[yMwdHms])([0-9]*[yMwdHms])?$');
final _numericRegExp = RegExp(r'^\[([0-9]+) ([0-9]+) ?([0-9]+)?\]$');
final _ontologicalRegExp = RegExp(r'^\[([\w-]+)( [\w-]+)*\]$');

abstract class ChannelSummaryScope
    with HasVariablePathMixin
    implements HasVariablePath {
  final String define;
  final SummaryScopeDirection direction;
  final List<String>? path;

  ChannelSummaryScope._(this.define, this.direction, this.path);

  int get repeatCount;

  factory ChannelSummaryScope(String define) {
    final scopeMatch = _scopeRegExp.firstMatch(define);
    if (scopeMatch == null) {
      return DummyScope(define);
    }

    SummaryScopeDirection direction;
    switch (scopeMatch.group(1)) {
      case 'H':
        direction = SummaryScopeDirection.horizontal;
        break;
      case 'V':
        direction = SummaryScopeDirection.vertical;
        break;
      default:
        direction = SummaryScopeDirection.none;
        break;
    }

    List<String> variable;
    if (scopeMatch.group(2) != null) {
      variable = HasVariablePathMixin.loadPath(scopeMatch.group(2)!);
    } else variable = const [];

    String range = scopeMatch.group(3)!;

    final temporalMatch = _temporalRegExp.firstMatch(range);
    if (temporalMatch != null) {
      return ChannelSummaryTemporalScope.from(
        define,
        direction,
        variable,
        temporalMatch.group(1)!,
        temporalMatch.group(2),
      );
    }

    final numericMatch = _numericRegExp.firstMatch(range);
    if (numericMatch != null) {
      final startStr = numericMatch.group(1);
      final endStr = numericMatch.group(2);
      final stepStr = numericMatch.group(3);
      if (startStr == null || endStr == null) {
        return DummyScope(define);
      }
      final start = double.parse(startStr);
      final end   = double.parse(endStr);
      final step  = (stepStr != null) ? double.parse(stepStr) : null;
      return ChannelSummaryNumericScope(
        define,
        direction,
        variable,
        start,
        end,
        step,
      );
    }

    final ontologicalMatch = _ontologicalRegExp.firstMatch(range);
    if (ontologicalMatch != null) {
      final classes = range
          .substring(1, range.length - 1)
          .split(' ')
          .toList(growable: false);
      return ChannelSummaryOntologicalScope(
        define,
        direction,
        variable,
        classes,
      );
    }

    return DummyScope(define);
  }

  List<List<ChannelSummaryCell>> filter(
      List<List<ChannelSummaryCell>> data, DateTime? selectedDate) {
    if (data.isEmpty || !data.any((row) => row.length > 0)) {
      /// 入力データが無かった場合
      return [<ChannelSummaryCell>[]];
    }

    var repeatCount = 0;
    final tempResult = <List<List<ChannelSummaryCell>>>[];
    for (var row in data) {
      final rowResult = <List<ChannelSummaryCell>>[];
      for (var cell in row) {
        DateTime? dateTime =
            (cell.scopeValue is DateTime) ? cell.scopeValue : selectedDate;
        final ret = _filter(cell, dateTime);
        rowResult.add(ret);
        if (ret.length > repeatCount) {
          repeatCount = ret.length;
        }
      }
      tempResult.add(rowResult);
    }

    var rowCount = data.length;
    var columnCount = data.map((e) => e.length).reduce(max);
    if (direction == SummaryScopeDirection.vertical) {
      rowCount *= repeatCount;
    } else if (direction == SummaryScopeDirection.horizontal) {
      columnCount *= repeatCount;
    }
    final List<List<ChannelSummaryCell?>> result = <List<ChannelSummaryCell?>>[];
    for (var i = 0; i < rowCount; ++i) {
      result.add(List<ChannelSummaryCell?>.filled(columnCount, null));
    }

    for (var rowIndex = 0; rowIndex < data.length; ++rowIndex) {
      final row = tempResult[rowIndex];
      for (var colIndex = 0; colIndex < row.length; ++colIndex) {
        final ret = row[colIndex];
        if (direction == SummaryScopeDirection.none) {
          result[rowIndex][colIndex] = ret.first;
        } else {
          for (var i = 0; i < ret.length; ++i) {
            if (direction == SummaryScopeDirection.vertical) {
              result[rowIndex * repeatCount + i][colIndex] = ret[i];
            } else {
              assert(direction == SummaryScopeDirection.horizontal);
              result[rowIndex][colIndex * repeatCount + i] = ret[i];
            }
          }
        }
      }
    }

    for (var rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
      for (var columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
        result[rowIndex][columnIndex] ??=
            ChannelSummaryCell(data.first.first.element);
      }
    }

    for (var i = 0; i < result.length; i++) {
      result[i] = result[i].cast<ChannelSummaryCell>();
    }
    return result.cast<List<ChannelSummaryCell>>();
  }

  List<ChannelSummaryCell> _filter(
      ChannelSummaryCell data, DateTime? selectedDate);
}

class DummyScope extends ChannelSummaryScope {
  DummyScope(String define) : super._(define, SummaryScopeDirection.none, null);

  @override
  int get repeatCount => 1;

  @override
  List<ChannelSummaryCell> _filter(
          ChannelSummaryCell data, DateTime? selectedDate) =>
      [data];
}

class ChannelSummaryTemporalScope extends ChannelSummaryScope {
  final SchemaPeriod? term;
  final SchemaPeriod? period;
  final int _repeatCount;

  int get repeatCount => _repeatCount;

  SchemaPeriodUnit get scopeUnit => period?.unit ?? term!.unit;

  ChannelSummaryTemporalScope(
    String define,
    SummaryScopeDirection direction,
    List<String> variable,
    this.term,
    this.period,
  )   : _repeatCount = _calcRepeatCount(term, period, direction),
        super._(define, direction, variable);

  factory ChannelSummaryTemporalScope.from(
    String define,
    SummaryScopeDirection direction,
    List<String> variable,
    String termStr,
    String? periodStr,
  ) {
    SchemaPeriod? term =
        SchemaPeriod.fromString((termStr.length > 1) ? termStr : '1$termStr');

    SchemaPeriod? period;
    if (periodStr != null) {
      period = SchemaPeriod.fromString(periodStr);
    }
    return ChannelSummaryTemporalScope(
        define, direction, variable, term, period);
  }

  static int _calcRepeatCount(
      SchemaPeriod? term, SchemaPeriod? period, SummaryScopeDirection direction) {
    if (period != null) {
      return (term!.asSecond().toDouble() / period.asSecond().toDouble()).ceil();
    } else if (direction == SummaryScopeDirection.none) {
      return 1;
    } else {
      return -1;
    }
  }

  @override
  List<ChannelSummaryCell> _filter(
      ChannelSummaryCell data, DateTime? selectedDate) {
    final termValue = SummaryPeriod.fromSchemaPeriod(term!, selectedDate!);

    final result = <ChannelSummaryCell>[];
    if (period == null && direction != SummaryScopeDirection.none) {
      result.addAll(filterItems(data.items!)
          .where((item) => _checkInRange(termValue, item))
          .map((item) {
        final cell = ChannelSummaryCell(data.element);
        cell.update([item], termValue.begin, scopeUnit);
        return cell;
      }));
    } else {
      var periodValue = (period == null)
          ? termValue
          : SummaryPeriod.fromSchemaPeriod(period!, termValue.begin, true);
      do {
        if (periodValue.end.compareTo(termValue.end) > 0) {
          periodValue = SummaryPeriod(periodValue.begin, termValue.end);
        }
        final cell = ChannelSummaryCell(data.element);
        final items = filterItems(data.items!)
            .where((item) => _checkInRange(periodValue, item))
            .toList(growable: false);
        cell.update(items, periodValue.begin, scopeUnit);
        result.add(cell);
      } while (
          period != null && (
            (periodValue = periodValue.add(period!))
              .begin.compareTo(termValue.end) <= 0
            )
        );
    }
    return result;
  }

  bool _checkInRange(SummaryPeriod periodValue, SchematicEntity item) {
    DateTime? targetValue;
    if (propPath.isEmpty ||
        (propPath.length == 1 && propPath.first == '%begin')) {
      targetValue = item.entity!.begin;
    } else {
      final val = getVariable(propPath, item)!.firstOrNull?.value;
      if (val == null || !(val is DateTime)) {
        return false;
      }
      targetValue = val;
    }
    return periodValue.contains(targetValue!);
  }
}

extension SchemaPeriodExt on SchemaPeriod {
  int asSecond() {
    switch (unit) {
      case SchemaPeriodUnit.Year:
        return amount * 365 * 24 * 60 * 60;
      case SchemaPeriodUnit.Month:
        return amount * 31 * 24 * 60 * 60;
      case SchemaPeriodUnit.Week:
        return amount * 7 * 24 * 60 * 60;
      case SchemaPeriodUnit.Day:
        return amount * 24 * 60 * 60;
      case SchemaPeriodUnit.Hour:
        return amount * 60 * 60;
      case SchemaPeriodUnit.Minute:
        return amount * 60;
      case SchemaPeriodUnit.Second:
        return amount;
      default:
        return -1;
    }
  }
}

class ChannelSummaryNumericScope extends ChannelSummaryScope {
  final double start;
  final double end;
  final double? step;
  final int _repeatCount;

  int get repeatCount => _repeatCount;

  bool get isInt =>
      (start == start.ceil()) && (end == end.ceil()) &&
      (step == null || step == step!.ceil());

  Type get scopeUnit => isInt ? int : double;

  bool isDecimalDataType = false;

  ChannelSummaryNumericScope(
    String define,
    SummaryScopeDirection direction,
    List<String> variable,
    this.start,
    this.end,
    this.step,
  )   : _repeatCount = _calcRepeatCount(start, end, step),
        super._(define, direction, variable);

  static int _calcRepeatCount(double start, double end, double? step) =>
      ((end - start) / (step ?? 1)).ceil();

  @override
  List<ChannelSummaryCell> _filter(
      ChannelSummaryCell data, DateTime? selectedDate) {
    final result = <ChannelSummaryCell>[];
    var cur = start;
    do {
      final seq = <double>[cur];
      if (direction == SummaryScopeDirection.none) {
        for (var i = cur + (step ?? 1); i <= end; i += (step ?? 1)) {
          seq.add(i);
        }
      }

      var cell = ChannelSummaryCell(data.element);

      final items = filterItems(data.items!).where((item) {
        if (propPath.isEmpty) {
          /// variable指定がなかった場合アイテムは入れない
          return false;
        } else {
          final value = _getValue(item);
          if (value == null) {
            return false;
          }
          return seq.any((cur) {
            if (isDecimalDataType) {
              if (step != null) {
                return (cur <= value && value < (cur + 1));
              } else {
                return (start <= value && value <= end);
              }
            } else {
              return (cur <= value && value < (cur + 1));
            }
          });
        }
      }).toList();

      /// start/end/stepすべて整数ならscopeValueをintにする
      final scopeValue = isInt ? cur.toInt() : cur;

      cell.update(items, scopeValue, scopeUnit);
      result.add(cell);

      if (direction == SummaryScopeDirection.none) {
        break;
      }
      if (step == null && isDecimalDataType) {
        break;
      }

      cur += step ?? 1;
    } while ((step == null || step! > 0) ? (cur <= end) : (cur >= end));

    return result;
  }

  num? _getValue(item) {
    final result = getVariable(propPath, item)!.firstOrNull;
    final val = result?.value;
    if (val == null) {
      return null;
    }

    final value = (val is num) ? val : num.tryParse(val);
    if (value == null) {
      return null;
    }

    isDecimalDataType |=
        result!.props!.any((prop) => !prop.range!.isIntegerDatatype);
    return value;
  }
}

class ChannelSummaryOntologicalScope extends ChannelSummaryScope {
  final List<String> classes;

  @override
  int get repeatCount => classes.length;

  ChannelSummaryOntologicalScope(
    String define,
    SummaryScopeDirection direction,
    List<String> variable,
    this.classes,
  ) : super._(define, direction, variable);

  @override
  List<ChannelSummaryCell> _filter(
      ChannelSummaryCell data, DateTime? selectedDate) {
    if (direction == SummaryScopeDirection.none) {
      final items = data.items!
          .where((item) => _checkInRange(item, classes))
          .toList(growable: false);

      data.update(items, classes.map((e) => data.schemata.classOf(e)));

      return [data];
    } else {
      final result = <ChannelSummaryCell>[];
      for (var clazz in classes) {
        final cell = ChannelSummaryCell(data.element);
        final items = data.items!
            .where((item) => _checkInRange(item, [clazz]))
            .toList(growable: false);
        cell.update(items, data.schemata.classOf(clazz));
        result.add(cell);
      }
      return result;
    }
  }

  bool _checkInRange(SchematicEntity item, Iterable<String> range) {
    if (propPath.isEmpty) {
      return range.any((clazz) => item.type.id == clazz);
    } else {
      final value = getVariable(propPath, item)!.firstOrNull?.value;
      if (value is SchematicEntity) {
        return range.any((clazz) => value.type.id == clazz);
      } else if (value is SchemaClass) {
        return range.any((clazz) => value.id == clazz);
      } else {
        return range.any((clazz) => value.toString() == clazz);
      }
    }
  }
}
