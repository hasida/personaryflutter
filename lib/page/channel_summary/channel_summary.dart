import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:plr_util/src/channel_summary/channel_summary.i18n.dart';
import 'package:provider/provider.dart' as p;

import '../../../util/show_message.dart';
import 'channel_summary_border_widget.dart';
import 'channel_summary_cell.dart';
import 'channel_summary_color.dart';
import 'channel_summary_element.dart';
import 'channel_summary_notifier.dart';
import 'channel_summary_page.dart';
import 'channel_summary_position.dart';
import 'channel_summary_table.dart';
import 'chart_widget/channel_summary_bar_chart.dart';
import 'chart_widget/channel_summary_bar_chart_header.dart';
import 'chart_widget/channel_summary_bar_chart_v.dart';
import 'chart_widget/channel_summary_bar_chart_v_header.dart';
import 'chart_widget/channel_summary_line_chart.dart';
import 'chart_widget/channel_summary_line_chart_header.dart';
import 'chart_widget/chart_data.dart';
import 'chart_widget/chart_data_set.dart';

const double DEFAULT_CELL_HEIGHT = 40;
const double DEFAULT_CELL_WIDTH = 80;
const int CHANNEL_SUMMARY_CHART_WIDTH = 6;
const double APPBAR_BOTTOM_FONT_SIZE = 12;

class SummaryDataMap {
  final Map<ChannelSummaryPosition, Widget> tableCellMap = {};
}

class ChannelSummary extends ChannelSummaryPageContentBase {
  ChannelSummary(
    super.account,
    super.channel,
    super.schemataSelectionValue,
    super.schemataSelectionController, {
      super.key,
  });

  final _summaryDataMapProvider = Provider((_) => SummaryDataMap());

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final schemata = schemataSelectionValue.selectedSchemata;
    final styleSheet = schemataSelectionValue.selectedStyleSheet;

    if (
      (schemata == null) ||
      (styleSheet is! ChannelSummaryStyleSheet) ||
      (schemata.timelineState == TimelineState.Disabled)
    ) return Container();

    final selectedDate = useRef<DateTime?>(null);

    return p.ChangeNotifierProvider<ChannelSummaryNotifier>(
      key: Key(
        "channel_summary_${channel.id}_${schemata.hashCode}_${styleSheet.id}",
      ),
      create: (_) {
        final notifier = ChannelSummaryNotifier(
          channel,
          schemata,
          styleSheet,
        );
        if (selectedDate.value != null) {
          notifier.selectedDate = selectedDate.value!;
        } else {
          notifier.update();
        }
        return notifier;
      },
      child: p.Consumer<ChannelSummaryNotifier>(
        builder: (context, notifier, child) => Scaffold(
          backgroundColor: Colors.white,
          appBar: createAppBar(context, styleSheet, selectedDate, notifier),
          body: Column(
            children: [
              _createSummaryWidget(context, ref, notifier.tables),
            ],
          ),
        ),
      ),
    );
  }

  /// AppBarのBottom部のテキスト高さ取得
  double _getAppBarBottomTextHeight(BuildContext context) {
    TextSpan ts = new TextSpan(
        text: 'No data'.i18n,
        style: Theme.of(context).textTheme.bodyLarge!.merge(const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w400,
            fontFamily: "Roboto",
            fontStyle: FontStyle.normal,
            fontSize: APPBAR_BOTTOM_FONT_SIZE)));
    TextPainter tp =
        new TextPainter(text: ts, textDirection: TextDirection.ltr);
    tp.layout();
    var textScale = MediaQuery.textScalerOf(context).scale(1.0);
    if (textScale < 1.0) textScale = 1.0;
    return tp.height * textScale;
  }

  /// カレンダーによる日付選択処理
  Future<DateTime> selectDate(
      BuildContext context, DateTime initialDate) async {
    final DateTime? selected = await showDatePicker(
            context: context,
            initialDate: initialDate,
            firstDate: DateTime(2015),
            lastDate: DateTime(2037))
        .catchError((e, s) {
          showError(context, e, s);
          return null;
        });
    if (selected == null) {
      return initialDate;
    }
    return selected;
  }

  PreferredSizeWidget createAppBar(BuildContext context, StyleSheet<SchemaObject> styleSheet,
      ObjectRef<DateTime?> selectedDate, ChannelSummaryNotifier summaryNotifier) {
    return AppBar(
      backgroundColor: Colors.white,
      titleSpacing: 0,
      title: GestureDetector(
        /// ソフトキーボードを閉じる
        onTap: () => FocusScope.of(context).unfocus(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            /// タイトル
            Text((channel.name?.defaultValue ?? " "),
                style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto",
                  fontStyle: FontStyle.normal,
                  fontSize: 18.0,
                ),
                textAlign: TextAlign.left),
          ],
        ),
      ),

      /// backボタン
      leading: IconButton(
        icon: const Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.of(context).pop(),
      ),
      actions: <Widget>[
        /// プログレスインジケータ
        if (summaryNotifier.state != NotifierState.finished)
          const Center(
            child: const CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
          )),
        const SizedBox(width: 5.0),

        /// カレンダーアイコン
        GestureDetector(
            child: const Icon(Icons.calendar_today, color: Colors.black),
            onTap: () {
              /// ソフトキーボードを閉じる
              FocusScope.of(context).unfocus();
              var result = selectDate(context, summaryNotifier.selectedDate);
              result.then((value) {
                if(summaryNotifier.selectedDate != value)
                  summaryNotifier.selectedDate = selectedDate.value = value;
              });
            }),
        const SizedBox(width: 5.0)
      ],

      /// アイテム日付とスタイルシート
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(
            APPBAR_BOTTOM_FONT_SIZE * MediaQuery.textScalerOf(context).scale(1.0) +
                50),
        child: Column(children: [
          GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Padding(
                padding:
                    const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 1.0),
                child: Column(
                  children: [
                    Row(
                      children: <Widget>[
                        /// 表示対象者
                        Consumer(
                          builder: (_, ref, __) {
                            final userInfo = ref.watch(
                              userInfoUpdateProvider(
                                channel, labelOfMe: defaultLabelOfMe,
                              ),
                            ).value;

                            return Container(
                              padding: const EdgeInsets.only(left: 48),
                              child: Text(
                                (userInfo?.name ?? channel.plrId?.email ?? ' '),
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Roboto",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 10,
                                ),
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                              ),
                            );
                          },
                        ),
                        Expanded(
                          child: Container(
                            height: _getAppBarBottomTextHeight(context),
                          ),
                        ),

                        /// スタイルシート
                        GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            showSchemaStyleSelectDialog(context);
                          },
                          child: Container(
                            height: _getAppBarBottomTextHeight(context),
                            padding: const EdgeInsets.only(left: 1, right: 1),
                            child: Text((styleSheet.label!.defaultValue ?? " "),
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Roboto",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 10,
                                ),
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left),
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
          ),
          Container(
            height: 50,
            color: Colors.black12,
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
                    onPressed: () {
                      summaryNotifier.setPrevPeriod();
                      selectedDate.value = summaryNotifier.selectedDate;
                    }),
                Expanded(
                  child: Center(
                    child: AutoSizeText(
                        summaryNotifier.summaryPeriodString ?? '',
                        maxLines: 1, minFontSize: 1,),
                  ),
                ),
                RotatedBox(
                  quarterTurns: 2,
                  child: IconButton(
                      icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
                      onPressed: () {
                        summaryNotifier.setNextPeriod();
                        selectedDate.value = summaryNotifier.selectedDate;
                      }),
                )
              ],
            ),
          ),
        ]),
      ),
      elevation: 10,
    );
  }

  /// 画面に表示するWidgetを作成する
  Widget _createSummaryWidget(
    BuildContext context,
    WidgetRef ref,
    List<ChannelSummaryTable> tables,
  ) {
    List<Widget> list = [];
    tables.forEach((element) {
      list.add(_createWidget(context, ref, element));
    });

    /// Schemata読み込み途中
    return Expanded(
        child: RefreshIndicator(
            onRefresh: () async {
              context.read<ChannelSummaryNotifier>().update();
            },
            child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: list,
                        ))))));
  }

  ///table内の各Widgetを作成し、tableCellMapに保持する
  Widget _createWidget(
    BuildContext context,
    WidgetRef ref,
    ChannelSummaryTable table,
  ) {
    ref.read(_summaryDataMapProvider).tableCellMap.clear();
    //チャート以外のWidgetを作成
    _createNormalWidget(context, ref, table);
    //チャートWidgetを作成
    _createChartWidget(context, ref, table);
    return _deployWidget(context, ref, table);
  }

  ///チャート以外のWidgetを作成する
  _createNormalWidget(
    BuildContext context,
    WidgetRef ref,
    ChannelSummaryTable table,
  ) {
    var tableCellMap = ref.read(_summaryDataMapProvider).tableCellMap;
    //チャート以外のElementを取得
    List<ChannelSummaryElement> tableElements = table.elements
        .where((element) => (element.define.rule == StyleSheetRule.Bordered ||
            element.define.rule == StyleSheetRule.None))
        .toList();
    tableElements.forEach((element) {
      List<Widget> columnChildren = [];
      //elementの縦幅、横幅を求める
      var height = _getHeight(table, element);
      var width = _getWidth(context, table, element);
      element.data?.forEach((cells) {
        List<Widget> rowChildren = [];
        cells.forEach((channelSummaryCell) {
          rowChildren.add(_createCell(
              context,
              channelSummaryCell,
              ChannelSummaryColor.parseColor(element.define.color),
              width,
              height,
              element.define.align,
              element.define.rule == StyleSheetRule.Bordered,
              element.data!.first == cells && _isTopBorder(table, element),
              channelSummaryCell == cells.last &&
                  _isRightBorder(table, element),
              _isBottomBorder(table, element)));
        });
        columnChildren.add(Row(
          children: rowChildren,
        ));
      });
      tableCellMap[ChannelSummaryPosition(
          element.define.line, element.define.column)] = Column(
        children: columnChildren,
      );
    });
  }

  ///Cellに入るWidgetを作成する[text]に表示文字列、罫線を引く場合は[hasBorder]をtrueにする、
  ///隣接Cellとの罫線の重複を防ぐため、上部の罫線は[isTop]、右部の罫線は[isRight]がtrueの場合のみ設定する
  Widget _createCell(
    BuildContext context,
    ChannelSummaryCell cell,
    Color? color,
    double width,
    double height,
    StyleSheetAlign? align,
    bool hasBorder,
    bool isTop,
    bool isRight,
    bool isBottom,
  ) {
    final alignment = switch (align) {
      StyleSheetAlign.Left => Alignment.centerLeft,
      StyleSheetAlign.Top => Alignment.topCenter,
      StyleSheetAlign.Right => Alignment.centerRight,
      StyleSheetAlign.Bottom => Alignment.bottomCenter,
      _ => Alignment.center,
    };
    final _userPlrId = channel.masterStorage.plrId.toString();

    Widget builder(String? text) {
      final recordEntity = cell.recordEntity;
      final _isCreator = (recordEntity == null)
        ? false : (_userPlrId == (recordEntity.creator ?? _userPlrId));
      final _readOnly = schemataSelectionValue.selectedSchemata
        ?.timelineState != TimelineState.Writable;
      final _hideDialog = _readOnly
        ? (recordEntity == null || cell.getCurrentItem() == null) : false;

      return ChannelSummaryBorderWidget(
        height: height,
        width: width,
        left: hasBorder,
        bottom: hasBorder && isBottom,
        top: hasBorder && isTop,
        right: hasBorder && isRight,
        padding: const EdgeInsets.only(
          left: 1.0, right: 1.0, top: 1.0, bottom: 1.0,
        ),
        alignment: alignment,
        onTap: () => _hideDialog ? null : showDialog(
          context: context,
          builder: (_) => EntityDialog(
            account, EntityDialogSettings(), channel, recordEntity!,
            _readOnly, _isCreator,
          ),
        ).then((value) {
            if (value == null) {
              cell.updateItem(value);
              context.read<ChannelSummaryNotifier>().update();
            }
          }
        ),
        child: AutoSizeText(
          text ?? "", minFontSize: 1,
          style: TextStyle(
            color: color, fontSize: 16, decoration: TextDecoration.none,
          ),
        ),
      );
    }

    var text = cell.text;
    if (
      (text == null) ||
      (!text.contains("[&subject") && !text.contains("[googleDrive:"))
    ) return builder(text);

    return Consumer(
      builder: (_, ref, __) {
        final userInfo = ref.watch(
          userInfoUpdateProvider(channel, labelOfMe: defaultLabelOfMe),
        ).value;
        text = text!.replaceAll(
          "[&subject%Name]", userInfo?.name ?? channel.plrId?.toString() ?? "",
        );

        final userNames = Map<String, String>.fromIterable(
          RegExp(r'\[(googleDrive:.+)\]').allMatches(text!),
          key: (m) => m.group(0), value: (m) {
            final id = m.group(1);
            if (id == _userPlrId) return defaultLabelOfMe(null);

            final u = channel.disclosedToUserOf(PlrId.fromStringOrNull(id));
            if (u == null) return "";

            return ref.watch(
              userInfoUpdateProvider(u, labelOfMe: defaultLabelOfMe),
            ).value?.name ?? "";
          },
        );
        for (final e in userNames.entries) {
          text = text!.replaceAll(e.key, e.value);
        }
        return builder(text);
      },
    );
  }

  ///[element]の横幅を返却する
  double _getWidth(BuildContext context, ChannelSummaryTable table,
      ChannelSummaryElement element) {
    var width = _getElementWidth(context, table, element);
    return _getWidthInternal(
        context, table, element.define.line, element.define.column, width);
  }

  double _getElementWidth(BuildContext context, ChannelSummaryTable table,
      ChannelSummaryElement targetElement) {
    var sameLineList = table.elements
        .where((conditions) =>
            conditions.define.column == targetElement.define.column)
        .toList();
    var maxWidth = 0.0;
    sameLineList.forEach((element) {
      var width;
      if (element.define.width == null || element.define.width!.isEmpty) {
        width = DEFAULT_CELL_WIDTH;
      } else if (element.define.width!.contains('em')) {
        TextSpan ts = const TextSpan(
            text: 'W',
            style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.none));
        TextPainter tp =
            new TextPainter(text: ts, textDirection: TextDirection.ltr);
        tp.layout();
        var oneCharacterWidth =
            tp.width * MediaQuery.textScalerOf(context).scale(1.0);
        width = double.parse(element.define.width!.replaceAll('em', '')) *
            oneCharacterWidth;
      } else {
        width = double.parse(element.define.width!) * DEFAULT_CELL_WIDTH;
      }
      if (maxWidth < width) maxWidth = width;
    });
    return maxWidth;
  }

  double _getWidthInternal(BuildContext context, ChannelSummaryTable table,
      int? line, int? column, double maxWidth) {
    var rightFirstElement = table.elements.firstWhereOrNull(
        (conditions) =>
            conditions.define.line == line &&
            conditions.define.column == (column! + 1));
    if (rightFirstElement != null) {
      // 右隣のElementがある場合、そのままmaxWidthを返す
      return maxWidth;
    } else {
      // 右隣が空白
      var rightColumnFirstElement = table.elements
          .where((conditions) => conditions.define.column == column! + 1);
      if (rightColumnFirstElement.length == 0) {
        //右隣の列に1つもElementがない場合

        var rightColumnFirstElement2 = table.elements.firstWhereOrNull(
            (conditions) => conditions.define.column! > column! + 1);
        if (rightColumnFirstElement2 != null) {
          return _getWidthInternal(context, table, line, column! + 1, maxWidth);
        } else {
          return maxWidth;
        }
      } else {
        //右隣の列に一つでもElementがある場合は、widthを拡張
        //最大幅を求める
        var max = 0.0;
        rightColumnFirstElement.forEach((element) {
          var w = _getElementWidth(context, table, element);
          w = w *
              ((element.data != null && element.data!.isNotEmpty)
                  ? element.data![0].length
                  : 1);
          if (max < w) max = w;
        });
        maxWidth = maxWidth + max;
        //更に右隣列の状態を確認しwidthを決定する
        return _getWidthInternal(context, table, line, column! + 1, maxWidth);
      }
    }
  }

  ///[element]の縦幅を返却する
  double _getHeight(ChannelSummaryTable table, ChannelSummaryElement element) {
    var height = DEFAULT_CELL_HEIGHT;
    if (element.data != null &&
        element.data!.isNotEmpty &&
        element.data!.length == 1) {
      //tableのelementを検索し同じ行の最大縦幅を求める
      List<ChannelSummaryElement> sameLineElements = table.elements
          .where((conditions) => conditions.define.line == element.define.line)
          .toList();
      sameLineElements.sort((a, b) => b.data!.length.compareTo(a.data!.length));
      //縦幅はDEFAULT_CELL_HEIGHT固定なのでDEFAULT_CELL_HEIGHT*列数が縦幅になる
      height = DEFAULT_CELL_HEIGHT * sameLineElements.first.data!.length;
      //下のオブジェクト数分伸ばす
      height = _getHeightInternal(table, element.define.line,
          element.define.column, DEFAULT_CELL_HEIGHT, height);
    }
    return height;
  }

  double _getHeightInternal(ChannelSummaryTable table, int? line, int? column,
      double defaultHeight, double maxHeight) {
    var rightFirstElement = table.elements.firstWhereOrNull(
        (conditions) =>
            conditions.define.column == column &&
            conditions.define.line == (line! + 1));
    if (rightFirstElement != null) {
      // 下のElementがある場合、そのままmaxWidthを返す
      return maxHeight;
    } else {
      // 下がElementが空白
      var rightColumnFirstElement = table.elements.firstWhereOrNull(
          (conditions) => conditions.define.line == line! + 1);
      if (rightColumnFirstElement == null) {
        //下の行に1つもElementがない場合
        return maxHeight;
      } else {
        rightColumnFirstElement = table.elements.firstWhereOrNull(
            (conditions) =>
                conditions.define.line == line! + 1 &&
                conditions.define.column! < column!);
        if (rightColumnFirstElement != null) {
          //下のElementの左側に一つでもElementがある場合は、widthを拡張しない
          return maxHeight;
        } else {
          //更に下行の左側にElementがない場合、widthを拡張し、更に下の行を調べる
          maxHeight = maxHeight + defaultHeight;
          return _getHeightInternal(
              table, line! + 1, column, defaultHeight, maxHeight);
        }
      }
    }
  }

  ///チャートWidgetを作成する
  _createChartWidget(
    BuildContext context,
    WidgetRef ref,
    ChannelSummaryTable table,
  ) {
    var tableCellMap = ref.read(_summaryDataMapProvider).tableCellMap;
    //チャートのElementを取得
    List<ChannelSummaryElement> chartElements = table.elements
        .where((element) => (element.define.rule == StyleSheetRule.LineGraph ||
            element.define.rule == StyleSheetRule.BarGraph))
        .toList();
    List<ChannelSummaryElement> graphDataElements = table.elements
        .where((element) => element.define.rule == StyleSheetRule.GraphData)
        .toList();
    var headerLine;
    var headerColumn;
    var chartLine;
    var chartColumn;
    ChartData chartData = ChartData();

    graphDataElements.forEach((dataElement) {
      if (dataElement.data == null) return;
      List<num?> values = [];
      double? max, min;
      var currentDataColumn = dataElement.define.column;
      var currentDataLine = dataElement.define.line;
      var isHorizontal = dataElement.data!.length > 1;
      var chartElement = isHorizontal
          ? chartElements.firstWhere(
              (element) => element.define.column == currentDataColumn)
          : chartElements
              .firstWhere((element) => element.define.line == currentDataLine);
      var rule = chartElement.define.rule;
      if (headerLine == null) {
        //ヘッダー（メモリ）、チャートを表示する位置を覚えておく
        headerLine = chartElement.define.line;
        headerColumn = chartElement.define.column;
        chartLine = currentDataLine;
        chartColumn = currentDataColumn;
      }

      //チャートに表示するデータをvaluesに格納、同時に最大値最小値を求める
      if (isHorizontal) {
        dataElement.data!.forEach((dataList) {
          var value = dataList[0].value;
          if (value == null) {
            values.add(null);
          } else {
            value = value as num;
            values.add(value);
            if (max == null || max! < value) max = value.toDouble();
            if (min == null || min! > value) min = value.toDouble();
          }
        });
      } else {
        dataElement.data![0].forEach((cell) {
          var value = cell.numValue;
          if (value == null) {
            values.add(null);
          } else {
            values.add(value);
            if (max == null || max! < value) max = value.toDouble();
            if (min == null || min! > value) min = value.toDouble();
          }
        });
      }

      //左右どちらのメモリにデータを割り当てるか決定する
      if (chartData.leftChartData.length == 0) {
        //1個目のデータのみ上側のメモリとする
        chartData.leftChartData.add(ChartDataSet(
            title: chartElement.data![0][0].value ?? "",
            color: ChannelSummaryColor.parseColor(chartElement.define.color) ?? Colors.black,
            values: values));
        chartData.leftMax = max;
        chartData.leftMin = min;
      } else {
        //2個目のデータ以降は下側のメモリとする
        chartData.rightChartData.add(ChartDataSet(
            title: chartElement.data![0][0].value ?? "",
            color: ChannelSummaryColor.parseColor(chartElement.define.color) ?? Colors.black,
            values: values));
        if(max != null && min != null) {
          if (chartData.rightMax == null || chartData.rightMax! < max!)
            chartData.rightMax = max;
          if (chartData.rightMin == null || chartData.rightMin! > min!)
            chartData.rightMin = min;
        }
      }

      var isLastChartData = isHorizontal
          ? chartElements.indexWhere((element) =>
                  element.define.column == (currentDataColumn! + 1)) ==
              -1
          : chartElements.indexWhere(
                  (element) => element.define.line == (currentDataLine! + 1)) ==
              -1;

      if (isLastChartData) {
        var leftVisible = true;
        if (chartData.leftMax == null) {
          chartData.leftMax = 0;
          chartData.leftMin = 0;
          leftVisible = false;
        }
        if (chartData.rightMax == null) {
          chartData.rightMax = 0;
          chartData.rightMin = 0;
        }

        var leftAbs = (chartData.leftMax! - chartData.leftMin!).abs();
        var rightAbs = (chartData.rightMax! - chartData.rightMin!).abs();
        if(leftAbs < 6) {
          chartData.leftMax = chartData.leftMax! + (6 - leftAbs) / 2;
          chartData.leftMin = chartData.leftMin! - (6 - leftAbs) / 2;
        }
        if(rightAbs < 6) {
          chartData.rightMax = chartData.rightMax! + (6 - rightAbs) / 2;
          chartData.rightMin = chartData.rightMin! - (6 - rightAbs) / 2;
        }

        //線グラフの場合はグラフが重ならないように位置をずらす
        if (chartData.rightMax != null && chartData.rightMin != null) {
          double leftInterval = (chartData.leftMax! - chartData.leftMin!) /
              CHANNEL_SUMMARY_CHART_WIDTH;
          double rightInterval = (chartData.rightMax! - chartData.rightMin!) /
              CHANNEL_SUMMARY_CHART_WIDTH;
          if (rule == StyleSheetRule.LineGraph) {
            if (leftInterval >= rightInterval) {
              chartData.leftMin = chartData.leftMin! - leftInterval;
              chartData.rightMax = chartData.rightMax! + rightInterval;
            } else {
              chartData.leftMax = chartData.leftMax! + leftInterval;
              chartData.rightMin = chartData.rightMin! - rightInterval;
            }
          } else {
            //棒グラフの場合は最小値を下げ、最大値を上げる
            chartData.leftMin = chartData.leftMin! - leftInterval;
            chartData.leftMax = chartData.leftMax! + leftInterval;
            chartData.rightMin = chartData.rightMin! - rightInterval;
            chartData.rightMax = chartData.rightMax! + rightInterval;
          }
        }
        //一つのチャートに表示するデータを全て取得したら、チャートのWidget、ヘッダーのWidgetを作成する
        var chartWidth = isHorizontal
            ? DEFAULT_CELL_WIDTH * CHANNEL_SUMMARY_CHART_WIDTH
            : dataElement.data![0].length * DEFAULT_CELL_WIDTH;
        var chartHeight = isHorizontal
            ? dataElement.data!.length * DEFAULT_CELL_HEIGHT
            : DEFAULT_CELL_HEIGHT * CHANNEL_SUMMARY_CHART_WIDTH;
        Widget chartWidget = ChannelSummaryBorderWidget(
            width: chartWidth,
            height: chartHeight,
            left: true,
            bottom: true,
            top: true,
            right: true,
            child: rule == StyleSheetRule.LineGraph
                ? ChannelSummaryLineChart(chartData: chartData)
                : isHorizontal
                    ? ChannelSummaryBarChartV(chartData: chartData)
                    : ChannelSummaryBarChart(chartData: chartData));
        tableCellMap[ChannelSummaryPosition(chartLine, chartColumn)] =
            chartWidget;

        List<Widget> children = [];
        var width = isHorizontal
            ? DEFAULT_CELL_WIDTH * CHANNEL_SUMMARY_CHART_WIDTH
            : _getWidth(context, table, chartElement);
        var height = isHorizontal
            ? DEFAULT_CELL_HEIGHT
            : DEFAULT_CELL_HEIGHT * CHANNEL_SUMMARY_CHART_WIDTH;
        if (chartData.rightChartData.isNotEmpty) {
          //複数表示の場合はメモリを出さない
          children.add(ChannelSummaryBorderWidget(
              height: height,
              width: width,
              left: true,
              bottom: isHorizontal ? false : true,
              top: true,
              right: isHorizontal ? true : false,
              child: Container()));
        } else {
          children.add(_createChartHeaderWidget(
              rule,
              isHorizontal,
              chartData.leftMax!,
              chartData.leftMin!,
              width,
              height,
              chartData.leftChartData[0].color,
              leftVisible));
        }
        tableCellMap[ChannelSummaryPosition(headerLine, headerColumn)] =
            isHorizontal
                ? Column(
                    children: children,
                  )
                : Row(
                    children: children,
                  );

        //初期化処理
        headerLine = null;
        headerColumn = null;
        chartLine = null;
        chartColumn = null;
        chartData = ChartData();
      }
    });
  }

  ///チャートのヘッダーWidgetを作成する
  Widget _createChartHeaderWidget(
      StyleSheetRule? rule,
      bool isHorizontal,
      double max,
      double min,
      double width,
      double height,
      Color color,
      bool visible) {
    Widget? child;
    switch (rule) {
      case StyleSheetRule.LineGraph:
        child = ChannelSummaryLineChartHeader(
          max: max,
          min: min,
          width: width,
          color: color,
        );
        break;
      case StyleSheetRule.BarGraph:
        if (isHorizontal) {
          child = ChannelSummaryHorizontalBarChartHeader(
            max: max,
            min: min,
            height: height,
            color: color,
          );
        } else {
          child = ChannelSummaryBarChartHeader(
            max: max,
            min: min,
            width: width,
            color: color,
          );
        }
        break;
      default:
        break;
    }

    Widget headerWidget = ChannelSummaryBorderWidget(
        height: height,
        width: width,
        left: true,
        bottom: isHorizontal ? false : true,
        top: true,
        right: isHorizontal ? true : false,
        child: visible ? child : Container());
    return headerWidget;
  }

  ///Widgetをそれそれの場所に配置する
  Widget _deployWidget(
    BuildContext context,
    WidgetRef ref,
    ChannelSummaryTable table,
  ) {
    int allMaxLine = 0;
    int allMaxColumn = 0;
    //Widgetを配置する際の最大Line、Columnを求める
    table.elements.forEach((element) {
      if (element.define.line! > allMaxLine) allMaxLine = element.define.line!;
      if (element.define.column! > allMaxColumn)
        allMaxColumn = element.define.column!;
    });

    //作成したWidgetを実際に配置する
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:
            _createColumnChild(context, ref, table, [], 0, 0, allMaxLine, allMaxColumn));
  }

  List<Widget> _createColumnChild(
      BuildContext context,
      WidgetRef ref,
      ChannelSummaryTable table,
      List<Widget> baseColumnChildren,
      int currentLine,
      int currentColumn,
      int maxLine,
      int maxColumn) {
    var tableCellMap = ref.read(_summaryDataMapProvider).tableCellMap;
    List<Widget> rowChildren = [];
    int column, line;
    for (line = currentLine; line <= maxLine; line++) {
      for (column = currentColumn; column <= maxColumn; column++) {
        if (!tableCellMap.containsKey(ChannelSummaryPosition(line, column))) {
          //Cellにデータがなかったら次の要素
        } else {
          //Cellにデータあり
          var cell = tableCellMap[ChannelSummaryPosition(line, column)]!;
          rowChildren.add(cell);
          var heightExpandCount = _getHeightCount(table, line, column);
          if (heightExpandCount > 1) {
            //下に伸ばす場合
            var nested = Column(
                children: _createColumnChild(context, ref, table, [], line, column + 1,
                    line + heightExpandCount - 1, maxColumn));
            rowChildren.add(nested);
            column = maxColumn;
            line = line + heightExpandCount - 1;
          }
        }
      }
      baseColumnChildren.add(Row(
        children: rowChildren,
      ));
      rowChildren = [];
    }
    return baseColumnChildren;
  }

  int _getHeightCount(ChannelSummaryTable table, int? line, int? column) {
    var height = DEFAULT_CELL_HEIGHT;
    //tableのelementを検索し同じ行の最大縦幅を求める
    List<ChannelSummaryElement> sameLineElements = table.elements
        .where((conditions) => conditions.define.line == line)
        .toList();
    sameLineElements
        .sort((a, b) => (b.data ?? []).length.compareTo((a.data ?? []).length));
    //縦幅はDEFAULT_CELL_HEIGHT固定なのでDEFAULT_CELL_HEIGHT*列数が縦幅になる
    if (sameLineElements.isEmpty) {
      height = DEFAULT_CELL_HEIGHT;
    } else {
      height = DEFAULT_CELL_HEIGHT * (sameLineElements.first.data ?? []).length;
    }
    //下のオブジェクト数分伸ばす
    height =
        _getHeightInternal(table, line, column, DEFAULT_CELL_HEIGHT, height);
    return height ~/ DEFAULT_CELL_HEIGHT;
  }

  bool _isRightBorder(
      ChannelSummaryTable table, ChannelSummaryElement element) {
    //上と右に線を引くElementがあったら線を引かない。
    var rightElement = table.elements.firstWhereOrNull(
        (conditions) =>
            conditions.define.line == element.define.line &&
            conditions.define.column! > element.define.column!);
    if (rightElement != null &&
        (rightElement.define.rule == StyleSheetRule.Bordered ||
            rightElement.define.rule == StyleSheetRule.LineGraph ||
            rightElement.define.rule == StyleSheetRule.BarGraph ||
            rightElement.define.rule == StyleSheetRule.GraphData)) {
      return false;
    }
    return true;
  }

  bool _isTopBorder(ChannelSummaryTable table, ChannelSummaryElement element) {
    var upperLeftElement = table.elements.lastWhereOrNull(
        (conditions) =>
            conditions.define.line! < element.define.line! &&
            conditions.define.column! <= element.define.column!);
    if (upperLeftElement != null &&
        (upperLeftElement.define.rule == StyleSheetRule.Bordered ||
            upperLeftElement.define.rule == StyleSheetRule.LineGraph ||
            upperLeftElement.define.rule == StyleSheetRule.BarGraph ||
            upperLeftElement.define.rule == StyleSheetRule.GraphData)) {
      //上部、左上に線を引くElementがあったら線を引かない。
      return false;
    }
    return true;
  }

  bool _isBottomBorder(
      ChannelSummaryTable table, ChannelSummaryElement element) {
    //下に線を引くElementがあったら線を引かない。
    var upperLeftElement = table.elements.firstWhereOrNull(
        (conditions) =>
            conditions.define.line == element.define.line! + 1 &&
            conditions.define.column! < element.define.column!);
    if (upperLeftElement != null &&
        (upperLeftElement.define.rule == StyleSheetRule.LineGraph ||
            upperLeftElement.define.rule == StyleSheetRule.BarGraph ||
            upperLeftElement.define.rule == StyleSheetRule.GraphData)) {
      //下、左下に線を引くElementがあったら線を引かない。
      return false;
    } else {
      var upperElement = table.elements.lastWhereOrNull(
          (conditions) =>
              conditions.define.line! > element.define.line! &&
              conditions.define.column == element.define.column);
      if (upperElement != null &&
          (upperElement.define.rule == StyleSheetRule.LineGraph ||
              upperElement.define.rule == StyleSheetRule.BarGraph ||
              upperElement.define.rule == StyleSheetRule.GraphData)) {
        //下部に線を引くElementがあったら線を引かない。
        return false;
      }
    }
    return true;
  }
}
