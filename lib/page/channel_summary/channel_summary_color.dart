import 'package:flutter/material.dart';

class ChannelSummaryColor {
  //  The following names are also accepted: red, blue, green, black, white, gray, cyan, magenta,
  //  yellow, lightgray, darkgray, grey, lightgrey, darkgrey, aqua, fuchsia, lime, maroon,
  //  navy, olive, purple, silver, and teal.
  static Map<String, Color> colorStringMap = {
    'red': Colors.red,
    'blue': Colors.blue,
    'green': Colors.green,
    'black': Colors.black,
    'white': Colors.white,
    'gray': Colors.grey,
    'cyan': Colors.cyan,
    'magenta': const Color(0xFFFF00FF),
    'yellow': Colors.yellow,
    'lightgray': const Color(0xFFF5F5F5),
    'darkgray': const Color(0xFFA9A9A9),
    'aqua': const Color(0xFF00FFFF),
    'fuchsia': const Color(0xFFFF00FF),
    'lime': Colors.lime,
    'maroon': const Color(0xFF800000),
    'navy': const Color(0xFF000080),
    'olive': const Color(0xFF808000),
    'purple': Colors.purple,
    'silver': const Color(0xFFC0C0C0),
    'teal': Colors.teal,
  };

  static Color? parseColor(String? colorString) {
    try {
      if (colorString == null || colorString.isEmpty) return Colors.black;
      if (colorStringMap.containsKey(colorString))
        return colorStringMap[colorString];
      return Color(_getColorFromHex(colorString));
    } catch (e) {
      return Colors.black;
    }
  }

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
