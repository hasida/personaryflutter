class ChannelSummaryPosition {
  int? line;
  int? column;

  ChannelSummaryPosition(this.line, this.column);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChannelSummaryPosition &&
          runtimeType == other.runtimeType &&
          line == other.line &&
          column == other.column;

  @override
  int get hashCode => line.hashCode ^ column.hashCode;
}
