import 'package:intl/intl.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:plr_util/src/channel_summary/channel_summary.i18n.dart';

import 'channel_summary_element.dart';
import 'channel_summary_scope.dart';
import 'channel_summary_table.dart';

class ChannelSummaryNotifier extends Notifier<List<ChannelSummaryTable>> {
  final Channel channel;
  final Schemata schemata;
  final ChannelSummaryStyleSheet styleSheet;
  late Future _tableCreateFuture;

  ChannelSummaryNotifier(this.channel, this.schemata, this.styleSheet) {
    _tableCreateFuture = _createTable(styleSheet).catchError((e, s) {
      print('[ChannelSummaryNotifier] Error: $e');
      if (s != null) print('$s');
    });
    _calcSummaryPeriod();
  }

  bool _tableInitialized = false;

  List<ChannelSummaryTable> get tables => _tables ?? [];
  List<ChannelSummaryTable>? _tables;

  DateTime _selectedDate = DateTime.now();

  DateTime get selectedDate => _selectedDate;

  set selectedDate(DateTime value) {
    _selectedDate = value;
    _calcSummaryPeriod();
    _updateTable([], _selectedDate);
    update();
  }

  SummaryPeriod? _summaryPeriod;

  SummaryPeriod? get summaryPeriod => _summaryPeriod;

  String? _summaryPeriodString;

  String? get summaryPeriodString => _summaryPeriodString;

  Future<void> _createTable(ChannelSummaryStyleSheet styleSheet) async {
    var defines = styleSheet.definitions.toList();
    defines.sort((a, b) =>
        (a.line == b.line) ? (a.column! - b.column!) : (a.line! - b.line!));

    var table = ChannelSummaryTable(this);
    var tables = <ChannelSummaryTable>[table];
    int? offset = 0;
    int? lastLine = 0;
    var lineScopes = <int, Iterable<ChannelSummaryScope>>{};
    var columnScopes = <int, Iterable<ChannelSummaryScope>>{};
    for (var define in defines) {
      if (define.line! - lastLine! > 1) {
        offset = define.line;
        table = ChannelSummaryTable(this);
        lineScopes.clear();
        columnScopes.clear();
        tables.add(table);
      }
      lastLine = define.line;

      var line = define.line! - offset!;
      var column = define.column!;
      var element = ChannelSummaryElement(table, define);
      if (element.horizontalScopes.isNotEmpty) {
        columnScopes[column] = element.horizontalScopes;
      } else if (columnScopes.containsKey(column)) {
        element.scopes.insertAll(0, columnScopes[column]!);
      }
      if (element.verticalScopes.isNotEmpty) {
        lineScopes[line] = element.verticalScopes;
      } else if (lineScopes.containsKey(line)) {
        element.scopes.insertAll(0, lineScopes[line]!);
      }
      table.insertElement(element, column, line);
    }

    _tables = tables;
    _tableInitialized = false;
  }

  @override
  Future<bool> build([List<ChannelSummaryTable>? value]) async {
    _tables = value;
    return true;
  }

  @override
  Future<void> $update(bool force) async {
    await _tableCreateFuture;
    final selectedDate = this.selectedDate;

    if (!_tableInitialized) {
      _updateTable([], selectedDate);
      await _loadItems(selectedDate);
      _tableInitialized = true;
    }

    await channel.syncSilently();
    await _loadItems(selectedDate);
  }

  Future<void> _loadItems(DateTime? selectedDate) async {
    if (selectedDate != this.selectedDate) {
      /// 日付が変更されていた場合終了
      return;
    }

    final period =
        SummaryPeriod.fromSchemaPeriod(styleSheet.period!, selectedDate!);

    await channel
        .timelineItemModels(
          period.begin,
          period.end,
        )
        .handleError(
          (e, s) => print('$e'),
        )
        .listen((result) {
      final List<SchematicEntity<Item>> items = result
          .map((e) => SchematicEntity.from(schemata, e))
          .nonNulls
          .toList(growable: false);
      _updateTable(items, selectedDate);
    }).asFuture();
  }

  void _updateTable(List<SchematicEntity> items, DateTime? selectedDate) {
    if (selectedDate == this.selectedDate) {
      var tables = this._tables!;
      for (var table in tables) {
        table.update(items, selectedDate);
      }
      notify(tables);
    }
  }

  void _calcSummaryPeriod() {
    _summaryPeriod =
        SummaryPeriod.fromSchemaPeriod(styleSheet.period!, selectedDate);
    _summaryPeriodString = _getSummaryPeriodString();
  }

  String _getSummaryPeriodString() {
    if (styleSheet.period!.amount == 1) {
      return _getSummaryPeriodFormat().format(_summaryPeriod!.begin);
    } else {
      final beginStr = _getSummaryPeriodFormat().format(_summaryPeriod!.begin);
      final endStr = _getSummaryPeriodFormat().format(_summaryPeriod!.end);
      return '$beginStr ～ $endStr';
    }
  }

  DateFormat _getSummaryPeriodFormat() =>
      DateFormat(_getSummaryPeriodFormatString());

  String _getSummaryPeriodFormatString() {
    switch (styleSheet.period!.unit) {
      case SchemaPeriodUnit.Year:
        return 'yyyy'.i18n;
      case SchemaPeriodUnit.Month:
        return 'yyyy-MM'.i18n;
      case SchemaPeriodUnit.Week:
        return 'yyyy-MM-dd'.i18n;
      case SchemaPeriodUnit.Day:
        return 'yyyy-MM-dd'.i18n;
      default:
        return 'yyyy-MM-dd HH:mm:ss'.i18n;
    }
  }

  void setNextPeriod() => selectedDate =
      selectedDate.add(_summaryPeriod?.duration ?? const Duration(days: 1));

  void setPrevPeriod() => selectedDate =
      selectedDate.subtract(_summaryPeriod?.duration ?? const Duration(days: 1));
}
