import 'package:flutter/material.dart';

class ChannelSummaryBorderWidget extends StatelessWidget {
  final double? width;
  final double? height;
  final bool left;
  final bool bottom;
  final bool top;
  final bool right;
  final Alignment? alignment;
  final EdgeInsetsGeometry? padding;
  final Function? onTap;
  final Widget? child;

  ChannelSummaryBorderWidget(
      {this.width,
      this.height,
      required this.left,
      required this.bottom,
      required this.top,
      required this.right,
      this.alignment,
      this.padding,
      this.onTap,
      this.child});

  @override
  Widget build(BuildContext context) {
    var borderSide = const BorderSide(color: Colors.grey, width: 1);

    return InkWell(
        onTap: () {
          if (this.onTap != null) this.onTap!.call();
        },
        child: Container(
          width: width,
          height: height,
          alignment: alignment,
          padding: padding,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                left: left
                    ? borderSide
                    : BorderSide.none,
                bottom: bottom
                    ? borderSide
                    : BorderSide.none,
                top: top
                    ? borderSide
                    : BorderSide.none,
                right: right
                    ? borderSide
                    : BorderSide.none,
              )),
          child: child,
        ));
  }
}
