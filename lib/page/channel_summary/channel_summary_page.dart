import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../logic.dart';
import '../../util.dart';
import '../../widget.dart';
import 'channel_summary.dart';
import 'channel_summary_page.i18n.dart';

class ChannelSummaryPage extends StatelessConsumerPlrWidget {
  final Channel channel;

  ChannelSummaryPage(
    this.channel, {
      super.key,
  });

  late final SchemataSelectionProvider _schemataSelectionProvider;
  late final SchemataSelection _schemataSelectionController;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _schemataSelectionProvider = schemataSelectionProvider(
      channel, styleSheetType: StyleSheetType.channelSummary,
    );
    _schemataSelectionController = ref.read(
      _schemataSelectionProvider.notifier,
    );

    if (channel.plrId != root.plrId) {
      root.friendOf(channel.plrId).firstOrNull.then((result) {
          if (result != null) {
            automaticGenerateChannels(root, result.value);
          }
      });
    }

    final dsPlrId = channel.channelDataSetting?.plrId;
    if ((dsPlrId != null) && (dsPlrId != root.plrId)) {
      root.friendOf(dsPlrId).firstOrNull.then((result) {
          if (result != null) {
            automaticGenerateChannels(root, result.value);
          }
      });
    }
    autoDisclose(root, account, channel);
  }

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    final value = ref.watch(_schemataSelectionProvider).value;

    if (value?.selectedSchemata == null) return Container(
      alignment: Alignment.center,
      child: const CircularProgressIndicator(),
    );

    if (value?.selectedStyleSheet is! ChannelSummaryStyleSheet) {
      return DummyChannelSummaryPageContent(
        account, channel, value!, _schemataSelectionController,
      );
    }
    return ChannelSummary(
      account, channel, value!, _schemataSelectionController,
    );
  }
}

extension _ on SchemataSelection {
  static final _dialogShowns = Expando<bool>();

  bool get dialogShown => _dialogShowns[this] ?? false;
  set dialogShown(bool dialogShown) => _dialogShowns[this] = dialogShown;
}

abstract class ChannelSummaryPageContentBase extends StatelessPlrWidget {
  final Channel channel;
  final SchemataSelectionState schemataSelectionValue;
  final SchemataSelection schemataSelectionController;
  const ChannelSummaryPageContentBase(
    super.account,
    this.channel,
    this.schemataSelectionValue,
    this.schemataSelectionController, {
      super.key,
  });

  Future<void> showSchemaStyleSelectDialog(BuildContext context) async {
    if (schemataSelectionController.dialogShown) return;

    schemataSelectionController.dialogShown = true; try {
      final schemataSources = schemataSelectionValue.sources.where(
        (ss) => ss.name?.defaultValue != null
      );

      final schemataSourceId = schemataSelectionValue.selectedSchemataSourceId;
      final schemata = schemataSelectionValue.selectedSchemata;
      final styleSheetId = schemataSelectionValue.selectedStyleSheetId;

      // TODO&DONEの20210304a(チャネルのビュー)対応時にはチャネルサマリ以外の
      // スタイルシートも選択可能にする
      final styleSheets = HashSet<StyleSheet>(
        equals: (a, b) => a.id == b.id,
        hashCode: (e) => e.id.hashCode,
      )..addAll(
        schemata?.channelSummaryStyleSheets ?? <ChannelSummaryStyleSheet>[],
      );

      /// 選択ダイアログ表示
      final selected = await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
          title: Text("Select schema".i18n),
          children: <Widget>[
            if (schemataSources.isNotEmpty) Container(
              padding: const EdgeInsets.all(5),
              child: Text(
                "Schema".i18n, style: const TextStyle(color: Colors.blue),
              ),
            ),
            ...schemataSources.map(
              (ss) => _buildSchemaSelectOption(
                context, ss.name!.defaultValue,
                selected: schemataSourceId == ss.id,
                onPressed: () => Navigator.of(context).pop(ss),
              ),
            ),
            if (styleSheets.isNotEmpty) Container(
              padding: const EdgeInsets.all(5),
              child: Text(
                "Style".i18n, style: const TextStyle(color: Colors.blue),
              ),
            ),
            ...styleSheets.map(
              (ss) => _buildSchemaSelectOption(
                context, ss.label?.defaultValue ?? "",
                selected: styleSheetId == ss.id,
                onPressed: () => Navigator.of(context).pop(ss),
              ),
            ),
          ],
        ),
      );

      switch (selected) {
        case SchemataSource(:final id): {
          await schemataSelectionController.setSelectedSchemataSourceId(id);
        }
        case StyleSheet(:final id): {
          await schemataSelectionController.setSelectedStyleSheetId(id);
        }
      }
    } finally {
      schemataSelectionController.dialogShown = false;
    }
  }

  Widget _buildSchemaSelectOption(
    BuildContext context, String name,
    { bool selected = false, Function()? onPressed }) {
   return SimpleDialogOption(
      onPressed: onPressed,
      child: Row(
          children: <Widget>[
            Icon(
              Icons.check, color: selected ? Colors.black : const Color(0)
            ),
            Expanded(
              child: Text(name),
            ),
          ]
      ),
    );
  }
}

class DummyChannelSummaryPageContent extends ChannelSummaryPageContentBase {
  const DummyChannelSummaryPageContent(
    super.account,
    super.channel,
    super.schemataSelectionValue,
    super.schemataSelectionController, {
      super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => showSchemaStyleSelectDialog(context),
    );
    return emptyWidget;
  }
}
