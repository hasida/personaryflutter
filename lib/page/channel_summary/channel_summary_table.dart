import 'package:plr_ui/plr_ui.dart';

import 'channel_summary_element.dart';
import 'channel_summary_notifier.dart';

abstract class ChannelSummaryTable {
  final ChannelSummaryNotifier notifier;

  Iterable<ChannelSummaryElement> get elements;

  ChannelSummaryTable._(this.notifier);

  factory ChannelSummaryTable(ChannelSummaryNotifier notifier) =>
      ChannelSummaryTableImpl(notifier);

  void update(List<SchematicEntity> entityList, DateTime? selectedDate);

  void insertElement(ChannelSummaryElement element, int x, int y);

  ChannelSummaryElement? getElement(int x, int y);
}

class _Coordinate {
  int x;
  int y;

  _Coordinate(this.x, this.y);

  @override
  bool operator ==(Object other) {
    if (other is _Coordinate) {
      return (x == other.x && y == other.y);
    } else {
      return false;
    }
  }

  @override
  int get hashCode => ((x & 0xFFFFFFFF) | (y << 32));
}

class ChannelSummaryTableImpl extends ChannelSummaryTable {
  final Map<_Coordinate, ChannelSummaryElement> _elementMap = {};

  ChannelSummaryTableImpl(ChannelSummaryNotifier notifier) : super._(notifier);

  int _sortFunc(_Coordinate a, _Coordinate b) =>
      (a.y == b.y) ? (a.x - b.x) : (a.y - b.y);

  @override
  Iterable<ChannelSummaryElement> get elements {
    var entries = _elementMap.entries.toList();
    entries.sort((a, b) => _sortFunc(a.key, b.key));
    return entries.map((e) => e.value);
  }

  @override
  void insertElement(ChannelSummaryElement element, int x, int y) =>
      _elementMap[_Coordinate(x, y)] = element;

  @override
  ChannelSummaryElement? getElement(int x, int y) {
    var key = _Coordinate(x, y);
    if (_elementMap.containsKey(key)) {
      return _elementMap[key];
    } else {
      return null;
    }
  }

  void update(List<SchematicEntity> entityList, DateTime? selectedDate) {
    for (var e in elements) {
      e.update(entityList, selectedDate);
    }
  }
}
