import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../style.dart';
import '../../widget.dart';
import 'friend_page.i18n.dart';
import 'privates.dart';

class UserPage extends StatelessConsumerPlrWidget {
  final HasProfile user;
  UserPage(this.user, { super.key });

  late final UserInfoUpdate _userInfoUpdateController;
  late final UserInfoUpdateProvider _userInfoUpdateProvider;

  late final ProfileItemsProvider _profileItemsProvider;
  late final ProfileItems _profileItemsController;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _userInfoUpdateProvider = userInfoUpdateProvider(user);
    _userInfoUpdateController = ref.read(_userInfoUpdateProvider.notifier);

    _profileItemsProvider = profileItemsProvider(user);
    _profileItemsController = ref.read(_profileItemsProvider.notifier);
  }

  void _initListeners(BuildContext context, WidgetRef ref) {
    ref.listen(
      _userInfoUpdateProvider, (_, next) => processError(context, next),
    );
  }

  Future<void> _refresh([ bool force = false ]) => Future.wait([
      _userInfoUpdateController.refresh(force: force),
      _profileItemsController.refresh(force: force),
  ]);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    _initListeners(context, ref);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("User details".i18n),
        ),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _refresh,
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: _buildPage(context, ref),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, WidgetRef ref) => Consumer(
    builder: (context, ref, _) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UserHeader(
            user,
            buttons: (user is PublicRoot) ? [
              ImportAppDefinitionButton(account, user as PublicRoot),
              ShowFriendRequestButton(user as PublicRoot),
            ] : null,
          ),

          Padding(
            padding: bottomPadding[3],
            child: ProfileView(
              provider: _profileItemsProvider,
              exclude: const [ nameClass, pictureClass ],
            ),
          ),

          divider,
        ],
      );
    },
  );
}
