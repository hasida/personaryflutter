import 'dart:collection';
import 'dart:io' show SocketException;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide SearchController;
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../logic.dart';
import '../../style.dart';
import '../../util.dart';
import '../../widget.dart';
import '../../page/item_sets/item_sets_page.dart';
import '../../page/timeline/timeline_page.dart';
import '../../dialog/channel_edit/channel_info_dialog.dart';
import 'friend_page.i18n.dart';
import 'user_page.dart';
import 'privates.dart';

const _sendTimeout = Duration(minutes: 1);

enum _DisclosedToPopupItem {
  create, selectFriendChannel;

  String get label {
    switch (this) {
      case _DisclosedToPopupItem.create:
        return 'Create channel'.i18n;
      case _DisclosedToPopupItem.selectFriendChannel:
        return "Disclose existing channel".i18n;
    }
  }
}

Future<bool> openFriendOrUserPage(
  BuildContext context,
  Account account,
  HasProfile user,
) async => (
  await runWithProgress(
    context, () async {
      await for (
        var r in account.root.friendOf(user.plrId).handleError(
          (e, s) => showError(context, e, s)
        )
      ) {
        Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => FriendPage(r.value),
          ),
        );
        return true;
      }
      Navigator.push(
        context, CupertinoPageRoute(
          builder: (_) => UserPage(user),
        ),
      );
      return false;
    }, errorValue: false,
  )
)!;

class FriendPage extends StatelessConsumerPlrWidget {
  final Friend friend;
  FriendPage(this.friend, { super.key });

  late final FriendSyncProvider _friendSyncProvider;
  late final FriendSync _friendSyncController;

  late final UserInfoUpdate _userInfoUpdateController;
  late final UserInfoUpdateProvider _userInfoUpdateProvider;

  late final ProfileItemsProvider _profileItemsProvider;
  late final ProfileItems _profileItemsController;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _friendSyncProvider = friendSyncProvider(friend);
    _friendSyncController = ref.read(_friendSyncProvider.notifier);

    _userInfoUpdateProvider = userInfoUpdateProvider(friend);
    _userInfoUpdateController = ref.read(_userInfoUpdateProvider.notifier);

    _profileItemsProvider = profileItemsProvider(friend);
    _profileItemsController = ref.read(_profileItemsProvider.notifier);

    Future.microtask(() => _checkState(context, ref.read(_friendSyncProvider)));
  }

  static final _subscriptionCheckeds = Expando<bool>();
  bool get _subscriptionChecked => _subscriptionCheckeds[this] ?? false;
  set _subscriptionChecked(bool subscriptionChecked) =>
    _subscriptionCheckeds[this] = subscriptionChecked ? true : null;

  void _initListeners(BuildContext context, WidgetRef ref) {
    ref.listen(_friendSyncProvider, (_, next) => _checkState(context, next));
    ref.listen(
      _userInfoUpdateProvider, (_, next) => processError(context, next),
    );
  }

  void _checkState(BuildContext context, AsyncValue<FriendState> state) {
    processError(context, state);

    switch (state) {
      case AsyncData(isLoading: false): {
        _automaticGenerateChannels(context);
        if (!_subscriptionChecked) {
          _subscribeInfoChannel();
          _subscriptionChecked = true;
        }
      }
      case AsyncError(error: EntryNotFoundException()): {
        showConfirmDialog(
          context,
          "This friend has been removed.".i18n,
          "Close this page and back to the previous screen.".i18n,
          showCancelButton: false,
        ).then((_) => Navigator.pop(context));
      }
    }
  }

  Future<void> _refresh([ bool force = false ]) => Future.wait([
      _friendSyncController.refresh(force: force),
      _userInfoUpdateController.refresh(force: force),
      _profileItemsController.refresh(force: force),
  ]);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    _initListeners(context, ref);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("User details".i18n),
        ),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _friendSyncController.refresh,
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: _buildPage(context, ref),
              ),
            ),
          ),
        ),
      ),
    );
  }

  T? _setupRoot<T extends HasChannels>(
    BuildContext context,
    WidgetRef ref,
    FriendState friendValue,
    T? theRoot,
  ) {
    if (theRoot == null) return null;

    final syncProvider = hasChannelsSyncProvider(theRoot);
    if (ref.exists(syncProvider)) {
      Future.microtask(
        () => ref.read(syncProvider.notifier).refresh(
          force: friendValue.isForced,
        ),
      );
      return theRoot;
    }

    ref.listen(syncProvider, (_, next) async {
        switch (next) {
          case AsyncData(:final value, isLoading: false): switch (theRoot) {
            case MeToFriendRoot(): {
              if (value.hadErrors) return;

              if (await mayDepositPassphrase(
                  account.root, theRoot, syncFirst: false
              )) {
                showPassphraseDepositedDialog(context, [ theRoot.friend ]);
                ref.read(syncProvider.notifier).refresh();
              }
            }
            case FriendToMeRoot(): onFriendDisplayed(
              notificationRegistry, theRoot.friend, !value.hadErrors,
            );
          }
        }
    });
    return theRoot;
  }

  Widget _buildPage(BuildContext context, WidgetRef ref) => Consumer(
    builder: (context, ref, _) {
      MeToFriendRoot? meToFriendRoot;
      FriendToMeRoot? friendToMeRoot;
      PublicRoot? publicRoot;

      switch (ref.watch(_friendSyncProvider)) {
        case AsyncData(:final value): {
          meToFriendRoot = _setupRoot(
            context, ref, value, value.meToFriendRoot,
          );
          friendToMeRoot = _setupRoot(
            context, ref, value, value.friendToMeRoot,
          );
          publicRoot = _setupRoot(context, ref, value, value.publicRoot);
        }
      }

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildUserHeader(meToFriendRoot, friendToMeRoot, publicRoot),

          Padding(
            padding: bottomPadding[3],
            child: ProfileView(
              provider: _profileItemsProvider,
              exclude: const [ nameClass, pictureClass ],
            ),
          ),

          divider,

          _buildPublicRootSection(context, ref, publicRoot),
          _buildFriendToMeRootSection(context, ref, friendToMeRoot),
          _buildMeToFriendRootSection(context, ref, meToFriendRoot),
        ],
      );
    },
  );

  Widget _buildUserHeader(
    MeToFriendRoot? meToFriendRoot,
    FriendToMeRoot? friendToMeRoot,
    PublicRoot? publicRoot,
  ) => UserHeader(
    friend,
    buttons: [
      BidirectionalFriendIcon(friend),
      if (publicRoot != null) ImportAppDefinitionButton(account, publicRoot),
      if (meToFriendRoot != null) _DepositPassphraseButton(meToFriendRoot),
      if (friendToMeRoot != null) _CopyPassphraseButton(friendToMeRoot),
      if (publicRoot != null) ShowFriendRequestButton(publicRoot),
    ],
  );

  Widget _buildPublicRootSection(
    BuildContext context,
    WidgetRef ref,
    PublicRoot? publicRoot,
  ) {
    if (publicRoot == null) return emptyWidget;

    final provider = publicRootSyncWithSearchProvider(publicRoot);
    final controller = ref.read(provider.notifier);

    final title = "Channels published from this user".i18n;

    final buttons = [
      SearchButton(
        provider: provider, heroTag: "public_channel_search",
        compact: true, useChannelDatabase: true,
        onPressed: () => _search(context, controller),
      ),
    ];
    List<Widget> buildButtons(List<Channel> items) => buttons;

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => _buildList(context, ref, shrinkWrap, provider, controller, publicRoot);

    return buildLimitedListSection(
      context, ref, provider: provider, //readOnly: true,
      title: title, showAllButtonTooltip: "Show all channels.".i18n,
      itemsGetter: defaultChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Widget _buildFriendToMeRootSection(
    BuildContext context,
    WidgetRef ref,
    FriendToMeRoot? friendToMeRoot,
  ) {
    if (friendToMeRoot == null) return emptyWidget;

    final provider = friendRootSyncWithSearchProvider(friendToMeRoot);
    final controller = ref.read(provider.notifier);

    final title = "Channels disclosed from this user".i18n;

    final buttons = [
      _buildNodesDisclosedButton(friendToMeRoot),
      _buildDisclosureHistoryButton(friendToMeRoot),
      SearchButton(
        provider: provider, heroTag: "friend_to_me_root_channel_search",
        compact: true, useChannelDatabase: true,
        onPressed: () => _search(context, controller),
      ),
    ];
    List<Widget> buildButtons(List<Channel> items) => buttons;

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => _buildList(context, ref, shrinkWrap, provider, controller, friendToMeRoot);

    return buildLimitedListSection(
      context, ref, provider: provider, //readOnly: true,
      title: title, showAllButtonTooltip: "Show all channels.".i18n,
      itemsGetter: defaultChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Widget _buildMeToFriendRootSection(
    BuildContext context,
    WidgetRef ref,
    MeToFriendRoot? meToFriendRoot,
  ) {
    if (meToFriendRoot == null) return emptyWidget;

    final provider = friendRootSyncWithSearchProvider(meToFriendRoot);
    final controller = ref.read(provider.notifier);

    final title = "Channels disclosed to this user".i18n;

    final buttons = [
      _buildNodesDisclosedButton(meToFriendRoot),
      _buildDisclosureHistoryButton(meToFriendRoot),
      SearchButton(
        provider: provider, heroTag: "me_to_friend_root_channel_search",
        compact: true, useChannelDatabase: true,
        onPressed: () => _search(context, controller),
      ),
      PopupMenuButton(
        child: InlineAddButton(
          heroTag: "hero_disclose_channel",
          tooltip: "Disclose channel".i18n,
        ),
        itemBuilder: (context) => _DisclosedToPopupItem.values.map<
          PopupMenuEntry<_DisclosedToPopupItem>
        >(
          (e) => PopupMenuItem(child: Text(e.label), value: e),
        ).toList(),
        onSelected: (value) {
          if (value == _DisclosedToPopupItem.create) {
            _createChannel(context);
          }
          else if (value == _DisclosedToPopupItem.selectFriendChannel) {
            _selectFriendChannel(
              context, meToFriendRoot,
              ref.read(provider).value?.channels ?? const [],
            );
          }
        },
      ),
    ];
    List<Widget> buildButtons(List<Channel> items) => buttons;

    Widget buildList(
      BuildContext context,
      WidgetRef ref,
      bool shrinkWrap,
    ) => _buildList(
      context, ref, shrinkWrap, provider, controller, meToFriendRoot,
    );

    return buildLimitedListSection(
      context, ref, provider: provider, readOnly: false,
      title: title, showAllButtonTooltip: "Show all channels.".i18n,
      itemsGetter: defaultChannelListItemsGetter,
      buttonsBuilder: buildButtons, listBuilder: buildList,
    );
  }

  Widget _buildList<
    C extends HasChannels,
    P extends PlrAsyncNotifierProvider<N, S>,
    N extends HasChannelsSyncInterface<S>,
    S extends HasChannelsStateInterface
  >(
    BuildContext context,
    WidgetRef ref,
    bool shrinkWrap,
    P provider,
    N controller,
    C container,
  ) => ChannelList<Channel, P, N, S>(
    shrinkWrap: shrinkWrap,
    provider: provider,
    container: container,
    cellConfig: channelCellConfigWith(context, ref, controller),
  );

  Widget _buildNodesDisclosedButton(FriendRoot friendRoot) => Consumer(
    builder: (context, ref, _) {
      final itemSets = ref.watch(
        friendRootSyncProvider(friendRoot),
      ).value?.itemSets;

      return CompactIconButton(
        icon: Hero(
          tag: "${friendRoot.runtimeType}_nodes_disclosed",
          child: const Icon(Icons.list_alt),
        ),
        tooltip: "Nodes disclosed".i18n,
        onPressed: (itemSets?.isNotEmpty == true) ? () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => ItemSetsPage(friendRoot),
          ),
        ) : null,
      );
    },
  );

  Widget _buildDisclosureHistoryButton(FriendRoot friendRoot) => Consumer(
    builder: (context, ref, _) {
      var disclosureHistory = ref.watch(
        friendRootSyncProvider(friendRoot),
      ).value?.disclosureHistory;

      return CompactIconButton(
        icon: Hero(
          tag: "${friendRoot.runtimeType}_disclosure_history",
          child: const Icon(Icons.history),
        ),
        tooltip: "Disclosure History".i18n,
        onPressed: (disclosureHistory != null) ? () => mayOpenTimelinePage(
          context, account, disclosureHistory,
          onItemUpdated: disclosureHistory.onItemUpdated,
          onItemDeleted: disclosureHistory.onItemDeleted,
        ) : null,
      );
    },
  );

  Future<void> _search(
    BuildContext context,
    HasChannelsSyncWithSearchInterface controller,
  ) async {
    if (!await showSearchChannelsDialog(context, controller)) return;

    await runWithProgress(context, controller.search);
  }

  Future<void> _createChannel(BuildContext context) async {
    var meToFriendRoot = friend.meToFriendRoot;
    if (meToFriendRoot == null) return;

    var channel = await showDialog<Channel>(
      context: context,
      builder: (_) => ChannelInfoDialog(
        root: root,
        notificationRegistry: notificationRegistry,
      ),
    );
    if (channel == null) return;

    await runWithProgress(
      context, () async {
        if (!await channel.waitForSent(timeout: _sendTimeout)) {
          throw const SocketException("waitForSent timeout.");
        }
        await meToFriendRoot
          .addChannelAndRequestDisclosureConsent(channel, root: root);
        return true;
      },
      onError: (e, s) => processNeedNetworkException(context, e, s),
      errorValue: false,
      onFinish: (r) {
        if (r == true) _friendSyncController.refresh();
      },
    );
  }

  Future<void> _selectFriendChannel(
    BuildContext context,
    MeToFriendRoot meToFriendRoot,
    List<Channel> currentChannels,
  ) async {
    final channelNotifications = await showChannelNotificationSelectDialog(
      context,
      excludeIds: currentChannels.map((c) => c.id).nonNulls,
      selectionMode: AsyncListSelectionMode.multiple,
      showSelectAll: false,
      onError: (e, s) => showError(context, e, s),
    );
    if (channelNotifications == null) return;

    final unDisclosables = (
      await runWithProgress<List<Channel?>>(
        context, () => Future.wait(
          channelNotifications.map((n) async {
              Channel? c = n.channel;
              if ((c == null) && (c = (await n.anyModel)?.model) == null) {
                return null;
              }
              if (!c!.isLoaded) await c.syncSilently();
              final ds = c.channelDataSetting;
              if (ds != null) {
                if (!ds.isLoaded) await ds.syncSilently();
                if (!ds.isDisclosableToOthers) return c;
              }
              await meToFriendRoot.addChannelAndRequestDisclosureConsent(
                c, root: root,
              );
              return null;
            },
          ),
        ),
        onError: (e, s) => processNeedNetworkException(context, e, s),
      )
    )?.nonNulls;

    if (unDisclosables?.isNotEmpty == true) {
      showConfirmDialog(
        context, "Warnings".i18n,
        "The following channels could not be added"
        " because they are not allowed.\n%s".i18n.fill([
            unDisclosables!.map(
              (c) => "- " + (
                c.name?.defaultValue?.toString() ?? c.id ?? "Unknown".i18n
              )
            ).join("\n"),
        ]),
        showCancelButton: false,
      );
    }
    _friendSyncController.refresh();
  }

  Future<void> _automaticGenerateChannels(
    BuildContext context, [
      bool force = false,
  ]) async {
    if (
      await automaticGenerateChannels(
        root, friend, onError: (e, s) => showError(context, e, s), force: force)
    ) _friendSyncController.refresh();
  }

  /// フレンドを広報チャネルの購読対象に設定
  Future<void> _subscribeInfoChannel() async {
    if (friend.publicRoot?.hasInfoChannels == true) {
      var infoRegistry = await root.storage.infoRegistry;
      if (!infoRegistry.isLoaded) {
        await infoRegistry.syncSilently();
      }
      maySubscribeInfoChannel(root, friend, infoRegistry: infoRegistry);
    }
  }
}

double _iconSizeOf(BuildContext context) =>
  Theme.of(context).iconTheme.size ?? 24;

class _CopyPassphraseButton extends ConsumerWidget {
  final FriendToMeRoot friendToMeRoot;
  const _CopyPassphraseButton(this.friendToMeRoot);

  @override
  Widget build(
    BuildContext context,
    WidgetRef ref,
  ) => CompactIconButton(
    icon: copyPassphraseIconOf(_iconSizeOf(context)),
    tooltip: "Copy this friend's alleged passphrase.".i18n,
    onPressed: switch (ref.watch(friendRootSyncProvider(friendToMeRoot))) {
      AsyncData(:final value, isLoading: false) when value.isLoaded => () {
        Clipboard.setData(
          ClipboardData(text: value.passphrase),
        );
        showMessage(
          context,
          "This friend's alleged passphrase has been copied to the clipboard.".i18n,
        );
      },
      _ => null,
    },
  );
}

Future<bool> showAppDefinitionImportDialog(
  BuildContext context,
  WidgetRef ref,
  Account account,
  PublicRoot publicRoot, [
    AppDefinitionRegistryState? appDefinitionRegistryValue,
]) async {
  final (imports, applied) = await showAppDefinitionSelectDialog(
    context, publicRoot,
    title: "Import app definitions".i18n,
    okButton: "Import".i18n,
    excludeIds: appDefinitionRegistryValue?.appDefinitions.where(
      (ad) => !ad.isInner,
    ).map(
      (ad) => ad.id,
    ),
    selectionMode: AsyncListSelectionMode.multiple,
    showApplyButton: true,
    onError: (e, s) => showError(context, e, s),
  );
  if (imports == null) return false;

  for (final i in imports) {
    account.appDefinitionRegistry.addAppDefinition(i);
  }
  ref.read(
    appDefinitionRegistrySyncProvider(account.appDefinitionRegistry).notifier,
  ).refresh();

  if (applied == null) return false;

  return await applyAppDefinition(context, ref, account.root, applied);
}

class _DepositPassphraseButton extends ConsumerWidget {
  final MeToFriendRoot meToFriendRoot;
  const _DepositPassphraseButton(this.meToFriendRoot);

  @override
  Widget build(
    BuildContext context,
    WidgetRef ref,
  ) => switch (ref.watch(friendRootSyncProvider(meToFriendRoot))) {
    AsyncData(:final value, isLoading: false) when value.isLoaded => (
      value.hasRealPassphrase ?
        releaseButtonOf(context, ref, meToFriendRoot, value) :
        depositButtonOf(context, ref, meToFriendRoot, value)
    ),
    _ => emptyWidget,
  };

  static Widget depositButtonOf(
    BuildContext context,
    WidgetRef ref,
    MeToFriendRoot meToFriendRoot,
    FriendRootState value,
  ) => CompactIconButton(
    icon: depositPassphraseIconOf(_iconSizeOf(context)),
    tooltip: "Deposit your passphrase.".i18n,
    onPressed: () async {
      if (!await showConfirmDialog(
          context, "Deposit your passphrase.".i18n,
          "You're about to deposit your passphrase to this friend.\nAre you sure?".i18n,
      )) return;

      await runWithProgress(
        context, () async {
          if (await meToFriendRoot.setRealPassphrase()) {
            ref.read(
              friendRootSyncProvider(meToFriendRoot).notifier,
            ).refresh();
          }
        }, onError: (e, s) => showError(context, e, s),
      );
    },
  );

  static Widget releaseButtonOf(
    BuildContext context,
    WidgetRef ref,
    MeToFriendRoot meToFriendRoot,
    FriendRootState value,
  ) => CompactIconButton(
    icon: releasePassphraseIconOf(_iconSizeOf(context)),
    tooltip: "Cancel your passphrase deposit.".i18n,
    onPressed: () async {
      if (!await showConfirmDialog(
          context, "Cancel your passphrase deposit.".i18n,
          "You're about to stop depositing your passphrase to this friend.\nAre you sure?".i18n,
      )) return;

      if (meToFriendRoot.setFakePassphrase()) {
        ref.read(
          friendRootSyncProvider(meToFriendRoot).notifier,
        ).refresh();
      }
    },
  );
}
