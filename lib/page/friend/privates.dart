import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart' show showError, applyAppDefinition;
import '../../widget.dart' show FriendRequestPanel;
import 'friend_page.i18n.dart';

double _iconSizeOf(BuildContext context) =>
  Theme.of(context).iconTheme.size ?? 24;


class ImportAppDefinitionButton extends ConsumerWidget {
  final Account account;
  final PublicRoot publicRoot;
  ImportAppDefinitionButton(
    this.account,
    this.publicRoot,
  );

  Widget build(BuildContext context, WidgetRef ref) {
    final appDefinitionRegistryValue = ref.watch(
      appDefinitionRegistrySyncProvider(account.appDefinitionRegistry),
    ).value;

    final bool? hasToImports; {
      final publicRootValue = ref.watch(
        publicRootSyncProvider(publicRoot),
      ).value;

      if ((appDefinitionRegistryValue != null) && (publicRootValue != null)) {
        final registered = appDefinitionRegistryValue.appDefinitions.where(
          (ad) => !ad.isInner,
        ).map(
          (ad) => ad.id,
        ).toSet();

        hasToImports = (
          publicRootValue.appDefinitions.toSet()..removeWhere(
            (ad) => registered.contains(ad.id),
          )
        ).isNotEmpty;
      } else hasToImports = false;
    }

    return CompactIconButton(
      icon: Icon(Icons.app_settings_alt, size: _iconSizeOf(context)),
      tooltip: "Import app definitions from the user.".i18n,
      onPressed: hasToImports ? () => showAppDefinitionImportDialog(
        context, ref, account, publicRoot, appDefinitionRegistryValue,
      ) : null,
    );
  }
}

Future<bool> showAppDefinitionImportDialog(
  BuildContext context,
  WidgetRef ref,
  Account account,
  PublicRoot publicRoot, [
    AppDefinitionRegistryState? appDefinitionRegistryValue,
]) async {
  final (imports, applied) = await showAppDefinitionSelectDialog(
    context, publicRoot,
    title: "Import app definitions".i18n,
    okButton: "Import".i18n,
    excludeIds: appDefinitionRegistryValue?.appDefinitions.where(
      (ad) => !ad.isInner,
    ).map(
      (ad) => ad.id,
    ),
    selectionMode: AsyncListSelectionMode.multiple,
    showApplyButton: true,
    onError: (e, s) => showError(context, e, s),
  );
  if (imports == null) return false;

  for (final i in imports) {
    account.appDefinitionRegistry.addAppDefinition(i);
  }
  ref.read(
    appDefinitionRegistrySyncProvider(account.appDefinitionRegistry).notifier,
  ).refresh();

  if (applied == null) return false;

  return await applyAppDefinition(context, ref, account.root, applied);
}

class ShowFriendRequestButton extends StatelessWidget {
  final PublicRoot publicRoot;
  ShowFriendRequestButton(this.publicRoot);

  Widget build(BuildContext context) => CompactIconButton(
    icon: const Icon(Icons.qr_code_2),
    tooltip: "Show the friend request code for this user.".i18n,
    onPressed: () => showConfirmDialog(
      context, "Friend request code".i18n,
      FriendRequestPanel(publicRoot.toFriendRequest()),
      showCancelButton: false,
      okButtonLabel: MaterialLocalizations.of(context).closeButtonLabel,
    ),
  );
}
