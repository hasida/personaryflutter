import 'package:i18n_extension/i18n_extension.dart';
import 'package:plr_ui/plr_ui.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  confirmationTranslation +
  unknownTranslation +
  {
    "en_us": "User details",
    "ja_jp": "利用者詳細",
  } +
  {
    "en_us": "Disclose channel",
    "ja_jp": "チャネルを開示",
  } +
  {
    "en_us": "Create channel",
    "ja_jp": "チャネルを作成",
  } +
  {
    "en_us": "Disclose existing channel",
    "ja_jp": "既存のチャネルを開示",
  } +
  {
    "en_us": "Select channel",
    "ja_jp": "チャネルを選択",
  } +
  {
    "en_us": "No common course",
    "ja_jp": "共通の科目はありません",
  } +
  {
    "en_us": "Show timetable",
    "ja_jp": "時間割を表示",
  } +
  {
    "en_us": "Remove",
    "ja_jp": "解除",
  } +
  {
    "en_us": "Channels published from this user",
    "ja_jp": "この利用者が公開しているチャネル",
  } +
  {
    "en_us": "Channels disclosed from this user",
    "ja_jp": "この利用者が開示しているチャネル",
  } +
  {
    "en_us": "Channels disclosed to this user",
    "ja_jp": "この利用者に開示しているチャネル",
  } +
  {
    "en_us": "Disclosure History",
    "ja_jp": "開示履歴",
  } +
  {
    "en_us": "Nodes disclosed",
    "ja_jp": "開示しているノード",
  } +
  {
    "en_us": "Show all channels.",
    "ja_jp": "すべてのチャネルを表示",
  } +
  {
    "en_us": "This friend has been removed.",
    "ja_jp": "この友達は削除されました",
  } +
  {
    "en_us": "Close this page and back to the previous screen.",
    "ja_jp": "このページを閉じて、前の画面に戻ります。",
  } +
  {
    "en_us": "Copy this friend's alleged passphrase.",
    "ja_jp": "この友達のパスフレーズかも知れないテキストをコピー",
  } +
  {
    "en_us": "This friend's alleged passphrase has been copied to the clipboard.",
    "ja_jp": "この友達のパスフレーズかも知れないテキストをクリップボードにコピーしました。",
  } +
  {
    "en_us": "Deposit your passphrase.",
    "ja_jp": "パスフレーズを預ける",
  } +
  {
    "en_us": "You're about to deposit your passphrase to this friend.\nAre you sure?",
    "ja_jp": "この友達にパスフレーズを預けます。\nよろしいですか?",
  } +
  {
    "en_us": "Cancel your passphrase deposit.",
    "ja_jp": "パスフレーズ預け先を解除",
  } +
  {
    "en_us": "You're about to stop depositing your passphrase to this friend.\nAre you sure?",
    "ja_jp": "この友達にパスフレーズを預けるのをやめます。\nよろしいですか?",
  } +
  {
    "en_us": "Import app definitions from the user.",
    "ja_jp": "このユーザからアプリ定義をインポートします。",
  } +
  {
    "en_us": "Import app definitions",
    "ja_jp": "アプリ定義をインポート",
  } +
  {
    "en_us": "Import",
    "ja_jp": "インポート",
  } +
  {
    "en_us": "Show the friend request code for this user.",
    "ja_jp": "このユーザのフレンドリクエストコードを表示します。",
  } +
  {
    "en_us": "Friend request code",
    "ja_jp": "フレンドリクエストコード",
  } +
  {
    "en_us": "Warnings",
    "ja_jp": "警告",
  } +
  {
    "en_us": "The following channels could not be added because they are not allowed.\n%s",
    "ja_jp": "以下のチャネルは、許可されていないため追加できませんでした。\n%s",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
