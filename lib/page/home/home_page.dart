import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import '../../style.dart';
import '../../dialog/channel_edit/channel_info_dialog.dart';
import 'home_page.i18n.dart';

class HomePage extends StatelessConsumerPlrWidget {
  HomePage({ super.key });

  late final RootSyncWithSearchProvider _rootSyncProvider;
  late final UserInfoUpdateProvider _userInfoUpdateProvider;

  late final RootSyncWithSearch _rootSyncController;
  late final UserInfoUpdate _userInfoUpdateController;

  late final ProfileItemsProvider _rootProfileItemsProvider;
  late final ProfileItems _rootProfileItemsController;

  late final ProfileItemsProvider _publicProfileItemsProvider;
  late final ProfileItems _publicProfileItemsController;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _rootSyncProvider = rootSyncWithSearchProvider(root);
    _userInfoUpdateProvider = userInfoUpdateProvider(root);

    _rootSyncController = ref.read(_rootSyncProvider.notifier);
    _userInfoUpdateController = ref.read(userInfoUpdateProvider(root).notifier);

    _rootProfileItemsProvider = profileItemsProvider(root);
    _rootProfileItemsController = ref.read(_rootProfileItemsProvider.notifier);

    _publicProfileItemsProvider = profileItemsProvider(
      publicRoot, scanOthers: false,
    );
    _publicProfileItemsController = ref.read(
      _publicProfileItemsProvider.notifier,
    );

    _checkState(context, ref.read(_rootSyncProvider));
  }

  void _initListeners(BuildContext context, WidgetRef ref) {
    ref.listen(_rootSyncProvider, (_, next) {
        processError(context, next);
        _checkState(context, next);
    });
    ref.listen(
      _userInfoUpdateProvider, (_, next) => processError(context, next),
    );
    ref.listen(
      _rootProfileItemsProvider, (_, next) => processError(context, next),
    );
    ref.listen(
      _publicProfileItemsProvider, (_, next) => processError(context, next),
    );
  }

  void _checkState(BuildContext context, AsyncValue<RootState> state) {
    switch (state) {
      case AsyncData(:final value, isLoading: false): {
        _mayInitializeRoot(context, value.hadErrors);
        updatePublicDisclosure(
          root.lifeRecord, publicRoot,
          onDisclose: () => _publicProfileItemsController.refresh(force: true),
        );
      }
    }
  }

  Future<void> _refresh([ bool force = false ]) => Future.wait([
      _rootSyncController.refresh(force: force),
      _userInfoUpdateController.refresh(force: force),
      _rootProfileItemsController.refresh(force: force),
      _publicProfileItemsController.refresh(force: force),
  ]);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    _initListeners(context, ref);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Home".i18n),
        ),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _refresh,
            child: PageScrollView(
              child: Container(
                padding: allPadding[3],
                child: _buildPage(context, ref),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, WidgetRef ref) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      _buildHeader(context, ref),
      divider,
      _buildChannelsSeciton(context, ref),
    ],
  );

  Widget _buildHeader(BuildContext context, WidgetRef ref) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      UserHeader(
        root,
        buttons: [
          LifeRecordButton(
            account,
            rootSyncProvider: _rootSyncProvider,
            publicProfileItemsProvider: _publicProfileItemsProvider,
          ),
          ProfileDiscloseButton(
            account, publicProfileItemsProvider: _publicProfileItemsProvider,
          ),
        ],
      ),
      Padding(
        padding: bottomPadding[3],
        child: ProfileView(
          provider: _rootProfileItemsProvider,
          include: const [ genderClass, birthClass, homeClass ],
          exclude: const [ nameClass, pictureClass ],
        ),
      ),
    ],
  );

  Widget _buildChannelsSeciton(BuildContext context, WidgetRef ref) {
    final _channelsButtons = [
      SearchButton(
        provider: _rootSyncProvider,
        heroTag: "hero_search_channel", compact: true, useChannelDatabase: true,
        onPressed: () async {
          if (
            !await showSearchChannelsDialog(context, _rootSyncController)
          ) return;

          await runWithProgress(context, _rootSyncController.search);
        },
      ),
      InlineAddButton(
        margin: const EdgeInsets.only(right: 4),
        heroTag: "hero_create_channel",
        tooltip: "Create channel".i18n,
        onPressed: () async {
          var channel = await showDialog<Channel>(
            context: context,
            builder: (_) => ChannelInfoDialog(
              root: root,
              notificationRegistry: notificationRegistry,
            ),
          );
          if (
            channel?.channelDataSetting?.isTopPage == true
          ) root.topPageChannelId = channel!.channelId;

          _refresh();
        },
      ),
    ];

    final title = "Channels".i18n;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: <Widget>[
            Expanded(
              child: Consumer(
                builder: (context, ref, _) => Hero(
                  tag: title,
                  flightShuttleBuilder: textFlightShuttleBuilder,
                  child: SectionHeader(
                    title + switch (ref.watch(_rootSyncProvider)) {
                      AsyncData(:final value) => " (${value.channelCount})",
                      _ => "",
                    },
                  ),
                ),
              ),
            ),
            ..._channelsButtons,
          ],
        ),
        ChannelList<
          Channel, RootSyncWithSearchProvider, RootSyncWithSearch, RootState
        >(
          shrinkWrap: true,
          provider: _rootSyncProvider,
          container: root,
          cellConfig: channelCellConfigWith(context, ref, _rootSyncController),
          onError: (e, s) => showError(context, e, s),
        ),
      ],
    );
  }

  Future<void> _mayInitializeRoot(
    BuildContext context,
    bool noErrors,
  ) async {
    // Temporarily stop generation of initial channels.
    /*
    if (!noErrors) return;

    var result;
    try {
      result = await checkInitialChannels(root);
    } catch (e, s) {
      showError(context, e, s);
      return;
    }
    if (result) {
      showMessage(context, "Initial channels have been created.".i18n);
      _rootSyncController.refrsh();
    }
    */
  }
}
