import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  lifeRecordTranslation +
  {
    "en_us": "Home",
    "ja_jp": "ホーム",
  } +
  {
    "en_us": "Channels",
    "ja_jp": "チャネル",
  } +
  {
    "en_us": "Create channel",
    "ja_jp": "チャネルを作成",
  } +
  {
    "en_us": "Initial channels have been created.",
    "ja_jp": "初期チャネルを作成しました。",
  } +
  {
    "en_us": "Show all channels.",
    "ja_jp": "すべてのチャネルを表示",
  } +
  {
    "en_us": "Publish profile",
    "ja_jp": "プロフィールの公開",
  };

  String get i18n => localize(this, t);
}
