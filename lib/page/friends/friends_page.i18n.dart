import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Friends",
    "ja_jp": "友達",
  };

  String get i18n => localize(this, t);
}
