import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import '../../widget.dart';
import '../../dialog/friend_add/friend_add_dialog.dart';
import 'friends_page.i18n.dart';

class FriendsPage extends StatelessConsumerPlrWidget {
  FriendsPage({ super.key });

  final _provider = friendsWithSearchProvider();
  late final FriendsWithSearch _controller;

  @override
  void init(BuildContext context, WidgetRef ref) {
    _controller = ref.read(_provider.notifier);

    _checkState(context, ref.read(_provider));
  }

  void _checkState(BuildContext context, AsyncValue<FriendsState?> state) {
    if (processError(context, state)) return;

    switch (state) {
      case AsyncData(isLoading: false): _checkFriendRequests(context);
    }
  }

  Future<void> _refresh([
      bool force = false,
  ]) => _controller.refresh(force: force);

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) {
    ref.listen(_provider, (_, next) => _checkState(context, next));

    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(context, ref),
        body: RefreshIndicator(
          onRefresh: () => _refresh(true),
          child: TimerShownDetector(
            onTimer: _refresh,
            child: _buildList(context, ref),
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context, WidgetRef ref) => AppBar(
    title: Consumer(
      builder: (context, ref, _) => Text(
        "Friends".i18n + 
        switch (ref.watch(_provider)) {
          AsyncData(:final value) when (
            (value != null) && (value.count > 0)
          ) => " (${value.count})",
          _ => "",
        },
      ),
    ),
    actions: [
      SearchButton(
        provider: _provider, color: Colors.grey, useFriendDatabase: true,
        onPressed: () async {
          if (!await showSearchUsersDialog(context, _controller)) {
            return;
          }

          // Reset forced flag before redraw list.
          _controller.resetForced();

          await runWithProgress(context, _controller.search);
        },
      ),
      Container(
        width: 40, height: 40, margin: const EdgeInsets.only(right: 10),
        child: FloatingActionButton(
          heroTag: "AddFriends",
          child: const Icon(Icons.person_add),
          onPressed: () => Navigator.of(
            context, rootNavigator: true,
          ).push(CupertinoPageRoute(
              builder: (_) => const FriendAddDialog(),
              fullscreenDialog: true,
            ),
          ),
        ),
      ),
    ],
  );

  Widget _buildList(BuildContext context, WidgetRef ref) => FriendList(
    key: const PageStorageKey(0),
    physics: const AlwaysScrollableScrollPhysics(),
    provider: _provider,
    cellConfig: friendCellConfigWith(context, ref, _controller),
    onError: (e, s) => showError(context, e, s),
  );

  Future<void> _checkFriendRequests(BuildContext context) async {
    var registered = await checkFriendRequests(root);
    if (registered?.isNotEmpty == true) {
      _mayDepositPassphrase(context, registered!);
      _refresh(true);
    }
  }

  Future<void> _mayDepositPassphrase(
    BuildContext context,
    Iterable<Friend> registered,
  ) async {
    var deposited = await mayDepositPassphraseToFriends(root, registered);
    if (deposited.isNotEmpty) {
      showPassphraseDepositedDialog(context, deposited);
    }
  }
}
