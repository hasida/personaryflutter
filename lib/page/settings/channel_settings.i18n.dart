import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Auto disclose",
    "ja_jp": "自動開示",
  } +
  {
    "en_us": "Public channel",
    "ja_jp": "公開チャネル",
  } +
  {
    "en_us": "Info channel",
    "ja_jp": "広報チャネル",
  } +
  {
    "en_us": "Channel settings",
    "ja_jp": "チャネル設定",
  };

  String get i18n => localize(this, t);
}
