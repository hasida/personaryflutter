import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Notification",
    "ja_jp": "通知",
  } +
  {
    "en_us": "Terms of use",
    "ja_jp": "利用規約",
  } +
  {
    "en_us": "Privacy policy",
    "ja_jp": "プライバシーポリシー",
  } +
  {
    "en_us": "Support",
    "ja_jp": "お問い合わせ",
  } +
  {
    "en_us": "Rate app",
    "ja_jp": "アプリを評価",
  } +
  {
    "en_us": "Licenses",
    "ja_jp": "ライセンス",
  } +
  {
    "en_us": "Assemblogue Inc.",
    "ja_jp": "アセンブローグ株式会社",
  } +
  {
    "en_us": "Meditrina Inc.",
    "ja_jp": "Meditrina Inc.",
  } +
  {
    "en_us": "Delete my account",
    "ja_jp": "アカウント削除",
  } +
  {
    "en_us": "Are you sure to delete your account?",
    "ja_jp": "アカウントを削除してもよろしいですか？",
  } +
  {
    "en_us": "Deleted account cannot be restored. Are you sure to delete your account?",
    "ja_jp": "削除したアカウントは元に戻せません。アカウントの削除を実行してもよろしいですか？",
  } +
  {
    "en_us": "Deleted account cannot be restored. Are you sure to delete your account?\n This is the final confirmation.",
    "ja_jp": "削除したアカウントは元に戻せません。アカウントの削除を実行してもよろしいですか？これが最終確認です。",
  };

  String get i18n => localize(this, t);
}
