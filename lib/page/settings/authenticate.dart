import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show PlatformException;
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/error_codes.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../util.dart';
import 'authenticate.i18n.dart';

Future<bool> authenticate(BuildContext context) async {
  if (!isMobile && !Platform.isWindows) return true;

  var auth = LocalAuthentication();
  try {
    if (!await auth.isDeviceSupported()) return true;

    return await auth.authenticate(
      localizedReason: "Please authenticate to access your passphrase.".i18n,
      options: const AuthenticationOptions(
        useErrorDialogs: false,
        stickyAuth: true,
      ),
    );
  } catch (e, s) {
    if (e is PlatformException) {
      switch (e.code) {
        case passcodeNotSet: return true;
        case notAvailable: return false;
      }
    }
    showError(context, e, s);
    return false;
  }
}

Future<String?> currentPassphraseOf(
  BuildContext context,
  Root thisRoot,
) => runWithProgress(
  context, () => passphraseOf(thisRoot), onError: (e, s) => print("$e\n$s"),
);
