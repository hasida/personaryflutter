import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget.dart' show StatelessConsumerPlrWidget;
import 'account_settings.dart';
import 'channel_settings.dart';
import 'health_settings.dart';
import 'settings_page.i18n.dart';
import 'other_settings.dart';

class SettingsPage extends StatelessConsumerPlrWidget {
  const SettingsPage({ super.key });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: Builder(
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: Text('Settings'.i18n),
        ),
        body: PageScrollView(
          child: const Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const AccountSettings(),
              const HealthSettings(),
              const ChannelSettings(),
              const OtherSettings(),
            ],
          ),
        ),
      ),
    ),
  );
}
