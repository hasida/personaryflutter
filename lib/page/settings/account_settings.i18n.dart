import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "PLR passphrase deposits",
    "ja_jp": "PLRパスフレーズ預け先",
  } +
  {
    "en_us": "Confirm/reset PLR passphrase",
    "ja_jp": "PLRパスフレーズ確認/再設定",
  } +
  {
    "en_us": "App definitions",
    "ja_jp": "アプリ定義",
  } +
  {
    "en_us": "Logout",
    "ja_jp": "ログアウト",
  } +
  {
    "en_us": "Please authenticate to access your passphrase.",
    "ja_jp": "パスフレーズにアクセスするには認証してください。",
  } +
  {
    "en_us": "Please remember the below passphrase because you need it in order to log in later. Your can no longer access your data if you cannot log in.",
    "ja_jp": "下記はPLRのパスフレーズです。パスフレーズは再度ログインする際に必要なので決して忘れないように記録しておいて下さい。ログインできなくなったらデータにアクセスする手段はありません。",
  } +
  {
    "en_us": "Failed to logout.",
    "ja_jp": "ログアウトに失敗しました。",
  } +
  {
    "en_us": "[Passphrase retrieval error]",
    "ja_jp": "[パスフレーズ取得エラー]",
  } +
  {
    "en_us": "Are you sure to logout?",
    "ja_jp": "ログアウトしてもよろしいですか?",
  } +
  {
    "en_us": "Register profile information",
    "ja_jp": "プロフィールの登録",
  } +
  {
    "en_us": "Edit profile information",
    "ja_jp": "プロフィールの編集",
  } +
  {
    "en_us": "Publish profile",
    "ja_jp": "プロフィールの公開",
  };

  String get i18n => localize(this, t);
}
