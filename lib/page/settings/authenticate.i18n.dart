import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
      {
        "en_us": "Please authenticate to access your passphrase.",
        "ja_jp": "パスフレーズにアクセスするには認証してください。",
      };

  String get i18n => localize(this, t);
}
