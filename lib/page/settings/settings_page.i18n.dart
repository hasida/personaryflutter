import "package:i18n_extension/i18n_extension.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  {
    "en_us": "Settings",
    "ja_jp": "設定",
  };

  String get i18n => localize(this, t);
}
