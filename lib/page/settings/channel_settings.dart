import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../widget.dart';
import '../../dialog/auto_disclose_setting/auto_disclose_setting_dialog.dart';
import '../../dialog/info_channel_setting/info_channel_setting_dialog.dart';
import '../../dialog/public_channel_setting/public_channel_setting_dialog.dart';
import '../../page/data_settings/data_settings_page.dart';
import 'channel_settings.i18n.dart';
import 'setting_item.dart';

class ChannelSettings extends StatelessConsumerPlrWidget {
  const ChannelSettings({ super.key });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      SettingItem(
        "Auto disclose".i18n,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => AutoDiscloseSettingDialog(),
            fullscreenDialog: true,
          ),
        ),
      ),
      SettingItem(
        "Public channel".i18n,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => PublicChannelSettingDialog(),
            fullscreenDialog: true,
          ),
        ),
      ),
      SettingItem(
        "Info channel".i18n,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => InfoChannelSettingDialog(),
            fullscreenDialog: true,
          ),
        ),
      ),
      SettingItem(
        "Channel settings".i18n,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => DataSettingsPage(account)
          ),
        ),
      ),
    ],
  );
}
