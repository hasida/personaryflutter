import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../widget.dart';
import '../../dialog/health_data_coordination/health_data_coordination_dialog.dart';
import 'health_settings.i18n.dart';
import 'setting_item.dart';

class HealthSettings extends StatelessConsumerPlrWidget {
  const HealthSettings({ super.key });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      if (isMobile) SettingItem(
        "Health data coordination".i18n,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => const HealthDataCoordinationDialog(),
            fullscreenDialog: true,
          ),
        ),
      ),
    ],
  );
}
