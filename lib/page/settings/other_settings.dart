import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:plr_ui/plr_ui.dart' show showConfirmDialog, runWithProgress;

import '../../constants.dart';
import '../../logic.dart';
import '../../util.dart';
import '../../widget.dart' show StatelessConsumerPlrWidget;
import '../sign/sign_page.dart';
import '../web_view/web_view_page.dart';
import 'other_settings.i18n.dart';
import 'authenticate.dart';
import 'setting_item.dart';

class OtherSettings extends StatelessConsumerPlrWidget {
  const OtherSettings({ super.key });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      SettingItem(
        "Terms of use".i18n, onPressed: () => openWebViewPage(
          context, "Terms of use".i18n, termsOfUseLink,
        ),
      ),
      SettingItem(
        "Privacy policy".i18n, onPressed: () => openWebViewPage(
          context, "Privacy policy".i18n, privacyPolicyLink,
        ),
      ),
      SettingItem(
        "Support".i18n, onPressed: () => openWebViewPage(
          context, "Support".i18n, supportLink,
        ),
      ),
      /*
      SettingItem(
        "Rate app".i18n, onPressed: () => launch(
          Platform.isIOS ? rateLinkIOS : rateLinkAndroid,
          forceSafariVC: false, forceWebView: false,
        );
      ),
      */
      SettingItem(
        "Licenses".i18n, onPressed: () async => showLicensePage(
          context: context,
          applicationName: appName,
          // PackageInfo process may be return immediately.
          applicationVersion: (await PackageInfo.fromPlatform()).version,
          applicationIcon: Image.asset(appIcon, width: 128, height: 128),
          applicationLegalese: "© ${appLegalese.i18n}",
        ),
      ),
      const Divider(
        height: 20,
        thickness: 0,
        indent: 20,
        endIndent: 20,
      ),
      SettingItem("Delete my account".i18n, showArrow: false,
        onPressed: () => _confirmDeleteAccountData(context, ref),
      ),
    ],
  );

  Future<void> _confirmDeleteAccountData(
    BuildContext context,
    WidgetRef ref,
  ) async {
    if (!await authenticate(context)) return;

    var currentPassphrase = await currentPassphraseOf(context, root);
    if (currentPassphrase == null) {
      currentPassphrase = "[Passphrase retrieval error]".i18n;
    }

    final deleteAccount = "Delete my account".i18n;
    final scrollController = ScrollController();

    showConfirmDialog(
      context, deleteAccount,
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: CupertinoScrollbar(
              controller: scrollController,
              thumbVisibility: true,
              child: SingleChildScrollView(
                controller: scrollController,
                child: _confirmDeleteAccountMessage,
              ),
            ),
          ),
          const SizedBox(height: 16)
        ],
      ),
      okButtonLabel: deleteAccount,
      onOk: () =>
          showConfirmDialog(
            context, deleteAccount,
            "Are you sure to delete your account?".i18n,
            onOk: () => showConfirmDialog(
              context, deleteAccount,
              "Deleted account cannot be restored. Are you sure to delete your account?\n This is the final confirmation.".i18n,
              onOk: () => _deleteAccount(context, ref),
            ),
          ),
    );
  }

  Future<void> _deleteAccount(
    BuildContext context,
    WidgetRef ref,
  ) => runWithProgress(
    context, () async {
      await deleteAccount(ref);
    },
    onError: (e, s) => showError(context, e, s),
    onFinish: (_) => Navigator.of(
      context, rootNavigator: true,
    ).pushAndRemoveUntil(
      CupertinoPageRoute(builder: (_) => const SignPage()), (_) => false,
    ),
  );

  RichText get _confirmDeleteAccountMessage => RichText(
    text: TextSpan(
      children: [
        const TextSpan(
            style: const TextStyle(fontSize: 16, color: Colors.red),
            text: "! "
        ),
        TextSpan(
          style: const TextStyle(fontSize: 16, color: Colors.black),
          text: "Deleted account cannot be restored. Are you sure to delete your account?".i18n,
        ),
      ],
    ),
  );
}
