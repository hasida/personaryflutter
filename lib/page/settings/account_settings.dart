import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../logic.dart';
import '../../util.dart';
import '../../widget.dart' show StatelessConsumerPlrWidget;
import '../../dialog/app_definition/app_definition_dialog.dart';
import '../../dialog/passphrase_depositing/passphrase_depositing_dialog.dart';
import '../sign/sign_page.dart';
import 'account_settings.i18n.dart';
import 'setting_item.dart';
import 'authenticate.dart';

class AccountSettings extends StatelessConsumerPlrWidget {
  const AccountSettings({ super.key });

  @override
  Widget doBuild(BuildContext context, WidgetRef ref) => SafeArea(
    child: _buildPage(context, ref),
  );

  Widget _buildPage(BuildContext context, WidgetRef ref) => Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      SettingItem(
        "PLR passphrase deposits".i18n, showArrow: true,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => const PassphraseDepositingDialog(),
            fullscreenDialog: true,
          ),
        ),
      ),
      SettingItem(
        "Confirm/reset PLR passphrase".i18n, showArrow: false,
        onPressed: () => _changePassphrase(context),
      ),
      SettingItem(
        "App definitions".i18n, showArrow: true,
        onPressed: () => Navigator.push(
          context, CupertinoPageRoute(
            builder: (_) => AppDefinitionDialog(),
            fullscreenDialog: true,
          ),
        ),
      ),
      SettingItem("Logout".i18n, showArrow: false,
        onPressed: () => _confirmDeleteAccount(context, ref),
      ),
    ],
  );

  Future<void> _changePassphrase(BuildContext context) async {
    if (!await authenticate(context)) return;

    var currentPassphrase = await currentPassphraseOf(context, root);

    var passphrase = await showPassphraseDialog(
      context, initialValue: currentPassphrase, isEditor: true,
    );
    if ((passphrase == null) || (passphrase == currentPassphrase)) return;

    await runWithProgress(
      context, () async {
        await changePassphrase(root, passphrase);
        await dismissPassphraseChangedNotification(storage);
      }, onError: (e, s) => processNeedNetworkException(context, e, s),
    );
  }

  RichText get _confirmLogoutMessage => RichText(
    text: TextSpan(
      children: [
        const TextSpan(
            style: const TextStyle(fontSize: 16, color: Colors.red),
            text: "! "
        ),
        TextSpan(
          style: const TextStyle(fontSize: 16, color: Colors.black),
          text: "Please remember the below passphrase because you need it in order to log in later. Your can no longer access your data if you cannot log in.".i18n,
        ),
      ],
    ),
  );

  Widget _confirmPassphraseOf(String currentPassphrase) => TextButton(
    style: TextButton.styleFrom(
      foregroundColor: Colors.black87,
    ),
    child: Row(
      children: [
        Expanded(
          child: Center(
            child: Text(
              currentPassphrase,
              style: const TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        const Icon(Icons.copy),
      ],
    ),
    onPressed: () => Clipboard.setData(
      ClipboardData(text: currentPassphrase),
    ),
  );

  Future<void> _confirmDeleteAccount(
    BuildContext context,
    WidgetRef ref,
  ) async {
    if (!await authenticate(context)) return;

    var currentPassphrase = await currentPassphraseOf(context, root);
    if (currentPassphrase == null) {
      currentPassphrase = "[Passphrase retrieval error]".i18n;
    }

    final logout = "Logout".i18n;
    final scrollController = ScrollController();

    showConfirmDialog(
      context, logout,
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: CupertinoScrollbar(
              controller: scrollController,
              thumbVisibility: true,
              child: SingleChildScrollView(
                controller: scrollController,
                child: _confirmLogoutMessage,
              ),
            ),
          ),
          const SizedBox(height: 16),
          _confirmPassphraseOf(currentPassphrase),
        ],
      ),
      okButtonLabel: logout,
      onOk: () => showConfirmDialog(
        context, logout,
        "Are you sure to logout?".i18n,
        onOk: () => _removeAccount(context, ref),
      ),
    );
  }

  Future<void> _removeAccount(
    BuildContext context,
    WidgetRef ref,
  ) => runWithProgress(
    context, () async {
      if (!await removeAccount(ref)) {
        showMessage(context, "Failed to logout.".i18n);
      }
    },
    onError: (e, s) => showError(context, e, s),
    onFinish: (_) => Navigator.of(
      context, rootNavigator: true,
    ).pushAndRemoveUntil(
      CupertinoPageRoute(builder: (_) => const SignPage()), (_) => false,
    ),
  );
}
