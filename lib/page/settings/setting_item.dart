import 'package:flutter/material.dart';

const _settingItemHeight = 48.0;
const _settingItemForegroundColor = Colors.black87;
const _settingItemBorderColor = Colors.grey;
const _settingItemTextStyle = TextStyle(color: _settingItemForegroundColor);

class SettingItem extends StatelessWidget {
  final String text;
  final bool showArrow;
  final double? height;
  final Color? backgroundColor;
  final Color? borderColor;
  final TextStyle? textStyle;
  final VoidCallback? onPressed;
  const SettingItem(
    this.text, {
      this.showArrow = true,
      this.height,
      this.backgroundColor,
      this.borderColor,
      this.textStyle,
      this.onPressed,
  });

  @override
  Widget build(BuildContext context) => Container(
    height: height ?? _settingItemHeight,
    decoration: BoxDecoration(
      color: backgroundColor ?? Theme.of(context).cardTheme.color,
      border: Border(
        bottom: BorderSide(color: borderColor ?? _settingItemBorderColor),
      ),
    ),
    child: TextButton(
      style: TextButton.styleFrom(
        padding: const EdgeInsets.symmetric(horizontal: 16),
      ),
      onPressed: onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(text, style: textStyle ?? _settingItemTextStyle),
          if (showArrow) const Icon(
            Icons.keyboard_arrow_right, color: _settingItemForegroundColor,
          ),
        ],
      ),
    ),
  );
}
