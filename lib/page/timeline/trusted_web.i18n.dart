import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart' show unknownTranslation;
import "../../logic.dart";

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  unknownTranslation +
  {
    "en_us": "Credential",
    "ja_jp": "証明書",
  } +
  {
    "en_us": "Issuer",
    "ja_jp": "発行者",
  } +
  {
    "en_us": "Issuance Date",
    "ja_jp": "発行日",
  } +
  {
    "en_us": nameKey,
    "ja_jp": "名前",
  } +
  {
    "en_us": sexKey,
    "ja_jp": "性別",
  } +
  {
    "en_us": dateOfBirthKey,
    "ja_jp": "誕生日",
  } +
  {
    "en_us": achievementKey,
    "ja_jp": "業績",
  } +
  {
    "en_us": descriptionKey,
    "ja_jp": "説明",
  } +
  {
    "en_us": achievedLevelKey,
    "ja_jp": "達成度",
  } +
  {
    "en_us": creditsEarnedKey,
    "ja_jp": "取得単位",
  } +
  {
    "en_us": "Verify",
    "ja_jp": "検証",
  } +
  {
    "en_us": "Create VP",
    "ja_jp": "VP生成",
  } +
  {
    "en_us": "Error",
    "ja_jp": "エラー",
  } +
  {
    "en_us": "Failed to load document to verify.\n- %s",
    "ja_jp": "検証対象のドキュメントが取得できませんでした。\n- %s",
  } +
  {
    "en_us": "Verification Error",
    "ja_jp": "検証エラー",
  } +
  {
    "en_us": "Document verification failed.",
    "ja_jp": "ドキュメントの検証に失敗しました。",
  } +
  {
    "en_us": "Verification Success",
    "ja_jp": "検証成功",
  } +
  {
    "en_us": "Document has been successfully verified.",
    "ja_jp": "ドキュメントは正常に検証されました。",
  } +
  {
    "en_us": "The channel has no RSA private key for the DID.",
    "ja_jp": "このチャネルには、DIDに対応するRSA秘密鍵が保存されていません。",
  } +
  {
    "en_us": "VP creation has beend completed.",
    "ja_jp": "VPの生成が完了しました。",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}

