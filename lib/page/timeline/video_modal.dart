import 'dart:async';
import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:mime/mime.dart';
import 'package:rxdart/subjects.dart';
import 'package:video_player/video_player.dart';
import 'package:dart_vlc/dart_vlc.dart' as VLC;
import 'package:plr_ui/plr_ui.dart';
import '../../util/show_message.dart';

import 'timeline.i18n.dart';

const Color _playButtonColor = const Color(0xCCFFFFFF);

/// 動画を再生するモーダルダイアログ
/// factoryを使用し環境によって使い分ける
abstract class VideoModal extends ModalRoute<void> {
  VideoModal();

  factory VideoModal.fromFile(io.File file)
    => isMobile ?
      _VideoModalForMobile.fromFile(file) :
      _VideoModalForDesktop.fromFile(file);

  factory VideoModal.fromMmdata(FileMmdata mmdata)
    => isMobile ?
      _VideoModalForMobile.fromMmdata(mmdata) :
      _VideoModalForDesktop.fromMmdata(mmdata);

  /// 背景色
  @override
  Color get barrierColor => Colors.black.withOpacity(0.8);

  /// 背景をタップしたとき閉じるかどうか
  @override
  bool get barrierDismissible => false;

  /// 背景のラベル
  @override
  String? get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  bool get opaque => false;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 100);

  void onError(var e, var s){
    print('$e');
    showError(e, s);
  }

  Stream<io.File> getFileFromMmdata(FileMmdata mmdata) async* {
    await for (final ret in mmdata.content.handleError(onError)) {
      /// 表示が固まるのを避けるため少し待つ
      await Future.delayed(const Duration(milliseconds: 100));

      if (cancelFlag){
        /// 既にダイアログが閉じていれば以降の処理はしない
        await ret.value.drain();
        return;
      }
      final contentStream = ret.value.stream.handleError(onError);

      /// 仮ファイル作成、formatから拡張子を推定できれば付与する
      final tmpPath = (await paths.temporaryDirectory).path +
        (io.Platform.isWindows ? r'\' : '/') +
        (mmdata.id?.replaceAll('/', '_') ?? '') +
        (mmdata.format != null ? ('.' + (extensionFromMime(mmdata.format!) ?? mmdata.format!)) : '') ;
      final tmpFile = io.File(tmpPath);
      _tmpFiles.add(tmpFile);

      /// 仮ファイルに書き込み
      final sink = tmpFile.openWrite();
      await sink.addStream(contentStream);
      await sink.flush();
      await sink.close();

      yield tmpFile;
    }
  }

  /// 読み込み中にダイアログが閉じた場合のためのフラグ
  bool cancelFlag = false;

  /// 作成した仮ファイル（閉じる際に削除する）
  final _tmpFiles = <io.File>[];

  @override
  void dispose() {
    cancelFlag = true;
    super.dispose();
    for (final file in _tmpFiles) {
      file.delete();
    }
  }
}


/// 動画を再生するモーダルダイアログ
/// Android/iOS用
class _VideoModalForMobile extends VideoModal {

  /// 動画の再生を制御するクラス
  VideoPlayerController? _videoPlayerController;

  /// 動画の読み込み完了時に描画更新するためのstream
  final StreamController<VideoPlayerController> _streamController = BehaviorSubject();
  Stream<VideoPlayerController> get _stream => _streamController.stream;

  /// 動画の再生/停止時に描画更新するためのstream
  final StreamController<bool> _playStatusController = BehaviorSubject();
  Stream get _playStatusStream => _playStatusController.stream;

  /// ファイルから読み込んで再生する
  _VideoModalForMobile.fromFile(io.File file) {
    final videoController = VideoPlayerController.file(file);
    videoController.initialize().then((_) {
      _streamController.sink.add(videoController);
    });
    videoController.addListener(() {
      _playStatusController.sink.add(videoController.value.isPlaying);
    });
  }

  /// mmdataから読み込んで仮ファイルに保存したうえで再生する
  _VideoModalForMobile.fromMmdata(FileMmdata mmdata) {
    /// mmdataの内容読み込み
    /// (listenはクラウドとの同期次第で複数回呼ばれる可能性がある)
    getFileFromMmdata(mmdata).listen((tmpFile) async {
      /// Android/iOSの場合
      if (_videoPlayerController != null) {
        final value = _videoPlayerController!.value;
        while (value.isPlaying) {
          /// 既に再生されている場合は終わるまで待つ
          await Future.delayed(value.duration - value.position);
        }
      }

      /// Controllerを新規生成
      final controller = VideoPlayerController.file(tmpFile);
      controller.initialize().then((_) {
        /// 読み込み完了後に描画更新
        _streamController.sink.add(controller);
      });
      controller.addListener(() {
        /// 再生時や停止時にlistenerが呼ばれるので描画更新
        _playStatusController.sink.add(controller.value.isPlaying);
      });
    });
  }

  @override
  void dispose() {
    _streamController.close();
    _playStatusController.close();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation)
    => Material(
      type: MaterialType.transparency,
      child: StreamBuilder<VideoPlayerController>(
        stream: _stream,
        builder: (context, snapshot) {
          if (snapshot.data is VideoPlayerController) {
            _videoPlayerController = snapshot.data;
            if (_videoPlayerController!.value.isInitialized) {
              return GestureDetector(
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    Center(
                      /// ビューの大きさを動画の縦横比に合わせて中央に配置
                      child: AspectRatio(
                        aspectRatio: _videoPlayerController!.value.aspectRatio,
                        child: VideoPlayer(_videoPlayerController!),
                      )
                    ),
                    StreamBuilder<bool>(
                      stream: _playStatusStream as Stream<bool>?,
                      initialData: false,
                      builder: (context, snapshot) => (snapshot.data != true) ?
                        /// 再生していない場合は中央に再生アイコンを表示
                        const Center(
                          child: const Icon(
                            Icons.play_arrow,
                            color: _playButtonColor,
                            size: 100,
                          )
                        ) : Container(),
                    ),
                    SafeArea(
                      child: Container(
                        alignment: Alignment.topRight,
                        padding: const EdgeInsets.only(right: 20, top: 5),
                        child: InkWell(
                          child: Text(
                            '×',
                            style: TextStyle(
                              height: 1.0,
                              fontSize: 60,
                              color: Colors.white,
                              backgroundColor: Colors.grey.withOpacity(0.7),
                            ),
                          ),
                          onTap: () {
                            if (_videoPlayerController!.value.isPlaying) {
                              _videoPlayerController!.pause();
                            }
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                /// タップで再生/停止
                onTap: () => _videoPlayerController!.value.isPlaying ?
                  _videoPlayerController!.pause() :
                  _videoPlayerController!.play(),
              );
            }
          }
          else if (snapshot.data == false) {
            /// falseが来たらダイアログを閉じる
            Navigator.of(context).pop();
          }

          /// 読み込み中表示
          return Center(
            child: Container(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Loading video...'.i18n),
                ],
              ),
            )
          );
        },
      ),
    );
}


/// 動画を再生するモーダルダイアログ
/// Windows/Mac用
class _VideoModalForDesktop extends VideoModal {
  final VLC.Player player = VLC.Player(id: 69420);

  final StreamController<io.File> _fileStreamController = StreamController();

  bool loaded = false;

  _VideoModalForDesktop.fromFile(io.File file) {
    _fileStreamController.sink.add(file);
  }

  _VideoModalForDesktop.fromMmdata(FileMmdata mmdata) {
    getFileFromMmdata(mmdata).listen((file) {
      _fileStreamController.sink.add(file);
    });
  }

  @override
  void dispose() {
    player.stop();
    super.dispose();
    _fileStreamController.close();
  }

  /// 背景をタップしたとき閉じるかどうか
  @override
  bool get barrierDismissible => true;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation)
    => Material(
        type: MaterialType.transparency,
        child: SafeArea(
          child: Stack(
            alignment: Alignment.center,
            children: [
              VLC.Video(
                player: player,
              ),
              StreamBuilder<io.File>(
                stream: _fileStreamController.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (!loaded) {
                      final file = VLC.Media.file(snapshot.data!);
                      player.open(file, autoStart: true);
                      player.setPlaylistMode(VLC.PlaylistMode.single);
                      loaded = true;
                    }

                    return Container();
                  }
                  else {
                    /// 読み込み中表示
                    return Center(
                      child: Container(
                        padding: const EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Loading video...'.i18n),
                          ],
                        ),
                      )
                    );
                  }
                }
              ),
              Container(
                alignment: Alignment.topRight,
                padding: const EdgeInsets.only(right: 20, top: 5),
                child: InkWell(
                  child: Text(
                    '×',
                    style: TextStyle(
                      height: 1.0,
                      fontSize: 60,
                      color: Colors.white,
                      backgroundColor: Colors.grey.withOpacity(0.7),
                    ),
                  ),
                  onTap: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          ),
        ),
      );
}
