import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart' as intl;
import 'package:open_file_safe/open_file_safe.dart';
import 'package:path/path.dart' as path;
import 'package:personaryFlutter/dialog/api_dialog/api_dialog.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:plr_ui/plr_ui.dart';

import '../../constants.dart';
import '../../util.dart';
import '../../widget.dart';
import '../../logic/channel.dart';
import '../../util/timeline_api.dart';
import '../channel_summary/channel_summary_page.dart';
import 'create_meeting.dart';
import 'timeline.i18n.dart';
import 'timeline_item.dart';
import 'timeline_state_base.dart';

const double APPBARBOTTOM_FONTSIZE = 12;

const _tiltleStyle = const TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400, fontFamily: "Roboto",
  fontStyle: FontStyle.normal, fontSize: 16.0,
);

const _disclusedToStyle = const TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400, fontFamily: "Roboto",
  fontStyle: FontStyle.normal, fontSize: 10,
);

Future<bool> mayOpenTimelinePage<T extends Object?>(
  BuildContext? context,
  Account account,
  Timeline timeline, {
    Key? key,
    NavigatorState? navigator,
    DateTime? dateTime,
    bool direction = false,
    ItemFunction? onItemUpdated,
    ItemFunction? preItemDelete,
    ItemFunction? onItemDeleted,
    void onPop(T? value)?,
}) async {
  if ((context == null) && (navigator == null)) throw StateError(
    "Either context or navigator argument is required.",
  );
  navigator ??= navigator ?? Navigator.of(context!);

  navigator.push<T?>(
    CupertinoPageRoute(
      builder: (_) => TimelinePage(account, timeline),
    ),
  ).then((value) => onPop?.call(value));

  return true;
}

/// タイムライン画面
class TimelinePage extends TimelinePageBase {
  TimelinePage(
    super.account,
    super.timeline, {
      super.key,
      super.dateTime,
      super.direction,
      super.onItemUpdated,
      super.preItemDelete,
      super.onItemDeleted,
      super.schemataSources,
  });

  @override
  ConsumerState<TimelinePage> createState() => _TimelineState();
}

enum ScrollChangeType {
  TOP,
  BOTTOM,
  KEEP,
}

/// タイムライン画面の状態クラス
class _TimelineState extends TimelineStateBase<TimelinePage> {
  /// スクロールコントローラ
  final ItemScrollController _itemScrollController = ItemScrollController();

  /// スクロール方向
  ScrollDirection _scrollDirection = ScrollDirection.idle;

  /// タイムラインアイテムリストのインデックスリスナー
  final ItemPositionsListener _itemPositionsListener =
      ItemPositionsListener.create();
  List<ItemPosition> get _visibleItemList {
    var result = _itemPositionsListener.itemPositions.value.toList();
    result.sort((a, b) => a.index - b.index);
    return result;
  }

  /// 先頭に表示中のタイムラインアイテムのインデックス情報
  int _firstItemIndex = 0;
  ScrollChangeType _scrollChangeType = ScrollChangeType.BOTTOM;

  /// テキストフィールドのコントローラ
  final _textFieldController = TextEditingController();

  /// 現在Viewの一番上に表示されているタイムラインアイテムの日付
  var timelineFirstItemDate;
  final BehaviorSubject<String?> timelineFirstItemDateStream = BehaviorSubject();

  var opacity = 1.0;

  final _focusNode = FocusNode();

  bool _showFAB = true;
  bool getShowFAB() => _showFAB;

  @override
  void initState() {
    loading = true;
    super.initState();

    /// 表示中アイテムを知るためのリスナー登録
    _itemPositionsListener.itemPositions.addListener(_itemPositionsCallback);

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _showFAB = false;
      } else {
        _showFAB = true;
      }
      hasMenuStream.sink.add(hasMenu);
    });

    if (timeline is Channel) {
      autoDisclose(root, account, timeline as Channel);
    }
  }

  /// 破棄
  @override
  void dispose() {
    _itemPositionsListener.itemPositions.removeListener(_itemPositionsCallback);
    timelineFirstItemDateStream.close();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Future<void> didPush() async {
    /// 初回表示は最下段にスクロールさせる
    await super.didPush();
  }

  /// 周期タイマ処理
  @override
  void onTimer(Timer timer) {
    /// 周期処理時はスクロールさせない
    /// タイムラインアイテムロード(周期処理)
    super.onTimer(timer);
  }

  Widget createTimelineItem(List<RecordEntity> timelineItemList, int index) {
    bool dispDateFlag = true;

    RecordEntity recordEntity = timelineItemList[index];

    /// 直前アイテム取得
    RecordEntity? aboveItem = (index > 0) ? timelineItemList[index - 1] : null;

    if (aboveItem != null) {
      /// 対象Itemの日付
      DateTime? itemBegin = recordEntity.begin;

      /// 1アイテム以上の場合は現アイテムと直前アイテムの年月日を比較
      DateTime? aboveBegin = aboveItem.begin;
      if (
        (itemBegin != null) && (aboveBegin != null) &&
        (itemBegin.year == aboveBegin.year) &&
        (itemBegin.month == aboveBegin.month) &&
        (itemBegin.day == aboveBegin.day)
      ) {
        /// 同年月日の場合は日付エリアを表示しないのでフラグをfalse
        dispDateFlag = false;
      }
    }

    /// ※ここでstylesheetにおうじて表示アイテムをフィルタ

    return GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
            key: Key("${timelineItemList.hashCode} ${recordEntity.entity?.id} ${readOnly}"),
            margin: const EdgeInsets.only(bottom: 10),
            child: TimelineItem(
                account,
                recordEntity,
                dispDateFlag,
                timeline,
                userPlrId,
                readOnly,
                timelineSchemata,
                (entity) => onItemUpdated(entity),
                (entity) => preItemDelete(entity),
                (entity) => onItemDeleted(entity),
            )
        )
    );
  }

  /// DateTime表示フォーマット
  final _dateFormatter = intl.DateFormat('yyyy-MM-dd (E.)'.i18n);
  /// 表示するタイムラインアイテムの日付を取得
  void _updateFirstItemDate(List<RecordEntity> list) {
    if (list.length == 0) {
      /// タイムラインアイテムなし
      if (loading) {
        timelineFirstItemDate = '';
      } else if (loading == false) {
        /// load中でなければデータ無し
        timelineFirstItemDate = 'No data'.i18n;
      }
    } else {
      /// タイムライン日付を取得
      /// 日付表示を更新
      if (timelineSchemata != null && 0 <= _firstItemIndex && list.length >= _firstItemIndex) {
        final item = list[_firstItemIndex];
        if (item.begin != null) {
          timelineFirstItemDate = _dateFormatter.format(item.begin!.toLocal());
        }
      }
    }

    timelineFirstItemDateStream.sink.add(timelineFirstItemDate);
  }

  List<RecordEntity>? _lastItemList;
  late var _lastCallbackTime;
  /// タイムラインアイテムリストスクロール時のコールバック
  void _itemPositionsCallback() {
    if (!this.mounted) return;

    /// 表示中タイムラインアイテムのインデックス情報を取得
    final _visibleIndexes = _visibleItemList;
    if (_visibleIndexes.isEmpty) {
      print("_visibleIndexes.isEmpty");
      return;
    }

    var _now = DateTime.now();
    if (_lastItemList == null || _lastItemList!.length == 0 || _lastCallbackTime.difference(_now).inSeconds < -1) {
      _lastItemList = items;
      _lastCallbackTime = _now;
    }

    /// 先頭に表示中のタイムラインアイテムのインデックス取得
    _firstItemIndex = _visibleIndexes.first.index;
    if ((_firstItemIndex > 0) && (_firstItemIndex >= _lastItemList!.length)) {
      _firstItemIndex = _lastItemList!.length - 1;
    }

    /// 表示するタイムラインアイテムの日付を取得
    /// 日付に変更があればViewを再構築
    _updateFirstItemDate(_lastItemList!);
  }

  /// AppBarのBottom部のテキスト高さ取得
  double _getAppBarBottomTextHeight() {
    TextSpan ts = new TextSpan(text: 'No data'.i18n,
        style: Theme.of(context).textTheme.bodyLarge!.merge(
            const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontFamily: "Roboto",
                fontStyle: FontStyle.normal,
                fontSize: APPBARBOTTOM_FONTSIZE)));
    TextPainter tp = new TextPainter(text: ts, textDirection: TextDirection.ltr);
    tp.layout();
    var textScale = MediaQuery.textScalerOf(context).scale(1.0);
    if (textScale < 1.0) textScale = 1.0;
    return tp.height * textScale;
  }

  /// カレンダーによる日付選択処理
  @override
  Future<bool> selectDate(BuildContext context) async {
    if (!await super.selectDate(context)) return false;
    _firstItemIndex = 0;
    _scrollChangeType = ScrollChangeType.TOP;
    return true;
  }

  @override
  void onFinish() async{
    super.onFinish();
  }

  Future<void> _waitForStoreNewItem(Entity entity) async {
    // Sigh... No way to detect completion of synchronization of new item.
    while (true) {
      if (!entity.isNewTimelineItem) return;
      await Future.delayed(const Duration(milliseconds: 100));
    }
  }

  /// アイテム追加・編集時コールバック
  @override
  void onItemUpdated(RecordEntity? recordEntity) {
    if ((recordEntity != null) && (timeline is Channel)) {
      final entity = recordEntity.entity!;
      Future(() async {
          await _waitForStoreNewItem(entity);
          await _mayMailTimelineUpdates(
            timeline as Channel, recordEntity, Item(entity, timeline), false,
          ).catchError(onError);
      });
    }
    super.onItemUpdated(recordEntity);

    _mayShowLastItem(recordEntity);
  }

  @override
  void onItemSyncUpdated(RecordEntity? recordEntity) {
    if (recordEntity == null) return;
    _mayShowLastItem(recordEntity);
  }

  /// タイムライン上にある一番下（最新）のアイテムが表示されるように
  /// スクロール位置を変更するメソッドです。
  void _mayShowLastItem(RecordEntity? recordEntity) {
    if (recordEntity == null) return;

    if (recordEntity.entity?.id == items.last.id) {
      // 引数のRecordEntityが一番下のアイテムだった場合のみ処理
      int index = items.length - 1;
      Iterable<ItemPosition> positions =
        _itemPositionsListener.itemPositions.value;

      if (!positions.any((e) => e.index == index)) {
        // 一番下のアイテムが表示されていない場合、表示するようスクロール位置を変更
        _itemScrollController.jumpTo(index: index, alignment: 0.8);
      }
    }
  }

  static final _formatResults = Expando<FormatResult>();

  /// アイテム削除前コールバック
  @override
  void preItemDelete(RecordEntity? recordEntity) {
    if ((recordEntity != null) && (timeline is Channel)) {
      _formatResults[recordEntity] = recordEntity.format();
    }
    super.preItemDelete(recordEntity);
  }

  /// アイテム削除時コールバック
  @override
  void onItemDeleted(RecordEntity? recordEntity) {
    if ((recordEntity != null) && (timeline is Channel)) {
      _mayMailTimelineUpdates(
        timeline as Channel, recordEntity,
        Item(recordEntity.entity!, timeline), true,
      ).catchError(onError);
    }
    /// 周期処理時はスクロールさせない
    super.onItemDeleted(recordEntity);
  }

  Future<void> _mayMailTimelineUpdates(
    Channel channel,
    RecordEntity recordEntity,
    Item item,
    bool deleted,
  ) async {
    if (!channel.isMailTimelineUpdates) return;

    Set<String> recipients = (
      channel.disclosedToUsers(true).map(
        (u) => u.plrId?.email,
      ).toSet()
      ..add(channel.plrId?.email)
      ..remove(root.plrId.email)
      ..remove(channel.storage?.plrId.email)
      ..remove(channel.masterStorage.plrId.email)
      ..remove(null)
    ).cast<String>();

    if (recipients.isEmpty) return;

    var name = channel.name?.defaultValue ?? "Unknown".i18n;

    String body; {
      var link = (await item.toUri()).toString();
      var updator = await _updatorOf(channel);
      var updates = _buildUpdates(recordEntity, deleted);

      var sb = StringBuffer("Timeline of the channel `%s' has been updated.\nPlease check the following link:\n\n%s".i18n.fill([ name, link ]));

      if ((updator != null) || updates.isNotEmpty) {
        sb.write("\n\n");
        if (updator != null) {
          sb..write("Updated by: %s".i18n.fill([ updator ]))..write("\n");
        }
        if (updates.isNotEmpty) {
          final header = deleted ? "Deletion:\n%s" : "Update:\n%s";
          sb..write(header.i18n.fill([ updates ]))..write("\n");
        }
      }
      body = sb.toString();
    }

    await queueMail(
      storage,
      bcc: recipients,
      subject: "%s: Timeline has been updated - %s".i18n.fill([
          appName, name,
      ]),
      body: body,
    );
  }

  Future<String?> _updatorOf(Channel channel) async {
    Future<Map<SchemaClass, Iterable<ProfileItem>>?> profilesOf(
      HasProfile hasProfile,
    ) async {
      Map<SchemaClass, Iterable<ProfileItem>>? profiles;
      await for (
        var p in hasProfile.profileWith(
          scanOthers: false, classes: [ nameClass ],
        )
      ) {
        profiles = p;
      }
      return profiles;
    }

    Map<SchemaClass, Iterable<ProfileItem>>? profiles;
    if (channel.plrId == root.plrId) {
      var ps = await profilesOf(channel);
      if (ps?.isNotEmpty == true) profiles = ps;
    }
    if ((profiles ??= await profilesOf(publicRoot)) == null) return null;

    return recentNameTextOf(profiles!);
  }

  String _buildUpdates(RecordEntity recordEntity, bool deleted) {
    final FormatResult formatResult; try {
      if (deleted) {
        var r = _formatResults[recordEntity];
        if (r == null) return "";
        formatResult = r;
      } else formatResult = recordEntity.format();
    } finally {
      _formatResults[recordEntity] = null;
    }

    final updates = <String>[];
    if (
      (recordEntity.type.id != messageClass) &&
      (formatResult.typeLabel != null)
    ) {
      var sb = StringBuffer()
        ..write("[")..write(formatResult.typeLabel!)..write("]");

      if (formatResult.text?.isNotEmpty == true) {
        sb..write(" ")..write(formatResult.text!);
      }
      updates.add(sb.toString());
    }

    if (formatResult.hasMmdata) {
      for (var mmdata in formatResult.mmdatasById("cnt")) {
        var sb = StringBuffer();
        if (mmdata.isLiteral) {
          var t = mmdata.asLiteral.text.defaultValue;
          if (t?.isNotEmpty == true) sb.write(t!);
        } else if (mmdata.isFile) {
          sb.write("<Attachment>".i18n);

          var f = mmdata.asFile;
          var t = f.text.defaultValue;
          if (t?.isNotEmpty == true) sb..write(" (")..write(t!)..write(")");

          t = f.comment.defaultValue;
          if (t?.isNotEmpty == true) sb..write(" ")..write(t!);
        } else continue;

        updates.add(sb.toString());
      }
    }
    return updates.join("\n---\n");
  }

  /// タイムライン画面View構築
  @override
  Widget build(BuildContext context) {
    print('[TimelinePage] build()');

    var textScale = MediaQuery.textScalerOf(context).scale(1.0);
    if (textScale < 1.0) {
      textScale = 1.0;
    }

    return SafeArea(
      child: Scaffold(
        /// ヘッダー部
        appBar: AppBar(
          backgroundColor: Colors.white,
          titleSpacing: 0,
          title: GestureDetector(
            /// ソフトキーボードを閉じる
            onTap: () => FocusScope.of(context).unfocus(),
            child: switch (timeline) {
              Channel() => Consumer(
                builder: (_, ref, __) {
                  final state = ref.watch(
                    channelUpdateProvider(
                      timeline as Channel, labelOfMe: defaultLabelOfMe,
                    ),
                  ).value;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(state?.name ?? "", style: _tiltleStyle),
                      Text(
                        state?.userNames.join(", ") ?? "",
                        style: _disclusedToStyle,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  );
                },
              ),
              LifeRecord() => Text("Profile".i18n, style: _tiltleStyle),
              DisclosureHistory() => Text(
                "Disclosure history".i18n, style: _tiltleStyle,
              ),
              _ => Container(),
            },
          ),

          actions: <Widget>[
            /// プログレスインジケータ
            TimelineAppBarProgress(loadingStream.stream),

            StreamBuilder(
              stream: hasMenuStream,
              builder: (context, snapshot) {
                List<TimelineQuery>? timelineQueries;
                if (snapshot.data == true) {
                  timelineQueries = timelineSchemata?.coordinations.map(
                    (c) => c.timelineQueries,
                  ).expand((qs) => qs).toList();
                }
                var enabled = timelineQueries?.isNotEmpty == true;

                return PopupMenuButton<TimelineQuery>(
                  child: Icon(
                    Icons.assessment_outlined,
                    color: enabled ? Colors.black : Colors.grey,
                  ),
                  tooltip: 'Timeline data coordination.'.i18n,
                  enabled: enabled,
                  position: PopupMenuPosition.under,
                  onSelected: (value) => _onTimelineQuerySelected(
                    context, value,
                  ),
                  itemBuilder: (context) => [
                    for (var q in timelineQueries!) PopupMenuItem(
                      value: q,
                      child: Tooltip(
                        message: q.comment?.defaultValue ?? "",
                        triggerMode: (
                          (q.comment?.defaultValue?.isNotEmpty == true) ?
                            null : TooltipTriggerMode.manual
                        ),
                        child: Text(q.label?.defaultValue ?? q.id ?? "Unknown"),
                      ),
                    ),
                  ]
                );
              },
            ),

            if (timeline is Channel) CompactIconButton(
              icon: const Icon(Icons.link),
              tooltip: 'Copy deep link to clipboard.'.i18n,
              onPressed: () => Clipboard.setData(
                ClipboardData(
                  text: (timeline as Channel).toTimelineUri().toString(),
                ),
              ).then((_) => showMessage(
                  context, 'Deep link copied to clipboard.'.i18n,
              )),
            ),

            /// Google Meetの作成ボタン
            if (timeline is Channel && !readOnly)
              CreateMeetingButton(timeline as Channel, onDone: (uri) {
                if (uri != null) {
                  createMessage(uri).then((message) {
                    if (message == null) return;

                    Future<bool> future = message.save(timeline, false);
                    Navigator.of(context)
                        .push(ProgressModal(context, future, 'データの保存中です...'))
                        .then((_) => onItemUpdated(message));
                  });
                }
              }),
            //チャネルサマリーアイコン
            Consumer(
              builder: (context, ref, _) => switch (
                ref.watch(
                  timelineSchemataProvider(timeline),
                )
              ) {
                AsyncValue(:final value) when (
                  value?.schemata.any(
                    (s) => s.channelSummaryStyleSheets.isNotEmpty,
                  ) == true
                ) => Row(
                  children: [
                    const SizedBox(width: 5.0),
                    GestureDetector(
                      child: const Icon(Icons.bar_chart, color: Colors.black),
                      onTap: () => Navigator.push(
                        context, MaterialPageRoute(
                          builder: (context) => ChannelSummaryPage(
                            timeline as Channel,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                _ => emptyWidget,
              },
            ),
            const SizedBox(width: 5.0),
            /// カレンダーアイコン
            GestureDetector(
              child: const Icon(Icons.calendar_today, color: Colors.black),
              onTap: () {
                /// ソフトキーボードを閉じる
                FocusScope.of(context).unfocus();
                selectDate(context);
              }
            ),
            const SizedBox(width: 5.0)
          ],

          /// アイテム日付とスタイルシート
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(APPBARBOTTOM_FONTSIZE * MediaQuery.textScalerOf(context).scale(1.0)),
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
            child: Padding(
              padding:
                  const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 1.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          /// アイテム日付
                          Container(
                              height: _getAppBarBottomTextHeight(),
                              child: TimelineFirstItemDate(
                                  timelineFirstItemDateStream.stream)
                          ),
                          const SizedBox(width: 5.0),
                          /// 「データ取得中です」
                          Container(
                              height: _getAppBarBottomTextHeight(),
                              child: TimelineLoadingData(
                                  loadingStream.stream)
                          )
                        ]
                    ),
                    flex: 2,
                  ),
                  /// スタイルシート
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      if ((timeline is! Channel) ||
                          (selectedSchema != null)) {
                        showSchemaAndStyleSheetSelectDialog();
                      }
                    },
                    child: Container(
                      height: _getAppBarBottomTextHeight(),
                      padding: const EdgeInsets.only(left: 1, right: 1),
                      child: TimelineSchemaAndStyleSheet(
                        selectedSchemataAndStyleSheet.stream,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          ),
          elevation: 10,
        ),

        /// ボディー部
        body: StreamBuilder(
          stream: hasMenuStream.stream,
          builder: (_, snapshot) => switch (timelineSchemata?.timelineState) {
            null => FutureBuilder(
              future: Future.delayed(const Duration(seconds: 3)),
              builder: (_, snapshot) => switch (snapshot.connectionState) {
                ConnectionState.done => Align(
                  alignment: Alignment.center,
                  child: Text(
                    'Retrieving the schema.\nPlease wait a while...'.i18n,
                  ),
                ),
                _ => emptyWidget,
              },
            ),
            TimelineState.Disabled => Align(
              alignment: Alignment.center,
              child: Text(
                'Timeline is disabled.\nPlease select another schema.'.i18n,
              ),
            ),
            _ => _buildTimeline(context, textScale),
          },
        ),

        resizeToAvoidBottomInset: true,

        /// キーボード表示時に表示をずらす
        /// アイテム検索ボタン、アイテム追加ボタン
        floatingActionButton: Row(
          verticalDirection: VerticalDirection.up,
          /// ※アイテム検索機能は未実装のため検索ボタンを一時非表示
          /// 　それに伴いAlignmentを一時変更
////        mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            /// アイテム検索
            /// ※未実装機能のため一時非表示
/*/*          Container(
              height: 40,
              width: 40,
              margin: const EdgeInsets.only(
                  left: 30.0,
                  bottom: ((timeline is Channel) && (readOnly == false))
                      ? 35.0
                      : 0.0),
              child: FloatingActionButton(
                heroTag: "hero_search",
                backgroundColor: Colors.amberAccent,
                tooltip: 'Search class',
                child: const Icon(Icons.search, size: 20),
                onPressed: () {
                  //ボタンタップ時イベント
                  print("[AW DBG] item search button tapped.");
                  /// ソフトキーボードを閉じる
                  FocusScope.of(context).unfocus();
                },
              ),
            ),
*/*/
            /// アイテム追加
            TimelineAddEntityButton(
                account, hasMenuStream.stream, timeline,
                createRecordEntity, onItemUpdated, getShowFAB)
          ],
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }

  Widget _buildTimeline(BuildContext context, double textScale) => Column(
    mainAxisSize: MainAxisSize.max, children: <Widget>[
      Expanded(
        child: RefreshIndicator(
          /// スワイプ時のリフレッシュ処理
          onRefresh: () async {
            if (opacity == 1.0 && _scrollChangeType != ScrollChangeType.BOTTOM) {
              _scrollChangeType = ScrollChangeType.TOP;
              loadTimelineItems(force: true, direction: false);
              loadTimelineItemsSync();
            }
          },
          child: NotificationListener<ScrollNotification>(
////              child: ListView.builder(
////                controller: _scrollController,
////                itemCount: itemList.length,
////                itemBuilder: (context, index) {
////                  return createTimeline_item(timelineItemList[index]);
////                  },
////              ),
            onNotification: (notification) {
              if (notification is UserScrollNotification) {
                /// スクロール方向保持
                _scrollDirection = notification.direction;
                _scrollChangeType = ScrollChangeType.KEEP;
              } else if (notification is OverscrollNotification) {
                print("[AW DBG] Scroll over　　Direction: $_scrollDirection");
                if (_scrollDirection == ScrollDirection.reverse) {
                  /// 直前にスクロール方向が終点(下方向)の場合
                  if (!loading && opacity == 1.0) {
                    /// velocity => 実際にスクロールされた瞬間速度
                    /// 既に最下部までスクロールされていればvelocity=0になる
                    /// その場合に未来方向のロードを行う
                    if (notification.velocity == 0) {
                      _scrollChangeType = ScrollChangeType.BOTTOM;
                      reverseLoadingStream.sink.add(true);
                      loadTimelineItems(force: true, direction: true);
                    }
                  }
                }
              }
              return false;
            },
            child: _buildItemList(),
          ),
        ),
      ),
      /// メッセージ、添付ファイル追加
      if ((timeline is Channel) && (readOnly == false)) BottomAppBar(
        color: Theme.of(context).colorScheme.surface,
        child: Container(
          child: Row(
            children: <Widget>[
              //添付ファイル追加アイコン
              Expanded(
                child: Container(
                  color: Theme.of(context).colorScheme.surface,
                  child: IconButton(
                    iconSize: (18 * textScale),
                    icon: const Icon(Icons.attach_file, color: Colors.black),
                    onPressed: () async {
                      //アイコンタップ時イベント
                      print("[AW DBG] attach file icon tapped.");
                      RecordEntity? attachItem = createRecordEntity(messageClass);
                      showDialog(
                        context: context,
                        builder: (context) =>
                            EntityDialog(
                              account, EntityDialogSettings(), timeline, attachItem!,
                              false, true,
                            ),
                      ).then((value) => onItemUpdated(value));
                    },
                  ),
                ),
              ),
              //メッセージ追加テキストボックス
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 3.0, bottom: 3.0),
                  alignment: Alignment.centerLeft,
                  color: Theme.of(context).colorScheme.surface,
                  child: TextField(
                    controller: _textFieldController,
                    focusNode: _focusNode,
                    autofocus: false,
                    textAlignVertical: TextAlignVertical.center,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        backgroundColor: Colors.white.withOpacity(0.0)),
                    decoration: InputDecoration(
                        isDense: true,
                        contentPadding: const EdgeInsets.only(left: 12, top: 18),
                        hintText: 'Add comment'.i18n,
                        fillColor: Colors.teal[50],
                        filled: true,
                        border: OutlineInputBorder(
                            borderSide: const BorderSide(color: const Color(0xffaaaaaa), width: 0.5),
                            borderRadius: BorderRadius.circular(20.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: const Color(0xffaaaaaa), width: 0.5),
                            borderRadius: BorderRadius.circular(20.0))),
                    ),
                ),
                flex: 9,
              ),
              //メッセージ送信アイコン
              Expanded(
                child: Container(
                  child: IconButton(
                    iconSize: (18 * textScale),
                    icon: const Icon(Icons.send, color: Colors.black),
                    onPressed: () async {
                    //アイコンタップ時イベント
                    var inputText = _textFieldController.text;
                    RecordEntity? messageRecordEntity;
                    String? AIMessage;
                    AIConditionData? aiCondition = (timeline as Channel).channelDataSetting?.aiCondition;
                    String? url = aiCondition?.url;
                    String? parameter = aiCondition?.parameter;
                    if (url != null && parameter != null) {
                      String? inputMessage;
                      if (inputText.isEmpty) {
                        inputMessage = " ";
                      } else {
                        inputMessage = inputText;
                      }
                      messageRecordEntity = await createMessage(inputMessage);
                      if (messageRecordEntity == null) return;
                      Future<bool> future = messageRecordEntity.save(timeline, false);
                      Navigator.of(context).push(ProgressModal(context, future, 'Save data...'.i18n)).then((_) async{
                        FocusScope.of(context).unfocus();
                        onItemUpdated(messageRecordEntity);
                      });
                      String? apiKey = aiCondition!.apiKey;
                      final SharedPreferences prefs = await SharedPreferences.getInstance();
                      if (apiKey == null) {
                        String prefsApiKey = "${(timeline as Channel).id}_API_KEY";
                        bool ret = await apiDialog(context, prefsApiKey);
                        if (ret == false) return;
                        apiKey = prefs.getString(prefsApiKey);
                        if (apiKey == null) return;
                      }

                      String prefsTokenKey = "${(timeline as Channel).id}_token_key";
                      Map<String, dynamic>? systemPrompt = await timelineAPI(context, account, timeline, timelineSchemata, inputText, aiCondition, prefsTokenKey, userPlrId);
                      if (systemPrompt == null) {
                        showMessage(context,'Generated failed'.i18n);
                        return;
                      }

                      if (systemPrompt["state"]) {
                        AIMessage = systemPrompt["responseMessage"];
                        prefs.setString(prefsTokenKey, systemPrompt["token"].trim());
                      } else {
                        showMessage(context, systemPrompt["reason"]);
                      }

                      if (AIMessage == null) return;
                      RecordEntity? AIMessageRecordEntity = await createMessage(AIMessage);
                      if (AIMessageRecordEntity == null) return;
                      AIMessageRecordEntity.creator = creatorPAI;
                      await AIMessageRecordEntity.save(timeline, false);
                      onItemUpdated(AIMessageRecordEntity);
                    } else {
                      if (inputText.isEmpty) return;
                      RecordEntity? message = await createMessage(inputText);
                      if (message == null) return;
                      Future<bool> future = message.save(timeline, false);
                      Navigator.of(context).push(ProgressModal(context, future, 'Save data...'.i18n)).then((_) {
                        onItemUpdated(message);
                        FocusScope.of(context).unfocus();
                      });
                    }
                    if (this.mounted) {
                        setState(() => _textFieldController.text = "");
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  );

  Widget _buildItemList() => StreamBuilder<List<RecordEntity>>(
      stream: itemStream,
      initialData: const [],
      builder: (context, snapshot) {
        final lastVisibleIndices = _visibleItemList;

        List<RecordEntity> timelineItemList = [];
        if (snapshot.hasData) {
          timelineItemList.addAll(snapshot.data!);
        }

        int itemCount = timelineItemList.length;
        /// schemataがnullの場合はアイテムカウント0
        if (isSchemataNull) itemCount = 0;
        print("[TimelinePage] itemCount = $itemCount");

        var listView = ScrollablePositionedList.separated(
          itemBuilder: (context, index) {
            if (index < timelineItemList.length) {
              return createTimelineItem(timelineItemList, index);
            } else {
              var viewHeight = MediaQuery.of(context).size.height;
              var appBarHeight = AppBar().preferredSize.height + 10;
              var listBottom = 0.0;
              var visibleItemList = _visibleItemList;
              if (visibleItemList.isNotEmpty) {
                listBottom = visibleItemList.last.itemLeadingEdge;
              }

              if ((timeline is Channel) && (readOnly == false)) {
                /// Channel(read onlyでない)の場合
                /// AppBar(top/bottom)のHeight + 拡張分(10)
                appBarHeight = (AppBar().preferredSize.height * 2) + 10;
              }

              /// 空行の高さを算出(-0.01は補正分)
              var blankHeight = (viewHeight - appBarHeight) * (1.0 - listBottom - 0.01);
              if (listBottom > 0.9) blankHeight = blankHeight + 10;

              return Column(
                children: [
                  if (loading) Container(height: 10),

                  /// プログレスインジケータ(未来方向へのスワイプ時のloadingのみ)
                  TimelineReverseLoadingProgress(reverseLoadingStream.stream),

                  if (itemCount == 0)
                    GestureDetector(
                        onTap: () => FocusScope.of(context).unfocus(),
                        /// アイテムがないときはすべて埋めるだけの空Containerを作る
                        child: Container(height: viewHeight - appBarHeight, color: Theme.of(context).colorScheme.surface)
                    )
                  else
                    GestureDetector(
                        onTap: () => FocusScope.of(context).unfocus(),
                        ///アイテムがあるときはボタンの大きさ分だけ下に余裕を持たせる
                        child: Container(height: blankHeight, color: Theme.of(context).colorScheme.surface)
                    )
                ],
              );
            }
          },
          separatorBuilder: (context, index) => Container(),
          itemCount: itemCount + 1,
          itemScrollController: _itemScrollController,
          itemPositionsListener: _itemPositionsListener,
          initialAlignment: lastVisibleIndices.isNotEmpty ? lastVisibleIndices.first.itemLeadingEdge : 0,
          initialScrollIndex: lastVisibleIndices.isNotEmpty ? lastVisibleIndices.first.index : 0,
          physics: Platform.isIOS ? const ClampingScrollPhysics() : null,
        );

        opacity = 1.0;
        if (lastVisibleIndices.length > itemCount) {
        }
        else if (_scrollChangeType == ScrollChangeType.BOTTOM &&
            (lastVisibleIndices.isNotEmpty && lastVisibleIndices.last.index < itemCount - 1)) {
          opacity = 0;
          WidgetsBinding.instance.addPostFrameCallback((_) {
            _itemScrollController.jumpTo(index: itemCount, alignment: 0.8);
            Future.delayed(const Duration(milliseconds: 100))
                .then((_) {
              updateItems(true);
              WidgetsBinding.instance.addPostFrameCallback((_) {
                _scrollChangeType = ScrollChangeType.BOTTOM;
              });
            });
          });
        }
        else if (_scrollChangeType == ScrollChangeType.TOP &&
            (lastVisibleIndices.isNotEmpty && lastVisibleIndices.first.index > 0)) {
          opacity = 0;
          WidgetsBinding.instance.addPostFrameCallback((_) {
            _itemScrollController.jumpTo(index: 0);
            Future.delayed(const Duration(milliseconds: 100))
                .then((_) {
              updateItems(true);
              WidgetsBinding.instance.addPostFrameCallback((_) {
                _scrollChangeType = ScrollChangeType.TOP;
              });
            });
          });
        }

        return Opacity(
          opacity: opacity,
          child: listView,
        );
      }
  );

  Future<void> _onTimelineQuerySelected(
    BuildContext context,
    TimelineQuery query,
  ) async {
    FocusScope.of(context).unfocus();

    var range = await _pickDateTimeRange(context);
    if (range == null) return;

    var result = await runWithProgress(
      context, () => query.callWith(timeline, range[0], range[1]),
      onError: (e, s) => processNeedNetworkException(context, e, s),
    );
    if (result == null) return;

    if (query is ServerTimelineQuery) {
      var conn = query.serverConnection;
      if (
        (conn is HttpServerConnection) &&
        (conn.resultFormat == ResultFormat.RAW_TO_FILE)
      ) {
        await _processFileResult(context, result);
        return;
      }
    }
    await _processEntityResult(context, result);
  }

  Future<void> _processFileResult(
    BuildContext context,
    Map<String, dynamic> result,
  ) async {
    var file = result["file"];

    if (
      !await showConfirmDialog(
        context, 'The result has been saved'.i18n,
        'The coordination result has been saved in Downloads folder\n'
        'FileName: %s'.i18n.fill([ path.basename(file.path) ]),
        okButtonLabel: 'Open'.i18n,
      )
    ) return;

    if (isMobile) OpenFile.open(file.path);
    else launchUrl(file.uri);
  }

  Future<void> _processEntityResult(
    BuildContext context,
    Map<String, dynamic> result,
  ) async {
    var newEntity = await runWithProgress(
      context, () => createRecordEntityFromMap(result),
      onError: onError,
    );
    await showDialog(
      context: context,
      builder: (context) => EntityDialog(
        account, EntityDialogSettings(), timeline, newEntity!, false, true,
      ),
    ).then((entity) => onItemUpdated(entity));
  }

  Future<List<DateTime>?> _pickDateTimeRange(BuildContext context) async {
    var now = DateTime.now();

    var beginKey = GlobalKey<DateTimeFieldState>();
    var endKey = GlobalKey<DateTimeFieldState>();

    if (
      !await showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Date time range of the data to be sent'.i18n),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Expanded(
                    child: DateTimeField(
                      key: beginKey,
                      initialValue: now.subtract(const Duration(days: 30)),
                      showClearButton: false,
                    ),
                  ),
                  const Text("〜"),
                  Expanded(
                    child: DateTimeField(
                      key: endKey, initialValue: now, showClearButton: false,
                    ),
                  ),
                ],
              ),
            ]
          ),
          actions: [
            TextButton(
              child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
              onPressed: () => Navigator.pop(context, false),
            ),
            TextButton(
              child: Text('Send'.i18n),
              onPressed: () => Navigator.pop(context, true),
            )
          ],
        ),
      )
    ) return null;

    var begin = beginKey.currentState!.value!;
    var end = endKey.currentState!.value!;
    if (end.isBefore(begin)) {
      showError(context, 'Invalid date time range.'.i18n);
      return null;
    }
    return [ begin, end ];
  }
}

class TimelineFirstItemDate extends StatelessWidget {
  final Stream<String?> timelineFirstDateStream;

  TimelineFirstItemDate(this.timelineFirstDateStream, {super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
      stream: timelineFirstDateStream,
      initialData: '',
      builder: (context, snapShot) => Text(
          snapShot.hasData ? snapShot.data! : '',
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontFamily: "Roboto",
              fontStyle: FontStyle.normal,
              fontSize: APPBARBOTTOM_FONTSIZE),
          textAlign: TextAlign.left),
    );
  }
}

class TimelineLoadingData extends StatelessWidget {
  final Stream<bool> timelineLoadingDataStream;

  TimelineLoadingData(this.timelineLoadingDataStream, {super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: timelineLoadingDataStream,
      builder: (context, snapShot) => Text(
          (snapShot.hasData && snapShot.data!) ? 'Loading data'.i18n : '',
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontFamily: "Roboto",
              fontStyle: FontStyle.normal,
              fontSize: APPBARBOTTOM_FONTSIZE),
          textAlign: TextAlign.left),
    );
  }
}

class TimelineSchemaAndStyleSheet extends StatelessWidget {
  final Stream<String> timelineSchemaAndStyleSheetStream;

  TimelineSchemaAndStyleSheet(this.timelineSchemaAndStyleSheetStream, {super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
        stream: timelineSchemaAndStyleSheetStream,
        builder: (context, snapShot) {
          if (snapShot.hasData && snapShot.data != null) {
            return Text(
                snapShot.data!,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto",
                    fontStyle: FontStyle.normal,
                    fontSize: APPBARBOTTOM_FONTSIZE),
                textAlign: TextAlign.right);
          } else {
            return Container();
          }
        }
    );
  }
}

class TimelineAppBarProgress extends StatelessWidget {
  final Stream<bool> loadingStream;

  TimelineAppBarProgress(this.loadingStream, {super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: loadingStream,
        builder: (context, snapShot) {
          if (snapShot.hasData && snapShot.data!) {
            return const Center(
                child: const CircularProgressIndicator(
              valueColor: const AlwaysStoppedAnimation<Color>(Colors.black),
            ));
          } else {
            return Container();
          }
        });
  }
}

class TimelineReverseLoadingProgress extends StatelessWidget {
  final Stream<bool> loadingStream;

  TimelineReverseLoadingProgress(this.loadingStream, {superkey});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: loadingStream,
        builder: (context, snapShot) {
          if (snapShot.hasData && snapShot.data!) {
            return const Center(
                child: const CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                ));
          } else {
            return Container();
          }
        });
  }
}

class TimelineAddEntityButton extends StatelessPlrWidget {
  final Stream<bool> hasMenuStream;
  final Timeline timeline;
  final Function createRecordEntity;
  final Function onItemUpdated;
  final Function getShowFAB;

  TimelineAddEntityButton(super.account, this.hasMenuStream, this.timeline,
      this.createRecordEntity, this.onItemUpdated, this.getShowFAB, {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return StreamBuilder(
        stream: hasMenuStream,
        builder: (context, snapShot) {
          var hasMenu = snapShot.data == true;

          if (hasMenu && getShowFAB() == true) {
            /// read onlyでなくかつMenuが存在すれば表示
            var textScale = MediaQuery.textScalerOf(context).scale(1.0);
            if (textScale < 1.0) {
              textScale = 1.0;
            }
            return Container(
              height: 40,
              width: 40,
              margin: EdgeInsets.only(
                  bottom: ((timeline is Channel) && hasMenu)
                      ? 35.0 * textScale
                      : 15.0),
              child: FloatingActionButton(
                heroTag: "hero_add",
                backgroundColor: Colors.teal[50],
                tooltip: 'Add class',
                child: const Icon(Icons.add, size: 20, color: Colors.black),
                onPressed: () {
                  //ボタンタップ時イベント
                  print("[AW DBG] item add button tapped.");

                  /// ソフトキーボードを閉じる
                  FocusScope.of(context).unfocus();
                  showDialog(
                    context: context,
                    builder: (context) =>
                      EntityDialog(
                        account, EntityDialogSettings(), timeline, createRecordEntity(),
                        false, true,
                      )
                  ).then((entity) => onItemUpdated(entity));
                },
              ),
            );
          } else {
            return Container();
          }
        });
  }
}
