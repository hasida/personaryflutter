import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart'
  hide ChangeNotifierProvider, Consumer, Notifier, Provider;
import 'package:personaryFlutter/semeditor/model/sem_link_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/extension.dart';
import 'package:synchronized/synchronized.dart';

import '../../logic.dart' show onTimelineDisplayed;
import '../../util.dart' show showError, TimelineListing;
import '../../widget.dart' show StatefulPlrWidget, PlrState;

import 'timeline.i18n.dart';

const int SELECTED_SCHEMA_SOURCE = 0;
const int SELECTED_STYLE_SHEET = 1;

typedef ItemFunction = void Function(RecordEntity recordEntity);

/// タイムライン系画面の基底クラス
class TimelinePageBase extends StatefulPlrWidget {
  final Timeline timeline;
  final DateTime? dateTime;
  final bool direction;
  final ItemFunction? onItemUpdated, preItemDelete, onItemDeleted;
  final List<SchemataSource> schemataSources = [];
  TimelinePageBase(
    super.account,
    this.timeline, {
      super.key,
      this.dateTime,
      this.direction = false,
      this.onItemUpdated,
      this.preItemDelete,
      this.onItemDeleted,
      Iterable<SchemataSource>? schemataSources,
  }) {
    if (schemataSources != null) this.schemataSources.addAll(schemataSources);
  }

  @override
  ConsumerState<TimelinePageBase> createState() => TimelineStateBase();
}

/// タイムライン系画面の状態クラスの基底クラス
class TimelineStateBase<T extends TimelinePageBase> extends PlrState<T> with RouteAware {
  final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

  /// 周期タイマ(5,000msec)
  final int _syncInterval = 5000;
  /// ロードするタイムラインの件数
  final int _maxListCount = 15;
  /// タイムラインアイテム表示上限
  int get _itemDisplayLimit {
    if (Platform.isAndroid) {
      return 300;
    } else if (Platform.isIOS) {
      return 300;
    } else if (Platform.isWindows) {
      return 300;
    } else if (Platform.isMacOS) {
      return 300;
    } else if (Platform.isLinux) {
      return 300;
    }
    return 300;
  }

  /// タイマオブジェクト
  Timer? _timer;
  /// タイムラインオブジェクト
  Timeline get timeline => widget.timeline;

  /// タイムラインの状態(「無効」「閲覧のみ」「編集可能」)
  TimelineState get timelineState =>
    timelineSchemata?.timelineState ?? TimelineState.Disabled;

  /// タイムラインが閲覧のみかの判定フラグ
  bool get readOnly {
    if (timelineState != TimelineState.Writable) {
      /// read only
      return true;
    }
    return false;
  }

  /// Itemリスト
  static HashSet<Item> createItemSet()
    => HashSet<Item>(equals: (a,b) => a.id == b.id, hashCode: (o) => o.id.hashCode);
  final HashSet<Item> _itemSet = createItemSet();
  List<Item> get _itemList => _sortedItems(_itemSet);
  bool _isValidItem(Item item) => (item.begin != null) && !item.isDeleted;
  /// Itemリストソート
  List<Item> _sortedItems(Iterable<Item> iterable){
    var tmpList = iterable.where(_isValidItem).toList();
    tmpList.sort((a,b) => (a.begin != null && b.begin != null) ? a.begin!.compareTo(b.begin!) : 0);
    return tmpList;
  }
  int get itemCount => isSchemataNull ? 0 : _itemSet.where(_isValidItem).length;

  /// 日付指定でアイテム取得した場合の起点日時
  DateTime? _beginTime;

  final StreamController<List<RecordEntity>> _itemStreamController = BehaviorSubject();
  Stream<List<RecordEntity>> get itemStream => _itemStreamController.stream;

  List<RecordEntity>? _items;
  List<RecordEntity> get items => (_items ??= const <RecordEntity>[]);
  set items(List<RecordEntity> value) {
    _items = value;
    _itemStreamController.sink.add(value);
  }

  /// タイムラインアイテムのリスト（items）を更新します。
  Future<void> updateItems([bool force = false])
    => 'updateItems'.synchronized(() async {
      if (_itemList.isEmpty || (timelineState == TimelineState.Disabled)) {
        _itemStreamController.sink.add([]);
      }
      else {
        bool updated = false;
        final result = <RecordEntity>[];
        await _itemSet.synchronized(() {
          for (final entity in _itemList) {
            if (!force) {
              final old = items.firstWhereOrNull(
                (e) => entity.id == e.entity?.id,
              );
              if (old != null && old.entity != null &&
                  old.lastVersionName == entity.lastVersionName)
              {
                result.add(old);
                updated |= true;
                continue;
              }
            }

            final recordEntity = getRecordEntity(entity);
            if (recordEntity != null && checkOutputtable(recordEntity.type)) {
              result.add(recordEntity);
            }
            updated |= true;
          }
        });
        if (updated) {
          result.sort((a, b) =>
            (a.begin != null && b.begin != null) ?
             a.begin!.compareTo(b.begin!) : 0
            );
          items = result;
        }
      }
    });

  /// スキーマタ
  Schemata? _timelineSchemata;
  Schemata? get timelineSchemata => _timelineSchemata;
  set timelineSchemata(Schemata? value) {
    _timelineSchemata = value;
    hasMenuStream.sink.add(hasMenu);
    updateItems();
  }

  bool get isSchemataNull => timelineSchemata == null;

  bool get hasMenu => !readOnly && _hasMenu(timelineSchemata?.classOf("Event"));
  bool _hasMenu(SchemaClass? schemaClass){
    if (schemaClass == null) return false;
    final recordEntity = createRecordEntity(schemaClass.id);
    if(recordEntity == null ) return false;
    if(EntityDialog.getMenu(schemaClass, recordEntity, true).length > 0) {
      return true;
    } else {
      return false;
    }
  }
  BehaviorSubject<bool> hasMenuStream = BehaviorSubject();

  Iterable<SchemataSource>? schemataSources;
  Schemata? selectedSchemata;
  SchemataSource? _selectedSchema;
  SchemataSource? get selectedSchema => _selectedSchema;
  set selectedSchema(SchemataSource? value) {
    if (_selectedSchema != value) {
      _selectedSchema = value;
      selectedSchemataAndStyleSheet.sink.add(getSchemataNameAndStyleSheetName());
      saveSelectedSchema();
      loadSchemata().then((_) => loadTimelineItems(force: true));
    }
  }

  /// サブリソースから文字列リスト取得
  List<String>? _getSubsStringList(Iterable<SchemaClass>? iterable){
    if (iterable == null) return null;

    List<String> list = iterable.map((e) => e.id).toList();
    for (var sub in iterable) {
      list.addAll(_getSubsStringList(sub.subs)!);
    }
    return list;
  }

  /// フィルター
  List<String>? get timelineFilter {
    return _getSubsStringList(timelineSchemata?.classOf('Event')?.subs.where((e) => e.id != SemLinkModel.graphRelationClass));
  }

  /// スタイルシート
  Future<
    List<TimelineStyleSheet>?
  > get timelineStylesheets => _schemataGettingLock.synchronized(() async {
      if (timelineSchemata == null){
        return null;
      }
      var data = HashSet<TimelineStyleSheet>(
        equals: (a, b) => a.id == b.id,
        hashCode: (e) => e.id.hashCode,
      );
      data.addAll(timelineSchemata!.timelineStyleSheets);

      return data.toList();
  });

  RecordEntity? createRecordEntity([String? typeId]){
    if (timelineSchemata == null) return null;
    return RecordEntity.create(timelineSchemata!, typeId ?? eventClass);
  }

  Future<RecordEntity?> createMessage(String message) async {
    if (timelineSchemata == null) return null;
    return await RecordEntity.createMessage(timelineSchemata!, message);
  }

  Future<RecordEntity?> createRecordEntityFromMap(Map<String, dynamic> map) async {
    if (timelineSchemata == null) return null;
    return await RecordEntity.fromMap(timelineSchemata!, map);
  }

  /// Record Entity
  RecordEntity? getRecordEntity(Item item){
    if (timelineSchemata == null) return null;
    return RecordEntity.from(timelineSchemata!, item);
  }

  /// Format Result
  FormatResult? getFormatResult(Item item) {
    /// Schenata取得中チェック
    return timelineSchemata?.format(item);
  }

  /// load中フラグ
  bool loading = false;
  BehaviorSubject<bool> loadingStream = BehaviorSubject();
  /// 未来方向へのスワイプ時のload中フラグ
  BehaviorSubject<bool> reverseLoadingStream = BehaviorSubject();
  /// Schemata取得中ロック
  final _schemataGettingLock = Lock();
  /// フレンドプロフィール取得中フラグ
  bool disclosedToUsersGetting = false;

  /// 選択中スタイルシート
  StyleSheet<SchemaObject>? __selectedStylesheet;
  StyleSheet<SchemaObject>? get selectedStylesheet => __selectedStylesheet;
  set selectedStylesheet(StyleSheet<SchemaObject>? value) {
    __selectedStylesheet = value;
    selectedSchemataAndStyleSheet.sink.add(getSchemataNameAndStyleSheetName());
    saveSelectedSchema();
    updateItems(true);
  }
  BehaviorSubject<String> selectedSchemataAndStyleSheet = BehaviorSubject();

  /// ユーザPlrID
  String get userPlrId => root.plrId.toString();

  /// 開示ユーザMap
  // TODO: SemEditからの呼び出しを削除
  @deprecated
  final StreamController<Map<String,UserProfile>>
    _userProfileStreamController = BehaviorSubject();

  // TODO: SemEditからの呼び出しを削除
  @deprecated
  Stream<Map<String,UserProfile>> get userProfileStream
    => _userProfileStreamController.stream.asBroadcastStream();

  @override
  void initState() {
    super.initState();

    if (timeline is Channel) {
      final channel = timeline as Channel;
      print('[ChannelDetail] channel=${channel.name?.defaultValue}(${channel.id}) node=${channel.node.id}');
    }

    loadingStream.stream.listen((event) => loading = event);

    /// Itemリストクリア
    _itemSet.clear();

    /// 周期タイマスタート
    _timer = Timer.periodic(Duration(milliseconds: _syncInterval), onTimer);

    /// メタデータ更新タイマスタート
    timeline.startListing();
  }

  /// 破棄
  @override
  void dispose() {
    /// タイマオブジェクト破棄
    _timer?.cancel();

    /// メタデータ更新タイマ停止
    timeline.stopListing();

    saveSelectedSchema();

    /// Close stream
    // ignore: deprecated_member_use_from_same_package
    _userProfileStreamController.close();
    _itemStreamController.close();
    hasMenuStream.close();
    loadingStream.close();
    reverseLoadingStream.close();
    selectedSchemataAndStyleSheet.close();

    /// Itemリストクリア
    _itemSet.clear();
    super.dispose();
  }

  /// Change dependencies
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    print("[TimelineStateBase] didChangeDependencies()");
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute<dynamic>);
  }

  void onError(e, s) => showError(context, e, s);

  @override
  Future<void> didPush() async {
    print("[TimelineStateBase] didPush()");

    /// SchemataとStyleSheet取得
    await getSchemata();

    /// タイムラインアイテムロード
    loadTimelineItems(
      dateTime: widget.dateTime, direction: widget.direction,
    );

    if (timeline is Channel) timeline.sync().handleError(onError).listen((_) async {
        /// SchemataとStyleSheet取得
        await getSchemata();

        /// タイムラインアイテムロード
        loadTimelineItems(
          dateTime: widget.dateTime, direction: widget.direction,
        );
      },
    );
  }

  @override
  void didPop() {
    print("[TimelineStateBase] didPop()");
  }

  @override
  void didPushNext() {
    print("[TimelineStateBase] didPushNext()");
  }

  @override
  void didPopNext() {
    print("[TimelineStateBase] didPopNext()");
  }

  /// 周期タイマ処理
  void onTimer(Timer timer) {
    /// タイムラインアイテムロード(周期処理)
    loadTimelineItemsSync();
  }

  /// Schemata取得
  Future<void> getSchemata() => _schemataGettingLock.synchronized(() async {
      try {
        final Stream<List<SchemataSource>> ss;
        if (widget.schemataSources.isNotEmpty) {
          ss = Stream<List<SchemataSource>>.fromIterable([
              [ ...widget.schemataSources ],
          ]);
          widget.schemataSources.clear();
        } else {
          ss = timeline.schemataSourcesWith(notificationRegistry);
        }

        await for (final sources in ss.handleError(onError)) {
          // 同期後に更新があった場合は複数回ここを通る場合がある
          schemataSources = sources;

          if (schemataSources!.length == 1) {
            // スキーマが1つならそのまま使う
            selectedSchema = schemataSources!.first;
          }
          else {
            SchemataSource? selected;
            // 選択内容が保存されていて、一致するスキーマが存在すればそれを使用する
            final config = await readSelectedSchema();
            if (config != null && config.length > SELECTED_SCHEMA_SOURCE) {
              selected = schemataSources!.firstWhereOrNull(
                (e) => e.id == config[SELECTED_SCHEMA_SOURCE],
              );
            }

            if (selected != null) {
              selectedSchema = selected;
            }
          }
        }
        if (schemataSources?.isNotEmpty != true) {
          // スキーマが見つからなければページを閉じる
          Navigator.of(context).pop();
          return;
        }

        if (selectedSchema == null) {
          // 保存済みの選択内容がないか一致しなければ選択ダイアログを表示
          await showSchemaAndStyleSheetSelectDialog();
        }
      } catch (e, s) {
        showError(context, e);
        print(s);
      } finally {
        await loadSchemata();
      }
  });

  Future<void> loadSchemata() async {
    if (selectedSchema == null) return;

    await for (final s in selectedSchema!.create().handleError(onError)) {
      timelineSchemata = s;
      if (s == null) {
        selectedStylesheet = null;
        continue;
      }

      if (s.timelineStyleSheets.length > 1) {
        final config = await readSelectedSchema();
        StyleSheet? selected;
        if (selectedStylesheet != null &&
            s.timelineStyleSheets.any((e) => e.id == selectedStylesheet!.id)) {
          selected = selectedStylesheet;
        }
        else if (config != null && config.length > SELECTED_STYLE_SHEET) {
          selected = s.timelineStyleSheets.firstWhereOrNull(
            (e) => e.id == config[SELECTED_STYLE_SHEET],
          );
        }
        if (selected != null) {
          selectedStylesheet = selected;
        }
        else {
          await showSchemaAndStyleSheetSelectDialog();
          return;
        }
      }
      else if (s.timelineStyleSheets.isNotEmpty) {
        selectedStylesheet = s.timelineStyleSheets.first;
      }
      else {
        selectedStylesheet = null;
      }
    }

    // 選択中のSchemataSourceからSchemataを作成
    if (selectedSchema != null) {
      selectedSchema!.create().doOnDone(() {
        print("[TimelineStateBase] selectedSchemata: $selectedSchemata");
      }).doOnData((event) {
        selectedSchemata = event;
      }).listen(null);
    }
  }

  /// スキーマ＆スタイルシート選択ダイアログ
  Future<void> showSchemaAndStyleSheetSelectDialog() async {
    print('[TimelinePage] _showSchemaAndStyleSheetSelectDialog()');

    bool showSchema = schemataSources?.isNotEmpty ?? false;
    bool showStylesheet = true;
    var styleSheets = <TimelineStyleSheet>[];
    if (timelineSchemata != null) {
      var tmp = HashSet<TimelineStyleSheet>(
        equals: (a,b) => a.id == b.id,
        hashCode: (e) => e.id.hashCode,
      );
      tmp.addAll(timelineSchemata!.timelineStyleSheets);
      styleSheets.addAll(tmp);
      if (styleSheets.length <= 1 && schemataSources!.length <= 1) {
        return;
      } else if (styleSheets.isEmpty) {
        showStylesheet = false;
      }
      else if (selectedStylesheet == null) {
        showSchema = false;
      }
    }
    else {
      showStylesheet = false;
    }

    /// 選択ダイアログ表示
    var selectedItem = await showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text('Select schema'.i18n),
        children: <Widget>[
          if (showSchema)
            Container(
              padding: const EdgeInsets.all(5),
              child: Text('Schema'.i18n,
                style: const TextStyle(color: Colors.blue),
              )
            ),
          if (showSchema)
            for (var schema in schemataSources!)
              if (schema.name?.defaultValue != null)
                _buildSchemaSelectOption(context, schema.name?.defaultValue,
                  showCheckbox: selectedSchema != null,
                  selected: selectedSchema?.name?.value != null &&
                      selectedSchema!.name!.value == schema.name?.value,
                  onPressed: () => Navigator.pop(context, schema),
                ),
          if (showStylesheet)
            Container(
                padding: const EdgeInsets.all(5),
                child: Text('Style'.i18n,
                    style: const TextStyle(color: Colors.blue))
            ),
          if (showStylesheet)
            for (var styleSheet in styleSheets)
              _buildSchemaSelectOption(context, styleSheet.label?.defaultValue,
                showCheckbox: true,
                selected: selectedStylesheet?.label?.value != null &&
                    selectedStylesheet!.label!.value == styleSheet.label?.value,
                onPressed: () => Navigator.pop(context, styleSheet),
              ),
        ],
      ),
    ).catchError((e, s) {print('$e'); showError(context, e, s);});

    if (selectedItem is SchemataSource) {
      selectedSchema = selectedItem;
    }
    else if (selectedItem is TimelineStyleSheet) {
      selectedStylesheet = selectedItem;
    }
  }

  Widget _buildSchemaSelectOption(
    BuildContext context, String? name, {
      bool showCheckbox = false, bool selected = false, Function()? onPressed})
    => SimpleDialogOption(
         onPressed: onPressed,
         child: Row(
           children: <Widget>[
             if (showCheckbox)
               if(selected)
                 const Icon(Icons.check, color: Colors.black)
               else
                 Icon(Icons.check, color: Colors.black.withOpacity(0.0)),
             if (showCheckbox)
               const SizedBox(width: 5.0),
             Expanded(
               child: Text(name ?? ""),
             ),
           ]
         ),
       );

  // TODO: SemEditからの呼び出しを削除
  @deprecated
  void getChannelInfo(){}

  /// タイムラインアイテムロード時コールバック
  void onUpdate(Item item) {
    /// 基底クラスでは特に何もしない
    /// 派生クラスでoverrideして使用することを想定
  }

  /// タイムラインアイテムロード完了時コールバック
  void onFinish() {
    /// View更新
    if (this.mounted){
      loadingStream.sink.add(false);
      reverseLoadingStream.sink.add(false);
      updateItems();
    }
  }

  /// 既存Item同期
  Future<void> syncTimelineItem() async {
    var streams = <Stream>[];

    /// 同期開始
    for (var item in _itemSet) {
      streams.add(item.sync());
    }
    /// 同期完了待ち
    for (var s in streams) {
      /// 同期させることが目的なので処理はしない
      /// ignore: unused_local_variable
      await for (var r in s.handleError(onError)){}
    }
  }

  bool first = true;

  int addItems(Iterable<Item> items, [bool reverse = false]) {
    final lastLength = _itemSet.length;

    _itemSet.addAll(items);
    if (_itemSet.length > _itemDisplayLimit) {
      /// タイムラインアイテム表示上限超の場合
      final filtered = reverse ?
        _itemList.take(_itemDisplayLimit) :
        _itemList.reversed.take(_itemDisplayLimit);
      _itemSet.clear();
      _itemSet.addAll(filtered);
    }

    return _itemSet.length - lastLength;
  }

  /// タイムラインアイテムロード
  /// forceがfalseなら15件のみ取得
  void loadTimelineItems({DateTime? dateTime, bool force = false, bool direction = false}) async{
    loadingStream.sink.add(true);

    /// 既存Item同期
    if (_itemList.length > 0) {
      syncTimelineItem();
    }

    /// タイムラインアイテムロードの基準時間取得
    if (dateTime == null) {
      /// 日付指定が無ければ現在or最古/最新のItemから遡って15件取得
      if (_itemList.length > 0) {
        if (direction) {
          /// 未来方向への取得 → 最新Itemの日付
          dateTime = _itemList.last.begin ?? DateTime.now();
        }
        else {
          /// 過去方向への取得 → 最古Itemの日付
          dateTime = _itemList.first.begin ?? DateTime.now();
        }
      }
      if (dateTime == null) {
        /// 現在日付
        dateTime = DateTime.now();
      }
    }

    /// forceフラグに応じてロード件数設定
    var itemLoadCount = -1;
    if (force == false) {
      /// 最大15件の範囲内でロード
      itemLoadCount = _maxListCount - _itemList.length;
      if (_itemList.length >= _maxListCount) {
        /// 15件以上ある場合はロードせず終了
        onFinish();
        return;
      }
    } else {
      /// 既存アイテムに追加してロード
      itemLoadCount = _maxListCount;
      itemLoadCount += _itemSet.where(
        (e) => e.begin != null && e.begin!.compareTo(dateTime!) == 0,
      ).length;
    }

    var hadErrors = false;
    void onError(e, s) {
      hadErrors = true;
      showError(context, e, s);
    }

    final lastItems = [..._itemList];
    var nodes = timeline.timelineItemModelsByCount(
      dateTime,
      itemLoadCount,
      direction: direction,
      typeFilter: timelineFilter,
      skipDeleted: true,
    ).handleError(onError);
    await for (final event in nodes) {
      final items = createItemSet()
        ..addAll(lastItems)
        ..addAll(direction ?
          _sortedItems(event).take(itemLoadCount) :
          _sortedItems(event).reversed.take(itemLoadCount),
        );
      _itemSet.clear();
      addItems(items, !direction);
      updateItems();
    }

    /// 全アイテムロード完了
    if (direction == true) {
      final int loadedCount = _itemSet.length - lastItems.length;
      // 正方向取得（=日付指定または上スワイプ）
      if (loadedCount < _maxListCount) {
        // 取得数が指定未満なら最新アイテムまで取得できたとみなしbeginTimeをクリア
        _beginTime = null;
      } else {
        _beginTime = dateTime;
      }
    }
    else if (force == false && _itemList.length < _maxListCount) {
      // forceなし・過去方向取得（=タイムラインを開いたとき）取得数が指定未満なら
      // 未来方向のアイテムも取得を試みる
      itemLoadCount = _maxListCount - _itemList.length;
      dateTime = (_itemList.length > 0)
          ? (_itemList.last.begin ?? DateTime.now())
          : DateTime.now();

      /// アイテムリストが最大件数以下の場合は未来のアイテムをload
      final lastItems = [..._itemList];
      var nodes = timeline.timelineItemModelsByCount(
        dateTime,
        itemLoadCount,
        direction: true,
        typeFilter: timelineFilter,
        skipDeleted: true
      ).handleError(onError);
      await for (final event in nodes) {
        final items = createItemSet()
          ..addAll(lastItems)
          ..addAll(direction ?
            _sortedItems(event).reversed.take(itemLoadCount) :
            _sortedItems(event).take(itemLoadCount),
          );
        addItems(items, !direction);
        updateItems();
      }
    }
    /// 全アイテムロード完了
    onFinish();

    if (first && timeline is Channel) {
      onTimelineDisplayed(notificationRegistry, timeline as Channel, !hadErrors);
      first = false;
    }
  }

  /// タイムラインアイテムロード(周期処理)
  void loadTimelineItemsSync() async {
    /// loading中ならスキップ
    if (loading) return;

    loadingStream.sink.add(true);

    if (_itemList.isEmpty) return loadTimelineItems();

    /// タイムラインアイテムロードの基準時間
    // 開始は最古Itemの日付
    DateTime beginTime = _itemList.first.begin ?? DateTime.now();

    DateTime? endTime;
    if (_beginTime != null && _itemList.length > 1) {
      // カレンダーで日付指定していた場合は取得している中で最新のアイテムの日時まで取得
      endTime = _itemList.last.begin;
      if (beginTime == endTime) {
        endTime!.add(const Duration(seconds: 1));
      }
    }
    if (endTime == null) {
      // 日付指定されていない場合（指定後に最新のアイテムまで取得できた場合も含む）は
      // 現在時刻まで取得
      endTime = DateTime.now();
    }

    final lastItems = [..._itemList];
    var nodes = timeline.timelineItemModels(
      beginTime,
      endTime,
      typeFilter: timelineFilter,
      skipDeleted: false
    ).handleError((e, s) => showError(context, e, s));
    await for (final event in nodes) {
      addItems(event);
      updateItems();
    }
    /// 全アイテムロード完了
    if (_itemSet.isNotEmpty){
      syncTimelineItem().catchError((e, s) => showError(context, e, s))
          .then((value) => onFinish());
      for (final item in _itemList) {
        if (!lastItems.any((e) => e.id == item.id)) {
          onItemSyncUpdated(getRecordEntity(item));
          if (timeline is Channel) onTimelineDisplayed(
            notificationRegistry, timeline as Channel, true,
          );
        }
      }
    }
    else {
      onFinish();
    }
  }

  Item asItemOf(Entity entity)
    => (entity is Item) ? entity : Item(entity, timeline);

  /// アイテム追加・編集時コールバック
  void onItemUpdated(RecordEntity? recordEntity) {
    try {
      if (!this.mounted) return;
      if (recordEntity == null) {
        /// nullだったら終了(Cancelを想定)
        return;
      }

      // RecordEntityを(既存のものあれば削除してから)追加
      // (updateItemsで並び替えが行われるので末尾追加でOK)
      _itemSet.synchronized(() {
        final item = asItemOf(recordEntity.entity!);
        _itemSet..remove(item)..add(item);
      }).then((_) {
        updateItems();
      });
    } finally {
      if (recordEntity != null) widget.onItemUpdated?.call(recordEntity);
    }
  }

  /// アイテム削除前コールバック
  void preItemDelete(RecordEntity? recordEntity) {
    if (recordEntity != null) widget.preItemDelete?.call(recordEntity);
  }

  /// アイテム削除時コールバック
  void onItemDeleted(RecordEntity? recordEntity) {
    try {
      if (recordEntity?.entity == null) {
        /// RecordEntityまたはEntityがnullだったら終了(ないはず)
        return;
      }

      _itemSet.synchronized(() {
          _itemSet.remove(asItemOf(recordEntity!.entity!));
      }).then((_) {
          if (mounted) updateItems();
      });
    } finally {
      if (recordEntity != null) widget.onItemDeleted?.call(recordEntity);
    }
  }

  void onItemSyncUpdated(RecordEntity? recordEntity) {
    // 派生クラスで実装する用
  }

  /// カレンダーによる日付選択処理
  Future<bool> selectDate(BuildContext context) async {
    if (!this.mounted) return false;
    try {
      loading = true;
      final DateTime? selected = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(2015),
          /// ignore: return_of_invalid_type_from_catch_error
          lastDate: DateTime(2037)).catchError((e, s) => showError(context, e, s));
      if (selected == null) return false;
      /// Itemリストクリア
      _itemSet.clear();
      setState(() {
        /// 選択された日付でタイムラインアイテムロード
        loadTimelineItems(dateTime: selected, direction: true);
      });
      return true;
    } finally {
      loading = false;
    }
  }

  /// 選択スキーマ保存
  Future<void> saveSelectedSchema() async {
    List<String>? pastStringList = await readSelectedSchema();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('select_schema_${timeline.id}',
        [selectedSchema?.id ?? pastStringList?.first ?? '', selectedStylesheet?.id ?? pastStringList?.last ?? '']);
  }

  /// 選択スキーマ読込み
  Future<List<String>?> readSelectedSchema() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList('select_schema_${timeline.id}');
  }

  /// preferencesデータ削除
  Future<void> deleteSelectedSchema() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('select_schema_${timeline.id}');
  }

  /// 選択中Schemata & StyleSheet文字列取得
  String getSchemataNameAndStyleSheetName() {
    return '${selectedSchema?.name?.defaultValue ?? ''} ${selectedStylesheet?.label?.defaultValue ?? ''}';
  }

  @override
  Widget build(BuildContext context) => const Scaffold();
}

// TODO: SemEditからの呼び出しを削除
@deprecated
class UserProfile {
  String userName;
  List<int> userThumbnail;

  UserProfile(this.userName, this.userThumbnail);
}
