import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static Translations t = Translations.byText('en_us') +
      {
        'en_us': 'Create meeting',
        'ja_jp': 'ミーティングを作成',
      } +
      {
        'en_us': 'Are you sure you want to create meeting?',
        'ja_jp': 'ミーティングを作成しますか？',
      };

  String get i18n => localize(this, t);
}
