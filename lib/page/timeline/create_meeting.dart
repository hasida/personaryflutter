import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:googleapis/calendar/v3.dart' hide Channel, Colors;
import 'package:random_string/random_string.dart' show randomAlphaNumeric;

import 'create_meeting.i18n.dart';

class CreateMeetingButton extends StatelessWidget {
  final Channel channel;
  final Function? onDone;
  final Function? onError;

  CreateMeetingButton(this.channel, {this.onDone, this.onError,  super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Icon(Icons.local_phone, color: Colors.black),
      onTap: () {
        /// ソフトキーボードを閉じる
        FocusScope.of(context).unfocus();
        showConfirmDialog(
           context, 'Create meeting'.i18n,
          'Are you sure you want to create meeting?'.i18n,
           onOk: () {
             var users = <HasPlrId>[
               channel, ...channel.disclosedToUsers(true)
             ].where((e) => e.plrId != channel.masterStorage.plrId);
             var future = createMeeting(channel.masterStorage, users);
             Navigator.of(context).push(
                 ProgressModal<String?>(context, future, 'ミーティングを作成しています...')
             ).catchError(onError ?? (e,s) => print('$e')).then((uri) {
               if (uri == null){
                 ScaffoldMessenger.of(context).showSnackBar(
                     const SnackBar(content: const Text("ミーティングの作成に失敗しました"))
                 );
                 return;
               }
               if (onDone != null){
                 onDone!(uri);
               }
             });
          }
        );
      },
    );
  }
}

Future<String?> createMeeting(Storage storage, Iterable<HasPlrId> users) async {
  if (storage.type != storageTypeOf("googleDrive")) {
    // Show error, or let the user to select target Google Drive account.
    return null;
  }

  Event event = Event();
  event.summary = 'Meeting';

  var start = EventDateTime();
  start.dateTime = DateTime.now().toUtc();
  start.timeZone = 'UTC';
  event.start = start;

  var end = EventDateTime();
  end.dateTime = DateTime.now().add(const Duration(hours: 1));
  end.timeZone = 'UTC';
  event.end = end;

  event.attendees = users.map((user){
    var a = EventAttendee();
    a.email = user.plrId!.email;
    return a;
  }).toList();

  var conferenceData = ConferenceData();
  var createRequest = CreateConferenceRequest();
  createRequest.requestId = _generateRequestId();
  conferenceData.createRequest = createRequest;
  event.conferenceData = conferenceData;

  var calendar = CalendarApi(storage.connection);
  var ret = await calendar.events.insert(
      event, 'primary', conferenceDataVersion: 1
  /// ignore: return_of_invalid_type_from_catch_error
  ).catchError((e,s) => print('[createMeeting] $e'));
  return ret.hangoutLink;
}

String _generateRequestId() => randomAlphaNumeric(16);
