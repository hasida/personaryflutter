import 'dart:async';
import 'dart:io' as io;
import 'dart:isolate';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:plr_ui/plr_ui.dart';

import 'timeline.i18n.dart';

class _LoadParams {
  Stream<List<int>> stream;
  StreamSink sink;

  _LoadParams(this.stream, this.sink);
}

class ImageModal extends ModalRoute<void> {
  @override
  Color get barrierColor => Colors.black.withOpacity(0.8);

  @override
  bool get barrierDismissible => false;

  @override
  String? get barrierLabel => null;

  final StreamController _imageStream = StreamController();
  final StreamController _scaleStream = StreamController();

  bool cancelFlag = false;
  Isolate? loader;

  num get _scale => pow(2, _scaleFactor);
  num _scaleFactor = 0;
  final _scaleMax = 5;

  late num _lastX;
  late num _lastY;
  num? _lastScale;

  final ScrollController _scrollControllerX = ScrollController();
  final ScrollController _scrollControllerY = ScrollController();
  Image? _imageView;

  ImageModal.fromFile(File file) {
    _imageStream.sink.add(file);
  }

  ImageModal.fromMemory(Uint8List data) {
    _imageStream.sink.add(data);
  }

  ImageModal.fromStream(Stream stream) {
    stream.listen((event) => _imageStream.sink.add(event));
  }

  ImageModal.fromMmdata(FileMmdata mmdata) {
    mmdata.content.handleError(onError).listen((ret) async {
      /// 表示が固まるのを避けるため少し待つ
      await Future.delayed(const Duration(milliseconds: 100));

      while (loader != null) {}
      if (cancelFlag) {
        return;
      }
      var content = ret.value;
      var stream = content.stream.handleError(onError);
      _loadFromStream( _LoadParams(stream, _imageStream.sink));
    });
  }

  @override
  void dispose() {
    _imageStream.close();
    _scaleStream.close();
    super.dispose();
  }

  void onError(var e, var s) {
    print('$e');
    /// imageStreamに空のListを渡すとモーダルが閉じる
    _imageStream.sink.add([]);
  }

  void _loadFromStream(_LoadParams params) async {
    var stream = params.stream;
    /// ignore: close_sinks
    var sink = params.sink;

    List<int> data = [];
    var startTime = DateTime.now(); // @@DEBUG@@
    var count = 0;
    await for (var d in stream) {
      if (cancelFlag) {
        /// 読み込み中止
        print('[ImageModal] canceled.');
        return;
      }
      ++count;

      data.addAll(d);
    }
    sink.add(data);
    var endTime = DateTime.now(); // @@DEBUG@@
    var estTime = endTime.difference(startTime); // @@DEBUG@@
    print('[ImageModal] load completed: ${data.length} bytes. ($count reads, $estTime)');
  }

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return Material(
      type: MaterialType.transparency,
      child: PopScope(
        canPop: false,
        onPopInvokedWithResult: (didPop, _) {
          if (!didPop) {
            cancelFlag = true;
            Navigator.pop(context);
          }
        },
        child: Stack(
          fit: StackFit.expand,
          children: [
            _buildImageView(context),
            Container(
              alignment: Alignment.topRight,
              padding: const EdgeInsets.only(right: 20, top: 20),
              child: _buildCloseButton(context),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildImageView(BuildContext context) {
    return StreamBuilder(
      stream: _imageStream.stream,
      builder: (context, snapshot) {
        var data = snapshot.data;
        if (data is List<int>) {
          if (data.isEmpty) {
            Navigator.of(context).pop();
          }
          _imageView = Image.memory(Uint8List.fromList(data), fit: BoxFit.contain);
        }
        else if (data is io.File) {
          _imageView = Image.file(data, fit: BoxFit.contain);
        }
        else {
          return Center(
            child: Container(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Loading image...'.i18n),
                ],
              ),
            )
          );
        }

        var mq = MediaQuery.of(context);

        _scaleStream.sink.add(0);

        return Stack(
          children: [
            Container(
              width: mq.size.width,
              height: mq.size.height - 50,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                controller: _scrollControllerY,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  controller: _scrollControllerX,
                  child: StreamBuilder(
                    stream: _scaleStream.stream,
                    builder: (context, snapshot) {
                      return SizedBox(
                        width: mq.size.width * _scale,
                        height: (mq.size.height - 50) * _scale,
                        child: _imageView,
                      );
                    },
                  ),
                ),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              child: Container(
                width: mq.size.width,
                height: mq.size.height,
              ),
              onScaleStart: (ScaleStartDetails data) {
                _lastX = data.focalPoint.dx;
                _lastY = data.focalPoint.dy;
                _lastScale = 1;
              },
              onScaleUpdate: (ScaleUpdateDetails data) {
                final pos = data.focalPoint;
                final sx = _scrollControllerX.offset;
                final sy = _scrollControllerY.offset;
                if (data.scale != _lastScale) {
                  var diff = data.scale - _lastScale!;
                  final w = mq.size.width;
                  final h = mq.size.height;
                  final bw = w * _scale;
                  final bh = h * _scale;
                  _scaleFactor += diff;
                  if (_scaleFactor < 0) _scaleFactor = 0;
                  else if (_scaleFactor > _scaleMax) _scaleFactor = _scaleMax;
                  final aw = w * _scale;
                  final ah = h * _scale;
                  final x = ((aw - bw) / 2) + sx;
                  final y = ((ah - bh) / 2) + sy;
                  _scrollControllerX.jumpTo(x);
                  _scrollControllerY.jumpTo(y);

                  _lastScale = data.scale;
                  _scaleStream.sink.add(0);
                }
                else {
                  /// 斜めにもスムーズに動かすため、手動で調整する
                  final x = sx - (pos.dx - _lastX);
                  final y = sy - (pos.dy - _lastY);
                  _scrollControllerX.jumpTo(x);
                  _scrollControllerY.jumpTo(y);
                }
                _lastX = pos.dx;
                _lastY = pos.dy;
              },
            )
          ],
        );
      },
    );
  }

  Widget _buildCloseButton(BuildContext context) {
    return InkWell(
      child: Text(
        '×',
        style: TextStyle(
          height: 1.0,
          fontSize: 60,
          color: Colors.white,
          backgroundColor: Colors.grey.withOpacity(0.7),
        ),
      ),
      onTap: () => Navigator.of(context).pop(),
    );
  }

  @override
  bool get maintainState => true;

  @override
  bool get opaque => false;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 100);

}
