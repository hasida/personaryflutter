import 'dart:convert' show json, utf8;

import 'package:dart_bbs/dart_bbs.dart' show vpCreate;
import 'package:dart_bbs/src/models/vc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:plr_ui/plr_ui.dart';
import "package:crypto_keys/crypto_keys.dart" show RsaPrivateKey;
import "package:pointycastle/export.dart" show RSAPrivateKey;

import '../../logic.dart';
import '../../util.dart';
import '../../widget.dart' show divider;
import 'timeline_state_base.dart' show ItemFunction;
import 'trusted_web.i18n.dart';

Future<void> showCredentialsDialog(
  BuildContext context,
  Root root,
  Timeline timeline,
  Schemata? schemata,
  RecordEntity recordEntity, [
    ItemFunction? onItemUpdated,
]) async {
  var credentialFiles = getCredentialFiles(recordEntity);
  if (credentialFiles.length != 1) {
    showMessage(context, "Multiple credentials are not supported, yet.");
    return;
  }

  var credential = await _loadCredential(context, credentialFiles.first);
  if (credential == null) return;

  await showCredentialDialog(
    context, root, timeline, schemata, credential, onItemUpdated,
  );
}

const _titleWidth = 112.0;

Future<void> showCredentialDialog(
  BuildContext context,
  Root root,
  Timeline timeline,
  Schemata? schemata,
  String credential,
  ItemFunction? onItemUpdated,
) async {
  var type = credentialTypeOf(credential);

  var canCreateVp = (
    (type == CredentialType.verifiableCredential) &&
    (timeline is Channel) &&
    ((timeline.plrId != null) && (timeline.plrId == root.plrId))
  );

  VerifiableCredential vc; {
    switch (type) {
      case CredentialType.verifiableCredential: {
        vc = VerifiableCredential(credential);
        break;
      }
      case CredentialType.verifiablePresentation: {
        var vcs = json.decode(credential)[verifiableCredentialKey];
        if (vcs.length != 1) {
          showMessage(context, "Multiple VCs in one VP is not supported, yet.");
          return;
        }
        vc = VerifiableCredential(json.encode(vcs.first));
        break;
      }
      default: {
        showMessage(context, "Unknown credential type.");
        return;
      }
    }
  }

  Widget? createRow(String title, dynamic value) => (value != null) ? Row(
    children: [
      Container(
        width: _titleWidth,
        padding: const EdgeInsets.only(right: 10),
        child: Text(
          title.i18n, style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      Expanded(child: Text(value.toString())),
    ],
  ) : null;

  late StateSetter setState;
  var revealedIndices = <int>[];

  Widget createCard(
    Iterable<MapEntry<String, dynamic>> entries,
    int revealedIndex, [
      String? label,
  ]) {
    var card = Card(
      color: Colors.blueGrey[50],
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: Column(
          children: [
            if (label != null) Column(
              children: [
                Text(
                  label.i18n,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                const Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: divider,
                ),
              ],
            ),
            ...entries.map(
              (e) => createRow(e.key, e.value),
            ).where((r) => r != null).cast<Widget>().toList(),
          ],
        ),
      ),
    );
    if (!canCreateVp) return card;

    return CheckboxListTile(
      controlAffinity: ListTileControlAffinity.leading,
      contentPadding: EdgeInsets.zero,
      activeColor: Colors.blue,
      value: revealedIndices.contains(revealedIndex),
      onChanged: (v) => setState(() {
          if (v!) revealedIndices.add(revealedIndex);
          else revealedIndices.remove(revealedIndex);
      }),
      title: Transform.translate(
        offset: const Offset(-20, 0),
        child: card,
      ),
    );
  }

  Iterable<MapEntry<String, dynamic>> createEntries(
    Map<String, dynamic>? property,
    Iterable<String> keys,
  ) => keys.map((k) => MapEntry(k, property?[k]));

  Widget createProfiles(
    Map<String, dynamic> profile,
  ) => createCard(createEntries(profile, profileKeys), profile.revealedIndex);

  Widget createAchievementSubjects(
    Map<String, dynamic> achievementSubject,
  ) => createCard([
      ...createEntries(achievementSubject[achievementKey], achievementKeys),
      ...createEntries(achievementSubject[resultKey], resultKeys),
      ...createEntries(achievementSubject, achievementSubjectKeys),
    ], achievementSubject.revealedIndex, achievementKey);

  Widget createCredentialSubjects(VerifiableCredential vc) => Column(
    children: vc.credentialSubjects.map((s) {
        switch (s[typeKey]) {
          case profileType: return createProfiles(s);
          case achievementSubjectType: return createAchievementSubjects(s);
          default: return null;
        }
    }).where((w) => w != null).cast<Widget>().toList(),
  );

  var title = "Credential".i18n; {
    var name = vc.name;
    if (name != null) title += ": ${name}";
  }

  await showDialog(
    context: context,
    builder: (context) => ProgressHUD(
      child: StatefulBuilder(
        builder: (context, _setState) {
          setState = _setState;
          return AlertDialog(
            title: Text(title),
            content: Column(
              children: [
                createRow("Issuer", vc.issuer) ?? Container(),
                createRow("Issuance Date", vc.issuanceDate) ?? Container(),
                const SizedBox(height: 5),
                Flexible(
                  child: CupertinoScrollbar(
                    child: SingleChildScrollView(
                      primary: true,
                      child: createCredentialSubjects(vc),
                    ),
                  ),
                ),
              ],
            ),
            actions: [
              Row(
                children: [
                  if (canCreateVp) TextButton(
                    child: Text("Create VP".i18n),
                    onPressed: revealedIndices.isNotEmpty ? () => _createVp(
                      context, root, timeline, credential, vc, revealedIndices,
                    ).then((i) {
                        if ((i != null) && (schemata != null)) {
                          var e = RecordEntity.from(schemata, i);
                          if (e != null) onItemUpdated?.call(e);
                        }
                    }) : null,
                  ),
                  const Spacer(),
                  TextButton(
                    child: Text("Verify".i18n),
                    onPressed: () => _verifyCredential(context, credential),
                  ),
                  TextButton(
                    child:
                    Text(MaterialLocalizations.of(context).closeButtonLabel),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              ),
            ],
          );
        },
      ),
    ),
  );
}

Future<String?> _loadCredential(
  BuildContext context,
  FileMmdata credential,
) async {
  Content? content;
  await for (
    var r in credential.content.handleError((e, s) => showError(context, e, s))
  ) {
    if (content != null) content.drain();
    content = r.value;
  }
  if (content == null) {
    await showConfirmDialog(
      context, "Error".i18n,
      "Failed to load document to verify.".i18n.fill([
          credential.id ?? "Unknown".i18n,
      ]),
      showCancelButton: false,
    );
    return null;
  }
  return await utf8.decodeStream(content.stream);
}

Future<bool> _verifyCredential(
  BuildContext context,
  String credential, [
    bool showSuccessDialog = true,
]) async {
  bool? result; try {
    result = await runWithProgress(
      context, () => verifyCredential(credential),
    );
  } catch (e, s) {
    if (!processNeedNetworkException(context, e, s)) {
      await showConfirmDialog(
        context, "Verification Error".i18n, e.toString(),
        showCancelButton: false,
      );
    }
    return false;
  }
  if (result != true) {
    await showConfirmDialog(
      context, "Verification Error".i18n,
      "Document verification failed.".i18n,
      showCancelButton: false,
    );
    return false;
  }

  if (showSuccessDialog) await showConfirmDialog(
    context, "Verification Success".i18n,
    "Document has been successfully verified.".i18n,
    showCancelButton: false,
  );
  return true;
}

Future<Item?> _createVp(
  BuildContext context,
  Root root,
  Channel channel,
  String credential,
  VerifiableCredential vc,
  List<int> revealedIndices,
) async {
  var kid = channel.didPublicKey?.keyId;

  RSAPrivateKey? privateKey; {
    var k = channel.didPrivateKey?.cryptoKeyPair.privateKey as RsaPrivateKey?;
    if (k != null) {
      privateKey = RSAPrivateKey(
        k.modulus, k.privateExponent, k.firstPrimeFactor, k.secondPrimeFactor,
      );
    }
  }
  if ((kid == null) || (privateKey == null)) {
    await showConfirmDialog(
      context, "Error".i18n,
      "The channel has no RSA private key for the DID.".i18n,
      showCancelButton: false,
    );
    return null;
  }

  Item item; try {
    item = (
      await runWithProgress(
        context, () async {
          var vp = await vpCreate(
            credential, revealedIndices..sort(), privateKey, kid,
          );
          var item = await channel.newTimelineItemModel(vpClass)
            ..creatorPlrId = root.plrId;
          var file = (
            await item.newFileModel(cntProperty)
          )
          ..format = vpContentType
          ..title.value = "VP (${vc.name ?? "Unknown".i18n})";

          await file.putData(utf8.encode(vp));
          await channel.syncNewTimelineItems().drain();

          return item;
        },
      )
    )!;
  } catch (e, s) {
    if (!processNeedNetworkException(context, e, s)) {
      await showConfirmDialog(
        context, "Error".i18n, e.toString(),
        showCancelButton: false,
      );
    }
    return null;
  }

  await showConfirmDialog(
    context, "Create VP".i18n,
    "VP creation has beend completed.".i18n,
    showCancelButton: false,
  );
  return item;
}
