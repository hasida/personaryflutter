import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart'
  hide ChangeNotifierProvider, Consumer, Notifier, Provider;
import 'package:image/image.dart' as image show Image, decodeImage;
import 'package:intl/intl.dart' as intl;
import 'package:personaryFlutter/page/timeline/video_modal.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';
import '../../logic.dart' show hasCredentials;
import '../../util.dart';
import '../../widget.dart' show StatelessPlrWidget, StatefulPlrWidget, PlrState;
import '../friend/friend_page.dart';
import 'trusted_web.dart';
import 'image_modal.dart';
import 'timeline.i18n.dart';
import 'timeline_state_base.dart';

enum TimelineItemPopup{
  INSERT_TOP,
  INSERT_BELOW,
  DELETE,
  DOWNLOAD_IMAGE,
  DOWNLOAD_FILE,
  COPY_DEEP_LINK,
  COPY_NODE,
  COPY_ADDITIONAL_NODE,
  DISCLOSE_TO_FRIEND;

  String get label {
    switch (this) {
      case TimelineItemPopup.INSERT_TOP: return 'Add front'.i18n;
      case TimelineItemPopup.INSERT_BELOW: return 'Add rear'.i18n;
      case TimelineItemPopup.DELETE: return 'Delete'.i18n;
      case TimelineItemPopup.DOWNLOAD_IMAGE: return 'Download image(s)'.i18n;
      case TimelineItemPopup.DOWNLOAD_FILE: return 'Download'.i18n;
      case TimelineItemPopup.COPY_DEEP_LINK: return 'Copy deep link'.i18n;
      case TimelineItemPopup.COPY_NODE: return 'Copy node'.i18n;
      case TimelineItemPopup.COPY_ADDITIONAL_NODE: return 'Copy additional node'.i18n;
      case TimelineItemPopup.DISCLOSE_TO_FRIEND:
        return 'Disclose to friend'.i18n;
    }
  }
}

typedef RecordEntityCallback = void Function(RecordEntity? entity);

class TimelineItem extends StatefulPlrWidget {
  final RecordEntity recordEntity;
  final bool dateArea;
  final Timeline timeline;
  final String userPlrId;
  final bool readOnly;
  final Schemata? timelineSchemata;
  final RecordEntityCallback updatedCallback;
  final RecordEntityCallback preDeleteCallback;
  final RecordEntityCallback deletedCallback;
  final FormatResult formatResult;
  final bool fullDate;
  final bool contentOnly;

  TimelineItem(
    super.account,
    this.recordEntity,
    this.dateArea,
    this.timeline,
    this.userPlrId,
    this.readOnly,
    this.timelineSchemata,
    this.updatedCallback,
    this.preDeleteCallback,
    this.deletedCallback, {
      super.key,
      this.fullDate = false,
      this.contentOnly = false,
    }
  ) : formatResult = recordEntity.format();

  @override
  PlrState createState() => TimelineItemState();
}

/// タイムラインアイテムの日付部分
class TimelineItemState extends PlrState<TimelineItem> {
  RecordEntity get recordEntity => widget.recordEntity;
  bool get dateArea => widget.dateArea;
  Timeline get timeline => widget.timeline;
  String get userPlrId => widget.userPlrId;
  bool get readOnly => widget.readOnly;
  Schemata? get timelineSchemata => widget.timelineSchemata;
  RecordEntityCallback get updatedCallback => widget.updatedCallback;
  RecordEntityCallback get preDeleteCallback => widget.preDeleteCallback;
  RecordEntityCallback get deletedCallback => widget.deletedCallback;
  FormatResult get formatResult => widget.formatResult;
  bool get fullDate => widget.fullDate;
  bool get contentOnly => widget.contentOnly;

  /// importance表示／非表示フラグ
  final bool _importance1Visible = false;
  final bool _importance2Visible = false;

  /// タップ位置ストア
  late var tapPosition;

  void storePosition(TapDownDetails details) {
    tapPosition = details.globalPosition;
  }

  /// 「上に挿入」「下に挿入」用 RecrodEntity生成処理
  RecordEntity? createRecordEntityItemAdd(RecordEntity baseRecordEntity, TimelineItemPopup selected){
    if (timelineSchemata == null) return null;

    RecordEntity? addRecordEntity = RecordEntity.create(timelineSchemata!, "Event");
    if (addRecordEntity == null) return null;

    if (selected == TimelineItemPopup.INSERT_TOP) {
      addRecordEntity.begin = baseRecordEntity.begin!.subtract(const Duration(milliseconds: 1));
    } else if (selected == TimelineItemPopup.INSERT_BELOW) {
      addRecordEntity.begin = baseRecordEntity.begin!.add(const Duration(milliseconds: 1));
    }
    return addRecordEntity;
  }

  /// ポップアップメニュー処理
  Future<void> showPopupMenu(BuildContext context) async {
    var ancestorContext =
      context.findAncestorStateOfType<TimelineStateBase>()?.context;
    if (ancestorContext == null) return;
    context = ancestorContext;

    /// ignore: unused_local_variable
    bool isFile  = false;
    /// ignore: unused_local_variable
    bool isImage = false;
    bool isVideo = false;
    bool isOthers = false;

    for (var mmdata in formatResult.mmdatas){
      if (!mmdata.isFile) continue;

      final downloadPermission =
        mmdata.asFile.peer?.firstLiteralValueOf(downloadPermissionProperty);
      if (!isCreator && downloadPermission == false) {
        continue;
      }

      /// ファイル
      isFile = true;

      var format = mmdata.asFile.format;
      if (format?.startsWith('image/') ?? false){
        /// ファイル形式が画像
        isImage = true;
        break;
      }
      else if (format?.startsWith('video/') ?? false) {
        /// ファイル形式が動画
        isVideo = true;
        break;
      } else isOthers = true;
    }

    /// ポップアップメニュー項目設定
    var _menuList = [
      if (!readOnly)
        TimelineItemPopup.INSERT_TOP,
      if (!readOnly)
        TimelineItemPopup.INSERT_BELOW,

      /// 編集可能なアイテムなら『削除』を表示
      if (!readOnly && isCreator)
        TimelineItemPopup.DELETE,

      /// ファイルが含まれれば『ダウンロード』を表示
      if (isImage)
        TimelineItemPopup.DOWNLOAD_IMAGE,
      if (isVideo || isOthers)
        TimelineItemPopup.DOWNLOAD_FILE,

      if ((timeline is Channel) && (recordEntity.entity is Item))
        TimelineItemPopup.COPY_DEEP_LINK,

      if ((timeline is Channel) && (recordEntity.entity is Item))
        TimelineItemPopup.COPY_NODE,

      if ((timeline is Channel) && (recordEntity.entity is Item) && !recordEntity.isFirstCopy)
        TimelineItemPopup.COPY_ADDITIONAL_NODE,

      if (!readOnly && (timelineSchemata != null))
        TimelineItemPopup.DISCLOSE_TO_FRIEND,
    ];
    if (_menuList.isEmpty) return;

    RenderBox overlay = Overlay.of(context).context.findRenderObject() as RenderBox;
    TimelineItemPopup? selected;
    selected = await showMenu(
        context: context,
        position: RelativeRect.fromRect(
            tapPosition & const Size(-20, -20),
            Offset.zero & overlay.size,
        ),
        items: _menuList.map((TimelineItemPopup menuItem) {
            return new PopupMenuItem<TimelineItemPopup>(
                child: Text(menuItem.label),
                value: menuItem,
            );
        }).toList()
    ).catchError((_) => null);

    if (selected == null) return;

    switch (selected) {
      case TimelineItemPopup.INSERT_TOP:
      case TimelineItemPopup.INSERT_BELOW: {
        /// 上に挿入 / 下に挿入
        _insert(context, selected);
        break;
      }

      case TimelineItemPopup.DELETE: _delete(context); break;

      case TimelineItemPopup.DOWNLOAD_IMAGE:
      case TimelineItemPopup.DOWNLOAD_FILE: {
        /// 画像のダウンロード
        _createDownloadDialog(context);
        break;
      }

      case TimelineItemPopup.COPY_DEEP_LINK: _copyDeepLink(context); break;

      case TimelineItemPopup.COPY_NODE: _copyNode(context); break;

      case TimelineItemPopup.COPY_ADDITIONAL_NODE: _addCopyNode(context); break;

      case TimelineItemPopup.DISCLOSE_TO_FRIEND: {
        _discloseToFriend(context);
        break;
      }
    }
  }

  Future<void> _insert(
    BuildContext context,
    TimelineItemPopup selected,
  ) => showDialog<RecordEntity?>(
    context: context,
    builder: (context) => EntityDialog(
      account, EntityDialogSettings(), timeline,
      /// 開始日時をわずかにずらしたアイテムを作成
      createRecordEntityItemAdd(recordEntity, selected)!,
      false, true,
    ),
  ).then(updatedCallback);

  Future<void> _delete(BuildContext context) => showDialog(
    context: context,
    builder: (context) => _showDeleteConfirmDialog(context),
  );

  Future<void> _copyDeepLink(BuildContext context) async {
    await Clipboard.setData(
      ClipboardData(
        text: (await (recordEntity.entity as Item).toUri()).toString(),
      ),
    );
    showMessage(context, 'Deep link copied to clipboard.'.i18n);
  }

  Future<void> _copyNode(BuildContext context) async {
    recordEntity.resetClipboardTimeline();
    _addCopyNode(context);
  }

    Future<void> _addCopyNode(BuildContext context) async {
    recordEntity.copyNode(recordEntity, userPlrId);
    await Clipboard.setData(
      ClipboardData(
        text: (await (recordEntity.entity as Item).toUri()).toString(),
      ),
    );
    showMessage(context, 'Node and Deep link copied to clipboard.'.i18n);
  }

  Future<void> _discloseToFriend(BuildContext context) async {
    Friend friend; {
      var friends = await showFriendSelectDialog(
        context, title: 'Select a friend to disclose'.i18n,
        empty: 'No friends to disclose.'.i18n,
        onError: (e, s) => showError(context, e, s),
      );
      if (friends == null) return;

      friend = friends[0];
    }

    var meToFriendRoot = friend.meToFriendRoot;
    if (meToFriendRoot == null) {
      meToFriendRoot = await runWithProgress(
        context, () async {
          await friend.syncSilently();
          return friend.meToFriendRoot;
        },
      );
      if (meToFriendRoot == null) {
        showMessage(context, "The friend has no me-to-friend root.".i18n);
        return;
      }
    }

    var item = recordEntity.entity;
    if (item is! Item) {
      showMessage(context, "The node is not an timeline item.".i18n);
      return;
    }

    String name; {
      var plrId = friend.plrId;
      name = plrId.email;

      var storageId = friend.friendStorage?.id;
      if (storageId != null) {
        var userData = await userDataOf(storageId, plrId.toString());
        if (userData?.name != null) name = "${userData!.name} (${name})";
      }
    }

    if (!await showConfirmDialog(
        context, "Disclose to friend".i18n,
        "You're about to disclose the node to:\n- %s.\nAre you sure?"
          .i18n.fill([ name ]),
    )) return;

    try {
      await runWithProgress(
        context, () async {
          await meToFriendRoot!.syncSilently();
          await meToFriendRoot.addItem(
            item, notificationRegistry: notificationRegistry,
          );
          await meToFriendRoot.syncSilently();
        },
      );
    } catch (e, s) {
      processNeedNetworkException(context, e, s);
      return;
    }

    showConfirmDialog(
      context, "Disclose to friend".i18n,
      "Node disclosure to the friend has been completed.".i18n,
      okButtonLabel: MaterialLocalizations.of(context).closeButtonLabel,
      cancelButtonLabel: 'Open user details'.i18n,
      onCancel: () => Navigator.push(context, CupertinoPageRoute(
          builder: (_) => FriendPage(friend),
      )),
    );
  }

  /// アイテム追加・編集時コールバック
  void onPressed(RecordEntity recordEntity) {
    /// タイムライン画面へコールバック
    updatedCallback(recordEntity);
  }

  @override
  Widget build(BuildContext context) {
    /// contentAreaに表示するアイテムのリスト
    List<TimelineItemContentArea> contentList = [];

    /// 左側Widget, 右側Widget
    Widget leftPosition;
    Widget? rightPosition;

    /// アイテムの作成者を判定
    bool owner = isCreator;

    /// アイテム作成日付Widget
    var dateWidget = dateArea ?
      TimelineItemDateArea(recordEntity, dateArea) : null;

    /// アイテム作成者Widget
    var ownerWidget = !contentOnly ?
      TimelineItemOwner(recordEntity, owner) : null;

    /// アイテム開始日時／終了日時Widget
    var periodWidget = !contentOnly ?
      TimelineItemPeriod(recordEntity, fullDate: fullDate) : null;

    /// Mmdata
    if (formatResult.hasMmdata) {
      /// Mmdataを持っていればリスト取得
      List<Mmdata> mmdataList = formatResult.mmdatasById("cnt");

      /// リストからMmdataを取得しcontentListに追加
      for (Mmdata mmdata in mmdataList) {
        contentList.add(
          TimelineItemContentArea(
            account, timeline, timelineSchemata, recordEntity,
            readOnly, isCreator,
            formatResult, mmdata,
            updatedCallback, preDeleteCallback, deletedCallback,
            storePosition, showPopupMenu
          )
        );
      }
    }

    if (contentOnly) {
      leftPosition = _createBalloon(contentList, periodWidget, context, owner);
    } else if (owner) {
      /// アイテム作成者なら右に作成者、左にバルーン表示
      rightPosition = _createCreator(ownerWidget!, context);
      leftPosition = _createBalloon(contentList, periodWidget, context, owner);
    } else {
      /// アイテム作成者でなければ右にバルーン表示、左に作成者
      leftPosition = _createCreator(ownerWidget!, context);
      rightPosition = _createBalloon(contentList, periodWidget, context, owner);
    }

    var widget = Column(
      children: [
        if (dateWidget != null) Container(
          child: GestureDetector(
            /// アイテムタップイベント
            /// ソフトキーボードを閉じる
            onTap: () => FocusScope.of(context).unfocus(),
            child: dateWidget,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(width: 5),
            leftPosition,
            if (rightPosition != null) rightPosition,
            const SizedBox(width: 5),
          ],
        )
      ],
    );
    if (contentOnly) return widget;

    return ChangeNotifierProvider<RecordEntityCreatorNotifier>.value(
      key: Key('timeline_Item_${recordEntity.id}'),
      value: _getCreatorNotifier(),
      child: widget,
    );
  }

  static final Map<String, RecordEntityCreatorNotifier> _creatorNotifierCache = {};

  RecordEntityCreatorNotifier _getCreatorNotifier()
     => (_creatorNotifierCache[recordEntity.id]
            ??= RecordEntityCreatorNotifier(
                  recordEntity, timeline: timeline,
                )
        )..update(true);

  /// 所有者判定
  /// 自分が所有者であればtrue、それ以外の場合はfalseを返す
  bool get isCreator => (userPlrId == (recordEntity.creator ?? userPlrId));

  bool _mayShowCredentialsDialog() {
    if (!hasCredentials(recordEntity)) return false;

    showCredentialsDialog(
      context, root, timeline, timelineSchemata,
      recordEntity, updatedCallback,
    );
    return true;
  }

  /// 所有者表示エリア
  Widget _createCreator(Widget owner, BuildContext context) {
    return GestureDetector(
        child: Container(
          padding: const EdgeInsets.only(left: 10, top: 6, right: 10),
          child: owner,
        ),

        /// アイテムタップイベント
        /// ソフトキーボードを閉じる
        onTap: () => FocusScope.of(context).unfocus(),

        /// タップダウン時イベント
        onTapDown: storePosition,

        /// アイテム長押しイベント
        onLongPress: () => showPopupMenu(context));
  }

  /// バルーンエリア生成
  Widget _createBalloon(List<Widget> contentList, Widget? period, BuildContext context, bool isCreator) {
    bool itemReadOnly = readOnly;
    if (!itemReadOnly && !isCreator && !recordEntity.isEditable) {
      itemReadOnly = !recordEntity.hasMarkedUncompletedInputtableNext;
    }

    var typeImageData = recordEntity.type.image?.defaultData;

    return Expanded(
      child: Container(
        child: Column(
          children: <Widget>[
            if (!contentOnly && !isCreator)
              /// フレンド所有アイテムの場合
              Container(child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  /// フレンド名
                  GestureDetector(
                      child: ContentFriendName(isCreator),
                      /// アイテムタップイベント
                      onTap: () async {
                        /// ソフトキーボードを閉じる
                        FocusScope.of(context).unfocus();

                        if (_mayShowCredentialsDialog()) return;

                        if (timeline is Channel) {
                          /// チャネルの場合はEntity dialogへ
                          showDialog<RecordEntity?>(
                            context: context,
                            builder: (context) =>
                                EntityDialog(
                                  account, EntityDialogSettings(), timeline, recordEntity,
                                  readOnly, isCreator,
                                ),
                          ).then(updatedCallback);
                        } else {
                          /// 生活録の場合はDate dialogへ
                        }},
                      /// タップダウン時イベント
                      onTapDown: storePosition,
                      /// アイテム長押しイベント
                      onLongPress: () => showPopupMenu(context)
                  ),
                  /// 期限
                  if (period != null) _createPeriodArea(period, context)
                ]),
              ),

            /// タイムラインアイテム開始日／終了日
            if (period != null && isCreator)
              _createPeriodArea(period, context),

            /// バルーン部分
            Container(
              key: const Key('Balloon'),
              color: recordEntity.checkUncompleted(false) ? (!itemReadOnly ? Colors.pink[100] : const Color(0xffffe0b2)) : Colors.white ,
              padding: const EdgeInsets.only(right: 0, left: 0, top: 0),
              child: Column(
                children: <Widget>[
                  /// メッセージの場合は表示しない
                  if (recordEntity.type.id != "M")
                  Row(
                    children: <Widget>[
                      /// 重要度1 (※よく分からないため非表示)
                      if (_importance1Visible == true)
                        Expanded(
                          child: IconButton(
                            icon: const Icon(Icons.star, color: Colors.amberAccent),
                            iconSize: 20,
                            onPressed: () =>
                                print("[AW DBG] Importance1 icon tapped"),
                          ),
                        ),
                      if (_importance2Visible == true)

                        /// 重要度2 (※よく分からないため非表示)
                        Expanded(
                          child: IconButton(
                            icon: const Icon(Icons.star, color: Colors.amberAccent),
                            iconSize: 20,
                            onPressed: () =>
                                print("[AW DBG] Importance2 icon tapped"),
                          ),
                        ),

                      /// タイムラインアイテム
                      if (formatResult.typeLabel != null)
                        Expanded(
                          child: GestureDetector(
                              child: Container(

                                  /// 外枠
                                  padding: const EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: const Color(0xffaaaaaa),
                                          width: 0.3)),

                                  /// ラベルとフォーマット
                                  child: Row(children: <Widget>[
                                    if (typeImageData != null) Container(
                                      height: 20,
                                      margin: const EdgeInsets.only(right: 5),
                                      child: memoryImageOf(typeImageData),
                                    ),

                                    if (formatResult.text?.isNotEmpty != true)
                                      /// formatが無ければアイテムラベルのみ表示
                                    Flexible(
                                        child: AutoConvertUrlAndEmailToLinkText(
                                      formatResult.typeLabel!,
                                      softWrap: true,
                                      textStyle: const TextStyle(
                                          color: const Color(0xff000000),
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "Roboto",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 16),
                                          )
                                      ),

                                    /// フォーマットが存在すれば表示
                                    if (formatResult.text?.isNotEmpty == true)
                                      /// formatがあればアイテムラベルとformatを表示
                                      Flexible(
                                        child: Wrap(
                                            children: [
                                              AutoConvertUrlAndEmailToLinkText(
                                                (formatResult.typeLabel! + ' ' + formatResult.text!).substring(0, formatResult.typeLabel!.length),
                                                softWrap: true,
                                                textStyle: const TextStyle(
                                                    color: const Color(0xff000000),
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: "Roboto",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 16),
                                              ),
                                              AutoConvertUrlAndEmailToLinkText(
                                                (formatResult.typeLabel! + ' ' + formatResult.text!).substring(formatResult.typeLabel!.length),
                                                softWrap: true,
                                                textStyle: const TextStyle(
                                                    color: const Color(0xff000000),
                                                    fontWeight: FontWeight.normal,
                                                    fontFamily: "Roboto",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 14),
                                              ),
                                            ],
                                          ),
                                        ),
                                    ]
                                  )
                              ),

                              /// アイテムタップイベント
                              /// Entity dialogへ
                              onTap: () async {
                                /// ソフトキーボードを閉じる
                                FocusScope.of(context).unfocus();

                                if (_mayShowCredentialsDialog()) return;

                                showDialog<RecordEntity?>(
                                  context: context,
                                  builder: (context)
                                    => EntityDialog(
                                      account, EntityDialogSettings(), timeline, recordEntity,
                                      readOnly, isCreator,
                                    ),
                                ).then(updatedCallback);
                              },

                              /// タップダウン時イベント
                              onTapDown: storePosition,

                              /// アイテム長押しイベント
                              onLongPress: () => showPopupMenu(context)),
                          flex: 10,
                        ),
                    ],
                  ),

                  /// Mmdata
                  Column(
                    children: contentList,
                  ),
                ],
              ),
            ),
          ],
        ),
        // width: double.infinity,
      ),
    );
  }

  Widget _createPeriodArea(Widget period, BuildContext context) {
    /// 文字列を取得('()'が含まれる場合もあるためendもチェックする)
    var formatter = intl.DateFormat('HH:mm');
    String dateTime = formatter.format(recordEntity.begin!);
    if (recordEntity.end != null) {
      /// end設定ありの場合
      var begin = DateTime(recordEntity.begin!.year, recordEntity.begin!.month, recordEntity.begin!.day);
      var end = DateTime(recordEntity.end!.year, recordEntity.end!.month, recordEntity.end!.day);
      if (begin.compareTo(end) != 0) {
        /// 同日でない場合
        var endFormatter = intl.DateFormat('MM-dd (E.) HH:mm'.i18n);
        dateTime = endFormatter.format(recordEntity.end!);
      }
    }

    /// 文字列の高さを取得
    double height = 20.0;
    TextSpan ts = new TextSpan(text: dateTime, style: Theme.of(context).textTheme.bodyLarge);
    TextPainter tp = new TextPainter(text: ts, textDirection: TextDirection.ltr);
    tp.layout();
    var textHeight = tp.height * MediaQuery.textScalerOf(context).scale(1.0);
    /// 文字の高さ+上下paddingが予定値(20)を超えたら書換
    if (height < textHeight) height = textHeight;

    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        child: Container(
          alignment: Alignment.centerRight,
          height: height,
          child: period,
        ),

        /// アイテムタップイベント
        onTap: () async {
          /// ソフトキーボードを閉じる
          FocusScope.of(context).unfocus();

          if (_mayShowCredentialsDialog()) return;

          if (timeline is Channel) {
            /// チャネルの場合はEntity dialogへ
            showDialog<RecordEntity?>(
              context: context,
              builder: (context) =>
                EntityDialog(
                  account, EntityDialogSettings(), timeline, recordEntity,
                  readOnly, isCreator,
                ),
            ).then(updatedCallback);
          } else {
            /// 生活録の場合はDate dialogへ
          }
        },
        /// タップダウン時イベント
        onTapDown: storePosition,
        /// アイテム長押しイベント
        onLongPress: () => showPopupMenu(context));
  }

  Future<void> _createDownloadDialog(BuildContext context) async {
    FocusScope.of(context).unfocus();
    showDownloadDialog(context, formatResult.mmdatas, isCreator);
  }

  Widget _showDeleteConfirmDialog(BuildContext context) {
    return AlertDialog(
      title: Text("Delete the item".i18n),
      content: Text('Are you sure you want to delete this item?'.i18n),
      actions: <Widget>[
        TextButton(
          child: Text('Cancel'.i18n),
          onPressed: () => Navigator.pop(context),
        ),
        TextButton(
            child: Text('OK'.i18n),
            onPressed: () {
              preDeleteCallback(recordEntity);
              recordEntity.delete();
              recordEntity.save(timeline, false).then((result) {
                if (result) {
                  deletedCallback(recordEntity);
                }
              });
              Navigator.pop(context);
            },
        ),
      ],
    );
  }
}

/// Mmdataアイテム用クラス
class TimelineItemContentArea extends StatelessPlrWidget {
  final Timeline timeline;
  final Schemata? timelineSchemata;
  final RecordEntity recordEntity;
  final bool isReadOnly;
  final bool isCreator;
  final FormatResult formatResult;
  final Mmdata mmdata;
  final RecordEntityCallback updatedCallback;
  final RecordEntityCallback preDeleteCallback;
  final RecordEntityCallback deletedCallback;
  final void Function(TapDownDetails details) storePosition;
  final Function showPopupMenu;

  TimelineItemContentArea(
      super.account,
      this.timeline,
      this.timelineSchemata,
      this.recordEntity,
      this.isReadOnly,
      this.isCreator,
      this.formatResult,
      this.mmdata,
      this.updatedCallback,
      this.preDeleteCallback,
      this.deletedCallback,
      this.storePosition,
      this.showPopupMenu,
      {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (formatResult.typeLabel != null && !mmdata.isFile &&
        (mmdata.isLiteral && mmdata.text.defaultValue == "")) {
      /// cnt未入力の場合は空コンテナを返す
      return Container();
    }

    Color? mmdataColor = (recordEntity.isUncompleted) ? Colors.pink[100] : Colors.teal[50];
    /// 他人のエンティティは薄黄色(#FFFFCC)にする
    if (!isCreator) mmdataColor = const Color(0xffffffcc);

    Uint8List? thumbnailData;
    if (mmdata.isFile) {
      thumbnailData = mmdata.asFile.thumbnailData;
      if (thumbnailData?.isEmpty == true) thumbnailData = null;
    }

    /// コメントのパディングはファイル有無によって変更する
    var commentPadding = const EdgeInsets.all(5.0);
    if (thumbnailData != null) {
      /// 画像ありの場合はleftパディングはなし
      commentPadding =  const EdgeInsets.only(top: 5.0, right: 5.0, bottom: 5.0);
    }

    var imageWidth = 150.0;
    var imageHeight = 130.0;
    if (thumbnailData != null) {
      /// ファイルが存在する場合
      if (mmdata.asFile.format?.startsWith('image/') == true) {
        image.Image? thumbnailImage; try {
          thumbnailImage = image.decodeImage(thumbnailData);
        } catch (e, s) {
          print(e); print(s);
        }
        if (thumbnailImage != null) {
          /// 画像ファイルの場合はサムネイルサイズに応じて表示
          if (thumbnailImage.width > thumbnailImage.height) {
            imageHeight = thumbnailImage.height.toDouble();
            if (imageHeight < 100) imageHeight = 100;
          } else {
            imageWidth = thumbnailImage.width.toDouble();
            if (imageWidth < 100) imageWidth = 100;
          }
        }
      } else {
        /// 画像ファイル以外の場合は固定サイズでアイコン表示
        imageHeight = 50;
        imageWidth = 50;
      }
    }

    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              child: Container(
                /// 外枠
                decoration: BoxDecoration(
                  border: Border.all(
                    color: const Color(0xffaaaaaa),
                    width: 0.3
                  )
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if (mmdata.isFile && mmdata.asFile.text.isNotEmpty &&
                        mmdata.asFile.text.defaultValue != "")
                      /// ファイルの場合
                      /// タイトル
                      Container(
                        padding: const EdgeInsets.only(left: 5.0, top: 5.0),
                        width: double.infinity,
                        color: mmdataColor,
                        child: AutoConvertUrlAndEmailToLinkText(
                          mmdata.asFile.text.defaultValue ?? "",
                          textStyle: const TextStyle(
                              color: const Color(0xff000000),
                              fontWeight: FontWeight.normal,
                              fontFamily: "Roboto",
                              fontStyle: FontStyle.normal,
                              fontSize: 16),
                        ),
                      ),
                    Container(
                      color: mmdataColor,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          if (thumbnailData != null)
                            /// ファイルの場合
                            /// イメージ
                            GestureDetector(
                                child: Container(
                                  width: imageWidth,
                                  height: imageHeight,
                                  padding: const EdgeInsets.all(5),
                                  child: (mmdata.asFile.format?.startsWith('video/') ?? false) ?
                                    Container(
                                      color: Colors.black,
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          const Icon(
                                            Icons.movie,
                                            color: Colors.white,
                                            size: 36,
                                          ),
                                          Container(
                                            padding: const EdgeInsets.only(top: 4),
                                            child: const Icon(
                                              Icons.play_arrow,
                                              color: Colors.black,
                                              size: 16,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ) : memoryImageOf(thumbnailData),
                                ),

                                /// アイテムタップイベント
                                onTap: () => onThumbnailTap(context),

                                /// タップダウン時イベント
                                onTapDown: storePosition,

                                /// アイテム長押しイベント
                                onLongPress: () => showPopupMenu(context),
                            ),

                          if (mmdata.isFile &&
                              mmdata.asFile.comment.isNotEmpty &&
                              mmdata.asFile.comment.defaultValue != "")
                            /// コメント(ファイルの場合)
                            Flexible(
                              child: Container(
                                padding: commentPadding,
                                child: ContentCommentArea(mmdata.asFile.comment.defaultValue ?? ""),
                              )
                            ),

                          if (mmdata.isLiteral &&
                              mmdata.text.isNotEmpty)
                            /// コメント(リテラルの場合)
                            Flexible(
                              child: Container(
                                padding: const EdgeInsets.all(5),
                                child: ContentCommentArea(mmdata.asLiteral.text.value ?? "",),
                              )
                            ),
                        ]
                      )
                    )
                  ]
                )
              ),

              /// アイテムタップイベント
              /// Entity dialogへ
              onTap: () {
                /// ソフトキーボードを閉じる
                FocusScope.of(context).unfocus();

                if (hasCredentials(recordEntity)) {
                  showCredentialsDialog(
                    context, root, timeline, timelineSchemata,
                    recordEntity, updatedCallback,
                  );
                  return;
                }

                showDialog<RecordEntity?>(
                  context: context,
                  builder: (context) =>
                    EntityDialog(
                      account, EntityDialogSettings(), timeline, recordEntity,
                      isReadOnly, isCreator,
                    ),
                ).then(updatedCallback);
              },

              /// タップダウン時イベント
              onTapDown: storePosition,

              /// アイテム長押しイベント
              onLongPress: () => showPopupMenu(context)))
    ]));
  }

  void onThumbnailTap(BuildContext context) async {
    /// ソフトキーボードを閉じる
    FocusScope.of(context).unfocus();
    if (mmdata.asFile.format?.startsWith('image') ?? false) {
      Navigator.push(context, ImageModal.fromMmdata(mmdata.asFile));
    }
    else if (mmdata.asFile.format?.startsWith('video') ?? false) {
      Navigator.push(context, VideoModal.fromMmdata(mmdata.asFile));
    }
    else {
      showDownloadDialog(context, [mmdata], isCreator);
    }
  }
}

/// タイムラインアイテム日付
class TimelineItemDateArea extends StatelessWidget {
  final RecordEntity recordEntity;
  final bool dateArea;

  TimelineItemDateArea(this.recordEntity, this.dateArea, {super.key});

  /// タイムラインアイテムの日付を取得
  String _getItemDate() {
    /// 表示フォーマットに変換
    var formatter = intl.DateFormat('yyyy-MM-dd (E.)'.i18n);
    return formatter
        .format(recordEntity.begin!)
        .toString();
  }

  @override
  Widget build(BuildContext context) {
    if (dateArea) {
      return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                /// タイムラインアイテム日付
                Container(
                  child: Text(
                    _getItemDate(),
                    style: const TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          )
      );
    } else {
      return Container();
    }
  }
}

const _ownerIconSize = 40.0;

const _personIcon = const Icon(Icons.person, size: _ownerIconSize);

Image? __googleFitIcon;
Image get _googleFitIcon => __googleFitIcon ??= Image.asset(
  "assets/gfit_512dp.png", height: _ownerIconSize,
  filterQuality: FilterQuality.medium, alignment: Alignment.centerLeft,
);

Image? __appleHealthIcon;
Image get _appleHealthIcon => __appleHealthIcon ??= Image.asset(
  "assets/Apple_Health.png", height: _ownerIconSize,
  filterQuality: FilterQuality.medium, alignment: Alignment.centerLeft,
);

Image? __GPT4Icon;
Image get _GPT4Icon => __GPT4Icon ??= Image.asset(
  "assets/gpt4.webp", height: _ownerIconSize,
  filterQuality: FilterQuality.medium, alignment: Alignment.centerLeft,
);

Image? __PAIIcon;
Image get _PAIIcon => __PAIIcon ??= Image.asset(
  "assets/pai.png", height: _ownerIconSize,
  filterQuality: FilterQuality.medium, alignment: Alignment.centerLeft,
);

/// タイムラインアイテム所有者
class TimelineItemOwner extends StatelessWidget {
  final RecordEntity recordEntity;
  final bool isCreator;

  TimelineItemOwner(this.recordEntity, this.isCreator, {super.key});

  @override
  Widget build(BuildContext context) {
    if (isCreator) {
      /// 所有者が自分の場合
      return Column( children: <Widget>[
        const SizedBox(height: 20),
        Container(
            child: Text('Me'.i18n,
                style: const TextStyle(
                    color: const Color(0xff000000),
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto",
                    fontStyle: FontStyle.normal,
                    fontSize: 12),
                textAlign: TextAlign.center))
      ]);
    } else {
      /// 所有者が自分以外の場合
      final creator = recordEntity.creator;
      if (creator == creatorGoogleFit) {
        return _googleFitIcon;
      }
      else if (creator == creatorAppleHealth ||
               creator == 'HealthKit') {
        return _appleHealthIcon;
      }
      else if (creator == creatorGPT4) {
        return _GPT4Icon;
      }
      else if (creator == creatorPAI) {
        return _PAIIcon;
      }
      else {
        return Consumer<RecordEntityCreatorNotifier>(
          key: Key('${recordEntity.id}_${recordEntity.creator}'),
          builder: (context, notifier, child) {
            return Container(
              height: _ownerIconSize,
              child: (notifier.picture != null) ?
                memoryImageOf(notifier.picture!) : _personIcon
            );
          }
        );
      }
    }
  }
}

/// タイムラインアイテムの開始日時と終了日時
class TimelineItemPeriod extends StatelessWidget {
  final RecordEntity recordEntity;
  final bool fullDate;

  TimelineItemPeriod(this.recordEntity, {super.key, this.fullDate = false});

  /// タイムラインアイテムの日付を取得
  String _getItemPeriod() {
    bool isDateOnly = recordEntity.isDateOnly == true;

    intl.DateFormat formatter; {
      if (fullDate) {
        if (isDateOnly) {
          formatter = intl.DateFormat('yyyy-MM-dd (E.) HH:mm'.i18n);
        } else {
          formatter = intl.DateFormat('yyyy-MM-dd (E.)'.i18n);
        }
      } else {
        if (isDateOnly) {
          formatter = intl.DateFormat('MM-dd (E.)'.i18n);
        } else {
          formatter = intl.DateFormat('HH:mm');
        }
      }
    }

    /// offset用日付
    String returnPeriod = formatter.format(recordEntity.begin!);
    if (recordEntity.end != null) {
      /// end設定ありの場合
      var begin = DateTime(recordEntity.begin!.year, recordEntity.begin!.month, recordEntity.begin!.day);
      var end = DateTime(recordEntity.end!.year, recordEntity.end!.month, recordEntity.end!.day);
      if (begin.compareTo(end) == 0) {
        /// 同日の場合
        if (!isDateOnly) returnPeriod += " ~ " + formatter.format(recordEntity.end!);
      } else {
        /// 同日でない場合
        var endFormatter;
        if (recordEntity.begin!.year == recordEntity.end!.year) {
          /// 同年の場合
          if (isDateOnly) {
            endFormatter = intl.DateFormat('MM-dd (E.)'.i18n);
          } else {
            endFormatter = intl.DateFormat('MM-dd (E.) HH:mm'.i18n);
          }
        } else {
          /// 別年の場合
          if (isDateOnly) {
            endFormatter = intl.DateFormat('yyyy-MM-dd (E.)'.i18n);
          } else {
            endFormatter = intl.DateFormat('yyyy-MM-dd (E.) HH:mm'.i18n);
          }
        }
        // 日付のみ且つ同日でない場合beginは非表示
        if (isDateOnly) returnPeriod = "";
        returnPeriod += " ~ " +
            endFormatter.format(recordEntity.end);
      }
    }
    return returnPeriod;
  }

  @override
  Widget build(BuildContext context) {
    return Text(_getItemPeriod(),
        style: const TextStyle(
            color: const Color(0xff000000),
            fontWeight: FontWeight.w400,
            fontFamily: "Roboto",
            fontStyle: FontStyle.normal,
            fontSize: 14),
        textAlign: TextAlign.right);
  }
}

/// MmdataコメントエリアのテキストWidgetクラス
class ContentCommentArea extends StatelessWidget {
  final String commentText;

  ContentCommentArea(this.commentText,);

  @override
  Widget build(BuildContext context) {
    /// リンク外テキストデフォルトスタイル
    var textStyle = Theme.of(context)
        .textTheme
        .bodyLarge!
        .merge(const TextStyle(
        fontWeight: FontWeight.normal,
        fontFamily: "Roboto",
        fontStyle: FontStyle.normal,
        color: Colors.black,
        fontSize: 16));

    return AutoConvertUrlAndEmailToLinkText(commentText, textStyle: textStyle, softWrap: true,);
  }
}

class ContentFriendName extends StatelessWidget {
  final bool isCreator;

  ContentFriendName(this.isCreator, {super.key});

  @override
  Widget build(BuildContext context) {
    if (isCreator) {
      // 「自分」の場合はアイコン部分に表示しているので何も出さない
      return Container();
    }
    else {
      return Consumer<RecordEntityCreatorNotifier>(
        builder: (context, notifier, child) {
          final creator = notifier.recordEntity.creator;
          String? name;
          if (creator == creatorGoogleFit) {
            name = creatorGoogleFit;
          }
          else if (creator == creatorAppleHealth || creator == 'HealthKit') {
            name = creatorAppleHealth;
          }
          else if (creator == creatorGPT4) {
            name = creatorGPT4;
          }
          else if (creator == creatorPAI) {
            name = creatorPAI;
          }
          else {
            name = notifier.name;
          }

          return Container(
            child: AutoSizeText(
              name ?? '',
              style: const TextStyle(
                color: const Color(0xff000000),
                fontWeight: FontWeight.w400,
                fontFamily: "Roboto",
                fontStyle: FontStyle.normal,
                fontSize: 10,
              ),
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
          );
        }
      );
    }
  }
}
