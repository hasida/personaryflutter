import 'package:i18n_extension/i18n_extension.dart';

import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText('en_us') +
      /// timeline_state_base
      unknownTranslation +
      deepLinkTranslation +
      deepLinkTooltipTranslation +
      deepLinkCopiedTranslation +
      {
        'en_us': 'Copy node',
        'ja_jp': 'ノードをコピー',
      } +
      {
        'en_us': 'Copy additional node',
        'ja_jp': 'ノードを追加コピー',
      } +
      {
        'en_us': 'Node and Deep link copied to clipboard.',
        'ja_jp': 'ノードとディープリンクをクリップボードにコピーしました。',
      } +
      {
        'en_us': 'Timeline data coordination.',
        'ja_jp': 'タイムラインデータ連携',
      } +
      {
        'en_us': 'Date time range of the data to be sent',
        'ja_jp': '送信するデータの日時範囲',
      } +
      {
        'en_us': 'Invalid date time range.',
        'ja_jp': '不正な日時範囲です。',
      } +
      {
        'en_us': 'Send',
        'ja_jp': '送信',
      } +
      {
        'en_us': 'The result has been saved',
        'ja_jp': '結果が保存されました',
      } +
      {
        'en_us':
          'The coordination result has been saved in Downloads folder\n'
          'FileName: %s',
        'ja_jp':
          '連携結果がダウンロードフォルダに保存されました。\n'
          'ファイル名: %s',
      } +
      {
        'en_us': 'Open',
        'ja_jp': '開く',
      } +
      {
        'en_us': 'Coordination Result',
        'ja_jp': '連携結果',
      } +
      {
        'en_us': 'Me',
        'ja_jp': '自分',
      } +
      {
        'en_us': 'Select schema',
        'ja_jp': 'スキーマ選択',
      } +
      {
        'en_us': 'Profile',
        'ja_jp': 'プロフィール',
      } +
      {
        'en_us': 'Disclosure history',
        'ja_jp': '開示履歴',
      } +
      /// timeline_page
      {
        'en_us': 'Retrieving the schema.\nPlease wait a while...',
        'ja_jp': 'スキーマの取得中です。少しお待ちください。',
      } +
      {
        'en_us': 'Timeline is disabled.',
        'ja_jp': 'タイムラインが無効です。',
      } +
      {
        'en_us': 'Timeline is disabled.\nPlease select another schema.',
        'ja_jp': 'タイムラインが無効です。\n他のスキーマを選択してください。',
      } +
      {
        'en_us': 'Loading data',
        'ja_jp': 'データを取得中です',
      } +
      {
        'en_us': 'Channel summary type',
        'ja_jp': 'チャネルサマリ種類',
      } +
      {
        'en_us': 'yyyy-MM-dd (E.)',
        'ja_jp': 'yyyy年MM月dd日(E)',
      } +
      {
        'en_us': 'Save data...',
        'ja_jp': 'データの保存中です...',
      } +
      {
        'en_us': 'Add comment',
        'ja_jp': 'コメントを追加',
      } +
      {
        'en_us': 'No data',
        'ja_jp': 'データはありません',
      } +
      /// timeline_item
      {
        'en_us': 'OK',
        'ja_jp': 'OK',
      } +
      {
        'en_us': 'Cancel',
        'ja_jp': 'キャンセル',
      } +
      {
        'en_us': 'Add front',
        'ja_jp': '上に挿入',
      } +
      {
        'en_us': 'Add rear',
        'ja_jp': '下に挿入',
      } +
      {
        'en_us': 'Delete',
        'ja_jp': '削除',
      } +
      {
        'en_us': 'Download image(s)',
        'ja_jp': '画像のダウンロード',
      } +
      {
        'en_us': 'Download',
        'ja_jp': 'ファイルのダウンロード',
      } +
      {
        'en_us': 'Are you sure you want to download this file?',
        'ja_jp': 'ファイルをダウンロードしますか？',
      } +
      {
        'en_us': 'Downloading...',
        'ja_jp': 'ダウンロード中です...',
      } +
      {
        'en_us': 'File download completed.',
        'ja_jp': 'ファイルのダウンロードが完了しました',
      } +
      {
        'en_us': 'Failed to download file.',
        'ja_jp': 'ファイルのダウンロードに失敗しました',
      } +
      {
        'en_us': 'MM-dd (E.) HH:mm',
        'ja_jp': 'MM月dd日(E) HH:mm',
      } +
      {
        'en_us': 'yyyy-MM-dd (E.) HH:mm',
        'ja_jp': 'yyyy年MM月dd日(E) HH:mm',
      } +
      {
        'en_us': 'MM-dd (E.)',
        'ja_jp': 'MM月dd日(E)',
      } +
      {
        'en_us': 'yyyy-MM-dd (E.)',
        'ja_jp': 'yyyy年MM月dd日(E)',
      } +
      /// image_modal
      {
        'en_us': 'Loading image...',
        'ja_jp': '画像の読み込み中です...',
      } +
      /// video_modal
      {
        'en_us': 'Loading video...',
        'ja_jp': '動画の読み込み中です...',
      } +
      {
        'en_us': 'Schema',
        'ja_jp': 'スキーマ',
      } + {
        'en_us': 'Style',
        'ja_jp': 'スタイル',
      } +
      {
        'en_us': 'Large file',
        'ja_jp': '大きなファイル',
      } +
      {
        'en_us': 'Are you sure you want to download this large file?',
        'ja_jp': '大きなファイルをダウンロードしようとしています。\n続行しますか？',
      } +
      {
        'en_us': 'Delete the item',
        'ja_jp': 'アイテム削除',
      } +
      {
        'en_us': 'Are you sure you want to delete this item?',
        'ja_jp': 'アイテムを削除してよろしいですか？',
      } +
      {
        'en_us': '%s: Timeline has been updated - %s',
        'ja_jp': '%s: タイムラインの更新があります - %s',
      } +
      {
        'en_us': "Timeline of the channel `%s' has been updated.\nPlease check the following link:\n\n%s",
        'ja_jp': "チャネル「%s」のタイムラインに更新があります。\n下記のリンクを確認してください:\n\n%s",
      } +
      {
        'en_us': 'Updated by: %s',
        'ja_jp': '更新者: %s',
      } +
      {
        'en_us': 'Update:\n%s',
        'ja_jp': '更新:\n%s',
      } +
      {
        'en_us': 'Deletion:\n%s',
        'ja_jp': '削除:\n%s',
      } +
      {
        'en_us': '<Attachment>',
        'ja_jp': '<添付ファイル>',
      } +
      {
        'en_us': 'This file is not downloadable.',
        'ja_jp': 'このファイルはダウンロードできません。',
      } +
      {
        'en_us': 'Disclose to friend',
        'ja_jp': '友達に開示',
      } +
      {
        'en_us': 'Select a friend to disclose',
        'ja_jp': '開示先の友達を選択',
      } +
      {
        'en_us': 'No friends to disclose.',
        'ja_jp': '対象となる友達がいません',
      } +
      {
        'en_us': 'The friend has no me-to-friend root.',
        'ja_jp': '指定した友達のme-to-friend rootが存在しません。',
      } +
      {
        'en_us': 'The node is not an timeline item.',
        'ja_jp': '指定したノードはタイムラインアイテムではありません。',
      } +
      {
        'en_us': 'You\'re about to disclose the node to:\n- %s.\nAre you sure?',
        'ja_jp': '指定したノードを次の友達に開示します:\n- %s\nよろしいですか?',
      } +
      {
        'en_us': 'Node disclosure to the friend has been completed.',
        'ja_jp': '友人へのノード開示が完了しました。',
      } +
      {
        'en_us': 'Show credentials',
        'ja_jp': '証明書表示',
      } +
      {
        'en_us': 'Open user details',
        'ja_jp': '利用者詳細を開く',
      } +
      {
        'en_us': 'Generated failed',
        'ja_jp': '生成に失敗しました',
      };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
