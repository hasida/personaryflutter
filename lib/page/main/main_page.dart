import 'dart:io' show Platform;

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show AsyncCallback;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show MethodChannel, SystemNavigator;
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:i18n_extension/i18n_extension.dart';
import 'package:personaryFlutter/theme.dart';
import 'package:plr_ui/plr_ui.dart';
import 'package:navigator_scope/navigator_scope.dart';
import 'package:synchronized/extension.dart';

import '../../app.dart' show App;
import '../../config.dart';
import '../../logic.dart';
import '../../util.dart';
import '../../widget.dart';
import '../../dialog/friend_add/friend_add_dialog.dart';
import '../channel/channel_page.dart';
import '../friend/friend_page.dart';
import '../timeline/timeline_page.dart';
import 'active_app_definition.dart';
import 'app_definition.dart';
import 'main_page.i18n.dart';

class MainPage extends StatefulHookConsumerWidget {
  final bool isSignIn;
  const MainPage({
      super.key,
      this.isSignIn = false,
  });

  static void setTab(
    BuildContext context,
    ApplTabPageInterface tab, {
      bool forceBuild = false,
  }) => context.findAncestorStateOfType<_MainPageState>()?.setTab(tab);

  @override
  ConsumerState<MainPage> createState() => _MainPageState();
}

const _personaryChannel = MethodChannel("personary");

class _MainPageState extends PlrConsumerState<MainPage> {
  @override
  void initState() {
    super.initState();

    ApplTabPageBuild.reset();

    Future.microtask(() async {
        await requestNotificationsPermission(context);
        if (widget.isSignIn) await _initStorage();
    });
    updateBadgeCounter();

    clearAutomaticGenerationFutureMap();
  }

  Future<void> _initStorage() async {
    Future<void> showUsersDialog(
      String title,
      Iterable<HasPlrId> users,
    ) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title.i18n),
        content: Text(
          users.map((u) => u.plrId?.email).nonNulls.join("\n"),
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        actions: [
          TextButton(
            child: Text(MaterialLocalizations.of(context).okButtonLabel),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );

    var subscribed = <SubscribedUser>[];
    var deposited = <Friend>[];

    registerAutomaticFriends(
      account.root, automaticFriends,
      onSubscribeInfoChannel: subscribed.add,
      onDepositPassphrase: deposited.add,
    ).then((registered) async {
        if (registered.isEmpty) return;

        if (deposited.isNotEmpty) showPassphraseDepositedDialog(
          context, deposited,
        );
        if (subscribed.isNotEmpty) showUsersDialog(
          "The info channel of the friend(s) are subscribed", subscribed,
        );
        await showUsersDialog("Friend(s) are registered", registered);

        Future<void> _showAppDefinitionImportDialog(
          Friend friend,
        ) => registered.synchronized(() async {
            final publicRoot = friend.publicRoot;
            if (publicRoot == null) return;

            await publicRoot.syncSilently();

            if (publicRoot.hasAppDefinitions) {
              await showAppDefinitionImportDialog(
                context, ref, account, publicRoot,
              );
            }
        });
        await Future.wait(
          registered.map((f) => _showAppDefinitionImportDialog(f)),
        );
    });
  }

  void _initListeners(BuildContext context, WidgetRef ref) {
    ref.listen(
      plrMessageProvider, (_, next) {
        switch (next) {
          case AsyncData(:final value): {
            if (handleMessage(value)) return;
            if (value is Control) _processControl(value);
          }
          case AsyncError(:final error, :final stackTrace): {
            showError(context, error, stackTrace);
          }
        }
      },
    );

    Future.microtask(() => _processNotification(
        context, ref.read(systemNotificationQueueProvider),
    ));
    ref.listen(
      systemNotificationQueueProvider, (_, next) {
        _processNotification(context, next);
      },
    );
  }

  int _tabIndex = 0;
  List<ApplTabPageInterface> _tabs = const [ homeTab, settingsTab ];
  List<ApplTabPageInterface> get tabs => _tabs.toList();

  ApplTabPageInterface get currentTab => _tabs[_tabIndex];

  int _tabIndexOf(ApplTabPageInterface tab) =>
    _tabs.indexWhere((t) => t == tab);

  bool setTab(ApplTabPageInterface tab, { bool forceBuild = false }) {
    var tabIndex = _tabIndexOf(tab);
    if (tabIndex < 0) {
      if (forceBuild) {
        _tabs[_tabIndex].navigator!.push(
          MaterialPageRoute(builder: (_) => tab.build()),
        );
        return true;
      }
      return false;
    }
    if (_tabIndex == tabIndex) return true;

    setState(() => _tabIndex = tabIndex);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (didPop, _) async {
        if (didPop || (await _tabs[_tabIndex].navigator?.maybePop() == true)) {
          return;
        }
        if (!Platform.isAndroid) SystemNavigator.pop();
        else _personaryChannel.invokeMethod("moveTaskToBack");
      },
      child: I18n(
        child: SafeArea(
          child: ProgressHUD(
            child: Consumer(
              builder: _build,
            ),
          ),
        ),
      ),
    );
  }

  Widget _build(BuildContext context, WidgetRef ref, _) {
    _initListeners(context, ref);

    final AppDefinitionInterface appDefinition; {
      final String? activeAppDefinitionId;
      switch (ref.watch(activeAppDefinitionProvider(storage))) {
        case AsyncData(:final value): activeAppDefinitionId = value;
        default: return const Center(child: CircularProgressIndicator());
      }
      if (activeAppDefinitionId == null) {
        appDefinition = DefaultAppDefinition(account);
      } else {
        final appDefinitionRegistryValue = ref.watch(
          appDefinitionRegistrySyncProvider(appDefinitionRegistry),
        ).value;
        if (appDefinitionRegistryValue == null) {
          return const Center(child: CircularProgressIndicator());
        }
        final _appDefinition = appDefinitionRegistryValue
          .appDefinitions.firstWhereOrNull(
            (ad) => ad.id == activeAppDefinitionId,
          );
        if (_appDefinition == null) {
          appDefinition = DefaultAppDefinition(account);
        } else {
          final appDefinitionValue = ref.watch(
            appDefinitionSyncProvider(_appDefinition),
          ).value;
          if (appDefinitionValue == null) {
            return const Center(child: CircularProgressIndicator());
          }
          appDefinition = appDefinitionValue;
        }
      }
    }
    return _buildPage(context, ref, appDefinition);
  }

  Widget _buildPage(
    BuildContext context,
    WidgetRef ref,
    AppDefinitionInterface appDefinition,
  ) {
    _setAppDefinition(appDefinition);

    return Theme(
      data: appDefinition.theme,
      child: Scaffold(
        body: NavigatorScope(
          currentDestination: _tabIndex,
          destinationCount: _tabs.length,
          destinationBuilder: (_, index) => NestedNavigator(
            navigatorKey: _tabs[index].navigatorKey,
            builder: (_) => _tabs[index].build(),
          ),
        ),
        bottomNavigationBar: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Divider(height: 0),
            NavigationBar(
              height: 60,
              selectedIndex: _tabIndex,
              onDestinationSelected: (i) {
                if (_tabIndex == i) {
                  _tabs[_tabIndex].navigator?.popUntil((r) => r.isFirst);
                  return;
                }
                setState(() => _tabIndex = i);
              },
              destinations: _tabs.map(
                (t) {
                  String title, comment; {
                    final titleLabel = t.titleLabel?.defaultValue?.toString();
                    title = titleLabel ?? "Unknown".i18n;
                    comment = t.commentText?.defaultValue?.toString() ?? title;
                  }

                  return NavigationDestination(
                    icon: SizedBox(
                      width: 24,
                      child: HookBuilder(
                        builder: (context) {
                          final snapshot = useStream(
                            useMemoized(
                              () => t.titleIcon.handleError(
                                (e, s) => showError(context, e, s),
                              ),
                            ),
                          );
                          if (snapshot.data != null) return snapshot.data!;

                          return switch (snapshot.connectionState) {
                            ConnectionState.done =>
                              const Icon(Icons.broken_image),
                            _ => const LinearProgressIndicator(minHeight: 24,),
                          };
                        },
                      ),
                    ),
                    label: title,
                    tooltip: comment,
                  );
                },
              ).toList(),
            ),
          ],
        ),
      ),
    );
  }

  AppDefinitionInterface? _lastAppDefinition;

  void _setAppDefinition(AppDefinitionInterface appDefinition) {
    int getInitialTabIndex() {
      final i = appDefinition.tabPages.indexWhere(
        (t) => t.order == appDefinition.initialTab,
      );
      return (i > 0) ? i : 0;
    }

    if (
      (_lastAppDefinition != appDefinition) ||
      (_lastAppDefinition?.id != appDefinition.id)
    ) {
      final tabPages = appDefinition.tabPages;
      if (tabPages.isEmpty) _tabs = [ homeTab, settingsTab ];
      else _tabs = [ ...tabPages, settingsTab ];

      if ((_lastAppDefinition?.id == appDefinition.id)) {
        final lastTabPages = _lastAppDefinition!.tabPages;
        if (_tabIndex < lastTabPages.length) {
          final lastTabIndex = _tabIndexOf(lastTabPages[_tabIndex]);
          _tabIndex = (lastTabIndex < 0) ? getInitialTabIndex() : lastTabIndex;
        }
      } else {
        _tabIndex = getInitialTabIndex();
        _lastAppDefinition = appDefinition;
      }
    }
  }

  Future<bool> _setTab(
    ApplTabPageInterface tab, {
      bool forceBuild = false,
  }) async {
    if (!setTab(tab, forceBuild: forceBuild)) return false;

    while (tab.navigator == null) {
      await Future.delayed(const Duration(milliseconds: 1));
    }
    return true;
  }

  void _pushPage(
    WidgetBuilder builder, {
      bool fullscreenDialog = false,
  }) => currentTab.navigator?.push(
    CupertinoPageRoute(
      builder: builder, fullscreenDialog: fullscreenDialog,
    ),
  );

  Future<bool> _setTabAndPushPage(
    ApplTabPageInterface tab,
    WidgetBuilder builder, {
      bool forceBuildTab = false,
      bool forcePushPage = true,
      bool fullscreenDialog = false,
  }) async {
    if (!await _setTab(tab, forceBuild: forceBuildTab) && !forcePushPage) {
      return false;
    }
    _pushPage(builder, fullscreenDialog: fullscreenDialog);

    return true;
  }

  bool _restartDialogShown = false;

  Future<void> _processNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) => synchronized(() => runWithProgress(
      context, () async {
        if (_processPassphraseChangedNotification(context, queue)) return;

        await _processContentUpdatesNotification(context, queue);
        await _processFriendNotification(context, queue);
        await _processHealthDataRetrievedNotification(context, queue);
        await _processChannelIndexNotification(context, queue);
        await _processChannelDisclosureConsentRequestNotification(
          context, queue,
        );
        await _processFidoRegistrationTokenResultNotification(
          context, queue,
        );
        await _processDidIssuanceResultNotification(context, queue);
        await _processVcIssuanceResultNotification(context, queue);
      },
  ));

  bool _processPassphraseChangedNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) {
    if (!queue.isPassphraseChanged(storage) || _restartDialogShown) {
      return false;
    }
    _restartDialogShown = true;

    Future.microtask(() async {
        try {
          await showConfirmDialog(
            context, "Need to restart the app.".i18n,
            "Your passphrase was changed on other device,"
            " so you need to restart the application.".i18n,
            showCancelButton: false,
            okButtonLabel: "Restart".i18n,
          );
          await dismissPassphraseChangedNotification(storage);
          storage.clearCipher();
          App.restart(context);
        } finally {
          _restartDialogShown = false;
        }
    });
    return true;
  }

  Future<void> _processContentUpdatesNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) async {
    var n;
    while ((n = queue.nextContentUpdates) != null) {
      if (n.storage.id != storage.id) continue;

      try {
        var modelResult = await n.model.catchError((e, s) {
            showError(context, e, s);
            return null;
        });
        if (modelResult == null) continue;

        var model = modelResult.model;
        if (model is Friend) {
          await _setTabAndPushPage(friendsTab, (_) => FriendPage(model));
        } else if (model is Channel) {
          await _setTabAndPushPage(channelsTab, (_) => ChannelPage(model));
          if (await (n as ChannelNotification).needTimelineNotify == true) {
            mayOpenTimelinePage(
              null, account, model, navigator: currentTab.navigator!,
            );
          }
        }
      } finally {
        await n.setCheckedToNow();
      }
    }
  }

  Future<void> _processFriendNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) async {
    var f;
    while ((f = queue.nextFriend) != null) {
      if (f.storage.id != storage.id) continue;

      var friend = f;
      await _setTabAndPushPage(friendsTab, (_) => FriendPage(friend));
    }
  }

  Future<void> _processHealthDataRetrievedNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) async {
    var s;
    while ((s = queue.nextHealthDataRetrieved) != null) {
      if (s.storage.id != storage.id) continue;

      if (await s.isChannelTimeline) {
        var channel = await s.timelineAsChannel;
        await _setTabAndPushPage(channelsTab, (_) => ChannelPage(channel));
        mayOpenTimelinePage(
          null, account, channel, navigator: currentTab.navigator!,
        );
      } else {
        var lifeRecord = await s.timelineAsLifeRecord;
        if (await _setTab(homeTab)) mayOpenTimelinePage(
          null, account, lifeRecord, navigator: currentTab.navigator!,
        );
      }
    }
  }

  Future<void> _processChannelIndexNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) async {
    var n;
    while ((n = queue.nextChannelIndex) != null) {
      if (n.notification.storage.id != storage.id) continue;

      var channel = await queue.indexEventChannelOf(n).catchError((e, s) {
          showError(context, e, s);
          return null;
      });
      if (channel == null) continue;

      if (channel.indexEvent == null) {
        await showConfirmDialog(
          context, "Channel controller".i18n,
          "The specified timeline item was not found: %s\n\nOpen the timeline usual.".i18n.fill([ n.itemId ]),
          showCancelButton: false,
        );
      } else await channel.indexEvent!.syncSilently();

      await _setTabAndPushPage(channelsTab, (_) => ChannelPage(channel));
      mayOpenTimelinePage(
        null, account, channel, navigator: currentTab.navigator!,
        dateTime: channel.indexEvent?.begin,
      );

      if (channel.indexEvent?.sweepNotificationTimes() == true) {
        await channel.syncSilently();
      }
    }
  }

  Future<void> _processChannelRequestNotification(
    BuildContext context,
    ChannelRequest? next(), [
      bool? consentRequested,
  ]) async {
    var n;
    while ((n = next()) != null) {
      var channel = await _findChannel(
        n.channelId, () => _showChannelNotFoundDialog(n.channelId),
      );
      if (channel == null) continue;

      await _setTabAndPushPage(
        channelsTab, (_) => ChannelPage(
          channel, consentRequested: consentRequested,
        ),
      );

      if (n.itemTags.isNotEmpty) {
        var item = await _findItemByTag(context, channel, n.itemTags.first);
        mayOpenTimelinePage(
          null, account, channel, navigator: currentTab.navigator!,
          dateTime: item?.begin,
        );
      }
    }
  }

  Future<void> _processChannelDisclosureConsentRequestNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) => _processChannelRequestNotification(
    context, () => queue.nextChannelDisclosureConsentRequest, true,
  );

  Future<void> _processFidoRegistrationTokenResultNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) => _processChannelRequestNotification(
    context, () => queue.nextFidoRegistrationTokenResult,
  );

  Future<void> _processDidIssuanceResultNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) => _processChannelRequestNotification(
    context, () => queue.nextDidIssuanceResult,
  );

  Future<void> _processVcIssuanceResultNotification(
    BuildContext context,
    SystemNotificationQueue queue,
  ) => _processChannelRequestNotification(
    context, () => queue.nextVcIssuanceResult,
  );

  Future<void> _showChannelNotFoundDialog(String? channelId) => showDialog(
    context: context, barrierDismissible: false,
    builder: (context) => AlertDialog(
      title: Text("Channel controller".i18n),
      content: Text("Can't find the target channel: %s\n\nThe channel may not be referrable from the account, or it may not be registered in the database yet.\n\nPlease retry in a while.".i18n.fill([ channelId ?? "" ])),
      actions: [
        Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(left: 10),
          child: Row(
            children: [
              TextButton(
                style: TextButton.styleFrom(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                ),
                child: Text("Discard request".i18n),
                onPressed: () async {
                  if (await _onRemoveConsentRequest(channelId)) {
                    Navigator.pop(context);
                  }
                },
              ),
              const Spacer(),
              TextButton(
                child: Text(
                  MaterialLocalizations.of(context).okButtonLabel,
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
        ),
      ],
    ),
  );

  Future<bool> _onRemoveConsentRequest(String? channelId) async {
    if (channelId == null) return false;

    if (!await showConfirmDialog(
        context, "Discard request".i18n,
        "Discard the request.\n\nYou will not be notified until the request is made again. Are you ok?".i18n,
    )) return false;

    await runWithProgress(
      context,
      () => removeChannelDisclosureConsentRequest(storage, channelId),
    );
    return true;
  }

  Future<void> _processControl(Control control) async {
    try {
      switch (control.command) {
        case friendRegControl:
          await _processFrendRegControl(control); break;
        case friendRequestListControl:
          await _processFrendRegListControl(control); break;
        case channelControl:
          await _processChannelControl(control); break;
        case timelineControl:
          await _processTimelineControl(control); break;
      }
    } catch (e, s) {
      showError(context, e, s);
    }
  }

  Future<void> _processFrendRegControl(Control control) => _setTabAndPushPage(
    friendsTab, (_) => FriendAddDialog(friendRequestString: control.param),
    fullscreenDialog: true,
  );

  Future<void> _processFrendRegListControl(Control control) async {
    showMessage(context, "FriendRegList control is not supported, yet.");
  }

  Future<Channel?> _findChannel(
    String? channelId, [
      AsyncCallback? onNotFound,
  ]) async {
    if (channelId == null) return null;

    var n = account.notificationRegistry.channelNotificationById(channelId);
    if (!await n.exists) {
      await account.notificationRegistry.updateSilently();
      n = account.notificationRegistry.channelNotificationById(channelId);
    }

    // Should check DisclosedToUser...
    var channel = (await n.model)?.model;
    if (channel == null) {
      if (onNotFound != null) await onNotFound();
      else await showConfirmDialog(
        context, "Channel controller".i18n,
        "Can't find the target channel: %s\n\nThe channel may not be referrable from the account, or it may not be registered in the database yet.\n\nPlease retry in a while.".i18n.fill([ channelId ]),
        showCancelButton: false,
      );
    }
    return channel;
  }

  Future<void> _processChannelControl(Control control) async {
    var channel = await _findChannel(control.param);
    if (channel == null) return;

    _setTabAndPushPage(channelsTab, (_) => ChannelPage(channel));
  }

  Future<Item?> _findItemByTag(
    BuildContext context,
    Channel channel,
    String itemTag,
  ) async {
    Item? item; try {
      item = await channel.timelineItemModelByTag(itemTag);
    } on ArgumentError {
    } catch (e, s) {
      showError(context, e, s);
    }
    if (item == null) {
      await showConfirmDialog(
        context, "Channel controller".i18n,
        "The specified timeline item was not found: %s\n\nOpen the timeline usual.".i18n.fill([ itemTag ]),
        showCancelButton: false,
      );
    } else await item.syncSilently();

    return item;
  }

  Future<void> _processTimelineControl(Control control) async {
    var channel = await _findChannel(control.param);
    if (channel == null) return;

    var item = await _findItemByTag(context, channel, control.tag!);

    await _setTabAndPushPage(channelsTab, (_) => ChannelPage(channel));
    mayOpenTimelinePage(
      null, account, channel, navigator: currentTab.navigator!,
      dateTime: item?.begin,
    );
  }
}
