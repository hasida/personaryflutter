import 'package:plr_ui/plr_ui.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'active_app_definition.g.dart';

const _key = "active_app_definition";

@riverpod
class ActiveAppDefinition extends _$ActiveAppDefinition {
  FutureOr<String?> build(Storage storage) => storage.getSettingValue(_key);

  Future<void> set(AppDefinition appDefinition) => setId(appDefinition.id);
  Future<void> setId(String id) async {
    state = await AsyncValue.guard(() async {
        await storage.putSettingValue(_key, id);
        return id;
    });
  }

  Future<void> clear() async {
    state = await AsyncValue.guard(() async {
        await storage.removeSetting(_key);
        return null;
    });
  }
}
