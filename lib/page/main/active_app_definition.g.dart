// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_app_definition.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$activeAppDefinitionHash() =>
    r'5bbe8206dd31caa413f99155b0d7e4a51f5ce460';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$ActiveAppDefinition
    extends BuildlessAutoDisposeAsyncNotifier<String?> {
  late final Storage<dynamic> storage;

  FutureOr<String?> build(
    Storage<dynamic> storage,
  );
}

/// See also [ActiveAppDefinition].
@ProviderFor(ActiveAppDefinition)
const activeAppDefinitionProvider = ActiveAppDefinitionFamily();

/// See also [ActiveAppDefinition].
class ActiveAppDefinitionFamily extends Family<AsyncValue<String?>> {
  /// See also [ActiveAppDefinition].
  const ActiveAppDefinitionFamily();

  /// See also [ActiveAppDefinition].
  ActiveAppDefinitionProvider call(
    Storage<dynamic> storage,
  ) {
    return ActiveAppDefinitionProvider(
      storage,
    );
  }

  @override
  ActiveAppDefinitionProvider getProviderOverride(
    covariant ActiveAppDefinitionProvider provider,
  ) {
    return call(
      provider.storage,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'activeAppDefinitionProvider';
}

/// See also [ActiveAppDefinition].
class ActiveAppDefinitionProvider
    extends AutoDisposeAsyncNotifierProviderImpl<ActiveAppDefinition, String?> {
  /// See also [ActiveAppDefinition].
  ActiveAppDefinitionProvider(
    Storage<dynamic> storage,
  ) : this._internal(
          () => ActiveAppDefinition()..storage = storage,
          from: activeAppDefinitionProvider,
          name: r'activeAppDefinitionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$activeAppDefinitionHash,
          dependencies: ActiveAppDefinitionFamily._dependencies,
          allTransitiveDependencies:
              ActiveAppDefinitionFamily._allTransitiveDependencies,
          storage: storage,
        );

  ActiveAppDefinitionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.storage,
  }) : super.internal();

  final Storage<dynamic> storage;

  @override
  FutureOr<String?> runNotifierBuild(
    covariant ActiveAppDefinition notifier,
  ) {
    return notifier.build(
      storage,
    );
  }

  @override
  Override overrideWith(ActiveAppDefinition Function() create) {
    return ProviderOverride(
      origin: this,
      override: ActiveAppDefinitionProvider._internal(
        () => create()..storage = storage,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        storage: storage,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<ActiveAppDefinition, String?>
      createElement() {
    return _ActiveAppDefinitionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ActiveAppDefinitionProvider && other.storage == storage;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, storage.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ActiveAppDefinitionRef on AutoDisposeAsyncNotifierProviderRef<String?> {
  /// The parameter `storage` of this provider.
  Storage<dynamic> get storage;
}

class _ActiveAppDefinitionProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<ActiveAppDefinition,
        String?> with ActiveAppDefinitionRef {
  _ActiveAppDefinitionProviderElement(super.provider);

  @override
  Storage<dynamic> get storage =>
      (origin as ActiveAppDefinitionProvider).storage;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
