import "package:i18n_extension/i18n_extension.dart";
import 'package:plr_util/src/util/base_translations.dart';

extension Localization on String {
  static Translations t = Translations.byText("en_us") +
  unknownTranslation +
  discardRequestTranslation +
  discardRequestConfirmTranslation +
  {
    "en_us": "Need to restart the app.",
    "ja_jp": "アプリを再起動します",
  } +
  {
    "en_us": "Your passphrase was changed on other device, so you need to restart the application.",
    "ja_jp": "パスフレーズが他の端末で変更されたため、アプリを再起動します。",
  } +
  {
    "en_us": "Restart",
    "ja_jp": "再起動",
  } +
  {
    "en_us": "Channel controller",
    "ja_jp": "チャネルコントローラ",
  } +
  {
    "en_us": "Can't find the target channel: %s\n\nThe channel may not be referrable from the account, or it may not be registered in the database yet.\n\nPlease retry in a while.",
    "ja_jp": "対象チャネルが見付かりません: %s\n\nチャネルがアカウントから参照可能ではないか、まだデータベースに登録されていない可能性があります。\n\nしばらく後に、再度お試しください。",
  } +
  {
    "en_us": "The timeline of the target channel is disabled.",
    "ja_jp": "対象チャネルのタイムラインは無効です。",
  } +
  {
    "en_us": "The specified timeline item was not found: %s\n\nOpen the timeline usual.",
    "ja_jp": "指定のタイムラインアイテムが見つかりませんでした: %s\n\n通常のタイムラインを開きます。",
  } +
  {
    "en_us": "The info channel of the friend(s) are subscribed",
    "ja_jp": "友達の広報チャネルを購読しました",
  } +
  {
    "en_us": "Friend(s) are registered",
    "ja_jp": "友達が登録されました",
  };

  String get i18n => localize(this, t);
  String fill(List<Object> params) => localizeFill(this, params);
}
