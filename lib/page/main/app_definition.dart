import 'dart:typed_data' show Uint8List;

import 'package:flutter/material.dart' hide Image;
import 'package:image/image.dart' show Image;
import 'package:plr_ui/plr_ui.dart';

import '../../theme.dart';
import '../../widget/layout.dart' show emptyPage;
import '../channels/channels_page.dart';
import '../custom/custom_page.dart';
import '../friends/friends_page.dart';
import '../home/home_page.dart';
import '../announcement/announcement_page.dart';
import '../settings/settings_page.dart';

const homeTab = const PersonaryHome();
const channelsTab = const PersonaryChannels();
const friendsTab = const PersonaryFriends();
const announcementTab = const PersonaryAnnouncement();
const settingsTab = const PersonarySettings();

mixin _DummyApplTitleImageMixin implements HasApplTitleInterface {
  @override
  Mmdata? get applTitle => null;

  @override
  bool get hasTitle => hasTitleLabel || hasTitleImage;

  @override
  bool get hasTitleLabel => titleLabel != null;

  @override
  bool get hasTitleImage => false;

  @override
  Stream<Uint8List> get titleImageData => const Stream.empty();

  @override
  Stream<Image> get titleImage => const Stream.empty();
}

abstract interface class _HasTitleIcon {
  Widget get _titleIcon;
}

extension TabTitleIcon on HasApplTitleInterface {
  Stream<Widget> get titleIcon async* {
    if (this is _HasTitleIcon) yield (this as _HasTitleIcon)._titleIcon;
    else yield* titleImageWidget;
  }
}

extension ApplTabPageBuild on ApplTabPageInterface {
  static late Expando<GlobalKey<NavigatorState>> _navigatorKeys;
  static late Expando<Widget> _pages;

  static void reset() {
    _navigatorKeys = Expando<GlobalKey<NavigatorState>>();
    _pages = Expando<Widget>();
  }

  GlobalKey<NavigatorState> get navigatorKey =>
    _navigatorKeys[this] ??= GlobalKey<NavigatorState>();

  NavigatorState? get navigator => navigatorKey.currentState;

  Widget build() => _pages[this] ??= switch (this) {
    ApplHomeInterface() => HomePage(),
    ApplFriendsInterface() => FriendsPage(),
    ApplChannelsInterface() => ChannelsPage(),
    ApplAnnouncementInterface() => AnnouncementPage(),
    PersonarySettings() => const SettingsPage(),
    ApplCustomInterface() => CustomPage(this as ApplCustomInterface),
    _ => emptyPage,
  };
}

class DefaultAppDefinition with _DummyApplTitleImageMixin
  implements AppDefinitionInterface {

  final Account account;
  const DefaultAppDefinition(this.account);

  @override
  final id = "_DefaultAppDefinition_";

  @override
  PlrId get plrId => account.root.plrId;

  @override
  Literal get titleLabel => DirectLiteral(const {
      "": "Personary",
  });

  @override
  Literal get commentText => DirectLiteral(const {
      "": "Default App defition of Personary.",
      "ja": "Personaryのデフォルトアプリ定義です。",
  });

  @override
  final roles = const <Literal>[];

  @override
  final initialTab = 2;

  @override
  final tabPages = const [
    const PersonaryHome(),
    const PersonaryFriends(),
    const PersonaryChannels(),
    const PersonaryAnnouncement(),
  ];

  @override
  String? get colorSchemeSeedString => null;
  @override
  String? get backgroundColorString => null;
  @override
  String? get focusColorString => null;
  @override
  String? get textButtonColorString => null;
  @override
  String? get appBarBackgroundColorString => null;
  @override
  String? get appBarForegroundColorString => null;
  @override
  String? get navigationBarBackgroundColorString => null;
  @override
  String? get navigationBarIndicatorColorString => null;
  @override
  String? get fabBackgroundColorString => null;
  @override
  String? get fabForegroundColorString => null;
  @override
  String? get cardColorString => null;
  @override
  double? get cardElevation => null;
  @override
  String? get sectionHeaderColorString => null;

  ThemeData get theme => defaultTheme;
}

abstract class PersonaryTabPage with _DummyApplTitleImageMixin
  implements _HasTitleIcon, ApplTabPageInterface {

  const PersonaryTabPage();
}

class PersonaryHome extends PersonaryTabPage implements ApplHomeInterface {
  const PersonaryHome();

  @override
  Literal get titleLabel => DirectLiteral(const {
      "": "Home",
      "ja": "ホーム",
  });

  @override
  final order = 0;

  final _titleIcon = const Icon(Icons.home);

  @override
  Literal get commentText => DirectLiteral(const {
      "": "Move to the Home Tab.",
      "ja": "ホームタブに移動します。",
  });
}

class PersonaryFriends extends PersonaryTabPage
implements ApplFriendsInterface {

  const PersonaryFriends();

  @override
  Literal get titleLabel => DirectLiteral(const {
      "": "Friends",
      "ja": "友達",
  });

  @override
  final order = 1;

  final _titleIcon = const Icon(Icons.people);

  @override
  Literal get commentText => DirectLiteral(const {
      "": "Move to the Friends Tab.",
      "ja": "友達タブに移動します。",
  });
}

class PersonaryChannels extends PersonaryTabPage
  implements ApplChannelsInterface {

  const PersonaryChannels();

  @override
  Literal get titleLabel => DirectLiteral(const {
      "": "Channels",
      "ja": "チャネル",
  });

  @override
  final order = 2;

  final _titleIcon = const Icon(Icons.chat);

  @override
  Literal get commentText => DirectLiteral(const {
      "": "Move to the Channels Tab.",
      "ja": "チャネルタブに移動します。",
  });
}

class PersonaryAnnouncement extends PersonaryTabPage
  implements ApplAnnouncementInterface {

  const PersonaryAnnouncement();

  @override
  Literal get titleLabel => DirectLiteral(const {
      "": "Announcement",
      "ja": "お知らせ",
  });

  @override
  final order = 3;

  final _titleIcon = const Icon(Icons.info);

  @override
  Literal get commentText => DirectLiteral(const {
      "": "Move to the Announcement Tab.",
      "ja": "お知らせタブに移動します。",
  });
}

class PersonarySettings extends PersonaryTabPage
  implements ApplTabPageInterface {

  const PersonarySettings();

  @override
  Literal get titleLabel => DirectLiteral(const {
      "": "Settings",
      "ja": "設定",
  });

  @override
  final order = 3;

  final _titleIcon = const Icon(Icons.settings);

  @override
  Literal get commentText => DirectLiteral(const {
      "": "Move to the Settings Tab.",
      "ja": "設定タブに移動します。",
  });
}
