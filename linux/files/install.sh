#! /bin/sh

CWD="$( cd "$( dirname "$0" )" && pwd )"

APPLICATIONS_DIR=${HOME}/.local/share/applications
mkdir -p ${APPLICATIONS_DIR}

cat << EOF > ${APPLICATIONS_DIR}/personary.desktop
[Desktop Entry]
Type=Application
Name=Personary
Exec=${CWD}/personary %u
StartupNotify=false
MimeType=x-scheme-handler/plrctl;
EOF

xdg-mime default personary.desktop x-scheme-handler/plrctl
