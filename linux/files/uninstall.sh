#! /bin/sh

MIMEAPPS_LIST=${HOME}/.config/mimeapps.list
APPLICATIONS_DIR=${HOME}/.local/share/applications

sed -e '/^x-scheme-handler\/plrctl=/D' < ${MIMEAPPS_LIST} > ${MIMEAPPS_LIST}.new
mv ${MIMEAPPS_LIST}.new ${MIMEAPPS_LIST}
rm -f ${APPLICATIONS_DIR}/personary.desktop

