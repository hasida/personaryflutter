package com.assemblogue.plr.app.personary

import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

const val PERSONARY_CHANNEL = "personary"

class MainActivity: FlutterFragmentActivity() {
	override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
		super.configureFlutterEngine(flutterEngine)

		MethodChannel(
			flutterEngine.dartExecutor.binaryMessenger, PERSONARY_CHANNEL
		).setMethodCallHandler { call, result ->
			when (call.method) {
				"moveTaskToBack" -> result.success(moveTaskToBack(true))
				else -> result.notImplemented()
			}
		}
	}
}
