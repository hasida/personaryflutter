# personaryFlutter

Personary in Flutter = personaryFlutter App

personaryFlutterと同じディレクトリにplrDartを配置してください。

# ENV
.env

SYSTEM_PROMPT_API_KEY=システムプロンプト取得時のAPIキー

.envを変更した場合は以下のコマンドでジェネレータを実行
`flutter pub run build_runner build --delete-conflicting-outputs`

# ビルド Build

Androidビルドには、リリース署名の設定が必要です。

リリース署名のキーストアを持っていない場合は、
`android/app/gradle.properties.debug`を、
`android/app/gradle.properties`にコピーしてください。
デバッグ鍵で署名が行なわれます。

リリース署名のキーストアを持っている場合は、
`android/app/gradle.properties`に、次の内容を記述してください。

```
productKeyStore=キーストアファイルのパス
productKeyStorePassword=キーストアのパスワード
productKeyAlias=キーエイリアス名
productKeyAliasPassword=キーエイリアスのパスワード
```

iOSビルド設定

・実行権の付与
`chmod 755 ios/scripts/retrieve_dart_defines.sh`
 
ios/.symlinks/plugins/dart_vlc/iOS/dart_vlc.podspecを編集

・シミュレータビルド時
    1. s.script_phasesにコメントアウトを追加
```
        s.script_phases     = [
    #    {
    #        :name => 'Fetch submodules... only needed if using git in pubspec.yaml',
    #        :show_env_vars_in_log => true,
    #        :script => 'cd ${PODS_TARGET_SRCROOT}/.. && git submodule update --init --recursive',
    #        :execution_position => :before_compile
    #    },
         {
            :name => 'Build Simulator lib',
            :show_env_vars_in_log => true,
            :script => 'cmake -Bdartvlc_core '\
            '${PODS_TARGET_SRCROOT}/../dartvlc '\
            '-GXcode '\
            '-DCMAKE_SYSTEM_NAME=iOS && '\
            'pwd && '\
            'xcodebuild -project dartvlc_core/dart_vlc_core.xcodeproj '\
            '-sdk iphonesimulator '\
            '-scheme dart_vlc_core '\
            'CONFIGURATION_BUILD_DIR=${PODS_TARGET_SRCROOT}/deps/simulator',
            :execution_position => :before_compile
        },
    #     {
    #        :name => 'Build Device lib',
    #        :show_env_vars_in_log => true,
    #        :script => 'xcodebuild -project dartvlc_core/dart_vlc_core.xcodeproj '\
    #        '-sdk iphoneos '\
    #        '-scheme dart_vlc_core '\
    #        'CONFIGURATION_BUILD_DIR=${PODS_TARGET_SRCROOT}/deps/device',
    #        :execution_position => :before_compile
    #     }
        ]
 ```

   2. `cd iOS`（personaryFlutter/iOS）
      `pod install` を実行

・実機ビルド時
    1. s.script_phasesにコメントアウトを追加
```
        s.script_phases     = [
    #    {
    #        :name => 'Fetch submodules... only needed if using git in pubspec.yaml',
    #        :show_env_vars_in_log => true,
    #        :script => 'cd ${PODS_TARGET_SRCROOT}/.. && git submodule update --init --recursive',
    #        :execution_position => :before_compile
    #    },
    #     {
    #        :name => 'Build Simulator lib',
    #        :show_env_vars_in_log => true,
    #        :script => 'cmake -Bdartvlc_core '\
    #        '${PODS_TARGET_SRCROOT}/../dartvlc '\
    #        '-GXcode '\
    #        '-DCMAKE_SYSTEM_NAME=iOS && '\
    #        'pwd && '\
    #        'xcodebuild -project dartvlc_core/dart_vlc_core.xcodeproj '\
    #        '-sdk iphonesimulator '\
    #        '-scheme dart_vlc_core '\
    #        'CONFIGURATION_BUILD_DIR=${PODS_TARGET_SRCROOT}/deps/simulator',
    #        :execution_position => :before_compile
    #    },
         {
            :name => 'Build Device lib',
            :show_env_vars_in_log => true,
            :script => 'xcodebuild -project dartvlc_core/dart_vlc_core.xcodeproj '\
            '-sdk iphoneos '\
            '-scheme dart_vlc_core '\
            'CONFIGURATION_BUILD_DIR=${PODS_TARGET_SRCROOT}/deps/device',
           :execution_position => :before_compile
         }
        ]
 ```    
   2. `cd iOS`（personaryFlutter/iOS）
         `pod install` を実行 
    

# 内容 Contents

libがPersonaryの主な機能を含む。
UIはlib/page/の下にあり、メイン画面はlib/page/main/main_page.dart。
たとえば51行目あたりのCupertino.dartでタブを生成。

lib contains main functions of Personary.
UI is under lib/page/.
The main screen is lib/page/main/main_page.dart.
For instance, Cupertino.dart creates tabs.

# UI

成熟したGUIツールがないので、UIのコードを手で書いて開発している。
ファイルを保存すればデバック中のアプリに反映されるHotReload機能が便利。

We are hand-writing UI codes because no mature GUI tools are available yet.
The HotRelaod function is useful, which reflects sotored files into the app being debugged.
